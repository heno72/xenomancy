---

kanban-plugin: basic

---

## Open

- [ ] Update [Adran's](/Characters/Adok%20Ranensis.md) date of birth as Adran has been around for thousands of years


## On-going

- [ ] **Completes [[Outline - Messianic Arrival|Messianic Arrival]] draft<br>**<br><br>I basically plans to split it into two:<br>- [ ] [Good heavens](/Chapters/Xe-1%20Good%20heavens.md)<br>- [ ] [On earth as it is in Heaven](/Chapters/Xe-1%20On%20earth%20as%20it%20is%20in%20Heaven.md)


## Done

**Complete**




%% kanban:settings
```
{"kanban-plugin":"basic"}
```
%%