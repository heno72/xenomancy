# Table of Content

## The Three Ways War
1. [**The Collision Path.**](../Chapters/The%20Collision%20Path.md)
The activation sequences of the Godfluenza.
1. [**The Agency.**](../Chapters/The%20Agency.md)
The Kuker Intelligence Agency experiencing the first ever paranormal experience.
1. [**The WTF.**](../Chapters/The%20WTF.md)
The first ever encounter between the WTF and the Agency.
1. [**The Insurgency.**](../Chapters/The%20Insurgency.md)
The beginning of EPL's Insurgency Branch.
1. [**The EPL Group.**](../Chapters/The%20EPL%20Group.md)
The EPL's Insurgency Branch's maiden move.
1. [**Angel's Job.**](../Chapters/Angel's%20Job.md)
The clash between Heinrich and Anthony.
1. [**The Birth of Daniel Ashton.**](../Chapters/The%20Birth%20of%20Daniel%20Ashton.md)
An Etoan offered a chance to save Earth again, for the second time.
1. [**The Disturbances.**](../Chapters/The%20Disturbances.md)
Introducing the core casts of the Christ Cluster.
Anderson came by and have them know about the upcoming Denefasan invasion.
1. [**Inspections.**](../Chapters/Inspections.md)
Anderson is taken away by Henokh and Co.
Ashton came by and visited Steven and Daniel.
Ashton told them about the possible second coming.
1. [**The Artifact Hunt.**](../Chapters/The%20Artifact%20Hunt.md)
Hendrik got a mission to find more clues about the Beast Cluster, that is, the EPL's Insurgency Branch.
Through the mission, he learned about their next target: The Black's Staff.
2. [**Breach.**](../Chapters/Breach.md)
Aditya’s raiding the Securion’s security fault, where Black’s Staff is stored, BAIK agents are trying to stop it.
In the end however, they managed to capture two or three of Aditya’s men, yet Aditya managed to run with the Black’s Staff.
With that information obtained from Aditya’s men, they are able to locate Aditya’s base.
3. [**The Serpent.**](../Chapters/The%20Serpent.md)
Search for the serpent, then it led to a cult of the Umbra Cahaya, about the incoming Sea Serpent, Lord Yam as the Helper prophesied by Jesus.
Going deeper, Henokh and co surprised as discovery about their motive is made, that is to steal Black’s Staff to trigger Judgement Day.
It’s quite obvious at the time that they must stop it.
4. [**Betrayal.**](../Chapters/Betrayal.md)
Infiltration deep into the network of the EPL, revealed deeper plans to incite Judgement Day with the use of Black's Staff.
Turns of events and Heavenly Host's intervention, forces Anderson to conclude that to end all of this, he has to kill both Aditya and Henokh.
5. **Dawn Strike.**
5. **Anderson and Aditya.**

## The Oneironaut
1. [**The Divine Council of Earth.**](../Chapters/The%20Divine%20Council%20of%20Earth.md)
Ashton proposed a plan to fight the Divine Council of Earth directly.
To achieve that, they need an insider, that is Kothar.
To have a bargaining position against Kothar, they need to do him a favor, get him out of hell.
To get him out of hell, they'd need to find El, the First Divine, and somehow exploit him to allow safe passage to Hell.
1. [**The Divine Guilt.**](../Chapters/The%20Divine%20Guilt.md)
Manov embark on a journey with Anthony to discover Kothar's guilt.
Heinrich came by and fight with Manov so that he could kill Anthony, but Manov said no.
1. [**The List.**](../Chapters/The%20List.md)
An attempt to stop a serial killer from killing people on two different worlds, leads to the discovery of a weird phenomenon of world-slipping.
1. [**The Last Nightmare.**](../Chapters/The%20Last%20Nightmare.md)
In a group meeting, Michelle, Anthony, and David had a vision of who are in hell.
Michele and Anthony had their father in Hell, and so does David's mom.
1. [**Paramundus Integra.**](../Chapters/Paramundus%20Integra.md)
Hendrik must discover why the killings happen, and to protect the witnesses.
Meanwhile Steven and Co. had to save Helena from Hell.
With the help and direction from Ashton and Adran, they were beamed to Paramundus Integra.
1. [**Mirror Image.**](../Chapters/Mirror%20Image.md)
The Integra team tried to find their mirror image there, so that they could acquire their IDs to easily roam with authentic identification.
To their surprise, Hendrik Integra wrote about the life story of Xenomancy's casts.
The team is split, Henokh and Co. went to find more clues on the killings and why, while Steven and Co. went out to find Hendrik, that might know about El.
1. [**Finding Hendrik.**](../Chapters/Finding%20Hendrik.md)
Receiving a reminder that they're in a completely different, parallel world.
1. [**Joseph.**](../Chapters/Joseph.md)
First encounter with the Men of Tiamat.
1. [**Fictional Fallacy.**](../Chapters/Fictional%20Fallacy.md)
Hendrik Integra's meetings with the casts of his story, Xenomancy.
Also the time Hendrik Integra realized that he is in Integra.
The first time Hendrik Integra is surprised to see the Men of Tiamat is real, here, in Integra.
1. [**Elbert.**](../Chapters/Elbert.md)
With Joseph, they tried to find Elbert and find other people in the list soon.
All while The Men of Tiamat chased them.
1. [**Escape Room.**](../Chapters/Escape%20Room.md)
Hendrik Integra and Steven and co. tried to pave their way out of the apartment.
1. [**Regroup**](../Chapters/Regroup.md)
The two team meet each other again, and they get to know each other.
There natives of Integra learn about the nature of their world, and the nature of Xenomancy's world.
