# Xenomancy
By Hendrik Lie.

## Part 1: The First Trigger

### [Polar Opposites](../Chapters/Xe-1%20Polar%20Opposites.md)

Yam's bluish white stripes where his eyes would be, followed the incoming Seraphim of Hadad.
His serpentine head hissed as he spotted Hadad, leading a swarm of golden Seraphim.
The dark ocean blue scales of Yam emanated bluish white aura as his fists clenched.
His white-scaled chests glowed even brighter.
The great first pair of featureless translucent membranous wings of him spread wide, slowing his flight.
The second pair of smaller wings twitched, just enough for him to adjust his course midair.

His countless number of Seraphim followed his course correction.
Their arms unfolded into thin radiating petals, bloomed into black thorny flowers.
They were aiming at the swarm of Hadad's Seraphim.

Hadad had his pitch black pearly gaze sternly aimed at Yam.
The orb embedded in his forehead sparked blue arcs to his horns atop his golden bull head.
The gold-hued skin of him glowed brighter, and his gold-ish white feathered first pair of wings thrusted hard.
His second pair of wings adjusted his course, as his fists clenched.
He knew the dark blue swarm of Yam would strike hard, his gaze didn't waver anyway.
His Seraphim didn't waver either.

The black thorny flowers flashed bright green beams.
Hadad's Seraphim had their bodies shattered, their wings blazed.
One by one, a number of golden Seraphim fell to the ground.

Hadad waved one of his arms toward a platoon of his Seraphim.
The platoon had their forehead orbs arced bright blue.
Arcs after arcs grew between their orbs and their horns.

In no time, a large number of blue electric arcs smote the ocean blue swarm of Yam's Seraphim.
Yam's Seraphim had their limbs shattered, their bodies shattered, their wings torn.
Another number of ocean blue Seraphim fell to the ground.

Yam's first pair of wings folded close to his body, as the second pair of wings turned hard.
He took a dive, and his Seraphim followed suit.
He aimed toward the ocean, at the time in the state of turmoil.

Gargantuan serpentine bodies danced around in the ocean as he approached.
Some jumped out of water at great force.
Roars could be heard miles away, even by the spectating inhabitants on the shore.

Hadad and his Seraphim chased the descending swarm of Yam's Seraphim.
Jolts of blue arcs from his golden Seraphim smote more and more of ocean blue Seraphim.
Green beams flashed from the ocean blue Seraphim, and more golden Seraphim set ablaze.

\---

"It was just a thunderstorm," an elder man said as he was spectating the event by the shore. His voice quivered.

"No, they are Yam and Hadad," said a foreign man.

"They are, who?"

"Yam-Nahar and Baal-Hadad, sons of Bull-El. They are fighting."

"How would you know?"

"Baal-Hadad, is the one that gave me this mark," he said as he uncovered his cowl.
Every strand of hair on his beige skin was silvery white.
His grayish irises gazed sternly at the forming crowd around him.

Not so long after, a number of ocean blue Seraphim and golden Seraphim fell along the shore, and the ocean.
Some crawled to the land, their limbs missing, their wings torn.
They were gigantic, each were three times as tall as the tallest man.

The spectators gazed in awe.
Something about those fallen giants amazed them.
Perhaps it was their beauty, or their stature, or maybe the divinity.

An ocean blue Seraphim rose, amidst the damage on his wings.
His right arm bore a broken black thorny flower.
Its petals shattered, cripled, bent.
He disengaged the petals, and his sleek, serpentine head gazed around.

His body was covered in scales, glowing dim ocean blue aura.
As he turned to the spectators, one could observe the white-scaled chests.
Three pairs of broad chests.
The first pair connected to his arms.
The second, largest and thickest pair connected firmly to his first pair of wings.
The third, smallest pair, close to his pelvis, connected to his second pair of smaller wings.

"Beautiful," exclaimed a young girl.

"Magnificient," said another man.

"What are they," asked the elderly.

"Seraphim," said the foreign man, "the servant of gods."

A golden Seraph rose, he approached the standing ocean blue Seraphim.
His gold-ish white feathered wings stretched, aroused.
The feathers were untidy, some were burnt, and his upper left wing had almost all of their feathers gone.
The body was that of a human, but with three pairs of chests, just like the ocean blue Seraphim.
The skin was almost like a human, except the gold-ish hint and glow.

The golden bull head huffed, his pearly black gaze sternly affixed at the ocean blue Seraphim.
The bluish white stripes met the pearly black pair eyes.
They were gazing at each other, and the ocean blue Seraph hissed at the golden Seraph.

As the orb in between the horns of the golden Seraph arced, the ocean blue Seraph jumped and tore the orb away.
The golden Seraph howled like a slaughtered bull, his hands reached for the hole on his head.
The serpentine head gaped, with a hiss almost like an incoming gust of wind whirl.

Sparks aroused from the fists of the golden Seraph, and they flew toward the ocean blue Seraph.
The ocean blue Seraph shifted into a stance, and the ocean advanced toward them.
The inhabitants ran away, avoiding the approaching wave.
The wave surrounded the ocean blue Seraph, and absorbed the sparks of the golden Seraph.
The ocean blue Seraph, fully submerged, raised an arm.
A very strong, narrow jet stream of ocean water split the body of the golden Seraph into halves.

“Why are they fighting?” The elderly man asked.
The foreign man gazed at the elderly, but gave no answer.

\---

He could not chase Yam who dived into the ocean.
Hadad soared just above the ocean, he wouldn't risk diving into the ocean.
Not with the Chaos Beasts guarding it.

Hadad was not at the dead end, as he produced a pair of gauntlets.
One to his left hand, another to his right hand.
He clenched the right-hand gauntlet.

"Yagrush, spread and chase Yam!"

Ribbons of gold lights spread from the gauntlet.
By the end of each ribbon, were wolves made of light.
They scoured, flew, turned, and dived.

The Chaos Beasts attempted to sever the ribbons, but their tentacles were severed instead.
The ocean blue Seraphim attempted to break the ribbons, but the light wolves mauled them instead.
More and more ribbons pierced through the ocean, as more Chaos Beasts and ocean blue Seraphim gathered to stop them.

The ribbons tightened, and pulled up at such a great force.
Yam resurfaced by force, hanging under the might of Yagrush.
A ribbon pierced through his chest, and warped around his body.

"There you are," said Hadad.

Hadad giggled at Yagrush, "it really works."

The bluish white stripes where Yam's eyes should be, winced at the sight of Hadad's gauntlets.

"That, the weapon Kothar made."

"You recognized it, even better!"

Yam said nothing.

If a bull could smile, that would be Hadad's expression at the time.
His huffs sounded like how a bull laughs, if they could laugh.
Hadad directed his grasp to Yam.

"Aymuri, castrate his will!"

Strings emerged, jolted out from the gauntlet.
They danced toward Yam's body, and strapped it tight.
Their ends pierced through every orifice of Yam's body, to the space between his scales, and to every open wound.

Golden hues spread in Yam's body, replacing his bluish hues, inch by inch.
If a snake could roar, that would be the sound Yam made at the time.
His body shook, winced, jolted.

A snake made of light emerged from the body of Yam, constrained by the strings of Aymuri.
The snake wiggled to break free, but the strings tied it harder and firmer.
The snake was then engulfed by the strings, forming a cocoon, into what appears to be a ball of light strings.
The ball of strings was held firmly on Hadad's left hand.
The strings detached from Yam's dimmed body, that fell to the sea.

Hadad smiled.

"Tiamat, is in my grasp now," he said.

\---

Hadad landed by the base of a cliff near the shore.
Spectators curiously surrounded him, and were surprised by Hadad's size.
Hadad was almost twice as tall as his Seraphim.
He recognized one man among the crowd, a foreign man.

"Alt Parahomen, the killer of your own brother, Has Homen, I did not expect to see you here."

"Lord Hadad," praised Alt.

Alt's fists clenched.

"How long has it been, nine thousand years?" Hadad bursted a giggle.

Alt's teeth grinded.

"It appears Has's blood keeps you alive for this long! Oh poor Alt," Hadad's giggle evolved into a laugh.

Hadad was still laughing, when a cliff gaped wide, and black, featureless, furless wolves with glowing white eyes emerged from the opening.
The wolves was unusual, as their forelimbs had unnaturally long and slender fingers folded to their humerus.
Those long, slender fingers were connected with some sort of membranes, resembled those of bat's wings.
The membranes connecting their fingers also connect to the side of their body, and to the base of their pelvis.
Another set of membranes connect the behind of their rear limbs with their thin, furless tail.

A giant, wrinkled, and ancient wolf with a tall, raised pair of ears followed them.
Its wing membranes were full of holes and damages.
Its eye-stripes were at level to Hadad's pearly black eyes.

"Mot," said Hadad.

The ancient wolf nodded, and opened its mouth.
It spoke in a surprisingly human-like voice.

"What is it?"

It didn't even sound like Mot was asking.
The voice was flat, featureless, almost without a soul.

Hadad spread his hands and wings.

"*What is it?* You're asking me 'what is it?' Is there nothing you want to say to your nephew?" Hadad raised an eyebrow, if a bull had one.

"What is it?"

Hadad's smile wore out, his lips tightened.
He folded his arms, and said, "I am now the rightful heir of the Divine Council. Don't you want to say anything about it?"

"What is it?"

There was a silence.

Alt burst in laughter.
It tickled his mind that the so called mighthy Hadad wasn't even respected by his own uncle, Mot, the God of the Underworld.
*It was worth watching,* Alt thought to himself.
He stopped just in time, as Hadad gazed toward him.
At this point, Alt had to also consider not to offend him too much, he didn't wish to have any worse punishment than what he already had: immortality.
It was good enough that Has' blood made him youthful and immortal at the same time.
The last thing Alt would want to have would be immortality withouth perpetual youthfulness.

Hadad gazed back at Mot, "Forget it. I just want you to keep this with you."

Hadad handed the ball of light string to Mot.
Mot opened its mouth and swallowed the ball.

"Never give it to Yam," said Hadad.

Mot said nothing, and returned to the opening.
The black wolves followed it to the opening, and the opening closed.
The cliff returned to the way it was, no sign of a hole at all.

There was another silence.

"That's it? *He* is certainly not amusing," remarked Hadad.

He spread his wings, and faced the crowd.
He spread his arms, as if warning the crowd to stay away from him.
The crowd gave him some space.

He jumped, high, and he glided up.

His Seraphim already flew toward a giant hanging ark, advancing west-ward at great speed.
He catched up with the ark, and the ark was gone over the horizon.
The spectators can only gaze around, to discover that all of the Seraph bodies had been cleaned up, and the ocean was calm.

\---

"And that's all?"

Aditya was facing Henokh right beside him.
His elbow supported his head and shoulder.

"Yes, that's all."

Henokh didn't change his position, lying comfortably on his pillow.

"The story that your grandpa always tells you?"

"To the best that I can remember, yes."

"So, what does it mean?"

"Just a story," Henokh giggled.

Aditya laid back to the carpet they spread on the concrete floor, gazing at the stars.
There wasn't much to see, as light pollution of the city hid a significant fraction of the stars.
It was still the best location they favored for their weekly meetings.

"Your grandpa knew a lot of good stories," commented Derictor, right beside Henokh.

"Definitely much better than what Steven always tells to Daniel, right?"

"The ones about extrasolar gods? Your story wouldn't compare to his," said Derictor.

Henokh didn't say anything.
His lips tightened.
Derictor peeked at Henokh, and giggled.
Aditya followed giggling.

"No, really. But this one, your story, is more down to Earth, you know," said Derictor.

Henokh couldn't help it, but he stole a gaze at Derictor.

"El, Yam, Hadad, Mot, they're patron gods of the Canaanites. The way they're described in the myth reflects the cultural growths of our society."

"How so?"

Henokh couldn't help it.
He had to ask.
A grin started to form on his face.

"Yam is an important deity for the Phoenicians, a seafaring civilization. For example, the tripartite division of the Phoenician religion, Baal, Mot, and Yam, is thought to be a direct influence to Greek division between Zeus, Hades, and Poseidon."

Henokh nodded.
His gaze returned to the stars.
His thoughts wandered around.

"One thing I couldn't understand is, why Baal-Hadad and Yam-Nahar must be fighting?"

Derictor was about to speak, but Aditya spoke first.

"Yam, despite being portrayed as the god of the sea, is actually representing chaos, as opposed to Hadad that is representing order."

"Yes, yes that," said Derictor, "the fact that Hadad won in the story, represents our communal desire to adopt order. Order leads to civilization."

Henokh nodded, "so where is that order now? With all of the corruptions occurring in this world, and ugly politics and everything. It is chaotic."

"We can make it right," said Aditya, "order is inherently unsustainable. Chaos, on the other hand, is the natural order."

Henokh and Derictor turned silent.

"To restore the natural order, we destroy the current civilization, and start anew."

"I think," interrupted Derictor, "that's too extreme."

Henokh nodded.

\---

A day after, Henokh happened to sit on a bench beside a tree.
It was two hours before the next class started, and he decided to read a novel.
Little did he know that a man had been gazing at him within the past hours.

The man decided to be seated beside him, and greeted.

"Anderson Pondalissido," he offered a hand.

Without thinking, Henokh greeted the hand, and answered, "Henokh Lisander."

That was when he realized the pale white skin of the man, along with his all-white hair.
He wore a sunshade, and his ears were pointed.
His body was huge, and bulky, but something about him was emanating calmness.

"Grandson of Altair Oldman?"

"Yes," said Henokh.

"I'm from the Securion Incorporated International. Your grandfather was one of the shareholders."

"..., yes?"

"You must be very surprised by now."

"Yes, yes indeed I am."

Anderson fixed his position, and with a stern gaze, said "I am the answer."

"Of?"

"Of change, that you wish to achieve."

"What?"

"Chaos is definitely not the answer to this," Anderson let his answer sink in Henokh, before he continued, "Change must be applied in an orderly manner. And I can provide you that."

He opened his shade.
His eyes were almost all white, except for a vertical slit in the place of his iris.
It was almost shut tight.

"I am an etoan superior, one of the etoan Earth colonization initiative. You must be familiar with this, one of your acquaintances, Steven, Steven Pontirijaris, is also an etoan."

Henokh didn't say anything.
He froze.
It wasn't a fake contact lens or something.
He believed he saw the vertical irises twitched slightly.
It was as real as the vertical slit pupils of crocodiles he saw at the zoo.

"I have the resources, and manpower, required to slowly apply the change, for a betterment of the human race. But I need you to be a part of our team."

\---

A day prior to his meeting with Henokh, Aditya was lying down in the middle of a soccer field, gazing at the stars.
The lights interfere with the sight of stars, that he could almost see no star at all.
He didn't bother, he just pondered about life, the society, and everything.

That was when he saw something flying toward him.
He sprang up, that he strained his stomach.
His heart skipped a beat or two.
He forgot to breathe, and his knees shaked.

He had no idea what he was seeing.
If he were to describe this being, he'd describe it as follows.
In front of him was what Aditya could vaguely describe as a humanoid figure.
His head was like a snake, but instead of a pair of eyes, it got a pair of light stripes.
His scales were blue on the dorsal area of his body, along with his extremities.
On his ventral area were white scales.

He had six pairs of pectoral muscles.
The middle being the biggest that connects to a pair of strong limbs, supporting its serpentine membranous wings.
The uppermost pair connects to his arms, and the lowermost pair, on where it should be abdomen, were connected to a pair of secondary, smaller wings.
It was also tall.
Aditya felt dwarfed compared to it, like he was a child looking up to an adult.

"Be not afraid. You are looking at Lord Yam-Nahar."

Aditya's feet lost their strength, and he collapsed.
Before he knew it, he was kneeling in front of the beast.
Aditya said nothing.

Yam-Nahar kneeled down, and gazed at Aditya.
Aditya gazed back, he couldn't help it.
The white stripes where his eyes should be, have ribbon-like tails that waved around behind his head upwards.

"I have a proposal," Yam-Nahar started.

Aditya said nothing.

Yam-Nahar let out a certain amount of time to pass, and continued, "We both know how the world is right now. People live without guidance, without awareness. It was chaotic. The divines barely intervene. People worried about trivial, unimportant things."

Aditya nodded.

"It wasn't always like this."

Aditya stood still, on his knees.
He was certain that he could see the beast in front of him frowned, if its serpentine face could frown.
The next thing Aditya feel was Yam-Nahar's stern gaze piercing through Aditya's soul, followed with an offer, "If you follow my path, I can make this world reset. But to do that, you have to obtain what was taken away from me."

"H-H-What?" Aditya's voice finally returned.

"My will. My desire. You could also say, my balls, my courage. Whatever suits you."

Aditya's voice was retreating away from his throat again.
All he could voice was an inaudible mumble.
He couldn't even think what to say.

"Tomorrow. You'd listen to what your friend says. Pay attention. You'd know what I mean."

Aditya believed that the beast smiled, an assured smile.
In its eye-stripes, Aditya could sense satisfaction, as if Aditya already did the deeds Yam-Nahar requested.
The satisfaction was just there, as if the being knew Aditya could do it.

Lord Yam-Nahar rose, and turned his back to Aditya.
He walked away.
After a few steps away, he turned to Aditya,

"As of how, I'd guide you. You just have to do what you feel is right."

It was right at the moment Aditya was going to say, "How could I know which one is the right thing to do?"
Aditya hadn't had the chance to stop his mouth from asking it.

"I am Lord Yam-Nahar, the god of the sea, of the primordial chaos. I rule chaos, and patterns. I can make you do the right thing without you knowing. You just have to do your part."

Lord Yam-Nahar gazed at the sky, and jumped.
A strong gust of wind threw Aditya a few meters back.
Lord Yam-Nahar was no more.

Aditya rose and went to the spot where Lord Yam-Nahar last stood.
Giant footprints could be seen.
He touched it.
It was real, the depression was real.
He explored the rim, he explored the base.

He took pictures of it, but rather, he videotaped it.
He was lucky he videotaped it.
The depression bounced back, and the grasses restored in place, their wounds healed.
It was as if there were no depressions at all.
Except that Aditya had it videotaped.

Aditya played the video again and again that night.

\---

After their meeting, Aditya took his time to reconsider everything.
Lord Yam-Nahar didn't joke around.
He did get an answer to his question of what he must take, from Henokh's story.
He didn't know how to do it by himself though.
He needed a partner to do it, and the only one he could think about, would be his best friend, Henokh.

Henokh took his time to think about what Anderson said.
Steven said he knew Anderson, a fellow etoan colonists, a best friend of his grandpa.
When Henokh asked where his grandpa was, Steven said he was currently in a journey of attending various high schools on Earth.
He wouldn't normally believe in Steven's stories before.
It sounded absurd, and naturally he'd consider Steven was a man with big words, big lies.
He used to ponder, whether or not Steven genuinely believed in his lies.

His meeting with Anderson changed everything.
Steven's stories could be true.
Hidden society of foreign hominids, living among men in plain sight.
Steven didn't seem to hide the fact either, but why nobody paid them any attention?

Anderson's proposal, on the other hand, was a sound one.
However, he would need the support of his friends.
Above all, he wished that his best friend would accompany him in this absurd journey.

Henokh, Derictor, and Aditya decided to meet a week after their last meet up.
Henokh wanted to tell his friends about his encounter with this etoan superior.
Aditya wanted to tell his friends about his encounter with this Lord Yam-Nahar.
Derictor wanted to meet with his friends again and to see what they're up to.

"I can't do it myself," said Henokh and Aditya at the same time.

There was a pause as three of them gazed at each other.

"I want to do it together," said both, again.

Derictor gazed at them.

"What happened?" Inquired Derictor.

The two turned silent.
They realized how silly they’d sound.
Aditya thought his experience with Lord Yam-Nahar was too absurd for them.
Henokh thought it was crazy to say that an alien was asking them for help.
No one said anything for some time.

Derictor, irritated, gazed at Henokh, "You first, then."

"So...,"

Henokh lost his words, upon realization of how absurd it is, what he was going to say.

"So?" asked Derictor.

"Um, you know this Steven, right?" Asked Henokh, "that always tells us incredible stories, about extrasolar gods, about his smart car, and his *smartphone*?"

Aditya and Derictor nodded.

"What if," Henokh paused, "he's actually not lying about it?"

Derictor had his eyebrows raised, "..., what?"

Aditya didn't say anything.

"Yesterday I met a guy," Henokh chuckled, realizing that the next sentence would be an utterly ridiculous statement, "saying he's etoan superior. His eyes were like that crocodile eyes we saw at the zoo. Except that his eyes were all white, with vertical slit pupils,"

*Screw it*, Henokh thought to himself.
He'd just spill it out until he ran off something to say.
If they laughed at him, he'd just say it was just a joke and move on.

So he told them about his encounter.
Aditya and Derictor listened through it.
The more Henokh spoke about his weird encounter, the more Aditya realized, his story was at least as crazy as Henokh's story.

After Henokh was done with it, it was Aditya's turn.
He explained that a day before their meeting, he had this weird encounter with Lord Yam-Nahar, and how he said what he wanted to know will be told by his friend the day after.
It corresponds with the day Henokh told them about the story his grandpa always told him.
He also explained that, because of that, he believed the answer lies in Yam-Nahar, and they just need to follow his path.

Derictor was stupefied for a moment.
For two moments.
For three moments.
Derictor could not wrap his head around them.
His two friends turned lunatic in just a week.

"No, you gotta trust me in this," pleaded Henokh, "Anderson is the answer."

"Lord Yam-Nahar is the answer," Aditya added.

"Can we just ignore the answer?" Derictor paused, he almost let out a scream, "it was just a midnight talk, they were all nonsense."

Aditya and Henokh didn't say anything.

"Guys, do you seriously understand what you're talking about? Do you realize how silly they sound?"
Derictor gazed at Henokh, and then Aditya.
No one said anything.
"Henokh? Aditya? I believe I am not talking with walls right now, where are your answers?"

"I believe in what I saw," said Henokh and Aditya in a chorus.

Derictor hated that moment.
Both of his friends didn't lie about what they believe in.
But neither made any sense.

Henokh pulled his smartphone and showed Derictor a short video of Anderson and his eyes.
It was two thousand and eight, and the best camera for phone was around five megapixels.
With his candybar smartphone, equipped with a hundred and twenty eight megabytes of *Random Access Memory*, and internal storage of eighteen gigabytes, he showed them a clear sight of Anderson's eyes in a video of *Video Graphics Array*-compliant resolution, and took several photographs at close up range in five megapixels.

Aditya, another equally rich boy, pulled a similar phone.
He showed them a VGA-compliant video footage he took of Lord Yam-Nahar's footprints, that renormalized shortly after.
Unfortunately he did not take any pictures with his five megapixels rear camera.

Derictor saw both footages, and he was having a headache.
He could not determine which of them were telling the truth.
Both could've told the truth.

Derictor let out a sigh, and shook his head, "I am sorry, I can not choose." Derictor gazed at both of them, he wasn't even sure what face to make, "I think you two should resolve this."

Sufficient to say, that moment was not the best evening in their life.

### [The Boy from The Woods](../Chapters/Xe-1%20The%20Boy%20from%20The%20Woods.md)

Aditya found himself wandering in the woods.
It was what he liked to do in his free time.
A fresh start, from everything that had been the part of his life in years.

Friendship he had with Derictor and Henokh wasn't particularly a bad one.
It was just that, after their fight on Anderson and Lord Yam-Nahar, they were drifting away.
That, and the fact that he knew parting ways would be the right thing to do.

Aditya had been wondering about what Lord Yam-Nahar said to him.
The instructions themselves were very vague.
To achieve his dreams, Aditya had to acquire the object that Lord Yam-Nahar said to be his *will*.
Lord Yam-Nahar said he would guide Aditya, but Aditya had no idea how.
*Just have to do what you feel is right*, was all Lord Yam-Nahar said about his guide.
Aditya shook his head, it was still too abstract, or rather, absurd.

Until he heard a cry in the middle of the woods.
He looked for the source of the crying, penetrating the dense trees of the forest.
That was when he saw it, something like a sign.
A boy alone in the woods, accompanied with a vague floating water blob that somewhat resembled a dolphin.

"*Eomma, Appa,*" cried the boy, "*eodi gyeseyo?*"

The dolphin-like water body floated about, and danced, and chirped, apparently in an attempt to cheer the boy up.
Aditya didn't think that it would work.
The boy screamed louder, and the dolphin-like water body seemed to panic.

"Is this, considered *a sign*?" Said Aditya to himself.

The dolphin-like water body noticed Aditya, and it zoomed straight to him.
To Aditya's surprise, the dolphin-like water body introduced itself in plain English,
"I am Aurelia, a friend of this boy.
His parents left him alone in the woods."

"What?"

"*Ahjussi,*" said the boy, "*bumonim-eul chajdolog dowajuseyo.*"

"He asked you to help him find his parents," said Aurelia.

In his cry, the boy hugged Aditya's leg.
The boy was very young, probably around four to five years.
Aditya kneeled down until his eyes were more or less at level with the boy’s.

Aditya found himself asking to himself, or to Lord Yam-Nahar, or was he praying?
*Is this the path that you opened for me, Lord Yam-Nahar?*
There wasn't any answer from his own thoughts, nor was there from Lord Yam-Nahar.
He felt stupid, but something about this boy drew him in.

"What is your name?" Said Aditya in Indonesian.
The boy didn't answer, so Aditya repeated it again in English, "what is your name?"

"*Yang Gi-Hwan ibnida,*" replied the boy.

"Yang Gi-Hwan, don't worry okay," Aditya pointed at himself, "I am Aditya."

Yang Gi-Hwan hugged Aditya, his cries wetted Aditya's shirt, but Aditya didn't mind.
Based on the presence of Aurelia, and the fact that Yang Gi-Hwan was left alone, Aditya suspected that it was because of Yang Gi-Hwan's peculiarity.
Yang Gi-Hwan can summon spirits.

"I wasn't always a spirit, I used to be a real dolphin, roaming freely in the ocean.
I died, I got trapped in hell, and Gi-Hwan helped me out of hell," said Aurelius.

"You can read minds?"
Asked Aditya, Aurelia didn't respond.

\---

A year later, no one claimed to have lost a son named Yang Gi-Hwan in their city.
No one from the neighboring cities, not even from the counties.
To the best of Aditya's knowledge, the boy had no parents.
Aurelia was no help either, she didn't cooperate in identifying Yang Gi-Hwan's parents.
She decided that Yang Gi-Hwan's parents weren't worthy of her attention, as they dumped Yang Gi-Hwan to die in the woods.

Debating on what to do with this Yang Gi-Hwan boy, Aditya found himself looking at the boy.
Yang Gi-Hwan didn't say anything, he just sat in his bed in silence.
Aditya sighed, *screw it, whether or not it is the right thing to do, I couldn't just leave this boy alone,* he thought.
"Yang Gi-Hwan," said Aditya, the boy turned his gaze to Aditya, "do you want me to be your new father?"

Yang Gi-Hwan smiled.
Aditya's heart warmed up, he smiled back.
Yang Gi-Hwan rose from his bed and run to find Aditya's leg, he hugged it.

"No," said Yang Gi-Hwan, he looked up to Aditya, that kneeled down, "you're more like a *hyung*."

*Hyung* is used to refer to an older brother.
*Whatever the boy want to call me, I am okay,* thought Aditya.
Aditya let his hands wrap around Yang Gi-Hwan.

"*Hyung* it is," he said.

Indonesian officials wouldn't let Aditya to fill up the papers required to adopt Yang Gi-Hwan in *Hangul*, the Korean script.
Using Yang Gi-Hwan's romanized name would be okay, but the official suggested to use a more Indonesian name, to help the boy blend in.
Aditya sighed, he wasn't in a mood to argue, and the officials weren't very wrong either.
Aditya's concern was that Yang Gi-Hwan might be bullied if he was to keep his Korean name.
The name sounded too chinese for the locals, that it might alienate Yang Gi-Hwan from his peers.

Aditya decided to give Yang Gi-Hwan a new name: Anthony Matthias.
It was out of convenience, and wasn't a particularly meaningful name either.
A combination of the first two names that came to his thought.

Anthony was happy to be given a new name.
Apparently an English name appealed him.
Anthony was more fluent in English than Aditya's Indonesian, despite being a Korean.
Aditya always thought that Korean tended to be choppy in their English.
Aditya's best guess would be that Anthony's parents were a mixed marriage couple, a marriage of two individuals with differing nationality.

Anthony was sleeping soundly at their bed, as Aditya looked at him.
In their house, they must've taught Anthony to speak in either English or Korean, thought Aditya.
For some reasons, his parents didn't bother to teach him Indonesian.

"Must be from a rich family," Aditya thought to himself, he left Anthony to rest.

\---

"I'm sorry, Sir, we can't let Anthony continue his study here," said the Principal.
There were bruises on her face.

"But, why?"

"It's complicated. He did not comply with our teachings."

"He's a bright kid, you said it in the first few weeks he enrolled here."

"Sir, please, with all due respect, just take your son out of our school,"
the Principal's eyes were holding back tears, she showed Aditya the way out.

"If he's not smart enough, I could help him at home," pleaded Aditya.

"The problem is religious-"

"I'll make him attend churches-"

"JUST BRING THE DAMNED SON OF A DEVIL OUT!"

All remnants of a civilized manner had left the Principal's head, as she started to scream obsessively.
She cried, and the resident teachers barged in to help her.
Aditya was dismissed by force.

When Aditya approached Anthony, he realized why Anthony was expelled.
Anthony was talking with Aurelia.
A number of school workers conversed with audible whispers as they passed Anthony, they didn't seem to realize that Aditya was Anthony's legal guardian.

According to their whispers, that Aditya doubted to be considered whispering, Anthony was having a tantrum at school, when the dolphin ghost came out and wreaked havoc at Anthony's immediate vicinity.
What was more alarming, was that the Principal, which was also a lead Evangelist of the school's church, attempted an exorcism to Anthony.
It was evident, from the bruises on her face, that the exorcism didn't end well.

Aditya sighed, and approached Anthony.
"Tony, we're going home," Aditya smiled.

Anthony smiled back, and raced with enthusiasm toward Aditya.
Aditya held Anthony's small hands, and they walked away from the school premises.
Aditya was sure that the school was avenged properly with the damage Aurelia caused, for performing an exorcism without bothering to inform him as Anthony's legal guardian.

\---

School after school rejected Anthony.
Some because Anthony was feared by his classmates, some because the teachers feared Anthony and his "demonic powers."
Some had the courage to blatantly say that Anthony was the son of Satan.

The rumor spread even to Aditya's immediate neighborhood.
They weren't even shy to speak about it in front of Aditya and Anthony.
Aditya was too tired to deal with them, after countless attempts to try to clear things out, that Anthony was a good kid, if they disregard the dolphin ghost that occasionally followed him around.
But what could Aditya explain about the presence of the dolphin ghost anyway?

Until one day, a bearded man knocked Aditya's door.
He knocked again, and again.
Aditya opened the door, and found a bearded man dressed in gray formal wear.
The wear caused a walkie talkie-like contraption pinned on his left shoulder to stand out.

"I am Arwin," said the bearded man, extending his arm toward Anthony, "Arwin Muerte."

Aditya took the hand, "Aditya Wijaya. May I help you?"

Arwin wasn't alone, a young boy in white shirt with indigo denim outerwear, perhaps fifteen years old, stood beside him.

"This is my son, Dominic Muerte," Arwin introduced, "like your son, he's gifted."

"What?"

Arwin nodded to Dominic.
Dominic advanced toward Aditya, and his hand raised to Aditya's chest.
A pressure could be felt on Aditya's chest, even before Dominic's hand reached him.
He was jolted backward at that instant and hit the floor with a thump.

"What happened?" Aditya asked.

"Dominic has telekinesis, the ability to move objects remotely," said Arwin.

"And how is it my business?"

"I'm here to represent our institution, the Extrasensory perception and Psychokinesis Laboratory, EPL for short. We study esoteric powers and help those that wish to improve their powers."

Aditya stood up, cleaning his back, and exchanged looks with Arwin.

"Our institution also runs a school, if that matters," continued Arwin.

"So your school wouldn't expel kids like Anthony?"

"Our institution is open for anyone, gifted or not. You included, if you like."

"I'm sorry, I graduated already."

"Vocational study is a thing you know.
Or you could join our community.
Any way you like it," Arwin smiled, "at least in our community, you don't need to carry the burden of raising a gifted child on your own."

\---

The school Arwin mentioned was set up like any normal school.
The only difference was the presence of certain extracurricular classes.
Disguised as spiritual and martial art classes, they appeared to be just ordinary extracurricular activities.
Arwin gave Aditya a tour while waiting for Anthony's class to finish.

"The ability to master the environment is what we are cultivating here.
That ability is classified into what we now call the Magic Spectrum.
Three of the most common would be Type Blue, Type Green, and Type Yellow," said Arwin.

Aditya looked into one of the classrooms.
A gymnastic class where the students were practicing.
Nothing weird, except to the fact that they practiced without touching anything.
Objects there were free-floating, while the students concentrated on those objects.

"Type Blue consisted of those with the ability to interact with tangible objects remotely, or colloquially called psychokinetic powers, sometimes telekinesis," said Arwin.

At the other room, Aditya observed a bunch of students flipping through papers supposedly containing the problems for them to solve, and they wrote answers at their worksheets.
However Aditya noticed something, the papers they're flipping through were blank sheets.

"Type Yellow consisted of those with extrasensory perceptions, usually passive skills.
The catch-all name of Type Yellow would be those with the sixth sense.
In this batch," Arwin pointed at the classroom Aditya was inspecting, "are those with the ability to observe the properties of objects not normally seen to the naked eyes.
For this particular session, we projected pictures to those papers before handing those papers to them, and those students were instructed to look at the history of interactions those papers had that contain specific images.
Then they have to write it down for scoring."

"They could do that?"

"About zero point one per mille of all masses on the surface of Earth were the mass of reflexium particles, a part of the pervasive and ubiquitous network that enabled magic-like activities to occur.
Type Yellows, can access sensory informations that the reflexium particles managed to store, as long as the information were still there."

"So," Aditya looked around, "what about Anthony?"

Arwin smiled.
He brought Aditya to a meditation class, where Aditya could observe Anthony sitting in a chair, conversing with Aurelia.
Anthony was the only one in that class other than the instructor.

"A rarity they are," said Arwin, "we don't have many Type Greens.
Most new mages manifest their powers to be Type Yellow or Type Blue."

"What is so special about them?"

"Type Green is a mixture of Type Yellow and Type Blue, but distinctive from the other two.
They are able to interact with intangible objects.
The ability to converse and interact with spirits is one of the manifestations.
Others can alter your perception of reality by interacting with your essence, your soul.
Some can form pacts with powers," explained Arwin.

"So, Aurelia is a spirit?"

"You can say that.
Aurelia was a living being, that then die and her soul continued on.
She was in hell, but Anthony helped her out.
It was a rare ability, even among Type Greens.
Anthony's kind of skill, is often called Necromancy, the art of divination via contact with the death."

Aditya found himself led to yet another room, where all students had the same walkie-talkie-like device pinned to their shoulders, just like what Arwin had.
One of the instructors ordered one of them to lift a vase positioned at a table in the middle of the room.
One stood up, and they unpinned the device, turning it on and waved the antenna toward the vase.
The vase lifted off.

"Some of us, that could not perform any of those abilities, were provided with training wheels," Arwin unpinned the device at his shoulder, "this is a Computerized Organotronic with Neodymium-based Calculation Core, or a Conductor for short."

"That is not even a proper acronym...," said Aditya.

"We're still looking for a better name, so bear with it for now," Arwin's lips were tightened, "the point is, this device allows us to communicate with the reflexium particles like any natural mages.
It helped us to conduct the environment the way we see fit, though limited to what this Conductor can compute."

"So I will also be converted into a mage here, with or without a training wheel?"

"If you like to, I can procure a Conductor for you."

"Meh, being a mage is not my thing.
So what else can I do here, in this," Aditya raised two of his fingers on each hand, "...,community?"

"Be a part of our organization.
We have non-mage branches here as well.
Mastering magic is never our main objective, you know," said Arwin.

Arwin brought Aditya to a building.
It was a featureless, unremarkable building by the edge of the campus complex.
The reception clerks at the lobby appeared to be quite ordinary, not even very attractive.

Past the lobby and the ground floor, however, the building was bustling with activities.
People roamed about, documents literally flew from one section to another.
Arwin greeted and was greeted by a lot of them.

"Welcome to the control center of the Extrasensory perception and Psychokinesis Laboratory, Kendari City Branch," said Arwin, "where we change the world step by step, under the guidance of our patron god, Lord Yam-Nahar."

Aditya however, couldn't focus on any of those sayings.
His mind stopped working when Arwin mentioned Lord Yam-Nahar.
*Lord Yam-Nahar is the answer,* thought Aditya to himself, *he said he'd guide me.*

Arwin continued to speak about the aim of this organization.
An organization that works in the shadow with an aim to destroy the current governments of the world to establish a new world order.
A world of equal chances, free from poverty, free from scarcity of basic needs, no rulers needed.

*Is this your guide? Lord Yam-Nahar?* Thought Aditya.
It felt right, just like what he wanted to achieve.
Anarchism without competitions, with the aim for the benefits of all.
Personal freedom would be highly upheld, with a welfare system that ensures all basic needs of its citizens to be fulfilled.

"When magic was available for anyone to access, labor would no longer be required.
People would no longer need to focus on how to procure materials, on how to process them, on how to deliver them to where it would be needed.
People would then focus on their personal growth, of the limitless path of self-exploration and self-expression.
True freedom, where they would not be bound by struggles just to stay alive," said Arwin.

A liberal-left dream, with a concrete means to solve their problems: equal access to magic, not only restricted by those with the gift to naturally perform it.
Not only that, mingling with a magic organization might be a good start to obtain the esoteric request of Lord Yam-Nahar: to acquire Lord Yam-Nahar's *will*, whatever it means.
*That's it,* thought Aditya, *this is the answer.*
"I'm in," said Aditya.

Arwin's eyebrows were raised, then squinted.
His gaze took some moment to observe Aditya,
"that's it? You're in? I haven't even finished my explanation."

"Do you want me in or not?"

"I like your spirit," Arwin smiled, "of course I'd let you in."

\---

It was year two thousand and thirteen, Anthony was enrolled in a junior high school.
He was assigned to a normal school, to enable him to blend in into normal social circles.
It was when he learned an actual power struggle, the survival of the strongest.

It was the day a boy was harassed by a group of junior high seniors at the toilet.

"You can't run now, Kiel," said the head bully, shoving the fragile thin boy to the corner near an urinal, "now where's my money?"

Kiel didn't respond at all, his gaze was cold but stern, and it was aimed at the head bully.

"Do you think you're strong?" Said one of the accomplices, he gave Kiel a kick to his ribs.

Anthony raised his hand to one of the bullies from their back.
He was about to speak the name of Aurelia, when a warm but firm grasp lowered his arm.
The owner of that grasp was a high school student with a lean body and white hair -it wasn't gray, but white, as white as a plain paper, with a silvery quality.

"Hi, I am Michael Guntur," greeted the boy with white hair to the bullies.

The bullies, despite being about two years older than Anthony, had larger bodies compared to the boy with white hair.
They looked at Michael, scanning every inch of his body, and quickly dismissed him, proceeding to kick Kiel.

"You're Ezekiel Tanputra, a second-grader junior high student, right?"
Continued Michael.

The bullies stopped, they weren't sure what was happening.
"Hey kid, don't you see we're having a business with this sucker here?"
Said the head bully, pulling Kiel's necktie,
"go away and mind your own business."
The entire gang laughed.

Michael's hand, still grasping Anthony's wrist, didn't shake.
Anthony never thought anyone would be this calm, facing three boys bigger than him.
Grades didn't matter when it comes to a fight, a high school student would not be any stronger than a bunch of giant junior high school bullies.
Anthony would expect that Michael's eyes would at least waver at the threat.

That was when Anthony realized that Michael's irises were white.
That was when Anthony realized that there were no traces of reflexium particles in Michael's body.
That was when Anthony realized that Michael was not nobody, definitely wasn't anything he had ever encountered in his entire life.

"You're not doing business here.
You're bullying, and that isn't nice" said Michael again.

The sternness of Michael's tone somewhat caused the gaze of the bullies to quiver.
"G-go mind your own business!" said an accomplice with a kick thrusted toward Michael.

Effortlessly, Michael dodged it, and with his vacant feet, slid the thrusting foot away from the other, stretching the accomplice's feet as far away from each other.
The accomplice screamed in pain due to excessive strain on his abductor muscles.
*He would be having a hard time trying to stand up,* thought Anthony.

He was in fact unable to move.
His abductor muscles were so in pain he couldn't move them without causing more pain.
He cried, and cried, while the head bully cursed him for not being helpful.

"You're not being nice," said Michael.
His gaze didn't move away from the head bully.

The remaining bullies backed away as Michael approached them.
There was something about being extremely calm under threats, that the ones threatening found it frightening.
Michael didn't need to be brawny or act to scare out the bullies, he just needed to be extremely calm, indifferent, and firm in his approach.

Normal people didn't do that.
Michael didn't show fear, and bullies feed off the fear of those they threatened.
With no fear to feed on, the fear emerged in the bullies themselves.

Disregarding the two frozen bullies, and one sobbing bully in a split position at one corner, Michael helped Ezekiel up.
Ezekiel, however, prostrated in front of Michael, "let me be your accomplice!"

Michael smiled, "I'd teach you, so you don't have to be anyone's accomplice."

"Then, let me be your student!" said Ezekiel again.

"Very well."

"L-let me be your student too," said Anthony.

Michael looked back to Anthony.
His gaze was an old soul, full of assurance and wisdom, that Anthony backed away unconsciously.

"Anthony Matthias, we're not supposed to meet yet," said Michael, "let us be in our own ways until the day we meet again."

Michael and Ezekiel left the bathroom.
The two bullies helped their sobbing friend to get up.
They too, left the toilet, all three of them sobbed.
Anthony was left in the toilet, petrified, not sure what was happening.

Weeks after, it appeared that the older brothers of the bullies confronted Michael for hurting their younger brothers.
Rumor had it that they became Michael's students afterwards.
More and more students joined Michael as his students, and they started to acquire members from different schools as well.

Whenever Anthony tried to look for the group, it was like trying to catch eels in a mud pool.
They were slippery and agile, always slipped away whenever they were in one's grasp.
It wasn't long until the new group gained a name, somewhat along the line of a killer whale, as they hunted in packs.
It was an Orca group, or so Anthony heard.

Anthony's attempt to get into the group bore no fruits.
Until one day, about four thousand defectors of the group led by Dominic Muerte came to the EPL.
Dominic Muerte, the son of Arwin, was a spy for the EPL, who managed to join Orca group by chance.

He revealed that Michael's teaching allows non-mages to acquire mage abilities without any aid.
Aditya, then was a newly appointed chief of the EPL, knew that they must acquire that teaching.
EPL must acquire the so-called Orca group.

"What is it like to be in the group?" Asked Anthony to Dominic.

"It was more like joining a *dojo*, we studied a form of martial art," said Dominic.

"Martial art? How does it relate to the acquisition of esoteric abilities?"

"Control over one's meta, it is divided into tiers.
Control over one's body comes first, then followed with metapresence, the ability to project yourself outside your physical body.
After that, would be metaforming, the ability to transform your meta projection to the environment."

"And that metaforming, is how *they* perform magic?"

Dominic scoffed, "*we* don't use the term magic.
It was an inherent part of nature that obeyed one's command only when one is faithful enough to command nature.
Have you read the Bible?
At Matthew chapter seventeen verse twenty."

Anthony shaked his head.

Dominic smiled, "it says that, if you have faith like a grain of mustard seed, you can order a mountain to move, and it will move."

"Just that?"

"The problem is in the faith.
Michael's teaching involves body and mind practices that would help in shaping our faith, in understanding the nature of ourselves, of our meta."

"I don't need to do that to perform magic, I just do it," bragged Anthony.

Dominic scrubbed Anthony's hair in delight, "of course, you're naturally gifted, but not everyone is.
To you, faith comes effortlessly.
To me as well."

Anthony tidied up his messed up hair, and Dominic relaxed at the couch.
His gaze wandered to the ceilings, "but this Michael guy, is *no shit*.
His methods were far more comprehensive, and intuitive compared to our usual study curriculum.
It allowed me to master a more delicate technique with a novel perspective of how to do it.
I could never have studied it here."

Aditya joined the table Anthony and Dominic were sitting on.

"Dominic," said Aditya to the defector, "come and join me, we have to discuss something."

Anthony came to learn that night that despite being very good at the so-called *metaforming*, Dominic was very bad at teaching it.
Among the four thousand defectors that joined Dominic, at various proficiency over the technique known as M.G. Style, none was as good as Dominic in mastery.
The only way to study the teaching, would be to force Michael himself to teach them.

Skirmishes of EPL officers and ex-Orca members were launched toward the Orca group.
It resulted with a series of bombing to various facilities associated with the target group.
One of them was a bombing at Anthony's school, which was also the headquarters of the Orca group.

"Is it really necessary, father?" Asked Anthony, he sat on their bed.

"Yes," said Aditya, slowly positioned himself beside Anthony, "we need the technique, so more people could use magic.
Only when magic is widely spread, could our goals be achieved."

Anthony nodded, but his gaze turned to his fingers, playing with each other.
He remembered Michael, the kind guy behind the entire Orca movement.
"Why couldn't we just ask them nicely?"

"We couldn't," Aditya sighed, "another mage organization was behind that group, which was why Dominic and his mates left the group in the first place."

"What group?"

"Dominic said, it was the *WTF* guys.
He just couldn't remember what it stood for."

### [Incursion](../Chapters/Xe-1%20Incursion.md)

"The first thing we need is something to find funding and to procure infrastructures for our operations," said Henokh.
The marker on Henokh's grasp danced around the board.
Figures, and diagrams it conjured, followed with some scribbles here and there, and some more schematics.

"My family business, the Kuker Group, can only afford so much, but our scale of operation would need to be much bigger," Henokh finished scribbling.

Anderson rose off the seat and inspected the scribbles on the whiteboard, "Securion Incorporated International can help in that. We were planning to expand to Indonesia, and we can invest in the Kuker Group."

Derictor observed the whiteboard, he thought hard, "so, we are going to make the world a better place, by making companies?"

"Exactly," Henokh waved at the whiteboard, his gaze aimed to Derictor, "those companies would be the one that provide us with infrastructures. Our frontends."

Derictor nodded.

Anderson seated himself, he held a hand on his chin, "it would be hard to develop large scale hidden bases, or develop infrastructures in a low-profile manner. It would draw attention to us."

"So," Derictor interrupted, "if the infrastructure is built for the purpose of the companies, it would not attract attentions."

Henokh shook his head, "more like, we don't have to explain ourselves."

"So, what kind of operation are we going to do anyway?" said Derictor.

"Networking," said Anderson.

Henokh nodded.

Derictor's eyes darted at Henokh, and then Anderson, "are we a multilevel marketing group or something?"

"Control over people is vital," said Anderson, "we control people, we control everything."

"Even reality," said Henokh.

An eyebrow jolted up on Derictor's face, "I believe many have done that before. Does it work?"

Derictor gazed at Anderson and Henokh.
He hoped any of them turned to their senses.
He was wrong, not because they did not return to their senses.
He was wrong, because he did not know where his senses should be.

\---

Midday, the sky was clear and bright.
It was too bright, in fact, that Derictor had to squint his eyes just to be able to observe the road.
Just ten minutes prior, he was in his comfortably dim office, at the fifth floor of the mall.

Kang Hae-In, who was sitting in front of him, sipped on his iced strawberry milk tea.
His face was relieved for a brief escape of the hellish weather at that particular day.
A moment after he squinted hard, "*Apayo!*"

Derictor laughed hard, Kang Hae-In was so hurt he exclaimed in Korean.

Pounding the back of his neck with his face squinted, Kang Hae-In looked at Derictor, "Brain freeze!"

"Who told you to drink it that fast," Derictor's grin grew wider before he realized it.

Kang Hae-In massaged the temple of his head, and massaged the back of his neck, "I guess we're getting old."

"Nope, I'm still in my twenties, you're over forties," then Derictor took a big gulp of his vanilla milkshake.
Derictor, was in fact, just one year short from his thirties.

When Derictor had his own moment of brain freeze, Kang Hae-In laughed very hard that everyone around them turned.
Derictor turned his face, he blushed red and want to disappear that instant.
He couldn't just disappear, "S-so, where's this new recruit of yours? It's off by ten minutes already."

"She'd be here anytime soon," Kang Hae-In had to squint his eyes to scan the road, the bright sunlight wasn't very forgiving to them, "oh there she is."

A young woman, with light brown skin, and curled black hair, rushed to the pavilion Derictor and Kang Hae-In were on.
A very decent light gray blazer over her bright black shirt accompanied with her dark brown skirt really suited her, Derictor thought.
The outfit highlighted her natural skin tone even more.
Before he knew it, he was standing, his hand was with her hand, and they exchanged glances.

"Um, I'm Nurhayati," she blushed, but didn't break the eye contact with Derictor, "and you are?"

Kang Hae-In had to slap Derictor's head with a magazine, that Derictor realized they had been shaking hands for about half a minute by then.

Derictor broke the eye contact to gaze at Kang Hae-In, that with his eyes told Derictor to resume their eye contact.
Derictor returned his gaze at her shy gaze, "I'm Derictor, Derictor Wijaya."

Seven years since two thousand and nine he had been with the Intelligence Agency that Henokh and Anderson instituted, never did he think that someone like her would join.
Kang Hae-In did say that she was a member of a drama club and a paramilitary club of a university south of Surabaya, and that he picked her up for her acting and people skill.
Most importantly, her paramilitary training and her mastery over five languages.
A perfect infiltrator, Kang Hae-In said.
Kang Hae-In, however, never mentioned that she was beautiful.

\---

"Tomorrow's mission is quite straightforward," Kang Hae-In started their briefing, "to infiltrate this new movement that the Department of Surveillance managed to discover.
They seemed to be related to the Surabaya Bombing last year.
Our target, is Aditya, Aditya Wijaya."

"Derictor here is a member of the fourth Division, Logistics.
He will assist you in this mission," Kang Hae-In turned to Derictor, and resisted a laugh, "for all purposes and intent, he's your chauffeur." Kang Hae-In's laugh finally bursted.

It was Derictor's turn to slap Kang Hae-In's face with the wrinkled magazine Kang Hae-In used to slap his head before.
Nervous with Nurhayati's response, Derictor caught her gaze, their brown eyes meet each other.
Barely a second passed, they broke their eye contacts, both were blushed.

Kang Hae-In scrubbed his forehead, red because of the magazine, "Anyway, I spoke with Djun," his other hand pointed at Derictor, "his boss.
Among all of us, only Derictor and Henokh had actually meet Aditya, I need him to be able to identify Aditya, in case he attended tomorrow's gathering.
Henokh must not join, he's in the Department of Coordination, and he shouldn't be doing field works like this."

"Isn't there...," Nurhayati was reluctant to continue.

"Yes? What is it? Just say it, I don't bite, HAHAHAHA" Kang Hae-In let out his laughter as if it was the last breath he had.

"I mean," Nurhayati collected every ounce of courage she had, "isn't there any photographs of him?" continued Nurhayati.

"We had his photographs, in our phones," said Derictor, "which, ever since our last contact with Aditya, through a series of misfortunes and accidents, we lost them all."

"Derictor's phone fell to a lake, Henokh's fell from his apartment's terrace, and the photo album that contained their pictures got accidentally thrown off by Henokh's maid," spurted Kang Hae-In, followed with another laughter.

"You..., don't have to explain in details," Derictor's face turned red.

Kang Hae-In's expression turned serious in an instant, "oh yes, other than you two, you'd be assisted with our senior infiltration officer," his smile grew as he faced Derictor, "Anderson."

It had been years since Derictor last met Anderson, the guy with crocodile eyes.
Anderson did not wish to be in the administrative duty at the Department of Coordination, where Henokh was stationed.
Instead, Anderson was gladly accepted by the Department of Field Operation, and was assigned under Kang Hae-In, the head of the first Division, Frontline Responder.

Never did Derictor observe Anderson in action ever since the Intelligence Agency was instituted.
Derictor's first thought when Kang Hae-In said that Anderson was their senior infiltration officer, was that Anderson's peculiar features would be very hard to hide.
How would you hide his peculiar pale skin tone with hints of dark dermal layer, silvery white hair, pointed ears, a very big and muscular posture, not to mention his crocodile eyes?

"A-a senior infiltration officer?" said Nurhayati, her gaze quivered.

Nurhayati being shy, was considered cute by Derictor.
Her demeanor, her gestures, all screamed for Derictor's attention.
Or it could be that Derictor's attention screamed for Nurhayati's attention.
Derictor could not distinguish them at the time.

"Don't worry, he is literally a very kind guy. Ask him anything if you ever need any help," said Kang Hae-In, then something piqued his attention, "ah, there he is," Kang Hae-In waved to an approaching young Chinese-Indonesian man, "Anderson."

A bang could be heard, everyone at that premise jolted their gaze to the source of the noise.

"WHAT?" Derictor found himself standing with his chair lying on the floor, his hands ached as they slammed the table.
He found himself gazed at the young Chinese-Indonesian man, and him being gazed upon by people around them.

\---

Anderson stepped out of the car, scanned around the premise.
It was not an impressive convenience store, almost drowned by the food court in front of it.
There were not much people around the store that particular morning.

Derictor came out from the driver's seat, and from the front passenger seat, Nurhayati looked around.
Derictor opened the door for Nurhayati, at the same time Nurhayati opened the door for herself.
Both were startled, as Derictor almost lost his balance because of that, and their gaze caught each other's gaze again.
Blushed, they broke the eye contact again.

"You, can come out now," said Derictor, he attempted to look at Nurhayati, but his gaze wouldn't aim for her.

Nurhayati came out of the car, her face turned in a hope that Derictor wouldn't caught her blushing, "is this the right place?"

Derictor wanted to answer, but his voice was stuck at his tongue.
Anderson answered instead, "yes, we just have to go to the third floor."
Anderson scanned the entrance of the convenience store.

"Look," said Anderson, "there's a non-functional escalator right at the left corner, beside the cashier."

Anderson walked straight to the non-functional escalator, along with a number of visitors.
Nurhayati and Derictor tailed Anderson.
Derictor couldn't believe it at the time, that the guy they were tailing, was Anderson.
The huge pale and white-haired crocodile-eyed guy, was in the form of a normal young Chinese-Indonesian boy, looked very lean and fragile.
Derictor was certain that Anderson was taller than him when he last met Anderson.

"Anderson?" asked Derictor.

"Yes?"

"Anderson Pondalissido?"

"Yes, why would you ask?"

Derictor didn't answer that, he just look at Anderson.
Anderson looked back at Derictor, "Oh, this body? Yes I can change my body physiology and control the growth and regression. It took me days, but I can transform to this form, to any human form, and my original form."

"How?"

"I told you, I'm an *Etoan superior*," Anderson returned his attention to the staircases.

Nurhayati didn't say anything, but her attention was drawn to a name listed at a brochure the greeter boys provided.
Derictor peeked at her, and she was checking her phone, typing something from the brochure.
*Did she find something interesting in the brochure?* thought Derictor.

What came after, surprised Derictor.
She was no longer the same, shy lady.
Full of confidence, and emanating a completely different aura, a lively young adult, with glares on her eyes.
She was in her acting mode.

No wonder Kang Hae-In said she's a perfect infiltrator: she could easily switch between personalities, blend into various social situations.
According to Steven Brown, acting causes a loss of deactivation in the precuneus of our brain, essentially a deliberate process of possession: a substitution of one's identity by the persona they choose to embody. (fn: Brown, S., Cockett, P., & Yuan, Y. (2019). The neuroscience of Romeo and Juliet : an fMRI study of acting. Royal Society Open Science, 6(3), 181908. https://doi.org/10.1098/rsos.181908, p.16.)
Her identity faded, while the persona she assumed surfaced, and people interact with that persona as if it was her.
Any stranger wouldn't be able to differentiate it, and to the ones that know her, it was like interacting with someone else that was inside her.

Derictor's thought process was interrupted by a poke from an old man right beside him.
Derictor gazed to him, his eyes wide open, and the old man started to speak, "don't use your cell now, and focus on the preacher."

Ernie Windiana, her birth name was Ernest Winston.
There was a certain grace in her gait, a sophisticated woman indeed.
The change of name was done because she felt uncomfortable with her name, as if it wasn't her at all.
Derictor thought, *of course she'd change her name, why would anyone want to name their female baby as Ernest Winston?*
It took Derictor a certain moment until he realized that she had an adam's apple.

"Oh," Derictor's voice came out before he realized it.

"Sshh!" shushed the old man beside him.

Derictor couldn't focus to whatever she was saying, ever since he realized the fact that her gender identity is the opposite of her biological gender.
Gender Identity Disorder is described as a persistent identification of the opposite sex by an individual, followed with a persistent discomfort of their own sex or sense of inappropriateness of the gender role of their sex.
The label was renamed to Gender Dysphoria past two thousand and thirteen, to remove the stigma associated with the term "disorder," and its diagnosis was removed from the sexual disorders category to a category of its own in the Diagnostic and Statistical Manual of Mental Disorders, the fifth edition (DSM-5).
Since it was no longer a disorder according to the DSM-5, the treatment involves supporting said individual into transitioning of their gender identity.
That was in an ideal case, which was not the case in a highly conservative and religious society that belong to a certain abrahamic religion group.

Derictor didn't know any of those, he was naturally surprised that he thought about all of those.
It was a short moment after that he realized, that those trains of thought came from her speeches, that he unknowingly followed through.
Derictor shook his head, and started to focus up on her preach.
It was mainly about "humanizing humans," as some people unknowingly "dehumanize humans."
It was about how, as people blinded by dubious, irrational standards, they focus only on the outliers, and marginalized minorities.
That being in a minority group, seemingly gave the majority a right to scold them.
It wasn't right, but it was the rule of majority.
In the end, it causes unnecessary pain.
It nullified the right of minorities to be themselves, and to live like the rest of the society, with respect and dignity.

Derictor's attention was caught by a name she mentioned, of how Aditya Wijaya approached her, "he told me, that I am worthy of love, and be loved. That I am as human as anyone else."

By Aditya Wijaya's mention, everyone gave their praises.
Their saviour, their role model, their leader, and many more honorifics they gave to the man known as Aditya Wijaya.
Derictor had his attention fully focussed, to obtain any relevant information about Aditya.
However one young boy approached her, whispered something.
She calmed down the mass, and wrapped up her preach.

\---

"So you managed to approach him, uh, her?" asked Derictor.

"Y-yes, yes I did," said Nurhayati, taking a sip to her iced green tea.
She was back to her shy self.

Derictor stirred his Vanilla milkshake, "and what did you learn about Aditya's whereabouts?"

"Apparently she's a close friend of Aditya.
So, she did fall for Aditya, but Aditya rejected."
Nurhayati sipped her iced green tea again, she took a breath,
"Aditya had to take care of his foster son,"
Nurhayati caught Derictor's eyes.
Blushed, she darted her gaze somewhere else,
"Anthony, t-that young boy that whispered to Ernie."

Derictor observed Nurhayati wiping her lips with a piece of tissue.
Her lipstick was wiped off the lips area, advancing slightly to her left cheek,
"So, if we approached Ernie, he'll, um, she'll lead us closer to Aditya?"

"Y-yes, I can, she asked me to join her at a bar."

"I'm sorry but," Derictor took a clean piece of napkin and wiped her runny lipstick.
Nurhayati was petrified, unable to decide how to react.
She decided to just enjoy the process.

There was a moment between them, as their gaze caught each other.
The distance between them was closing, and someone's coughs could be heard.

"Uh, Anderson," Derictor was taken aback, he took his straw off his glass for no reasons.
Unable to decide what to do with the straw, he wiped the straw clean with a napkin on his hand, a napkin that was on Nurhayati's lips.
Nurhayati took another sip of her iced green tea.

Anderson giggled, "you humans are funny."

"We were exchanging information about Aditya," said Derictor.
Nurhayati nodded while continuing her sip.

"I got this," Anderson produced a brochure off his pocket.

"Legion of Umbra Cahaya, a support group," said Derictor, reading the insignia by the top left corner of the brochure.

"It was like that they spread at the beginning of the mass, look at the names," said Nurhayati, switching to her confident persona, and pointed at the bottom right corner, "Aditya Wijaya, and Anthony Matthias."

"This could get us closer to Aditya," said Derictor.

"Not 'us,' Nurhayati and I would approach them.
We need to be careful, and you're not used to this kind of job," said Anderson, "you're too visible.
Mistakenly refer to her as he that often, would annoy her.
At best they'd hate you, at worst, you'd be in their radar."

Derictor looked down at his Vanilla milkshake, and a wiped-clean straw in his grasp.
He put the straw back to the Vanilla milkshake.

"We need to disperse, you understand that.
So you could assume other roles later without being associated with us.
In fact, we shouldn't be sitting together here."

"But I'm your chauffeur," said Derictor.

"Now, yes.
Not necessarily later."

"But I am required to identify Aditya," said Derictor, he looked at Nurhayati.
Nurhayati was about to say something, but she reclined.

"Actually, I thought that too, but Nurhayati is surprisingly good, that we can establish which one is Aditya, and Anthony is one way to get to him," said Anderson, he smiled, "interesting that Nurhayati's success means your service is no longer required."

"But," Derictor attempted to formulate more reasons for him to join this mission.
He wanted to do it with Nurhayati.

Anderson seemed to recognize Derictor's intent, "besides, I need you to be somewhere else."

"Where?"

"Henokh.
You have to deal with Henokh.
The last thing we want to have is Henokh doing something stupid."
Anderson's gaze turned stern.
He did not waver, and the authority of the tone implied that the order was imperative.
For some reasons Derictor couldn't say anything back.
Derictor just nodded.

\---

"There is this growing movement. Observe the names of their speakers," said Derictor.

Henokh took the brochure from Derictor's hand, "join the Legion of Umbra Cahaya, where we would lead you to the world of equality, and peace, and freedom...," Henokh didn't continue it.
His gaze affixed at the name of the speaker, *Aditya Wijaya and Anthony Matthias.*
He turned to Derictor.

"I've tracked Anthony Matthias. Almost no record, but we got an acquaintance of him," said Derictor.

"Who else knows about this?"

"Well, I learned this when doing a mission Kang Hae-In assigned me to, with Anderson, and a new recruit Nurhayati," said Derictor.

"Let's meet this acquaintance of Anthony Matthias, together."

"Do you want me to inform Anderson? Anderson and Nurhayati will engage with our contact tomorrow, perhaps we can convince Anderson to let us join."

"No," Henokh said, he thought hard, "not yet.
Let's meet this acquaintance first.
It is not like it would jeopardize the mission."

"Are you sure?"

"We just want to meet an old friend," Henokh said, he nodded, "yeah, just to meet an old friend."

Derictor knew that this was exactly why Anderson told him to deal with Henokh.
Henokh can be irrational sometimes, especially when it is related to Aditya.
However, Derictor let it pass, as he wanted to do the mission.
He could not determine, that it was because he wanted to meet an old friend of him, or because he wanted to be with Nurhayati.

"Let's do this then," said Derictor.

\---

Dominic Muerte paced his steps on the corridor.
Security guards at the entrance reported a man in black tuxedo and a black high hat barged in, with some kind of supernatural power that they described as Kanuragan Style.

"What do you mean by Kanuragan Style?"
Dominic inquired through his intercom, increasing his pace, that he was almost running.
From the background he could hear the guards screamed, and some sort of thumping voice.
Cracks could be heard, that Dominic assumed to be broken bones.

In between his breaths, the surviving guard explained, "You know, that traditional martial art, that uses inner-energy to fight," he panted, "t-that guy, we barely able to touch him.
Everyone coming near him, they're just...."

"Just what?"

"Expelled- oh no."

Dominic was sure that he heard the guard cried, " please, please, please, I beg you, have mercy-"

The next thing Dominic heard was static noise.

Dominic stopped his pace.
An intense feeling could be felt straight through his spine.
He recognized that sensation.
A fellow ORCA.

ORCA stood for Organized Regiment of Cooperative Associates, a gang of some sort, with several thousand strong memberships.
Led by a mysterious young man named Michael Guntur or M.G., that taught his members a unique martial art, known as M.G. Style.
He knew it, as he was a member.

Tier one of the style was known as the mastery of one's body, basically just like any normal martial art.
Tier two, *metapresence*, was the ability to sense and manipulate the environment with your soul.
Tier three, *metaforming*, was the ability to transform one's soul and project it to the environment.

The way the guards described that man's power was consistent with a Tier three fighting style.
Those guards had no chance of fighting him at all.
He tightened his blue denim jacket, and loosen up his muscles.
He prepared his stance, hoping that his M.G. Style wasn't rusty already, and that the techniques he learned from the EPL, would give him an advantage.

The man in tux slammed the door open, without even touching it.
Dominic recognized the man in tux, his junior in ORCA, "Kiel?"

Ezekiel Tanputra was a direct apprentice to M.G., and was known to be very proficient, despite being five years younger than Dominic.
Dominic was sure he could win the battle against Ezekiel.

"Brother, you're here," greeted Ezekiel.

"How's M.G.?" replied Dominic.

"Good, he's expecting you to be back with us."

"Huh, is he? Tell him to suck up the *WTF* guys first."

The WTF stood for the Watchtower Foundation, an esoteric multi-species government-like organization.
Its function was mainly to regulate the conducts of its members.
Its members consisted of beings that were considered capable to bear the so called "true person-hood."
One of the requirements to bear true person-hood according to them was the ability to command the environment, instead of being enslaved by it.

Humans not capable of commanding the environment according to their will, or basically the remaining ninety nine percent of humanity, were considered feral to the WTF, and must be treated like any wild animals due to the lack of true person-hood.
EPL, in the other hand, saw that all beings capable of free will, must be treated equally.
Because of that, EPL was considered to be an organization that interferes with the welfare of feral humans, by introducing unnecessary unnatural intervention to the life of feral humans.

To the best of Ezekiel's knowledge, the bad guys would be the EPL.
The rule of the WTF was crystal clear for him, feral humans were to be left in their natural state, unless by their own free will and capability, able to transcend the limitations.
Triggered by Dominic's words, Ezekiel's eyebrows twitched, "I gave you a chance, leave those EPL guys, and join back to us."

"You know I wouldn't want to be back to the WTF infested ORCA.
ORCA wasn't the same anymore since those fuckers interfered."

With a move almost like a dance that ends with a firm stance, a firebolt jolted off Ezekiel's palm.
Dominic blocked it with a rotating kick that dissipated the fire before it even touched his body.
Dominic was surprised, he didn't expect a fire to come out, it wasn't in the M.G. Style curriculum.

"Watch your mouth," threatened Ezekiel, "your hate to the WTF doesn't warrant you the right to hurt our fellow ORCAs."

"Oh, the bombing?
It was just a start.
A price for sucking up the WTF guys," Dominic strengthened his stance, and launched a fist.

From his fist, a green beam screamed at Ezekiel, who raised a defensive stance.
The beam dispersed around, and cracked the concrete.
That move of Ezekiel was an authentic M.G. Style, Dominic reckoned.

"Surprised? I learned a few tricks from the EPL as well," said Dominic, his grin grew wide.

"I don't think there's any reason to bring you back to us.
Termination from this world is a more proper punishment for you," said Ezekiel.

Ezekiel charged toward Dominic, and with a firm palm he pierced through Dominic's defense.
Their skin barely touched, but the pressure was real, that Dominic was thrown aback.
A kick to the ground forced Dominic's own center of mass toward Ezekiel, while his palm prepared to pierce Ezekiel's ribs.

Ezekiel shouted as another palm blocked Dominic's.
A strong force was felt among the entirety of Dominic's skin that faced Ezekiel.
Before he knew it, he was midair already, speeding toward the wall behind him.

From his stance, Ezekiel lifted a foot and rooted it hard to the ground, before transferring his entire strength from the ground to his elbow, aimed straight at Dominic.
It was no longer a wind, almost like a surge of water splashed hard toward him and the wall.
The wall cracked and he was thrown hard several meters to the back.

The last thing Ezekiel saw before he fell among the spectators of the indoor concert he penetrated in, was a move, clearly was not a M.G. Style, reckoned Dominic, a move that allowed a sphere of expanding silence to cover the explosion.
Dominic could barely hear his own body thumped hard at the floor.
The pain, the shock, and the bone pushing against the floor through his own flesh and skin, was real indeed, absent the voice.

### [The Silent Concert](../Chapters/Xe-1%20The%20Silent%20Concert.md)

*Any sufficiently advanced technology is indistinguishable from magic. - Arthur C. Clarke*

*Rerum Flexius Lamina*, the reality bending layer, or known as the RFL Network, was an ever present and permeating network on the surface of the Earth.
Set up by Lord El when he first came to the Earth twenty thousand years ago, it formed the basis of all magic-like activities on Earth.
It wasn't magic though, it was just so sophisticated, so invasive, so elusive, and so ubiquitous that for all purposes and intent, it was indistinguishable from magic.

It wasn't even supposed to be experienced by any normal humans, in the middle of an ordinary concert.
It didn't matter which concert, even if the one singing at the stage was Ernie.
It only mattered that it was an indoor concert.
It mattered, because something was going to happen at the ceilings.

Derictor and Henokh found themselves pushing against moving bodies.
The moving bodies pushed back, hard.
Loud music, shouts, and chants mixed together into one noise, vibrant in colors.
The spirit of the concert, the beats, all compelled them to jump, dance, and sing along.

They thought it wouldn't be worse than this.
They must be able to push further to the stage, to meet Ernie, an acquaintance of Anthony Matthias.
The farther they tried to get there, the farther they were pushed away.

They did not realize it at first, that they heard nothing.
They did not just ignore the noise of the crowd, the noise was simply nonexistent in an instant.
Nor was there Ernie's voice, which was visibly singing with all of her strength at the stage.

It wasn't just them that found it weird.
People around them were as confused.
They looked at each other, at Derictor and Henokh, and at each other again.
People far away danced still, shouted still.
They heard nothing.

A certain man wearing a denim jacket was thrown close to Derictor and Henokh.
He was dribbling with wounds, sweats, and his denim was torn here and there.
Derictor, Henokh, and the crowd around them, didn't hear anything.
But they saw it, an event that should produce quite a thump.

The denim man whimpered as he rose up.
He blasted lights from his weakened fist to the ceilings.
Derictor and Henokh, and the rest of the adjacent crowds, looked up.
Another man in black tuxedo stood upside down at the ceilings.

The blasts screamed inaudibly toward the man in tux.
He danced away, all of the blasts missed him.
It was his turn, and he turned a stance.
Firmly maintaining his stance, he thrusted a fist.
Firebolts jolted toward the denim man.
He shot firebolts, to the denim man, to their crowd.

The scene turned chaotic.
Their crowd tried to save themselves, but the surrounding crowds resisted.
They screamed hard that their throats were sore, but nothing was heard.
They were in a bubble of silence, while those around them weren't any wiser.

Beaten up, the denim man lied on the floor.
He rose again, much weaker than before.
But his will barely waver, his eyes filled with determination.
In between his wails, he stomped the ground, and the floor shook inaudibly.
Metal beams sprout off the cracked floor.
Those beams chased the man in tux like hungry snakes.

The man in tux spread his arms.
Metal frames of the ceilings strapped the metal beams from the floor.
They were entangled, intertwined, crushed with each other.
More and more frames extended from the ceilings, and they impaled the denim man.
Metal beams from the floor turned against the denim man, all impaled him.

It was dark, and something liquidy ran to the floor.
Derictor and Henokh knew it was blood.
Dribbled from the impaled dead body that was barely recognizable as the denim man, the blood soaked the floor.
It soaked their feet as well, the stench of the fresh blood, and the squeaky sensation of the wetted floor, all were very vivid for them.
It was impossible for them to even consider all of those as mere hallucination.

The man in tux gazed at Derictor, Henokh, and the crowd.
He topped off his hat, giving them a nod.
At that moment, only one voice was heard by them.

"We apologize for the inconveniences."

The dead body of the denim man was lifted off toward the ceilings.
As if hanged by an invisible rope, it followed the man in tux.
He walked away from them.
He did not look back.

The metal beams from the ceilings returned to its proper position.
The metal bars from the floor returned to its proper position.
The blood withdrew and floated up, following the dead body.
The concrete floor sealed back, its cracks dissipated.
As if nothing has ever happened to them.

After that, the noise of the chants, shouts, and loud music returned.
Even the atmosphere had gone back to normal.
Derictor, Henokh, and their crowd gazed at each other.
No one said anything.
They gazed at the former battle ground.
Only a blank area of the floor in the middle was left, but no trace of a fight.

The only traces left were in their memories.
But no one would believe them.
The surrounding crowds around them heard nothing but the concert.
They saw nothing but the concert.

Derictor and Henokh were walking away from the scene, trying to locate the man in tux, and the body of the denim man.
As they inspected the surroundings, someone pulled Henokh's shoulder.

Henokh turned to face the man that dared to pull his shoulder, "What the hell, man?"

"Yeah, what the hell, man," shouted Anderson, "what the hell are you doing here?"

It was more than enough for Henokh to realize that he messed up.
It was when Derictor regretted his decision to come along with Henokh.
It was the time Nurhayati looked at them from behind Anderson.
It was followed by a wave of Derictor's hand, aimed for Nurhayati.

"You're not supposed to be here", Anderson said as he stood closer to Henokh, while Nurhayati waved back at Derictor.

Henokh didn't answer, he was indeed shouldn't be there.
He realized that his former excuse: to meet an old friend, sounded silly at that moment.

Nurhayati looked around to locate Ernie, and when she spotted Ernie.
She was into her character: a young, delighted, energetic woman.
She left for Ernie, far away from Anderson, Henokh, and Derictor.
Derictor's feet dragged him toward Nurhayati, but Anderson's firm grasp held Derictor's wrist so hard that he almost screamed.

"We're not done here," said Anderson.

\---

"You're kidding me," said Chandra Watthuprasongkh.

That elder man stood unwavering, as firm as a lamp post.
He was filled with rage.
His usual effervescence was gone, replaced with dim boiling anger that reddened his face.

"You're the Chief Executive Officer of the Department of Coordination, F-f," Chandra was about to curse, but he let out a sigh, "-for God's sake."

Having the Chief Executive Officer of the Department of Field Operations (DFO) of the Intelligence Agency shouting at him, was enough for Henokh to finally realize that he messed up.
Normally, it was the Department of Cooperation (DoC) that issued commands for the DFO to execute.
Having the CEO of the DoC, doing field's duty, was never considered an option since the institution of the Intelligence Agency.

That was exactly what Henokh did, and that was why Chandra could raise his voice against Henokh.
Henokh was not doing his duty as the CEO of the DoC, Henokh was acting against the very foundation of the Intelligence Agency, the Statute of Founding, that institutes the separation of duties between departments.

"As a privately funded intelligence agency, the Intelligence Agency needs that structure, for the purpose of compartmentalization.
Each department only concerns their own field of duties, always separate, always detached from each other.
Exchange of information only occurs at the need to know basis.
That way, any other institution would be having a hard time to pinpoint any commonality, any relationship between independent departments of the Intelligence Agency, and to learn about the very existence of the Intelligence Agency," explained Chandra.

As if it wasn't embarrassing enough that Chandra lectured him about the nature of the Intelligence Agency, the institution that Henokh himself was a founding father of, the CEOs of the other departments nodded in agreement to Chandra's speech.
Henokh knew he'd messed up since the beginning, when he decided to visit that concert, he was just not expecting that it could be brought up to the Council of the Chiefs of the Intelligence Agency.

"Alex, tell him what he'd face because of his action," said Chandra to one of the CEOs.

Alex Pontirijaris, the Chief Executive Officer of the Department of Treasury (DoT), stood up.
"According to the statute, shall any of the CEOs of any Departments, because of his compromised judgement, commit an action that is against the statute, he'd be relieved from his duty temporarily via the majority votes of the Chiefs, until it could be determined that he is no longer compromised, or via another majority votes of the chiefs that overrule the previous majority votes."

"Now, how do you plead?" said Chandra.

The other CEOs whispered among each other.
They casted their vote.
It wasn't a pleasant meeting for Henokh.

\---

"You're lucky they just voted for a month's suspension from your duty," said Chandra, sipping his pitch dark coffee.

Henokh didn't reply.
He stirred his cappuccino.
Derictor sipped his vanilla milkshake in silence as well.

"And you, Derictor, I haven't decided what punishment I must give to you," continued Chandra.

"Look *Khun* Chandra, I asked him to join me," said Henokh.

*Khun* preceded a Thai given names in the same manner as "Mr., Ms., Mrs." were in English, unless they carry a higher degree, such as a doctor.

Chandra looked at Henokh, he sighed and rolled his eyes away, "you know that you should not be conducting a field operation. It is the job of my department."

"I," said Henokh, his voice lowered, "I'm sorry."

Chandra heaved, "I'll take over the search for this Anthony guy, for you." his index finger aimed at a space in between Henokh's eyebrows, his eyes stared deep into Henokh's soul, "just promise not to do it that way again," his gaze softened.

Chandra's fatherly gaze gave Henokh the courage to look back at Chandra's eyes, "thank you, *Khun* Chandra."

"And for you, *Rick*," Chandra turned his attention to Derictor.

"Don't do this again."

"Yes *Khun* Chandra," said Derictor.

It was not the worst evening, thought Henokh.
At least they were having a nice and warm chat afterwards.

\---

In the middle of a white space, adorned with billions of black stars, Dominic found himself floating.
He felt no weight, almost like he was in a free fall.
His focus was drawn to one of the black stars he was approaching.
The black star was drawn closer to him, and its size grew in his field of view.

The black stars were a spherical hole each, analog to the usual two dimensional circular hole, but extended into three dimensions.
The closest thing that Dominic thought would be closer to what he was observing was a black hole, but there was no event horizon.
The sight from its behind was all visible, but it got distorted and squished around the edge of the hole.
Inside the hole, instead of a pitch black event horizon found in normal black holes, contained an entire separate world with black skies.

The hole looked like a spherical surface that displays the stars of an entire sky, looked from the inside.
However the spherical surface was torn inside out, and flattened to a circular area.
All the stars at the edge of the hole would get squished and distorted, almost like it was seen from a fish eye's lens.
That was what the black star looked like from Dominic's perspective.

Dominic was drawn closer to the globe, and the delineation of the globe and the white background dotted with black stars started to be his new horizon.
At the surface of the globe, he could see that the upper half of the sky was all white with black stars, and the lower half of the sky below the horizon was pitch black with white stars.
As he moved lower below the surface, he realized that the former white sky was then a globe in the black sky dotted with white stars, the reverse of what he observed before.

"A wormhole?" Dominic asked himself.
If it was a wormhole, the previous white sky was a separate universe to the current universe he entered.
The white globe would be the exit mouth of that wormhole.
He was drawn away from the exit mouth, and toward what appeared to be a normal star.
A planet was orbitting the star, and he fell toward it.

Dominic had heard about the RFL Network's ability to record and store information of everything that has ever happened on Earth.
All of the sufficiently sophisticated thinking beings, ranging from the infraorder Cetacea, the clade Elephantimorpha, the taxonomic families of Hominidae, Ursidae, and Corvidae, had their every single individuals recorded since the day of their conception to the day they cease to function.
The record that would later be named as the Synaptic Observation Upload Link (SOUL) would transcend the death of the specimen.
They would be subjected to rigorous tests before being selected by the Powers of Earth.

"Am I going to hell?"
A cold sensation crawled on the entire length of his spine, and to all of his bones.
In accordance with the Decree of Lord Baal-Hadad, all SOULs, bearing guilt, would be delivered to Hel, where they'd relive their guilt until they could move on.
In short, Hel is a laundry machine for the SOULs, before they could move to the next stages.
It was the last place Dominic wanted to be in.

Dominic heard his own scream as his body dragged lower to the planet surface.
The pain squeezed his entire body, and the pressure was most intense at the temple of his head.
A cold breeze was felt on the surface of his head above his temple, and fingers could be felt probing his exposed head.
He could not imagine what kind of horror was waiting for him to feel.
He heard that the torture at Hel would be the most imaginable kind of pain.

The pressure grew in intensity and the fingers probed deeper, pulling his head toward the breeze.
The pressure moved toward his face, his neck, his shoulder, and the bright light with intense coldness bombarded all of his senses.
He was born again.

Any SOUL that managed to leave Hel, no longer possessing a physical existence, would populate one of the countless Paramundi.
Paramundi, plural for Paramundus, referred to the virtual worlds existing inside the computing substrate of RFL Network.
It was colloquially referred simply as the spirit realms.
Those SOULs would populate the spirit realms along with the Volants, beings native to the spirit realms, where they would live on until either they die again in a spirit realm or migrate to another spirit realms.
Colloquially, the SOULs and the Volants were called simply as spirits.

At that moment, Dominic was relieved that he did not go to Hel.
He was directly transferred to one of the Paramundi as a newborn.
Never did he expect that a virtual world would feel just as real and as tangible as the real, tangible, physical world he used to be in.

"Behold, the birth of Sojobo Kuryama, the successor of the Daitengu of Mount Kurama," said a voice, Dominic's small feathery body was raised high.
The cold breeze was intense, the light was bright enough to hurt his eyes, and his eyes couldn't focus on anything.
A distant crowd cheered, the noise was so loud it was painful for him.
He stretched his beaks as his scream went out of the bottom of his lungs.

\---

Several weeks had passed in the real world, but it was many decades later in Paramundus Jagadlangit, the realm of the Sky.
A large number of tengus gathered in their tree-cities, flocking at the large branches surrounding the Central Tree that also function as the palace of the incumbent Daitengu, their leader.
The Tengus were the kind of spirits well known for their proficiency in battle, and had just recently united under the incumbent Daitengu of Mount Kurama.
However the incumbent Daitengu had his term come to and end and must resign from the office, as the new heir reached the legal age of majority.

The spectating resident Tengus with their red-feathered-faces that featured protruding beaks, cheered upon the upcoming ascension of their new Daitengu.
Their cheerings intensified as Prince Sojobo Kurama flew toward the palace, accompanied with a fleet of Kurama Tengu Forces.
Sojobo landed at the ascension platform, and his wings transformed into hands.
He waved at the spectating crowds, and they were ecstatic to welcome their new leader.

Their non-stop chants intensified as the Imperial Robe was fitted into Sojobo's body, and he gave his first order.
His first executive policy was to expand the domain of the Tengus across all the Paramundi, and ultimately, the real world.
The conquest started with the neighboring worlds, and one of them was Paramundus Jagadpadang, the realm of the Savanna.

A large number of fleets of giant tree ships lifted off, carrying the Kurama Tengu Forces zooming high above the Land of the Rising Sun, up to the space.
The fleet zoomed farther away from their home planet, from their star, and approached the wormhole.
As they entered the wormhole, the black sky was consumed by the white sky, until the black sky behind them appeared only as a black globe.
They moved farther from the other end of the wormhole mouth that was their homeworld, and toward a nearby black star that was the gate to the Paramundus Jagadpadang.

Inside the command seat of the leading giant tree ship, Sojobo wanted to grin, a facial expression he could no longer produce due to the fact that he had beaks instead of lips.
It did not hinder the pleasure, and he giggled instead.
His ambition lay far ahead: after all of the Paramundi fell under his rule, the next realm he would invade would be the real world that was his homeworld.

\---

The clouds shone bright white in the sunless green sky of Paramundus Jagadpadang.
Three creatures wrestled, chased each other, and then lied together on the vast expanse of limitless savanna.
They laughed, and teased one another.

The one that looked like a lion was Barong Ket.
Another looked like a tiger, was Barong Macan.
The last one that looked like a babirusa, was Barong Bangkal.
Every one of them was barongs, the kind of highly respected benevolent spirits.
Barongs can be distinguished with their distinctive horn in their forehead, and beneath it, in between their eyebrows, a hexagonal mirror.
Their eyes were glowing alternating concentric bands of various colors.

It was like any other day in the Domain of the Balidwipa Barong Association, if they weren't chasing some feral critters either for food or for sport.
Occasionally, like any healthy Band of Seeders, they would court or be courted by a Band of Bearers or more.
If they were lucky, the Band of Bearers would be willing to be impregnated, and carry their babies.
At other times, fellow Band of Seeders would occasionally cross their path, and both Bands would perform ritualistic fights and play for some period of time before parting.

It was almost always peaceful, and it had been that way for countless daylight and nighttime cycles.
Until the sky breached open.
Gigantic floating branched tree barks barged in from a spherical hole in the sky.

From the branches, countless numbers of flying creatures dispersed like a swarm of locusts.
Lightnings barraged from every single one of them, casting fire to the infinite plane of Savanna that the Barongs called home.
Ket, Bangkal, and Macan looked at each other, all of their paws grasped Macan's body, and they vaporized into thin air.

In his castle, Barong Airlangga, the Dewata of the Balidwipa Barong Association at the time, held a meeting with the resident barongs.
Ket, Bangkal, and Macan materialized at the castle, and they didn't like what they saw.
Airlangga, a human barong, seemed anxious, his humanoid body meandered in the middle of the court.
He scratched the horn in his forehead, and then wiped his hexagonal mirror in between his eyebrows.

"Don't tell me we're being invaded," he mumbled.

"But we are," said Bangkal.

"I know, don't tell me. I know it already," Airlangga scratched his head.

Airlangga looked up, took a deep breath, and exhaled.
He closed his eyes, and continued his breathing exercise.
He let out a deep sigh.

"Okay, what can we do?" He said.

A countless number of barongs that populated the court murmured with each other.
A moment was spent, and more moments were spent.
No one came out with anything.

Airlangga clenched his fists.
"Have any of you had any fighting experience before?"

"We haven't had battle since *time immemorial*," said an elephant barong.

An orangutan barong stood up, "I remember that back then, we had a number of weapons to fight Rangda. Do we still have those weapons?"

"If there's such weapons, the previous Dewata didn't seem to bother to tell me where," Airlangga sighed.

Ket looked around, and then to his band, Macan and Bangkal.
They nodded, and Ket advanced.
"We are active members of the Samsara Task Force, in Paramundus Integra."

Macan advanced as well, "we fought the Apex Souls there, powerful resident souls with special perks."

It was Bangkal's turn, "and we won the battle in several accounts."

The remaining barongs murmured with each other again.

"That's all? Among all of us, only three of you have recent battle experiences?" Pleaded Airlangga.

A gazelle barong came up, "well, we did play mock battles occasionally, I hope it counts."

\---

Ket led a militia force that consisted of barongs that had experiences in battle, mock battles or otherwise.
They stood at the demarcation between the burned savannah and a newly conjured dense forest.
For the majority of them, it was the first time they realized that trees can grow close to each other, spending the majority of their time in the landscape that was scarcely populated with trees.
Ket gave the sign, and they barged into the forest.

It was a massacre, countless numbers of their people were being pulled up, and dropped from great heights by the tengus.
Those that managed to survive launched their attacks.
Most weren't strong enough to deal damage to the tengus.

Ket howled hard, his mane emanated some sort of invisible shield, holding the incoming tengus back.
Another howl and a number of tengus were thrown aback.
An advancing tengu flew straight toward him, and he slapped the tengu hard with his claws.
The tengu were thrown several meters to a tree that snapped upon impact.

Macan moved like a mist, vaporized at one place and solidified at other places.
He solidified above a tengu, and with his fangs, he crushed its wings.
They both fell, but before they reached the ground, Macan vaporized again.
From the point where he materialized, he could see the tengu that was bitten fell among the crowds.

Bangkal charged at great speed.
No single attack of the tengus can stop him.
He hit the great trees headon, and every single one of them fell right away.
His trail opened a path for the incoming barongs to barge in, swarming the forest floor.

Macan and Ket looked at each other, they knew it wasn't enough.
There were too much of the tengus, capable of moving not only at the forest floors, but through the airspace as well, while most of the barongs were constrained on the forest floors.
At this rate they would fail.
They would be slaughtered.

Ket and Macan solidified near Bangkal, joining him on his charge.

"We will find the leader," said Macan.

"To cut the snake at its head," continued Ket.

"Go, I'd keep on smashing the trees" said Bangkal.

Macan and Ket nodded at each other, and their claws grasped each other.
They vaporized, and solidified at a tree branch.
They analyzed the movements of the tengus, and the propagation of their coordination.
Their formations changed when a certain type of tengus: messenger tengus, came to their vicinity and squeaked.

Ket and Macan dispersed, vaporized and solidified between tree branches.
They followed the messenger tengus, to a giant tree deep in the forest.
Atop of it, was a giant hole carved in.
Numerous messenger tengus flew to and from the hole at a constant rate.

"That's it, it must be their command center," said Ket, "can you look at the interior?"

Macan sniffed hard, his eyes wide open.
His sight broadened, and focussed at the hole, and turned around the hole.
The inside of the hole were posts, each occupied with tengus.
A number of tengus, messenger tengus included, flew by from one post to another.
In the middle of the structure, was a suspended post that featured a throne.

"I think I saw the leader," said Macan.

"Can you take us there?" Requested Ket, "it was too far up, beyond my translocation range."

Macan patted Ket's back, and they both were vaporized.
They materialized in front of the throne, startling the leader.

"Interesting indeed," said the leader after he collected his senses, "welcome to my lair.
I am Sojobo Kuryama.
Submit to me, and you might be spared."

His red face, adorned with black beaks, was tilted up.
Ket and Macan were convinced that he was smiling.
His right wing deformed into a hand, a human hand covered with the sleeve of his coat.

"We want you to cease your invasion, and return to your world," said Ket.

Sojobo's eyes didn't look happy, he drew his longsword and held it with both of his hands.
"Then I don't see any reason to treat you nicely," he advanced and almost penetrated Ket's body.

Ket let out a high pitched howl and a rumbling growl, his mane was raised.
The longsword vibrated and repelled away from Ket's mane.
The tone of Ket's howl undulated, and an invisible force pushed Sojobo back.
Sojobo resisted, with his longsword in a defensive stance.

"Macan, bring Bangkal here!" Shouted Ket.

Macan nodded, and he vaporized in a puff of air.
A moment after, he materialized with a giant babirusa who was Bangkal, whose eyes glared with anger.
Bangkal charged toward Sojobo, who blocked Bangkal's tusks with his longsword.

Bangkal jumped and landed at a great force that the suspension of the platform snapped.
They were in free fall, and a number of tengus at the platform flew away.
Sojobo was about to fly too, but Macan and Ket held his wrists with their fangs, preventing it from reshaping into wings.

Moments before the platform crashed, Macan translocated himself with Ket and Bangkal away.
The platform crashed hard, shattered into unrecognizable pieces.
Sojobo rose from the rubbles, his robes were torn here and there.

Sojobo raised his hands, and a number of tengus surrounded them, with their swords drawn, and aimed at the three of them.
Sojobo charged toward them, and the tengus around them slashed their swords in circles.
Space around the tengus disintegrated by their swords and wobbled out of existence.

They were in a bright white space, with countless black stars surrounding them.
They were freefalling.
Translocation did not work here, no matter how hard Macan and Ket tried.

"We're in the *Antarabhava*, the space between Paramundi," said Macan, observing his surroundings, "those black stars, each of them are individual Paramundus."

"Where are you taking us?" Inquired Ket.

"To a place far away, that you would not be able to return," said Sojobo.

Sojobo flew closer to Ket, and thrusted his longsword.
Ket parried the sword with his claws.
As the sword halted, in a rapid set of moves, Ket pinched the blunt side of the blade in between his fingers and his palm.
Ket recognized the reflex patterns Sojobo exhibited.
A pattern he knew so well from his experience in Paramundus Integra, the world that was populated mainly by humans.
He then disarmed Sojobo with little effort.

"You were a human," remarked Ket.

"Impressive indeed," Sojobo's eyes glowed in satisfaction, "I didn't know any barong alive today would recognize a human."

With the blade of the sword gripped with both of his hands, Ket swung the sword that the crossguard hit Sojobo's head, a half-swording technique known as Mordhau, or murder-stroke.
Sojobo were knocked off, flung toward one of the tengus that surround them.
The rest of the tengus slashed a downstroke, then a wormhole appeared at some distance beneath them.

On the other end of the wormhole, was a space filled with humans.
Ket recognized it to be a ballroom of an unidentified building.
It was apparent that a performance was held there.
Ket recognized the atmosphere of the world of men.
It felt awfully similar to Paramundus Integra.

"Integra?" Said Ket, he looked at the wormhole, and then at Macan.

"No," said Macan, his eyes scanned the other end of the wormhole.
Macan knew it was something similar to Paramundus Integra, but it wasn't integra.
Other than Paramundus Integra, there was at least one more realm he knew that was also populated mainly by humans.

Macan closed his eyes and sniffed, his suspicion was confirmed.
It wasn't the feel of any Paramundus he knew.
It wasn't a Paramundus at all.
"It is the *Manent realm,* the realm of physical reality."

"You never ceased to impress me, barong," said Sojobo, wiping the blood off his head.

### [The ORCA-strated Event](../Chapters/Xe-1%20The%20ORCA-strated%20Event.md)

A two meter tall man with pale skin and silvery-white hair, spoke with Nurhayati Maulidia, codenamed Klandestin.
His white crocodile-like eyes was no longer a surprise for her.
Anderson Pondalissido, codenamed Enam, was known well to be the only shapeshifter in the Intelligence Agency.

Another well-dressed man joined the conversation.
He was Nicolas Armaniputra, codenamed Jokowi, the only medical doctor in their team.
Nurhayati caught herself gazing at Nicolas's sky blue hem, that gave out the outline of his athletic body.

Anderson poked at Nurhayati, he giggled.
Nurhayati launched a fist to Anderson.
She denied that she was peeking at Nicolas, despite her wandering gaze landed at Nicolas again.
Nicolas covered his blushing face, attempting to appear comfortable.

"Flirting with Doctor Nicolas again, huh, what about Derictor?" Said another tall man in black shirt.

Nurhayati's gaze landed at him.
He was huge, at one hundred and eighty five centimeters in height.
His black shirt emphasized his fair skin.
His smile alone was enough to make Nurhayati froze.

"Andre, Andre Tjahaya Purnama," said the man, offering a handshake, "my codename is Empat."

Nurhayati took his hand and shook it for a full minute.

"You can let go of his hand now," said Anderson, his evil smile extended from ear to ear.

Nicolas was fascinated at Anderson's face, noticing that his crocodile-like eyeslits subtly opened in his smile.
He was about to ask more on Anderson's physical features, when another man with lean body frame wearing white shirt caught his attention.

"Bright Spears, codename is Arjuna" he said, offering a handshake to all of them, repeating his name on every handshake.

Another man with lean body wearing navy blue hem and brown cotton pants entered the room.
Everyone knew him as Derictor, codenamed Delapan, a good friend of Henokh from the DoC.
He fixed his round glasses, "alright, *Khun* Chandra and Kang Hae-In are going to arrive soon, please be seated."

"Okay, apparently all six of you are here.
Splendid!
Let's begin the briefings then," said Kang Hae-In.

"Okay, listen up," said Chandra, " Mobile Task Force Psi seventy-two, we will brief you real quick.
A charity concert will be held in two days at the rooftop of one of the highest building in Indonesia.
There, Ernie, our contact, will be performing. The objective of this mission is quite straightforward.
Find Aditya or Anthony, and attempt a capture of either of them."

"For that, I will divide you all into three teams of two," said Kang Hae-In,
"Klandestin and Enam, both from the first Division, will be approaching her again, see if we could gain any new information on the whereabouts of our targets.
Empat and Arjuna, from the third Division, you two will be the one to engage with Aditya and Anthony.
Jokowi from the second Division, and Delapan from the fourth Division, you two would blend into the crowd, see if anything is suspicious, especially on whether or not this operation is compromised."

"Is there any question?" said Chandra.

Bright Spears raised a hand.

"Yes."

"Bright Spears, Codename Arjuna, Sir.
Why do we need to be split, with two teams basically doing nothing, and only us are cleared to engage with Aditya and Anthony?" Asked Bright Spears.

"Dispersion, and contigency," Chandra paused, he approached Bright, "we can't be seen together.
Secondly, we don't wish to compromise the trust we gained from the contact.
That way, if this mission fail, we could still track them through Ernie."

Chandra turned to Kang Hae-In, "didn't you teach them about this?"

"I certainly did.
But that is normal, HAHAHAHA-uh, okay,"
Kang Hae-In's laugh stopped as Chandra gave him a glare,
"see, I told them about dispersion and contigency.
But Bright here, I mean, Arjuna, is a new recruit, certainly he'd need to have a firsthand experience of those concepts in the action to fully grasp it."

Chandra didn't say anything to Kang Hae-In, his gaze turned to the task force,
"is there anything else?
None?
Okay, then I'll excuse myself.
Kang Hae-In, I'll leave them to you."

"Okay, guys," Kang Hae-In took over as Chandra left the room, "this is the first mission of this first ever multidivision task force.
I'm very excited for you guys, the best of the best in your divisions, assembled into a task force!"
Kang Hae-In's face brightened, his smile was wide enough to cover his entire face.

He looked around to see if anyone responded his excitement,
"don't worry, I don't bite, I'm not *Khun* Chandra, HAHAHA!"

Derictor was the first to crack, and the rest started to giggle.
"*Hyungnim* is the best," said Derictor, making a heart with his fingers.
Kang Hae-In's laughter intensified, and the rest of the task force followed him.

*Hyungnim* is a term in Korean, a combination of *Hyung* (older brother) and *-nim*, a respectful way to refer to someone.

"Okay, okay, now keep this good spirit!
Don't be stressed, just do your best,"
Kang Hae-In continued as the task force calmed up.

He handed them with folders, "there you'd find the known physical characteristics of our targets, and of Ernie.
Included within are floor plans of the building, escape routes, and security cameras."

"Okay, that's all.
Study the documents well, and dispose them to the incinerator," Kang Hae-In looked at them,
"I wish you a good luck.
Dismiss!"

"Yes Sir!" said the task force in a chorus.

\---

Anderson was in his young Chinese-Indonesian man persona again, in a formal wear.
Nurhayati helped him tidying up his hair and wiping his sweats at a make up post.

"You looked tired," said Nurhayati.

"Very, I had to shift to this body in just two days.
Turning to smaller body is harder than the other way around," replied Anderson, "I had to lose excess weight and height."

"Oh my God!
How gorgeous!"
Screamed Ernie as she entered the room.

"Oh why thank you," said Anderson, his face brightened in an instand, as he offered a hand for her to shake.

Ernie went to give cheek kisses as a greeting instead.

"Can't wait to taste the main course huh?" said Anderson, he giggled.

"Aw," Ernie's cheeks warmed up, she walked to Nurhayati's side, "Nur, your husband here is very flirty!"

"Oh stop," Nurhayati giggled, "I would've kicked you off if he's my husband!"

"Oh," Ernie's eyes popped open, as if they were going to jump off, their attention jumped to Anderson instead, "so, you're vacant?"

"I am single," Anderson let out a warm smile.

Ernie shrieked and jumped in joy with Nurhayati.

Bright in security guard wear walked past the make up room, took a peek inside.
Ernie was flirting with Anderson, they were having a good laughs.

"This is Arjuna, the Bunny is engaging with Enam and Klandestin, over" Bright said through the communicator.

"Copy that, Arjuna," said Kang Hae-In,
"check the other rooms too, we haven't spotted Anthony or Aditya yet, over."

Kang Hae-In checked the monitors of the control room,
"Jokowi, Delapan, report!"

"Delapan here, I noticed a polar bear in formal wear by the southeast corner of the room, over."

"Copy that, Delapan, can you check on the-" Kang Hae-In's brain stopped working,
"Uh, Delapan, can you repeat, over?"

"You heard me, Houston, there is a polar bear in formal wear by the southeast corner of the room, over."

They had a shapeshifter in their team, and Kang Hae-In it was more than enough dose of weirdness.
Having a polar bear in formal wear in the middle of their mission was beyond Kang Hae-In's imagination.

"Can anyone get me a visual of a polar bear in formal wear by the southeast corner of the room? Over,"
Kang Hae-In couldn't believe he said that.

"Houston, Empat here, we have a problem," said Andre, "the polar bear is looking at me."

On the video feed, a polar bear in formal wear looked directly at the hidden camera on Andre's chest.
For Kang Hae-In, it was as if the polar bear looked directly at his eyes.
The polar bear set up a stance, and jolted a fist.
There was a firebolt, and the camera turned static, followed with a scream of Andre.

"Contact! Andre was hit!" Said Derictor, "Houston, Delapan and Jokowi requested a permit to engage."

"Delapan and Jokowi, Engage!"
Kang Hae-In took a moment to process the situation.
It was completely unexpected to have a polar bear that could produce firebolts.
They weren't planning for this.

Nicolas and Derictor rushed through the dense crowd of people.
No one seemed to care about Andre, and the polar bear.
The polar bear walked away.

"Jokowi, I'd be following the polar bear, you stay with Empat," said Derictor.

He pushed himself against the crowd, toward the polar bear.
The polar bear effortlessly move in between the crowd.
The crowd somehow avoid the vicinity of the polar bear, while completely unaware of its existence.

*Is it going to happen again?
Another silent concert?*
Thought Derictor to himself.
He checked his auditory senses, all was working fine.
He could hear the crowd.
He must be beyond the silent bubble.

Working his way through, he went to the side of the room, and found a tall man in black cassock, white clerical collar, and a black fedora.
Derictor tried to pass by the side of the man in cassock, but the man blocked him.
He tried through the other side, the man blocked him again.

"Please move away," said Derictor.

"Please don't," said the man.

"You don't understand, there's a polar bear there, and I had to-"

"I know, that is why don't go any closer."

Derictor was stupefied, "y-you know about the polar bear?"

"Yes, I'm with him.
Now please move away, we don't mean any harm."

From the other corner of the room, the polar bear's mouth was opened wide.
His throat was on fire, and the blue fire spread to his furs, burning the formal wear along the way.
The polar bear was covered in blue flames.

"That," said Derictor, "is what you mean by you don't mean any harm?"

"You don't understand, we're protecting you *humans*."

"Are you not a human?"

The man looked at the ceilings, "too late.
Just don't disturb us.
Let us do our job."

"Hey, what are you-"
Derictor was petrified.
A black humanoid shadow stood in front of him, it held Derictor's hands.
Derictor was having goosebumps, as the cold shadow melted and creeped on his entire skin.
He could not move, nor could he speak.
His eyes and his breathing, were the only thing he could control.

Three more shadow beings appeared behind the man in cassock.
Derictor looked at the ceilings, and a weird spherical distortion appeared.
It was white, with black dots, that appeared to be denser at the rim.
A distorted sight of a number of human-bird-like creatures, a lion, a tiger, and a babirusa were enlarged from simple dots.
The creatures started to normalize, the closer they appeared on the distortion.

The time Derictor managed to process what he was observing, they were flying under the ceilings already.
Everyone saw that, and the performers were petrified for a moment.
The lead singer continued his song, albeit shaking.
The background dancers resumed their dance.
They tried to pretend like nothing happened.

"Delapan, report.
Can you confirm the sightings Nicolas reported?" Said Kang Hae-In.
Derictor couldn't say anything.

\---

A tall man in cassock identical to the one Derictor encountered stood beside the flaming polar bear.
He had a flame-patterned tattoo on the right side of his face.
He was looking at the tengus and the barongs fighting on the ceilings, "Xiangyu, how long is it until you can neutralize them?"

The polar bear produced a modulated roar, and a short moment later, a humanoid voiceover could be heard, "faster if you stop talking."

"Harsh. Just make it quick."

The polar bear started its dance, while the man with tattoo jumped near Derictor.

"What is this guy doing here? Hendrik?"

"Oh, Heinrich, he was disturbing me. Why don't you help Xiangyu?"

"Doesn't sound like he want a help."

Ket bite the wing of one of the tengu, but its other wing turned into a fist and hit Ket's nose.
Surprised, Ket got another surprise kick from the tengu's feet.
Ket was thrown hard toward Heinrich.

"I thought that 'angel' body of yours," said Hendrik, making a pair of quotation marks with his hands, "made you stronger? And you still got thrown off when a lion was thrown at you?"

Volants didn't have any body in the physical realm, but RFL Network was kind enough to simulate physical properties to ensure compatibility among realms.
Ket that shouldn't have any weight in real world, was assigned with the weight of a lion in his lion form, and the weight of a man in his human form.
It was realistic enough, that for all practical purposes, it felt like volants actually exist physically.

"Momentum," said Heinrich, setting aside the lion body that slowly turned into the body of an unconscious naked asian man,
"is influenced by a body's velocity times its mass.
The lion, uh, now a man, was thrown to me at a velocity, and I was at rest.
Of course I'd absorb his momentum as well, and acquire velocity, no matter how strong I am."

Heinrich realized that Hendrik had been giggling the entire time, "oh, you're just messing with me."

"C'mon, it wouldn't hurt to smile," said Hendrik.

The naked asian man that was Ket stood up, "I'm sorry, the tengu is unforgiving.
Oh, I'm Ket, by the way, who are you-oh, you're twins?"

"Focus on your fight first," said Heinrich.

"Right," Ket said, his body transformed into a lion, and he flew toward the tengus.

Xiangyu finished its dance, and the blue flame spread to form a thin layer of protection between the humans and the fighting creatures.

"It is our cue, get into position," said Hendrik.

Heinrich ran toward the other corner, and with a brief dance, he produced rings of fire around his body.
The ring of fire produced fiery tendrils that connect to the fire veil Xiangyu created.
Hendrik waved his hands, and two of his shadows flew through the veil, and blocked any of the tengus from touching the fire veil.

A number of men in tuxedo and fedora hats approached Hendrik.
Derictor recognized the tuxedo, it was just like the man in tux he enconutered at the silent concert.
They prepared a stance so similar to the fighting stance of the man in tux.

Kicks and fists were jolted toward the veil, and invisible forces started to wrap the veil upwards, sealing the fight of the tengus and the barongs from the human spectators.
Fiery tendrils from Heinrich wrapped around the closing veil, tightening it.
The crowd gave thunderous applause to the scene.

"They thought it was a part of the show," said Ezekiel to Hendrik.

"Exactly like what we want them to think.
So we don't have to bother on fixing their memories."

The fire veil wrapped in fiery tendrils, were shrunk by the invisible force fields from the men in tux.
It shrunk even more, to be about the size of the open gate to the *Antarabhava*.
Heinrich pulled the tendrils so the veil moved toward the gate, when a tengu slashed open the veil.

Two, three, four tengus escaped the veil.
Bangkal breached through the veil to chase the tengus.
One of Hendrik's shadow seeped out of the veil, transformed into a huge cloud with tendrils to chase and capture the tengus.

"Containment breach!" Said one of the men in tux.

More and more tengus escaped the veil, and the veil, along with the tendrils, dissipated.
Heinrich looked at Xiangyu, and to Hendrik.
They nodded, and Xiangyu prepared another dance.

Hendrik had his last shadow engulfed him, forming a black uniform.
He jumped to capture one of the tengu.
The tengu pulled his longsword and attempted a stab to Hendrik's body.
It got deflected by the black uniform, that extended black tendrils, spreading on the tengu's body.
The tengu fell, unable to move as the black tendrils tied it tight, while Hendrik jumped to another tengu.

Heinrich had jets of fire from his feet and hands, propelling him toward the escaping tengus.
A fire beam from one of his hand burned the wings of one of the tengu, while the other evaded it.
A black tendril from one of Hendrik's shadow pulled the evading tengu, then Heinrich blasted its head with a jet from his feet.

Some tengus landed near Ezekiel and his men, thrusting its longsword toward Ezekiel's chest.
With a shout, Ezekiel pushed the blade aside, away from his chest.
He rotated midair and landed a kick toward its beaks.
Before the kick reached its beaks, a strong pressure pushed the tengu away from Ezekiel's feet.

His men jumped forward, forming a stance and launched their fists toward the tengu.
It was thrown hard toward the stage, where the frightened dancers and singers were still performing.
It was near the reffrain, and the singer happen to scream as a part of the song, and in horror of the incoming tengu body.

Macan appeared in front of them, and he jumped toward the tengu body.
They disintegrated into dusts afterward, just about the time the scream subsided.
The crowd cheered enthusiastically, they jumped around, screaming the sound deep from their throat.

Gunshots could be heard, as a crowd of tengu approached Ernie, Nurhayati, and Anderson.
They slashed their swords, where Anderson blocked their swords, with a sword made of fabric-like material off his suit.
It was like a dance, but Anderson advance several thrusts and slashing moves toward the tengus, fighting up to four tengus at the same time.

Ernie screamed, and hide behind Nurhayati, as another tengu approached them with a longsword.
Nurhayati jumped toward one of the tengu and disarm its sword with a slap toward its hand, a rotating move, and another slap to its beaks.
The tengu receded while protecting its beak.
Nurhayati took the sword, but it dissipated into dusts as soon as she pulled it.

The sword materialzied back at the tengu's hand, and it was about to slay Nurhayati and Ernie, when Bright shot his electrolaser gun toward the tengu.
It went straight through the tengu and toward Nurhayati, that was electrocuted and fell to the floor.
The tengu looked at Bright, and it ran toward him.

He ran away, when the tengu flew and landed in front of him.
The tengu opened its beak wide, and was about to peck on Bright's face, a flaming bear pushed it down.
The flame spread to the body of the tengu, burning its feathers, as it screamed and turned into dusts.
The flaming bear looked at Bright, then turned away toward the next tengu it targetted.

Nicolas shot several rounds of his railgun, but the bullets just went through the incoming tengus as if they're made of clouds.
They landed landed in the middle of the crowd, that spectated them in excitement.
The tengu was about to slash its longsword toward the spectatign crowd, when Michael Guntur stood in front of one of the tengu.

"*Ik-kor kakta-tol-Kav-ros kalram,* (I invoke the name of Kav)" he shouted, a hand of him raised toward one of the tengu, "*Ik-ros solit-nah!* (Do not bother me!)"

A silent blast could be felt in that room.
The tengus and barongs turned into dusts, vacuumed toward the gate to Antarabhava.
The gate collapsed afterwards.
The flame of the bear was gone, and so were the jet stream of Heinrich, he fell to the ground.
Hendrik's shadows were gone as well, along with his black uniform.

Derictor collapsed to the floor, the pressure that was on his body was gone, he could move again.
He rushed toward Nurhayati, that was tended by Ernie and Anderson.

"She," said Ernie, sobbing, "she saved my life."

Derictor checked Nurhayati's pulse, then her breath.
"Nur," Derictor shook her body, "Nur, are you okay?"

"She'd be fine, she was just electrocuted," said Anderson.

Everyone on the crowd started to notice the men with fedora hats surrounding them, and a polar bear at the stage.
The performers forgot to breathe, they stood still.
Heinrich jumped at the stage, he shouted, "and that's how you rock!"

The crowds cheered in ecstasy, they gave all of their strength to clap and shout at the concert.
Heinrich nodded to the performers, that waved back at the spectators.
He left the stage, and walked to Xiangyu.

"What happened?"

Xiangyu roared, then another roar, but none was made into a voiceover.
With his paws, he touched his muzzle, and tried to speak again.
His roars were no longer translated into English.

Hendrik came toward Heinrich and Xiangyu, "I can not access my meta, my extrasensory was turned off as well."

Ezekiel came with his men, "same here, we can't perform tier two abilities or more."

"Wasn't that M.G.?" Asked one of Ezekiel's men, "I saw him shouting something, in the middle of the crowd."

"M.G.," said Hendrik, "the chief of ORCA?
He was here?"

"I don't know," said Ezekiel.

"Let's think about that later, we need to see the Major at the VIP section," said Heinrich.

They nodded, and walked toward the nearest exit.

"Hey!" Derictor shouted, he ran toward them, and Bright followed.

"Where are you going, Delapan?"
Said Kang Hae-In.

"I have to reach them," Derictor said, panting, "hh-hh-we have to know what this is."

### [The Esoteric Business](../Chapters/Xe-1%20The%20Esoteric%20Business.md)

The room for the *Very Important Person* was a mess, with shattered viewing windows, remnants of sword slashes everywhere.
Security guards laid motionless on the floor, blood sprayed to almost every corner.
Seated at the VIP couch, was the City Major.
An open wound was visible on his stomach, he panted, coughed, and was very weak.

The VIP must had been breached when the tengus managed to escape the fire veil Xiangyu set up.
Hendrik suspected it was because the security forces opened fire for a few tengus that managed to enter that room.
Non-enhanced bullets wouldn't do anything to them, but to piss them off.

The reflexium particles all around the room started to ping again.
The pings were relayed to his reflexius clusters on Hendrik's right brain hemisphere.
The processed stream of information was relayed to his parietal lobe, integrating the stream into his spatial awareness.
Included in the stream, was informations regarding of open wounds of the Major: damages to his major arteries, weakened heartbeats, and the partially clogged airways.

"My extrasensory returned," said Hendrik.

"Romanov! Call him to save the Major," said Heinrich.

Hendrik nodded, "Romanov Dexter, as a part of the Trinity Compact, I ordered you to appear."

A spherical distortion came with ocean blue glow.
Two dolphins emerged from it, along with splashes of water that dried off before it reached the floor.
The two dolphins floated about and swam around as if the room was underwater.

One of the dolphin turned into a humanoid figure.
He looked like Hendrik and Heinrich, but instead had a tattoo on the left side of his face that comprised of circles by the temple, and stripes extended from the largest circular pattern to his forehead and his jawline.

"Manov here," he said, his attention was drawn to the Major, "oh, sure."

He put his palm on the wound, and the blood was withdrawn to the wound, while the wound itself started to seal itself.
"Purpose, can you check on the fallen guards?"

The dolphin chirped, and it swam to the fallen guards.
It poked one them with its nose, and the blood was withdrawn back to his wounds.
It poked more guards, and their blood retreated back to their respective bodies.
Except for some bodies, that somehow didn't respond to Purpose's touch.

"Three are no longer alive," a voiceover was heard, superimposed on Purpose's chirp.

"What happened," said the Major as he regained consciousness.

"We mean no harm," Heinrich approached the Major, "there was an attack from the Paramud-"

"-Spirit realms," Hendrik cut on Heinrich's words, "yeah, an attack from the spirit realms."

Hendrik produced a contact card.
It was matte black, with an insignia of a letter "W" and letter "M" stacked vertically.
The width was stretched out, and the Major wasn't sure what to make of the symbol.

"We're the Watchtower Foundation," said Hendrik.
He almost mentioned the abbreviated name, *WTF*, but he realized it meant something else for the humans.

The keyword "watchtower" was enough for the Major to realize that the insignia somewhat appear like a watchtower beaming lights to the sides.

"An esoteric organization, with one of our aim was to ensure no supranatural phenomenon was experienced by normal humans," continued Hendrik, "consider that we owe you one.
Feel free to contact us with that card, in case your city need anything.
I believe we can arrange something in return to our failure today."

"Okay, those that can be restored had been restored," said Manov, "those three, they're lost forever. Don't forget to inform their family, they died honorably."

Purpose chirped, and the ocean blue spherical gate opened again.

"We have other business to attend, Paramundus Jagatpadang needs our help," said Manov.
Purpose and Manov jumped to the gate, that then collapsed into nothingness.

"We are sorry for the inconveniences," said Heinrich, he raised his fedora hat.

As they paced on the corridor, Heinrich approached Hendrik,
"What is wrong with just telling him that it was an attack from the Paramundi Complex?
Why must you refer to it as the *spirit realms*"

"Do they need to know technical details," said Hendrik, he turned to Heinrich, "about the virtual cosmology of the RFL Network?"

"No."

"Is there any reason for us to explain more than just *an attack from the spirit realms*?"

Heinrich thought for a moment before answering, "No."

"There you have it, the answer," Hendrik turned away, continuing his pace, and stopped, "oh, a group of people is approaching."

Derictor, Bright, and Nicolas panted to the side of the corridor when they saw a group of men.
Two men with black cassocks and fedora hats, and a bunch of people with tuxedos, all with fedora hats.
Along with them was a polar bear.
Derictor recognized one of the men in tux as the same man that impaled the denim man with metal bars from the previous concert.

"I believe, hhh-"" said Derictor, collecting his breaths, "you owe us an explanation."

Four shadows appeared behind Hendrik, and with a wave of his finger, three of them flew toward Derictor, Bright, and Nicolas.
They could not move, as the shadows crawled on their skin.
Against their will, their bodies walked to the side of the corridor, unblocking the path for Ezekiel, the men in tux, Heinrich, Hendrik, and Xiangyu.

As Heinrich walked past Derictor, Heinrich dropped a glance.
A spare WTF contact card was present in Heinrich's pocket.
He pulled the card and slipped it to Derictor's suit.

"Yes, perhaps we ove you an explanation," said Heinrich.

It was a full ten minutes before the shadow dissipated.
Derictor, Bright, and Nicolas collapsed to the ground.

"What was that?" Bright said, he helped Nicolas to stand up.

"I thought having a shapeshifter as our team mate was the weirdest thing that could happen in our entire operations," said Nicolas.

Derictor knew two half-aliens from his university years.
One becomes the CEO of the DoT, while the other opened a clinic with Nicolas and Derictor's cousin.
They looked human enough, that Nicolas never realize that they were half-aliens.

Derictor didn't believe that they were half-aliens either, until he met Anderson, a true alien.
It was an once in a lifetime experience, thought Derictor back then.
Well, it *was*.
Derictor was certain that night, that it wouldn't be an once in a lifetime experience.

\---

At the concert room, people were still talking about the fight.
They were enthusiastic, but was sad to know that all of their phones were not functional during the show.
Plenty agreed that it was the best concert they've ever attended.

Nurhayati didn't care about any of that, she was too weak to think of anything.
It wasn't a pleasant experience to be electrocuted.
She peed and defecate the moment she was electrocuted, as the shot disturbs normal muscle functions.

"Well, at least you don't get a second degree burn on your chest," said Andre, "this will leave a scar."

"Don't mind about the scar, my nephew Doctor Steven will help you with it," said Anderson.

Ernie came to the infirmary, and she went straight to Nurhayati.

"Oh dear, how are you feeling?"

"I am doing fine, sans the embarrassment," said Nurhayati, she let out a smile.

"Don't be embarrassed, you were electrocuted, what else do you expect to happen?
Here, I brought you some juice and oranges, eat them okay?"
Ernie wiped Nurhayati's sweats, and checked her temperature.

"Thank you Ernie, you're the best," said Nurhayati, "is everyone okay?"

"Yes yes, they are.
Scared, yes, but they're doing great!
The dancers were terrified, but they decided to continue the dance, how professional!
I am proud with Arwin, even though his son Dominic died a month ago, he could still sing under pressure!
You wouldn't believe what he said.
He said that he had lost his son, he had nothing else to lose, but not with his voice!
What a gentleman!"

"What about Anthony, was he here?" Asked Anderson.

"Oh Anthony, no he wasn't here.
Tomorrow he had a quiz, so Aditya prohibit him to join the concert.
They're at their apartment on the East Coast.
Anthony is lucky, his father loved him so much he was tutored directly by Aditya!"

Ernie continued to speak.
Nurhayati was amazed by Ernie's endless stream of words she produced.
Anderson nodded at Andre, that typed on his phone.

> Houston, Info from The Bunny.
> Both of our targets live in an apartment on the East Coast.
> The boy is enrolled to a school.
> Tomorrow he's going to have a quiz.

"This is it," said Kang Hae-In, "finally a clue on Aditya and Anthony."

"What about the mysterious group of psychics?" Said Chandra.

"We don't know yet, but Derictor reported that one of them handed us a contact card."

"A what?"

\---

"We're practically defenseless against such attack," said Chandra to other CEOs.

The other CEOs murmured as video feeds from the task force was displayed at the meeting screen.
A polar bear that turned fiery, the tengus, the barongs, the living shadows, and a man that could produce a stream of fire.
As if it wasn't enough, from Nicolas's feed, was shown that the bullets went through the body of the tengu as if it was a mere mist.
Another feed showed when Nurhayati was going to pull one of the tengu's sword off the ground, it turned into dusts, and rematerialized on the tengu's hand again.

"So, what is your suggestion, *Khun* Chandra?" Asked Henokh.

"The group of psychics here appeared to be benevolent," said Chandra, he turned to the footage again, and highlighted the dancing fiery polar bear, "they helped to contain the attack of the spirits, as we can see from the footage."

"So, if we couldn't control the esoteric powers, we should be aligned with one of them," said Henokh.

"Yes, yes that!" Chandra pulled up the matte black contact card, "back then in the UN Peacekeepers, we also employ the help of psychics in some operations, especially if the insurgents used esoteric means of attack.
At least now, we know one of such group."

"So how do we contact them? Is there an address there?"

"Just a writing here, I suppose it is some sort of spell," Chandra turned the card around, and read the printed letters, "*Fiat nuntium*."

The card turned white.
The other CEOs spectated at the card.
About five minutes, and nothing happened.

"So, where are they?" Asked Henokh.

A beam of light appeared, either to or from the sky, in the middle of their room.
The ceilings, along with the roofs, were disintegrated, and two figures landed in the middle of the light beams.
As the beam dissipated, the roofs and the ceilings were restored, as if it was never disintegrated.

"You," said Chandra, "*Nong* Mich."

*Phi* (read: Pee) is a proper way to address anyone who is older, usually translated as older brother or sister.
For those that is younger, *Nong* is used, that literally translated as younger brother or sister.

"*Sawatdee Phi*," replied one of the figures, as he bowed with his hands pressed together in a praying-like manner.
It was a Thai greeting referred to as the *wai*, related to the Indian *namaste* gesture.
*Sawatdee* is derived from the sanskrit *svasti*, meaning well-being, and is generally used by the Thai people in place of hello, greetings, and goodbye.

Beside him, Hendrik wasn't sure about what his partner was doing.
To the best of his knowledge, it was a similar gesture to *sembah* in the royal court of Java.
In Bali, the greeting word *om swastiastu* is spoken during the *sembah.*
It was also rooted from the sanskrit word *svasti,* so he reasoned that it was equivalent to *sawatdee.*
In response, Hendrik followed the gesture and bowed with his hands pressed together toward the spectators, while contemplating whether or not he would also say *sawatdee* or *om swastiastu.*
He did not say a word.

"*Sawatdee*," replied Chandra, he turned to the rest of the CEOs.
"Allow me to introduce Michael Carmichael, my psychic partner from the UN Peacekeepers."

"We are not *psychics*!" Protested Hendrik.
Carmichael patted at Hendrik, he shook his head as Hendrik turned at him.

\---

"Let *Phi* treat *Nong* Mich a cup of coffee," said Chandra, he handed two cups of coffee for Carmichael and Hendrik.

"Aw, *khobkun mak na Phi* (thank you very much Bro)" said Carmichael.

"*Mai pen rai* (it is okay), I haven't seen you for years already," Chandra brought two more cups of coffee for himself and Kang Hae-In.

"Michael *Hyung*, where have you been?" asked Kang Hae-In.

"To a lot of places," Carmichael said, as he sipped the coffee, "I joined this *WTF*-"

"The Watchtower Foundation," corrected Hendrik.

"Yes that. I've missed you guys, didn't realize that *Phi* is the one that summoned me in the end."

"Oh," said Kang Hae-In, he pointed at Hendrik, "I recognized you, you were the one that petrify Derictor!"

"Yes I was, have we ever meet?"

"Well, you meet my squad members," said Kang Hae-In, a delight could be seen on his gaze, "and what they saw, I saw them as well."

"Oh," Hendrik straightened his back, "you're the man behind the cameras."

"We have plenty of time to get to know each other," said Chandra.

Hendrik and Kang Hae-In turned to Chandra.
Chandra turned to Carmichael.
Hendrik turned to Carmichael as well, "what does he mean, master?"

"I agreed with Henokh, that we would incorporate about a hundred strong reflexiors to join his organization," said Carmichael.

Reflexiors refer to individuals with the ability to alter their environment through RFL Network.

"Again? We just assigned a number of reflexiors to the ORCA as well," said Hendrik.

"Different purposes.
To ORCA, we were there to guide them and enforce the rules of the WTF.
To this Intelligence Agency, we were after the same party, EPL."

"But, I thought ORCA is currently in conflict with EPL?"

"ORCA was in a defensive role, they have a completely different agenda.
They are uplifting feral humans to achieve *true personhood.*"

"While our organization aims to ensure peace and order, a direct opposition to the EPL's approach of chaos," said Chandra.

"Wait, I'm still here guys," said Kang Hae-In, "what do you mean by sending, *revel-*, uh, *mages* to our organization?"

"WE ARE NOT M-" Hendrik was about to finish it when Carmichael signed Hendrik to stop.

"What else do you expect?
They'd join our ranks.
First, a new department is instituted, the Department of Esotericism, or DoE."

"Michael *Hyung* will be the chief?" Asked Kang Hae-In.

"No, *Nong* Mich would be the Head of the fifth division, Esoteric Counter-Response, an umbrella division for all combat oriented *psychics*."

"Oh, yes, Hendrik.
You, and your two soul fragments, Manov and Heinrich, will join his task force," Carmichael pointed at Kang Hae-In.

Kang Hae-In looked at Carmichael, and turned to Chandra, "the Mobile Task Force Psi seventy-two?"

Carmichael and Chandra looked at each other.
Both turned to Kang Hae-In, and Chandra nodded.
Kang Hae-In couldn't believe it, he would be a fellow division heads with Michael *Hyung*.
They have plenty of time to catch up.

Hendrik couldn't believe it.
He would be in the same squad that Xiangyu wounded to get rid of the camera, and the three guys that he temporarily petrified for obstructing his business.
It wasn't a particularly nice first impression, and Hendrik had no idea how to face them.
