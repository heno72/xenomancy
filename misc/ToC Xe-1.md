# Table of Contents

1. Two Man, Two Ways.
2. The Esoteric Business.
3. The Boy from The Woods.
4. The Lost Son.
5. The Angel's job.
6. 2021: The Artifact Hunt.
7. The Umbra Cahaya.
8. The Birth of Daniel Ashton.
9. 2021: The Dawn Strike.
10. The Second Coming.
11. Grandpa is my classmate.
12. The Invisible Hero.
13. 2022: The Battle of Kendari (1) - The Archangels.
14. 2022: The Battle of Kendari (2) - The Four Horsemen.
15. 2022: The Battle of Kendari (3) - The Bet.
16. Victory is sweet, reunion is sweeter.
17. CIA: Consolidation of the Intelligence Agency.
18. The Weakest Link of a Chain.
19. Problems of Our Parents.
20. Dehumanizing a lady is never good.
21. My Friend is The Son of My Father's Enemy.
22. 2025: The Fallen Angel.
23. April Fool 2027: The Battle of Medan / The 2027 April Fool Insurgency (1) - The Vengeance.
24. April Fool 2027: The Battle of Medan / The 2027 April Fool Insurgency (2) - The Four Horsemen's Rescue Mission.
25. April Fool 2027: The Battle of Medan / The 2027 April Fool Insurgency (3) - The Esoteric Battle.
26. April Fool 2027: The Battle of Medan / The 2027 April Fool Insurgency (4) - The Handover.
27. The Caged Beast.
28. The Dragon Assisted Escape.
29. Yam's Vacation.
30. The Goddess of the Sea.
