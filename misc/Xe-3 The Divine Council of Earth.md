# Xenomancy, Book 3: The Divine Council of Earth

After Integra incident, El returned to the Divine Council, taking over the Throne, and again, the Second Age of Gods dawned.
El ordered the Heavenly Hosts to descend to Earth, and form perimeters for the preparation against the incoming Denefasan attack.
Everyone on Earth is declared to be under the martial law, and Earth is led entirely by El.

Ashton planned a path with a discussion with Anthony, and Manov, that they need to bargain with Kothar, to obtain Yagrush and Aymur.
Arriving in the Shamayim, they meet Kothar, and revealed the story about the previous Baal Cycle.
Kothar allowed them to wield Yagrush and Aymur, and use them to fight the Divines.

However wielding Yagrush and Aymur is just too much for the human minds.
The group fell under the dreamworld manifestation of both Yagrush and Aymur.
Can they survive the dream and mastered Yagrush and Aymur?

## Notes to consider
1. [Wielding Yagrush and Aymuri](../Notes/L210%20Wielding%20Yagrush%20and%20Aymuri.md)
1. [Roles of the Four Horsemen in Xe-3](../Notes/L215%20The%20Four%20Horsemen%20and%20The%20Six%20Wielders.md)
1. [Oneiromundus in Shamayim as the main stage of Xe-3](../Notes/L223%20Locations%20in%20Oneiromundus.md)

## Alteration L210 Wielding Yagrush and Aymuri
This is for Book 3

When the six of them went into Shamayim and got a hold of Yagrush and Aymuri, they entered the mental landscape of Yagrush and Aymuri. So the fight with intelligent dogs commenced.

The mental landscape might be called the Oneiromundus. In that world, was a wide landscape populated with intelligent dogs and humans. Dogs are the representation of Yagrush, the Chaser. Humans there are the representation of Aymuri, the Driver.

Yagrush is very good at detecting and isolating, like a pack of wolves in a hunt. That's why Yagrush is represented as intelligent wolves.

Aymuri is the driver of changes to the environment, the creative powerhouse that shape the environment. On Earth, humans are one of the major drivers of changes to the natural environment, so in Oneiromundus Aymuri is represented as humans.

The landscape represent the Powers, the Force of Nature. The weather is Hadad, and the ocean is Yam. El resides in the Mount Sinai, right at the center of the territory, and scattered packs of Bulls everywhere else on the land, aside from the Golden Bulls that live in the Mount Sinai. Mot was not present in Shamayim, so under the ocean of the Oneiromundus, one could see that the land has no roots, as the ocean floor was pixelating into nothingness.

I think the story could start a long (subjective) time after the fight with intelligent dogs. It spans many years inside the Oneiromundus, probably many decades to centuries. Where the imaginary world was split into at least five states.

First of all, the names of the six persons in Oneiromundus:

| Original Name | Oneiromundus Names |
| - | - |
| Derictor Wijaya | Prick Petal Proctor |
| Henokh Lisander | Lead Manu Deisono |
| Daniel Lusien | Firm Felix Redstone |
| Fernando Suryantara | Tawan Viraloneiro |
| Hendrik Lie | Kao Ninekao Waikabkwamrak |
| Steven Pontirijaris | Cad Aleirer Cadviri |

The State of Metal Petals, Derictor's domain. An artic region, their main industry is metal refinery and mining. Derictor's experiencing phantom limbs since his stay in Integra, where he had an ability, that his brain interpreted as an additional set of limbs. So when he returned to the real world, he's experiencing phantom limbs. However, the hole is filled when he's inside the Oneiromundus, and under the stress of the initial invasion, his power is reestablished here. He couldn't remember his name, but his power originally gave him the nick Prick, and eventually he rose to power as the leader, Prick the Petal Proctor. People and the wolves are under his rule in a Benevolent Lawful Dictatorship. The metal petals in his domain are his ears, eyes, and his hands. People regarded him as their Patron God, and would refere him and honor him, instituting their own government under his guide, but he'd step down when something went wrong.

The Hypnocratic Hypernation, Henokh's and Daniel's shared domain. An industrialized, urban territory, with main industry of manufacturing of goods and tools. Within every living quarter here is a screen on every house, broadcasting messages from Henokh every day, and sermons from Daniel every night. Henokh (called Lead, short for Leader) is very good at dictating those that could understand words and are awake, on what to do. Daniel (called Firm) is very good at manipulating the conscious state of his subjects. Naturally, it helps if both are leading together. The land of eternal hypnosis, living a conditioned good and happy life while also doing deeds for the kingdoms. The Sun Emperor Lead, leads people when they're awake. The Moon Whisperer Firm, leads people when they're unconscious. They first meet on the early years after the invasion of the intelligent wolves, and they knew right away that they're looking for the same thing. They each has a hole in their heart, longing for someone, but not either one of them. Under their rule, the people are in the Hypnocratic state, perpetually being controlled under them in a surreal landscape that spans not only in their environment, but also in their mindscape.

The Free Land of the Sun, Fernando's domain. A lush tropical land, also known as the Country of A Thousand Temples, because there are plenty of temples where social aids are provided to those in needs. Their main industry are foods, spices, tourism and entertainments. Fernando isn't really a leader here, as there is an established Public Administration for the people and the wolves. But Fernando here (known as Tawan, he didn't remember his name) is a public figure that becomes a role model for everyone, and that is loved by everyone, wolves and humans alike. He has two close companions, Arm the Human, and Off the Wolf. Arm is well known for his broad range of skills and is very capable, especially on survival on the wild, and his good sense of fashion and design. Off is well known for his looks and personality, very good at cooking, and especially because of his peculiar nature (for the intelligent wolves) of being easily frightened, but always has the courage to try out new things. Tawan himself is naturally nice, polite, and love animals. He loves to brag and to show off his knowledge, usually with the growing smile of proudness he couldn't hide. There are minor characters such as Joss the Wolf and Gun the Human. He's treated as some sort of a royalty, with the government resembling that of a constitutional monarchy. He becomes he face of the nation, an idol, with Public Administration as the government.

The Police State of Umbra Legion, Hendrik's domain. An archipelago near the equator, with main industry of fishes and fresh seafood. Under the name of Kao, as he couldn't remember his name. Hendrik's power here, the metagenesis, is used to the maximum extent. A police state, everyone's life is monitored closely by the Shadow Police. Criminals would be removed at the Reeducation Facility, and bad contractans would be forced to restitute. Contracts would have an assigned Shadow Executor, that would guard the execution of the contract, forcing everyone to follow what they've agreed upon. This is the domain where everyone is safe, but is not free.

The Free States of People, Steven's domain, near Mount Sinai. At the border, they exchange goods with other states, with their main exports are preserved meats, and spices. A hunter gatherer society, led by the Werewolf Chief, Steven (known as Cadviri, he couldn't remember his name). They only had a big hunt every once a week, while the rest of the week was used to prepare the food for feasts and social events. Wolves hunts for big meals, while humans hunt fishes and scoured vegetables. They are nomadic in small groups of less than a hundred individuals, finding new ground for hunting, before returning to the Mount Sinai every year.

### Aid from the Four Horsemen
So when they wield Yagrush and Aymuri, and entered Oneiromundus, Adran informed Midnight and Ashton. So they beamed their consciousness to Adran's instance in Shamayim, and entered the Oneiromundus. That's when they discover the weird landscape with its five states: SMP, HH, FLS, PSUL, and FSP.

They first land in the FLS, on which they didn't realize anything weird, except that there are more dogs, and those dogs are also citizens, instead of pets. It took them months to adapt in the state, and during that time they saw on TV: Fernando hosted programs. He is apparently an entertainer that is loved by everyone, people and dogs alike. Hence the quest to reach out Fernando. Here we explored various ways to reach out idols.

The second stage, they're using Tawan's connection for a tour to other countries. First stop is HH, where they're going to find Henokh and Daniel. Loosely based on The Gifted's Supot in term of power, they discover that the society has certain taboo they couldn't talk about or do even though they want it. And they're almost always want to go to sleep when it's time to do so. They discover the peculiarity that there's always a screen in every room, that would show Henokh, providing guidelines on day to day life. When it's night, Daniel would appear, answering phones from fans and citizens, listening to their concerns and giving advices, and people would listen to Daniel's songs to rest. Tawan had a strong feeling to Daniel, until he let out a word, "Hin," the one he's looking for all of this time. During their struggle to go to the Capital, Tawan reacquired his power from Integra. The target here is to obtain an agreement to develop engines and technologies to alter the world. Geoengineering, and nuclear reactors.

The third stage, is to go to SMP to negotiate mining deals required for the engines. This is when they could feel that the world is reacting. Bulls ravaging the civilized lands, and storms becomes more severe. However the ocean seemed to be inviting, as fishes flourishes for citizens to capture, with prolific growth of kelps and algae on the ocean. Ashton knew what is happening, the fight between Powers has started, as Tiamat returned to Yam after El was awakened. Ashton reasoned that the fight is affecting this world. They need to find a way out of this world, and that is by reuniting the six wielders of Yagrush and Aymuri. Alas, Prick thought that the calamity is caused by the engine that HH is building, and stepped into the affairs. Lead and Firm are not equipped to fight Prick, but Tawan is very capable, as both of their powers are in the level of environmental manipulation.

## Alteration L215 - The Four Horsemen and The Six Wielders

So when the six protagonists wield Yagrush and Aymuri, and entered Oneiromundus, Adran informed Midnight and Ashton. So they beamed their consciousness to Adran's instance in Shamayim, and entered the Oneiromundus. That's when they discover the weird landscape with its five states:

1. SMP: The State of Metal Petals, led by Prick Petal Proctor.
2. HH: The Hypnocratic Hypernation, led by Lead Manu Deisono and Firm Felix Redstone.
3. FLS: The Free Land of the Sun, led by Tawan Viraloneiro.
4. PSUL: The Police State of the Umbra Legion, led by Kao Ninekao Waikabkwamrak.
5. FSP: The Free States of People, led by Cad Aleirer Cadviri.

They first land in the FLS, on which they didn't realize anything weird, except that there are more dogs, and those dogs are also citizens, instead of pets. It took them months to adapt in the state, and during that time they saw on TV: Fernando hosted programs. He is apparently an entertainer that is loved by everyone, people and dogs alike. Hence the quest to reach out Fernando. Here we explored various ways to reach out idols.

The second stage, they're using Tawan's connection for a tour to other countries. First stop is HH, where they're going to find Henokh and Daniel. Loosely based on The Gifted's Supot in term of power, they discover that the society has certain taboo they couldn't talk about or do even though they want it. And they're almost always want to go to sleep when it's time to do so. They discover the peculiarity that there's always a screen in every room, that would show Henokh, providing guidelines on day to day life. When it's night, Daniel would appear, answering phones from fans and citizens, listening to their concerns and giving advices, and people would listen to Daniel's songs to rest. Tawan had a strong feeling to Daniel, until he let out a word, "Hin," the one he's looking for all of this time. During their struggle to go to the Capital, Tawan reacquired his power from Integra. The target here is to obtain an agreement to develop engines and technologies to alter the world. Geoengineering, and nuclear reactors.

The third stage, is to go to SMP to negotiate mining deals required for the engines. This is when they could feel that the world is reacting. Bulls ravaging the civilized lands, and storms becomes more severe. However the ocean seemed to be inviting, as fishes flourishes for citizens to capture, with prolific growth of kelps and algae on the ocean. Ashton knew what is happening, the fight between Powers has started, as Tiamat returned to Yam after El was awakened. Ashton reasoned that the fight is affecting this world. They need to find a way out of this world, and that is by reuniting the six wielders of Yagrush and Aymuri. Alas, Prick thought that the calamity is caused by the engine that HH is building, and stepped into the affairs. Lead and Firm are not equipped to fight Prick, but Tawan is very capable, as both of their powers are in the level of environmental manipulation.

The fourth stage, they went to Mount Sinai, and discovered a pack of wolves along with humans that can transform into wolves. The FSP was led by Cad, that at first thought the incoming group was a part of PSUL. PSUL had been more aggressive lately as the nature started to behave.

## Locations in Oneiromundus.

### Five states:
1. SMP: The State of Metal Petals, led by Prick Petal Proctor.
2. HH: The Hypnocratic Hypernation, led by Lead Manu Deisono and Firm Felix Redstone.
3. FLS: The Free Land of the Sun, led by Tawan Viraloneiro.
4. PSUL: The Police State of the Umbra Legion, led by Kao Ninekao Waikabkwamrak.
5. FSP: The Free States of People, led by Cad Aleirer Cadviri.

### Positions
Let's determine the positions. We know that PSUL and FSP tend to be in conflict. PSUL is an archipelago, while FSP is mountainous. SMP is either arctic or antarctic. HH is like the mainland china. FLS might be temperate, but no snow. Higher than the philipines, but still connected to the mainland.

The route we're after is: FLS, HH, SMP, FSP, PSUL. The model we're using is Asia.

I think, FSP and PSUL should be quite close to one another, so perhaps it's around Nepal and China? Or perhaps, FSP is on Australia, and PSUL is the Greater Indonesia region?

Meanwhile HH is mainland China, and is very close to FLS that spans around Thailand, Vietnam, and Myanmar.

Then SMP must be very higher up, toward the north of HH.
