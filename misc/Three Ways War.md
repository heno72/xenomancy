# OUTLINEv2: The Three Ways War
This outline aims to straighten the chronological order of the story, and its plot structure.

I will name this structure as the three ways war structure.
There are the Antichrist Cluster, the Beast Cluster, and the Christ Cluster.
Basically the Antichrist cluster is the polar opposite of the Beast cluster, and they're meant to neutralize one another.
Meanwhile, the Christ Cluster is Kavretojives’s insurance to secure his plans.

## Age of the Godfluenza
It all started by the meeting of Henokh and Aditya, that activates the Antichrist Godfluenza propagation.
The godfluenza grew, and spreads fast when they split.
There must be a way to mitigate it, which is when Anthony is separated from Martha and Michelle via Yam's intervention.
Then Anthony meets Aditya, and the Beast Cluster is activated, at around the same time Anderson entered Henokh's life.
Afterwards, is the Age of the ABC Clusters.

## Age of the ABC Clusters
The Antichrist Cluster is Kavretojives' attempt to neutralize the Antichrist Godfluenza, from infecting more viable harvests.
Its members are anyone working in close quarters with Anderson.
The members include: Anderson, Henokh, Derictor, Heinrich, Romanov, Hendrik.

Its activation sequence is Anderson meets Henokh, persuades him to form a company, with Securion Incorporated as one of the investors.
Henokh took Derictor in, and from him the idea to form BAIK/KIA.
With the formation of BAIK, WTF is drawn in, and BSW, a sub-group of WTF, sends Hendrik, Romanov, and Heinrich to merge with BAIK.

The Beast Cluster is Hadad's attempt to prevent further propagation of the Antichrist Godfluenza, however favoring a more radical attempt to reset Earth's memetosphere.
It even goes as far as cooperating with the Denefasans.
Its members are anyone working in close quarters with Aditya.
The members include: Aditya, Anthony, Keenan, Lucy, Martha.

Its activation sequence is Aditya found Anthony.
Anthony grew as a reflexior, gained his training with the help of EPL, which then helps him to meet Keenan and Lucy.
They introduced Anthony to the Denefasan Ambassador.
Then, at last, as the Denefasan enters the scene, Martha was drawn by the sightings of the Denefasans.

The Christ cluster is Kavretojives' guarantee that the system will be on his favor, working by treating the symptoms or after effect of the Antichrist Godfluenza.
Its members are anyone working in close quarters with Ashton.
The members include: Ashton, Steven, Daniel, Fernando, David, Michelle.

The activation sequence starts with Ashton's re-arrival on Earth, merging with his self on Earth, and gave Steven and Daniel a visit.
Fernando was under the umbrella of The Antichrist Cluster, but through Daniel, Fernando was reassigned as a core member of the Christ Cluster when the Denefasans came.
At the same time, Ashton enters David's school life, and exposed himself to Michelle during Denefasan attack, making her a part of his cluster.

# Timeline
| Date Time     | Scene | Chapter | Notes |
| ------------- | ----- | ------- | ----- |
| 20210406 0150 | - | Angel's Job | Heinrich doing his job as an Angel |
| 20210406 0200 | The Trip | The Lost Son | Cal survived a car accident, only to be found by Christopher Lee. |
| 20210407 0030 | The Signs | The Artifact Hunt | Hendrik on attempting to figure out the next move of LUC. |
| 20210418 0240 | - | The Raid | Aditya's skirmish to obtain The Black's Staff. |
| 20210817 0300 | - | Dawn Strike |
| 20210817 0545 | Steven and Adran | The Disturbances |
| 20210817 0700 | Daniel and Anderson | The Disturbances |
| 20210817 1000 | Henokh and The Operatives | Inspections |
| 20210817 1030 | The Second Coming | Inspections |

# Planned Part Structures
## The Warring Clusters
### Part 1
Before all of these starts, a trigger had to be pulled.
Heinrich, wasn't activated yet, was doing his usual business as an Angel, receiving a command and carried it out.
It coincides with the command carried by a yet another Angel: Anthony.
It resulted in his failure to summon Tiamat.
Anthony picked up the signal, that there's something wrong with the Powers of Earth.
Something of a great force was in motion, and he had no idea how big everything is going to change afterwards.
Thus begins, the age of the Warring Clusters.

**See:**
1. [Angel's Job](../Chapters/Angel's%20Job.md)

### Part 1A
Christopher Lee.

**See:**
1. [The Lost Son](../Chapters/The%20Lost%20Son.md)

### Part 2
The Antichrist Cluster and The Beast Cluster fights with each other a lot.
The first one is on the possession of the Black's Staff, which, at first was with The Antichrist Cluster, but later was acquired by The Beast Cluster.

**See:**
1. [The Artifact Hunt](../Chapters/The%20Artifact%20Hunt.md)
2. [Breach](../Chapters/Breach.md)

### Part 3
**Short summary:** This sparks an attempt by The Antichrist Cluster to track down The Beast Cluster, by infiltrating it and to chop off its head.
At this point, the stage is laid, and the forces at play are going to emerge.

**Long summary:** Interrogating the captured insurgents, they came to learn about the Legion of Umbra Cahaya movement.
Following the first lead, Edward Kevlar, Hendrik came into contact with a transgender, that leads them into a gay bar.
In the gay bar, Hendrik discovered Anthony, Aditya's second in command, that for some reasons knew Manov so dear (he mistaken Hendrik for Manov).
Leading a fight with Anthony, inquiring why he knew about Manov, Hendrik failed to capture Anthony.
Anderson reported that the lead he's after is stopped by a Heavenly Host as well.
But the next clue revealed where the next mass meeting of this LUC might take place.

**See:** [The Serpent](../Chapters/The%20Serpent.md)

### Part 4
The Beast Cluster uses The Black's Staff to summon The Denefasans, and then making a deal, which will allow Denefasans to grow their base on EPL territory, after EPL pulls some strings.
At the same time The Antichrist Cluster tries to disband The Beast Cluster by infiltrating their sphere of influence, which results in a fight.
However they fails and the connection between Manov and Anthony is revealed: the weak link between The Antichrist Cluster and The Beast Cluster.
A connection between the Beast Cluster with the Heavenly Host is discovered.
The Antichrist Cluster failed to eradicate the head of the Beast Cluster.

### Part 5
After the fight, Anderson came up with a radical attempt to annihilate The Beast Cluster: He would stop the vector of Godfluenza propagations, that are Henokh and Aditya.
This is the beginning of the force of nature: Anger.
Anderson planned the bombing of Henokh, that would attract Aditya's attention, and will get them in one place, together.
The plan was to kill both of them.

**See:** [Betrayal](../Chapters/The%20Serpent.md)

### Part 6
The attempt went awry, and The Antichrist Cluster suffers the most by the attempt, while the Beast Cluster continues their agenda.
Wounded, Anderson came to visit Steven from the dormant Christ Cluster.

**See:**
1. Dawn Strike
2. Anderson and Aditya

### Part 7
Anticipating the failure of The Antichrist Cluster, The Christ Cluster is activated early, and when the triggers are pulled by The Antichrist Cluster, The Christ Cluster starts to work.
Ashton starts to activate his agents.
He meets Daniel and Steven, and informing them about his latest memory download, and to warn them about Denefasans.
And to prepare for the next triggers, he enrolled to David's school, becoming a part of his school life.

**See:**
1. [The Birth of Daniel Aston](../Chapters/The%20Birth%20of%20Daniel%20Ashton.md)
2. [The Disturbances](../Chapters/The%20Disturbances.md)

### Part 7A
Ashton enrolled to David's school.

### Part 8
The activation of Kuin, the Omega Cluster.
Kuin was furious for the death of his father, Bain, and decided to design a humanoid body to act as his ambassador in the physical world.
Kuin uses the case as a leverage for the Board of Directors, where he couldn't act fast enough because he wasn't trusted to act on his own behalf.
This is the progression of Anger toward Loss.

### Part 9
During Denefasan Invasion, The Christ Cluster shone the most, fighting off the Denefasans, long enough to help the just been awakened Kuin (due to The Antichrist's cluster failure) to intervene.
This is the activation of the force known as Altruism.
They're now becoming the new threat to The Beast Cluster, and The Beast Cluster tried to pull them down, out of their ways.

### Part 10
As David had been activated when Asthon enrolled, now it is Michelle and Zean that got activated during the attack of Denefasans to their school.
Michelle is given the pole, and Zean is given the art of Cagraligatur.
The team helped the school mates to survive the attack and tackled the Demo Invasion on their school.

### Part 11
The Beast Cluster tried to find a weak link to cripple The Christ Cluster, and they found out two of the weakest links: David and Daniel.
The Beast Cluster aimed at both at the same time, starting with Daniel as a distraction, that allows them to take David for their final, sinister plan.

### Part 12
Steven was forced to slain his own son.
It backfired soon enough, that Steven stabbed Aditya hard onto his eyes in a mess Aditya himself wasn't sure what was happening.
Both took a heavy blow.
Ashton's team came by with Adran and secure Steven from further damaging Aditya.

### Part 13
It was the awakening, and the lowest point of both side, the beginning of a process, known as Love, is rolled into effect.
The Antichrist Cluster trailed behind, only to come too late for them to affect anything.
They clean up as much as they can, but Anthony took Romanov for a favor: to save his step-dad.

### Part 14
In an attempt to recover with each other, The Christ Cluster took this moment to infiltrate deeper into The Beast Cluster.
Especially since Martha had been activated, and so does Michelle.
And now David and Anthony are in love with Michelle.
It is a fight that would either go toward Michelle's failure to remain in the Christ Cluster, which sparks the force of Lust, or Anthony's lenience toward The Christ Cluster, which sparks the force of Friendship.
It was a war that Ashton can't really interfere, the work of the mysterious force known as a Friendship.

### Part 15
This time, friendship grew and wins in between the three of them.
Anthony is now more open to the Antichrist Cluster.
They've established a backdoor deep inside The Beast Cluster.
It coincides with the completion of Steven's and Aditya's recovery.
And at the same time, Fernando's activation is complete.

### Part 16
The Beast Cluster tried to collect everything they've lost during their last blow, and took some time before they could resume their processes.
Now they are exploring their guise as an ordinary movie studio, always stays below the radar, but constantly spreading the Godfluenza.
Then finally they meet their Princess, the compatible body where Tiamat could reside in.
Of course the lead finally find its way to The Antichrist Cluster, now however crippled, tried to send a blow again.

### Part 17
Before they could summon Tiamat, they would need to pay the price first, they need to offers Mot amusement over the unexpected deaths, the horror, and the thrill.
They was the most feared insurgency back then, but time flies and people forgot about them already.
They need to lit the public memories again.
Unexpected plane crashes are the start, its appetizer.

### Part 18
The next stage comes years later, the Terror Tires, the second massive insurgency all over Indonesia.
This is the time where all three clusters were to meet and fight together.
The Antichrist Cluster wants to eradicate The Beast Cluster, and The Beast Cluster took their time to torture The Christ Cluster.
It was quite a blow when finally, the force of Revenge is being tested, and The Christ Cluster showed Forgiveness instead of Revenge.

### Part 19
The Christ Cluster handed over The Beast Leader to The Antichrist Cluster.
The Antichrist Cluster inquired The Beast Leader, however it comes unexpected when The Beast Leader uncovered the truth the Antichrist Leader was hiding.
Anderson was the one attempting to kill Aditya and Henokh.
The Antichrist Cluster crumbled at that moment, clueless, aimless.
It was broken from the inside.
The Beast Leader took the moment to escape. Letting the Anger to kick back, and Distrust arises.

### Part 20
The Beast Cluster decided to finally reap their harvest, after all the chaos they've done, all the horror, the thrill.
They summoned Tiamat to start into their next step toward bringing Armageddon to Earth.
The Antichrist Cluster knew about it and tries to intervene, however, they ran into Keenan and Lucy, which ends in a fight that slows them significantly, long enough that the Tiamat is summoned.
But as Anthony summoned her, something snapped.
Tiamat, as chaotic as she could be, was unpredictable, and was amused by the snappening, and left The Beast Cluster.
Anthony screamed in pain, and Aditya contacted Henokh, to get in line with Manov, and then Manov come for help.
Here, Altruism came into play again, as without any conditions, Manov helped Anthony.

## The Roads to Hell
### Part 1
The Antichrist and The Beast clusters suffers after they went through all of it.
The Christ Cluster, in the other hand, is intact, and healthy.
They come with a solution, that is to directly attack the Divine Council.
For that, they visited The Antichrist Cluster, and they said it was a crazy plan, and is impossible.
Tweaking around, they need to have an insider among Powers, and Ashton suggested that it could be Kothar, which is currently in Hell.
They could help him escape hell and have him return the favor for them.
Heinrich objected, saying that they don't really have a bargaining position once Kothar is out of hell.
After some convincing, Ashton said they first need to know why Kothar is in hell in the first place, which, could be their key to convince Kothar to help them.
Manov volunteered to help, on which Ein objected.
Ashton concluded that it means they can know Kothar's weakness, and afterwards, all they need to do is to find El.
Ashton's source said that El had the key to the Hell, and so, they need to find him first.

### Part 2
Manov embarks on a journey to find Kothar's guilt, and he asked Anthony for help, considering that he had two favors to return to Manov anyway.
They embarked on a journey, and discovers Kothar's guilt.
During their trip to hell, Heinrich came to their body, and tried to kill Anthony, which is when Manov split and fought Heinrich at the same time.
After some arguments, Manov said that Ein had to trust him in this one, and that he won't be hurt now.
Ein finally softens, and gave Manov a hug.
He said that if he ever caught Anthony hurting Manov, Anthony is going to be finished by his very own hands.
Manov said he knew.
So they learned that Kothar was in hell because of his guilt on betraying Yam.
And when they return to the real world, Anthony was surprised to find the environment around them are devastated, which Manov casually said as: we got a visit by Ein.
Before they part ways, Manov accidentally mentioned that he is curious on El's whereabouts, and Anthony laughs.
Anthony said that El was never a part of his faction's goals.
And it is fine for him to talk about it. And that El is in Integra.

### Part 3
At the same time, a list of familiar names appears in front of the Kukermall, and Hendrik is set to investigate it.
Curiosity appears as some of the name weren't in this world.
And his name was in that list, which, curiously, had different titles on it.
Hendrik was supposed to be Hendrik Lie, S.H., M.H., but this one had an M.Kn. instead of M.H., which is either a mistake, or a clue.
Turns out, there had been killings reported, with names that appears to be in that list, died, in sequences.
He tried to find the next spot of the killing, and discovered the next names to be visiting at the Kukermall.
He looked through control room, trying to identify the attacker.
Seeing the direness of the situation, and that the people on that list might be killed anytime soon, he ran to the middle of the mall, casting protective fields toward the targets, and tried to locate the attacker.
He did find it, and it turns out, the Tiamat-invested woman was the culprit.
More and more WTF officers landed on the mall, and a full-blown WTF task forces attempted to capture the woman.
After some fights, they managed to capture the lady and had him brought to the WTF Containment Chamber.
Meanwhile Hendrik discovered that his version on Integra had the correct titles written on the paper, which means it wasn't him, but his version on Integra.

### Part 4
Around this moment, David meets with Anthony and Michelle at their house.
It appears the family brighten, as they've been reunited, and David is a healthy and decent speciment for Michelle's mate, despite them not dating at the moment.
Besides, David is the son of Martha's boss, she is happy to have him here.
They were supposedly hanging out.
Anthony happens to explain a story about his familiars, and his profession as a necromancer.
Michelle asked him what about her father, that is also his father, and he said that he has no idea who his father was, so he couldn't track him.
Michelle went out of her room for a while, and returned with a photograph, an old one, and weathered significantly.
In it, was shown the picture of Anthony as a baby, Martha, and their father.
Anthony focussed on it, and he glimsed to hell.
He found his father there, and he saw the scene, sequences similar to his dreams: his father wanted to leave him in the middle of the woods, memories of his father when attacked by Aurelia when he hit Martha and Anthony in their old bathroom.
it appears, that he dreamed of that because he accidentally connected to his father in hell before.
And it triggers his actual memory of that day.
It also confirms that his father is in hell.
But then, Anthony saw yet another face he recognize, David's mom. She was in hell as well.
David asked what about her, and Anthony saw a man in her torture chamber.
No, he said, there were two men, one is Steven, the other is, Fernando.
Apparently, Anthony said, David's mom had an unrecruited, unresolved feelings toward Fernando, while still having a relationship with his father.

### Part 5
Reunited, Hendrik, Heinrich, Manov, Ashton, Steven, Daniel, Fernando, Henokh, and Derictor, gathered around and discussed this matter.
It is clear that they had to find El on Integra, as well as protecting the other guys whose name is in the list, from being killed.
Their killings must have some reasons behind it, that they wouldn't know if they let them be killed.
So the party decided that they must went there, secure El, contain the malice, and protect the witnesses.
Other than that, Steven put forward the intel he had from David, that Helena is in hell.
This makes Steven, Daniel, and Fernando so adamant in finding El.
Because of the time constraint, they must go immediately.
With the help of Adran, Manov (which already is a volant), Steven, Daniel, Fernando, Henokh, and Derictor were uploaded and beamed to the Integra Paramundus Dataspace.
They took some time to orient themselves in Integra, they arrived at a local organization set up by WTF to facilitate transfer and policing on Integra, since Integra turned out to be a world where baseline to normalcy is calculated.
The organization was called the Global Foundation of Esotericism (GaFE).
They protect Integra from unauthorized accesses, sometimes anomalous to the locals.
After being equipped with necessary tools to help them, including normal guns, resources, etc, and anomalous bags, with an anomalous smartphone.
Basically they must minimize usage of anomalous objects, and the bags are quite safe according to their standard.
The smartphone, which they dubbed the Fiat Ring (because traditionally it was a ring, until recent models were made into smartphones with an user-friendly interface), is their back up tool, if something bad happens outside their control that compromises the mission.

## The Integra Crisis.
### Part 1
The moment they left the GaFE base, that is currently disguised as an old DVD rental store, they're on their own.
The first thing they do is to try to discover a proper ID to fit in.
The next thing they would need are vehicles, and Manov attempted to fulfil their requirements.
They discovered that Henokh has no counterpart in this world, and a quick search to his family, discovered that his mother is not married, and his father marry another woman, and had a son named Sylvester Lisander.
Likewise, Derictor has no counterpart as well. Daniel had a counterpart, but the family is poor.
Steven, being half etoan, is not even exist here. Fernando had a counterpart, and his house is nearby.
Manov pulled the ID-s from the Fiat Ring, and also their phones. Hendrik's ID and phone for Manov, Sylvester's for Henokh, Daniel's for Daniel, and Fernando's for Fernando.
Derictor and Steven are without IDs.
However, out of mercy, Daniel requested Manov to return his counterpart's phone, saying that it is far more valuable than his counterpart's wallet and its content.
So they decided to go to the house of Fernando's counterpart, that appears to be not present, and took his car.
It turns out the house possess a working internet connection, so they take the time to browse and familiarize themselves to the world.
Daniel looked for El, but instead found a version of Hendrik in this world, that is a writer, that writes about their adventure, in the real world!
Meanwhile, Manov, Henokh, and Derictor managed to pinpoint several names that are present in the list.
They consulted on how would they do this.
First of all, for Steven and Co., finding El is their utmost objective, while for Henokh and Co., finding informations on why people listed on the list are killed, is their utmost objective.
Besides, it is very weird for Manov to try and locate Hendrik, since he shares the same face as Hendrik.
So it is decided, they are going to be split into two teams: Team 1 seeks Hendrik, and find a way to use him to find El; Team 2 is to find and secure people listed on The List.

### Part 2
Team 1 tried to get the address of this Hendrik guy, but they had a hard time, since this guy left only a trace of online presence, other than his blog.
Since his account is not private, albeit rarely updated, they use it to track his friends, and happen to find a recent story stream in the last 7 hours, that mentions his whereabouts.
It is tagged with a map location, so they searched the direction to that location.
With Fernando's previous experiences on the Private Police Force, they decided to impersonate officials, and asked in a manner as if they're investigators.
They started to question the whereabouts of Hendrik to the store owner.
It turned out, it wasn't in their shift, so they requested addresses of those that work in that shift.
It turned out, one of them knew the friend Hendrik was with, it was a worker in that bar too.
They locate the worker, and uses the picture they had with Hendrik in real world to convince her that they were his friends, and that they planned to make a suprise party.
The friend asked, "what surprise party?" They remembered that Hendrik's birthday is yet to come, so they decided to say that it is the anniversary of his oath as a lawyer, and they're the colleagues of Hendrik from his university.
As absurd as it sounds, the friend of him actually believes in it.
She also noted how different Hendrik is in that picture compared to his usual expressions when they took pictures together.
They wished to exchange pictures, but unfortunately the Kaos of Kukerphones and the Android Operating System won't cooperate.
Nor can Kaos uses internet connection in this world, due to vastly different protocols used.
Apparently technology on prime Earth and Integra Earth, despite being superficially similar, is vastly different in the background.
Afterwards, they managed to reach Hendrik's apartment, only to discover that they'd need access card to get anywhere in it.
They tried to ask the security guy, saying that they lost their key and if the lobby has a spare key.
It turned out, the security guy don't have the spare key, they can only provide help with the access card.
After some thinking, they decided to wait around, since they believe Hendrik will come home from the office, and then they can follow him to his room.
They waited in the lobby.

### Part 3
Team 2 dropped by Team 1 at Sylvester's residence, and apparently he's wasted, probably having a hangover since last night.
So Team 2 took his phone, his wallet, and his car keys.
tried to find the four names listed in The List that happen to exist in this world, and happens to be in the city they're in.
The first name that come in mind is Yusuf, which happens to be in the Prosecutor Office.
Trying to meet him in the office is quite hard, they have to pretend to be someone important, Manov conjured badges of Badan Intelijen Nasional (BIN), and tried to convince the prosecutors that they need to find Yusuf.
As it doesn't work (Manov realized that despite being one of Hendriks, he is not the lawyer Hendrik is, not enough to convince them).
So they tried to sneak in, and capture Yusuf.
At the same time, an anomalous gang wreck havoc on lower levels, they uses anomalous guns that makes object hit with it disappears into thin air.
People are screaming, some gunshots are heard.
Manov said something hard to explains downstairs, and Yusuf seemed frightened, so they offered a way out.
Henokh and Derictor readied their guns, and they trace through the corridors, finding an alternative route.
Gunshots by the anomalous team, large chunks of the wall disappears.
They use it to run to the streets, jumping off it and with a spell they run on the building's exterior wall as if it is a level ground.
Yusuf screamed but they managed to let him run with them.
On intersection between the wall and the ground, they step on the ground and their weight normal displaced from the normal surface of the wall and the normal surface of the ground.
Yusuf, Henokh, and Derictor fell face first to the ground.
Manov grab them up and they ran toward their car before the anomalous gang followed them.
To the car they go and left, but then the car disappears and four of them fell to the ground.
They hide behind a parked car, which also disappears.
They ran to the nearby building, returning the shots with their normal guns.

### Part 4
Team 1 waited for too long, and they decided to eat around the apartment, take some meals etc.
Apparently the night dawned and they haven't discovered Hendrik yet.
It turns out, there Daniel noticed that the building could be entered from the parking lot as well, which means Hendrik could've been in his room already, but they didn't know.
So they asked the security guy, saying that their friend that lives on the 31st floor replied, and they'd like to take the lift now.
The security guy scanned his access card on the lift and they went to the 31st floor.
There, they knocked, and after some pause, some rustles, etc, they found Hendrik opened the door in his small apartment room.
They barged in.
Hendrik baffled, asked them who they are, but the face tells them that he recognized their face.
Steven asked him, how did he know about their past, and Hendrik, seemed cautious, asked them what do they mean.
Steven told him about their high school days, that Fernando proposes him, he got scared away and almost killed Daniel that confronted him at the wrong moment, and that they finally forgive each other, and Daniel is now with Fernando.
Hendrik just gave an audible "ah".
He thought some more, and asked, "so Integra is here?" On which, the trio, seemed confused, said "yes".
So Hendrik asked, "so, Xenomancy is real?" Daniel asked what is Xenomancy instead, and he said that it was because the foreign divines are interfering with Earthly matter, that the characters are trying to understand, to divine the intent of the foreign divines, hence Xenomancy.
Fernando put forward the motion, that they're currently looking for El, and Hendrik said that he has no idea.
Some audible screams could be heard from the outside, far as in some hundred meters away.
They decided to look at the balcony, and discovered that the screamings come from the ground level, there are guys thrown away to the air, some shouts from the securities, and people at the pool on the first level gazed down.
Some guys jumped to the pool, and then more peasants thrown into the air.
Hendrik then commented, and there are the men of Tiamat.

### Part 5
Team 2 barged into an insurance office, and asked to meet the guy known as Elbert, and they said that they have not much time.
The armed guns they carried are sufficient to cause panic, and Manov said don't panic, they need to bring Elbert to safety.
There are group of men trying to kill him.
Yusuf, with his official clothing said that it is an official business, and he showed his badges, saying that he is a prosecutor.
Layperson don't really know what a prosecutor do, so they quickly bring forth Elbert, who looked confused.
Manov, with his black cassocks and a white cleric collar, looked more like a priest to Elbert, asked him if there is a second exit.
Elbert showed them. Elbert pondered because Manov looked familiar to him.
Then the front door disappears, and a gang group barged in, making people screams.
The five of them ran away to the second exit.
Derictor throws some of digital representation of his knives, some of the gang members fell, and adds more screams to the civilians.
Henokh proclaimed that he never actually liked face-to-face fight with psychics before, on which Manov corrected that he is a reflexior, not a psychic, but paused to think awhile, concluding that reflexiors are just a thing on their real world, in this world it could be anything.
Elbert asked, "real world? Isn't this the real world?" Manov took some moment to think, and only said, "long story, tell you once we're safe."

### Part 6
Team 1 decided to go through the emergency exit, as, according to Hendrik, he believes that the gang members might not be well aware about the emergency exit.
Steven readied his phone to the offensive mode, and Hendrik asked if he could see his exoself, and Steven said it wasn't the best time.
Nodded, they barged through the emergency exit that leads to the parking lot.
Soon after, they're only midway, Hendrik said that he forgot that they'd need some good minutes to actually finish the stairs, and the trio mentioned that it was quite hot here.
There are no receptions here as well.
They exited at a blocked exit.
In the other side of the door were boxes, so they tried to push through slowly, and peeked.
The gang members are barging toward the entrance to lobby.
They made the door disappears, while normally one would need access card to access it.
Hendrik said he is not sure if his bike is a proper method to get out of here.
Steven said they had a car.
They managed to sneak toward their car, when it was obliterated by the gang member that noticed them.
Steven aimed his phone and flashed a blinding light to temporarily blind him.
They ran, and now have no idea how to escape, with no car available.
Hendrik said that he is currently taking care of his uncle's car, His uncle and his son were in a vacation now, and they decided to take the car.
Fernando drove, and they left the building, breaking the security bars, and full speed toward the exit gate.
The gang members ran to chase them, but they turned north as soon as they reached an intersection, blocking the gang's view to the car.
Hendrik suggested to go to a nearby empty field next to yet another, calmer apartment building, north to his apartment.
They decided to contact Team 2, but Team 2 seemed to be busy.
Soon after, they received a prompt to meet at the house of Fernando's counterpart.
In their trip, Hendrik took a moment to ask, "so, when I lost one of my phones and my wallet today, is it because of what I think it is?"
Steven said that most probably it is.

### Part 7
Rahma is a tall woman, and is currently working happily at a notary office.
She had been worried because Yusuf hadn't replied anything to her messages.
She was upset already, when her friends told her that Yusuf is here looking for her.
She was surprised and ran to the front gate, and Yusuf pulled her.
She resisted, but then she discovered that he's not alone.
There are four chinese-indonesian with him, one with a tattoo in his left face.
She recognized the one with tattoo, asking, "Hendrik?" Manov said, "No, I am Romanov, I just happen to share the same face with the guy you recognize.
Now get in, we're going to meet Hendrik at our place."
Her boss angrily bursted out of the office and said they can't leave, and Yusuf said it is urgent.
They left.

### Part 8
Team 1 was puzzled, Fernando's counterpart stood in front of the opened gate beside his motorbike, and the opened front door.
Then he gazed at his car, currently waiting outside.
The night was calm in the complex, and Daniel stepped down and approached him.
He asked Daniel's identity, and he said that he is Daniel, and he and his friends can explain everything, as long as he stays calm and composed, whatever he is going to see.
Fernando's counterpart inquired him to just explain it already.
Fernando and Steven stepped off the car and approached Fernando's counterpart.
Gasped, he asked them who they are, while backing down, and Daniel reminded him to stay calm.
He explained that they're from an alternative Earth, and is currently here for a mission.
And they happen to have Fernando's counterpart here.
To ease things out, they used Fernando's facility to help their mission.
After all, having the car driven by the owner-lookalike with a proper ID is less suspicious than having a stolen car around.
They asked him to stay calm, and Fernando talked with his counterpart, while Hendrik drove the car in.
Apparently his counterpart prefers to be called Edo, and Fernando said he'd like to be addressed as Nando.
So Nando told him again that they're from an alternative Earth.
And they are currently in a mission to save their friend from hell.
However, to help their friend, they need to find the Biblical El that hides here, in this version of Earth.
Hendrik added that he is from this world that Edo inhabits.
Edo objected, stating that their excuse is just extravagantly ridiculous, how could the key to go to hell to save a friend requires the Biblical El, and that the Biblical El is in this Earth, let alone the part about multiverse shit.
Then they noted that another car came in to Edo's house.
Edo went outside with the group to greet the second group.
Team 2 arrived, and Hendrik was stunned to see Manov.
Yusuf and Rahma were surprised to find Hendrik, and that Manov wasn't Hendrik.
And Edo inquired, why are they on Sylvester's car, until Henokh came out, which, he recognizes, as resembling Sylvester, but not exactly Sylvester.
Nando explained that Henokh doesn't have a counterpart here, because his father married another woman, and his mother is an old virgin in this version of Earth.
Therefore, he was never born, but he had a biological half-brother known as Sylvester.
Then from inside, Daniel yelled that he found his pictures on Edo's yearbook.
Elbert shouted, that he finally remembers, when seeing Hendrik, that they meet in a training on an investment agency!
Hendrik also recognizes Elbert.

### Part 9
Edo usually enjoys silence in his house, but today it was the exact opposite of what he enjoys.
The house is full of people.
Most are strangers, one an exact copy of him, one is a half brother of his best friend that never is a half brother, and a bunch of people he just met.
Daniel here turned out to be the doppelganger of his high school best friend, Daniel.
He claimed that in his version of Earth, they went to the medical school together, but Edo said that in this version of Earth, Daniel never went to the same medical school as him, he is not even sure if Daniel here took the medical school.
Manov explained that, Hendrik, Yusuf, Elbert, and Rahma are in danger because a group of anomalous gang are chasing them, and it appears the gang are chasing the names in a list he hold.
Hendrik said he believed that it was the men of Tiamat.
Manov gazed at Hendrik, then he stated that he can't track the last name that he must protect, Ricardo, so they must make do with what they have now.
And Steven added that they haven't able to locate El yet.
Manov inquired, who are these men of Tiamat, and Hendrik said he had no idea.
Steven asked once more, "what about El?" and Hendrik said he had no idea yet.
Rahma asked them, why are their name on the list, and why are they chased? Manov said he had no idea.
At first he thought one thing that all of them had in common is because they all know Hendrik, but it makes no sense, since in his version of Earth, where nobody knew about people from this version of Earth, he is certain that all the victims had no connection to Hendrik of this world.

### Part 10
Hendrik started to talk, that the men of Tiamat was designed by him in his story as a part of the advances goddess Tiamat set up in this world to track down the last key required to open the gate of Hell.
And that the last key required is the Mark of El, which, is yet to be resolved where it is.
Hendrik then invented the biblical figure Cain, as the one that bear the mark, given by El, now known as the Mark of Cain.
However Hendrik did not find any mention that Cain could still be alive today.
Biblical resources didn't provide any clue that he would be immortal.
Most likely, he is not.
"Whatever it is," Hendrik said, "perhaps our best chance is to track down Cain, if there's any here."
"How?" inquired Henokh.
"I have no idea."
Henokh then asked, what about these guys, why are they been attacked?
Hendrik said that they're related to El in one way or another.
Manov had his attention drawn.
"What kind of connection?"
Hendrik said he had no idea.
"But perhaps, it is that they had been interacting with El during their entire life."
"Have you?"
"I don't know,"
"but your name is in the list."
"Does it mean, I must've been interacting with him without my knowledge?"
Manov thought hard on this.
"Why are Tiamat chasing those that had been interacting with El?"
"Perhaps because we carry clues on how to find El, or his mark?"
Henokh stepped up and said that it is apparent that they have no other choice but to protect those whose names are on the list.
Then they need to interrogate them and try to find who is this El they've been interacting with.
Manov said, then they must secure Ricardo first.

### Part 11
The next morning, the group of two cars left Edo's house.
Hendrik recognized the guy they're after, a cousin of one of his friend that are currently studying at a veterinary school to the west of Surabaya.
Team 1 is like yesterday: Steven, Daniel, Nando, Edo, Hendrik.
Team 2 is: Manov, Henokh, Derictor, Rahma, Yusuf, Elbert.
They dispatched with the goal of securing Ricardo.
And afterwards, they could worry on how to extract the information that leads to El.
Manov still wonders on why would Tiamat provided the list to Hendrik in the real world, and why are they chasing them.
Steven, Fernando, and Daniel thought really hard about how to find El, as Helena is still in hell.
There are two possibilities on Ricardo's whereabouts.
The first one is that he is attending his class on a veterinary school to the west of Surabaya, or that he is visiting his relative on the southern Surabaya.
So Team 1 decided to go to the western Surabaya, and Team 2 to southern Surabaya.

### Part 12
Here is the situation recap so far.
Fernando, Hendrik, Elbert, Joseph, and Rahma from Integra had their life changed forever.
They can no longer return to their normal life, not with the rampage Men of Tiamat still on the loose.
Romanov, Henokh, Derictor, Fernando (Xe), Daniel (Xe), and Steven are yet to accomplish their missions.
Romanov, Henokh, and Derictor are yet to secure all names in the list they found, and they still have to figure out why are they targeted.
Steven, Fernando (Xe) and Daniel (Xe) are yet to find El, they need to find a way to track El down in this world.

### Part 13
Team 1 was on their way.
Daniel sat in between Nando and Edo, and he fell asleep due to lack of sleep.
Out of curiosity, Edo asked Nando, are Daniel and Nando great friends?
Nando said that it is more, he and Daniel are a couple.
Hendrik from the driver's seat asked, "wait, what?"
Steven, right beside Hendrik stated, "they had sex together."
Hendrik said: too much details, Steven: you asked it.
Edo just asked, "so Daniel is gay?"
Nando: "No, more like bisexual.
He was into Steven's wife, Helena.
But as Helena chooses Steven, he moved on, and happen to be fond of me."
Steven responded:
"We made quite a drama back then.
I love Helena, and so did Daniel.
We fought for Helena, which, as I lately learned, Helena had feelings for Fernando, but Fernando confessed his love to me back then.
It was two thousand and three I believe.
I freaked out, and because of that I almost got raped by a man-eater, and then I almost had Daniel killed because he confronted me at the wrong time.
In the end, I stays with Helena and had four great kids, and Fernando-Daniel are together ever since."
Edo took some moment in silence.
Nando asked, "so what about you?"
Edo: "Your life is colorful.
I didn't know that Daniel was bi.
If I knew, I'd dated him back then, on high school."
Nando: "why didn't you?"
Edo: "Is your Earth more accepting to LGBT? Here it was harsh.
I decided that I would rather be alone, and die alone, than to pretend and live my life with a woman that, despite being a great woman, I can't love."
Nando took some time to let Edo's words sink in him, "No, my Earth is just as harsh.
But I accepted that as a part of reality, and just embrace who I am.
I can't live forever in a lie I built myself.
And I happen to discover Daniel, and he is great."
Edo looked at Daniel, "yes, he was great."
They parked at the campus, full with college students going by around.
After some time walking around, they stumbled upon an administrative building.
And at the time, they saw Helena.
Steven ran hard toward her, hugging her, and kissing her, and got a slap from her.
On the floor, Steven confused, looking at this Helena.
Different hairstyles, so energetic, and strong.
Slightly confused, she looked at Daniel, asking him who this guy is, and what he did to his hair.
"Uh, I just got a haircut, and this is my friend, Steven,"
"You could grow your hair at the barbershop?"
Before he could even answer, Helena slapped another statement,
"Doesn't it bother you when a friend of yours kissed your wife in front of you?"
"A wife?"
Even more confused, she looked at Nando and Edo, "..., Edo? And, y, your twin?"
Hendrik said that perhaps it wasn't the best idea to bring Edo and Nando together, while trying to get Steven up.
"What the hell?"
That was the voice of Daniel, but not coming from Daniel's mouth, it was from behind them, where another Daniel came by, with a receding hairline.
They could see Helena's face is blanking, full of confusion.
Hendrik backed off slightly, "Guys, I don't know anything about what is going to happen next. This part is yet to be written,"

### Part 14
Team 2 arrived at a dormitory next to a large campuss in Southern Surabaya.
There is one dormitory painted in red, that exactly looks like what Hendrik described.
But when they came by, people are running away from the dormitory.
People wounded from the fall from higher up, some had broken bones, some had plenty of blood around.
The never ending scream accompanied what looked like Men of Tiamat, from the fourth level, the topmost level.
People are flung away, and they tried to barge into a locked room.
Romanov looked at the opening in between walkways.
The dormitory's floor-plan is made by three rows of bedrooms set in parallel.
The first row left to the entrance is facing right, and in front of it is the second row, facing the first row.
The third row is right behind the second row, facing away to an alley in the back.
The next levels are similar in floor plans, but there are open fenced gap where the first floor could be seen.
Atop of it, at the uppermost ceilings, are translucent glasses, that allows passage of natural lights during daylight.

Romanov instructed them to follow his exact steps.
He jumped toward the wall, and to the ceilings.
Now the uppermost ceilings are far underneath him, as his down direction turned upside down.
Hesitantly, the remaining of them followed his steps and they're upside down.
Romanov jumped down.

(...)

Tiamat's Men, comprised of two group: men and women.
Women are all Tiamat's manifestation.
Men are all Yahweh's manifestation.
Tiamat do her job, and Yahweh ensures as little native observers as possible, to prevent the possibility of global awareness of Integra natives.

### Part 15
Team 1 and the triads of Integra.

### Part 16
Team 2 got cornered by a Tiamat manifestation, Romanov involved in a close quarter fight with her, and she was impressed by Romanov's realization of the relativity of this reality.
After she's bored, she's going to kick them off this reality with a hand grip.
The grip is almost completed when Steven ran toward them and raised his phone, that obliterated immediately and sends a shockwave that obliterates all instances of Hadad and Tiamat in the entire campus.
It was Adran's work, etoan tech, in expense of Adran's instance in this world.
Only three were present, that acts as the projector of the (Xe) visitors, also the only ones strong enough to cancel other projections.
As one is down, two are left on Fernando and Daniel.

They decided to return to the gate and initiate a jump out of Integra.
Romanov realized that as they also need projectors in this world to exist as guests, the Men of Tiamat must also have their own projectors, a conduit that translates their existence to Integra environment.
If they managed to find their projectors and destroy it with the expense of one of the protagonists' remaining projectors, they'd be gone from Integra for some time before they could resend their manifestations back.

### Part 17
They took a trip to the GaFE base they emerged from, but there was something weird, very weird in fact.
As they approached the block the base was in, suddenly the planar dimension of the block expanded significantly, leaving the height intact.
Trees went farther away from each other, buildings stretched on horizontal plane direction, but cars, people, and creatures remained with their normal dimension, only getting away from one another.
Far in the middle, they could see that the base weren't expanded like other buildings around it, and six men stood surrounding the building, with their fists held in front of them, forming some sort of invisible wall that protects the dimension of the base.
Their arms glow bright blue, and they seemed to struggle.
A group of twelve surrounded the base, spreading their arms, stretching the dimension, they were comprised of men and women.
It was a fight between Men of Tiamat and GaFE guardians.

The protagonists decided to act on it, and start to attack the men of tiamat one by one.
The dimensional stretch start to destabilize, but one of the guard approached them, saying that it wasn't their fight.
Asked why by Manov, answered by the guard that they need to go from yet another gate, and that their mission wasn't protecting the gate, but rather, to find El.
He crossed his arms and a bright blue shockwave sends the protagonist team flew high to the sky, horizon start to curve around them, and from a distance they could see a translucent blue stream connecting the space ad infinitum to the gate they're banished from.

On the contact between the stream and the gate, they could see a bright red explosion, and the stream snapped, receding away from the gate toward the space.
The gate was destroyed, Manov realized.

Getting further to the sky, and the horizon start to curve even more, they could see that there are a number of blue streams connecting space ad infinitum to certain points on the surface, the gates.
Some of the streams receded away from the surface, that the group concluded to be the gates failing to the attack of the Men of Tiamat.
They approached one intact stream, and from the stream they could feel themselves withdrawn from their core toward the surface.
Horizon uncurved as they approached the ground at a great speed, so great they suffocated due to the rush.
A meter before touching the ground they could feel like their inside was pulled with such a sheer force from the direction of space, and then the pull snapped and they fell to the surface, feeling disoriented, with short breaths, and upset stomachs.

Looking around, they find themselves in a field right in front of the Major's office.
They could see that it was built on the side of a cliff, and there was a monument right across the road, before the hill slanted significantly.
The monument was a tail of a dragon.
Manov, which was now able to differentiate certain strands that connect objects in this world, noticed that the monument had the same invisible blue stream extending ad infinitum to space.
It was a gate.

### Part 18
Someone approached them, an old man with dark skin, large glasses, and tall.
Manov and Hendrik (Integra) recognized the old man as Uncle Tahoo.
The name stuck in their memories because in Indonesian, it is written as "tahu", which is also a word for both an Indonesian noun for tofu and a verb, "to know."
It was said that he knew everything, he can repair anything, and is very resourceful.
And he was currently there to repair the world, he said that there was an unauthorized gate forming on Batusori tourist resort.

Hendrik realized, that the projector for the Men of Tiamat originates there.
He just knew it.
Uncle Tahoo deliver them to a bus, driven by Erwin, a driver that both Manov and Hendrik (Integra) recognized as well, as the man that was known well to be able to drive any type of land vehicles, large or small.

### Part 19
On the road to Batusori, suddenly cars appear on their front and from their behind, trapping them in the middle.
Manov tried hard and flung any car in front of them.
The cars behind them are approaching, at that moment, Hendrik realized that it is like lucid dreaming.
Focussing, he "zipped" the road, the edges of the road closes together, wrapping anything on the road underneath, crushing them.
Manov was surprised, and Steven et al. noticed a mark appeared on Hendrik's forehead: a cross encircled with a circle.

A narrative appeared, about oneironaut, about reversed causality.
And to Hendrik's past, about his faith, his guilt, and his exploration to lucid dreaming, and now, realizing that the entire Integra was just a dream, and that they're all oneironauts.

### Part 20
Romanov shaked Hendrik, and the Mark of Hadad disappeared.
Romanov asked what happened, and Hendrik said he felt like the entire world is like a dream.
Steven added that, in a way, it is.
It is a virtual world after all.

Manov saw it with his mental eyes, there was that blue stream from the peak of Batusori resort.
The stream beamed up *ad infinitum*, the signature shape of a gate.
From it, some orange and green beam could be seen extending to multitude of directions.
Green and orange are signatures of the projections of Tiamat and Hadad, The Men of Tiamat.

Manov knew it that they're coming from this gate originally, before spreading to the world.
The team entered, and paid for 5k per individuals for entrance to Batusori.
The peak is just a field, nothing much featured, just a field, and by the edges people gathered to take pictures.
Their attention were piqued by a structure in the middle of the field, like a lobby, or a toilet.
It was the gate.

From inside it, come out a number of identical men and a number of identical women.
The Men of Tiamat went out along with sudden pulses of the green and orange beams.
They're convinced beyond doubt that they came from the structure.
Public around them just gaze with curiosity.
They appear to care more about what is happening than their own safety.
Some starts recording.

Hadad and Tiamat had a fight over how to fix this recording problem they had.
Tiamat didn't listen and start attacking the protagonists.
They fight, as Daniel began drawing his utility tablet, and Tiamat pulled it away from Daniel.
With no utility tablets, Daniel and Steven fought with no armor, so Fernando, Daniel, and Steven exchanged Fernando's armor.

Daniel's utility tablet fell from the cliff to the sea, as he tried to recover it and Tiamat kicked it off.
To prevent the recording to spread, Hadad caused Earthquake on Batusori, and with a surprise, blasted air waves toward the spectators, some fell through the cliff, their phones were taken away and spread across the sea around Batusori by Hadad.
Amidst the fight, Daniel (In) and Helena (In) kissed each other.
Helena had a moment with Edo via a brief gaze, while Edo thought of Daniel (In).
In his head, Edo and Daniel (In) would kiss, and Edo announced his love to him, and Daniel (In) would leave Helena.
Edo was about to move toward Daniel, Hendrik stopped him.

There was a moment between Hendrik and Edo, and Helena let go the gaze, melting in the love of Daniel.
Retrospectively commenting the moment, Edo agreed that it wasn't the best option there is to relive his past love with Daniel (In).
Given the moment, Edo realized how Daniel (In) was complete now, with Helena.
Hendrik added, that if Edo told Daniel that Edo loves him at the time, Daniel would break, and torn between Helena and Edo.
Daniel might eventually choose Helena, but a part of him would then be torn, he'd not be the same.
Edo gazed at Daniel again, pondering about the possibilities of them being together.
Hendrik added that in this world there's no Steven.
With Steven around, Fernando would first love Steven, and both Steven and Daniel would want to be with Helena.
When Steven captured Helena's heart, even though Helena loved Fernando as she is in this world, Daniel decided to stop.
And Fernando was there for him.
And they were together, both wounded, both understand each other, and both had the capacity to accept each other.

Edo asked, why does it felt like Hendrik knew them well, even though Hendrik never meets them.
Hendrik said that he designed all of those characters, he knew every single one of them by heart.
Or at least he thought he designed them.
What is stranger is that they actually exist, both in the alternate Earth, and their own Earth.
"Stranger than fiction" Edo muttered.

### Part 21
How does one know that he is not a butterfly dreaming to be a man?

That line occured to Hendrik's thought.
Usually that line of thought branched out to various other thoughts.
He's in brainstorming mode.

Mankind is made after God's image.
Or perhaps mankind is just a reflection of God himself.
Perhaps he is dreaming to be a man.
Or not just any man, all of them.

What if the sole purpose of religion is for mankind to be God?

Another line of thought, that ultimately, the aim of every religion is that so that mankind becomes more like God.
Or that God wanted to experience the world, by becoming humankind.
Not just any human, all of them.
A collective experience of the entire human race might be God's way to answer to himself.
To live.
Perhaps the God himself wonders about his own existence, his own self-purpose.

Or that God is forced to feel through the entire human race as a punishment, or a distraction, from another jealous God.

Or, his own son.

Or his own grandson.

El came to this Earth, and fostered humankind to prosper, be fruitful and multiply.
Rule over this Earth, and take care of it.
Those that lives by El's standard, be granted immortality, living among gods.

Another brainstorming about El, Hendrik thought.

His son was to be crowned, but was defeated by his nephew.
Yam was defeated by Hadad.
Hadad rules, and Yam fails to be a ruler.

Yam had his very desire taken away from him, neutered him to be a loyal servant of Hadad, as an Eunuch.
Hadad rules, and Yam fails to be a ruler.

The very essence of Yam's desire was Tiamat, his heart.

And Tiamat was with Hadad now, fighting the protagonists of the story.

Why is Tiamat helping Hadad?

Manov came by and shaked Hendrik, "Hey, your head, it is showing Hadad's sign again!"

Hendrik barely listens to Manov, he gazed at one of Tiamat.

She smiled back.

"Father, I am here."

"It is opening," there was a pause, " into an eye, a third eye," continued Manov.

The entire protagonists and antagonists gazed toward Hendrik.

Hadad wanted El to stay asleep.
Tiamat helped Hadad only on the surface, the reality is, she wanted her father to wake up.
She wanted me to wake up.

"Guys, apparently, I am El," it didn't feel right.

He looked at other Integra residents.
All of them shared the same looks.
All of them shared the same souls.
All of them, *us*, are El.

"No, I mean, *we* are El, all of Integra."

Manov stood by the cliff, trying to discern the streams that project them.
The one strand of stream with the color not unlike that of a gold.
Manov pulled it, and Daniel's phone was ejected out of the water body to Manov's hand.
Manov gave it to the Integra citizens under their protection.

"Held the phone well, okay. We are going to save you!"

Manov nodded to Fernando, which then converted his armor back to utility tablet, and threw it to one instance of Tiamat.
It exploded in shockwave that turned Tiamat and Hadad instances to dusts.

"Now!"

The utility tablet that was currently in the hand of Edo exploded in an energetic burst that sends them all but Hendrik away from Earth.
They were withdrawn with such a force their heart raced hard, they suffocate even though they can still breathe.
It felt like their hearts were pulled away so hard they were about to jump out of their bodies.

### Part 22
The Earth curved and they can see the entire Earth's spherical body.
The blue streams of the gate turned white, starting from the surface.
A white strand chased them as they move away from Earth.
The space around them curved, and high above they could see the sky opens into a blank space of white background.
Another look downwards and the old sky, and the old Earth down below curved so much that they see both of them as a spherical ball.

They saw Earth exploded in a bright white, almost erasing the space encompassing it.
The white lights spread out of the space-earth ball and on the perimeter of the enclosed space, the white lights turned into black strands.
The black strands chased them as they move away from the strands.

Another look to the top revealed the whole scenery of the spot they depart from on the "real" Earth, all contained in a spherical shape.
It was still far, far away.
As far as they were with the expanding thickening zillions of black strands with a core of bright white sphere that was once Integra, the strands continue to expand.
It looked like an eye with a bright iris, and pitch black sclera, surrounded by thick eyelashes radiating away from it.

Joseph and Rahma gazed at each other.
Rahma said that if it is the way her life ends, she wanted to spend it with the right person.
Joseph said that he is willing to be that right person.
Rahma said that he must not change, because he is.
They kissed.

Daniel (In) held Helena close to him, Helena said that at least their life is fulfilled, they were there for each other.

Elbert just gaze at the impeding failure of them.
Edo asked him if he thinks they could outrun the strands.
Elbert said that ever since he meets Hendrik, he realized that he is networking not for money.
Edo asked what it was, and Elbert said it was to connect with other people.
Edo failed to see the reason behind it.

Elbert explained that connecting with a lot of people made him realize that deep inside, everyone is the same.
Everyone is just an entity trying to struggle in this world, experiencing joy, pain, love, hate.
Depending on how they experience the world, they changes, but deep inside, they're the same.
Perhaps, it was because, all of them are the same.
Perhaps, whatever divinity was on their Earth, it was struggling the same as them, experiencing joy, pain, love, hate.
Like what Hendrik said, perhaps we're all just a single soul, refracted into billions of fragments, appeared to be billions of men.

Edo gazed at Elbert, confused.
Elbert said, look down there, if we were to ignore our fear of the unknown, of *that*, we could see a subtle whisper, 'come here, come home.'
Edo backed off, but Elbert continued his words anyway, "perhaps we were scared for no reasons at all."
Everything down there was their entire world.
And their world changed, and now is chasing them.
Perhaps it was because we were running away from it, to be detached from it.

Edo gazed at Elbert, and then at the expanding black mass underneath them, and then to the surrounding emptiness of white background.
Edo gazed up, despite looking peaceful, the sphere above them didn't seem like they were welcoming.
It wasn't his place, it wasn't their place.
Their home is down below.
Edo heard a faint whisper, not in words, but something Edo could understand, "come here, come home."

Elbert said, "See? Perhaps the black mass isn't trying to catch us, perhaps it wanted us to rejoin us, to connect."
The moment Edo nodded in agreement with Elbert, from their body extended black strands that connect with the nearest strands to them.
From their body radiated additional strands, connecting them to Rahma and Joseph, Daniel and Helena.
Manov realized what just occurred, and stepped back, chasing them before being taken away, screaming a loud "Noo!"

They're now strands, and Manov just gazed at the black mass underneath them, ever expanding, as they are ever moving away from it.
As contradictive as it was for him, now half of the empty space is black, the lower half from their perspective.
Above them is a green sphere of the field they departed from the real world, surrounded with a white background.
Below them is a white sphere that was Integra, surrounded with a black background.
The black strands are now the black sky underneath them, still ever expanding, yet never reached them, as they move away from it.

"Weird," Manov said.

"What is weird?" asked Steven.

They gazed at the white pearl underneath them.
They gazed at it as if it was a close up, but it was so far away it is no bigger than a mole in their field of vision.
Bright white souls orbiting their own collective mass, joined together with strands of white strings.
All men that've ever lived on Integra were joining together into one.
All fragments of El was collecting into a full El.
All of them gazed at the protagonists.

"What happ-"

### Part 23
Steven barely able to finish his question when they felt a very quick burst of breath, sucking all of their soul to their body.
They jumped up, from the bed they were on, and Hendrik pulled Manov who was floating, querying him, "what did you all do?"

The Earth was shaking hard, and they gazed from the windows of Adran, a giant black tower-like structure emerged from the ground, growing like a tree, reaching immense height.
Gigantic hanging ships descended down from the heaven, with string attaching them to a structure high above the equator could be seen.
From the ships, jumped off platoons of floating beings, the Heavenly Hosts.
They started to systematically attack the extruding towers.

The sun rose up, and Adran flew away to the night side.
It was only after Adran flew away from the nearby tower did they realize that it was not just one towers.
Far in the horizon, they could see that one of the tower that emerged at the sea launched off, from the sea itself.
Also a deep rumble could be heard from the tower they first saw, and they could see the land concealed in heavy smokes, while the tower raised rapidly in a burst.
There were explosions all over the surface of the towers, as Heavenly Hosts tried to bring them into halt.
But it was too many of those rockets.

Then the rockets split into smaller rockets after several height, while their first stage fell to the ground.
The smaller rockets spread so evenly that the Heavenly Hosts had some hard time to focus their attack.
It was the war of gods, of the gods from the Shamayim, to a god from the Earth itself.

"What happened?" asked Steven.

"Apparently you woke up El, and El is going to take the throne back."

Daniel sat down, while looking at the background war between Heavenly Hosts and thousands of black rockets going to space.
"Hey, that means we successfully find El."

"It was never about waking up El, we just need to find him to find the key to Hell!"
Hendrik was almost shouting.
He pulled his hairs, gazing at all the mess there is.

"So, we didn't find the key to Hell, and El is awakened," said Fernando.
