# Xenomancy
By Hendrik Lie.

## Part 1: The First Trigger

### [Polar Opposites](../Chapters/Xe-1%20Polar%20Opposites.md).

[Aditya](../Characters/Aditya%20Wijaya.md) and [Henokh](../Characters/Henokh%20Lisander.md) were best friends.
They were having a sleepover party, and Henokh told him a story about Yam and Hadad that were having a fight, as he was told by his grandfather from his mother's side.
They have the same dream, of solving the problems of the world, one step at a time.
Aditya came to the conclusion that the society is broken, and it needs to be reshaped so that the world could be a better place.
Henokh said that society can be healed, one step at a time.
When [Anderson](../Characters/Anderson%20Pondalissido.md) entered Henokh's life, and offering a way to do it in Henokh's way, Aditya left for a more radical approach, and Henokh couldn't do anything about it.

### [The Boy from The Woods](../Chapters/Xe-1%20The%20Boy%20from%20The%20Woods.md).
Aditya discovered [a boy lost in the woods](../Characters/Anthony%20Matthias.md), accompanied with a dolphin-like being made of water blobs.
He raised the gifted kid in secret, in fear that people would scold this weird kid.
One day, an organization contacted him, offering membership, and a chance to develop the kid's full potentials.

### [Incursion](../Chapters/Xe-1%20Incursion.md).
Years after, Henokh founded an unnamed Intelligence Agency, originally to track down Aditya, but later also ensuring to maintain the stability of the society they're in, so that Henokh's dream could be established.

### [The Silent Concert](../Chapters/Xe-1%20The%20Silent%20Concert.md).
Never did it crossed his thought that such this covert operation would also collide with other covert society, until he actually saw it with his own eyes: the esoteric battles far hidden from the mainstream human history.
Following that, Chandra is mad that Henokh decided on a solo operative mission, with Derictor, so he take over the mission.

### [The ORCA-strated Event](../Chapters/Xe-1%20The%20ORCA-strated%20Event.md).
Not enough with witnessing the battle between mages, Chandra had to witness another esoteric event: the battle between Spirit Realms.

### [The Esoteric Business](../Chapters/Xe-1%20The%20Esoteric%20Business.md).
Nevertheless, Chandra, feeling powerless against such situation, requested Henokh to have an Esoteric Counter-Response division.
Followed with the institution of the DoE and the fifth division of the DFO.

## Part 2: The Divine Matters

### [The Lost Son](../Chapters/Xe-1%20The%20Lost%20Son.md).
Pre-Ashton age.
[Charles Lee](../Characters/Daniel%20Ashton.md) is content with his life, but Ostaupxitrilis Pontirijaris (Os) that claimed to be him bugged him in his dreams.
Os told Charles of a mission, to save humanity, to make humans human.
Charles wanted peace, mentioning that Anderson is doing the job well, and they don't need him anymore, as all that he had to be done had been done already.

### [The Angel's job](../Chapters/Xe-1%20Angel's%20Job.md).
Two Angels, different orders.
The two Angels [clashed with each other](../Chapters/Angel's%20Job.md), which raises the question, what is happening in the heavens?

### 2021: [The Artifact Hunt](../Chapters/Xe-1%20The%20Artifact%20Hunt.md).
EPL seeks a Divine Artifact, while the Intelligence Agency tried to circumvent it.
What is the plan of EPL with the Divine Artifact anyway?


## Part 3: The Commotion

### The Umbra Cahaya.
The Intelligence Agency attempting to learn EPL's plan, which lead them to the movement known as the Legion of Umbra Cahaya, a front-end of the EPL.
Hendrik learned that the guy they've been looking for all of this years: Anthony Matthias, is actually a best friend of Romanov Dexter.
Anderson realized that the Divine Council of Earth actually intervenes with the matter.

### [The Birth of Daniel Ashton](../Chapters/Xe-1%20The%20Birth%20of%20Daniel%20Ashton.md).
Begins with the arrival of Sechan's and Sehyung's father in a rare occasion, Charles that was trying to be content with his life, happen to meet Fernando where he shouldn't, which turned out to be Haein's cousin.
Being confronted by Tee about his nature on why Fernando referred to him as grandpa Os, he revealed his etoan ancestry, and the mission guy (Os) that had been bugging him in the past year.
Charles is reminded by Tee that there's no point of running away from his mission.
[Accepting who he is](../Chapters/The%20Birth%20of%20Daniel%20Ashton.md), he merged with Os, and becomes Daniel Ashton.

### 2021: The Dawn Strike.
An explosion occurred in the headquarter of the Intelligence Agency. What is happening? Aditya fled the scene with a sea serpent, Anderson was missing, and the task force of the Intelligence Agency struggled with the works of Anthony the Necromancer.


## Part 4: Like A Thief

### [Anderson's Failure](../Chapters/Xe-1%20Anderson's%20Failure.md).
First, comes the Flares.
Anderson came to [Steven](../Characters/Steven%20Pontirijaris.md) and [Daniel](../Characters/Daniel%20Lusien.md), warning them about the Sea Serpent that is aligned with Aditya.

### [Epiphaneia](../Chapters/Xe-1%20Epiphaneia.md).
Henokh and the rest of [The Nine Tailed Fox](Notes/KIA%20MTF%20Psi-72%20Nine%20Tailed%20Fox.md) came to pick Anderson.
Ashton came to Daniel and Steven, telling them about the second coming.

### [Grandpa is my classmate](../Chapters/Xe-1%20Grandpa%20Is%20My%20Classmate.md).
Ashton enrolled as [David](../Characters/David%20Pontirijaris.md)'s classmate, and is trying to be a part of his great grandson's life.
Ashton taught David about motorcycles, and a glimpse of his experiences in his previous high schools.

### [The Invisible Hero](../Chapters/Xe-1%20The%20Invisible%20Hero.md).
[Michelle](../Characters/Michelle%20Williams.md), and David.
David is eyeing an eccentric girl that appreciated his drawings.


## Part 5: 2022: The Battle of Kendari

### [The Archangels](../Chapters/Xe-1%20The%20Archangels.md).
It was a normal morning for Steven and Daniel in their peaceful clinic.
The [clerk](../Characters/Martha%20Williams.md) is late that day.
Who would've thought that dragons would flew on the sky of that particularly normal day, and hellhorses roamed about?
More importantly, the fleshy spiders surged in their clinic.
Wait, what?

### [The Four Horsemen](../Chapters/Xe-1%20The%20Four%20Horsemen.md).
Michelle had not been feeling well since this morning, until she threw a chair off the balcony.
And that wasn't the most surprising thing of the day, as the chair hit a dragon flying nearby, and it fell to the school field.
Panic ensued, and amidst chaos, Ashton asked [Zean](../Characters/Zean%20Lisander.md) and David to help, and Michelle is helping as well, evacuating as many schoolmates as possible from the premise, as more and more alien troops barged in.

### [The Bet](../Chapters/Xe-1%20The%20Bet.md).
Steven and Daniel discussed about which one to attack first, a suspicious submarine on the bay, or a fleshy floating island-sized whale on the open sea?
They bet the submarine first, until Aditya gets serious.

### [Victory is sweet, reunion is sweeter](../Chapters/Xe-1%20Victory%20Is%20Sweet.md).
Finally, Kuin take over the fight.
[The Four Horsemen](Notes/KIA%20MTF%20Gamma-42%20Four%20Horsemen.md) and [The Archangels](Notes/KIA%20MTF%20Alpha-19%20The%20Archangels.md) reunited with family.
Anderson came by, and asked them to join the Intelligence Agency as task forces.

### [Consolidation of the Intelligence Agency](../Chapters/Xe-1%20Consolidation%20of%20the%20Intelligence%20Agency.md).
A meeting is held among the higher ups of the Intelligence Agency, regarding their stance to the latest invasion, and about the so called Archangels and the Four Horsemen.
The Intelligence Agency concluded that it would help their cause, because public would be distracted from their moves.
Headline of the news that day marginalized the involvement of the Archangels and the Four Horsemen.

### [The Weakest Link of a Chain](../Chapters/Xe-1%20The%20Weakest%20Link%20of%20a%20Chain.md).
Aditya was upset, his plan was not executed perfectly.
He sets out an unnecessary plan to capture the weakest link to hurt Steven, that does not involve his [cousin Helena](../Characters/Helena%20Irawan.md): Daniel and David.
He'd do it both, so Steven would have to choose who to save first, between his best friend that he knew longer than his son, and his own son.
Even after saving Daniel, Steven is still faced with two choices: would he let Aditya kill David, or would Steven kill David with his own hands?
What would Steven choose?


## Part 6: Nightmares

### Problems of Our Parents.
After the previous fight, Steven and Aditya was terribly hurt.
It affects their sons, David (Steven's) and Anthony (Aditya's).
Michelle is close with both, and have to face with both of their pain.
Michelle came with the best idea she could came up with, since they're both sharing a similar pain, why don't she make them meet up, and share their burden?
I mean, what could went wrong?

### Dehumanizing a lady is never good.
The surprise triangular date that was set up by her ended up with both David and Steven fighting in the cinema.
It escalated as Michelle told them to stop, and they replied by bickering with one another about who's worthy of her love.
Upset for being objectified, she left the scene.
Ashton had both of them electrocuted, and lectured them while [Manov](../Characters/Romanov%20Dexter.md), [Purpose](../Characters/Purpose%20Coralline.md), and [Xiangyu](../Characters/Xiangyu%20Lee.md) cleaned the scene.

### My Friend is The Son of My Father's Enemy.
Realizing their mistake, they returned to Michelle's house, where Aditya and Anthony stayed for Aditya's recovery.
Getting to know each other, Anthony and David realized that their parents's problems shouldn't be theirs, as neither of them contribute to the problem anyway.
That night, Anthony had another nightmare.


## Part 7: The Battle of Medan

### 2025: The Fallen Angel.
In a plan to warn the goverments over the globe, EPL planned to shot down airplanes carrying important governmental officers.
One of the airplane was the one where Helena's a passenger.
Knowing that Helena's plane is important to be shot down for his plan, Aditya had to choose on whether or not to spare Helena's plane.

### The 2027 April Fool Insurgency: The Vengeance.
Terror Tyres, the latest work of EPL, to first induce curiosity, and terror at the same time. Tyres spread across the cityscape, making people crowd around it. Then it exploded. People that was withdrawn to it, now ran away from it. Terror of the entire city. It was a perfect plan to take over Medan, but Aditya decided to stop by Steven's store to avenge Steven, and his family.

### The 2027 April Fool Insurgency: The Four Horsemen's Rescue Mission.
David left Steven, Daniel, and Fernando so that he could assemble the Four Horsemen and save his brothers.
The fight went ugly until The House Angel showed up.
Done dealing with Aditya's men, it is time for the Four Horsemen to help with Steven and co.

### The 2027 April Fool Insurgency: The Esoteric Counter-Response.
At first considering it an april fool prank, they refused to respond, until the Terror Tyres arrived at the front gate.
G28 Forces deployed, and they tried to neutralize the threat.
However, esoteric forces were interfering as well.
So the Intelligence Agency sent their Esoteric Corp to the battle.

### The 2027 April Fool Insurgency: The Handover.
The Four Horsemen entered the scene, aiding the struggling Archangels.
The Archangels and The Four Horsemen finally able to have an upper hand against Aditya and his men.
An opening was available for Steven to kill Aditya, and end this bloody fight.
Or he could spare Aditya and make things right.
What would he choose?

### The Caged Beast.
Captured Aditya was taken to a hidden facility of the Intelligence Agency.
Finally Henokh had a chance to talk with Aditya again.
Henokh tried to convince Aditya to stop all of this silliness that begins with their fight.
Henokh said that there must be better ways to resolve all of this.
That until Aditya told Henokh, that the one Henokh trusted to do Henokh's "better ways" was the one planning to kill him in the first place: Anderson.
Henokh was enraged.


## Part 8: Out of Hands

### The Dragon Assisted Escape.
A squad of dragons led by Anthony barged into the facility and managed to bring Aditya away.
The MTF Nine Tailed Fox tried to halt the escape, and had Hendrik badly wounded because of that.

### Yam's Vacation.
[Yam]() was almost always quiet and obedient to [Hadad]() in doing most of his assignments.
One day, just that one day, he asked for a brief vacation on Earth.
His existence in the surface of the Earth interfered, indirectly, with the success of the MTF Nine Tailed Fox's attempt to track down Aditya and to stop whatever he was trying to pull.

### The Goddess of the Sea.
In a secluded, private beachside, under the pretense of shooting a horror movie, the EPL congregation gathered.
The sacrifice is prepared, a lady named [Irene Yvone](../Characters/Irene%20Tanuwinata.md), offering her body to summon the Goddess of the Primordial Chaos, [Tiamat]().
Dancing under the moonlight, and chanting from the congregation, led by Anthony's dance, marking the sigils into her body surface.
Followed by the union of the two, Tiamat was summoned, binded to Irene, ending with the scream of Anthony, and Tiamat franctically laughed, and jumped to the sea, nowhere to be found by them.
