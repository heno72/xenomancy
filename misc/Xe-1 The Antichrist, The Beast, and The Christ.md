# Xenomancy, Book 1: The Antichrist, The Beast, and The Christ


## Short Summary
This is a story that explores the insurgency attack of Aditya and its effects.

A clandestine operation is conducted to stop an upcoming insurgency.
However the insurgents managed to steal a divine artifact.
What do they plan with the artifact?

Steven and Daniel got an emergency visit by Anderson, and he told them about the upcoming invasion of the beasts.
Meanwhile Ashton came to inform them about the Second Coming.
What on Earth is happening?

## Notes
Alternative titles are:

- Xenomancy: The Second Coming
- Xenomancy: The Three Ways War
- Xenomancy: The Antichrist, The Beast, and The Christ
- Xenomancy: The Sea Serpent
- Xenomancy: Dawn

# Planned Chapter Structures

## Table
### Parts:
| Part | Chapters | Part Name | Description |
| ---: | -------: | :-------- | :---------- |
| 1 | 6 | The First Trigger | How the separation of Aditya and Henokh started a chain reaction to reveal the hidden world of esotericism and alien involvements. |
| 2 | 3 | The Divine Matters | How the Powers of Earth and their influences affect the life of mortals. |
| 3 | 3 | The Commotion | The fall of the Anderson's Cluster and the start of the Ashton's Cluster. |
| 4 | 4 | Like A Thief | The activation sequence of Ashton's Cluster: The Four Horsemen. |
| 5 | 6 | 2022: The Battle of Kendari | Ashton's Cluster mitigating the denefasan invasion. |
| 6 | 3 | Nightmares | Michelle, David and Anthony on their quest of understanding friendship and relationship. |
| 7 | 6 | The Battle of Medan | Major battles between Anderson's Cluster, Ashton's Cluster, and Aditya's Cluster. |
| 8 | 3 | Out of Hands | Aditya's Cluster gaining an upper hand. |

### Part 1: The First Trigger

Split of Yam and Hadad.

| Chapter | Title | Description |
| ------: | :------------ | :---------- |
| 1 | [**Polar Opposites**](../Chapters/Xe-1%20Polar%20Opposites.md) | [Aditya](../Characters/Aditya%20Wijaya.md) and [Henokh](../Characters/Henokh%20Lisander.md) were best friends, until they separate into their own ways. |
| 2 | [**The Boy from The Woods**](../Chapters/Xe-1%20The%20Boy%20From%20The%20Woods.md) | Aditya found an esoteric [lost boy in the woods](../Characters/Anthony%20Matthias.md), that led his life to an esoteric organization. |
| 3 | [**Incursion**](../Chapters/Xe-1%20Incursion.md) | The founding of an Intelligence Agency, and Henokh's quest to find Aditya again. |
| 4 | [**The Silent Concert**](../Chapters/Xe-1%20The%20Silent%20Concert.md) | The assassination  of Dominic Muerte and its consequence to the Paramundus Jagadpadang. |
| 5 | [**The ORCA-strated Event**](../Chapters/Xe-1%20The%20ORCA-strated%20Event.md) | The battle of the Barongs and the Tengus at the real world and its aftermath. |
| 6 | [**The Esoteric Business**](../Chapters/Xe-1%20The%20Esoteric%20Business.md) | The founding of the fifth division of the Intelligence Agency. |

### Part 2: The Divine Matters

Negotiation of Yam and Kav.

| Chapter | Title | Description |
| ------: | :------------ | :---------- |
| 7 | [**Overtime**](../Chapters/Xe-1%20Overtime.md) | Encounter of Cal and Anthony. Cal set a goal to find El's whereabouts. |
| 8 | [**Facing The Forking Path**](../Chapters/Xe-1%20Facing%20The%20Forking%20Path.md) | Cal stayed for too long by Anthony's side that things went against his wishes |
| 9 | [**The Lost Son**](../Chapters/Xe-1%20The%20Lost%20Son.md) | Cal decided to end his life for a new life as a normal human. |
| 10 | [**Close Encounter**](../Chapters/Xe-1%20Close%20Encounter.md) | Ostaupixtrilis' journey to get his new identity. In-story date for The Birth of Charles Lee is J906 0200 |
| 11 | [Messianic Arrival](../Chapters/Xe-1%20Messianic%20Arrival.md) | Os came to Earth, but found that Charles Lee was not available for merger. Something changes, as Cal stayed too long with Anthony. Therefore the old Plan is comrpomised. A backup Plan is set into run. Therefore, see The Angel's Job. Plan A is Hadad's Plan; Plan B is Yam's Plan; Plan C is Kav's Plan. *Note:* [Charles Lee](../Characters/Daniel%20Ashton.md) is content with his life, but Ostaupxitrilis Pontirijaris (Os) that claimed to be him bugged him in his dreams. |
| 12 | [An Offer in a Vacation]() | Charles Lee found his peace, but Ostaupixtrilis Pontirijaris gave him an offer: a mission to humanize humans. *Note:* Os told Charles of a mission, to save humanity, to make humans human. Charles wanted peace, mentioning that Anderson is doing the job well, and they don't need him anymore, as all that he had to be done had been done already. |
| 13 | [The Angel's job](../Chapters/Xe-1%20Angel's%20Job.md) | Two angels, two different orders. Hadad intercepted Yam's Plan, or what he thought was Yam's Plan, that is to prevent the resurrection of Tiamat. This is required to deplete Hendrik's quota. Therefore Yam could successfully obtain the Black.s Staff. By the end, Anthony meets Alt, the answer Cal and Anthony was looking for. |

### Part 3: The Commotion

Kav changes his side to Yam, as Hadad isn't cooperating.
It is also where the A cluster failed.

| Chapter | Title | Description |
| ------: | :------------ | :---------- |
| 14 | 2021: [The Artifact Hunt](../Chapters/Xe-1%20The%20Artifact%20Hunt.md) | What the EPL was planning to do? |
| 15 | [The Breach]() | The quest to protect the Black's Staff. Breach into Securion Facilities. (Originally: [The Raid](../Chapters/The%20Raid.md)) |
| 16 | [The Mass]() | Investigation on Edward Kevlar resulted in knowledge about the mass. |
| 17 | The Umbra Cahaya | An attempt to contact Anthony through Ernie revealed the connection of Anthony and Manov, while Anderson discovered the interference of the Powers on Earth. |
| 18 | [The Birth of Daniel Ashton](../Chapters/Xe-1%20The%20Birth%20of%20Daniel%20Ashton.md) | Charles's meeting with Fernando and his conversation with Tee, triggered his decision to accept Os's offer. |
| 19 | 2021: The Dawn Strike | Anderson played out his plan to end the Godfluenza vectors at once, but was disturbed by Aditya's cluster. |

### Part 4: Like A Thief

Activation of C Cluster, as Plan A is no longer relevant.

| Chapter | Title | Description |
| ------: | :------------ | :---------- |
| 20 | [Anderson's Failure](../Chapters/Xe-1%20Anderson's%20Failure.md) | Anderson visited Steven and Daniel, to warn Ashton's Cluster about his failure. |
| 21 | [Epiphaneia](../Chapters/Xe-1%20Epiphaneia.md) | The rest of the Nine Tailed Fox task force came to pick Anderson, followed with a visitation by Ashton. |
| 22 | [Grandpa is my classmate](../Chapters/Xe-1%20Grandpa%20Is%20My%20Classmate.md) | Ashton's quest on being a part of his great grandson's life. This is where Ashton said to David that he is going to manipulate David. |
| 23 | [The Invisible Hero](../Chapters/Xe-1%20The%20Invisible%20Hero.md) | David's love story on his encounter with a peculiar woman. David loves to draw, but was too shy to show it. Michelle helped him to appreciate himself more. |

### Part 5: 2022: The Battle of Kendari

Yam's first attempt to distract Hadad.

| Chapter | Title | Description |
| ------: | :------------ | :---------- |
| 24 | [The Archangels](../Chapters/Xe-1%20The%20Archangels.md) | Steven and Daniel on facing the Denefasan invasion. |
| 25 | [The Four Horsemen](../Chapters/Xe-1%20The%20Four%20Horsemen.md) | Ashton's primary team on facing the Denefasan invasion. |
| 26 | [The Bet](../Chapters/Xe-1%20The%20Bet.md) | Steven and Daniel on deciding which one to attack first: a suspicious submarine on the bay or a fleshy floating island-sized whale on the open sea? |
| 27 | [Victory is sweet, reunion is sweeter](../Chapters/Xe-1%20Victory%20Is%20Sweet.md) | The Intelligence Agency finally responded, while Ashton's cluster take a rest. |
| 28 | [Consolidation of the Intelligence Agency](../Chapters/Xe-1%20Consolidation%20of%20the%20Intelligence%20Agency.md) | Aftermath of the invasion from the perspective of the Intelligence Agency |
| 29 | [The Weakest Link of a Chain](../Chapters/Xe-1%20The%20Weakest%20Link%20of%20a%20Chain.md) | Aditya's scheme to hurt Steven, by making him choose between his best friend or his first son, followed by a choice to let his son be killed by Aditya, or by his own hands. |

### Part 6: Nightmares

A part of Plan C is to open a backdoor to B Cluster, so C Cluster can exploit it.

| Chapter | Title | Description |
| ------: | :------------ | :---------- |
| 30 | Problems of Our Parents | Recovery of Steven, David, and Anthony after the events of the previous chapter. |
| 31 | Dehumanizing a lady is never good | Michelle's attempt to heal both David and Anthony, that ends with a fight to determine which is best for Michelle. |
| 32 | My Friend is The Son of My Father's Enemy | Michelle was mad because she was objectified, that results in the development of a friendship between Anthony and David. |

### Part 7: The Battle of Medan

Yam's second attempt to distract Hadad.

| Chapter | Title | Description |
| ------: | :------------ | :---------- |
| 33 | 2025: The Fallen Angel | To fulfill Tiamat's request of global terror, Aditya had to make a decision on whether or not to sacrifice his cousin that was in a flight intended to be shot down. |
| 34 | The 2027 April Fool Insurgency: The Vengeance | Terror tyres spread terrors to the entire Medan, followed by EPL's take over, and Aditya's plan to avenge Steven. |
| 35 | The 2027 April Fool Insurgency: The Four Horsemen's Rescue Mission | The Four Horsemen's quest to help David's brothers from the EPL. |
| 36 | The 2027 April Fool Insurgency: The Esoteric Counter-Response | G28 Forces was helpless against the enemy's esoteric forces, until the Intelligence Agency's Fifth Division came to help. |
| 37 | The 2027 April Fool Insurgency: The Handover | The struggling Archangels got a help from the Four Horsemen, that led to the defeat of Aditya's Cluster, and Steven's decision on whether or not to avenge Aditya. |
| 38 | The Caged Beast | An apparent defeat of Aditya's cluster, and Aditya spilled Anderson's secret assassination plan. The stage is in a ship, a [SEC-0715 instance](https://xenomancy.id/wiki/SEC/SEC-0715). |

### Part 8: Out of Hands

Yam's final move to obtain Tiamat.

| Chapter | Title | Description |
| ------: | :------------ | :---------- |
| 39 | The Dragon Assisted Escape | Anthony's quest to save Aditya that ends with Hendrik getting terribly hurt. Hendrik chased with a jet (Ein's power). Anthony stopped him with a wall of water. |
| 40 | Will you still like me tomorrow? | Anthony felt guilty, he apologized to Manov in his bar, but Manov said he needs time. Anthony meets the Four Horsemen, and he cried in Ashton's shoulders. |
| 41 | Words of the wind | An information about the ritual leaked to the Four Horsemen, transmitted to A Cluster from Zean. From Zean's information, A Cluster planned to intercept the ritual. |
| 42 | Yam's Vacation | Yam was having a vacation, that interfered with Anderson's cluster attempt to halt Aditya's cluster. |
| 43 | The Goddess of the Sea | Anthony's ritual to summon Tiamat went awry. |
