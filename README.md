# Readme

Project Xenomancy is not a story.
It is a setting, a world to build upon, and plenty of settings for various stories.
Xenomancy is a fictional world with strong leaning of scientific accuracy.
Its genre is a softer hard fiction, not as hard as diamond, but sufficiently hard as the creator could comprehend.

This is the project home for the Xenomancy Universe Project.

[TOC]

## Xenomancy: We are sorry for the inconveniences

Currently, we are still developing the first book: ```Xenomancy: We are sorry for the inconveniences```.
Below we will list semifinal drafts of chapters:

| Chapter | Title |
| ------: | :---- |
| 1 | [**Polar Opposites**](Chapters/Xe-1%20Polar%20Opposites.md) |
| 2 | [**The Boy from The Woods**](Chapters/Xe-1%20The%20Boy%20From%20The%20Woods.md) |
| 3 | [**Incursion**](Chapters/Xe-1%20Incursion.md) |
| 4 | [**The Silent Concert**](Chapters/Xe-1%20The%20Silent%20Concert.md) |
| 5 | [**The ORCA-strated Event**](Chapters/Xe-1%20The%20ORCA-strated%20Event.md) |
| 6 | [**The Esoteric Business**](Chapters/Xe-1%20The%20Esoteric%20Business.md) |
| 7 | [**Overtime**](Chapters/Xe-1%20Overtime.md) |
| 8 | [**Facing The Forking Path**](Chapters/Xe-1%20Facing%20The%20Forking%20Path.md) |
| 9 | [**The Lost Son**](Chapters/Xe-1%20The%20Lost%20Son.md) |
| 10 | [**Close Encounter**](Chapters/Xe-1%20Close%20Encounter.md) |

## Xenomancy Book 2

Will explore the next stage of the story, where the main casts are exploring an alternative version of Earth to search for Toru El.

## Xenomancy Book 3

Will explore the last stage of the entire Xenomancy arc.
The main focus would be attempts by main casts to fight Powers on Earth.

