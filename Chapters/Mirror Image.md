# Mirror Image

The moment they left the GaFE base, that is currently disguised as an old DVD rental store, they're on their own.
The first thing they do is to try to discover a proper ID to fit in.
The next thing they would need are vehicles, and Manov attempted to fulfil their requirements.
They discovered that Henokh has no counterpart in this world, and a quick search to his family, discovered that his mother is not married, and his father marry another woman, and had a son named Sylvester Lisander.
Likewise, Derictor has no counterpart as well.
Daniel had a counterpart, but the family is poor.
Steven, being half etoan, is not even exist here.
Fernando had a counterpart, and his house is nearby.
Manov pulled the ID-s from the Fiat Ring, and also their phones.
Hendrik's ID and phone for Manov, Sylvester's for Henokh, Daniel's for Daniel, and Fernando's for Fernando.
Derictor and Steven are without IDs.
However, out of mercy, Daniel requested Manov to return his counterpart's phone, saying that it is far more valuable than his counterpart's wallet and its content.
So they decided to go to the house of Fernando's counterpart, that appears to be not present, and took his car.
It turns out the house possess a working internet connection, so they take the time to browse and familiarize themselves to the world.
Daniel looked for El, but instead found a version of Hendrik in this world, that is a writer, that writes about their adventure, in the real world!
Meanwhile, Manov, Henokh, and Derictor managed to pinpoint several names that are present in the list.
They consulted on how would they do this.
First of all, for Steven and Co., finding El is their utmost objective, while for Henokh and Co., finding information on why people listed on the list are killed, is their utmost objective.
Besides, it is very weird for Manov to try and locate Hendrik, since he shares the same face as Hendrik.
So it is decided, they are going to be split into two teams: Team 1 seeks Hendrik, and find a way to use him to find El; Team 2 is to find and secure people listed on The List.
