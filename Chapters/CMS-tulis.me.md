---
title: Cenayang di Malam yang Sunyi
## Fonts and formatting.
fontfamily: times
fontsize: 12pt
linestretch: 1.5
indent: true
papersize: a4
---

Semua dimulai dengan sebuah pusaka.
Pusaka yang saking saktinya dapat membuka gerbang neraka.
Pusaka yang telah ada di dunia sejak Kain dikutuk karena telah membunuh Habel.

Malam itu dingin, dan langit cerah.
Rick melihat ke langit sembari menghembuskan nafasnya.
Jutaan matahari yang jauh di seberang jagad raya memandang balik padanya.
Ia tersenyum, langkahnya bertambah ringan.

Hari ini tidak ada Manov, kembarannya yang usil itu sibuk berperang di jagad seberang.
Tidak ada pula Ein, kembarannya yang berhati dingin itu sibuk berperang gaib di Kawah Ijen.
Rick sedang sendiri, dan dia menikmatinya, hampir.

Siang hari dirinya dikenal sebagai Rick si pengacara.
Malam hari dirinya dikenal sebagai Sukarno, anggota Satuan Tugas Rubah Ekor Sembilan.
Pada saat pulang rumah, dia harus berhadapan dengan dua kembarannya, Manov si Hantu, dan Ein si Malaikat.

Hanya saat-saat seperti inilah ia bisa menarik nafas sesaat.
Berjalan sendirian di malam hari, ditemani bintang di langit dan trotoar dingin.
Ia merasa bisa menjadi dirinya.
Jika saja ia tidak memiliki indera keenam, semua akan lebih sempurna, pikirnya.
Menutup mata, menutup telinga, satupun pun tidak ada gunanya.

Ia mengerutkan alisnya, matanya tertutup rapat, dan ia bergeming di trotoar.

"Aku mau pulang," kata Rick.
Pada saat itu, gawainya berdering.
Lalu berdering.
Dan berdering lagi.
Dengan satu helaan nafas, ia mengambil gawainya dari saku celana.
"Ya, Sukarno di sini."

"Enam di sini.
Klandestin dapat info, salah satu pegawai Kukercorp adalah anggota Laskar Umbra Cahaya.
Cari tahu apa target mereka."

"Tidak bisakah besok saja?
Saya lelah."

"Hari ini kesempatan terakhir.
Besok komputer-komputer di kantor sana akan dibersihkan.
Kau ada bawa *K-Drive*?"

Rick tersenyum, *K-Drive* adalah alat khusus berbentuk *flash disk* yang dapat mengekstrak data dari komputer yang dihubungkan dengannya.
Alat itu, tidak ada sama Rick saat itu.

"Sayangnya tidak," jawab Rick.

"Sudah kuduga," kata Enam, "mobil telah dikirim ke tempatmu, di dalamnya ada *K-Drive* dan alat-alat lain yang mungkin kau perlukan. *Good luck*!"

Panggilan terputus.

"Sial, tidak bisakah saya istirahat sehari saja?"

Indera keenamnya memperlihatkan sebuah mobil mendekat.
Mobil otomatis tanpa pengemudi, yang sebentar lagi akan masuk dalam pandangannya setelah berbelok dari perempatan di ujung komplek.
Di dalamnya terdapat sebuah tas perlengkapan berisi dua *soargun*, empat borgol semi-otomatis, empat *ornithopter* mini seukuran bola golf, sebuah kacamata pintar, dan sebuah flash disk, *K-Drive*.

Kacamata pintar dikenakannya, dan kedua *soargun* digantung di punggungnya.
Tas tersebut ia letakkan di kursi penumpang.
Setelah selesai dengan persiapannya, ia duduk di kursi pengemudi.
Mobil tersebut bergerak sendiri.

Tangannya ia lambaikan di udara, dan sebuah mandala dikerangkeng dalam sebuah cincin redup, terdiri dari kerlap kerlip seperti debu di udara terbentuk.
Pada bagian bawah cincin redup, terdapat kerlap kerlip kuning terang, hanya sekitar lima derajat lebarnya.

"Rendah sekali, pasti kerjaan si Ein," Rick menepuk jidatnya.

Ein selalu bertarung habis-habisan.
Semua kuota bersama mereka hari itu pasti telah dihabiskan oleh Ein di Kawah Ijen.
Ia berharap Ein sedikit lebih berhemat.

Mobil ini telah berjalan dari tadi, dan kini telah tiba di tujuannya, tepat di samping sebuah gedung.
Tidak ada jendela di sisi gedung itu, hanya ada tembok beton yang bahkan belum dicat.
Rick merenggangkan tangannya, dan pundaknya.
Hatinya terasa kecut saat ia mengingat keborosan Ein.

"Mesti berhemat," gumamnya.

Rick keluar dari mobil, memasang tas perlengkapan ke sabuknya, dan memandangi dinding beton tidak bercat tersebut.
Ia menghitung, ruang server ada di lantai tiga, antara sembilan dan dua belas meter ke atas.
Satu tarikan nafas, dan satu loncatan kemudian, ia telah berada di udara.
Segera ia sentuh tembok beton di depannya dengan kedua tangannya, kakinya lalu ikut menempel.

"Gaya cicak," Rick terkekeh, "seharusnya ini tidak makan banyak kuota."

Dinding itu dingin, agak lembab.
Permukaannya terasa padat dan kokoh, agak kasar.
Dirasakannya jutaan benang karbon tak kasat mata yang bekerja sama menjembatani permukaan kulit telapak tangannya, sol sepatunya, dan dinding tersebut.

Tangan kirinya ia ayunkan di udara, dan sekelompok benang-benang karbon yang melayang di udara tersusun sambil menyala.
Meteran mandala-nya pun muncul.
Bagian yang berpendar kuning terang belum berkurang sama sekali.

Senyumnya merekah, tangan kirinya ia tempelkan kembali ke dinding.
Menutup mata, ia pun berkonsentrasi.
Benang-benang karbon tersebut ada di mana-mana.
Di mata batin Rick, ia bisa melihat dan merasakan apapun yang dapat dilihat dan dirasakan mereka.
Ia juga dapat memerintahkan mereka, selama kuotanya tidak habis.

Ia berfokus ke satu benang.
Satu benang itu menyentuh benang lainnya, dan kedua benang itu menyentuh benang-benang lainnya.
Tidak butuh waktu, milyaran benang karbon berada dalam kendali Rick.

Satu per sepuluh ribu dari seluruh massa di permukaan bumi adalah benang-benang tersebut.
Setengah kilometer ke atas dan ke bawah permukaan bumi.
Tidak ada satupun yang luput dari benang-benang tersebut.

Ia berkonsentrasi ke benang-benang yang ada dalam beton tersebut.
Benang tersebut terkonsentrasi ke benda yang lebih padat, sehingga Rick dapat membedakan logam dari beton.
Beton berkerangka logam umumnya digunakan untuk bagian struktural gedung, bagian-bagian yang perlu diperkuat agar tidak mudah rusak.

Ia mencari bagian tembok yang tidak banyak logam tertanam di sana.
Bagian-bagian non-struktural yang walaupun dibongkar, tidak akan meruntuhkan gedung.
Setelah ia dapati bagian yang sesuai, benang-benang itu mulai saling mendorong.

Tembok tersebut runtuh ke dalam membentuk terowongan silinder.
Kerikil-kerikil masih terikat dengan benang-benang karbon.
Rick berencana mengembalikan tembok tersebut seperti semula nantinya.
Tapi ia tidak dapat terus berkonsentrasi ke tembok ini selama ia menjalankan misi.
Harus ada cara lain.

Setiap orang terlahir di dunia dengan kembaran gaib mereka.
Bentuk semu dari tiap orang, yang ada karena distribusi benang-benang karbon.
Menyelinap dalam setiap sentimeter kubik darah, daging, dan tulang.
Mereka mengumpulkan informasi, yang umumnya disebut sebagai jiwa.
Orang-orang seperti Rick, dapat berkomunikasi dengan benang-benang tersebut melalui jiwa mereka.
Jiwa Rick membelah, dan ia perintahkan untuk mempertahankan bentuk terowongan tembok tadi.

"Aduh, membelah jiwa itu mahal," Rick menepuk jidat.

Segera ia mengecek meteran mandalanya.
Kilap kuning terang berkurang sekitar satu derajat.
Resolusi indera keenamnya mulai berkurang.
Semuanya bagai kabut, sebagian agak padat, sebagian lowong.
Ia tidak dapat membedakan lagi antara tembok, beberapa furnitur, dan gagang pintu.
Untuk pertama kalinya setelah sekian lama, ia harus mengandalkan indera lahiriahnya.

Tangannya ia bentuk seperti sedang memegang bola imajiner, "*fiat lux orbis*," dan sebuah bola cahaya putih terbentuk, melayang di tengah-tengah bola imajiner tersebut.

Ia berada di dapur.
Pintu ada di samping tempat cuci piring.
Satu tangannya memegang bola imajiner yang berisikan cahaya tersebut, sementara tangan satunya membuka gagang pintu.

Koridor.
Panjang, gelap, dan pintu-pintu berjejeran.
Rick melempar pelan bola imajinernya ke depan.
Cahaya tersebut melayang dan terus melaju ke depan.
Rick berjalan dituntun bola cahaya tersebut.
Ia tiba di suatu ruangan kantor.
Tersekat-sekat menjadi kubus-kubus meja kerja pribadi para pegawai.

Kacamata pintarnya dilengkapi mode penglihatan malam.
Seketika Rick merasa betapa *kurang pintarnya* dia tidak mengaktifkan fitur tersebut sedari tadi.
"Sepintar-pintarnya suatu gawai, penggunanya juga harus pintar," gumam Rick.
Ia mulai menjelajahi kantor tersebut.
Belum lewat lima menit Rick menjelajah, ia menyadari lagi kurang pintarnya dirinya.
Ruangan itu dilengkapi kamera pengawas.

"*Binatang*," umpatnya, diikuti dengan "*melata*, adalah binatang yang berjalan dengan perutnya menempel di tanah."

Mata manusia hanya dapat melihat cahaya sampai dengan panjang gelombang tujuh ratusan nanometer, warna merah.
Kamera digital dapat melihat cahaya dengan panjang gelombang maksimal sekitar seribu nanometer, *near-infrared.*
Kamera profesional dilengkapi dengan filter inframerah untuk meningkatkan kualitas gambar.

Kamera pengawas, di sisi lain, tidak mempedulikan resolusi atau kualitas gambar.
Beberapa bahkan dikhususkan dengan LED inframerah sebagai penerangan.
Di tempat tergelap sekalipun, mereka dapat melihat dengan jelas.
Itu, adalah titik lemah yang Rick dapat manfaatkan dalam kondisi ini.
Segera, ia perintahkan benang-benang karbon yang mengelilinginya untuk berpendar cerah dalam inframerah.
Bagi kamera inframerah di gedung tersebut, ia hanya titik cahaya terang yang tidak berbentuk.
Rick tersadar, kamera cerdasnya memakai kamera inframerah untuk fungsi penglihatan malamnya.
Pendaran cahaya yang barusan dibuatnya untuk mengelabui kamera pengawas di gedung, juga mengelabui kacamata cerdasnya.

Rick hanya dapat terkekeh.
Ia melepas kacamatanya, dan melirik bola cahaya yang tadi dibuatnya.
Bola cahaya itu kembali menjadi temannya dalam gelap.
Satu komputer dalam mode *standby*, tidak dipasangi kata kunci.
Rick mencolok *K-Drive* ke salah satu port dan data dari komputer mulai diunduh.

Dari lubuk hatinya yang terdalam, sempat terbersit.
Misi ini lebih seru kalau ada musuh yang datang, dan harus Rick lawan.
Ia hanya terkekeh, dirinya terlalu sering menonton film aksi espionase.

Ada yang tidak beres.
Ada sinyal yang diterima oleh jiwa Rick, dan disampaikan ke lobus parietal belahan kanan otak Rick.
Area Broca umumnya ada di otak kiri, dikenal sebagai pusat bahasa.
Pada orang-orang seperti Rick, selain area Broca di sebelah kiri, ada juga kluster Reflexius di sebelah kanan.
Yang satu berfungsi untuk berbicara, yang satunya lagi menjembatani diri mereka dan jiwa mereka.

Terdapat pemindahan tiba-tiba benang-benang karbon tak kasat mata di udara tepat di lantai bawah.
Rick merasakan beberapa gumpalan kabut menyerupai tiang.
Seratus tujuh puluh sentimeter, diameter tidak lebih dari satu meter.
Mereka bergerak ke tangga menuju lantai tiga.
Rick tidak berkata apa-apa, dirinya tahu ia harus segera mematikan bola cahayanya.
Badannya kaku, bergeming.

Tiang-tiang itu tiba di lantai tiga.
Meter demi meter, jarak antara tiang-tiang itu dan Rick semakin dekat.
Satu mendekati pintu, Rick berseru, setengah berbisik, "*fiat umbra*."
Bola cahaya yang dibuatnya menjadi bola kabut hitam.
Seharusnya ia mengucapkan mantera *fiat nil lux*, yang akan menghilangkan bola tersebut.

Rick tunduk bersembunyi di salah satu meja kerja.
Pintu terbuka.
Perlahan, ia matikan layar komputer yang tadi dibukanya.
Lampu *K-Drive* berkelap-kelip, tanda ia sedang bekerja.
Butuh sepuluh menit lagi untuk selesai menyalin data.

Tiang-tiang tersebut menyebar dan menjelajahi ruangan.
Rick mulai merasakan bentuk kabut-kabut berbentuk tiang tersebut.
Memiliki tangan dan kaki, sepertinya orang.
Ada enam hingga delapan, masing-masing memegang suatu objek metal, bengkok.
Pistol.

Meteran mandalanya menunjukkan kuotanya tidak cukup untuk mengalahkan enam hingga delapan individual bersenjata.
Masih ada delapan menit sampai selesai mengunduh data.
Ia harus mengulur waktu.

Empat *ornithopter* di tasnya, ia ambil dan melemparkannya ke empat pojok ruangan.
Keempatnya teraktivasi, sayap-sayap plastik mereka mengepak bising.
Kacamata cerdas Rick dijalankan, menampilkan tampilan kamera *ornithopter*.
Ia dapat melihat delapan orang.

Mereka mengamati balik ke empat *ornithopter* yang tiba-tiba muncul itu.
Berbisik-bisik di antara mereka, salah satu dari mereka memberi komando.
Mereka menyebar di kantor, baris demi baris meja mereka sisiri.

Dua orang dari mereka berdiam di salah satu komputer, mereka juga sedang menyalin data.
Salah satu dari mereka melihat sebuah bola kabut yang melayang di udara.
Ia meneriakkan sesuatu ke kawan-kawannya, semua melihat ke bola kabut itu.

*Ini kesempatanku* pikir Rick, "*FIAT LUX MAXIMUS!*"

Matahari mini bersinar cerah di ruangan tersebut.
Kedelapan orang tersebut terbutakan terang.
Kacamata cerdas Rick menggelapkan kacanya.
Dua orang terdekat dari Rick, ia tumbangkan.
Beberapa gerakan gesit kemudian, tangan mereka terborgol.

"Sisa enam," gumam Rick.

Kontak tembaga dari perangkat mereka yang menempel di komputer, dapat Rick rasakan setelah berkonsentrasi.
Satu perintah mental, perangkat mereka terpental dari komputer.
Mereka melihat ke arah Rick, mata mereka sudah beradaptasi.
Enam pistol diarahkan ke Rick.

Tembakan.
Rick melompat, menghindar.
Film-film aksi tidak menampilkan suara tembakan dengan akurat.
Suara tembakan asli terdengar seperti udara ikut tersobek.
Terkoyak tajam, memekakkan telinga.
Cukup untuk menutupi rasa sakit dari goresan peluru di pundak Rick.

"Ein, sebagai anggota Ikrar Trinitas," rapal Rick, "pinjamkan saya kekuatanmu!"
Tato api hitam menyebar dari pelipis kanannya ke dahi dan dagunya.

Para musuh mengokang, deretan tembakan kedua mulai terdengar.
Rick menghindar, dan mengayunkan tangannya.
Ein si Malaikat adalah seorang *pyromancer*, pembisik api.
Filamen-filamen yang terbuat dari boron-karbon-nitrida mulai berkumpul dan berpendar biru.
Mereka menyerap kelembaban dari udara, memecahnya menjadi oksigen dan hidrogen.

Digaetnya beberapa filamen tersebut, satu filamen dipantik.
Filamen-filamen lain ikut terpantik menjadi bola-bola api, dan meluncur ke enam orang tadi.
Seorang terkena, dan berteriak melengking bak anak gadis.

Kemampuan Ein ternyata cukup hemat.
Meteran mandalanya tidak menunjukkan kuota berkurang signifikan.
Lebih banyak bola api meluncur ke musuh-musuh yang lain.
Meja-meja sekitar mereka mulai terbakar.

Serangan api mungkin tidak bijak dilakukan dalam ruangan tertutup.
Rick sudah membakar beberapa meja, dan kursi.
Seseorang menderita luka bakar.
*Sprayer* air pemadam api menyala.

Ruangan itu diterangi matahari mini, dan terguyur hujan buatan.
Air merupakan musuh dari perlengkapan elektronik.
Khususnya yang tidak dirancang untuk di luar ruangan.

"Oh, komputer," kata Rick, "*Fiat arx!*"
Jendela-jendela pecah menjadi partikel-partikel silikat.
Mereka tersusun ulang menjadi lapisan kaca pelindung di sekeliling komputer Rick.

"Oh, *gob*-," umpat Rick, "mereka jadi tahu kan komputer mana yang saya pakai."

Tiga orang mendekati komputer berlapis perisai kaca, tiga lainnya mencoba menyudutkan Rick.
Rick memeriksa sekilas meteran mandalanya.
Cukup untuk melumpuhkan dua pistol.
Nyaris tidak cukup untuk mempertahankan belahan jiwanya di dapur tadi.

"*Sayonara*, belahan jiwaku," gumam Rick.

Dua pistol yang diarahkan ke kepalanya, dengan satu perintah mental dari Rick, laras mereka membengkok ke atas.
Benang-benang karbon tak kasat mata melayangkan kedua *soargun* di punggungnya ke tangan.
Ia arahkan satu ke seorang berpistol utuh, Rick menarik laras.
Satu bersit petir keluar dari laras *soargun*, orang tersebut dikejut listrik.

*Soargun*, *Securion Aluminum-projectile Rail Gun* adalah senjata dengan dua konfigurasi.
Konfigurasi railgun menembakkan aluminium berkecepatan tinggi.
Konfigurasi elektrolaser mengalirkan aliran listrik voltase tinggi melalui kolom plasma.

Rick bersyukur *soargun* tersebut tidak diset ke mode railgun.
Bukan niatnya membunuh orang malam ini.
Seorang lagi mendekatinya, segera saja terkapar dengan sebersit petir dari laras Rick.
Sisa dua pistol, dan Rick berpikir cepat.

"Manov, sebagai anggota Ikrar Trinitas," rapal Rick, "pinjamkan saya kekuatanmu!"
Tato api hitam memudar dari sisi kanan wajah Rick, dan tato lain muncul dari pelipis kirinya.
Tiga pola cincin konsentrik terbuka, dari padanya tersebar bilah-bilah pedang.
Tiga memanjang ke dahinya, tiga lainnya ke dagunya.

Satu orang melepas tembakan, bersamaan dengan *soargun* kedua yang diset ke mode railgun.
Timah panas dari pistol ditabrak proyektil aluminum *soargun*, sisanya terpantul balik ke arah penembak pertama.
Pecahan timah dan aluminium panas menembus tangan dan wajah penembak.
Sebagian kecil pecahan timah panas masih terus meluncur ke arah Rick.
Jaring-jaring karbon memadat di depan pecahan-pecahan tersebut, mementalkan pecahan-pecahan tersebut ke samping.

Rick dapat melakukan semua tersebut dalam sekejap mata karena kemampuan Manov, *Jangkar Waktu*.
Manov, teknisnya adalah belahan jiwanya yang telah sepenuhnya terpisah dan mandiri.
Karenanya, Manov tidak memiliki raga, dan pikirannya menjadi lebih fleksibel.
Ia hidup sebagai koneksi-koneksi di antara benang-benang karbon hanya sebagai informasi, atau *infomorph*, hantu.

Sebagai *infomorph* yang tidak dibatasi dengan raga, dan otak biologis, ia mampu merekrut benang-benang karbon untuk digabung dalam pikirannya.
Semakin banyak yang direkrut, pikirannya semakin cepat.

Namun Rick adalah manusia dengan raga.
Semakin cepatnya pikirannya, dunia lambat, bahkan seperti berhenti.
Raganya ikut terhenti.
Tapi tidak berarti ia tidak dapat mengendalikan kenyataan dalam keadaan ini.

Satu pecahan timah panas luput dari perhatian Rick.
Saat Rick sadar, pecahan itu hampir menyentuh perisai kaca di komputer Rick.
Seketika, ia mengecek meteran mandalanya.
Kuotanya hampir habis, dengan cepat.
Jangkar Waktu sangat boros kuota.

Ia tidak punya cukup kuota untuk menghentikan pecahan terakhir tersebut.
Tapi masih cukup untuk melepaskan *K-Drive* dari port komputer.
*K-Drive* tidak boleh rusak ketika komputer diguyur hujan dari pemadam api.

Benang-benang karbon di *K-Drive* dan port komputer saling mendorong kuat.
Di detik-detik terakhir dalam Jangkar Waktu, Rick menyempatkan diri menikmati butiran-butiran air yang terhenti di udara, pecahan aluminium dan timah panas yang sedang merembes ke kulit musuh, dan retakan yang menyebar di perisai kaca komputer Rick.
Jangkar Waktu terhenti.
Meteran mandala menguap.
Dan dunia sekejap sunyi bagi Rick.

Perisai kaca yang pecah, dan letupan komputer yang terguyur air, tidak Rick pedulikan lagi.
Orang di depannya teriak, kehilangan satu jari dan wajah ditanami pecahan aluminium dan timah panas.
Rick hanya peduli dengan *K-Drive*-nya, dan kedipan meteran mandalanya yang kurang dari satu derajat lebarnya.
*K-Drive* melayang ke tangannya, sisa kuota Rick ia simpan untuk aksi terakhirnya malam itu.

Musuh panik, mengurusi kawan mereka yang terluka, empat orang terkapar.

Sebelum kuotanya benar-benar habis, ia merapalkan, "*fiat nil lux*."
Matahari mini tadi menguap.
Gelap yang tiba-tiba membutakan para musuh, mata mereka belum terbiasa dengan gelap.
Kacamata cerdas Rick mengaktifkan mode penglihatan malam.

Di dapur tempatnya masuk, terowongan yang dibuatnya tadi runtuh jadi kerikil-kerikil beton.
Tersisa sebuah lubang menganga di dinding.
Ia melompat dari lubang di dinding tersebut, sepuluh meter ke bawah, ke arah mobilnya.
Segera ia menangkap dinding, dan menempel bagai cicak.

Ia mulai merayap turun, terburu-buru, dengan gelisah memeriksa meteran mandalanya.
Empat meter dari tanah, meteran mandalanya menguap, tangannya tergelincir.
Ia segera mendorong keras dinding dengan segenap tenaganya.
Rick berguling di tanah, mengalihkan momentumnya ke samping.
Kakinya terkilir.

Dengan susah payah ia merayap masuk ke mobil, duduk di kursi belakang.
Di antara senggalan nafasnya, ia memerintahkan, "*Safe house, now*!"
Mobil tersebut segera mengunci pintu, dan meluncur dengan sendirinya.

Ia menutup matanya, dan tersenyum.
Untuk pertama kalinya setelah sekian lama, ketika kelopak matanya menutup, tidak ada lagi yang terlihat, gelap.
Ia juga baru menyadari, ternyata mobil ini kedap suara.

Rick tertawa.
Dan tertawa.
Seandainya mobil ini memiliki supir manusia, ia pasti merasa Rick sudah gila.

Kedelapan orang tadi tidak menyangka akan berhadapan dengan seorang *dukun*.
Mereka mengamati kondisi medan tempur, dan hanya dapat menghela nafas.
Kacau.
Semua komputer dibongkar paksa, cakram disknya diambil mereka.
Sebagian korslet, mereka berharap setidaknya salah satu diantaranya masih berfungsi.
Satu jari hilang, satu wajah luka, satu luka bakar tingkat dua, setidaknya tidak akan sia-sia.
Setidaknya mereka memperoleh sesuatu dari remah-remah pertempuran.

Saat mereka berusaha berjalan keluar, mereka meliriki pintu dapur yang terbuka.
Terheran dengan adanya embusan angin malam, mereka mengarahkan senter ke dalamnya.
Sebuah lubang tampak menganga di dinding dapur.
Lingkaran sempurna, tidak seperti bekas ledakan.

Mereka tidak peduli, dan langsung berjalan melewati dapur tersebut.
Ada hal yang lebih aneh hari ini, mereka baru saja berhadapan dengan seorang *dukun*.
Mereka lebih khawatir lagi memikirkan, mau bilang apa sama bos mereka?
Siapa yang akan percaya bahwa lawan mereka adalah seorang *dukun*?

Rick sedang berada dalam suatu ruangan kelas.
Ia baru saja menunjukkan ke teman-temannya kalau ia dapat melihat meskipun matanya ditutup rapat dengan kain, dan digeluti dengan teman-temannya.
Ia menghadap ke arah pintu kelas yang berlawanan dengan papan tulis.

Seorang siswa menuliskan sesuatu di papan tulis, dan Rick membaca semuanya dengan tepat, hingga titik komanya.
Seorang dari teman-temannya mulai menyebut Rick *dukun.*
Seketika sekelas mengulang-ulang kata tersebut sambil menghadap Rick.

"Dukun, dukun, dukun"

"Saya bukan dukun!"

Rick mendapatinya teriak sendiri dalam mobil tanpa pengemudi.
Ternyata hanya mimpi.
Dirinya benci disebut dukun sedari dulu.
Ia bukan dukun.
Ia hanyalah seorang pengendali kenyataan.

Tawa Rick menggelegar, ia menutupi matanya dengan lengannya.
Sesuatu masih di tangannya, tangannya turun dari wajahnya.
Genggamannya merekah, dan di dalamnya, sebuah *K-Drive.*

Sebuah layar komputer di belakang kursi pengemudi kosong menarik perhatian Rick.
Dicolokkannya *K-Drive* ke port yang tersedia, dan data mulai diunduh ke komputer mobil.
Ia mulai memeriksa berkas-berkas di sana satu persatu.
Hingga ia mendapati suatu berkas, yang menjabarkan suatu artifak langka.

"*A Black's Staff*," bacanya, "*an artifact that had been around on Earth, since the day Cain killed Abel.*"

"Manusia sudah gila, saya harus melawan maut hari ini hanya untuk mendapat ini?"

Meteran mandalanya muncul di depannya.
Satu derajat kerlipan kuning terang muncul di bagian dasar lingkaran, perlahan-lahan bertambah hingga sepuluh derajat, lima belas, sembilan puluh.
Setiap kubik sentimeter dari mobil ini pun terasa, setiap detik bertambah detail, dan kini menutup mata tidak ada lagi efeknya.

"Sial, Ein barusan gajian," Rick menepuk jidatnya.

Senyum merekah di wajah Rick.
Ia tertawa, dan tertawa.
Sebagian dirinya menyesal karena ia tidak segera tidur selagi indera keenamnya tidak aktif.
Sebagian dirinya lagi bersyukur karena indera keenamnya berfungsi baik.
Indera keenamnya yang menggangu itu, ternyata masih Rick butuhkan.

Punya hak apa dia untuk membenci sesuatu yang berguna untuknya?

