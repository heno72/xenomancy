# The Bet

**Remember that this is from the old 20190110 Draft, new changes had been added but yet to be incorporated.**

Steven and Daniel fought for about an hour now.
They decided that it’s almost impossible to stop them all.
They barely reduce the number of the invading aliens.
They noticed two things during that time, however.
First, there’s a group of human observing the invasion but not touched by the aliens, and they’re doing it from a raised platform above an unknown model of very large, half submerged submarine.
Second is that the very large fleshy island is actually a very large whale with six fins underwater.
Either one of them could be the controlling mind, or at least command center of all of these aliens.
Expecting unexpected, they decided to try attacking the first.
As they flew closer to the submarine, they could almost identify one of them, but they couldn’t really remember who he was.
Closer they got toward the submarine, it is apparent that one of them is Steven’s cousin in law, Aditya.
Perhaps that was the second most surprising thing Steven realized.
The first is the fact that when Anderson said that he was attacked by Aditya, Steven never considered his cousin in law as that Aditya, but then right now deep inside his gut he knew that his cousin in law is the one that wounded Anderson.
Anderson was a man that’s attacked by a gang of violent motorcyclists and sent them all into infirmary, without himself got any scratches.
This is the third thing that surprised him at the moment.
Aditya flew with a pair of feathery green wings, and at that moment Steven realized that he was covered with some kind of green scaly armor, probably biological equivalent of his and Daniel’s armor.

Eagles fought with a rather weird posture, they grabbed each other’s talons and they flew, rotating rapidly in the air until either one of them fainted.
That’s what Aditya’s doing with Daniel right now.
Daniel, not surprisingly, fainted soon after.
Human body isn’t meant to survive high gees of acceleration.
In fact Aditya should’ve fainted as well, but he didn’t.
Steven thought that it is the fourth most surprising fact about him.
At that moment Aditya flew to Steven and quickly did that maneuver to him.
It is a rather lucky coincidence that Steven’s ancestor were designed to survive high gee world of Aucafidus, that he could match Aditya, they both passed almost simultaneously.
It doesn’t matter who fainted first, as such that short slice of time isn’t enough for either of them to take advantage of the situation.
They both fell, until their armor turned autopilot and get them into level position.
That’s what happens to Daniel back then as well, if anybody thinks I forget about him.
If you think they were just toying around, then you’re probably right, and as equally, probably wrong.
The next fights are more serious, as Steven’s left arm armor cracks, Daniel’s (coincidentally also his left) wing torn midway, almost, and Aditya’s chest ripped off, okay it was his armor’s chest anyway.
They fought in the sky, underwater, among buildings, atop a dragon, and suddenly Aditya flew back to the submarine.
Just as they thought they were winning, Aditya ordered his men to shot two silver fiery darts (read: misiles) to them.
Perhaps Aditya decided to stop toying around and be practical.
Either way one of the missiles were exploded prematurely by a pair of green laser beams from Steven’s car who caught Aditya’s attention by surprise (hey, it’s a Flying car!).
Another one is redirected by Steven into the submarine.
The submarine sank, Aditya’s men flew with similar armor.
Angered, they flew toward Steven’s car and barely scathed it.
Then they decided to aim Steven and Daniel instead.
The car was about to help when suddenly a flock of dragons bite the car, flung it, and whipped it rather violently that it’s almost shapeless, not because they were ordered by Aditya’s men, but rather because on its way to Steven, the car hit the flock leader’s head and knock him into the sea, rather embarrassingly.
(The dragons are sentient! Was probably the car’s thought at the moment).
The reactionless drives still held it stationary anyway.
Perhaps it is a car’s equivalent of shock.

Steven and Daniel now fought Aditya’s men, they barely managed to escape, when finally they agreed that they should have aimed for the giant whale instead.
Obviously outnumbered (they were as well when they fought the dragons, but oh well), and unable to break free, Daniel gave Steven his third most brilliant advice of the day: order your car to fight for the whale instead! But at that moment Steven decided to make his own, most brilliant idea of the day: detach one of the drive and at close proximity to the whale, accelerate itself into relativistic velocity and let its surface fuse with the air and the skin of the whale to do the rest of the job.
The whale blew rather spectacularly and the swarm that occupying Kendari were instantly disoriented.
Dragons collide with one another, denefasan horses and denefasan critters stomped by denefasan elephants, denefasan elephants burn each other, et cetera.
