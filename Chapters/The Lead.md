# The Lead

> In-story date is 20210418 0300

They land on an opening near the cliff, dekat Tanjunglangkat, Gunung Sinambung.

Suharto is waiting in the landing pad.
Both copter land safely, and a group of troop took the seven person to interrogation room, while Enam, Sukarno, Derictor, and Jokowi follows Suharto to Briefing Room.
They discuss about the seven person, and that the target runs away.
Jokowi is tasked for interrogation, Enam says that they have two options.

The first is to hunt down the target, remains in secrecy, but it would took enormous amount of resource to, while they still have to monitor the company from inside.
Enam argues this one they might not be able to be done alone, not impossible, but the possibility is thin.
Derictor agrees, as they can't allocate corporate resources any bigger than they have now.
Suharto emphasize the importance of the target to be captured, as he owns a piece of information about anomalous cash flow inside KukerCorp that is redirected to BAIK (not explicitly noted in the data, but might lead to the exposure of BAIK to public).

The second option is to share this intelligence data to other secret agencies, in risk that they might be known to others.
But this one allows the other factions to take over KERNoK, with less effort in BAIK sides.
Henokh argues, it can't be done, as if this agency is known, it would make them quite vulnerable, as informations may leak and some agencies that they wish to not be known by (CIA is another example, and more importantly, BIN), will know them.
Derictor also argues such that leak would result in decreasing trusts from investors, as BAIK also actively monitors KukerCorp's investors.
They also throws a lot of corruptors, so potential corruptors would be against the existence of this agency.

Enam says it is logical to gain more help than to do everything alone, and he argues that if the company remains aligned to dreams Suharto wish to realize, then they should consider sharing this informations, to at least limited number of third parties, to also aids the search.
Because that way BAIK could focus more on its original creation goal, to maintain corporate stability and integrity of KukerCorp, rather than chasing out insurgents.
The need of many outweighs the need of some, Enam quotes.

Suharto looks at Sukarno, as if they're talking through their eyes.
Suharto then says that they can't risk on being known.
With more eyes directed to the agency, means less space of movement for the agency to do its job, hence preventing the dreams Suharto is seeking from happening.

Sukarno agrees, that they'd better split the focus, as the agency is supposed to protect the corporation from inner threats (integrity), and outsider threats, and KERNoK is such outsider threat that worth attentions.
So Sukarno as the chief decided that they should stay the way they are now, monitor KERNoK, and only engage when necessary, the target is high priority.
So he tasked four person including himself to focus more on this matter.
Enam (the so called KERNoK expert), Jokowi, Sukarno, and Derictor, will focus more on KERNoK, while the remaining of the team would only focus on internal affairs.

Reluctantly all of them agrees.

Enam also exposes the fact that he had tracked about 257 new Yayasan were founded within the last year.
All of them work in social services.
Derictor fail to see the relevance, and Enam explains, that he managed to visited all of them, and gain information that at least 132 of them have pengurus-pengurus that are listed on his database regarding known KERNoK members.
They're all from Unit C of KERNoK, recruiting unit.
Suharto raises his eyebrows.

Suharto asks for why are they not detected by BIN or other agencies, as such extremist yayasan were usually found.
That means it should not be their concern.
Enam then stated that the problem is all of that Yayasan are not religious ones, rather pure social services.
Therefore mostly ignored, and yes the cashflow has no direct relationship with known terrorism faction cashflows, so officials thought of them as genuine yayasan.

Derictor argues that it is irrelevant for the task they just discussed about.
Sukarno argues the same thing, and asked, whether or not it has something to do with the target (that is from Unit H).
Suharto just stares, curiously waiting for Enam's response.

Enam explained that among all of them, two of that Yayasan were in Kendari.
And he tracked at least fourteen members of both Yayasan combined, were employees of KukerCorp's sister companies on Kendari branch.
Now Sukarno raises his eyebrows.

Suharto asked, how could there be employees of KukerCorp enlisting to KERNoK? Suharto argues that it is almost impossible for him to not be notified, as each regional head should be reporting it.
It is corporate policy to notify if any employee were to join any yayasan or outsider organizations, so the system could rearrange their schedules to fit their outsider organizations demand.

Enam grinned.
Apparently the regional head on that area has some sort of anomalous cashflow, he said.
With salary of about 20 million rupiah, he and his family has enormous amount of wealth, not from any apparent clear sources like warisan, or wife's income (the wife do not work at all).
Somehow Enam just realized the pattern after the second year of that regional head is in charge.

Suharto looked at Sukarno, "You're regional head of Konawe, why don't you give him a visit? As fellow regional head, and your apparent clumpsiness, I think that would make him think that you're vulnerable.
Let us wish he'd try to use you for his benefit" Suharto said.
"Ouch, that is hurt" replied Sukarno.

Enam argues that Suharto is correct in some ways, try to complain about how hard it is to maintain your financial state (faked, of course) might draw him into spilling small secrets he has to achieve great wealth, only fractions of it.
But first they need to make Sukarno close with the big bastard.
Suharto looks at the calendar, and noted that within some weeks, there's annual regional head meeting, in KukerCorp Training Center, Trawas.
So it could be arranged that Sukarno and the second target sharing one room, and in one team for outbound activities.
