# The Weakest Link of a Chain
**Remember that this is from the old 20190110 Draft, new changes had been added but yet to be incorporated.**

Also in another secret office of yet another secret organization led by Aditya, Aditya decided to scheme a revenge for Steven, the one that ruined his plans and that destroyed his favorite submarine.
Days after, things appeared to reach normalcy, except for the post-war landscape of Kendari.
Economy collapsed, locally, schools damaged, and some teachers went missing.
However, (un)fortunately for the students, schools are still being held in ad hoc classes.
It’s almost lawless.
If there’s any time where people could be killed without much media coverage, it is now.
And exactly that is why Aditya decided to dispatch his men on killing Steven and Daniel.
Daniel’s the easiest target at the time.

Steven decided to drop by Daniel’s house, and Fernando’s the one that open the door.
He wore only his boxer, and a towel hugging his back, apparently just bathing.
Steven asked him where Daniel was, and he said Daniel’s still sleeping upstairs.
Steven proceeded to his room accompanied with Fernando, and discovered Daniel still on the bed, naked inside his blanket.
Steven woke him up, and Daniel was so surprised he tried to cover up his body.
Fernando and Steven laughed, and Steven remarked how lucky Daniel was that Steven didn’t bring Helena.
They talked for some time and Daniel cuddled with Fernando, remarking how hungry he were, when, suddenly, he realized that Steven’s still in this room.
Awkwardly, Fernando tried to convince Steven that Daniel really meant literal hunger.
Steven cheerfully asked Fernando to eat and buy some food for Daniel, while Daniel get himself ready.
Fernando then wore the clothes, subtly reluctantly, while a disappointed expression hinted on Daniel’s face.
Steven just smiled at Daniel.
Suddenly Daniel felt, or rather, reminded, how alien Steven was.
Steven was just so happy that he thinks he’s doing Daniel a favor.
Steven seriously consider that Daniel meant to ask for some time alone to have a good time, so he decided to bring Fernando along to ease Daniel having a good time, alone.
Perhaps Steven’s pure honesty at unusual time like this occasion that makes Daniel sometimes fails to understand Steven.
As they leave the room, Fernando put a sorry face to Daniel, and Daniel was now alone in his room.

In the road home (as Steven’s car happily drive itself) after they finished their meal, Fernando asked Steven, whether or not Steven actually understand what Daniel meant back then, Steven explained just like that, and adds a remark on how good Fernando was to pick up Daniel’s sign and pretend that Daniel really meant to be hungry.
That’s why Steven decided to pretend that he believe Fernando.
Steven said that he reads that sometimes human male needs some time alone to have a good time with himself, even if he’s in a relationship.
That’s perhaps one thing Steven didn’t really understand about humans.
Fernando wondered how Steven’s mind works, but decided not to question it further.
Perhaps the most surprising fact unbeknownst to both of them is that how Steven’s actually far more human-like than Fernando is to aliens, mainly due to the fact that he’s only half Etoan: his father is Etoan, and his mother is human.
However Steven prefer to be likened to Etoan than to human, especially because of his innate ability to interact with Etoan technology in a way is impossible for human: Steven can, innately, receive and transmit radio frequency.
That and the fact that Steven’s heart has seven chambers instead of four.
As they approached Daniel’s house, both of them (yes, the car included) are startled to discover a black range rover on the yard, and the front door was left open.
The car confirmed to Steven that it was locked when they left.
Steven decided that it’s also better to back up Fernando as well.
Things are going crazy lately, and he couldn’t afford to lose his friends.
Fernando agreed, even allowing Steven to do so.
A second later Fernando went through similar experiences Daniel experienced days ago as the couch engulfed Fernando.
Two angels exited the car, and carefully, silently entered the house.
Steven located and seized a guy on the kitchen, Fernando went upstairs to Daniel’s room.

As Fernando entered the room, the guy’s surprised and release a shot to Daniel’s left chest.
Steven, receiving feed from Fernando’s armor, was terrified as well.
He drags the man he petrified on the kitchen up as fast as possible, and remotely ordered Fernando’s armor to seize the second guy as well, then tend Daniel as quickly as possible.
The car ejected a nanomedic capsule to the window, which Steven quickly catched and injected it to Daniel’s wound.
Perhaps worth noting is that miraculously Daniel has a rare condition known as Situs Inversus Totalis, meaning that his morphology is mirrored entirely from left to right.
In short, his heart is on his right side rather than left.
Also worth noting that this makes Daniel technically left-handed, as his dominant arm is his right, if chirality of his anatomy is to be considered, a fact that most people wouldn’t notice and assume that he’s a normal right-handed person.
Also worth noting that shortly after, Steven realized that he needs not worry so much about the fatality of his wound, as Daniel is backed up already, as recent as during his sleep, and he could resurrect Daniel again should he died at the moment.
Still that fact has no meaning for Fernando, as his lover is in terrible pain, and mainly because his blood spilt rather messily.
And that didn’t prevent Steven to be fueled with fury and anger to the ones hurting his best friend.
Crushing their guns in front of them was, surprisingly, sufficient to make them spilt out required informations Steven might need, plus they peed in their pants.

More and more Aditya’s men converged at Daniel’s house, then Fernando, carrying Daniel in his arms, jumped to the car, now stationed right below the window of Daniel’s room.
Steven jumped down as well, releasing green laser shots toward Aditya’s men.
G28 and police officers, called by Steven, was also on the way, converging to exactly the same spot, by a delay of five minutes.
At that time, however, Aditya’s men already recovered their two comrades, and left the site.
That wasn’t Steven’s main concern at the time.
As they’re zooming through the roads, Steven knew that another group of men were converging at David’s school and another group on Peter and Eden’s school.
Even at the time like this, Daniel still managed to give the most brilliant idea of the day: Steven to David, and Fernando to Peter and Eden, while the car, after contacting Helena, delivered Daniel to Helena for further treatment.

Steven couldn’t locate David, and he discovered David’s utility tablet laid among rubbles on the school’s gate.
A notification by Fernando that Peter and Eden are okay was a short-lived relief that ended when he asked Fernando to bring them with Helena and Daniel, so that they could board Steven’s ship under his house, for emergency occasion.
Scribbled on the screen of David’s utility tablet by waterproof black inks was an address.
He followed that lead into an abandoned building.
A group of men surrounded a boy, which quickly recognized by Steven as David.
Aditya was on David’s side, holding a dagger positioned on David’s neck.
Aditya opened his speech with how delicate the plan was, and how he was planning to liberate earth from corruptions.
Then Steven arrived and messed up all of his delicate plans.
He then argued that however he disliked it, he decided to give Steven a lesson to mind his own business than to interfere with other’s business.
Steven then realized that all of them (but David, obviously) wore the armor they worn days ago during the invasion.
Steven’s alone, so he couldn’t possibly win against all of them.
Steven was given two options: to let Aditya kill David, or for Steven to kill David instead.
Steven was devastated.
David was even more, that his expression is blank.
Bruises could be seen all over his face, and his arms.
His uniform torn, his undershirt tainted with specks of bloods.
He seemed to have some difficulties on breathing.
Steven’s pretty sure that some of his ribs were fractured.
Anger lit up in his heart’s furnace, but at the same time, he could almost feels David’s pain.
No, he felt it in a radio band, that belong to David.
Regrets flooded in, should he arrived sooner, David wouldn’t be through this pain.
Aditya asked Steven again to choose.
Aditya added, that Steven should also undone his armor.
Steven was petrified.
To motivate Steven, Aditya sliced David’s shoulder to the bone.
David heard himself screaming, and Steven’s cry: You crazy bastard! He’s your nephew! He’s your cousin’s son! Aditya laughed.
Aditya argued that he didn’t even consider David a human.
David’s not.
David’s an etoan.
Etoan, Aditya said, treated humans as if humans were animals on the zoo.
He had no reason to treat etoan as humans.
It’ll be the same, he said, that when he slain David, it’ll be the same as if he slain a lamb.
Aditya said he’s being considerate, in giving Steven a choice to slain his own son, rather than doing it himself.
The fact that Aditya know that Steven has etoan descent didn’t bother him.
His son’s pain is all in his mind right now.

He undone his armor, shakingly approaching his son, he took the dagger from Aditya’s firm grasp.
David transmitted this, the feeling of surrendering his fate to his trusted father.
David knew both of them couldn’t do much, and if they defy Aditya’s demand, both would have died anyway.
David wished that at least his dad would survive.
David also notified his dad that he had been backed up this morning.
David might have said that he’s okay and he’s ready, and that he’s fine as he had a recent backup of himself.
If he were to be resurrected later, he wouldn’t feel this pain anyway.
But this thought occurred to Steven: That would be a facsimile of his son.
He wanted his son, this son he was going to slain.
All of this time he brags about the wonder of Aucafidian technology, that everybody is practically immortal.
If you die, you could be resurrected.
Death is voluntary.
But right now, he said to himself, David’s death wouldn’t be voluntary, they were forced.
Even with all of Aucafidian tech’s wonder, what happened right now is far from what makes Aucafidian great: freedom of choice.
This kind of thought repeated over and over in his head.
A thought occurred to him, that he would just have to slain David right now, then himself, then when they both resurrected in Aucantica, with the rest of his family and his friends, they wouldn’t remember this moment, this pain, this agony.
A good idea, indeed, thought Steven.
He sank in his thoughts, he didn’t even realize how many tears he had shed, not even David’s, not even to the fact that they’re hugging each other right now, both crying like babies, not even to the fact that the dagger in his hand had been in David’s flesh, halfway to David’s heart.
A second later, Aditya heard three screams: Steven’s, David’s, and his.
The dagger didn’t make it to David’s heart.
It’s in Aditya’s neck.
Steven throw it in the last seconds.
With a command Steven’s armor moved and wrapped itself around David, protecting him inside and tended the wound.

His armor immediately flew, away from them, straight to his house, where his ship’s waiting.
Aditya furiously ordered his men to eliminate Steven, and Steven, equally furiously, evaded their attacks and rushed directly to Aditya.
He caught Aditya to the ground, pulling his dagger out of Aditya’s neck, without even care how miraculous it was that Aditya’s still alive after such that wound in his neck, and he prepared to stab Aditya in his eyes.
His eyes, Aditya’s eyes gleamed a deep regret.
Aditya pleaded: Steven! You’re a doctor! You’re sworn to help people, not to harm them! Steven answered Aditya, soon after, in a cold, heartless tone: Oh, him? You killed him when you forced him to slain his own child.
Perhaps the last expression shown by Aditya’s left eye was that of fear, ultimate horror realization of waking up a sleeping beast.
That day, Aditya lost his left eyes, and severely damaged his optical nerve when the dagger reached deep into his eye socket, and straight into his visual cortex.
Before he could even pull the dagger to break more nerves in Aditya’s head, his car zoomed into the building, leapt, and almost impossibly morphed in a shape to catch Steven and put him neatly in his seat and then, touchdown and left the scene as fast as it entered.
