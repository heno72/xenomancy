# The Invisible Hero

She's the most undesired kind of here. Not because she couldn't save people, but because she's so good at it nobody realized it was her.

She has this unique and annoying ability to know people with bad intent, and to also notice their target. And she has this annoying will to always help despite of her not actually caring. Or she doesn't wish to care, she just did.

One day in a fine road, she was driving her bike. It was a smooth ride and a nice evening. Not to hot, not to cold. The air was clear, the sky was a nice shade of orange. At such a beautiful moment she wished to be able to enjoy it. But she couldn't: she noticed a young boy on a bike not far from her, eyeing the phone of an unsuspecting youth passenger of a bike. The unsuspecting youth gazed around the scenery, right at the opposite side of his hand that held his phone. He was enjoying the nuance she wished to enjoy.

A young boy eyeing the unsuspecting youth's phone, was closing their distance to get the phone from the side the unsuspecting youth wasn't looking at: the side where he held the phone. She was also closing her gap between her and them. She get the phone first, that startled the unsuspecting youth. She waved the phone in the air, and the perplexed young boy decided to grab it from her, in his confusion. She draw the phone from the air and the young boy's bike destabilized and he fell to the road. She gave the phone back to the unsuspecting youth. He cursed her. No, they cursed her.

She reminded herself why didn't she care to anyone around at all, nobody will thank her for what she done. She left the scene. She just stopped a crime and punished the to-be wrongdoer. But nobody appreciated her.

On a prom night, she noticed that someone might want to rape one of the weak girl that she knew. When he's close at getting his target into position, she picked the man's wallet and phone, and revealed dick pics of the man in his phone, and a printed out nudes of him in his wallet to the prom invitees. Needless to say she successfully stopped the rape. But the class accused her for not respecting one's privacy.

Her unusual ability of knowing when a crime is about to happen around her is very accurate, that she never remembered a single time of failure. She wasn't always like that.

Her life start as an eccentric youth with a single mother raising her. Her mother wasn't really the best mother, but she tried to be. At least her mother worked to get her daughter something to eat.

Her only social circle was her classmates, but even then she wasn't favored by her classmates. So to gain attention, she learned how to do crimes. She learned how to pickpocket, she learned how to tackle people, she learned how to get someone's secret. In practicing, she caused nearly everybody hated her.

She was the random weirdo of her classroom. She ended up being lonely. This is when she realized that instead of being good at doing crimes, she's being extremely good at recognizing the signs of crimes, even before it is done.

It was a day after Indonesia's Independence Day ceremony. She didn't feel well that morning, but it wasn't from anyone in the classroom. She couldn't understand it as well. She decided to just dozed off in the class. Until one time, her unpleasantness peaked, and in the middle of the class she stood. The teacher asked her why, and she didn't respond. Her teacher's questions might be annoying and makes her feel unpleasantness, but it was nothing compared to the feeling she had. She grabbed a chair: yes she's that strong, and she throw it to the door, that was open at the time. They were on the third floor of the school building, and after the door was a corridor where students could observe the central sportfield. Everyone screams afterwards. She did manage to hit a dragon's eye passed through the building, and it fell to the field.

Deep inside she wished anyone noticed her deed. But most was too busy being scared off by the dragon. But then someone grabbed her hand. It was a man, the kind of man she'd miss most of the time, the kind of man too kind for her sense to pick.

"I want to be your girlfriend" was the first thing he said.

She was confused, until he revised it.

"I mean, I want to be your boyfriend."

She laughed. It wasn't because he was cute. He is cute to her sense, though. She laughed out of joy, her tear almost spilled. After all of this time she wished to attract everyone's attention (and almost none given), someone was attracted to her. Without further ado she kissed him, and he was surprised.

It wasn't because he dejected her move, but because he didn't expect her to reciprocate this soon. He let her explore his mouth.

After the kiss, most students (and teachers) scattered all over the school, trying to run away from the incoming monsters. There was only four people left in the room. She was included, and then a young, shivered boy, another boy with white hair, and the boy she just kissed.

The shivered boy commented, "I don't think it is the most appropriate moment to confess your love, loves, or whatever."

The boy with white hair said "we should draw the monster's attention away from our classmates, to give them time to leave the school"

"Really Ashton? It was the first concern of yours? Their safety? What about ours?" Inquired the shivered boy.

"Do you need something to protect you? Here, my backpack will protect you" he replied.

Zean, the shivered boy took the backpack, but still couldn't figure out how the backpack would help.

"David!" Yelled Ashton.

They were startled and followed Ashton. Her ability to stop crime did work out in a fight. She can predict the creature's next step and prevent it from happening. It wasn't really a surprise for her, but it did surprised David. What surprised her was, though, that David and Ashton wore a thin suit or uniform that is resistant from tearing and scratch. And it could help them to stand against normal on any surface, of any orientation, while could be as slippery as finest ice surface right at the freezing point, that they could slid around at great velocity on a surface.

Zean and her was equally startled to discover that the backpack Ashton gave him was able to deform into a pile of active black sand-like swarm, that could move freely about and create structures, both or either offensive and defensive structures.

She felt safe, not because of the magic they showed, but because she was with her alike: freaks, outliers, exceptional people. And they considered her equals.
