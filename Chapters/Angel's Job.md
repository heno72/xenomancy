# Angel's Job

> In-story date is 20210406 0150

It was a restless night for Heinrich.
He stood on the balcony of Hendrik’s apartment while Romanov and Hendrik were sleeping.
On the trail of his gaze, he saw a bird approached the balcony.
It landed on Hendrik’s towel hanger attached to the balcony wall.
Something was stubbornly attached to its feet, while the bird was pecking it persistently, trying to get it off.
Heinrich was there watching the whole ordeal.

Heinrich decided to help, and on his palm was this paper, with a strange insignia of Hadad’s sign: a cross encased in a circle.
He knew that it was time for another job, a job for an Angel to take care of mortal affairs for the Divines.
Sighing slightly, he climbed the fence of the balcony, and jumped forward out of the thirty first floor toward the pool down below.
The bird gazed at him, puzzled by what it saw.
He would have hit the pool within four seconds, but that was enough for him to summon the water to retrieve him.

Sir Isaac Newton once approximated a way to calculate the impact depth of a projectile moving at high velocities: an object moving in a sufficiently high velocity will penetrate a depth that is equal to its own length times its relative density to the target material.
From that approximation, Heinrich knew that to accommodate his one meter eighty centimeters tall body with a density close to the density of water into a water pool the depth of one meter twenty centimeters without breaking the base of the pool is to raise the water level about twice as much.

With a spread of his arms, a large boulder of water rose to welcome him, and he penetrated right through its surface.
The water boulder splashes back as the force that was raising it disappears, wetting the surrounding park in the process.
Heinrich rose from the floor of the pool, and to the park.
He looked around, to see more signs on where he needed to go.

With a hunch growing from his gut, he decided to head southeast, and with a spell, his synthetic body accelerated through the air at an acceleration of fifty Earth gravity.

\***

A hundred and eighty kilometers in two minutes later, without a voice, Heinrich stood at the tip of the Ijen Crater.
There were a group of people at the other end, preparing filming equipment.
To the untrained eyes, they were filming, but Heinrich knew they consists of a group of reflexiors.
They were not from the Watchtower Foundation, or WTF, Heinrich’s reflexior organization, but from a competing group known as the Extrasensory perception and Psychokinesis Laboratory, or EPL.
It appears, that Heinrich’s divine mission was to prevent them from doing whatever they were trying to do, on which Heinrich didn't care enough to find out.

But no, it didn’t feel right, his attention was drawn to another set of noise, from the incoming fleet of containers far on the base of the crater.
Upon inspection, a container had the same insignia he witnessed no less than five minutes ago, the sign of Hadad.

He prepared his stance, from a nearby hill he landed on, he sat down.
He tried to sense the boulders under him, deep buried inside the hill.
He peered deeper, as network of reflexium particles lit up and signals each other across the hill.
Another network of reflexium particles on the air lined up into strings, into webs, and they connect themselves to the reflexium particles under the ground.
To his sight, the entire hill was a foggy construct of reflexium particles trying to move the entire hill.

He targeted the container, and suddenly, a thousand metric tons of dirt lifted off the ground on the side of the road.

A moment after, “Let there be a landslide,” said Heinrich.

The dirt fell, and there was a landslide.
The driver fell in tremor, trying to figure out what had happened, but before he knew it, the truck and most of the fleet were deep inside dirt and stones, tens of meters away beside the road track.
The remaining of the fleet were observing their surroundings, until one pointed out toward Heinrich’s directions.
Heinrich was intrigued.

A group of men with bikes climbed up the hill toward Heinrich.
That was the time Heinrich realized that the trees and plants on their path were bending away, stretched beyond their tensile strength, some snapped off in an instant, and resulting in a track of butchered, spaghettified  trees and bushes.
It was still going, more and more trees and plants bent to give them ways, a straight line toward Heinrich.

Heinrich stood up, and panic soon struck him, as he noticed some of the passengers of the bike actually carried walkie talkies, which they used to aim toward the trees.
It was seconds after when Heinrich realized that those walkie talkies weren’t ordinary walkie talkies, they were Fiat Wands, developed by the EPL reflexiologists to allow areflexi, men without reflexius capabilities, bends reality.

Fiat Wands work by emulating a reflexior’s Reflexius Clusters, usually found in the counterparts of Brocca’s area and Wernicke’s area on the opposite brain hemisphere of a reflexior.
In normal humans, or areflexi-kinds, those parts of their brain were somewhat dormant.

On why the region is dormant on most normal humans, Julian Jaynes on his theory of the bicameral minds said that the opposite region of one’s speech centers were the voice of “god” that one would obey thoughtlessly.
In essence, according to his theory, as recent as three thousand years ago, most people were unconscious, only taking orders from their own internal voice of “god” without even questioning it.
In the following millenia, as our society grew in complexity and we require a more flexible mental framework, consciousness emerged and the region that produces voice of “god” turned dormant.

Julian Jaynes was only partially mistaken.
The regions were dormant on normal humans because normal humans lost the ability to utilize their Reflexius Clusters, and therefore, the ability to communicate with the Rerum Flexius Lamina, the layer of reality bending infrastructure comprised of networks of reflexium particles, that allows one to bend our physical reality.
The notion were noted by Jesus as recent as two thousand years ago, on Matthew chapter seventeen verse twenty:

> He said to them, “Because of your little faith. For truly, I say to you, if you have faith like a grain of mustard seed, you will say to this mountain, ‘Move from here to there,’ and it will move, and nothing will be impossible for you.”

In essence, Fiat Wands are like training wheels for a normal human, to reactivate their Reflexius Clusters, and cost Heinrich troubles with fighting a group of reflexius-able areflexis.

With a spread of Heinrich’s arm, a combatant rider hits an invisible wall and the motorbike continued to run under him, without carrying his body.
The passenger behind him had his head bumped hard on the skull of the rider, and thus they fell to the ground, confused.

Two other passengers on adjacent motorbikes swung their Fiat Wands in unison, and a large tree unearthed, which was then flung toward Heinrich.
Facing the flying tree head on, the tree split into two, and then into four, and then into many small pieces, each piece turned into sharp, serrated wooden spears.
Heinrich spread his arms and the spears shoots toward the incoming combatants.
The spears avoided the two incoming motorbikes as if an invisible dome protected them against them.

Heinrich was irritated, because the giant tree was nothing but a distraction.
Immediately, another fleet of motorbikes came from the sides and had a tree warped around his body in unnatural angles.
He was surrounded by about a dozen men with Fiat Wands aimed to him shortly after.

The land above them slid at such force that the men didn't have time to brace, and brought along the slides.
Heinrich shattered the tree into wooden spears, and spread them around him.
The sight of a man in the middle of wooden spears rising from the ground somewhat brought an atmosphere of intimidation, which in turn caused some of them to flee.

The remaining ones launched a final blow.
Heinrich felt his entire front surface hit an invisible wall moving against him, and was thrown away a few ten meters back.

Those brave men waved their Fiat Wands and their bikes unearthed in pristine conditions.
Gazing upon one another, they agreed that they should leave the scene before the mysterious man returned.

Farther back on the other side of the hill, Heinrich gritted his teeth.
He was fooled by those fake reflexiors.

Despite thinking that his mission was a failure, he received full payments from the Divine Council of Earth, his employer.
As usual, his queries on why weren't responded at all.
The Powers were esoteric in many meanings, their ability of making him doing whatever job he needed to do without even telling what the job was a proof of their esotericism.

\***

One of the riders from the fight reported back to a young man who rode a giant shark appearing to be composed of weaved glowing strings, and countless number of stringy appendages emanated from its body.

“A WTF guy was in our way, and so that woman on the container died.
I am sorry, Sir, it appears that we cannot continue the ritual.”

The rider didn't even dare to face that young man.
The young man didn't even look at him, he was gazing at the blue fire of Ijen Crater.

The beast approached the rider, and to untrained eyes, it was going to caress his skin.
The young man stroked his ride, a glowing stringy shark abomination, causes it to withdraw its tentacles away from the rider.

“Kamaitachi really wishes to cut you into pieces, you know,” the young man said.

“I'm sorry Sir Anthony! I really am sorry,”
cried the rider, pleading on his knees.

“Oh no, no, you're getting the wrong idea,”
Anthony jumped off his ride, approaching the crying rider.

“Kamaitachi wanted to cut you into pieces, not me,”
said the young man.

His smile was reassuring, and for a moment, the rider's only hope.

“Look, we lost our woman, so we'll simply have to find a new one. Summoning the Chaos Beast surely would attract unwanted attention. It is just not the right time,”
continued Anthony.

“But it took us ages to find that woman, Sir.”

“Just call me my name, Tony. You're double my age.”

“Yes sir, Tony sir.”

Anthony grunted.

“We will wait until the time is perfect to summon the beast. The time will come.”

Anthony gazed at the blue ring of fire dancing about the Ijen Crater.
His thoughts however weren't on it, but rather pondering around the possibility that the Deity that suggested him to summon the beast and the Deity that stops the summon are in reality, two different deities.
Different deities having different agenda was a sign of the next Baal Cycle, like how it was two thousand years ago.

He wished that he was able to divine the situation, to divine the thoughts of the Divines.
*Theomancy* was the name of such divination he had in mind, but it didn't ring on his taste.
