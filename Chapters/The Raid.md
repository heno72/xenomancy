# The Raid

> In-story date is 20210418 0240

At that particular day, Hendrik could feel what it was like to be a normal human.
Usually the tranquility was cherised the most by him.
However an old sayings caught his acknowledgment: that one appreciated the worth of something the most when it was gone.

A torrent of hot leads pierced the air around him.
Concretes penetrated and rubbles flung around.
Piercing swoosh accompanied every leads fired.

One might not be able to comprehend the pain in one's ears, being very close to the stream of bullets.
Each bullet rips the air in such a force, it was painful just to listen to it.
Your sound system in your living room lied to you: it was just way too soft.
It sounds right, but it wasn't.

Shuddered by his close encounter with death, he crawled for a hideout.
As powerless as he was at the time, he hid behind a wooden desk.
He was certain that it would not hold for long, but it provided him some time to recollect his scattered soul pieces and return the fire.

“ I told you I am not in the best shape for a fight like this!” Hendrik screamed.

“ What? Because you can't use your psychic power now?” said Nicolas Armaniputra.

Hendrik's head shook to his side, his gaze met Nicolas', and his fists clenched.

“ I AM NOT A PSYCHIC!” said him, “ I am a Reflexior.”

Nicolas didn't respond.
He broke eye contact with Hendrik, turned it to Derictor's handsign.
The bad guys were retreating.

“ They got the staff,” said Anderson.

Derictor rose from Anderson's side, and a short knife escaped his hands to reach one of the bad guys.
The next moment the poor guy heard was his own scream.
The scream crawled to the spines of the observers, both his opponents and his comrades.

Hendrik couldn't decide which was scariest, the guy's scream or Derictor's calm, cold gaze.
Derictor's cold gaze betrayed his seemingly-innocent baby face.

The poor guy's comrades agreed in silence, the poor guy wasn't worth saving.
The black case they carry worth more than the poor guy.
They left the poor guy, that fainted shortly due to oxygen shortage and low blood pressure caused by the traumatic pneumothorax.

Derictor chased the rest of the guys, followed by Anderson and Hendrik.
Nicolas Armaniputra inspected the fallen guy.
Hendrik returned to Nicolas Armaniputra.

"What are you doing?"

"Immediate medical attention required on Corridor 4F, one person, suffering traumatic pneumothorax on the right posterior thoracic cavity, adjacent to T7.
Intrapleural cavity might've been breached."

Hendrik knew Nicolas wasn't talking to him.
He was talking with mission control.
Done with the body, Nicolas and Hendrik left him.

It was a server room next to an emergency stairs leading to the roof.
They almost reached the open door, when hot leads streamed from within the room.
Hendrik almost had his nose gone, had he advanced a few more centimeters.
Anderson and Derictor made it to the other side of the open door, but neither of them managed to enter the room.
Anderson produced four balls, that unfurled their propeller blades as he threw them to the room.
Feeds of built in cameras on the drones streamed infrared eagle eye views of the room to their googles.
Anderson signalled the team with a nod, and they drew their electrolaser guns from their sleeves.

Anderson stepped in and shot his electrolaser guns with terrifying precision to four of bad guys.
Derictor followed behind and throw another blade to the nape of one of the bad guys.
He died instantly.

“ Isn't that too cruel? Why didn't you target non vital organs?” commented Hendrik.

“ Even if I were aiming for non vital parts, there is a chance of me hitting major arteries.
If I didn't, they would still be strong enough to fight back,” he aimed his short knife again to another guy, but the guy fell already due to Anderson's electrolaser gun, Derictor gazed at Hendrik again, “ It is then, more humane for me to led them into fast, less painful death than to let them die a slow, agonizing death.”

Hendrik was terrified at Derictor's cold tone, despite his child-like innocent voice.

“ That would still be an unjustifiable killing,” commented Hendrik in response.

Derictor gazed at Anderson,

“ This is why I object to have a bachelor of law in this team.”

“ He is perfect for this job,” Anderson insisted, “ even if he's not, he's the only psychic we have in our team.”

“ I am not a psychic! I am a Reflexior!”

None responded to Hendrik.

Air around a helicopter caused a certain turbulence pattern that excited free floating reflexium particles.
At that particular time, Hendrik was allowed another bit of quota for his gauge.
The signal reached his brain that translated into a feeling: air disturbance high above the building.
The shape and signature matched that of a helicopter.

“ I, I got my gauge replenished a bit,” Hendrik paused, “ A helicopter is approaching!”

“ See, this ability of him is what we need,” Anderson said.

Nicolas nodded, Derictor didn't budge.

They entered the door leading to the stairs, and a man appeared.
Derictor and Nicolas fought him.
He was quite agile, that Anderson noticed how inhuman his agility was.
Derictor and Nicolas barely able to match the guy.
However he did not carry the staff.
With a nod, Anderson signaled Hendrik to focus on those with the staff.
Anderson and Hendrik followed the other two to the roof.

Up on the roof, Hendrik arrived right after the two bad guys left the door.
With Romanov's Deep Time, he aimed right on the shorter guy's left feet.
Reflexium particles around the guy's feet collected and form strands.
The feet immobilized and the guy's momentum turned into torsion.
The guy fell flat to the floor.

Anderson gave Hendrik two thumbs up, to his surprise.
And he was about to caught the other guy, when that other guy produced a gun aimed right to Anderson's forehead.

Still in the Deep Time, Hendrik spent his last few seconds before turning into real time to propel the gun sideways, out to the dark night sky.
The guy was surprised, and Hendrik quickly feel the guy's face behind the mask.

“ Aditya!” shouted Hendrik.

Aditya moved sideways at a speed that surprised Anderson.
Unable to resist his momentum, that insisted Anderson to keep moving forward, Anderson fell face first to the ground.

“ Boss! Be careful, he's a *smart-person*!” screamed the guy whose left feet glued firmly to the floor.

*Smart-person* or *orang-pintar* is an Indonesian slang for a psychic user, that basically means a person with supernatural abilities.

“ I am not!” insisted Hendrik.

The stiffness spread from the guy's feet to his body, that his whole body was completely petrified.
His eyes darted around in fear.

“ A reflexior.
What a surprise.
So what my comrades on Kendari told me the other day, was true.
They fought a paranormal, they said.
Can you believe it?” Aditya laughed.

“ Finally, someone that know who I am.
I can't believe people keep calling me a paranormal.
I mean,” Hendrik's sense dawned upon him, “ wait, how did you know?”

Aditya gave the most enigmatic smile to Hendrik.
On the background was a distant sound of a helicopter.

Anderson, rubbing his face, complained, “ Really? Is this the best time to have a chit-chat with your opponent?”

Hendrik snapped, and raised his palms.
Bluish threads manifested on air surrounding Aditya, they wrapped around and tie Aditya up.

“ I'll ignite them, unless you give us the staff back!” screamed Hendrik.

“ Fascinating” was Aditya's only comment.
before he, with fair amount of struggle, break free from the strings.

“ What?”

Hendrik's gauge depleted.
It was not because of Aditya, he knew it.
His gauge was nearly depleted after all.
And the petrifying spell he gave to the other guy drained half of his gauge.
Hendrik was however surprised to the fact that Aditya could break free from the strings, which, supposedly contain enough energy to keep a normal human immobile for a few more seconds.

The other guy rose up, with a sprained ankle, and with some struggle approached Aditya.

“ What, are you surprised that I could break free from your weak spell?” asked Aditya.

“ What are you doing, Agent Sukarno? Electrocute him!” shouted Anderson to Hendrik.

Agent Sukarno was Hendrik's code name.

A helicopter approached the roof, and a rope ladder hanged below it.

The guy that fought Nicolas and Derictor dashed from the stairs to the roof toward the rope ladder.

Aditya dashed toward the ladder as well, but Anderson managed to capture him, and they fought.

And they fought.

And Anderson was surprised by Aditya's inhuman agility, that rivalled Anderson's already inhuman agility.

With no more time to contemplate, Anderson managed to lock Aditya's movements, and put Aditya in between him and Hendrik.

“ Now!” instructed Anderson.

Hendrik was about to pull the trigger when Aditya managed to turn and break free from Anderson.
The beam reached Anderson that was electrocuted instead of Aditya.

“ Sorry” was all Hendrik could express at the time.

Aditya managed to reach the rope ladder.
The shorter guy managed to reach the rope ladder as well, but shot by Hendrik's electrolaser.
He was about to aim at Aditya as well, but the growing distance between them did not permit a clear and accurate shot.

Anderson managed to rise, right at the moment Derictor and Nicolas arrived at the roof.
Nicolas and Derictor gazed at Anderson, and at the copter.

Nicolas gazed back at Anderson,

“ That guy can defeat Anderson?”

“ No, Agent Sukarno shot me,” said Anderson with a sharp gaze to Hendrik.

Naturally, Hendrik apologized, but he was intrigued by Anderson's gaze.
It was as if he was not serious.
Hendrik dismissed it as his imagination.

“ I managed to put a nanotracker gel on Aditya's neck” whispered Anderson to Hendrik.

They returned to the building and had the four stunned guys tied up.
The two deceased guys were put into mortuary bags.
All six of them were carried up to the roof.

“ Then what?” asked Nicolas, “ we have two dead bodies, four convicted bad guys, and a messy office building.”

“ Efraim, where are you?” whispered Anderson.

“ I'm talking to you, Agent Enam” inquired Nicolas.

“ I'm solving the six humans” replied Anderson.

“ I'm here, Sir Anderson” said someone in the air.

A shape, not unlike a helicopter appeared out of thin air, or rather, turned opaque.
It had no blades like usual helicopter does, but instead were two short wings with a number of small pores on the surface, each could fit a single fist.
But within the hole of each pore, was smaller blades.
There were this strong breeze that should have been very noisy, but they weren't.
Instead was a dampened noise, that Hendrik believed he could almost hear.

“ Take the bodies to our base” inquired Anderson.

Efraim opened up the front windshield in a manner that can only be described like a flowering of a snake's mouth.
Within were silver ribbons that wrapped the bodies and pulled them to the cockpit.
Efraim left almost immediately as the windshield reformed back into that of a normal helicopter.

“ Is that, a Securion Incorporated tech?” asked Nicolas in awe.

Derictor gave it a thoughtful face, and inquired,

“ If you do have such technology, why don't we use it to chase Aditya?”

“ If we were to chase them, they won't return to their base.
I applied a tracker on Aditya's neck.
I say we wait until they provided their secret base to us, unwillingly” explained Anderson.

His gaze turned to Hendrik, “ and now, is the job for Evidence Erasure Agency to remove our marks, isn't it?”

“ Splendid! I've never meet them, our new Evidence Erasure Agency,” commented Nicolas again, “ they're so good that nobody noticed the repair.
I want to see how good they are in person!”

“ This is unfortunate, but we have to go to our base.
They dislike bystanders when they're doing their job.
They hate humans, as humans are the only intelligent mammals on Earth that behaves like they're the only intelligent animal on Earth,” explained Hendrik.

Nicolas took some time to process what Hendrik just said.

Another instance of Efraim turned opaque and Hendrik stepped in.
That instance had four comfortable seats, and Hendrik took the front left seat.

Anderson sat right behind Hendrik, then Derictor beside Anderson, and in front of Derictor was Nicolas.

As the windshield was closing, the entire interior turned transparent, as if they were floating above the building's roof.

They lifted off, and accelerated away from the building.
But that was more than sufficient for Derictor and Nicolas to observe a purpoise and a polar bear made an appearance on the roof of that building.
The purpoise moved as if it was inside a water body, where in fact it was floating on the air.

Nicolas couldn't keep it down this time,

“ What the hell? What are those?”

“ They're members of the other two intelligent animals on Earth, a Higher Purpoise and a Yeti” said Hendrik.

Nicolas took a moment to digest Hendrik's words, and commented,

“ I assume, the most intelligent animals are us, humans?”

“ No.
There are other intelligent animals on Earth.
Beside the two you see there, is Homo sapiens proreflexi, on which I am supposedly one.
Homo sapiens sapiens like you two is just one of many intelligent species on Earth.”

“ What about Anderson?” asked Nicolas again.

“ Anything else but Earthlings,” Anderson answered.

# Buffer
They return to the building, and collecting all six of the tied individuals in the garage.
Enam send a command, and two autonomous copter like vessel descends toward them.
They look like copters but no noticeable large blades.
They just have a pair of wide, thick, short wing-like appendages from the sides, extending from the roof.
Many hexagonal holes with small blades inside each of them, a fan matrix.
The sound produces were just soft humming.
They fit all six of the captured individuals inside one copter, and they enter the second one.
Both flees automatically to BAIK Facility, Kompleks Hatta, Sumatra Utara.
