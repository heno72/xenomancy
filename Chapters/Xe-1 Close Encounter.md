# Close Encounter

> The Heavenly Hosts on Earth is a state department of the Divine Council of Earth that acts as the Council's right hand.
They enforce the rules set by the Throne via direct interventions.
For all purposes and intent, they are the police force of the Council.

> The members of the Heavenly Hosts are instances of a Power known as Yahweh.
The instances are colloquially called The Yahwehs by reflexiors.
On *feral* humans though, the name is associated as one of the names of an Abrahamic God, often interchangeable with the name *El Shaddai*.
El Shaddai, however, is an entirely separate and distinct Power, that goes by the name *Toru El.*
They have been gone since time immemorial, no one knows where they are.

> The Yahwehs aren't the only ones interfering with mortal realms.
For cases that require only minor interference, many Powers have their own servants to do their biddings.
They are The Angels, mortals recruited to act on behalf of the Powers that command them.

> In some special instances, like accompanying a foreign tourist to their retirement settings, Angels and The Yahwehs may collaborate...

## The Yahwehs

In the middle of a mountainside green line, a black SUV ran through.
The overcast sky was covered in a blanket of darkness.
The SUV's headlights were the only source of lights around.
Ostaupixtrilis Pontirijaris was awakened by a knock to his head.
It was not from anyone, but the result of a SUV speeding through an uneven road.

The only noise that accompanied Ostaupixtrilis Pontirijaris on the back passenger seat was the car's radio.
One of the news piqued his attention:

"A young boy is found dead falling from a fifteen stories tall abandoned building.
A recovered stream from his smartphone left atop the building suggests that he was trying to perform parkour at the rooftop's edge, when he suddenly missed a step...."

"Be ready," said a man on the driver seat, "an angel is ready to do his job anytime soon, on this road."

A woman looked back to Ostaupixtrilis Pontirijaris from the front passenger seat,
"You don't need to remember our names, it is best that way.
After the fall, you will survive, and you will run south.
Keep your path up until you find a narrow walkway.
There you'd meet your next contact."

It didn't take long for Ostaupixtrilis Pontirijaris to recognize who they were.
Members of etoan civilization could easily go through transitive procedures to many kinds of forms and shapes at their convenience.
Being a part of such civilization, he is used to recognize persons not by their looks, but by other attributes inherent to them.
Their gestures, tones, choice of words, and subtle, minute subliminal expressions.
The signatures of both the man and the woman were identical, and it belonged to one entity he recognized as Yahweh, the Lord of the Heavenly Hosts on Earth.

"What would be my new name?" Ostaupixtrilis Pontirijaris asked.

"We are not the one to give it to you.
Just remember, that you and your family went on a road trip.
There was a landfall, you escaped, your parents died, and you ran away in horror.
You found a church, and you couldn't remember where the location of your parents were, and their name, and your name," said the woman.

"I guess that is why this is called *a briefing,*" Ostaupixtrilis Pontirijaris, he chuckled, "it was *very brief.*"

"Just enough for your own purposes," said the man, unfazed, "the less you know, the better the likelihood of you actually completing it as instructed."

"Humans are known for their innate inability to follow the exact instructions," continued the woman, "oh, it begins now."

An entire tree was thrown right above them to the other side of the cliff.
A number of motorbikes followed the fall alongside the tree.
There were a number of motorbike noises from another road slightly above them.
They ran down, and trailed toward the SUV.

The Yahwehs appeared to be inhumanly calm, they didn't budge.
One of the motorbikes got confused, and hit the front of the car.
The male Yahweh raised his hands away from the steering wheel.
The SUV ran aimlessly, with more motorbikes coming toward them unable to avoid it.

At first Ostaupixtrilis Pontirijaris thought that it was shaking because the SUV was shaking.
However, what came next made him realize that the entire ground was shaking as well.
The motorbikes were helpless against the landslide.
It was not like the SUV was any more helpful against metric tons of ground.

When Ostaupixtrilis Pontirijaris realized that the door right beside him was unsecured, it was very late to find a seatbelt to secure him.
He was thrown off the car, experiencing weightlessness again like what he had on the Earth's orbit nearly seven decades ago.
At least seven decades ago, he was able to be sure that he would land safely on Earth's surface.
A landslide, however, gave no assurance that he would land smoothly.
Indeed it wasn't a smooth landing.

Among the stream of dirt, peebles, gravel, and rocks, mixed with sharp broken tree branches,
and what remained of motorbikes and the bodies of riders, he found himself lying face down, half submerged in ground.
He felt warm liquid crawling on his head, his clothes soaked.
The world seemed to shake hard, though his tactile senses told him that he and the ground were at rest.

He crawled out of the ground.
Pain made its way through his entire body.
He could not think of anything, but an inaudible scream inside him.
In fact his entire throat was pulled at such strength, a second later he realized that he was indeed screaming.
His body was shivering, it was both very heavy and very light.
He was barely able to stand upright.
He found a fallen tree, and stood by its side, trying to catch his breath.

The tree didn't look like it belonged there.
The roots were exposed, and the branches bent in such a way it was like they were pulled by themselves into those angles.
It was not the kind of angle you'd get by actually bending the trees.
He could still hear himself crying hard, and his breath was running away, as he struggled to catch the breath even more.
He was barely able to gasp for air.

## The Angels

> The Angels pledge their allegiance to the Powers they serve.
In return, the Powers would provide them exorbitant benefits.
A benefit of which is different from one Power to the other.

> Sometimes, an action demanded by one Power is deemed obstructive by the other Powers.
When it happens in the mortal world, their Angels would fight to protect the biddings of the Powers they pledge their allegiance with.
The Angels, being exclusively reflexiors of great powers, could and would fight with the aid of a fair and indifferent RFL Network.

> To a normal human though, the fight would appear like a fight between mages of great powers.

More motorbikes and bodies were thrown from above the cliffs.
He expected a thud, but a bubble of silence surrounded them, and everything seemed muffled.
Lights flared out from one part to the other.
Explosions seen from afar.

On his feet, Ostaupixtrilis Pontirijaris could feel the ground crumble.
Trees surrounding him started to uproot *themselves*, their branches bent, slithering like snakes.
Bodies they captured were strangulated, and stretched.
Muffled screams could be seen on their victim's faces, completely within the bubble of silence.

The realization that he was then no longer subjected to the protection of Kav's sovereignty, jolted adrenaline glands inside the body of Ostaupixtrilis Pontirijaris.
He ran, and ran away from the scene.
Far from the danger of death, a threat that for the first time in many millennia, felt very real.
A threat that he could have his retirement ends before it even starts.

Fires set, whirled, and exploded around.
It was silent, but a wave of hot air could be felt surrounding him.
The air was suffocating, he grasped for a tree trunk, lying beside him.

A polar bear fell hard on the trunk where Ostaupixtrilis Pontirijaris sat.
Their gazes caught each other briefly.
He recognized the polar bear's gaze.
The polar bear recognized his gaze.

"Xiangyu..." whisphered Ostaupixtrilis Pontirijaris.

The polar bear returned an emotionless gaze.
It turned to the surrounding fire.
A roar later, the fire was drawn to his body, forming a fiery armor.

A roar later, the fire moved away from an open path.
Xiangyu looked at Ostaupixtrilis Pontirijaris, and then to the open path it cleared.
Ostaupixtrilis Pontirijaris didn't wish to waste the help.
He ran through the path, to an area unaffected by the fight.

He found himself hiding behind a pristine tree, not yet bewitched.
Behind him, were dim lights of the forest fire made by the fight.
It was more than sufficient to lit up the surrounding trees,
that would otherwise be completely without any illumination.

The sky was starless, probably completely overcast.
A gust of wind could be felt.
It grew stronger, and the direction was toward the forest fire.
Ostaupixtrilis knew hot air is lighter than cold air.
In the presence of fire, air would be heated.
Hot air would be lifted up, creating negative pressure behind it.
Cold air would be sucked in to balance the pressure.

*A recipe for disaster,* Ostaupixtrilis Pontirijaris thought to himself.
He looked back, and it was a tower of fire.
It *whirled* high above.
*A fire devil,* thought Ostaupixtrilis Pontirijaris.

He looked back, trying to estimate the size and severity of the fire devil.
It probably stretched up to three football fields, soaring as high as several hundred meters.
Temperature inside probably reached up to a thousand and five hundred degree celsius.

"No, it is definitely not a *fire devil*, it is a *fire tornado*," Ostaupixtrilis Pontirijaris was glad that he could escape just in time.
Anything inside *that monster* would definitely be burned into ashes.
In a corner of his mind, he was hoping that Xiangyu could leave the site in time.
It was when he realized, someone else was panting near him.

It was weak, but definitely that of a person.
He gained his strength, and approached the person.
The person was badly wounded.
His clothes were soaked in a black liquid.

Ostaupixtrilis Pontirijaris recognized the smell as that of blood.
The face looked very familiar.
A face he met yesterday, in Alex's law office.

"Hendrik?"

Hendrik opened his eyes, he was still panting.
His thought faded in and out of consciousness, but it was long enough for him to recognize the face.

"Cal,"

"Yes?"

"*Calvin?*"

The name brought Ostaupixtrilis Pontirijaris' thought back to a memory lane.
*That* Hendrik he encountered in his earliest diplomatic mission back in two thousand eleven, was the same Hendrik as the one he met in his grandson's law office.
He couldn't believe that he missed that detail, that they were the same person, only eight years apart.

"No, it can't be like this," said Ostaupixtrilis Pontirjaris, "I shouldn't have met you, *yet*. You're not a part of *the plan.*"

It dawned on Ostaupixtrilis Pontirijaris, that meeting Hendrik far earlier than *the plan*, was a complication.
He was supposed to meet with Xiangyu, the one that would help him out of the battle.
He was supposed to meet with Hendrik, the one that would help his grandson in a battle, *eight years in the future.*

"Did I stray too much from the plan?"

He shivered at the thought that the entire plan would be screwed before it even began.
He shivered at the thought that it would be even more real should Hendrik die today.
Hendrik fell unconscious just then.

"No, *Kav,* no. No, please don't." His feet lose their power.
He found himself kneeling in front of Hendrik.

"No, no, I'm sorry Kav, I'm sorry," his voice broke down in tears, "I-I didn't mean to, I didn't mean to ruin your plan."

"Please, Kav, don't let him die..."

"I won't, please move aside," said Michael Carmichael.

Michael Carmichael's palm straightened, and he aimed it toward Hendrik.

"*We shall meet*...," said Michael Carmichael, lines of gold lights crawled on his palm.
They spread along his arm, and to his chests.

"..., *in the place,*" continued Carmichael.
More lines exploded from his chests.
They crawled back and forth to the arm, and the palm.
In no time his whole hand and chests glowed in bright golden light.

"..., *where there is no darkness*!"

A torrent of golden lights streamed off Michael Carmichael's palm, directly to the chest of Hendrik.
The lights crawled and spread across the body, Hendrik's wounds started to glow in gold.
As if stitching the wounds, the golden lights weaved the wounds, closing them, and in another flash, the wounds were all gone.

For some reasons, Ostaupixtrilis Pontirijaris hoped to see Michael Carmichael to flinch as he healed Hendrik.
Michael Carmichael's face, however, was unfazed.
Hendrik coughed up, clearing the liquids off his lungs, then his breaths stabilized.

Michael Carmichael glanced at Ostaupixtrilis Pontirijaris as the glow faded from his arm.
He aimed his straightened palm toward Ostaupixtrilis Pontirijaris.
Ostaupixtrilis Pontirijaris somehow understood that it was just a normal operation by a WTF officer.
Feral humans weren't supposed to perceive any magic-like manifestations.
It dawned on Ostaupixtrilis Pontirijaris, that by then he was a *feral* human.

"What a peculiar *feral* you are, you got no SOUL," remarked Michael Carmichael, "who are you?"

"Before you do that," said Ostaupixtrilis Pontirijaris.
It was clear then, for him, that it was an answer to his prayers.
It was an attempt by Kav to fix up his undoings.
The entire retirement plan of his was just a way for Kav to contain the damages he did to *the plan.*

"Please, at any cost, not now, for at least the next eight years, don't mention anything about me to him," he continued.

Michael Carmichael twitched his eyebrows.
"Sure, why would I mention you?"

It dawned on Ostaupixtrilis Pontirijaris, what The Yahwehs said earlier.
The less he remembered, the more likely he'd successfully accomplish the task.
The task was to retire.
The aim was to limit the damage he did.

Michael Carmichael was inspecting Ostaupixtrilis Pontirijaris intently.
An eyebrow twitched in his face.

"A SOUL-less human, means normal amnestics wouldn't work for you.
I'm sorry, poor fella, I'd have to use a more invasive technique.
Might get you a brain damage, but that's the most *humane* way to do it."

"Wait, *brain damage*? What do you m- uh-" Ostaupixtrilis Pontirijaris found himself petrified.

"*Who controls the past,*" Michael Charmicael started, "*controls the future.*"

Bright tendrils of light wrapped around Ostaupixtrilis Pontirijaris' body.
It wrapped tighter, he couldn't move a muscle.
His breath stopped, his eyes gazed in fear.

"*Who controls the present,*" continued Michael Carmichael.

The last thing Ostaupixtrilis Pontirijaris recalled about Michael Carmichael was his emotionless, indifferent gaze.
The last sentence he heard was:

"..., *controls the past.*"

Following that was silence.
Michael Carmichael proceeded by carrying Hendrik on his back.
He was about to leave as Xiangyu came in his fiery armor.
The fiery armor instantly faded out, leaving only colorless furs fuming with smoke.

"Is Hendrik okay?" Xiangyu inquired.

Michael Carmichael nodded.

"What about *that* human? Who is that?" Xiangyu's snout was aimed at an unconscious normal human boy.

"I have no idea. We'll just leave him here."

## Behind him was the firmament

A boy found himself in the middle of the woods.
His feet started to pace like they had their own thoughts, moving through the forest floor.
Wherever he was going, he knew he went south, toward the direction pointed by the Southern Cross.
About five times its cross length, is the approximate point of the south.
However he was in such pain he could not embrace the beauty of the starry night.
The kind of looks he'd never get in a city.

On how he would know how to read stars, he wouldn't know.
He couldn't.
What he remembered was that he was involved in some sort of a car crash.
His parents must've died.
His head must've been hit so hard, he forgot almost everything.

All he knew was that he must go south, until he found a walk.
He kept walking and walking.
His steps stopped as he crossed a walk.

He'd just follow the walk, he guessed.
The walk brought him into a wooden church.
He was about to knock, where his head hit the door first.

A man inside heard something at the front door.
He was certain it sounded like a thud.
When he opened the front door, he found a young boy lying on the floor.

His body was full of dirt, and burns on his clothes.
*Must be a survivor of the forest fire,* he thought to himself.
He lifted the boy up, and brought him inside.

Dirts and charcoal stains collecting on his black cassock didn't bother him much.
When the boy twitched an arm, and his elbow knocked his white clerical collar, the man let out a sigh.
"I just washed this collar yesterday," the man said, his laughter grew in disbelief.

He spent the entire night cleaning the wounds of the boy.
He wiped clean all the dirt off his body.
He recalled a box of donated clean clothes at the storage.
*Giving this boy a couple of clean clothes wouldn't be a problem*, he thought to himself, *it's literally giving to someone in need.*
He chuckled.

The entire pain the boy endured during the evening wasn't that easy for his body to take.
The entire scene was replayed in his dreams, only with annoyingly surreal alterations.
The trees he sat on waved and warped, crawled and carved on the ground, there were howls.
The ground broadened and tilted up, he was dragged along, and the trees rolled.
A rolling trunk was about to squash him.

His body lightened and saw the entire ground turned into a vertical wall.
Everything on its surface fell down below.
Behind him was the firmament, full of stars.
They glowed in a lively manner.

They pulsed.
Some switched off, while the other switched on.
Then they switched back on, and the other switched off.
They continued alternately in a rhythm.

He tried to locate his home star.
However he found himself in the middle of space.
The Earth started to shrink beneath him.

No, he was pulled away from Earth.
At such speed, his breath was barely able to catch him.
He was out of breath.

For a moment he could almost see Aucafidus.
It was in his arm's reach, despite its immense size and distance away.

"Finish it well and return here."

A voice he recognized, so familiar, so comforting.
Yet the memory of the speaker was out of his mind's grasp.
He almost cried.
No, he actually cried.

"I will return, I will return," he muttered.

"You will not be alone," the voice said again.

His hand reached to grasp Planet Aucafidus.
He shrunk fast and to the surface, finding a woman figure with an obscure face.
She opened her welcoming arms, longing for his chest to close the gap between them.
Everything around him started to lose clarity.
They blurred.

His heart rushed as everything tore apart into a white nothingness.
He made his heart to focus on the obscure woman.
With all of his strength, he reached for a grasp of her face.
A kiss landed on her smooth and warm lips.
Nothing else mattered for him.

His tongue explored her mouth cavity, her teeth.
Their tongues met each other, intertwining, while his entire body was pulled back, hard.
A force pushed their chests apart, hard.
He closed his eyes.

His lips were on her lips still, never separated.
Their body kept separating, pulling him hard away from Aucafidus.
He did not see with his eyes, but in his heart's eyes, he could see the stars warped around him.
The sky was no longer the sky of Aucafidus.
It was the sky of Earth.

The Earth pulled back on him hard, away from Aucafidus.
He was struck hard to Earth, but not to the ground.
It was in a bed, and a muffled sound could be heard, while his body took beatings from unknown sources.
He opened his eyes.

He was in a bed, and in front of him was a priest.
The priest's eyes glared, jolted between the young boy's eyes, and his mouth.
Their mouths were joined.

The Priest had been trying fruitlessly to release his face from the young man's strong grasp.
The young man released his grasp in confusion.

The priest was thrown back, full force.
Not because of the young man, but because of the priest's own jerks.
And everything turned silent in between them.

## I am sorry for the inconveniences

The priest was tidying up the scattered set of clothes beside them.
He was supposedly going to put the clothes on the boy.
However it appeared that the young man had to wear them on his own then.

So did the boy wear the clothes.
The priest, still frigid, gazed at the boy.

"I, I am Christopher," opened the priest, "Christopher Lee."

The priest decided to pull the fallen chair back to the bedside.

"A priest-in-charge at St. Lusien's church, in Ijen Crater area," said the priest, "you can call me Father Chris."

"I am sorry for the inconveniences," the young man said.

He was going to say his full name to Christopher Lee.
That he was also Christopher, Christopher Agape Lumintan, or what his friends call him: Cal.
However, his name was like memories of the obscure woman in his dream, they felt like they were reachable.
However those memories slipped out of his mental reach.
He had his name forgotten.

"I, I don't remember my name,"

There was silence,

"Pardon?"

"Father, I don't remember my name."

"Then, what should I call you?"

"I don't know."

"Where did you come from? Were you from the north?"

"I don't know."

There was another batch of silence.

"Then, I guess, until you regain your memory, you could stay here."

"How should you address me then?"

Father Chris had his thoughts traveled to his twin brother, Charles, who was then in a peacekeeping campaign near Sudan.

"Then, for the time being, I'd call you Charles."

"Charles Lee it is," said the boy.

