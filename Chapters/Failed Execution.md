# Failed Execution

## Antichrist

> In-story time is 20210815 1500

Anderson notified about possible Antichrist by Etovexiri.
He decided to take the matter all by his own.

## Bomb

> In-story time is 20210816 1800

Anderson put an explosive to corporate's car that Henokh is going to take.

## Missing Bomb

> In-story time is 20210817 0300


Anderson checked the bomb again at post midnight, but found out that the explosives had been removed.
Aditya faced Anderson.

Anderson asked Aditya to return the explosive, and they went into a fight.
Involved in a fight with Aditya, Anderson got hurt.

## Signs

> In-story time is 20210817 0400

Derictor wakes Henokh from his room, and Manov is present.
There's Sylvia, Henokh's wife.

Henokh's still not used of Manov's appearance, that can appear and disappear like ghost.

Derictor told Henokh about intruder alert in Building B Kukercorp, by Kuin.
Doctor Bain (Agent Lima) is there, along with Anderson.
Hendrik was on the way to the site, and Ein was ready with Oneiric Protocolers on the site.
From security feed Kuin sent, it was Aditya that faced Anderson.

Henokh was close with Aditya, Derictor want to let Henokh know that it was related to Henokh, and that they're closest to the scene, where they could actually do something with it.

## Surround
> In-story time is 20210817 0430

Henokh, Hendrik, Derictor, Jokowi, Empat and Sembilan assembled near Building B, Henokh divided the teams.
Manov on Alpha Team, with Jokowi and Sembilan, concerned solely on securing Agent Lima.
They were going to northern parking lot, to server room on the third floor.
Ein on Beta Team, with Empat and Delapan, concerned on finding and detaining Aditya.
They would approach from eastern road.

## Kamaitachi
> In-story time is 20210817 0445

Beta Team, Ein, Empat and Delapan walked from eastern road to enter the building.
Empat got his arm and leg severed, all of the sudden.
Ein shielded them and get them to safety, and noticed displacement of air.
Ein spotted a volant, and with a spell inducing the seal of Hadad (The Sun Cross), identified it as Kamaitachi.
He then proceeded his first try to defeat Kamaitachi, but then he discovered that Kamaitachi's tentacles sting his body as well, and probably for the first time in his life, he experienced physical pain.
Pyrokinesis didn't work well, as Kamaitachi's tentacles are too numerous and well-spread, almost like a spider web.
If he were to set fire on a strand, it detached the string, and proceed with another set of strings.

Ein decided to call Xiangyu, and a huge blast of flame came from the sky after some delay.
He tried to surround Kamaitachi with earth, but Kamaitachi are almost like a cloud of slippery strings, it unfurled and refurled somewhere else.
Ein stinged even more by several threads of the strings.
The sharkhead advanced toward Ein, and a pilar of flame formed in between Ein and Kamaitachi, a bear figure thrusted toward the open mouth of Kamaitachi and a stream of flame exploded in its mouth, dispresing the threads for some time before it remanifested.
Xiangyu complained that it is quite unexpected for Ein to call him to deal with this kind of nuisance.
Ein protested that the being is just impossible to defeat, it is just another volant, and he is not Manov.

> Note that it is a possible insert point of Barong Ket.
