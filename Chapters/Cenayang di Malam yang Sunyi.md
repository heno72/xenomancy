# Cenayang di Malam yang Sunyi

Semua dimulai dengan sebuah pusaka.
Pusaka yang saking saktinya dapat membuka gerbang neraka.
Pusaka yang telah ada di dunia sejak Kain dikutuk karena telah membunuh Habel.

Malam itu dingin, dan langit cerah.
Rick menarik nafas yang dalam.
Ia melihat ke langit sembari menghembuskan nafasnya.
Jutaan matahari yang jauh di seberang jagad raya memandang balik padanya.

Ia tersenyum, langkahnya bertambah ringan.
Hari ini tidak ada Manov, kembarannya yang usil itu sedang sibuk dengan peperangan di jagad seberang.
Tidak ada pula Ein, kembarannya yang berhati dingin itu sedang sibuk mengurusi serangan gaib di Kawah Ijen.
Rick sedang sendiri, dan dia menikmatinya.

Hanya satu hal yang mengusiknya: indera keenamnya.
Setiap langkahnya, setiap embusan nafasnya, setiap kerikil yang ia lewati, setiap serangga yang merayap di tanah.
Semuanya terlihat jelas di kepalanya.
Menutup mata, menutup telinga, satupun pun tidak ada gunanya.
Dunia tampak sama jelasnya dengan atau tanpa matanya.

Banyak orang ingin memiliki indera keenam.
*Omong kosong* ketus Rick, punya indera keenam seharusnya juga punya cara untuk menutupnya.
Seperti kita menutup mata, atau menyumbat kuping kita.

Telapaknya menutup rapat telinganya, instingnya mengatakan dengan demikian semua gemuruh rayapan serangga dan berbagai hewan melata, semua alunan tarian dari dedaunan di seberang jalan, hilang.
Tapi insting itu bekerja untuk orang yang tidak punya indera keenam.
Suara-suara berisik itu sama jelasnya dengan atau tanpa menutup telinganya.

Ia mengerutkan alisnya, matanya tertutup rapat, dan ia bergeming di trotoar.

"Aku mau pulang," kata Rick.

Pada saat itu, gawainya berdering.

Lalu berdering.

Dan berdering lagi.

Dengan satu helaan nafas, ia mengambil gawainya dari saku celana.

"Ya, Sukarno di sini."

"Enam di sini.
Klandestin dapat info, salah satu pegawai Kukercorp adalah anggota Laskar Umbra Cahaya.
Investigasi itu."

"Tidak bisakah besok saja?
Saya lelah."

"Hari ini kesempatan terakhir.
Besok adalah jadwal perawatan komputer di kantor.
Berkas-berkas sampah dan berkas-berkas yang sudah tidak terpakai dari komputer-komputer di kantor akan dihapus bersih.
Kau ada bawa *K-Drive*?"

Rick tersenyum, *K-Drive* adalah alat khusus berbentuk *flash disk* yang dapat mengekstrak data dari komputer yang dihubungkan dengannya.
Alat itu, tidak ada sama Rick saat itu.

"Sayangnya tidak," jawab Rick.

"Sudah kuduga," kata Enam, "mobil telah dikirim ke tempatmu, di dalamnya ada *K-Drive* dan alat-alat lain yang mungkin kau perlukan. *Good luck*!"

Panggilan terputus.

"Sial, tidak bisakah saya istirahat sehari saja?"

Siang hari dirinya dikenal sebagai Rick si pengacara.
Malam hari dirinya dikenal sebagai Sukarno, anggota Satuan Tugas Psi-72, Rubah Ekor Sembilan.
Pada saat pulang rumah, dia harus berhadapan dengan dua kembarannya, Manov si Hantu, dan Ein si Malaikat.

Hanya saat-saat seperti inilah ia bisa menarik nafas sesaat.
Saat ia sedang berjalan sendirian di malam hari, hanya ditemani bintang di langit dan trotoar semen yang dingin, ia merasa bisa menjadi dirinya.
Jika saja ia tidak memiliki indera keenam, semua akan lebih sempurna, pikirnya.

Enam telah memperhitungkan segalanya, seharusnya.
Selalu seperti itu.
Entah darimana Enam punya waktu untuk mendapat semua informasi itu, mengumpulkannya, dan menyusun rencana operasi.
Sehari-harinya Enam bertemu berbagai orang, dalam berbagai identitas.
Nyaris tidak pernah Rick melihat Enam beristirahat, tapi wajahnya selalu segar dan selalu siap mengambil identitas baru.

Rick berharap bisa seperti Enam, tapi mustahil manusia biasa dapat membagi waktu.
Mustahil meskipun Rick memiliki indera keenam.
Indera yang memberitahukannya bahwa sebuah mobil mendekatinya.
Mobil otomatis tanpa pengemudi, yang sebentar lagi akan masuk dalam pandangannya setelah berbelok dari perempatan di ujung komplek.

Mobil hitam tersebut menyediakan banyak perlengkapan yang telah disiapkan Enam.
Sebuah tas perlengkapan, dua *soargun*, empat borgol semi-otomatis, empat *ornithopter* mini seukuran bola golf, sebuah kacamata pintar, dan sebuah flash disk, *K-Drive*.

Tas perlengkapan diisinya dengan borgol, *ornithopter* mini, dan sebuah flash disk.
Kacamata pintar dikenakannya, dan kedua *soargun* digantung di punggungnya.
Tas tersebut ia letakkan di kursi penumpang.
Setelah selesai dengan persiapannya, ia duduk di kursi pengemudi.

Terdapat pedal gas, rem, dan kopling.
Roda kemudi, dan tuas transmisi pula di mobil tersebut.
Namun mobil tersebut memilih untuk bergerak sendiri.

Rick tidak masalah dengan itu, sebab itu memberinya waktu untuk bersiap-siap.
Segala perlengkapan fisik Rick sudah siap, sehingga langkah berikutnya adalah mengecek perlengkapan non-fisiknya.
Tangannya ia lambaikan di udara, dan sebuah mandala yang terdiri dari kerlap kerlip seperti debu di udara terbentuk.

Mandala tersebut dikerangkeng dalam sebuah cincin redup.
Pada bagian bawah, terdapat kerlap kerlip kuning terang.
Mungkin tidak sampai lima derajat saja dari keseluruhan cincin tersebut yang berkerlap-kerlip kuning.

"Rendah sekali, pasti kerjaan si Ein," Rick menepuk jidatnya.

Ein, salah satu kembarannya yang berdarah dingin dan penuh perhitungan, selalu bertarung habis-habisan.
Rick yakin nyaris semua kuota bersama mereka hari itu telah dihabiskan oleh Ein pada misinya di Kawah Ijen.

Biasanya, hal itu bukan masalah.
Rick tidak perlu menggunakan kekuatannya sepanjang hari.
Manov, kembaran Rick yang satunya lagi, tidak memerlukan banyak kuota, ia cukup efisien memanfaatkannya.
Tapi dengan misi yang sedang dijalani Rick saat ini, ia berharap Ein sedikit lebih berhemat.

Rick tersadar dari lamunannya, mobil ini telah berjalan dari tadi, dan kini telah tiba di tujuannya.
Mobil tersebut berhenti tepat di samping sebuah gedung.
Tidak ada jendela di sisi gedung itu, hanya ada tembok beton yang bahkan belum dicat.

Rick merenggangkan tangannya, dan pundaknya.
Hatinya terasa kecut saat ia mengingat keborosan Ein.

"Mesti berhemat," gumamnya.

Rick keluar dari mobil, memasang tas perlengkapan ke sabuknya, dan memandangi dinding beton tidak bercat tersebut.
Ia menghitung, ruang server ada di lantai tiga.
Seharusnya lantai itu ada di antara sembilan dan dua belas meter dari tempatnya berdiri.

Ia menutup matanya, dan dahinya berkontraksi.
Satu tarikan nafas, dan satu loncatan.
Sekejap mata saja, ia telah berada di udara, sekitar sepuluh meter dari atas tanah.
Segera ia sentuh tembok beton di depannya dengan kedua tangannya, kakinya lalu ikut menempel.

"Gaya cicak," Rick terkekeh, "seharusnya ini tidak makan banyak kuota."

Dinding itu dingin, agak lembab.
Permukaannya terasa padat dan kokoh, agak kasar.
Selain itu, Rick dapat merasakan jutaan benang karbon tak kasat mata yang bekerja sama menjembatani permukaan kulit telapak tangan Rick dan dinding tersebut.
Demikian pula dengan sol sepatunya yang menempel di dinding.

Ia perintahkan benang-benang karbon di telapak tangan kirinya untuk melepaskan diri dari dinding.
Tangannya yang terbebas itu ia ayunkan di udara, dan sekelompok benang-benang karbon yang melayang di udara tersusun sambil menyala.
Gambaran mandala yang terdiri dari debu gemerlapan, dikelilingi oleh cincin redup pun muncul.
Bagian yang berpendar kuning terang masih sama besarnya, belum berkurang sama sekali.

Senyumnya merekah, tangan kirinya ia tempelkan kembali ke dinding, dan ia pun menutup mata, berkonsentrasi.
Benang-benang karbon tersebut tidak hanya ada di tangannya.
Mereka ada nyaris di mana-mana.
Di mata batin Rick, ia bisa melihat dan merasakan apapun yang dapat dilihat dan dirasakan oleh benang-benang karbon tersebut.

Ia tidak hanya dapat melihat dan merasakan benang-benang karbon tersebut, ia juga dapat memerintahkan mereka untuk menggerakkan benda.
Ia berfokus ke satu benang.
Satu benang itu menyentuh benang lainnya, dan kedua benang itu menyentuh benang-benang lainnya.
Tidak butuh waktu, milyaran benang karbon berada dalam kendali Rick.

Satu per sepuluh ribu dari seluruh massa di permukaan bumi adalah benang-benang tersebut.
Setengah kilometer ke atas dari permukaan apapun di Bumi, sampai dengan setengah kilometer di bawah tanah, dan semua benda yang berasal dari tanah, semuanya dikerumuni oleh benang-benang tersebut.
Tidak ada satupun, benda sebesar apapun, sekecil apapun, badan manusia sekalipun, yang luput dari benang-benang tersebut.

Rick, hanya perlu memerintahkan mereka.
Dengan konsentrasi yang cukup, apapun dapat dilakukannya.
Selama kuotanya tidak habis.

Setengah berharap apa yang akan dilakukannya tidak akan memakan banyak kuota, ia memanggil benang-benang dalam kendalinya.
Ia berkonsentrasi ke benang-benang yang ada dalam beton tersebut.
Terdapat konsentrasi benang-benang yang berbeda-beda dalam tembok, dengan beberapa bagian yang padat dengan benang-benang membentuk pilar-pilar.
Mereka adalah benang-benang yang berada dalam kerangka logam yang ditanam dalam beton.
Logam lebih padat daripada beton, karenanya lebih banyak benang-benang tersebut di sana.
Beton berkerangka logam umumnya digunakan untuk bagian struktural gedung, bagian-bagian yang perlu diperkuat agar tidak mudah rusak.

Ia mencari bagian tembok yang tidak banyak logam tertanam di sana.
Bagian-bagian non-struktural yang walaupun dibongkar, tidak akan meruntuhkan gedung.
Setelah ia dapati bagian yang sesuai, ia cukup memerintahkan benang-benang di sana untuk mulai mendorong.
Dengan sedikit variasi dari tekanan dorongan benang-benang tersebut, ia dapat menyesuaikan bentuk retaknya tembok tersebut.

Tembok tersebut runtuh ke dalam membentuk terowongan silinder.
Kerikil-kerikil sisanya masih menempel dalam terowongan tersebut, masing-masing mereka masih terikat dengan benang-benang karbon tersebut.
Rick berencana mempertahankan bentuknya seperti itu, agar nanti ia dapat mengembalikan tembok tersebut seperti semula setelah semua misinya selesai.
Tapi ia tidak dapat terus berkonsentrasi ke tembok ini selama ia menjalankan misi.
Harus ada cara lain.

Ada yang bilang, setiap orang terlahir di dunia dengan kembaran gaib mereka.
Kenyataannya, kembaran gaib tersebut hanya bayangan.
Bentuk semu dari tiap orang, yang terbentuk karena distribusi benang-benang karbon yang menyelinap dalam setiap sentimeter kubik darah, daging, dan tulang.
Benang-benang karbon tersebut mengumpulkan informasi dan terus membaca keadaan badan.
Informasi yang terbaca oleh benang-benang itulah yang umumnya disebut sebagai jiwa.

Orang-orang seperti Rick, dapat berkomunikasi dengan benang-benang tersebut.
Tapi Rick agak berbeda, ia juga dapat berkomunikasi dengan jiwanya sendiri, termasuk memanipulasinya.
Jiwanya kemudian membelah, belahannya itu ia perintahkan untuk mempertahankan mantera tertentu atau memanipulasi lingkungan dengan cara tertentu.
Dalam kasus ini, perintahnya adalah mempertahankan bentuk kerucut tembok tadi.

"Aduh, membelah jiwa itu mahal," Rick menepuk jidat.

Segera ia mengecek meteran mandalanya, dan tampak kilap kuning terang berkurang sekitar satu derajat.
Melalui mata batinnya, ia menyadari resolusi indera keenamnya mulai berkurang.
Semuanya bagai kabut, sebagian agak padat, sebagian lain agak lowong.
Di ruangan gelap tersebut, pandangan batinnya hanya dapat melihat beberapa volume kabut yang agak padat, sepertinya perlengkapan logam.
Tembok sepertinya mengelilinginya sekitar empat meter dari tempatnya berdiri.
Ia tidak dapat membedakan tebalnya tembok tersebut, dan ia tidak dapat membedakan beberapa furnitur dan perlengkapan logam di ruangan tersebut dari gagang pintu atau engsel.

Terkekeh, ia hanya dapat bergumam, "apanya yang berhemat."

Ia menghembuskan nafas, menggeleng-gelengkan kepala.
Saking rendahnya sisa kuotanya, ia tidak dapat lagi mengandalkan indera keenamnya.
Untuk pertama kalinya setelah sekian lama, ia harus mengandalkan indera lahiriahnya.

Tangannya ia bentuk seperti sedang memegang bola imajiner, "*fiat lux orbis*," dan sebuah bola cahaya putih terbentuk, melayang di tengah-tengah bola imajiner tersebut.

Ia berada di dapur.
Pintu ada di samping tempat cuci piring.
Satu tangannya memegang bola imajiner yang berisikan cahaya tersebut, sementara tangan satunya membuka gagang pintu.

Koridor.
Panjang, gelap, dan pintu-pintu berjejeran.
Rick melempar pelan bola imajinernya ke depan.
Cahaya tersebut melayang dan terus melaju ke depan.
Satu hentakan jari Rick membuat bola tersebut berubah arah, dan hentakan jari lainnya membuatnya melompat ke arah lain.

Rick mencoba membuat peta ruangan tersebut dengan apa yang terlihat dari bola tersebut.
Ia pun melangkah setelah bola tersebut berhasil melihat ruangan kantor.
Ruangan yang tersekat-sekat menjadi kubus-kubus meja kerja pribadi para pegawai.
*Penjara kapitalis,* gumam Rick dalam hati.

Ia baru saja teringat bahwa kacamata pintarnya dilengkapi mode penglihatan malam.
Tawa meledak dari dadanya, namun ia berusaha menahannya.
Seandainya saat ini lampu ruangan tersebut menyala, dapat terlihat betapa merahnya wajah Rick, menyadari betapa *pintarnya* dia tidak mengaktifkan fitur tersebut sedari tadi.
Tampaknya ia selalu gagal berusaha menghemat kuotanya.

"Sepintar-pintarnya suatu gawai, penggunanya juga harus pintar," gumam Rick, ia mulai menjelajahi kantor tersebut.

Belum lewat lima menit Rick menjelajah, ia menyadari lagi kurang pintarnya dirinya.
Ruangan itu dilengkapi kamera pengawas.
Nafasnya berhenti.

"*Binatang*," umpatnya, diikuti dengan "*melata*, adalah binatang yang berjalan dengan perutnya menempel di tanah."

Kamera digital, seperti yang ada di gawai, dan kamera digital yang dijual di pasaran, dapat melihat cahaya dengan panjang gelombang maksimal sekitar seribu nanometer, panjang gelombang *near-infrared.*
Mata manusia hanya dapat melihat cahaya sampai dengan panjang gelombang tujuh ratusan nanometer, panjang gelombang warna merah.
Akibatnya, melalui kamera gawai, kita dapat melihat LED remot inframerah menyala ketika kita memencet tombolnya, meski dengan mata telanjang kita tidak dapat melihat bedanya.
Kamera profesional, pada umumnya dilengkapi dengan filter inframerah, karena cahaya inframerah tidak dapat difokuskan, dan dapat merusak kualitas potretan.

Kamera pengawas, di sisi lain, tidak mempedulikan resolusi atau kualitas gambar.
Beberapa kamera pengawas bahkan dikhususkan dengan LED inframerah sebagai penerangan, sehingga bahkan di tempat tergelap sekalipun, kamera pengawas tersebut dapat melihat dengan jelas.
Itu, adalah titik lemah yang Rick dapat manfaatkan dalam kondisi ini.

Segera, ia memerintahkan benang-benang karbon yang mengelilinginya untuk berpendar cerah dalam inframerah.
Bagi kamera inframerah di gedung tersebut, ia hanya titik cahaya terang yang tidak berbentuk.
Ia sedang berpikir betapa *cerdasnya* dirinya tidak melakukannya dari tadi.

Hingga ia sadar bahwa ia dapat melihat pendaran tersebut dengan kacamata cerdasnya.
Kacamata cerdasnya memakai kamera inframerah untuk fungsi penglihatan malamnya.
Pendaran cahaya yang barusan dibuatnya untuk mengelabui kamera pengawas di gedung, juga mengelabui kacamata cerdasnya.

Terkekeh, ia hanya dapat bergumam, "*back to square one*."

Rick melihat bola cahaya yang tadi dibuatnya masih melayang begitu saja di ruangan tersebut.
Ia pun lanjut menjelajahi kantor tersebut.
Di kepalanya, terus menerus ia mengingatkan dirinya untuk tidak menceritakan kebodohannya ke Ein dan Manov.

Satu komputer dalam mode standby, dan tidak dipasangi kata kunci.
Seseorang pasti tadi terburu-buru untuk pulang, ia tidak memastikan komputernya mati dengan baik.
Komputer itu Rick jalankan, dan ia mematikan antivirusnya.
Berikutnya, ia mencolok *K-Drive* ke port yang tersedia, dan mulai mengunduh data dari komputer tersebut.

Dari lubuk hatinya yang terdalam, ia merasa misi ini terlalu membosankan.
Sempat terbersit di pikirannya, akan lebih seru kalau ada musuh yang datang, dan ia harus bertarung agar memastikan misinya sukses.
Ia hanya terkekeh, menyadari dirinya terlalu sering menonton film aksi espionase.
Ia terkekeh juga dengan fakta bahwa di banyak film espionase, mereka menggunakan istilah 'agen,' misalnya 'James Bond, Agen 007.'
Kenyataannya, agen hanya digunakan untuk merujuk aset dari petugas intelijen.
Aset lokal seperti informan, atau orang bayaran yang entah ditugaskan atau dibodohi petugas intelijen untuk mengumpulkan data.

Ada yang tidak beres.
Ada sinyal yang diterima oleh jiwa Rick, dan disampaikan ke lobus parietal belahan kanan otak Rick.
Lokasi tersebut adalah kluster reflexius, terletak di area Broca yang umumnya tidak aktif di otak orang-orang normal yang tidak kidal.
Di area yang sama di belahan otak kanan, area tersebut adalah area yang dikenal sebagai pusat bahasa.
Area tersebut memungkinkan kita berbicara.
Pada orang kidal, area Broca di belahan otak kanan menjalankan fungsi tersebut, namun area Broca di otak kiri mereka yang gantian nonaktif.
Pada orang-orang seperti Rick, area Broca di otak kiri dan kanan aktif bersamaan.
Yang satu berfungsi normal untuk berbicara, sementara yang satunya lagi menjadi jembatan antara diri mereka dan jiwa mereka.

Jiwa Rick yang berupa perangkat lunak yang berjalan dalam benang-benang karbon tak kasat mata dimana-mana, berinteraksi dengan benang-benang karbon di luar badan Rick.
Interaksi tersebut diproses, dan disampaikan ke otak Rick, dan Rick mulai memahami makna sinyal tersebut.
Pemindahan tiba-tiba benang-benang karbon tak kasat mata di udara terjadi di lantai di bawah Rick.
Benang-benang tersebut berteriak, teriakan yang hanya dapat didengar oleh orang-orang seperti Rick.

Rick mencoba berkonsentrasi.
Dirasakannya suatu kabut menyerupai tiang, tinggi sekitar seratus tujuh puluh sentimeter, diameter tidak lebih dari satu meter.
Tidak hanya satu, terdapat beberapa tiang-tiang seperti itu.
mereka bergerak ke tangga menuju lantai tiga.

Rick tidak berkata apa-apa, dirinya tahu ia harus segera mematikan bola cahayanya.
Badannya kaku, bergeming.
Ia berada dalam keadaan *everyday trance*.
Pikirannya melakukan pencarian transderivasional untuk mencari makna apa yang ia rasakan.
Ketika pikirannya gagal memahaminya, ia lanjut melakukan pencarian transderivasional untuk mencaritahu kenapa ia tidak dapat memahaminya.

Tiang-tiang itu sekarang ada di lantai tiga, dan mulai mendekati kantor tempatnya mematung.
Meter demi meter, jarak antara tiang-tiang itu ke tempat Rick semakin dekat.
Salah satunya mulai mendekati pintu, dan Rick berseru setengah berbisik, "*fiat umbra*."

Mantera itu mengakibatkan bola cahaya yang dibuatnya menjadi bola kabut hitam.
Seharusnya ia mengucapkan mantera *fiat nil lux*, yang akan menghilangkan bola tersebut.
Atau, tepatnya ia akan melakukan hal tersebut, tapi bagian lain pikirannya, bagian yang menang, berpikir untuk menggelapkannya saja.
Peperangan solusi terjadi dalam pikiran Rick tadi, yang oleh para psikolog dikenal dengan Efek Stroop.
Pada kasus Rick, solusi yang salah-lah yang menang.

Rick tunduk bersembunyi di salah satu meja kerja, ketika ia mendengar pintu terbuka.
Sediam mungkin, ia mematikan layar komputer yang tadi dibukanya.
Sekilas ia mengecek lampu *K-Drive* yang berkelap-kelip tanda ia sedang bekerja.
Melalui gawai-nya, terdapat notifikasi bahwa butuh sepuluh menit lagi untuk selesai menyalin data.

Tiang-tiang tersebut mulai menyebar dan menjelajahi ruangan.
Pada jarak sedekat ini, Rick mulai dapat merasakan bentuk kabut-kabut berbentuk tiang tersebut.
Tiang tersebut memiliki tangan dan kaki, sepertinya manusia.
Ia mulai menghitung, setidaknya ada enam hingga delapan orang.
Masing-masing memegang suatu objek metal, bengkok.
Sepertinya pistol.

Perlahan, ia mengecek meteran mandalanya.
Tidak cukup.
Kuotanya tidak cukup untuk mengalahkan enam hingga delapan individual bersenjata.
Ia mengecek notifikasi gawainya, masih ada delapan menit.
Ia harus mengulur waktu.

Empat *ornithopter* di tasnya.
Hal itu terlintas di pikiran Rick.
Ia mengambil empat *ornithopter* tersebut dan melemparkannya sejauh mungkin dari posisinya.
Dari kejauhan, empat *ornithopter* teraktivasi, dan mulai mengepakkan sayap-sayap plastik mereka.

Rick menjalankan kacamata cerdasnya, dan kaca proyektor di sisi kiri atas pandangannya pun menampilkan tampilan yang dilihat oleh salah satu *ornithopter*.
Dengan usapan jari di bingkai kacamatanya, ia mengganti pandangan *ornithopter* aktif, dan ia dapat melihat delapan orang.
Mereka mengamati balik ke empat *ornithopter* yang tiba-tiba muncul itu.
Mereka berbisik-bisik di antara mereka, dan salah satu dari mereka memerintahkan untuk mulai menyebar di kantor, baris demi baris meja mereka sisiri.

Rick melihat dua orang dari mereka berdiam di salah satu komputer yang jalan, mereka juga sedang menyalin data.
Seorang lainnya mengamati suatu bola kabut yang melayang di udara.
Ia meneriakkan sesuatu ke kawan-kawannya.
Mereka semua melihat ke bola kabut itu.

*Ini kesempatanku* pikir Rick, "*FIAT LUX MAXIMUS!*"

Matahari mini bersinar cerah di ruangan tersebut, membutakan sesaat kedelapan orang tersebut.
Rick mengaktifkan mode siang hari dari gawai pintarnya, dan kacanya menggelap.
Dua orang terdekat dari Rick, ia tumbangkan.
Tak lupa ia memborgol tangan mereka dalam beberapa gerakan gesit.

"Sisa enam," gumam Rick.

Kontak tembaga dari perangkat mereka yang menempel di komputer, dapat Rick rasakan setelah berkonsentrasi.
Satu perintah mental cukup untuk mementalkan perangkat mereka dari komputer yang mereka buka.
Beberapa dari mereka sudah mulai terbiasa dengan cahaya terang dari matahari mini tersebut, mendengar suara flash disk mereka terlempar.
Mereka juga menyadari seorang pria yang berdiri di dekat matahari mini tadi.
Pistol mereka diarahkan ke Rick.

Tembakan.
Rick melompat, berusaha menghindar.
Film-film aksi tidak menampilkan suara tembakan dengan akurat.
Suara tembakan yang sesungguhnya, terdengar seperti udara ikut tersobek.
Sobekannya tajam, dan cukup memekakkan telinga.
Cukup untuk menutupi rasa sakit dari goresan peluru di pundaknya.

"Ein, sebagai anggota Ikrar Trinitas," rapal Rick, "pinjamkan saya kekuatanmu!"
Tato api hitam menyebar dari pelipisnya.
Pola api hitam tersebut menyebar ke dahi dan dagunya.

Para musuh mulai mengokang senjata mereka, dan deretan tembakan kedua mulai terdengar.
Rick mulai menghindar, sambil melakukan gerakan memutar dengan tangannya.
Kini bukan lagi benang-benang karbon yang Rick kendalikan.
Kekuatan Ein memungkinkannya mengendalikan jenis benang yang lebih langka, yang hanya dapat dikendalikan oleh para *pyromancer*, para pembisik api.
Filamen-filamen yang terbuat dari boron-karbon-nitrida mulai berkumpul dan berpendar biru, tanda mereka mulai bekerja.
Filamen-filamen tersebut menyusun diri jadi tabung supertipis, kemudian saling menggulung menjadi struktur triheliks, dan mulai menyerap kelembaban dari udara, memecahnya menjadi oksigen dan hidrogen.

Sambil terus menghindar, ia terus mengumpulkan filamen-filamen pendar biru tersebut.
Telah cukup padat gulungan filamen yang melayang di sekeliling Rick, dan ia merasa cukup.
Digaetnya beberapa filamen tersebut, dan dengan gerakan memutar, hampir seperti koreografi tarian, ia memantik salah satu filamennya.
Filamen-filamen lain meluncur melewati nyala api tersebut menjadi bola-bola api.
Salah satu bola api mengenai seorang musuh, yang berteriak melengking seperti anak gadis, berusaha mematikan api tersebut.

Pirokinesis, kemampuan pengendalian api milik Ein, ternyata cukup hemat.
Meteran mandalanya tidak menunjukkan pengurangan kuota yang signifikan.
Ia membuat lebih banyak bola api, dan melemparkannya ke musuh-musuh yang lain.
Meja-meja sekitar mereka mulai terbakar.

Sambil terus menghindari gerombolan yang mengejarnya, ia menyadari bahwa serangan api mungkin tidak bijak dilakukan dalam ruangan tertutup.
Ia sudah membakar beberapa meja, dan kursi.
Seseorang juga telah menderita luka bakar.
Selain itu, pendesain gedung yang baik, pasti telah menyiapkan langkah untuk melindungi dari api-api yang mungkin timbul.
Sprayer.

Ia baru menyadari hal itu ketika ia merasakan hujan dalam ruangan.
Terasa cukup aneh, ada hujan di ruangan yang ada matahari mini.
Air merupakan musuh dari perlengkapan elektronik, khususnya yang tidak dirancang untuk di luar ruangan.

"Oh, komputer," kata Rick, "*Fiat arx!*"

Jendela-jendela pecah menjadi partikel-partikel silikat yang terjerat di jaring-jaring karbon, yang kemudian menyusun ulang partikel-partikel tersebut menjadi lapisan kaca pelindung di sekeliling komputer tempat Rick menyalin data.
Ia senang melihat *mantera* bekerja dengan sangat efisien dan dapat diandalkan di masa modern ini.
Mantera adalah terobosan terbaru di dunia *reflexior*, para pengendali kenyataan, yaitu orang-orang seperti Rick.
Awalnya, setiap pengendalian kenyataan, atau kegiatan *reflexius*, memerlukan konsentrasi dan pemahaman mendalam oleh para *reflexior* untuk dapat dilakukan.

Seiring berjalannya waktu, beberapa *reflexior* menemukan teknik untuk membuat *peladen*, yaitu konstruksi kenyataan seperti jiwa buatan yang dapat merespon input-input spesifik dan melakukan tugas-tugas pengendalian kenyataan yang repetitif dan dapat diprogramkan.
Hal itu memungkinkan pengendalian kenyataan yang lebih kompleks dapat dilakukan dengan menggunakan pintasan berupa perintah-perintah, atau *mantera*.
*Fiat lux, fiat umbra, fiat nil lux, fiat lux maximus, fiat arx*, hanya beberapa dari mantera-mantera tersebut dari peladen *Fiat*, satu dari banyak peladen lainnya, yang umumnya digunakan oleh para *reflexior* tanpa perlu memikirkan teknis bagaimana memunculkan manifestasi-manifestasi pengendalian kenyataan yang rumit, khususnya jika dibutuhkan dengan cepat dan konsentrasi minim.
Tentu ada harga ekstra yang dibayar ke pemilik peladen, atau programmer yang membuat mantera tersebut...

"Oh, *goblok*," umpat Rick, "mereka jadi tahu kan komputer mana yang saya pakai."

Tiga orang mendekati komputer yang dilindungi perisai kaca tersebut, sementara tiga lainnya mencoba untuk menyudutkan Rick.
Secara singkat, Rick memeriksa meteran mandalanya, tidak sisa banyak.
Cukup untuk melumpuhkan dua pistol, dan nyaris tidak cukup untuk mempertahankan belahan jiwanya yang sedang menjagai lubang di dinding dapur yang dibuatnya tadi.

Dua pistol yang diarahkan ke kepalanya, ia bengkokkan larasnya ke atas.
Ia teringat dengan kedua *soargun* di punggungnya.
Benang-benang karbon mengikat kedua telapaknya dengan kedua *soargun* tersebut, yang kemudian menyusut dan menarik kedua *soargun* ke tangannya.
Ia arahkan satu *soargun* ke orang ketiga yang masih memiliki pistol utuh, dan menarik laras.
Satu bersit petir keluar dari laras *soargun* dan mengejutkan orang tersebut.

*Soargun* atau *Securion Aluminum-projectile Rail Gun* adalah senjata yang memiliki dua konfigurasi.
Konfigurasi railgun yang menembakkan aluminium berkecepatan tinggi dengan menggunakan listrik.
Konfigurasi elektrolaser yang memanaskan udara antara laras dan target menjadi plasma, dan mengalirkan aliran listrik voltase tinggi melalui plasma tersebut.

Rick bersyukur *soargun* tersebut tidak diset ke mode railgun, ia tidak siap menanggung rasa bersalah karena membunuh orang.
Ia arahkan ke orang keempat yang mendekatinya, yang segera saja terkapar setelah diberi kejutan.
Sisa dua pistol, dan Rick berusaha berpikir cepat.

"Manov, sebagai anggota Ikrar Trinitas," rapal Rick, "pinjamkan saya kekuatanmu!"
Tato api hitam memudar dari sisi kanan wajah Rick, dan tato lain muncul dari pelipis kirinya.
Tato tersebut adalah tato tiga cincin konsentrik terbuka, dan tiga bilah pedang yang memanjang ke dahinya, sementara tiga lain memanjang ke dagunya.

Satu orang melepas tembakan, bersamaan dengan satu *soargun* yang diset ke mode railgun.
Timah panas tersebut dihabiskan momentumnya oleh proyektil aluminum tersebut, yang masih memiliki cukup momentum untuk terus melaju ke arah penembak pertama.
Pecahan timah dan aluminium panas menembus tangan dan wajah penembak pertama.
Sebagian kecil pecahan timah panas masih terus meluncur ke arah Rick.
Jaring-jaring karbon memadat di depan pecahan-pecahan tersebut, dan menjadi selembaran yang mementalkan pecahan-pecahan tersebut ke samping.

Rick dapat melakukan semua tersebut dalam sekejap mata karena kemampuan Manov, *Jangkar Waktu*.
Jangkar Waktu adalah teknik Manov, kembaran Rick, yang teknisnya adalah belahan jiwanya yang telah sepenuhnya terpisah dan mandiri.
Karenanya, Manov tidak memiliki raga, dan pikirannya menjadi lebih fleksibel.
Ia hidup sebagai koneksi-koneksi di antara benang-benang karbon hanya sebagai informasi, atau *infomorph*.
Orang biasa mungkin akan menyebutnya hantu.

Sebagai *infomorph* yang tidak dibatasi dengan raga, dan otak biologis, ia mampu merekrut lebih banyak benang-benang karbon, untuk dimasukkan dalam proses berpikirnya.
Semakin banyak benang-benang karbon yang direkrut, pikirannya semakin cepat.
Karena keterbatasan template otak manusia, semakin cepatnya pikirannya terasa seperti dunia menjadi lambat, bahkan seperti berhenti.
Kecepatan pikirannya hanya terbatas dengan seberapa cepat benang-benang karbon yang direkrutnya dapat memproses pikirannya.

Rick bukanlah *infomorph*, sehingga efek yang ia rasakan adalah akibat ditransisikannya pikirannya ke benang-benang karbon yang direkrutnya, yang nantinya akan diintegrasikan kembali ke otak biologisnya.
Badannya tidak ikut bergerak cepat juga, sehingga ia menjadi pikiran yang terjebak dalam badan yang masuk dalam *slow motion*.
Tapi tidak berarti ia tidak dapat melakukan *reflexius* dalam keadaan ini.

Ia melewatkan satu pecahan timah panas, yang sekarang sedang dalam perjalanan menuju perisai kaca yang melindungi komputer yang tercolok ke *K-Drive*.
Seketika, ia mengecek meteran mandalanya, dan meterannya hampir habis, dengan cepat.
Jangkar Waktu sangat mahal, dan dibayarkan per unit waktu penggunaan.
Ia tidak punya cukup kuota untuk menghentikan pecahan terakhir tersebut, tapi masih cukup untuk melepaskan *K-Drive* dari port komputer.
Pada akhirnya, setidaknya *K-Drive* tidak ikut korslet ketika perisai kaca pecah dan komputer dibasahi oleh *sprayer* air pemadam kebakaran.

Ia memberikan perintah tersebut ke benang-benang karbon di *K-Drive* dan port komputer untuk saling mendorong, sekuat mungkin.
Di detik-detik terakhirnya yang ia rasakan dalam Jangkar Waktu, ia sempat menikmati butiran-butiran air yang tampak terhenti di udara, pecahan-pecahan aluminium dan timah panas yang sedang menekan ke kulit musuhnya, dan pecahan yang sedang mendorong perisai kaca di komputernya.
Retakan menyebar dari titik tekanan tersebut, dan merambat ke sekujur badan perisai kacanya.

Jangkar Waktu terhenti.
Meteran mandala menguap.
Dan dunia seketika terasa sunyi bagi Rick.

Ia tidak lagi mempedulikan perisai kaca yang pecah, dan komputer yang menghasilkan letupan ketika air merembes masuk ke mesinnya.
Ia tidak peduli lagi pria yang berteriak di depannya karena kehilangan satu jari dan wajahnya ditembus timah panas.
Ia berlari ke arah *K-Drive*, sementara dua pria lain ternganga melihat temannya yang terluka, empat terkapar di lantai, dan seorang lagi yang tadi tangannya Rick bakar dengan bola api, terlihat tenang diguyur hujan, membasahi luka bakarnya.

Meteran mandalanya menunjukkan kedipan cahaya kuning, yang sekarang kurang dari satu derajat lebarnya.
Cukup untuk melayangkan *K-Drive* ke tangannya.
Sisanya ia simpan untuk aksi terakhirnya malam ini.

Sebelum kuotanya benar-benar habis, ia merapalkan, "*fiat nil lux*."

Akhirnya matahari mini tersebut padam dan lenyap, menguap.
Gelap yang tiba-tiba tersebut membutakan para musuh, mata mereka belum terbiasa dengan gelap.
Rick memakai kacamata cerdasnya dan mengaktifkan mode penglihatan malam.

Di dapur tempatnya masuk, ia hanya dapat memandangi terowongan yang dibuatnya runtuh jadi pecahan-pecahan kerikil beton.
Sebuah lubang menganga tersisa di dinding.
Belahan jiwanya telah ia nonaktifkan sedari tadi untuk memberinya cukup kuota untuk aksi terakhirnya.

Ia melompat dari lubang di dinding tersebut, sepuluh meter ke bawah, ke arah mobilnya.
Segera ia menangkap dinding, dan menempel bagai cicak.
Ia mulai merayap turun, terburu-buru, dengan gelisah memeriksa meteran mandalanya.
Sisa sekitar empat meter dari tanah, dan tangannya mulai tergelincir, meteran mandalanya menguap.
Ia segera mendorong keras dinding dengan segenap tenaganya, dan berguling di tanah untuk mengalihkan seluruh gaya tabraknya ke tanah ke samping.

Kakinya terkilir.
Dengan susah payah ia merayap masuk ke mobil, ia di kursi belakang.
Di antara senggalan nafasnya, ia memerintahkan mobil tersebut, "*Safe house*," dan mobil tersebut segera mengunci pintu, dan meluncur dengan sendirinya.

Ia menutup matanya, mencoba rileks.
Dan ia tersenyum.
Untuk pertama kalinya setelah sekian lama, kelopak matanya yang menutup benar-benar memberikan efek yang ia harapkan.
Tidak ada lagi yang terlihat, gelap.
Ia juga baru menyadari, bahwa mobil tersebut cukup kedap suara.

Rick tertawa.
Dan tertawa.
Seandainya mobil ini memiliki supir manusia, ia pasti merasa Rick sudah gila.

Orang-orang yang Rick tinggalkan, mencoba memulihkan diri, dan menolong rekan-rekan mereka.
Mereka mengamati kondisi medan tempur, dan hanya dapat menghela nafas.
Kacau.

Mereka tidak menyangka akan berhadapan dengan seorang *dukun*.

Mereka membongkar paksa semua komputer, dan mengambil semua cakram disk di ruangan tersebut.
Sebagian besar sudah korslet, tapi mereka berharap setidaknya salah satu diantara mereka masih berfungsi baik.
Hilangnya satu jari, luka di wajah rekan mereka, dan satu luka bakar tingkat tiga dari rekan lainnya, setidaknya tidak akan sia-sia jika mereka bisa memperoleh sesuatu dari remah-remah pertempuran.

Saat mereka berusaha berjalan keluar, mereka meliriki pintu dapur yang terbuka.
Terheran dengan adanya embusan angin malam, mereka mengarahkan senter ke dalamnya.
Sebuah lubang tampak menganga di dinding dapur.
Lingkaran sempurna, tidak seperti bekas ledakan.

Mereka tidak peduli, dan langsung berjalan melewati dapur tersebut.
Ada hal yang lebih aneh hari ini, mereka baru saja berhadapan dengan seorang *dukun*.
Mereka lebih khawatir lagi memikirkan, mau bilang apa sama bos mereka?
Siapa yang akan percaya bahwa lawan mereka adalah seorang *dukun*?

Rick sedang berada dalam suatu ruangan kelas.
Ia baru saja menunjukkan ke teman-temannya kalau ia dapat melihat meskipun matanya ditutup rapat dengan kain, dan digeluti dengan teman-temannya.
Ia menghadap ke arah pintu kelas yang berlawanan dengan papan tulis, dan sekelompok siswa lain menutupi jendela-jendela agar ia tidak dapat melihat pantulannya.

Seorang siswa menuliskan sesuatu di papan tulis, dan Rick membaca semuanya dengan tepat, hingga titik komanya.
Seorang dari teman-temannya mulai menyebut Rick *dukun.*
Seketika sekelas mengulang-ulang kata tersebut sambil menghadap Rick.

"Dukun, dukun, dukun"

"Saya bukan dukun!"

"Dukun, dukun, dukun"

"Saya bukan dukun!"

Rick mendapatinya teriak sendiri dalam mobil tanpa pengemudi.
Ia ternyata hanya tertidur, dan tadi hanya mimpi.
Dirinya benci disebut dukun sedari dulu.
Ia bukan dukun.
Ia hanyalah seorang *reflexior*, sang pengendali kenyataan.

Ia tertawa ringan, dan menutupi matanya dengan lengannya.
Sesuatu masih di tangannya, sehingga ia menarik lengan dari matanya, dan membuka genggamannya.
Sebuah *K-Drive.*

Ia melihat layar komputer di belakang kursi kemudi yang kosong.
Dicolokkannya *K-Drive* ke port yang tersedia, dan data mulai diunduh ke komputer mobil.
Ia mulai memeriksa berkas-berkas di sana satu persatu.
Hingga ia mendapati suatu berkas, yang menjabarkan suatu artifak langka.

"*A Black's Staff*," bacanya, "*an artifact that had been around on Earth, since the day Cain killed Abel.*"

"Manusia sudah gila, saya harus melawan maut hari ini hanya untuk mendapat ini?"

Meteran mandalanya muncul di depannya.
Satu derajat kerlipan kuning terang muncul di bagian dasar lingkaran, perlahan-lahan bertambah hingga sepuluh derajat, lima belas, sembilan puluh.
Setiap kubik sentimeter dari mobil ini pun terasa, setiap detik bertambah detail, dan kini menutup mata tidak ada lagi efeknya.

"Sial, Ein barusan gajian," Rick menepuk jidatnya.

Malam itu, penyesalan terbesar Rick adalah, ia tidak melanjutkan tidurnya selagi ia kehilangan indera keenamnya.

