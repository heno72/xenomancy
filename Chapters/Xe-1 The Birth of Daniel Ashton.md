# The Birth of Daniel Ashton

## Meta-info
Note several changes:
- The first part of the original The Birth of Daniel Ashton is now a part of The Lost Son.
- The part that remained here is when he's merged with Charles Lee.
- Note that it would be in Father Chris's house, so no house angel.
- It can't be a taxi either, as it must be Adran that picked him up.

Begins with the arrival of Sechan's and Sehyung's father in a rare occasion, Charles that was trying to be content with his life, happen to meet Fernando where he shouldn't, which turned out to be Haein's cousin.
Being confronted by Tee about his nature on why Fernando referred to him as grandpa Os, he revealed his etoan ancestry, and the mission guy (Os) that had been bugging him in the past year.
Charles is reminded by Tee that there's no point of running away from his mission.
[Accepting who he is](Chapters/The%20Birth%20of%20Daniel%20Ashton.md), he merged with Os, and becomes Daniel Ashton.

One thing to consider, didn't Tee know about his ancestry before?

## Fernando
Begins with the arrival of Sechan's and Sehyung's father in a rare occasion, Charles that was trying to be content with his life, happen to meet Fernando where he shouldn't, which turned out to be Haein's cousin.

## Tee
Charles is reminded by Tee that there's no point of running away from his mission.

## From The Ashes
Ostaupixtrilis Pontirijaris woke up on a bed. A human bed.
He looked around, nobody is in the house. A House Angel approached him.
It was faceless, but wearing a complete tuxedo.
Deep, trembling and peaceful voice could be heard somewhere from his head, “Sir Ostaupixtrilis Pontirijaris, welcome back to Earth.”

Ostaupixtrilis Pontirijaris flexed his body, and he noticed something.
His body was very young, with a thin frame, and long, slender limbs.
His body's biological age was fifteen years old, around the same age as his actual age, divided by a thousand.
His past memories faded as if he were dreaming, massive landscapes of layered cities, he was zooming through layers of them.
Each layers older than those before it.
His mind narrowed and his augmentations limited.

His previous memories and his entire lifetimes were just too much for his human brain could store.
Limited augmentations he had on Earth would not be enough to access his vast past experiences.
He was beginning to experience temporary amnesia for his entire stay on Earth.
Before his memory faded, he downloaded a memory of his lifetime two thousand years ago.
As the download was complete, he forgot everything about his love, Tudva Pontiritarax.
Everything, but the last cuddle he had on his garden.
Even that faded beyond recognition.
He was here to work.

One line remained, from a face he had forgotten, the face he loved so dear and recognize only by the feel it brought to him, "Finish it well and return here."

There was something else bothering him.
A second set of memory, lingering around the corner of his mind, the memory of his stay on Earth.
The one that went around Earth since September twenty fourth, year nineteen and fifty.
The one that at around year two thousand and three, decided to return his biological clock to a seven years old boy.
The one that since year two thousand eleven, decided to stop aging and explored Earth's education system as a high school boy.
And the one that was about to go to his fifth high school, where his great grandson attended school.

He was yet to enroll, and he gave it some thoughts.
His current existence was a merge between his version here on Earth and the version that was just transferred from Aucafidus.
This school jumping that was once some old man's obsession to try some fun living as a human boy, lose its original intention.
Or rather, transformed.
He had a mission, from his God.
And he was reborn on Earth that day, the soul that was once lived among men two thousands years ago returned on Earth in his tourist body.
It was he second coming.

The second coming requires a new name, and his exoself peered deep into humankind's network known well as the Internet.
Looking at the appropriate relevant information just before he finished his blink of eyes, his exoself spurted a perfect name to his taste, with a meaning appropriate to his needs.

“I want a new name in this current existence. I was born anew, let me be Daniel Ashton. God be my judge, and from the ashes I rise like a phoenix.”

“There you go, Sir Daniel Ashton. Your query is registered and all required papers are printing on your work desk. What else can I do for you, sir?”

“Get me a transport to Steven and Adran.”

“A taxi had been ordered to carry you to Sejahtera Clinic, sir.”

“Thank you.”
