# Facing The Forking Path

More people were attending the sunday mass.
Most of them were women, young women.
Many of them didn't care about the teachings at all.

They were looking for the two gems of the Legion of Umbra Cahaya that performed at the chapel.
The young, korean descent boy: Anthony Matthias, and the gorgeous southeast-asian like boy of indeterminate origin: Christopher Agape Lumintan.
The mass weren't even embarrassed to show that they came only for the two gems.
They approached the two gems for their signatures, often providing them with gifts.
Cal and Anthony had to sneak out of the stage silently for every performance.

It shouldn't be that way, it was a distraction for Aditya.
EPL was supposed to educate them on how to humanize humans, a preparation for the new world order.
These women came purely for carnal pleasures, and Aditya was disgusted.

His foster son, Anthony, that he loved with all of his heart, was a part of the problem.
But Anthony was a problem only after Cal entered Anthony's life.
Cal was the root of the problem.

What annoyed Aditya the most, was that Cal looked just like his previous target, Michael Guntur.
Aditya looked for more information about Cal and discovered that he had no records other than he was raised in foster care.
Edward Kevlar, a frequent and loyal member to the Legion of Umbra Cahaya movement, was Cal's adoptive parent.

Questioning Edward didn't bear any fruit, as he proclaimed Cal to be just like any other kid at his house.
Cal studied, slept, joined extracurricular activities, hanging out with friends, just like any other kid.
Cal did not involve himself in a fight, nor did he ever have any problem with teachers.
A perfect kid, probably too perfect.

Aditya was frustrated because there was no way for him to drag Cal down.
There was nothing to discredit Cal in front of Anthony, to prove his point that Cal was not a good friend for Anthony.
Until the day he overheard the conversation between Anthony and Cal.

## No, I'll stab you

In a green field, loud thuds could be heard.
Curious corvids peered at the source, while snacking on unsuspecting bugs flying around the scene.
Another set of thuds could be heard, and it grew more fierce.

"See, transfer the force from the ground, then to your opponent like that," said Cal.

Anthony launched another series of jabs, kicks, all blocked by Cal.

"Now when you're doing defense, remember to close your vital areas, don't let it hanging open, inviting opponents to penetrate it," continued Cal, "and most importantly, your stance, so you won't easily be thrown by your opponent."

Cal launched a kick, Anthony deflected with another kick.
An opening was available and Cal almost penetrated it, when Anthony used his elbow to deflect Cal's jab.

"That's it, you remember that it is not your hands that could be used, all moving parts in your body are your tool."

Anthony launched an attack when he saw an opening, but Cal foresaw that.
With the help of Anthony's momentum, Cal dragged the penetrating arm while pivoting at Anthony's center of mass.
The next thing Anthony noticed was that he lied on the ground again.

"Don't lose your focus," said Cal, while trying to hide his growing grin, he offered a hand to Anthony, "let me help you."

"Let's call it a day," said Anthony, patting the hand Cal offered away.

They exchanged glances, and grins, before Cal decided to lie right beside Anthony.

"Why are you so good at martial arts?" asked Anthony.

"I started very early, fifteen thousand years ago. I and my brother, Alt, sparred every single day."

"Was he a white elf like you?"

"A human, a normal human."

"Do you mean a caveman?"

"No, don't be ridiculous, " Cal raised his head to face Anthony, his elbow supported his body, "fifteen thousand years ago, anatomically and behaviorally modern humans were present already."

"But still, they couldn't do much right? They just eat, sleep, poo, have sex, *et cetera*?"

Cal scoffed, "where did you learn that? Cinema?"

Anthony couldn't answer that.
His face grew red of embarrassment.
It wasn't wrong, but he wouldn't admit it.

"People fifteen thousand years ago were just as human as we are today," Cal laid back, his gaze affixed to the clouds bathed in bronze glow, "they feel joy, love, lust, envy, rage, fear."

"But it's different now, we have technology, and magic."

"It's no different. People are people. Different environments wouldn't change that. We still feel the same way between each other."

Anthony felt the words sinking in his heart.
For the first time in his life, the life of ancient people became as real as his life at that moment.
Perhaps due to the fact that an actual person that lived in that age narrated it to him.

"So were you born here on Earth?"

"No, I was born in space, en route to Earth. My *real* parents crash landed somewhere in Europe, near the west coast, and my *human parents* from a nearby village killed my parents just to get me."

"Your, what?"

Cal's smile grew wider, "funny, my human parents wanted to have children, but my human mom was sterile. The shaman of my tribe told them that a baby will come from the sky, but sky people would take me again when I was bigger. They didn't listen for the second part, or perhaps they didn't want to listen. They killed my real parents so they wouldn't be able to take me again, and they raised me as their own."

"They believed that? A baby will come from the sky?"

"Well, they did have doubts like we do today. My mother told my father to sleep with one of his slaves so he could father a son. And my brother, Alt, was born. About a year later, I came, and they took me too."

"Huh," Anthony sat up, he took a moment to think, "the story sounds awfully similar to Isaac and Ishmael, sons of Abraham."

"You might want to hear more," Cal was sitting too when Anthony turned to him, "a visitor came, one of my kind."

Cal took a deep breath, there was a certain glow in his eyes, or so Anthony thought, reminiscing a fond memory.

"It was a grass field like this, but much wider," his gaze panned around the field, "in the middle of the night, a black octahedron was floating in the middle of the sky, bathed under the full moon. Glitters of reflected moonlight on its surface created a surreal atmosphere on the grass fields, as if stars made their way to the ground. I decided to walk toward the octahedron."

"What did you find?"

"My first love, Istar Laupatura," a soft smile grew on Cal's lips, his gaze penetrated the present time back to a distant past, a past long gone, "intrigued to find one of her kind here, that believed Earth was flat."

"That Earth was Flat," Anthony scoffed, "were you a flat earther?"

"How would I know, that Earth is round," a burst of laughter emerged on Cal, "if at the time, Earth looked as flat as it could be."

After some moment in laughter, Anthony asked, "so what happened to Istar?"

"I told you that humans back then still feel the same feelings as we are feeling now, didn't I?"

Anthony nodded.

Cal's gaze returned to the past, "a foreign woman, from outside of our tribe came. She was beautiful, healthy, and fertile. I and my brother, Alt, fell in love with her. Naturally we tried to win her. My brother was experimenting with new seeds he gathered on an exchange with other tribes from a season prior, I forgot what season. He presented that to her. At the same time I gave her the best bison meat I could cook. Guess which one did she choose?"

"Damn, don't tell me she chose the meat?"

Cal nodded.

"That sounds an awful lot like the story of Cain and Abel! Don't tell me that your brother tried to kill you?"

Cal shook his head, "no he didn't try. He did it."

"You, died?"

"My first death."

"Your death? I thought you're immortal. And how can you still be alive now?"

"Istar took my body to her ship, resurrected me, and we left Earth."

"So, what happened to your brother afterwards?"

"How would I know? The trip back to her home world took decades, even with wormhole networks. Even if I did get back, my brother would most probably be dead."

"Served him right, he killed you just over a girl."

Cal smiled, "well, he wasn't a bad guy. He was actually a very caring brother. He just got his emotions to be the best of him."

"Weird, why didn't you hate someone that tried to kill you?"

"Time heals, I guess. It was so far back in the past, it barely mattered anymore."

They took a moment in silence, gazing at the sunset.
It was slowly descending to a city on the west.
The city appeared to be covered in a thick fog from their vantage point, air pollution to the best effect.
The sun wasn't even touching the horizon, as the fog swallowed it whole.

"So, when did you get back to Earth?"

"Many times afterwards. I frequently visited Earth every several centuries or so. Earth was the world I grew up, after all. I witnessed the rise and fall of major civilizations, I did a diplomatic mission here as Yeshua from Nazareth-"

"Did you mean Jesus Christ?"

"Yeshua, the name was the same as Moses's discipline. Today it was known as Jesus as a result of Greek and Latin transcription. Christ was never a part of my name, it merely meant anointed. But in modern times it was considered a part of my name. So funny what time can do to pervert that name of mine."

"Oh shit. I'm talking to a real Jesus!"

"Yeshua," Cal threw a fist to Anthony's shoulder, "don't pervert it."

"Okay, okay, Yeshua it is. So did you do other diplomatic missions afterwards?"

"I did not do diplomatic missions afterwards. I returned to Earth in year nineteen fifty, fathered two sons on Earth, and I traveled to many places on Earth after my sons were all grown up."

"You know it is hard to imagine you having grown up sons, when you're a fifteen years old," Anthony ran his hand across Cal's shoulders and arms, "were you always like this? A fifteen years old boy, all along?"

"Of course not," Cal bursted into laughter, "I was a grown up man. I just decided to turn into a seven year old boy for a change in two thousand and three. I realized that I haven't been experiencing life as a kid since my days as Yeshua. Then at two thousand and eleven, I decided to stick as a fifteen years old indefinitely."

"Why? What changed in two thousand eleven?"

Cal's face turned stern, with a subtle smile, "another diplomatic mission was given."

"To a minor?"

"I just appeared as a minor," Cal was visibly irritated, Anthony giggled.

"So what was the mission that you must stay at fifteen years old?"

"Agent of changes. I did a series of skirmishes with young people, because at this age, we're the most vulnerable to change. At this age, our brains are literally changing, rapidly."

"And you changed identities every three years? Hopping in between high schools?"

"This is the fourth time, and it wasn't always three years. I was Calvin Gauss for two years, Michael Guntur for three years, Alfa Iskandar Putra for about a year, and now, as Cal."

"What did you do on the first three? Did you make groups like that when you was Michael?"

"I can't tell you that," said Cal, his hands folded to shape a fake gun, he aimed at Anthony's head, "it's sensitive information."

"Will you kill me if I know them?"

"No, I'll stab you," Cal pushed his fake gun to Anthony's ribs.

A series of shrieks, loud laughter, and jumping steps could be heard, as Cal and Anthony tried to stab each other with their fingers.
They were having so much fun they didn't realize someone was approaching them.
It wasn't until a gunshot was heard, and Cal fell to the ground.

## You choose the right side

"*Hyung*, what did you do?" said Anthony.

"He is M.G., the one that caused Dominic to die. And the one that distracts you from our goal."

"I did not kill Dominic," said Cal, he stood up.
The bullet didn't make it to his heart.
It didn't even penetrate the clothes.
The boron-carbon-nitride nanotube reinforced fabric on his clothing expended all of the bullet's kinetic energy with just a few broken strands.

It was the first time Anthony witnessed true etoan engineering, as the clothes of Cal folded into a full origami-like fabric armor.
Aditya had seen it before in a video stream of the concert where the Tengu Incident happened.
It was the same technology that the shape shifter Anderson used.

"See, he's our enemy, son. He is with the WTF," said Aditya, his gun still aimed at Cal.

"He is no longer Michael," Anthony fell to his knees, pulling Aditya's shirt, "just let him go, please."

"He is influencing you! Just like his kind influenced Henokh! They took Henokh and Derictor from me," Aditya kicked Anthony away, "I won't let them take you too!"

A series of shots released toward Cal.
From Cal's wrist, an origami shield unfolded, that deflected away all of the incoming bullets.
It was not long afterwards that the gun ran out of ammo.

Aditya pulled a pair of daggers, he thrusted one toward Cal.
The origami shield folded again, and Cal blocked the incoming thrust with a blade made of the fabric that comprised his clothes.
Aditya thrusted another blade toward Cal, that was blocked with another blade from Cal's other arm.

A thrust, followed with a block, then a series of slashes and redirection later, Cal avoided Aditya's move.
Aditya found himself on the ground, confused.

"I did not want to fight you," said Cal, the headpiece of his armor unfolded back to his neck.

Aditya jumped up, and threw a series of kicks and stabs, "and I won't stop until either one of us dies."

"You asked for that," said Cal, and he blocked the kicks, deflected the stabs, and managed to disarm Aditya.

Cal had Aditya's arms on his grip, and pulled it to the back to disable Aditya.
Aditya attempted to rise, and pushed Cal with his back.
Despite the suit's strength, Cal had a fixed mass, and Aditya had a velocity, which combined with Aditya's mass, became a momentum that must be preserved, as dictated by the law of conservation of momentum.
Cal's body was thrown to the back, and Aditya was midair, launching a kick to send Cal to the ground.

Despite the suit's ability to absorb a wide range of damage, acceleration was not something it could magically negate.
As Cal hit the ground, he choked out of surprise.
His back was in pain, his head disoriented.

The suit extended automated ribbons that wrapped Aditya's leg, and threw him away from Cal.
Cal rose, he was barely able to stand up, if not because of his suit.

"It's time for you to choose, Tony. On whose side are you?" Said Aditya.

Anthony froze, he did not know what to choose, "I can't."

"My son must be able to choose their own path," said Aditya, "I told you many times. It just happens that at the moment, you must choose between my path, or his."

Anthony did not move, nor did he make any noise.
Aditya sighed.
He dashed toward Cal, he pulled another gun from his vest.

The clothes of Cal became much more violent, as Cal was not in the shape to give commands.
The ribbons slashed the gun, it almost had Aditya's fingers.
Another set of ribbons wrapped Aditya's feet, and he was being pulled to halt.
The ribbons pulled Aditya's limb apart, at a force that almost snapped them off the sockets, and Anthony could hear Aditya's scream.

"Vespa!" Screamed Anthony, his hands aimed at the ribbons that pulled Aditya apart.

A black cloud formed around them, and it coalesced into many black dots.
The dots developed into hornets, and the hornets started to hum.
The swarm flew into a stream and went through the strands.

Boron carbon nitride compounds might have a high strength, but its strength was no more than diamonds.
The hornets Anthony conjured were made out of diamondoid materials, especially sharpened near their wing tips.
As they flew through the space in between the ribbons, they let their wings touch the ribbon surface, eroding the ribbons in every pass.
In no time, the ribbons near Aditya's limbs were all cut off, while those that still wrapped around Aditya's limbs were munched by the hornets into pieces.

Aditya fell to the ground, relieving the pain on his joints that were pulled apart.
He looked at his son, swarming Cal with the black hornets that slowly eroded Cal's armor.
Proud could be seen emanating from his smile, he thought that his son made the right choice.

Cal's suit struggled to fight the swarm, as it could not focus its attacks, it was overwhelmed with the quantity of hornets Anthony summoned.
A hornet near the head of Cal made a noise that Cal immediately recognized as a speech.

"There's only one way out, you have to show him that you're an ambassador," said the hornet.

Cal looked at Anthony, nodding as their gaze met.
If Anthony said it to Aditya, Aditya would think that it was an excuse.
If Aditya saw it by himself that Cal was an ambassador, then Aditya couldn't use the same reasoning.

"Please," said the Hornet again, "with Aditya *Hyung*, I don't want to fight."

Cal nodded at Anthony, and whispered to the hornet, "at least let's pretend that you tried your best to fight me."

There was a growing smile in Anthony's face, but he tried to suppress that.

Anthony changed his stance, and he moved his hands closer together, pressing an imaginary ball in front of him.
The hornet acted on the cue and they were coalescing toward Cal.
Cal was visibly struggling, and Aditya grew prouder.

Diamond is flammable, as it is pure carbon arranged in a certain way.
It was a least known fact, as most jewelry sales would say that diamond was meant to be eternal, unchanging, and indestructible.
At around seven hundred degree Celsius, diamond would burn, reacting with oxygen to produce carbon dioxide.

Cal's clothes spread into filaments, trapping as many Hornets as it could.
It heated up, some of the grasses nearby started to catch fire.
Trapped hornets started to glow, before they exploded into hot diamond shards.

It didn't matter, as Anthony summoned more diamond hornets.
But it was sufficient to show that at least Anthony was fighting with all of his might, and Cal was struggling to keep up.
When their gaze met, Cal and Anthony nodded.

"*Ik-kor kakta-tol-Kav-ros kalram,*" Cal shouted, a hand raised to the air, "*Ik-ros solit-nah!*"

There was a silent thud.
The hornets disappeared into dusts, and within seconds they were all gone without traces.
Cal stood there, with his clothes torn there and here, the ribbons from his clothes were heavily damaged.
They started to fold themselves back into full clothes.

"What spell was that?" said Aditya, he limped toward Anthony, "can't you counter that?"

"It wasn't a spell," Anthony tried hard not to laugh, but the noise he made was almost like crying, "it was an order to the *Cherub*, that he did not want to be bothered in his diplomatic mission."

Cherub is the name of a thoughtless entity that answered directly to the Divine Council of Earth.
The cherub that Anthony was referring to was the RFL, that enabled magic-like manifestation on the surface of Earth.
The order was accepted by RFL because it was issued by the ruling Powers, to honor a sovereign.
As an ambassador, imbued in Cal's position was the sovereignty of Kavretojives.

"Diplomatic what?"

"He is an ambassador. By the order of the Divine Council, no *reflexius* act can bother an ambassador in his affair if he declared the clause, I can't kill him either."

Cal's clothes sprouted two pairs of wings made of origami folds.
A bigger upper pair started to flap, while the smaller lower pair adjusted to change his flight direction.
Cal flew away from the two of them.
Anthony and Aditya just looked at Cal as he flew further away to the west in the night sky.

"I'm sorry *Hyung*, there is nothing I can do."

Aditya put his hand on Anthony's shoulder, "I understand, son. It's a Power's order, there's nothing we can do."

Aditya looked at Anthony, and Anthony looked back.
"You did a good job, you chose the right side."

"I don't want to cross my path with *Hyung*," Anthony said, he hugged Aditya.

Aditya hugged back, he was proud that he still had his son with him.
Deep inside, some bitterness left in him, that he had to lose his two best friends to the etoan, and he had to lose Dominic, one of his loyal followers, to the WTF.
Anthony recognized that feeling, and inside him, grew uncertainties on what he should feel.
Two important persons in his life crossed their path at one another.
Should he be happy that Cal escaped, or should he be sad that his father was betrayed by his very own son?
Anthony's hug grew tighter, and Aditya patted Anthony's back.

"I'm sorry *Dad*," Anthony actually cried that time.

It was a surprise to Aditya, not because of Anthony's tears. Rather, because it was one of a highly rare occasion, that Anthony actually called him a *Dad.*

## I'm tired, Alex

Alex took some time to process Cal's story.

Cal was visibly uncomfortable.
Behind his gaze was a gaze of a wounded soul.
A soul full of regrets.

"So," said Alex, "you got attached to your target."

"I thought I could make a pass," said Cal, "I thought I could stay with him longer."

"Must be tough. You spent some time around your targets, it is inevitable that you develop attachments to them," Alex hugged his grandpa, that sat on his lap like a son of him would, "First it was Hendrik, then Derictor, then the polar bear-"

"Xiangyu," said Cal, he wrapped his arms around Alex's arm, "his name is Xiangyu."

"Yes, Xiangyu, and then now, Anthony."

Alex was surprised to find that Cal was crying.
Apparently, despite having an old soul, Cal's body was indeed just a kid.
At the biological age of fifteen years old, one's brain was changing, an intermediate state in between a kid's brain and an adult's brain.
At this biological age, one was more prone to have a shift in personality.
Being fifteen thousand years old wouldn't help against this change.

"I knew it would happen, complications," in between Cal's sob, he struggled to complete his sentence, "if I stayed for longer than necessary, complications might follow. And, and-"

Cal's mouth couldn't keep up with the surge of emotions he was experiencing, but through radio frequency, Cal completed it: *Aditya is the complication. It wouldn't happen if I leave Anthony after I finished negotiating with Yam. I didn't, I stayed for the whole year longer than necessary.*

"I'm tired, Alex," Cal's voice was hitting a higher tone, he was almost squealing.
Cal wiped his tears, "I'm tired with this constant need to move on, " his tone stabilized.

Alex let some moments pass, as Cal regulated his breath.
Cal's gaze panned around Alex's office, that at the time appeared as if they were next to a waterfall.
Alex's office was covered in phased array optics, etoan technology that enables projection of hologram sceneries.
In short, a three-dimensional screen without the need of special glasses like an ordinary stereoscopic screen.
Its sonar equivalent was also installed, that they could hear small bat-dogs at the scenery chirping, from behind their spot.

Those bat-dogs flew past them and toward another tree at the other side of the waterfall.
High above the sky, a thin glittering band that encircled the entire planet could be seen.
Another band that was situated above the first band could be seen.
The bands appeared to intersect one another at some point near the horizon.

"This isn't Earth," said Cal after he noticed the bands and bat-dogs.
Bat-dogs was a native species of Aucafidus.
The planet was also encircled with three bands of artificial strip habitats.

"I feel that a scenery from your home planet could help you calm down," said Alex.

*My home planet is Earth,* thought Cal, but he didn't say that.
Not a voice, not even through radio frequency.

"Thank you, Alex," Cal said.
