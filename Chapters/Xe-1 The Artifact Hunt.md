# The Artifact Hunt

## Meta-info
This version is different from the original version in several ways.
First of all, the updated KIA organizational structure must be considered, also BAIK is no longer a thing.
Second, the inclusion of Hendrik to the team means Anderson is one of the Nine Tailed Fox, as well as Hendrik.
Third, Hendrik's comment about him being the chief in the original version would not be valid anymore.

For the counter, since it was around 2 am a day earlier that Heinrich depleted it.
The gauge would then be replenished at a similar time.
So around 2 am today, would be when he regained his power.

Also important to consider would be the fact that EPL is now an official part of the universe.
So the insurgents would be referred as such.
And they shouldn't be too much surprised about fighting a paranormal.
Unless, of course, they're the human-only branch, I'm not sure yet.

Please consider reworking The Hunt section.
It could be merged with the main text, or at least rewritten.
A lot of useless informations there, but erasing it doesn't feel right either.

> In-story date for The Signs: 20210407 0030

## The Hunt
The story begins a year prior, in mid July, 2020, the day Securion Incorporated got a very hard blow in their nose.
Securion Incorporated's reputation as a private security firm was about to be jeopardized, when an insurgent group breached into their buildings, and stole a priceless artifact.
Their competition, G28 Private Police Firm, benefits from this situation as Securion's discredited.
This appeared like a normal business politics to the public.
Little did the public knew about the reason behind it.
Little did the public knew that G28 and Kukercorp's private intelligence agency secretly aid Securion on preventing the breach.

And they were  about to face a comparatively large insurgency agency known as Laskar Umbra Cahaya (Legions of Umbra Cahaya, otherwise shortened as the LUC), that supposedly opposes the reign of corporatocracy.
Their aim was to create an anarchic society, where everybody can be truly free.
Whatever their plans were, the plan involved the need to possess a very powerful divine artifact, a help from a Power, and support from a Denefasan Ambassador.

Securion Incorporated, a part of Prion Group, and Kuker Group, that include (among many more companies) Kukercorp and G28 Private Police Firm, made an agreement that's formulated in what's known as Oneiric Protocol.
The protocol was designed as such to maintain secrecy of the agreements, and to support intelligence agencies of either group on doing their jobs, maintaining sociopolitical stability on states where the two groups settled, all outside the concerned state's local law.
And years after the agreement was made, all seemed to be fine.
Until one day Securion discovered a very powerful artifact and decided to store it in their secure fault.

Well, it started with an artifact.
One that's so powerful that it could open the gate of Hell.
But this one's just one of three keys to open the gate of Hell, and the easiest of the three to obtain.
The fabled divine artifact, also known as Black's Staff, had been on the surface of Earth ever since Cain were cursed by the Divines for his brother's murder.
And all of the trouble Securion Incorporated was about to face stems from the fact that at the unfortunate moment Securion possessed the key in their fault.
This was also one of the main reasons Kukercorp interfere, to maintain possession of that artifact on their group.

## The Signs
The night was cold, and the sky was clear.
Hendrik took a deep breath and exhaled.
He gazed at the sky, and billions of faraway suns gazed back at him.
Hendrik smiled in relief.
No Romanov Dexter, he was currently working on another case.
No Heinrich Potens, that had just finished on dispersing riots on Medan.

He felt, alone, yet at peace.

However the ability to feel every centimeter cubic of everything in his vicinity disturbed the peace.
In a way, he sometimes wished that he could turn off his sixth sense, the sense that enabled him to feel through his reflexius ability.

He put his palm to his head as if to protect his ears from noisy sound, instinctively wished to be able to do the same to shut his sixth sense.

He pinched his eyebrows, his eyes shut and decided to return home after a walk or two.

But his phone rang.

And his phone rang again.

And on the third time his phone rang, he picked up the phone with a sour face.

“ Yes, Agent Soekarno here.”

“ Agent Enam here.
Pancasila got a lead that one of our employees might be working with Legions of Umbra Cahaya.
You were to investigate it tonight.”

“ I believe I'm the chief, so why am I taking orders from you?”

“ It's the best chance.
Tomorrow was archiving day, the datacenter might be freed up of junks and wiped clean.
Use the KukerDrive to retrieve the file from Kendari Branch's computer system.
Our office there were slightly outdated, we couldn't reach it from Makassar.”

Hendrik checked his pockets, and found no drive at all.

“ Sadly I didn't bring it with me, presently,” he said.

“ I know.
I just sent you a car with your equipment,” and Anderson hung it up.

“ Good lord, couldn't I just rest for a day?”

Hendrik's tired, and annoyed.
His normal-life profession as a lawyer requires a lot of mental burden to look through copious documents in his workplace.
His position as chief of BAIK's Agents meant he's the one that supposedly give orders, not Agent Enam.
He felt disrespected by Agent Enam that had this tendency to act boss.
He couldn't argue nevertheless, Agent Enam had it all calculated, and when he said that something must be done, that something must be decisive and time-dependent.

“ I wish it would be a smooth and easy mission.”

He missed his apartment unit in Surabaya.

He did not return home that night, in fact he'd stay in Kendari for a little bit longer.
A self driving car with equipment inside approached him.
He prepared for his infiltration to the company's Kendari Branch.

Inside the car was a briefcase, and inside it was a modified Soargun, four auto-strip belts that would automatically strip target's arms when activated, four microdrones the size of a golf ball each, a goggle with HUD and night vision installed (along with bluetooth enabled streaming camera), and a small flash disk with KukerCorp's logo on it.

He prepared his tool belt, strapped the modified Soargun on his back, then he stuffed four auto-strip belts, and four microdrones on every suitable orifice available in his toolbelt.
He wore the goggles, and put the flashdisk in his pocket.

He entered the car, strapped the seatbelt, and swiped his palm in the air.
A mandala drawn midair, its lining glow yellowish orange.
There was a circular gauge, where most of the arc were dim red in color.
Only a small fraction near the bottom of the gauge's arc that was bright yellow.
He sighed.

“ It must be Ein's doing”

Heinrich Potens, or Ein, was one of his double, known for his cold-headed calculative-heart fiery-passion, and his enormous display of strength as his fighting style.
Hendrik's convinced that Ein took a lot of their complexity gauge on his recent mission.
Usually that wouldn't be a problem, but when in unaccounted need like at the moment, Hendrik would be the one that must preserve as much to get the most of the remaining gauge.

He stretched his arms, and his shoulders.
There was this sour heart he felt that was shown in his expression.

“ We must play frugal then.”

With the car he drove into Kuker Corporation Kendari Branch Office, Building A.
He observed a parking lot next to a river, that had direct access to the building's side wall.
He thought it would be perfect, and parked his car right beside the side wall.
He climbed the car to its roof, and took a powered jump: he immediately reached the third floor.

“ Gecko-style reflexius wouldn't be too complex I believe,” he checked his gauge again with one of his arms, “ yeah, the arc's barely shorten.”

He was all four in the wall, his palms and feet were attached to the wall like a gecko.
He examined the surroundings.
It was dark, and he believed that his dark gray tuxedo matched the wall's paint.
Ignorance-inducer reflexius act took plenty of complexity gauges, so he couldn't use it at the time,

“ Good Lord, whoever listens, I wish nobody's seeing me,” he didn't mean that.

He knew that Powers in the Divine Council were ignorant of the state of the world.
They cared more on computational resources that was available for them.
However he sincerely wished that one of the true supranatural God, not the Powers might actually listen to him.

He took a deep breath, and traced his right palm on an imaginary wall surface about a centimeter away from the actual wall's surface.
He was feeling reflexium particles inside the wall.
Countless number of micron-sized carbon-based nanomachines with nanoscaled tentacles that could spread to several meters each, pulling and pushing objects or fellow reflexium particles, can work in tandem as a swarm, and project energy into light, heat, or electricity according to the need of a neighboring reflexior user, dispersed rather evenly according to density, on every major body of mass on the entire Earth's surface.
A ten thousandth of available masses were these tiny dust-like machines.

The reflexium particles replied to his mental pings, and his reflexius clusters on his right brain hemisphere performed mental calculations almost instinctively.
His parietal lobe integrated the map into his spatial awareness, and he felt them: countless of smartdust reflexium particles were ready for his command.
He smiled.

The plan was simple, to preserve complexity gauge, he'd craft a very simple command just to make deepest reflexium to push everything away from them with their tentacles, and the outermost, surface reflexium would pull the entire surface, to form a rigid body of tunnel opening.
Then another command reversed the pushers unto pullers as well.
Supposedly the wall would reverse its trajectory back into, well, a wall.
There'd be perceivable damage on the wall surface, but he was sure that he can always return tomorrow after his complexity gauge were replenished to complete the repair.

He made sure that no bar-like higher concentration on any plane of the wall, that usually indicate denser material, such as reinforced steel poles.
He didn't find any, and breathed in, breathed out.
He breathed in, breathed out, his eyes closed and he thought really hard.

Execution wise, it was quite extensive, to determine which reflexium particles to expand how, and which reflexium particles to strain.
His reflexius clusters worked so hard that beads of sweats formed on his forehead and his temples.
He was worried about his gauge now, and began pushing the wall inward with his right palm to realign his spatial awareness.
The outer side closest to his right palm would push the hardest, and the opposite, inner side would pull the least.
He determined the radius of deformation by his gaze.
He traced the perimeter of which the hole would supposedly form.

Tiny carbon-based machines swarmed inside the wall excited into activation, and they began to push, push, push in unison.
Cracks could be heard slowly forming, and larger cracks were visible, extending from Hendrik's palm to everywhere one meters in radius.
In the inner side, the wall began to crumble, unfolded into a cone, and from the outside the surface collapsed inward.
Large sound of cracks could be heard from outside.
Hendrik prayed to whoever supranatural god that listens, he hoped nobody would notice, or that everybody in his vicinity were sleeping, deeply.
The inward collapse accelerated hard until it came into a hard stop.
A tunnel of rubble formed in the wall.

Hendrik took a step inside, and touch an imaginary ball in front of his chest with his palms.
One palm above and another below.

“ Fiat lux orbis” he commanded, and a tiny ball of bright white light appeared and enlarged in the middle of an imaginary ball he was holding.

It stopped enlarging when it reached halfway to the full radius of that imaginary ball.

He stood in the middle of a pantry.
He threw the ball to his front, and it maintained its altitude, as it traced the path forward.

He checked his gauge again.
The gauge reduced by around a third of its original length.

“ So much for a frugal aim,” he mumbled.

The ball of light moved by commands of Hendrik's fingers.
It danced around objects, corridors, and windows.
Hendrik made a map of the building's layout in his mind, and let the ball of light guided him in the dark.

He arrived at the work cubicle, and the ball of light fled to a computer terminal Hendrik pointed.

Then he looked around and realized that this building's equipped with closed circuit television.

“ Oh good Lord” he sweated.

He excited airborne reflexium particles on his vicinity to glow in bright near infrared.

“ Should've done it earlier.”

Near-infrared light can be seen by security cameras, especially those for night vision.
It won't obstruct our vision, we couldn't see near-infrared.
However for security cameras, it would be a very obstructing glare.
This simple hack was a cheap solution, low complexity reflexius act, Hendrik thought.
He prided himself for his resourcefulness at the moment, until he realized that his infrared goggles couldn't see well under the glare he himself created.

He removed the goggles, felt terribly ashamed for himself, and decided to work solely on the light of his ball of light instead.
He thanked the unnamed true supranatural God again that nobody was looking at him.
He'd definitely not sharing this moment with Romanov Dexter and Heinrich Potens on their next memory sync, he thought to himself.

The KukerDrive plugged in to the computer terminal's Universal Serial Bus port, and began cloning the computer's user content.

Time elapsed on his wait only convinced him that this mission was, although fairly simple, definitely very boring.
He expected action scenes, deep inside his stomach.
However his gauge convinced him otherwise.
But this particular day he learned to be wary of what he wished for.
Whether or not the true supranatural God Hendrik usually prayed to actually listened to his expectations at the moment, or just a very bad random joke of the living universe, a group of insurgents entered the building from a backdoor to the building.

Airborne reflexium particles suddenly displaced by people's movements screamed a very thin ping to his reflexius clusters.
His parietal lobe interpreted the signal from his reflexius clusters, then he felt movements on the first floor, near the storage.

“ ...” was all he could say.

He didn't say a word, but he knew that he should turn the ball of light off.
He froze in a state of everyday trance.
His mind performed transderivational search to derive meaning of what he just felt, and when he couldn't worked it out, his mind performed another transderivational search to discover what it means when he couldn't worked it out.
He didn't expect anyone to enter the building presently, and he could felt them closing centimeters by centimeters to the computer room.

“ Fi, fiat umbra!” he commanded.

The ball of light turned into a ball of dark instead.

He should have commanded Fiat Nil Lux instead to completely annihilate the manifestation.
Or rather, he was going to, but another part of the brain thought of darkening it instead.
A tug of war occurred at his thoughts in what a psychologist would call the Stroop Effect, and in his case, the wrong solution won.

He ducked.

From below the table he plugged off the screen's video plug from the terminal's box.
A notification on his phone indicated that the drive would need another ten minutes to complete.
He'd need to buy some time.

The group entered.

At closer proximity he could differentiate the feel of at least six to eight individuals.
All of them carry high-density crooked hand devices.
By the density of reflexium particles within, Hendrik deduced that whatever they were holding, were metal-based, and probably were guns!

He checked his gauge, sighed very silently at the value.
It won't be enough to defeat that number of armed individuals.
He threw four of microdrones sideways while they trooped in, he hoped that their walking noise would conceal the sound of thrown drones.

He wore the goggles, and paired his smartwatch to his phone, then to his goggles.

Through his smartwatch he activated the drones, and the feed were streamed to the google's left glass.

The drones made this noise and moved into standard positions, equidistant spacing between themselves and nearby walls, two meters in the air.
He got a visual of them, and confirmed that they were  eight armed individuals!

They were surprised by the sudden appearance of the drones and six of them swept through the room, row by row.
They were  closing to Hendrik's position and he realized that the remaining two were copying something from another computer terminal as well.

They were  stealing data as well!

“ That's it, I might be stealing data myself, but I couldn't let them do the same,” he mumbled quietly.

Other than the existence of drones, one of the intruders noticed a floating dark orb above one of the computer terminals.
He then notified his comrades, which, then looked at the dark orb in confusion, that they halted their sweep for a fraction of a time, when,

“ FIAT LUX MAXIMUS!” he shouted, the black orb's turned into a ball of light again and the intensity reached that of sunlight.

They instinctively shielded their eyes from what appeared to be a mini-sun to them.

Hendrik switched the goggles into daylight mode, and the goggles filtered out most of the light.

Following the surprise min-sun, he knocked two closest guys to him, and put the auto-straps on their wrists.
A pair of straps and clicks impaired their movements, and he had to deal with the remaining six individuals, now slightly adapted to the bright light.

He sensed coppers on their plugged in drives, and another set of mental commands were crafted in his reflexius nexus.
Reflexium particles on the drive's contact surface and the terminal's then pushed each other apart.
They fell into the floor.
The six men now surrounded Hendrik in response.

They opened fire, and Hendrik barely managed to escape the bullets.
One scratched his shoulder.

“ Heinrich Potens, grant me your power as a member of the Trinity Compact of Hendriks!” he chanted.

A tattoo of fiery patterns emerged from his right temple, that radiated to his forehead and his jawline.

He could feel them reloading, and decided to take the chance.

He rose and traced circular motions with his arms around his body, and blueish linings could be visible around him.

The airborne reflexium gathered into strand-like structures, then very thin tubes, and grouped into three tubes, then into triple helix structure.
They gathered the humidity of the room and converted them to hydrogen and oxygen within each strand.

Hendrik pulled a number of nearby strands and they swirled around him.
He ignited one strand to form a flame, and other strands raced through the shooting line of sight in what appears to be a ball of fire thrown toward target.
One of them caught fire in his arm, and he screamed out in surprise.

Silently he checked his request to use amnestic library, but his request was denied: he'd used all of his quota this month!

“ This must be Romanov Dexter,” he slapped his forehead.

As expected, pyrokinesis would be one of the most easiest reflexius act to use, his gauge barely drained.
He used the fire again toward another men, that managed to somehow evade it.
The desk instead caught fire.

He jumped from one desk to another just to avoid the group of men from seizing him, and he decided that fire might be too much, he had burned a desk, a chair, and a man's arm.
And he felt a sudden flow from the sprinklers.

“ Good Lord!” he exclaimed, “ Fiat Arx!"

The window disintegrated into grains of silicate particles suspended by a web of carbon buckytubes.
Pulls and pushes there and there automatically executed and created a very thin layer of glass about two meters from the floor.
The drone trapped above the screen of glass, and showered by the sprinklers.
An opening were present above the burned desk.

Hendrik could feel a slight moment of slow motion.
He contemplated the wonder of modern reflexius achievements: automation.
The use of Fiat library provided a number of quick shortcuts of automated reflexius.
He did not need any hard work to compute every steps that must be taken and how to execute it on his reflexius nexus for trivial tasks such as creating an instant shielding layer that should protect the user, one which couldn't be done fast enough if he must instinctively process the command himself.

However it comes with a price, a hefty price.
About a hundred percent more expensive than if he was to perform the hard work of working out how by himself.
He traded time for more complexity gauge taxed from his quota.
He realized how smart the creator of this library system must have, library maintainers or the librarians must have amassed a large amount of complexity gauge credits for their own purposes, as incentives of every evocation of said library.

He broke his train of thoughts from wondering about the advancement of reflexius economy to put his attention for a more dire situation: the remaining six men!

A quick glance of the now ever-floating gauge near his head suggested that he had not much complexity gauge left.
Enough to disable two guns, and just barely sufficient to close the hole he used to enter this building.

He strangulate two guns aimed toward him, and their nozzles bent upwards, as he remembered that he had two Soarguns!

Reflexium particles around his right palm unfurled, and becomes interconnected into a web, entangling his palm with the handle of his Soargun, then they pulled together.
The tension sprang the Soargun into his grasp, and aimed for the man closest to him.
He zapped the man whose gun had been disabled, and the man fell into paralysis.
The Soargun configured to stun, he thought briefly, and aimed it on another approaching men as well, zapping him as well.
The remaining three men had only two guns left, and he evoked,

“ Romanov Dexter, grant me your power as a member of Trinity Compact of Hendriks!”

The fiery tattoo disappeared from his face, and another tattoo emerged on his left temple, now concentric open circles, and a series of blades or stripes radiated from it to his forehead and his jawline.

One man shot at him, and with a shot of an aluminium projectile toward the bullet, head-on, the bullet's momentum were entirely expended that the core and the aluminium rod thrown back toward the gun, the debris wounded the man's fingers, and his face.

The remaining debris of small lead particulate expanded toward Hendrik's direction, and he went into deep time.
The debris movement slowed down, and he unfurled a large number of reflexium particles on the lead, and entangled them debris to debris, then another web entangled to the glass over their head.
The reflexium particles pulled up and the debris collapsed into a compressed ball of lead particles and pulled upward to the glass surface overhead.

Deep time, was one of Romanov Dexter's very useful skills.
He usually uses it to come up with a plan very quickly, by spending longer subjective time in deep time.
Our brains weren't built to go faster, but the fact that Romanov Dexter had his thoughts not in a biological brain meant he was only restrained by how fast the reflexium particles on his vicinity can compute.
When Hendrik evoked this ability, his mindload were outsourced into reflexium particles on his body, that draws support from the surrounding reflexium particles, and connected into a very powerful distributed parallel computing platform.
This allows him to perceive a slowdown in time, so much slower in fact, that he couldn't actually move his body: his body had not enough time to respond.
But he can still perform reflexius in this state.

At the end of the deep time session, reflexium particles in his brain mechanically and chemically recalibrated his brain to integrate the memory during deep time in his biological brain, while still outsourcing his mindload to the existing reflexium network side by side.
The result was a very seamless transition from normal time to deep time.

He took a moment in deep time to think again, and he commanded his drive, still connected to the computer terminal, to push, really hard, away from the terminal.

Everything returned to normal time, and the glass shattered by sudden impact of small lead particles.
The room rained, and electronics fried up.

A man screamed for the loss of a finger of his, two man gaped, four men paralyzed in the floor, and a man whose arm burned relieved for the rain.

“ Good Lord, now my gauge was not sufficient to completely patch the hole” he murmured.

The drive flung toward his palm, and he ran away, catching the drive in his way back.

The tunnel's shape crumbled as Hendrik had insufficient complexity gauge remaining.
He decided to let the hole be, and jumped down to the car.

The very last juice of his gauge he spent on slowing down his descent to the car.

He entered the car and turned autopilot and a destination.

He rest his body on the seat.

The tattoo on his face disappeared, as the gauge was completely drained.

For the very first time in a very long time, Hendrik regained the feel of a normal human.
The world turned duller for him, he couldn't feel a thing for everything he can observe.
He had lost connection to the reflexium particles.
Somehow, from somewhere, a smile grew in his face.

“ I almost forget how silence felt like.”

He fell asleep in peace.

In his lucid dream, he replayed the entire event and discovered their insignia on their uniform.
It was the insignia of Legions of Umbra Cahaya, otherwise known as the L.U.C., the insignia of The Sea Serpent.

Unbeknownst to him the remaining two men tried to pull a terminal out by force.
And the unparalyzed men tried to carry their comrades out of the building.

In their walk, they discovered a giant hole in the pantry's wall.
The rubble were inside, as if it exploded inward, but the low spread indicated no explosion.
They dismissed it.
After all, they had experienced yet another weird experience today: fighting a man that can move objects from afar, can throw fire, can shoot a bullet head-on to another bullet, and deformed glass panes of the windows into a thin layer of glass shield overhead.
They were more confused on how to describe it to their superior, and managed only to come out with just six words: we were  attacked by a psychic.

Either way, Hendrik got the data in his drive, and the insurgents got what they want in the terminal they stole.

Hendrik woke up near a safe house of BAIK.
He plugged in the drive and searched for its content.
One item piqued his interest, and that was a reference of a divine artifact known as Black's Staff.
The remaining of the documents inside referred to a plan somewhere in Makassar.
A target simply named as PT Sincorp Asia Makassar Branch Office.

He disliked what he just read.
He disliked the name Black's Staff.
It was a long lost artifact, that had been in the surface of Earth ever since Cain's cursed for the murder of his brother Abel.

His gauge replenished a little, and suddenly he could feel every centimeter of all objects in his vicinity.

“ Argh,” he groaned, “ I just missed a chance to enjoy my peace!”
