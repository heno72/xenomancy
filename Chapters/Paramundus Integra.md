# Paramundus Integra

Reunited, Hendrik, Heinrich, Manov, Ashton, Steven, Daniel, Fernando, Henokh, and Derictor, gathered around and discussed this matter.
It is clear that they had to find El on Integra, as well as protecting the other guys whose name is in the list, from being killed.
Their killings must have some reasons behind it, that they wouldn't know if they let them be killed.
So the party decided that they must went there, secure El, contain the malice, and protect the witnesses.
Other than that, Steven put forward the intel he had from David, that Helena is in hell.
This makes Steven, Daniel, and Fernando so adamant in finding El.
Because of the time constraint, they must go immediately.
With the help of Adran, Manov (which already is a volant), Steven, Daniel, Fernando, Henokh, and Derictor were uploaded and beamed to the Integra Paramundus Dataspace.
They took some time to orient themselves in Integra, they arrived at a local organization set up by WTF to facilitate transfer and policing on Integra, since Integra turned out to be a world where baseline to normalcy is calculated.
The organization was called the Global Foundation of Esotericism (GaFE).
They protect Integra from unauthorized accesses, sometimes anomalous to the locals.
After being equipped with necessary tools to help them, including normal guns, resources, etc, and anomalous bags, with an anomalous smartphone.
Basically they must minimize usage of anomalous objects, and the bags are quite safe according to their standard.
The smartphone, which they dubbed the Fiat Ring (because traditionally it was a ring, until recent models were made into smartphones with an user-friendly interface), is their back up tool, if something bad happens outside their control that compromises the mission.
