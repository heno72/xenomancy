# Finding Hendrik

Team 1 tried to get the address of this Hendrik guy, but they had a hard time, since this guy left only a trace of online presence, other than his blog.
Since his account is not private, albeit rarely updated, they use it to track his friends, and happen to find a recent story stream in the last 7 hours, that mentions his whereabouts.
It is tagged with a map location, so they searched the direction to that location.
With Fernando's previous experiences on the Private Police Force, they decided to impersonate officials, and asked in a manner as if they're investigators.
They started to question the whereabouts of Hendrik to the store owner.
It turned out, it wasn't in their shift, so they requested addresses of those that work in that shift.
It turned out, one of them knew the friend Hendrik was with, it was a worker in that bar too.
They locate the worker, and uses the picture they had with Hendrik in real world to convince her that they were his friends, and that they planned to make a suprise party.
The friend asked, "what surprise party?" They remembered that Hendrik's birthday is yet to come, so they decided to say that it is the anniversary of his oath as a lawyer, and they're the colleagues of Hendrik from his university.
As absurd as it sounds, the friend of him actually believes in it.
She also noted how different Hendrik is in that picture compared to his usual expressions when they took pictures together.
They wished to exchange pictures, but unfortunately the Kaos of Kukerphones and the Android Operating System won't cooperate.
Nor can Kaos uses internet connection in this world, due to vastly different protocols used.
Apparently technology on prime Earth and Integra Earth, despite being superficially similar, is vastly different in the background.
Afterwards, they managed to reach Hendrik's apartment, only to discover that they'd need access card to get anywhere in it.
They tried to ask the security guy, saying that they lost their key and if the lobby has a spare key.
It turned out, the security guy don't have the spare key, they can only provide help with the access card.
After some thinking, they decided to wait around, since they believe Hendrik will come home from the office, and then they can follow him to his room.
They waited in the lobby.
