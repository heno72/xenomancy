# Escape Room

Team 1 decided to go through the emergency exit, as, according to Hendrik, he believes that the gang members might not be well aware about the emergency exit.
Steven readied his phone to the offensive mode, and Hendrik asked if he could see his exoself, and Steven said it wasn't the best time.
Nodded, they barged through the emergency exit that leads to the parking lot.
Soon after, they're only midway, Hendrik said that he forgot that they'd need some good minutes to actually finish the stairs, and the trio mentioned that it was quite hot here.
There are no receptions here as well.
They exited at a blocked exit.
In the other side of the door were boxes, so they tried to push through slowly, and peeked.
The gang members are barging toward the entrance to lobby.
They made the door disappears, while normally one would need access card to access it.
Hendrik said he is not sure if his bike is a proper method to get out of here.
Steven said they had a car.
They managed to sneak toward their car, when it was obliterated by the gang member that noticed them.
Steven aimed his phone and flashed a blinding light to temporarily blind him.
They ran, and now have no idea how to escape, with no car available.
Hendrik said that he is currently taking care of his uncle's car, His uncle and his son were in a vacation now, and they decided to take the car.
Fernando drove, and they left the building, breaking the security bars, and full speed toward the exit gate.
The gang members ran to chase them, but they turned north as soon as they reached an intersection, blocking the gang's view to the car.
Hendrik suggested to go to a nearby empty field next to yet another, calmer apartment building, north to his apartment.
They decided to contact Team 2, but Team 2 seemed to be busy.
Soon after, they received a prompt to meet at the house of Fernando's counterpart.
In their trip, Hendrik took a moment to ask, "so, when I lost one of my phones and my wallet today, is it because of what I think it is?"
Steven said that most probably it is.
