# The Agency

Anderson, Henokh, and Derictor had a discussion about the latest trend in corruption, and that the Corruption Eradication Committee, a government independent department fails to retain their independence.
Anderson added that if they want a condusive society, they had to do it themselves; they can’t rely to the state.
Derictor suggested that they must do it alone.
When they did, Derictor provides training for new agents, Anderson provides intelligence and performs infiltrations, and Henokh maintains a network of hidden infrastructures, often with corporate frontend for disguises.
Until one day, one of their company’s sister companies prepared a concert atop a tall building, and something happened: a gate to the other universe opens and disturbed the performance.
The manifestation dissipated shortly after, and apparently none of the crowd cares, they thought it was a part of the show.
