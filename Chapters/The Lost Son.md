# The Lost Son

> In-story date for The Birth of Charles Lee is 20190906 0200

## The Death of Christoper Agape Lumintan

"I think it is about time for me to move to another school," said Ostaupixtrilis Pontirijaris.

Ostapixtrilis Pontirijaris had been hopping from school to school since year two thousand and eleven, in the body of a fifteen years old boy.
At that moment, his name was Christopher Agape Lumintan (or Cal, to differentiate him from other Christophers in his classroom), enrolled in a school right beside a giant church.
He had been there for two years, and it was about some months away from the national examination periods.

It was also the time he decided, that his next school was due.
So he consulted with Alex Pontirijaris, a lawyer.
Also a representative of the Aucantica Authority.

"Very well, Grandpa.
Are you ready to end your life as Cal?"

Cal took his time to take a deep breath.
His entire time in his current school flashed up beneath his eyelids.
His time spent serving at the church beside the school accounted as well.

That was where he meets Anthony, his soulmate of a different faith.
Someone that he'd be looking up again after everything ends.
He was certain that he did not want to meet Anthony's foster father, Aditya.
Aditya was also a teacher, that taught economy, art, and language.
He was also the overseer of the filmography extracurricular class.

Their life rarely sliced on their school, as Cal's interest was history, science, and sports.
In fact he would not have meet Anthony if not because of the church right beside the school.
It was the school, his foster father, Edward Kevlar, attended.
The Church of Umbra Cahaya.
Its mass movements was known as The Legion of Umbra Cahaya (LUC), and had been considered quite radicals by mainstream Christianity.

Cal was registered as an orphan and Edward Kevlar decided to adopt him.

Cal was an orphan, adopted by Edward Kevlar, and attend a school where the extracurricular teacher was Aditya.
Their life rarely slices on their school, as Aditya was a filmography extracurricular teacher, that also taught economy, art, and language, while Cal's interest was history, science, and sports.
However, another part of his dad's life brought him closer to them: his dad is a member of a new religious group, the LUCs.
And apparently, Aditya is one of the elites in that group.

Cal wasn't really interested into Earthly religions, since he had his own personal encounter with his God, Kavretojives.
However he enjoyed the time spent observing others fell into spiritual ecstaty.
There's a certain peace, seeing people finding their own inner peace, however blind they are to the truth.
This view of him was shared with the son of Aditya, Anthony.

They quickly acclimatized with each other, and became close friends.
They're exchanging their views, they're exchanging their interests, and they're exchanging trusts.
Soon, they're bonded with a friendship rivalling the bond one has with good friends that knew each other for many decades, in an encounter that lasts barely two years.

Anthony is an angel of the Divines of Earth, and Cal is an avatar of Kavretojives.
When they meet, the Divines of Earth and the Kavretojives are conversing through them.
All of that wasn't that straightforward to their mortal minds.
To them, they felt like they're having a strong connection with each other.

Who wouldn't fall for the charisma of Anthony: he's young, at his prime, opportunistic, energetic, and brilliant.
Who wouldn't fall for the wisdom of Cal: he's youthful with ancient gaze, that saw the Earth civilization before there's any civilization.
Who wouldn't fall for the combinations of the two, wherever they go, girls gazed at them, giggling when either one of them or both gazed at their directions.

The most popular guy at the church was Anthony, and the most popular guy at the school was Cal, and together they're a strong magnet that attracts young souls to the LUC.
They weren't there for the LUCs, unfortunately.
They're there to witness the dynamic duo, Anthony and Cal.
When they sing at the mass, when they dance during the sings, they're the center of the gravity.

They're distracting the mass's focus, and the relationship came to Aditya.
Aditya came to confront Cal, and there was a fight.
Anthony is baffled because Cal isn't actually a human.

At the heat of the battle, Cal asked Anthony, what it means to be a friend?
"Does your father's enemy have any problem with you, personally?" asked Cal to Anthony.
Anthony was this close on killing Cal, but he stopped, he hesitated.
"You're a good friend, Anthony," said Cal.
"I can't kill you, you're a friend," said Anthony.
"And you will not.
I will die in the hand of an angel, but not you," said Cal.

Anthony was surprised to find that Cal knew he's an angel, an adversary of the Divines.
Anthony backed off, and Aditya asked him what's wrong.
Cal stood in front of them, and unfolded the fabric wings from his clothing.

"He's a White Elf," muttered Anthony.

Cal flew away, leaving the two of them stoned.

Cal did not return home, and instead landed above an abandoned half-built high rise building.
He prepared his phone, and stripped his shirt.
Half naked, he started with "This is take one of the High Rise Parkour,"
He jumped to the edge, lightly jumping around, "I will show you guys the most dangerous parkour feat to be done in this city, yet."
He took a step and prepared to run, and he slipped, with an audible "whoops."

## The Trip
> In-story date is 20190906 0200

He woke up in a trip with two strangers that he recognized.
Both are Yahweh, in different bodies, of different genders.
Living in a society like that of Etoan, trained him not to recognize people by their looks, but by their traits, and subliminal gestures.

"Be ready.
An angel is ready to do his job anytime soon, in this road," said the male body.

The female body looked back at Cal.

"You don't need to remember our names, it is best that way.
After the fall, you will survive, and you will run south.
Keep your path up until you find a narrow walkway.
There you'd meet your next contact," she said.

The car's radio could be heard saying,

"A young boy is found dead falling from a fifteen stories tall abandoned building.
A recovered stream from his smartphone left atop the building suggests that he was trying to perform parkour at the rooftop's edge, when he suddenly slips...."

"What would be my new name?" Asked Ostaupixtrilis Pontirijaris.

"We are not the one to give it to you.
Just remember, that you and your family went to a road trip, and there was a landfall, you escaped, your parents died, and you ran away in horror, only to find a church, and you couldn't remember where the location of your parents were, and their name, and your name," said the female body.

"I guess that is why this is called a briefing.
It was very brief."

"Just sufficient for your own needs," said the male body, "sometimes the less you know, the better the likelihood of you actually completing it as instructed."

"Humans are known for their innate inability to follow the exact instructions," continued the female body, "oh, it begins now."

A tree flew above them to the other side of the cliff.
Some motorbikes fell, some went up against the road, one hit the front of the car, and the male body lost control over the steering wheels.
Then the ground slided with immense thrust, crushing everything in its path, and Cal was thrown off the not secured door, up to the air, and hard against the rocks.
His head was hit, and he was dizzy, and half his body buried under the dirt.

He crawled up, and pain stung all over his body, to the point he could no longer think anything but an inaudible scream inside him.
In fact his entire throats were pulled at such strength, and a second later he realzied he was screaming.
His body shakes, it was both so heavy and so light, he barely able to stood upright.
He found a fallen tree, and stood by its side, trying to catch his breath.

The tree didn't look like it belong there, the roots were exposed, and the branches bent in such a way it was like they were pulled by themselves into those angles, not the kind you'd get by actually bending the trees.
He could still hear himself crying hard, and his breath was running away, as he struggled to catch the breath even more.
He was barely able to gasp for air.

A fight so weird, is happening around him.
It looks so loud, yet it was in complete silence.
Lights flared from one party to the other, explosion seen, ground crumbles, trees uprooted, branches bent in a slithery manner, almost like snake bodies.
Fires set, whirled, and exploded around.

A polar bear were thrown near the trunk Cal sat on, and their gazes caught each other briefly.
Cal recognized the bear's gaze.
The polar bear recognized Cal's gaze.
The polar bear returned to the battle.
And Cal ran away, finding a spot where he wouldn't be wounded.

Apparently he's not the only one in the corner where he hides.
Hendrik hid there, wounded, and surprised to find Cal.
Cal surprised to find Hendrik.
It was someone he meets eight years prior.
"Calvin?" Hendrik said.

"No, this must not be like this," said Cal.

This must not.
He's supposed to cross his path with just the bear.
He's supposed to cross the path with Hendrik two years in the future.
Not now.

Hendrik fell unconscious.

Michael came, rushed toward Hendrik's location.
He meets Cal there, and Cal said, "this man is wounded."

Michael nodded.
He was about to produce a hand gesture, that Cal recognized as a reflexior's gesture to induce amnestic effects on civilians.

Michael is not in the list of people he should've crossed his path with.
He's not essential.
Perhaps he could meddle with Michael instead.

"Before you do that," said Cal, and Michael paused.

"Please, at any cost, not now, for at least the next two years, don't mention anything about me to him, okay?" said Cal.

It dawned to Cal, of what Yahweh said earlier.
That the less he remember, the more likely he'd success this mission.
This is it, Cal thought to himself.
Perhaps Michael is not as uninportant as he thought before.
Perhaps he's supposed to cross path with-

Michael nodded, and snapped.

After the snap, Cal found himself in the middle of the woods, walking toward South, as pointed by the Southern cross.
He couldn't remember much.
He must've been in a car crash.
His parents must've died.
He couldn't even remember his name.
He just knew that he's supposed to walk south, until he found a walk.
And he'd follow that walk.

His feet started to take a walk, to the woods.
Wherever he was going, he knew he went south, toward the direction pointed by the Southern Cross.
About five times its cross length, is the approximate point of the south.
However he was in such a pain he could not embrace the beauty of the starry night.
The kind of looks he'd never get in a city.

He kept walking and walking, until there was a path, and he followed the path right into a wooden church.
He was about to knock, where his head hit the door first, and he fell to the floor.
A man came out, fashioned with a black cassock and a white clerical collar.
The man carried the young body into his bed, cleaning the wounds, cleaning the body, and gave him fresh clothing.

The entire pain he endured during the evening wasn't that easy for his body to take.
The entire scene was relived in his dreams, only with annoyingly surreal alterations.
The trees he sat on waved and warped, crawled and carved on the ground, there was howling.
The ground broadened and tilted up, he was dragged along, and the trees rolled, was about to squash him.

His body lightened and saw the entire ground turned into a vertical wall, and everything on its surface fell down below.
Behind him was the firmament, full of stars.
They glow in a lively manner, they pulsed, and some switched off, when the other switched on, and then it was reversed.

He tried to locate his home star, but suddenly find himself in space, and Earth started to shrink beneath him.
No, he was pulled away from Earth.
At such speed, his breath barely able to catch him, he was out of breath, and for a moment he could almost see Aucafidus, in his arm's reach, despite its immense size and distance away.

"Finish it well and return here."

A voice he recognized, so familiar, so comforting, yet the memory of the voice's owner were out of his mind's grasp.
He almost cried, no, he cried.

"I will return, I will return," he muttered.

"You will not be alone," the voice said again.

He grasped the Planet Aucafidus, and he shrunk fast and to the surface, finding a woman figure with obscure face, she opened her arms to welcome him.
Everything around him started to lose clarity, everything started to blur, and his heart rushed as everything torn apart into a white nothingness.
He made his heart to focus on the obscure woman, reached with all of his strength and grasp her face, and land a kiss on her, a passionate one.

He could feel the smoothness of her lips, her teeth, before his body pulled back, hard.
Something pushed his chest hard, and he closed his eyes.
His lips were still on her lips, never separated, but his body got pulled hard away from Aucafidus.
He did not see with his eyes, but he could see the stars warped around him, and the sky was no longer the sky of Aucafidus.
It was the sky of Earth.

And Earth pulled back on him hard, away from Aucafidus.
He was struck hard to Earth, but not to the ground.
It was in a bed, and a muffled sound could be heard, while his body took beatings from unknown sources.
He opened his eyes.

He was in the priest's bed, and the priest's eyes glared, jolted between the young boy's eyes, and his mouth.
And their mouths were joined, and the Priest had been trying in vain to release his face from the young man's strong grasp.
And the young man released his grasp in confusion.

The priest were thrown back, full force.
Not because of the young man, but because of the priest's jerks.
And everything turned silent in between them.

The priest was cleaning the sweats of the young boy, and prepared him a fresh set of clothes beside him.
The priest supposedly would put the clothes on him, but it appears that the young man had to wear them by his own, after all the awkwardness the priest had to endure.
And so he did wear the clothes, while the priest watched him, still in shock.

"I, I am Christopher, Christopher Lee," said the priest.

The priest decided to stand up, and pulled the fallen chair back to the bedside.

"I am sorry for the inconveniences," the young man said.

He was going to say his full name to Christopher Lee, that he was also Christopher, Christopher Agape Lumintan, or what his friends call him: Cal.
However, like the memories of the obscure woman that he loved so much, it felt like he was able to grasp her, and his name, but they ran away from his grasps, and they were never caught by his thoughts.
He had his name forgotten,

"I, I don't remember my name,"

There was silence,

"Pardon?"

"Father, I don't remember my name."

Father Chris suddenly remember of the story of the Prodigal Son.
He could not help to remember that, because this young man was a lost son of someone.
The story starts with "...
a certain man had two sons," in Luke Chapter fifteen verse eleven.
It was an usual storyteller's trope that would immediately bring to mind Cain and Abel, Ishmael and Isaac, and Esau and Jacob.

Another train of thought came by, and it was the name Abel, whose name can be associated with "vapor" or "puff of air."
The name of Abel was thought to be derived from a reconstructed word meaning "herdsman," with the modern Arabic cognate *ibil*, camels.
It was appropriate, considering that the theory made the name descriptive of his role as a man that works with livestock.

"Father?"

The young man's interruption snapped Father Chris back to reality, and he came up with a smile,

"In that case, I'll call you Abel for the time being."

In his imaginations, Abel was killed by Cain, that is, whatever catasthrope he had been in that he lost his memories, and returned to the world of the living, as a lost son.

Father Chris raised Abel as his own son, and Abel grew comfortable with Father Chris's care.
