---
tags:
  - messianic-arrival
  - draft
---
# Messianic Arrival

> Planet Aucafidus is a trojan planet located sixty degrees ahead of Subralis, a planet that human civilization classified it by the name Planet HD 28185 b.
> Both Aucafidus and Subralis orbits a main sequence star Biculus, that is known to human as HD 28185.
> Subralis is approximately a hundred and twenty eight point six light-years away from Sol in the constellation of Eridanus.
> 
> Planet Aucafidus does not possess any natural satellite.
> It has an exceptionally large iron-nickel core, comprising almost half of the planet's interior volume, and thus posses a significantly stronger magnetic field.
> Due to its large core, Aucafidus is denser than Earth.
> Therefore, despite its smaller size, it boasts a higher surface acceleration approximately one point four times Earth's surface gravity.
> Its higher surface acceleration grants an atmospheric pressure of about twice as much than that of Earth.
> 
> The surface of Aucafidus is currently inhabited by approximately twenty five billion physical entities, spread among roughly two million cities and settlements on the southern continent.
> The largest city is the Hanging City of Sidsido Ahidei, located on the eastern coast of Hadeo.
> The reason it was called a 'hanging city' was literally because the city is build hanging from the Heaven's Tether that connects the ground below to the lowest supramundane habitat strip that encircles the entire planet.
> The tether was comprised of many inter-weaved reinforced single molecule diamondoid ribbons so strong that the entire weight of the city and many number of traffics between the heaven and the ground were only an insignificant fraction of its load-bearing capacity.
> 
> The city's population is around three billion souls, both physical and virtual, encompassing administrative district of about eleven million square kilometers of land and sea area.
> They were not of any particular interest, except that the only person that can save human civilization from their inevitable collapse was one of them.
## Ostaupixtrilis Pontirijaris

Blue sky ahead and a warm breeze could be felt on the face of Ostaupixtrilis Pontirijaris, while the green grass caressed his back.
He gazed up into the sky, where a pair of intersecting dark bands could be seen above the ground, shyly present behind the curtain of cumulonimbus clouds.
The bands were not the only objects hiding behind the cloud curtains, the sun peeks softly as the wind blows a curtain of clouds.
Ostaupixtrilis Pontirijaris stretched his body, beginning from his arms, back, and then his legs.
He relaxed his body again, surrendering to the soft grass field that immediately caressing his skin again.
His eyes turned heavy and his breath slowed down.

A couple of air cars flew by without a sound, and some people appeared to zoom ahead of him toward the city below.
Some people landed near him, and Ostaupixtrilis Pontirijaris knew he should be wide awake soon.
The grass leaves crawled on his skin, and weaving into a green boxer.
That was all he needed, he thought.
With a turn, and an energetic jump, he was standing in front of a group of people.

A grassy green, moldy starfish-like beast the size of a bear standing with their strong pair of elephant-like rear limb, and their tail.
Another pair of forelimbs as thick as their legs were hanging on their sides, while near their stomach are a pair of smaller tentacle-like mid limbs, folding in front of their body, right below what appears to be their digestive orifice, located in their chest equivalent.
Their head is full of red eyespots, with the largest four in front of their “nose” pointed at Ostaupixtrilis Pontirijaris.
Ostaupixtrilis Pontirijaris knew this beast by heart, his best friend, a Solenadactilian known well as Leaf.

Beside that beast is a woman with vigorous youth emanating from her entire body curves and short, silvery hair.
She was not naked, but the black suit covered her body skin tight up to her neck, leaving only her head exposed under the sunlight.
Her gaze was sharp, capable of peering through every man's heart and soul.
A woman he would recognize as his current companion, Tudva Pontiritarax.

And the last person was a pitch black, humanoid figure that appears more like a flat shadow of a man.
Ostaupixtrilis Pontirijaris could not recognize the figure by sight alone, but on microwave wavelength he recognized its pattern as Adok “Adran” Ranensis, his loyal assistant, a synthetic intelligence not bound by a single body.
This pitch black figure was one of its avatar, and he was quite certain that its real body, an autonomous transport vehicle about two thousand and five hundred kilograms in mass, was somewhere else in this city.

“What brings you all here?” Inquired Ostaupixtrilis Pontirijaris.

A series of deep grunts from infrasonic range to the lower border of the sonic range could be *felt* from Leaf, however it was parsed almost immediately by his *exoself* into a meaningful stream of thoughts.

*I have news for you, my friend,* said Leaf.

“What news?”

Tudva Pontiritarax approached Ostaupixtrilis Pontirijaris, her eyes fixated at him, oddly implying watery looks.
There was a certain amount of disbelief, or rather, nonacceptance on her expression.

“You get to see our children and grandchildren on Earth.”

Another noise broadcast in the gigahertz radio frequency range was also present.
It would not normally be perceivable by Ostaupixtrilis Pontirijaris, however again his _exoself_ immediately parsed it into a stream of thought.

`Give my regards to my copy on Earth, okay?`, said Adran.
Its body barely moves, despite the cheery tone its message implied.

“Wait, I am not currently applying for a visa to Earth,” Ostaupixtrilis Pontirijaris recognized her gaze.
It was the same gaze she had nearly two thousand years ago, when he was about to be sent into a mission.

*You’re allowed to return to Earth, not as yourself. You’ll return there with a mission from Kavretojives.*

“What mission? Again? The last time I returned there about a decade ago, there’s no mention of any follow-up mission."

Ostaupixtrilis Pontirijaris did not want to accept the answer he already had in his head.
He wished it is something else he had forgotten, something not too complicated, and something that won't separate him from his loved ones.

"I have done everything I am supposed to do! I even left an instance of myself there to take care of anything unexpected that might pop up!"

*Yes, I am aware of that.*

"Then why am I being asked to return there again?"

There was silence.

It didn't take long for Ostaupixtrilis Pontirijaris to know what has happened.
It was none other but the doing of his own instance on Earth.

"*I* messed up didn't I? I mean my instance there?"

The second moment of silence was clear to be a confirmation for his statement.

"*Good heavens!* It can't be it," already he felt bitterness grows inside him, "did you mean...,"

*The second coming.*

The only phrase he feared in his fifteen thousand years of chronological age, came right from Leaf.
Not because he didn't want it to happen, because it will happen, supposedly without him.
It is supposedly be carried out by his instance on Earth.

"It sounds good, I suppose? Another instance of me would be forked and be sent back to Earth to replace my instance there, isn't it?"
Ostaupixtrilis Pontirijaris understood well that he was being a little bit too hopeful at that point.
He was counting on the fact that maybe it was all that is required.

_No, it is not enough. It requires **you** specifically._

"What makes **me** different than another *fork* of mine?"

If there was anyone capable of speaking hard truth in a direct and plain manner, it must only be Leaf.
Ostaupixtrilis Pontirijaris already knew that for thousands of years.
He just often had to be reminded again and again.
This was one of such reminder.

_This problem requires a sacrifice. **You** are the sacrifice._

His gaze can only trail to Tudva Pontiritarax and Adok Ranensis.
He did not even pay attention to the rest of Leaf's speech.

His mind wanders on what thing must *he*- his Earth instance had done wrong on Earth.
The only possible reason for *him* to be the one that carry out *the second coming* was if his instance *drifted*.

Unthinkable, he thought of himself.

How could _he_ even dare to think of drifting away from *the plan* already laid down by *Kav*?
The one he agreed to help so that humanity can carry on, and one day integrates into the greater galactic society.
To drift from the plan means the collapse of human civilization, and the possible dark ages that follows.

But how is that even possible?

_...you can only reach the correct memetic conditions if you were the one, and not your fork- **are you even listening**?_

"I'm sorry, I need time to process that."

_Some extra time is what I will give to you to process that, then._
And Leaf was quiet for the entire evening.

## Tudva Pontiritarax



----------

*That instance of yours did not have the required memetic tools, that you currently have.*

"Oh, so you're going to take a scan of me and send it to Earth, right?”

Another hope for him to avoid leaving Aucafidus.

*No*

Another slap on his thoughts.

"What do you mean by no?"

*The instruction is very specific, to disassemble you here and merge it to your instance on Earth.*

"That doesn't make any sense. What makes a non-destructive scan of me inadequate for this mission? It will be exactly the same as my present self."

*But the stance of your heart would be different. This one requires a sacrifice. Your instance here is the sacrifice.*

The last sentence had Ostaupixtrilis Pontirijaris stoned.
At the time he signed for this mission more than two thousand years ago, he is informed that this mission requires sacrifices.
A term he agreed fullheartedly.
His old self agreed to it.
It had been several incarnations ago.
And he thought the sacrifice had been given two thousand years ago.

Until he remembered the promise he said that he will return.
He actually thought that it meant Kavretojives will return, that it wasn't literally him.
It was apparent, that it wasn't the case, he literally had to return.

Instinctively, he gazed toward Tudva Pontiritarax.
Tudva Pontiritarax knew that gaze, the gaze of tremble.
Emotions spread like fire, and a single gaze is enough to infect her with a similar tremble.

Tears start to coalesce on her eyes.
Her face turned paler, as her milk-like blood filled the capilaries underneath her cheeks.
She could feel her cheeks tingles, painful warmth spreads over her face, over her eyes.
Her sight blurred, and she felt difficulties breathing.

"It is okay, Os," the pause lasted for less than a second, but for the two of them, it lasted forever.

"I will be okay," her voice started to tremble.

"I don't think I can do it alone," said Ostaupixtrilis Pontirijaris.

"You will never be alone, we will always be with you, even if you're far away from home" Tudva Pontiritarax said.

Her heart said something else completely, *I don't wanna lose you,* but she said nothing.
Not in the vocal range he could hear, not in the radio frequency he could detect.
But through her eyes, Ostaupixtrilis Pontirijaris saw it very clear, as if it was said outloud.

Ostaupixtrilis Pontirijaris spread his arms, welcoming Tudva Pontiritarax, his eyes spoke: I know, come here.
She buried herself to Ostaupixtrilis Pontirijaris’ bare chest.
His chest wet by her tears.

Ostaupixtrilis Pontirijaris had no idea what to say, without thinking, his hands wrapped around her body, he gazed at Leaf and Adran.
Adran walked slowly toward Leaf, its arm reached for Leaf’s mid limbs.
Their left small mid limb wrapped Adran’s right arm.
Leaf gazed at the direction of Adran’s face.
Though Adran was nearly a perfect absorber of light on visible range, Leaf could still perceive the contour of its face via echolocation.

*What makes you decide to express emotion?*

//Isn’t it the perfect moment to be emotional? One of our friends will embark on an adventure without our help.//

*He would have you on Earth, there is nothing to be worried about.*

//Well, we diverged for years already, I am not sure if my copy would find him interesting as I do now//

*You couldn’t even believe in yourself, unbelievable.*

//Hey! That aside, let us show them some compassion//

*Fair enough.*

Holding limbs, Leaf and Adran approached Ostaupixtrilis Pontirijaris, still gazing at them, and his companion Tudva Pontiritarax, still sobbing on his hug.
Leaf and Adran wrapped them around.
Leaf took the liberty to pat Ostaupixtrilis Pontirijaris’ head with their forearm, and Tudva Pontiritarax’s head with their midlimb.

\***

The sun was gone, and Subralis was about to set.
Aucafidus is located at the fourth Lagrangian Point of an exoplanet known to mankind as HD 28185 b.
From the surface of Aucafidus, it appears to be about a tenth the size of our full moon, and about a tenth times as bright.
The phase is always gibbous, and with a large enough magnification, one can tell that the clouds are moving.
Ancient Aucafidian thought that Subralis is heaven, hence it was given the name whose literal meaning is heaven.
In the modern era, Subralis is still the spiritual center of most major religions, because Kavretojives is thought to be seated under the clouds of Subralis.

Ostaupixtrilis Pontirijaris and Tudva Pontiritarax danced slowly in their garden.
The grasses sprouted fluorescent flowers of various shades of blue to ultraviolet, each vibrating at particular frequencies and at various beats, producing a relaxing, and soft ambient music.
The garden was lit, and they bathed under the light of Subralis, and surrounded by the lights of their garden.
The weaving of his boxer loosen, and the leaves that comprises it reabsorbed by the garden floor.
Her suit crawls back to her neck, wrapping itself into a black feathered necklace that shines bright in ultraviolet.

They lied down to the garden floor, and the lights around them pulsated in rhymes, the vibrant colors softened.
They were tasting each other passionately, the pace was sensual.
Their breath synchronized, and they just cuddled, rolled about the garden, and kissed.

Ostaupixtrilis Pontirijaris closed the gap between his chest and her chest.
His hands wrapped neatly around her back.
His neck against her neck.

He took the moment to study every inch of her skin area.
Smooth as silk, warm as milk.

Following his wrapped hands, Tudva Pontirijaris explored his neck with her hands, then to his shoulder, and back to his head.
She guided his face, that was sternly fixated to her eyes, to her face.
Their lips contacted each other.
A light bit provided with her teeth toward his lips.

She could feel it, the tenderness of his lips, and couldn't resist to put her tongue into his mouth, counting his teeth.
Instead of meeting his teeth, her tongue was blocked with his tongue, and they were wrestling, with their shared mouth cavity as the stage.
Twirling, swirling, wrapping they move around the stage, and the cavity shrunk fast, tightening the wrestle between the two tongues.

The owner of the wrestling tongues roll about the garden, and lost in the trance of love.

\***

Subralis was set already.
Two people lied on a glowing, pulsating light garden, panting.
They gazed at the stars, and a pair of dark bands featuring nightlights of Riverside cities, and a peek of stars from behind the bands, penetrating through the rivers.
There are megacities above multiple bands encircling Aucafidus.
Containing far more people than there is on the surface.

“I suppose there is not enough time to enjoy the river of the stars on that band above us,” spurted Ostaupixtrilis Pontirijaris.

“I suppose there is not enough time to even enjoy surface cities,” continued Tudva Pontiritarax.

“Right, surface cities.”

Ostaupixtrilis Pontirijaris rose and walked near the edge of his garden.
There were large, black veils extending from giant arches, and the garden was positioned at the top of one of the largest arches, extending kilometers above the ground.
The veils were there to prevent light pollution.
As a result, the stars shine happily, emanating without obstacles, without being cancelled by ambient lights.
The sky on Aucafidus is pristine.

With a mental command, the inside of the veils was rendered on his mental eyes.
Patchworks of lights on the surface, from dynamically supported giant, tall towers, from which many more plates of cities hung from the towers, each featuring large amounts of tall passive towers, varying in distribution density, height and designs.
Even larger plates of cities hung from the great arches, including the one hosting Ostaupixtrilis Pontirijaris' garden.
There were so much to explore. They were ever changing for every solar cycle.

“So much to explore. So little time. I'm about to be cast from heaven to live among men, again.”

“You'll save a lot of souls, good souls, your sacrifice would not be in vain,” assured Tudva Pontiritarax.

“What if I fail?”

“You won't. You must not. Finish it well and return here.”

Ostaupixtrilis Pontirijaris knew he shouldn't answer that.
The answer should be the action itself: finish it well and return to Tudva Pontirijaris.
Deep inside himself, he memorized the line well and carve it in his heart.

Tudva Pontiritarax stood beside Ostaupixtrilis Pontirijaris.
Their eyes gazed down to the black veils.
Their mental eyes were witnessing the busy surface cities under the veils that never sleeps.
To them, day and night have no meaning, never ending entertainment, joys, dramas, thrills, and whatever they wanted.
People were busy entertaining themselves on the giant bands thousands of kilometers above, and not less busy inhabitants of the surface also enjoys various exotic and or regular entertainments below.
In the middle were the settlements on the giant arches, where natural environment could be seen high above the top.

Ostaupixtrilis Pontirijaris checked the time, it was almost midnight.
It was about time for him to go.
And he thought fast on what he wants to do in his last minutes on Aucafidus.

“Let's jump.”

“What?”

“Within a few minutes I'd be destructively uploaded and beamed to Earth. I want to spend the time free falling to the city of lights below.”

Ostaupixtrilis Pontirijaris reached for the arms of Tudva Pontiritarax.

They jumped from the edge, and they zoomed past the veils, that in welcoming their approach, opened to let them slip into the cities of light.
Under the veil were projections of the night sky, only far more colorful.
The sky projections were overlaid with projections of the entire spectrum of lights, from weakest radio waves, to strongest gamma rays.
All compacted into visible lights.

The vibrant sky lights accompanied with the pulsating active lights from a multitude of city layers they crossed.
As they were approaching the lowest level, Ostaupixtrilis Pontirijaris disintegrated into dust, from his limbs, to his core body, approaching his head.
Tudva Pontiritarax reached his face and kissed for the last time.
In her lips, his lips, his tongue, turned into dust.
What was a warm touch to their lips turned into cold breeze on her lips.

Ostaupixtrilis Pontirijaris was gone.
Pressure could be felt on the entire underside of her skin, slowing her down before she touched the lowest level of the city layers.
It was the most ancient layer, barely changing since seventy thousand years ago.
Still no less impressive by themselves, they were buildings made of glass and metals beamed up several hundreds of meters.
However they seemed dwarfed by kilometers tall diamondoid buildings of nanotech age built on the following millenium.

On a paving stone near a lamp post, Tudva Pontiritarax sat down.
Her tears wet the paving.

## Offers in a Vacation
[Charles Lee](Characters/Daniel%20Ashton.md) is content with his life, but Ostaupxitrilis Pontirijaris (Os) that claimed to be him bugged him in his dreams.
Os told Charles of a mission, to save humanity, to make humans human.
Charles wanted peace, mentioning that Anderson is doing the job well, and they don't need him anymore, as all that he had to be done had been done already.
