# Overtime

"Hey there," said a raven.

Christopher Agape Lumintan opened his eyes, lifting his head off the bench and scanned his rooftop terrace.
On a hanging wire, the raven caught his gaze.
What piqued him the most was an insignia at the raven's forehead.
It was the star and crescent insignia, that represents The Moon and The Morning Star.

"Yes you," the raven glided to the handstand of the bench, right beside him, "I'm called Kao.
And you are called what?"

"Cal, you can call me Cal."

Kao scanned Cal from his head to his toes.
After it was satisfied with what it was looking at, it decided to return its gaze to Cal's eyes.
"I expect a longer name.
Didn't expect that you'd have a short name like that."

Cal was an initial, of his full name, used to differentiate him from four other Christophers from his class alone.
He used to think that he'd be called Chris instead, but it was only for a second when the first one he introduced himself into was named Christopher, and was also called Chris.
It took two more Christophers and the rest of the class started to give them other names: Chrome for Christopher Jerome, Cain for Christopher Indra Adamson, C.J. for Christopher Julius, and finally, Cal for Christopher Agape Lumintan.

But it wasn't the focus of Cal at the time.
A raven talked to him, with the star and crescent insignia on its forehead.
And it talked non-stop.

"I've never seen a White Elf before.
Rumor has it that one was in our neighborhood.
I decided to take my luck and see one with my own eyes.
Hah, you're no different than any man on the outside."

Cal thought it was done talking, when it continued, "but really, I could feel that you're different.
Hear me out.
If I closed my eyes, I could feel an empty space the shape of you in front of me," it closed its eyes, "see? You're an empty space."

It opened its eyes again, and Cal thought it made its point already.
It didn't, its talking continued, "see? I can see you again.
You can't imagine how frustrating it felt like.
To see you but not to feel you.
Everyone, everything, I can feel, I can see.
I can see you, but I can't feel you."

"You're a reflexior," said Cal as Kao took a break to breathe.

"Oho, that is very obvious isn't it?
Don't you see the insignia on my forehead?
I am a proud member of the Eastcoast Congregation of Corvids."

Surabaya was positioned on the east coast of the Island of Java, indeed.
It continued to explain how the ECC had been in conflict with the Southern Sea Cetacean Pod for many centuries already.
Of how they were greedy, even though they occupy a completely separate domain from the corvids.
Corvids couldn't enter the water body anyway, and the cetaceans got the entire water body of the southern sea.

All of those didn't matter for Cal.
It didn't matter for his mission anyway.
His mission was to save humans, one of the intelligent species on Earth.
A mediocre intelligent species that could only think of how great they were.
They thought that they were the best, and the most sophisticated animal on Earth.
They were the best in something indeed, the most destructive intelligent species on Earth.

## Definitely not a talking raven

Grasses were dancing under the guide of incoming breezes.
Birds chirping around, filling in the orchestra in a small square, probably less than three thousand meters squared in area.
Surrounding them was cold, industrial storehouses.
People roamed around, completely oblivious to the central grass field.
They were carrying loads, delivering papers, socializing, and ordering folks around.

"Do humans fail to see the beauty of that scenery? In the middle of this complex? Are they blind?" Inquired Kao.

"They are well aware of it," said Cal, his gaze panned around, "they just didn't care."

"Shame. Snacks are around in the field. Tell me, why didn't humans enjoy snacks the same way we did?"

"Those snacks of yours, are considered bugs for them, and bugs reminded them of roaches that scourged their leftovers."

"They're too hygienic," Kao scoffed, or so Cal thought, "A little dirt is good for your stomach."

"Well, they invented cooking, they get more nutrients that way," Cal paused, "I'm sorry, but why are you following me around these past few months?"

"You're my first white elf friend," Kao jumped to a passenger seat of a red motorbike.

Cal paced on the paved road, scanning every single storehouse at the complex.
He was looking for one particular storehouse, a law office of his grandson.
However, his thought was distracted that he couldn't find the office.
He was pondering on which one should be weirder, that he was talking to a raven, or that no one cared about that talking raven at all.

"Grandpa," said someone in front of the red motorbike.

Cal turned to the man standing in front of a red motorbike, in front of a law office, "Alex."

Young, energetic, and agile steps were done by Cal, and in a single blink of eyes, he was hugging Alex already.
Alex, that was biologically ten years older than Cal, hugged back.
Young, energetic, and agile were qualities one wouldn't normally attribute to someone they call grandpa.
Cal, was an exception, his biological age was fifteen years old, while his chronological age was that of a thousand times.

"You know one would believe if you say you're your grandchildren's younger brother," said Kao.

"I'd believe that he's my son, in fact," with a raised eyebrow, Alex said to the raven, "but he's my grandpa nevertheless."

"If you don't mind, Kao, I need to discuss something private with Alex," said Cal, he rushed to the office, the door was locked before Kao could fly in.

"I'm sorry, who is he?" Hendrik approached Alex and Cal.

"My grandpa? I did say he'd come here," said Alex.

"This is definitely not your grandpa."

There was a knocking noise.

"And the one raven that is knocking the door is definitely not a talking raven," replied Cal.

Hendrik opened the front door and Kao flew in.

"Sweet, another white elf and a reflexior," it landed at Hendrik's laptop.

Hendrik took a moment, petrified, "okay, that's fair."

"What else will I find here? In this concrete cube?" inquired Kao.

Hendrik snapped a finger, "definitely you wouldn't like staying at my laptop like that."

A flame jumped from Hendrik's fingers toward the raven.
With a swipe of its right wing, the flame dissipated.

"I'm a civilized corvid, how dare you dismiss me like ferals," it flew toward Hendrik and black strands formed from the walls and the floors, slithering and tangling themselves to Hendrik's body.

A shadow figure choked the raven midair.
Kao wrestled to no avail, it couldn't break free from the shadow's grip.

"What is this?" Asked the raven.

"You might disarm my body, but you couldn't stop me"

There was a sudden silence as Cal finished reciting a phrase the raven and Hendrik didn't notice in their fight, followed with a snap of fingers.
Hendrik remembered the sensation vividly, a sudden feel of powerlessness.
A sensation he felt years earlier in the Tengus Incursion Event.

Black strands that held Hendrik disappeared into thin air.
The black shadow that held the raven midair was gone.
Kao managed to catch its weight midair and flew to a chair beside the front door.

Kao and Hendrik looked at one another, their gaze communicated understanding.
They were both powerless, their sixth sense didn't work.
Kao could no longer produce a human voice.

"I did say I have an appointment with my grandson," said Cal.

"We could do that?" Asked Alex.

"Actually, it's just me.
I'm an ambassador.
It comes with perks of sovereignty."

"M.G.," Hendrik said, his breath catched him up, "is that you?"

"We met much earlier than M.G.'s era," said Cal,
"But now is not the time, we'll catch up again, Hendrik."

Cal and Alex went to the meeting room, leaving Kao and Hendrik powerless at the office reception.
The corvid squeaked, but none of those made any sense for Hendrik.
Hendrik told it to go away, but the raven couldn't understand it.

## I'm sorry, my grandson

"So you want to end Cal's life?" Asked Alex.

"Yes, I have been Cal for more than two years, and, uh," Cal hesitated, "the national exam is about to start, so I don't have to partake in it."

"Is that the sole reason why?"

That question hits Cal hard.
It wasn't the sole reason.
The other reason involved regrets.

"I stayed longer than I should have," said Cal.

"Uh, Edward Kevlar wouldn't like it," said Alex.

Alex saw that Cal's eyes didn't waver.
Cal's gaze penetrated every veil in Alex's soul.
It screamed certainty, anything else didn't matter for Cal anymore.
Not even Cal's own life, and everyone involved in it.

Alex knew he wasn't talking with Cal at that instant.
Cal was never his grandpa, even though he called Cal his grandpa all the time.
Cal was a mask, an identity, a tool of his grandpa.

Ostaupixtrilis Pontirijaris's gaze penetrated through Cal's gaze.
Cal's young, inquisitive, and energetic gaze was no longer there.
An ancient, indifferent, and calm, knowing gaze replaced it.
Alex knew he was looking at the gaze of his real grandpa.

"Very well," Alex's hands trembled, he clenched his fist to counter it, "are you ready to end," his voice started to waver, "the life of Christopher Agape Lumintan?"

Alex was barely able to breathe, as a stinging, icy cold sensation crawled from his spine, and spread to every nerve endings of his body.
He almost forgot how it felt like to talk with a fifteen thousand years old entity.
An entity older than the entire human civilization, was standing in front of him.
And that gaze was gone.

Alex's breaths returned, and in front of him was just Cal, with a normal fifteen years old young effervescent gaze.

"I'm sorry, didn't mean to choke you," Cal said with a warm smile, his hands scrubbing his neck.

"Can't you," said Alex, he was relieved, but his heart still raced fast, "at least give me a warning next time?"

Cal giggled, while roaming around the room, to the back of Alex's seat.

"I said I'm sorry, my grandson," Cal hugged Alex from behind.

It was warm and soft, bubbly and comforting, like any hug from one's close friend, or like any hug of a kid to their parents.
"That's fine, but just let me catch my breath okay," said Alex.

Cal smiled, he moved around and sat on Alex's lap, facing Alex's desktop.
"This time I want a truly immersive human life," he said.

"No more ambassador body?"

"No, just a body of an ordinary human," Cal picked a piece of brochure with a photograph of the Ijen Crater.

"What about your job as an ambassador?"

"Am I not allowed" Cal said with an unnervingly ancient and calm tone, followed with a lighter, younger tone, "to take a break?"

"Whoa," Alex raised his hands away from Cal's body, "you wouldn't even let me catch my breath, would you?"

Cal giggled, and Alex was melting in laughter.

"Say, grandpa, out of curiosity," Alex stopped laughing.

Cal didn't like where the conversation was going.
He knew it was inevitable though, that his inquisitive grandson would want to know more.

"Tell me, why did you stay longer than you should have?"

## A martial art. Nothing more, nothing less

> After spending three years as M.G., and another year as Alfa, Ostaupixtrilis Pontirijaris started his new life as Cal when Edward Kevlar decided to adopt him at an orphanage, and to enroll him in a decent school.
> It wasn't a bad day, and Edward Kevlar with his wife were not bad parents either.
> Everything was perfect, except that they were attending a particularly shady church, the Church of Umbra Cahaya.

> That particular church ran a school, and it was the school that Edward Kevlar considered "a decent school."
> It wasn't a bad school to begin with, except that finding kids with powers weren't uncommon there.
> In fact, kids with powers were embraced there, and were given special training.

> In no time, Cal made it into the ranks for a simple reason: the best clairvoyants in that school couldn't perceive Cal in any of their arrays of sixth sense, even though Cal was there right in front of them.
> It was considered a special power to them.
> To them, Cal was just like any kids with special powers that attended there.

> It wasn't.
> It was just a perk of him as an ambassador of Kavretojives, or Kav for short, an allied benevolent extrasolar Power to the Divine Council of Earth.
> Powers on Earth, that ran the Divine Council of Earth, promised sovereignty to the ambassadors of its allies.
That was all.

> As an ambassador, Cal held diplomatic missions, like what he had done in the past eight years since year two thousand eleven.
> He performed flawlessly in the past years, he was certain in doing his missions.
> Every mission under different human identities, to facilitate his missions on advancing Kav's cause.
> Every identity served him like tools, almost like gadgets.

> Until it wasn't.

> Cal's identity was required to negotiate a deal with one of the Powers on Earth.
> The Power's contact person was none other than Anthony Matthias.
> The boy that he had met since he was M.G., that he promised to meet again.

> Never did he know that to fulfil that promise, he had to lose Anthony again.

## Do you know me?

"Hey, new kid!"

Cal looked at the source of that voice.
A student with a green badge by his collar was approaching Cal.
Cal recognized the appearance.
The boy he met five years earlier as M.G., had grown into a second year high school student.

M.G. should be the boy's senior, but Cal was the boy's junior.
To Cal's surprise, he found himself chuckling.
He suppressed it, alas the boy noticed it first.

The boy looked at Cal with a raised eyebrow, "what's funny?"

"I'm sorry, *Kak,* you just reminded me of someone I met a long time ago."

*Kak*, or its long version *Kakak* is a pronoun in Indonesian, used to refer to someone not significantly older than the speaker, or to the older brother or sister of the speaker.
Equivalent to Thai pronoun *Phi*, Korean pronoun *Hyeong* (masculine) or *Noona* (feminine), and Mandarin pronoun *Ge* (masculine) or *Jie* (feminine).

"Well, am I that person?" Asked Anthony.

*Well, yes you are,* thought Cal, he didn't voice it out.

Cal did remind Anthony of someone else too.
Someone he met only once, five years ago.
*That someone must be in college by now*, thought Anthony.

"Forget that," Anthony shook his head, "I see that you're wearing a white badge. So you're a white mage, what can you do?"

"Actually I didn't do anything. Any kind of power just didn't work at me."

"So your power is a nullification power?"

"A negation of power is not equal to a power."

There was silence.
Anthony couldn't say anything against it because it wasn't wrong.
For the first time since he first met this new kid, Anthony put his focus on his sixth sense.
The new kid wasn't there in his sixth sense.
Just like the senior he met five years ago.

"Uh," Anthony took a step closer to Cal, "what's your name again?"

"Christopher Agape Lumintan. You can call me Cal."

Anthony's eyebrows wrinkled closer together.
His eyebrows raised when he realized that, if this new kid had a white hair, he'd look exactly the same as the Michael guy.
But it wasn't possible.

"Do you know me?" Anthony was meant to say *Do I know you?*, but his tongue betrayed him.

Cal smiled, "we meet again, Anthony Matthias."

## Not the answer you're looking for

> Every student of that school, disregarding their religion, was required to attend a religious mass once a week.
The school was right at the end of a gang beside the chapel.
One just had to walk for about thirty meters from the chapel to the school's front gate.
It was a common sight that students would take a walk to the chapel every Friday morning, attending the mandatory mass, and return to the school by foot.
One thing for sure, after each mass, it was a sea of students.

Anthony undid his props as fast as he could, and rushed toward the sea of students.
He didn't even say a word to the chief of dancers before he left.
His sight roamed across the crowd, and his target was acquired.
Finding one person among the crowd wasn't the hardest part, it was only a half of the problem.

The other half was to get to the target, in an unforgiving crowd of bored students, pacing as fast as they could to get back to the school.
Ironically, the faster everyone in that crowd tried to move, the slower the crowd would move.
Such was the obstacle faced in a traffic jam, and the one Anthony was facing to get to his target.

Past the bottleneck, that was the chapel's front gate, the crowd flow relaxed, thus reducing the jam.
Anthony was battered nevertheless.
Moving through an unforgiving crowd was almost always tiring.
His target was midway through to the school's gate.

His feet brought his body in an ever growing velocity, and he jumped to his target.
At the moment his hands touched the target, the target moved in a rapid succession, Anthony didn't realize what was happening.
The next thing he knew, he was lying on the ground, while Cal was trying to get him back up.

"You don't jump to people like that," Cal said, pulling Anthony's hand, "it's dangerous."

Anthony had jumped to many people before.
None managed to knock him down in less than a second like Cal did.

"You're dangerous," Anthony said, he managed to sit, while a crowd formed around them.

Anthony tried to recollect everything that happened in the past minutes.
He was jumping toward Cal, that upon contact, shifted and manipulated Anthony's center of mass through a step of moves.
A defensive reflex, yet was sufficient to knock Anthony to the ground.

"No," Anthony said with a growing smile, "your reflexes are dangerous."

"I'm a trained martial artist," Cal assisted Anthony to stand up, "what do you expect to find when you assault me like that?"

The crowd lose their interest and move on toward the school.

"You should join the ministry of praise and worship!" Anthony wrapped his left arm on Cal's neck, "you'd make a great background dancer!"

There was a pause, while they were looking at each other's eyes.

"Is that what you really want to ask me?"

Cal said that with a soul-penetrating gaze, that made Anthony feel naked.
It was as if Cal would know every secret of Anthony's soul in just a single gaze.

"No," said Anthony, he limped heavily as he moved.
Anthony had been waiting for five years to meet Michael again.
Who would've thought that he'd meet Michael again, as Cal.
Why?
Anthony had no idea, he might be the answer that EPL was looking for.
Dominic died, and the secret of M.G. Style teaching was yet to be reverse engineered.
Cal caught Anthony's thinking gaze.

Surprised, Anthony snapped out his train of thoughts, "I guess, I just want to spend more time with you."

"I'm not the answer you're looking for," said Cal, his gaze affixed at the school, "but you might be the answer I am looking for."

"That's enough, I guess."

## Do I get your attention?

Praise and worship service was meant to help setting up the mood for the mass.
The dancers were meant to be the background dancers, only for the praise and worship didn't feel empty.
They were meant to be fillers, not to be the center of the attention.
Until they weren't.

The crowd jumped rhythmically with such a great force, that the floor shook with the beat.
Women screamed from the bottom of their lungs as two young boys danced at the stage.
They seemed to forget that the main focus should be the praise and worship.

The performance concluded, and the mass couldn't move on from the performance.
"We want more! We want more!" Was what they chanted.
Anthony and Cal thanked, bowed, and left the stage, while female attendants of the mass squealed a piercing noise.

"You rocked the stage!" said Anthony, he was about to jump on Cal, but backed off.

"What, afraid that you'd find yourself on the ground again?" Cal put his widest grin.

"Being cautious is not wrong," Anthony shrugged.

Anthony and Cal undid their props, while the chief of dancers congratulated the entire dancing team.
The chief's words weren't the focus of Anthony and Cal.

"Thank you for tutoring me the moves," said Cal.

"I kinda regret that I did," said Anthony.

"Why?"

"You did it too well, the mass couldn't handle it."

Cal scoffed, "I'm too cute to handle."

"Where's the barf bag, I need one."

Cal and Anthony exchanged punches, when the chief of dancers stepped beside them.

"Do I get your attention?"

## Just as troublesome

"The answer, is in the Divine Council," said Cal, while fixing Anthony's stance, "-no, you don't do it like that, oh-"

Anthony threw a jab toward Cal, "what about the Divine Council?"

Cal deflected the jab with his elbow, "you don't give a jab like that, the force must come from the ground, through your body, and channeled to your fist," his hands locked on Anthony's wrist and collar, and the next thing Anthony knew, he was lying on the ground again, "nothing you did here can be done if not because the heavens permit it."

"So, if we want to defeat the serpent, we have to aim for the head,"
Anthony jumped off the ground, he rooted his feet to the ground, and sent a kick.

Cal pushed Anthony's knee before Anthony's foot could reach Cal, and Anthony found himself rooted to the ground again, "but we can't do that, we are not powerful enough to strike them head-on, we need a distraction."

"That's too complicated just to establish a new world order," Anthony was still confused that his foot didn't land on where it should be, on Cal's body, "how could you do that?"

"You're so focused on your foot, that you forgot my other foot was on the way to stop your kick entirely," said Cal, "see, we just need to strike at the right point, and at the right time, and before your opponent knew it, you won already."

"How do we distract the Powers?"

"Find a problem that is just as troublesome as the Powers themselves."

Anthony prepared another stance, "another power?"

"Have any in your mind?"

Cal and Anthony exchanged moves.
A kick was blocked, a jab was deflected, another jab avoided, and Cal pulled Anthony's jabbing hand to weaken the stance.
Anthony found himself on the ground again.

"Fallen," said Anthony, his hands scrubbed the ground, while his gaze was set straight to the stars, "the fallen power."

"If we can make him rise again, balance in the heavens will be disturbed. Enough as an opening to send a final strike. Then, you could achieve a new world order, exactly like what EPL was aiming for."

"But, *Toru El*, he was gone for many millennia already. No one knows where he is now."

"That is why, we must find him."

"How? I told you no one knows where he is," Anthony rose from the ground, he cleaned his back.

Cal lent a hand to clean Anthony's back, "let me worry about that, you should help your father with his quest to find Yam's will."

Anthony disliked the way Cal said it, "why does it sound like you're leaving soon?"

"My mission is done just now, I should move on," was what Cal wanted to say.
He couldn't bring himself to say that.
His thought roamed hard to find a reason that his mission was yet to be done.

"I haven't finished teaching you this," said Cal.

"M.G. Style Tier One? I don't see why you must teach me Tier One if my power is already equivalent to at least Tier Two?"

"There is no tiering system in my teaching," Cal smiled, "my students categorized it into tiers to help themselves wrap their mind around the idea. I just followed it for practical reasons."

"Then what are you teaching me?"

"A martial art. Nothing more, nothing less. A technique I developed through my fifteen thousand years of experience in hand to hand combat."

Cal sat on the ground, Anthony sat beside him.

"That allows your students," said Anthony, "to be mages?"

"No."

"So why bother teaching me at all?"

"Didn't you say you want to spend more time with me?"

Anthony swallowed the statement hard.
It wasn't wrong.
In fact Anthony was not against it either.
Anthony decided to nod, his gaze affixed to Cal's pair of eyes.

"Let me tell you the truth," Cal lied down, his gaze to the stars, "there's no M.G. Style, there's no tiering system. I taught them martial arts because I need them to establish a strong connection with their body. Only then, they could focus on something more than their body."

"Interface with the RFL," Anthony let his back fall to the ground beside Cal.

"Exactly. The martial art I taught is just a tool, it is not the technique."

"So the entire M.G. Style teaching," Anthony's gaze trailed from the stars to Cal, "is based on a lie."

"A useful lie indeed," Cal's smile grew, "once you connect with the RFL, you no longer need it."

"You told me this, now. So why didn't you tell the EPL when they requested it? Why did you let the conflict between EPL and ORCA happen?"

Cal turned to Anthony, "it wasn't my job. I did my job as Michael, and when I'm done, I'm out."

"But in the end, you just told me the secret, right here, right now. What changed?"

Anthony could hear Cal's breath, there was a heavy burden when he exhaled.

"I need you to know, I am not Michael. I was, but not anymore."

"You're the same person, as Michael."

"I tell you this, not as Cal either," there was something heavy in Cal's tone, also something ancient, the voice of a tired old soul, "I am telling you this as *myself*."

Goosebumps grew on Anthony's skin.
He could feel crawling iciness grew from his kidney, to his spine, and to his limbs.
His heart raced, his breaths shortened, he almost choked.

"I am tired," Ostaupixtrilis Pontirijaris said.

"I think after this I will take a vacation," Cal said.

A heavy burden was gone off Anthony's chest, his breath grew lighter, "where are you going?"

"Not *where*," Cal's gaze returned to the stars, "it is *how*."

"But now," Cal continued, "let's just enjoy this moment, right here, right now."

Cal hadn't answered Anthony's question, but he didn't dare to ask for it again.
