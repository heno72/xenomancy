# The ORCA-strated Event

A two meter tall man with pale skin and silvery-white hair, spoke with Nurhayati Maulidia, codenamed Klandestin.
His white crocodile-like eyes were no longer a surprise for her.
Anderson Pondalissido, codenamed Enam, was known well to be the only shapeshifter in the Intelligence Agency.

Another well-dressed man joined the conversation.
He was Nicolas Armaniputra, codenamed Jokowi, the only medical doctor in their team.
Nurhayati caught herself gazing at Nicolas's sky blue hem, which gave out the outline of his athletic body.

Anderson poked at Nurhayati, he giggled.
Nurhayati launched a fist to Anderson.
She denied that she was peeking at Nicolas, despite her wandering gaze landing at Nicolas again.
Nicolas covered his blushing face, attempting to appear comfortable.

"Flirting with Doctor Nicolas again, huh, what about Derictor?" Said another tall man in black shirt.

Nurhayati's gaze landed at him.
He was huge, at one hundred and eighty five centimeters in height.
His black shirt emphasized his fair skin.
His smile alone was enough to make Nurhayati freeze.

"Andre, Andre Tjahaya Purnama," said the man, offering a handshake, "my codename is Empat."

Nurhayati took his hand and shook it for a full minute.

"You can let go of his hand now," said Anderson, his evil smile extended from ear to ear.

Nicolas was fascinated at Anderson's face, noticing that his crocodile-like eye slits subtly opened in his smile.
He was about to ask more on Anderson's physical features, when another man with a lean body frame wearing white shirt caught his attention.

"Bright Spears, codename is Arjuna" he said, offering a handshake to all of them, repeating his name on every handshake.

Another man with a lean body wearing navy blue hem and brown cotton pants entered the room.
Everyone knew him as Derictor, codenamed Delapan, a good friend of Henokh from the DoC.
He fixed his round glasses, "alright, *Khun* Chandra and Kang Hae-In are going to arrive soon, please be seated."

"Okay, apparently all six of you are here.
Splendid!
Let's begin the briefings then," said Kang Hae-In.

"Okay, listen up," said Chandra, " Mobile Task Force Psi seventy-two, we will brief you real quick.
A charity concert will be held in two days at the rooftop of one of the highest buildings in Indonesia.
There, Ernie, our contact, will be performing. The objective of this mission is quite straightforward.
Find Aditya or Anthony, and attempt a capture of either of them."

"For that, I will divide you all into three teams of two," said Kang Hae-In,
"Klandestin and Enam, both from the first Division, will be approaching her again, to see if we could gain any new information on the whereabouts of our targets.
Empat and Arjuna, from the third Division, you two will be the one to engage with Aditya and Anthony.
Jokowi from the second Division, and Delapan from the fourth Division, you two would blend into the crowd, see if anything is suspicious, especially on whether or not this operation is compromised."

"Is there any question?" said Chandra.

Bright Spears raised a hand.

"Yes."

"Bright Spears, Codename Arjuna, Sir.
Why do we need to be split, with two teams basically doing nothing, and only us are cleared to engage with Aditya and Anthony?" Asked Bright Spears.

"Dispersion, and contingency," Chandra paused, he approached Bright, "we can't be seen together.
Secondly, we don't wish to compromise the trust we gained from the contact.
That way, if this mission fails, we could still track them through Ernie."

Chandra turned to Kang Hae-In, "didn't you teach them about this?"

"I certainly did.
But that is normal, HAHAHAHA-uh, okay,"
Kang Hae-In's laugh stopped as Chandra gave him a glare,
"see, I told them about dispersion and contingency.
But Bright here, I mean, Arjuna, is a new recruit, certainly he'd need to have a firsthand experience of those concepts in the action to fully grasp it."

Chandra didn't say anything to Kang Hae-In, his gaze turned to the task force,
"is there anything else?
None?
Okay, then I'll excuse myself.
Kang Hae-In, I'll leave them to you."

"Okay, guys," Kang Hae-In took over as Chandra left the room, "this is the first mission of this first ever multi division task force.
I'm very excited for you guys, the best of the best in your divisions, assembled into a task force!"
Kang Hae-In's face brightened, his smile was wide enough to cover his entire face.

He looked around to see if anyone responded to his excitement,
"don't worry, I don't bite, I'm not *Khun* Chandra, HAHAHA!"

Derictor was the first to crack, and the rest started to giggle.
"*Hyungnim* is the best," said Derictor, making a heart with his fingers.
Kang Hae-In's laughter intensified, and the rest of the task force followed him.

*Hyungnim* is a term in Korean, a combination of *Hyung* (older brother) and *-nim*, a respectful way to refer to someone.

"Okay, okay, now keep this good spirit!
Don't be stressed, just do your best,"
Kang Hae-In continued as the task force calmed up.

He handed them with folders, "there you'd find the known physical characteristics of our targets, and of Ernie.
Included within are floor plans of the building, escape routes, and security cameras."

"Okay, that's all.
Study the documents well, and dispose of them to the incinerator," Kang Hae-In looked at them,
"I wish you good luck.
Dismiss!"

## A polar bear in formal wear

Anderson was in his young Chinese-Indonesian man persona again, in a formal wear.
Nurhayati helped him tidy up his hair and wiping his sweats at a makeup post.

"You looked tired," said Nurhayati.

"Very, I had to shift to this body in just two days.
Turning to a smaller body is harder than the other way around," replied Anderson, "I had to lose excess weight and height."

"Oh my God!
How gorgeous!"
Screamed Ernie as she entered the room.

"Oh why thank you," said Anderson, his face brightened in an instant, as he offered a hand for her to shake.

Ernie went to give cheek kisses as a greeting instead.

"Can't wait to taste the main course huh?" said Anderson, he giggled.

"Aw," Ernie's cheeks warmed up, she walked to Nurhayati's side, "Nur, your husband here is very flirty!"

"Oh stop," Nurhayati giggled, "I would've kicked you off if he's my husband!"

"Oh," Ernie's eyes popped open, as if they were going to jump off, their attention jumped to Anderson instead, "so, you're vacant?"

"I am single," Anderson let out a warm smile.

Ernie shrieked and jumped in joy with Nurhayati.

Bright in security guard wear walked past the make up room, took a peek inside.
Ernie was flirting with Anderson, they were having a good laugh.

"This is Arjuna, the Bunny is engaging with Enam and Klandestin, over" Bright said through the communicator.

"Copy that, Arjuna," said Kang Hae-In,
"check the other rooms too, we haven't spotted Anthony or Aditya yet, over."

Kang Hae-In checked the monitors of the control room,
"Jokowi, Delapan, report!"

"Delapan here, I noticed a polar bear in formal wear by the southeast corner of the room, over."

"Copy that, Delapan, can you check on the-" Kang Hae-In's brain stopped working,
"Uh, Delapan, can you repeat, over?"

"You heard me, Houston, there is a polar bear in formal wear by the southeast corner of the room, over."

They had a shapeshifter in their team.
Kang Hae-In thought that it was more than enough dose of weirdness.
Having a polar bear in formal wear in the middle of their mission was beyond Kang Hae-In's imagination.

"Can anyone get me a visual of a polar bear in formal wear by the southeast corner of the room? Over,"
Kang Hae-In couldn't believe he said that.

"Houston, Empat here, we have a problem," said Andre, "the polar bear is looking at me."

On the video feed, a polar bear in formal wear looked directly at the hidden camera on Andre's chest.
For Kang Hae-In, it was as if the polar bear looked directly at his eyes.
The polar bear set up a stance, and jolted a fist.
There was a firebolt, and the camera turned static, followed with a scream of Andre.

"Contact! Empat was hit!" Said Derictor, "Houston, Delapan and Jokowi request a permit to engage."

"Delapan and Jokowi, Engage!"
Kang Hae-In took a moment to process the situation.
It was completely unexpected to have a polar bear that could produce firebolts.
They weren't planning for this.

Nicolas and Derictor rushed through the dense crowd of people.
No one seemed to care about Andre, and the polar bear.
The polar bear walked away.

"Jokowi, I'd be following the polar bear, you stay with Empat," said Derictor.

He pushed himself against the crowd, toward the polar bear.
The polar bear effortlessly walked past the crowd.
The crowd somehow avoided the vicinity of the polar bear, while completely unaware of its existence.

*Is it going to happen again?
Another silent concert?*
Thought Derictor to himself.
He checked his auditory senses, all was working fine.
He could hear the crowd.
He must be beyond the silent bubble.
Or that there weren’t any bubbles of silences at all.
It was something else entirely, people didn’t notice any anomaly.

Working his way through, he went to the side of the room, and found a tall man in black cassock, white clerical collar, and a black fedora.
Derictor tried to pass by the side of the man in cassock, but the man blocked him.
He tried through the other side, the man blocked him again.

"Please move away," said Derictor.

"Please don't," said the man.

"You don't understand, there's a polar bear there, and I had to-"

"I know, that is why you must not go any closer."

Derictor was stupefied, "y-you know about the polar bear?"

"Yes, I'm with him.
Now please move away, we don't mean any harm."

From the other corner of the room, the polar bear's mouth was opened wide.
His throat was on fire, and the blue fire spread to his furs, burning the formal wear along the way.
The polar bear was covered in blue flames.

"That," said Derictor, "is what you mean by you don't mean any harm?"

"You don't understand, we're protecting you *humans*."

"Are you not a human?"

The man looked at the ceilings, "too late.
Just don't disturb us.
Let us do our job."

"Hey, what are you-"
Derictor was petrified.
A black humanoid shadow stood in front of him, it held Derictor's hands.
Derictor was having goosebumps, as the cold shadow melted and creeped on his entire skin.
He could not move, nor could he speak.
His eyes and his breathing, were the only thing he could control.

Three more shadow beings appeared behind the man in cassock.
Derictor looked at the ceilings, and a weird spherical distortion appeared.
It was white, with black dots, that appeared to be denser at the rim.
A distorted sight of a number of human-bird-like creatures, a lion, a tiger, and a babirusa were enlarged from simple dots.
The creatures started to normalize, the closer they appeared on the distortion.

The time Derictor managed to process what he was observing, they were flying under the ceilings already.
Everyone saw that, and the performers were petrified for a moment.
The lead singer continued his song, albeit shaking.
The background dancers resumed their dance.
They tried to pretend like nothing happened.

"Delapan, report.
Can you confirm the sightings Jokowi reported?" Said Kang Hae-In.
Derictor couldn't say anything.

## A flaming bear

A tall man in cassock identical to the one Derictor encountered stood beside the flaming polar bear.
He had a flame-patterned tattoo on the right side of his face.
He was looking at the tengus and the barongs fighting on the ceilings.

"Xiangyu," said the man with a tattoo, "how long is it until you can neutralize them?"

The polar bear produced a modulated roar, and a short moment later, a humanoid voiceover could be heard, "faster if you stop talking."

"Harsh. Just make it quick."

The polar bear started its dance, while the man with a tattoo jumped near Derictor.

"What is this guy doing here? Hendrik?"

"Oh, hi Heinrich! Don't mind him, he was disturbing me. Why don't you help Xiangyu?"

"Doesn't sound like he wants help."

Ket bit the wing of one of the tengu, but its other wing turned into a fist and hit Ket's nose.
Surprised, Ket got another kick from the tengu's feet.
Ket was thrown hard toward Heinrich.

"I thought that 'angel' body of yours," said Hendrik, making a pair of quotation marks with his hands, "made you stronger? And you still got thrown off when a lion was thrown at you?"

Volants didn't have any body in the physical realm, but RFL Network was kind enough to simulate physical properties to ensure compatibility among realm.
Ket that shouldn't have any weight in the real world, was assigned with the weight of a lion in his lion form, and the weight of a man in his human form.
It was realistic enough, that for all practical purposes, it felt like volants actually exist physically.

"Momentum," said Heinrich, setting aside the lion body that slowly turned into the body of an unconscious naked asian man,
"is influenced by a body's velocity times its mass.
The lion, uh, now a man, was thrown to me at a velocity, and I was at rest.
Of course I'd absorb his momentum as well, and acquire velocity, no matter how strong I am."

Heinrich realized that Hendrik had been giggling the entire time, "oh, you're just messing with me."

"C'mon, it wouldn't hurt to smile," said Hendrik.

The naked asian man that was Ket stood up, "I'm sorry, the tengu is unforgiving.
Oh, I'm Ket, by the way, who are you-oh, you're twins?"

"Focus on your fight first," said Heinrich.

"Right," Ket said, his body transformed into a lion, and he flew toward the tengus.

Xiangyu finished its dance, and the blue flame spread to form a thin layer of protection between the humans and the fighting creatures.

"It is our cue, get into position," said Hendrik.

Heinrich ran toward the other corner, and with a brief dance, he produced rings of fire around his body.
The ring of fire produced fiery tendrils that connect to the fire veil Xiangyu created.
Hendrik waved his hands, and two of his shadows flew through the veil, and blocked any of the tengus from touching the fire veil.

A number of men in tuxedo and fedora hats approached Hendrik.
Derictor recognized the tuxedo, it was just like the man in tux he encountered at the silent concert.
They prepared a stance so similar to the fighting stance of the man in tux.

Kicks and fists were jolted toward the veil, and invisible forces started to wrap the veil upwards, sealing the fight of the tengus and the barongs from the human spectators.
Fiery tendrils from Heinrich wrapped around the closing veil, tightening it.
The crowd gave thunderous applause to the scene.

"They thought it was a part of the show," said Ezekiel to Hendrik.

"Exactly like what we want them to think.
So we don't have to bother with fixing their memories."

The fire veil wrapped in fiery tendrils, were shrunk by the invisible force fields from the men in tux.
It shrunk even more, to be about the size of the open gate to the *Antarabhava*.
Heinrich pulled the tendrils so the veil moved toward the gate, when a tengu slashed open the veil.

Two, three, four tengus escaped the veil.
Bangkal breached through the veil to chase the tengus.
One of Hendrik's shadows seeped out of the veil, transformed into a huge cloud with tendrils to chase and capture the tengus.

"Containment breach!" Said one of the men in tux.

More and more tengus escaped the veil, and the veil, along with the tendrils, dissipated.
Heinrich looked at Xiangyu, and to Hendrik.
They nodded, and Xiangyu prepared another dance.

Hendrik had his last shadow engulf him, forming a black uniform.
He jumped to capture one of the tengu.
The tengu pulled his longsword and attempted a stab to Hendrik's body.
It got deflected by the black uniform, that extended black tendrils, spreading on the tengu's body.
The tengu fell, unable to move as the black tendrils tied it tight, while Hendrik jumped to another tengu.

Heinrich had jets of fire from his feet and hands, propelling him toward the escaping tengus.
A fire beam from one of his hands burned the wings of one of the tengu, while the other evaded it.
A black tendril from one of Hendrik's shadows pulled the evading tengu, then Heinrich blasted its head with a jet from his feet.

Some tengus landed near Ezekiel and his men, thrusting its longsword toward Ezekiel's chest.
With a shout, Ezekiel pushed the blade aside, away from his chest.
He rotated midair and landed a kick toward its beaks.
Before the kick reached its beaks, a strong pressure pushed the tengu away from Ezekiel's feet.

His men jumped forward, forming a stance and launched their fists toward the tengu.
It was thrown hard toward the stage, where the frightened dancers and singers were still performing.
It was near the refrain, and the singer happened to scream as a part of the song, and in horror of the incoming tengu body.

Macan appeared in front of them, and he jumped toward the tengu body.
They disintegrated into dusts afterward, just about the time the scream subsided.
The crowd cheered enthusiastically, they jumped around, screaming the sound deep from their throat.

Gunshots could be heard, as a crowd of tengu approached Ernie, Nurhayati, and Anderson.
They slashed their swords, where Anderson blocked their swords, with a sword made of fabric-like material off his suit.
It was like a dance, but Anderson advanced several thrusts and slashing moves toward the tengus, fighting up to four tengus at the same time.

Ernie screamed, and hid behind Nurhayati, as another tengu approached them with a longsword.
Nurhayati jumped toward one of the tengu and disarmed its sword with a slap toward its hand, a rotating move, and another slap to its beaks.
The tengu receded while protecting its beak.
Nurhayati took the sword, but it dissipated into dust as soon as she pulled it.

The sword materialized back at the tengu's hand, and it was about to slay Nurhayati and Ernie, when Bright shot his electrolaser gun toward the tengu.
It went straight through the tengu and toward Nurhayati, that was electrocuted and fell to the floor.
The tengu looked at Bright, and it ran toward him.

He ran away, when the tengu flew and landed in front of him.
The tengu opened its beak wide, and was about to peck on Bright's face, a flaming bear pushed it down.
The flame spread to the body of the tengu, burning its feathers, as it screamed and turned into dusts.
The flaming bear looked at Bright, then turned away toward the next tengu it targeted.

Nicolas shot several rounds of his railgun, but the bullets just went through the incoming tengus as if they're made of clouds.
They landed in the middle of the crowd, that spectated them in excitement.
The tengu was about to slash its longsword toward the spectating crowd, when Michael Guntur stood in front of one of the tengu.

"*Ik-kor kakta-tol-Kav-ros kalram,*"[^invokekav] he shouted, a hand of him raised toward one of the tengu, "*Ik-ros solit-nah!*"[^dontbotherme]

[^invokekav]: I invoke the name of Kav
[^dontbotherme]: Do not bother me!

A silent blast could be felt in that room.
The tengus and barongs turned into dust, vacuumed toward the gate to Antarabhava.
The gate collapsed afterwards.
The flame of the bear was gone, and so were the jet streams of Heinrich, he fell to the ground.
Hendrik's shadows were gone as well, along with his black uniform.

Derictor collapsed to the floor, the pressure that was on his body was gone, he could move again.
He rushed toward Nurhayati, that was tended by Ernie and Anderson.

"She," said Ernie, sobbing, "she saved my life."

Derictor had Nurhayati's pulse checked, then her breath.
"Nur," Derictor shook her body, "Nur, are you okay?"

"She'd be fine, she was just electrocuted," said Anderson.

Everyone in the crowd started to notice the men with fedora hats surrounding them, and a polar bear at the stage.
The performers forgot to breathe, they stood still.
Heinrich jumped at the stage, he shouted, "and that's how you rock!"

The crowds cheered in ecstasy, they gave all of their strength to clap and shout at the concert.
Heinrich nodded to the performers, who waved back at the spectators.
He left the stage, and walked to Xiangyu.

"What happened?"

Xiangyu roared, then another roar, but none was made into a voiceover.
With his paws, he touched his muzzle, and tried to speak again.
His roars were no longer translated into English.

Hendrik came toward Heinrich and Xiangyu, "I can not access my meta, my extrasensory was turned off as well."

Ezekiel came with his men, "same here, we can't perform tier two abilities or more."

"Wasn't that M.G.?" Asked one of Ezekiel's men, "I saw him shouting something, in the middle of the crowd."

"M.G.," said Hendrik, "the chief of ORCA?
He was here?"

"I don't know," said Ezekiel.

"That’s the least of our concern, we need to see the Major at the VIP section," said Heinrich.

They nodded, and walked toward the nearest exit.

"Hey!" Derictor shouted, he ran toward them, and Bright followed.

"Where are you going, Delapan?"
Inquired Kang Hae-In.

"I have to reach them," Derictor said, panting, "hh-hh-we have to know what this is."
