# The Last Nightmare

Around this moment, David meets with Anthony and Michelle at their house.
It appears the family brighten, as they've been reunited, and David is a healthy and decent specimen for Michelle's mate, despite them not dating at the moment.
Besides, David is the son of Martha's boss, she is happy to have him here.
They were supposedly hanging out.
Anthony happens to explain a story about his familiars, and his profession as a necromancer.
Michelle asked him what about her father, that is also his father, and he said that he has no idea who his father was, so he couldn't track him.
Michelle went out of her room for a while, and returned with a photograph, an old one, and weathered significantly.
In it, was shown the picture of Anthony as a baby, Martha, and their father.
Anthony focused on it, and he glimpsed to hell.
He found his father there, and he saw the scene, sequences similar to his dreams: his father wanted to leave him in the middle of the woods, memories of his father when attacked by Aurelia when he hit Martha and Anthony in their old bathroom.
It appears, that he dreamed of that because he accidentally connected to his father in hell before.
And it triggers his actual memory of that day.
It also confirms that his father is in hell.
But then, Anthony saw yet another face he recognize, David's mom.
She was in hell as well.
David asked what about her, and Anthony saw a man in her torture chamber.
No, he said, there were two men, one is Steven, the other is, Fernando.
Apparently, Anthony said, David's mom had an unrequited, unresolved feelings toward Fernando, while still having a relationship with his father.
