# The Lost Son

> The *Etonet* is a global system of interconnected networks with a shared protocol to communicate between etoan computing substrates on Earth.
It is essentially etoan analog for The Internet, although it is separate, independent, and vaster.
It is run with the help of RFL Network as its backend, according to the agreement between The Divine Council of Earth and the Etoan Earth Colonization Initiative.

> The technology used by RFL, however, is slightly different from typical infrastructures found by civilized star systems in the celestial neighborhood of Sol.
It is out of date by about twenty thousand years.
However outdated the system is, it is more than sufficient to supply basic needs of etoans on Earth.

> Etoan on Earth heavily utilizes this network to connect with their technologies, and each other.
They do so in a far more intimate manner than a human's dependency with their smartphones.
The network is a real part of them and their everyday life, and is also the reason why humans can't utilize stray etoan tech, ranging from their attires, fabricators, tablets, and even their furniture and houses.

> Detaching themselves from their tech, is the same as dropping a civilized human into the wilderness, naked, vulnerable, and isolated.

## Just like a retirement

"So I reviewed your options," said Alex, while his eyes jolted up and down, left and right, under his closed eyelids, "if you want to resign from your position as an ambassador, you would have to dispose of your current body."

"Yes, that would not be a problem" said Cal, "I need to switch to a human body anyway."

"Why don't you just use your normal etoan body?" Alex opened his eyes, his gaze was set to Cal.

"I said that I want an immersive human experience, something that I wouldn't get when I could naturally listen to radio frequency, or perceive near infrared to ultraviolet."

"But it means you'd alienate yourself from etoan tech, *and society*."

"That wouldn't be a problem.
I lived here alone without etoan tech for a good twenty years."

"When?"

"Fifteen thousand years ago."

Alex rolled his eyes while closing his eyelids.
His thought queried for available options through the etonet.
His eyes opened, his hand scratched his head, "we can't print a human body, apparently."

"What?"

"The Divine Council of Earth owns the proprietary rights of *Homo sapiens*-specific genes.
We have to order it directly from them."

Cal nodded.

Alex stared at Cal for some time, his eyelids were closing again.
His thought wandered through the etonet, reaching out for his twin brother Steven.
Steven was surprised, a series of thought exchanges were established.
An approval was delivered to Alex's thoughts.
Alex opened his eyes to find Cal's inquisitive gaze.

"Steven is ready to perform the procedure," said Alex, "but, to be clear, you do realize that, except for special rights inherent to your ECI citizenship, you'd essentially be just like them, right? You could get hurt, sick, and even die."

Cal nodded.

"Like, for real, when you die, you would only be resurrected with your copy *prior* to your transfer to your human body."

He nodded again.

"In case you didn't get it, the human body, at their pristine state, is incompatible with our backup technology.
The only compatible backup method is the RFL's SOUL, even then you're not technically citizens of Earth.
You'd be excluded from SOUL, when you die, you die," Alex let some time for his words to sink in.

"I'm gonna lose you, *that you, grandpa* forever," Alex paused again, "... you know?"

Cal smiled. "I understand, grandson."

"But, why then?"

"I've lived fifteen thousand years.
I've died many times already, I stopped counting after my third."

Cal moved closer to Alex.

"I've had my life.
A long one indeed."

Shiver returned to Alex's spine, as he found Ostaupixtrilis Pontirijaris behind the young gaze of Cal.
At that point, Ostaupixtrilis Pontirijaris gave a different kind of gaze.
It was no longer intimidating, more like the gaze of a tired old soul after an eon long journey, longing for peace and tranquility.

"Death, is just like a retirement for me," Cal advanced closer, "don't you think?"

Cal closed the gap between their foreheads, consoling Alex.
Alex wiped his tears, his thought reengaged with Steven's.
A meeting was scheduled, and initiated.

Within a single blink, a wall appeared beside them, in the middle of the Aucafidian landscape.
It paced backwards, leaving a trail of floor, walls, and ceilings.
Before long, a sterile clinic room was molded on its inner side.

A figure was also molded inside.
A figure with Alex's face, except with a leaner build.
Instead of a tidy, formal suit Alex wore, the figure was decorated with a light blue medical gown.

It took no time for them to recognize the figure after it was fully rendered.

"Grandpa," said the figure.

"Hi Steven, long time no see," said Cal.

## A quieter world

> Telepresence is one of many ways etoan communicate remotely with one another.
In simple terms, telepresence is like video calling on steroids.
Audio and visual stimuli are not the only thing transferred in between callers.
Entire sceneries and objects on either side of the call, be it in audio, visual, tactile, olfactory, or kinesthetic information, could be recreated with a high level of accuracy.

> At the lowest level, such recreation is not very resource intensive.
Audio and visual stimuli could directly be beamed to the eyes and ears of the user.
Olfactory stimuli could be recreated by directly stimulating the olfactory bulb.
Tactile stimuli could be delivered via interaction of utility fog directly with the skin surface.
Kinesthetic senses could be fooled by setting an absurdly advanced treadmill that translated every locomotive movements into the virtual recreation of the scenery.

> The level of recreation may be as simple as projecting only the caller or the callee right in front of the user,
or by recreating sceneries of one party, both, neither, or even in an entirely virtual realm of their choosing.

Steven was dissolving a single teaspoon of brown sugar to a hot cup of black tea, just like what Daniel ordered.
He wondered whether or not Daniel had finished *grooming* Steven's car.
The thought brought him a chuckle, as he couldn't believe that his car, Adran, preferred Daniel over him, when it comes to grooming.
*They must be having a lot of fun,* Steven thought to himself, only to discover that all of the sugar grains had dissolved.

He was going to pour milk into the cup when a notification beamed to him.
He looked at his tablet, right beside a dispenser.
With his eyelids on, he learned that his twin brother, Alex, initiated a thought exchange request.

He accepted, and the exchanges made his brows twitch.

"Grandpa Ostau wants a human body made in his image?" Murmured Steven.
He strode to his room, while still exchanging details with Alex's thought.

His room was an empty white room, like it always was when unused.
His biological radio frequency transceivers near his *temporal lobes* were beaming in various bandwidths,
orchestrating the utility fog in the room to mold into an operating theater.

Steven closed his eyelids, his mind navigated through the catalog of templates.
He selected a template for a human, and a request order was made to the Divine Council of Earth.
Within microseconds, the request was accepted, while Steven was pulling up medical records of Ostaupixtrilis Pontirijaris.
He ordered his tablet to translate features on the records to the human template.
The order was completed within microseconds, and his tablet uploaded the adjusted template to the room.

The room was initializing the rendering of the body, while Steven got another request from Alex.
A telepresence request.

Steven accepted, and one of the walls in his room appeared to decay into an open scenery.
An Aucafidian scenery.
His brother Alex was in the scenery, and beside him was a figure he recognized.

"Grandpa," said Steven.

"Hi Steven, long time no see."

Daniel was baffled as he entered the clinic.
He had finished grooming Adran, yet Steven wasn't there to deliver his cup of tea.
Daniel wondered if he finished too early.
He paced to the kitchen, to find that it was empty.
His cup of tea was left unattended.

Steven's tablet was right beside the dispenser.
It took Daniel a couple of seconds to decide whether he'd sip his cup of tea first, or to grab Steven's tablet and deliver it to Steven.
He marched to Steven's room with a tablet in his grasp.

Daniel entered Steven's room, and the first word he said was: "shit."

Daniel was no stranger to etoan tech.
After befriending *a half etoan* for more than two decades, they didn't seem that weird to him anymore.
Other than the fact that etoan stopped aging in their twenties, any etoan could be found decorated with origami-like shapeshifting suits, a tablet with an intellect equivalent to several hundreds of human brains, and a room that could mold to accommodate *literally* anything the user would ever need.

It was the fact that etoan tech could do something like video calling on steroids, never crossed his mind.
He was baffled at the realization of how he could miss such trivial usage.
Other than that, he was fascinated to be reminded that etoans were not that different from modern humans.
They would also also do *video calls* with one another from time to time.

His fascination had to be paused as his gaze stopped at Cal.
Cal looked very familiar to Daniel.
If his hair were white instead of black, Daniel would instantly recognize him.

"Grandpa Ostau?" Said Daniel.

"Daniel," said Cal.

"Is Nicolas also here?" Said Alex.

"No," said Daniel, "he's away, probably doing some intelligence agency business."

"Shall we perform the procedure now?" Said Cal, he treaded beside the hospital bed.

"Can I walk there?" Asked Daniel.

"Yes," said Steven.

Daniel ran to the alien forest, chasing a pack of feral bat-dogs.
His smile was like that of a ten years old boy running through a park.
The smile attracted a smirk on Alex's face that stroded toward Daniel.
In no time, two adults were running in an alien landscape, while another one was accompanying a boy in a clinic room.

"So what should I do? Just lie here?" Cal looked at the hospital bed.

"No, Grandpa.
This bed is not for you," said Steven.

An outline of a human body, identical to Cal's body, appeared on the bed.
At first, it was translucent, but it thickened over time.
Cal and Steven looked at the body, inspecting every square centimeter of it.

"There will be some notable differences," said Steven, "when you migrate to this body."

Cal noticed that the ears were round without any pointed tip, like a normal pair of human ears would.
The skin color was more nuanced in reddish beige, compared to his current light beige with soft and subtle black marks under his skin.
Its skin was much darker in the ultraviolet spectrum, as melanin absorbs ultraviolet light.
He examined his skin, which was much brighter, as zinc oxide pigments on his skin reflected ultraviolet light.
The irises on its eyes were brown, and its gums were a brighter tone of red, due to the lack of tightly packed myoglobin suspension present in etoan flesh.

"The first thing you'd notice is reduced acuity of color perception.
Most male humans are *trichromats*, meaning they have three different kinds of cone cells that are sensitive to the red, green, and blue spectrums.
It was a major reduction to etoan, which are typically *pentachromatic*.
You would no longer be able to perceive near infrared and near ultraviolet light."

"That I know," said Cal.

"The next one would be the lack of radio-frequency hearing, and most importantly, our rudimentary radio detection and ranging sense."

"A quieter world," said Cal.

"Seems like you've done your homework," Steven put his hands on his hips, he exhaled, "I still don't understand, why would you choose to *torture* yourself by being a full human?"

"It's my kind of vacation," Cal ran his fingers across his human body on the hospital bed, "my dream."

"That's a very peculiar dream."

Cal smiled.

"You're only thirty four years old, as a half etoan" said Cal, "and you already concluded that there isn't any worth to be a human?"

"Well, I'm a *half human* etoan," said Steven, "half human, but still an etoan."

"Well yes could do too," Cal giggled, "it's your entire life.
Everything you have ever been since your birth.
I was just like you."

"I've spent countless amounts of time as various beings: as an ordinary etoan or even an enhanced etoan, as a virtual intelligence, as various kinds of beasts, aliens, machine life," continued Cal.

"Then is there a need for you to be a human?"

Cal's smile lingered around his face for a little longer.
"It was only at this rare opportunity, that the chance to be a human is present," Cal inspected the human body's face closely, "I might not get a chance to do it again."

"Why would you not get this chance again?
You could do it at any time at your convenience."

"Because we're in the middle of a war.
I don't know what will become of humanity after this war."

"A..., war? Where?"

"I don't know," Cal's gaze turned uncertain, "but we're fighting it."

"You don't know?
I don't even know that we're fighting anything right now."

"The fact is, we etoans are here on Earth,"
Cal's hands were drawn away from his human body,
"our presence here serves a certain purpose, a leverage.
We're chess pieces in this war."

"So what role do we serve here?" Steven giggled, "Bishops, knights, rooks?"

"Too much question," Cal looked at the ceilings, bursted in laughter.
"I don't know my role yet, Steven."

"Or you could just be a pawn," Steven let out a grin.

Cal returned a grin, Steven was surprised.
"But I know you're a pawn, grandson."

"The weakest piece," said Steven.

"The trump card in a chess," said Cal, "often overlooked, it could turn into any other pieces when it successfully penetrated the enemy's defense."

## An information processing unit

"What are they doing?" said Daniel, while struggling to sit atop a rock next to the waterfall.

"Preparing a virtual vessel before a human vessel finishes printing," said Alex. Balancing himself firmly on another piece of rock was effortless for him. His hands shaded his eagle gaze toward the faraway clinic room.

"Printing? Virtual vessel? Care to explain? I'm merely a human."

"Assembling, constructing, or even conjuring, whatever you want.
Basically we're manufacturing a human body, atom by atom, molecule by molecule, according to the specifications of Cal's features.
Cal's body itself must be disposed of first, so he had to transfer from that body to a virtual vessel before transporting it to the assembled human body."

"You made switching a body sound like copying data from one computer to the other."

"Because that's what our body is, an information processing unit, a computer."

Daniel nodded, his feet slipped, and he fell forward.
He caught himself screaming, but his face didn't hit the ground when it should.
It was after some moment that he realized Alex's firm arm was wrapped around his chest.
They were hanging from the tip of a cliff's edge.

For some moment, only the noise of the waterfall could be heard.
Daniel decided to break the silence, "so, uh, how do we get down?"

Alex giggled.

"I'm serious, it-it's scary up here," Daniel stuttered, "and y-you don't seem to be doing anything about it."

"We'd almost certainly be dead if I let go, wouldn't we?" Alex's giggle grew to a laughter.

"It's not funny," Daniel's heart raced.
It wanted to race itself out of Daniel's chest, saving itself from the fall,
leaving Daniel to dangle out at the mercy of Alex's grip,
"get us up now!"

"You know what," said Alex, he got Daniel's attention, "I'd let go."

"Wha-"

There was a scream, then a laughter, followed by silence.

"It does look real," Alex failed to suppress his giggle, "but you must remind yourself that it isn't."

Like a kitten whose nape was held by a pinch, Daniel was hugging Alex, motionless.
His limbs wrapped tightly around Alex, in the hope that Alex's body would suffer first from the fall before his.
However, it didn't happen.
Alex was standing firm.

The ground appeared to be far beneath them.
The cliff's edge was some distance above them.
Nothing was seen to support them.
Alex was erect midair.

"Oh right," said Daniel, "I'm still in Steven's room, am I not?"

"And I am still at my office," said Alex, he could no longer contain his laughter.

## Ready to be disposed

> *Somnambulism*, *noctambulism*, or sleepwalking, is a sleep disorder that occurs during slow wave sleep stage, in a stage of low consciousness, while performing activities that are usually performed in a state of full consciousness. Although their eyes are open, their expression is dim and glazed over.[@WPSleepwalking]

Ostaupixtrilis Pontirijaris rose from the hospital bed.
The first thing he noticed was that his ears felt much lighter.
They were also numb compared to his old ears.
The world was also much calmer, with the absence of radio frequency noise inherent to humankind's technological civilization.

To pull his legs to the floor, however, requires more effort than what he expected to be.

"I am experiencing muscle weakness, is that normal?"

"Your previous body was adapted to a higher gravity, about one and a quarter times higher than that on Earth," Steven led Cal's empty body to a chair.

From the hospital bed, Ostaupixtrilis Pontirijaris's gaze met Cal's gaze.
Its eyes had that empty gaze, like it was experiencing *somnambulism*.

"I'll never get used to that gaze.
Every time I change bodies, it always gives me goosebumps.
Look, I am having it now," Ostaupixtrilis Pontirijaris pointed at his arm.

Daniel strided back from the forest into the clinic.
Clinging on Daniel's side was Alex, his hands wrapped around Daniel's neck.

"Oy, Steve, you should've seen his face when I dropped ourselves from that cliff," said Alex.

Daniel tried to break himself free from Alex's grip.
He failed against Alex's immovable posture.

"What are you made of, stone?" protested Daniel, attempting to wriggle himself free.

Alex let go of his grip.
Daniel, who wasn't expecting it at all, failed to balance himself.
The next thing he realized was that he sat on the ground.

"I kinda forgot that you're no stronger than my bro," said Steven.

"Is he the strongest in your group?" Alex scoffed.

"It's not fair, you're an etoan," said Daniel.

"No, I'm *human*, with etoan descent," said Alex with a shrug.

Daniel sat on a chair beside Cal's empty body.
His curiosity grew as he examined the body, and its face.
His gaze turned to Ostaupixtrilis Pontirijaris that stood beside the hospital bed.
He looked back at Cal.

"What happened to, uh, *this* body?" said Daniel.

"Ready to be disposed of," said Alex.

"Disposed? He's still a person, isn't he?"

"After its brain was scanned out and tunneled to *this*" Steven pointed at Ostaupixtrilis Pontirijaris, "I scrambled its neural pathways.
Its brain is now like a lego model that had been disassembled and reassembled in a random order.
It is no longer capable of forming consciousness."

Daniel looked at Ostaupixtrilis Pontirijaris, "so grandpa is in that body now?"

Ostaupixtrilis Pontirijaris nodded.

"Your brain and major neural tissues have finished printing, but we still need some time for the rest of major organs and musculoskeletal tissues to be assembled," said Steven, "you can start to familiarize with your new body for now."

"How is it not done printing? He's there already," Daniel approached and wrapped his hands around Ostaupixtrilis Pontirijaris's neck, "here, blood and flesh, isn't he?"

"It is still a virtual projection of his body," said Alex, "it is about as real as *that* cliff."

Daniel shivered as his thought recalled the fall.
He decided to turn his attention to Grandpa Ostau, in flesh and blood, right beside him.
His hands swiped at the surface of grandpa's body.

The warmth of his skin, the subtle beats of his pulse, and the movements of his ribcage as he breathes.
It was too real, that Daniel felt like it would be a lie to say that it wasn't real.
Just like it was a lie if Daniel insisted that it was real.

"Right now, his body is no more than just neural tissues," said Steven, "covered with utility fog that are printing and assembling remaining tissues."

Daniel's gaze scanned the body of Ostaupixtrilis Pontirijaris.
Daniel nodded in agreement, with a frown.
His gaze turned to Cal.

"Then," Daniel said, "how would you dispose of this?"

## The most dangerous parkour

Daniel discovered a forgotten cup of black tea with brown sugar in the kitchen.
The one he abandoned earlier that morning.
What was then a warm cup of black tea, became a cold cup of boiled *Camellia sinensis* leaves.

*It had been made,* he thought.
*It had to be consumed.*

He heaved a breath, as he strided with the cup on his hand, pacing toward the terrace.

The sky reddened as the sun approached the horizon.
The welcoming clouds were bathed in golden glow.
The atmosphere was in contrast to Doctor Nicolas' sky blue hem.

"Oh, Nico, you are very late," greeted Daniel.

Nicolas' hair was a mess.
His hem was wrinkled here and there, partially wetted with sweat.
Daniel put the cup to the table, abandoning it for the second time in a day.
He approached Nicolas.

"Where have you been?" Inquired Daniel.

"Did you," Nicolas caught up his breaths, "did you make some weird requests to Alex?"

"I, um," Daniel muttered, "I don't think I did."

He wasn't sure if he could tell Nicolas about their recent encounter with Alex earlier.
Nicolas was still panting, he seemed to run out of thoughts.

"So," said Daniel, "what happened? What did Alex do?"

There was some moment of silence as their gaze met one another.
Daniel's curious gaze versus Nicolas's tired gaze.

"Alex told me and your cousin to escort this one kid at his office," said Nicolas.

"So you were with Derictor," said Daniel.

"Yes.
So we escorted this kid to an abandoned building, as specified by Hendrik,"
Nicolas was heaving more air to his lungs,
"Uh, Hendrik is Alex's associate."

Daniel's gaze was still inquisitive.
Nicolas decided that Daniel didn't need to know about Hendrik.
His thoughts scoured his memories to find where the conversation left.

"So, I thought we were supposed to protect him.
Perhaps he's something important to the goal of the Intelligence Agency."
Nicolas paused,
"I thought Derictor was briefed about it too."

"Did he?"

"No, he didn't, so we argued for some time about the kid's role.
It was not even a long fight.
Just a blink of an eye, and the kid was gone."

"Gone?"

"Like, we literally have no idea where he went.
We searched around the area, almost frantically.
Believe me, you can't keep your cool face when you lose to a kid.
Then *Rick* called me, he told me to open a link.
A live stream."

"Of?"

Nicolas searched his pockets, "it was this, uh, wait."
He couldn't find whatever he was looking for.
Nicolas darted to his car, and pulled a phone.
It was handed to Daniel, and a video was playing.

"Hi guys, today, I'm gonna show you the most dangerous parkour feat in this city, *yet*."

Daniel recognized this voice.
He gleaned on the face of that boy.
The boy he just met, with pointed ears.

"Oh *damn,*" said Daniel.

"Nothing happened *yet,* and you're cursing already," said Nicolas, "wait until the end."

Cal's body set the camera on some sort of support.
Most probably the camera stood somewhere beside a wall.
It appeared to be the top of a high rise building.
No other roof tops were visible.

He removed his t-shirt, leaving only his sweatpants and sport shoes on.
To the edge of a concrete fence he jumped.

"This is take one of the High Rise Parkour Feat by Christopher A.L., let's start."

He lightly jumped around to warm up.
A step forward, he started to run.
A foot missed its target and he slipped.

Following it was an audible "whoops," and he was gone from the screen.

"What happened?" Said Daniel, his eyebrows squinted.

"That freaking kid, fell atop of my car, lifeless.
Blood splattered around, his joints bent in anything but the correct positions."

Daniel didn't say anything.

"Rick called Alex, to inform him that the boy he told us to escort was dead already.
While I was inspecting the body, stupidly hoping that the boy might survive the impact."

"What did Alex say?"

"We just need to bring that kid there, in that building.
He didn't know that the kid would jump off the building."

"..., and the kid, did he survive it?"

"It was a nine-storied building.
Other than the fact that the kid got to the top in less than five minutes,
it is not a surprise that a fall that high would make you leave your body real quick."

*And that same kid is still in this clinic,* thought Daniel to himself.
The next thing Daniel heard was Nicolas's scream.
It was another surprise of the day, to hear Nicolas screaming like he was a kid, *again.*
Nicolas's first scream out of his lungs since the night they spent at the morgue during their med school.
It was nearly two decades prior to that day.

"Don't worry much,
it's just a bunch of bionanobots implanted at the *basal ganglia*,
controlling that lifeless body of mine,"
said Ostaupixtrilis Pontirijaris,
"The brain of that body was no longer functional anyway."

"You could wear proper clothes first, Grandpa," said Steven, with a pair of new clean clothes on his hands.

Nicolas was still screaming, he wasn't sure what to do anyway.

"Oh, so this body is done printing?"
Said Ostaupixtrilis Pontirijaris.
He reached for the clothes and wore them,
"this shirt looks a little whiter than I remember."

"You can no longer see beyond the human visible light spectrum," said Steven,
"bleaches broke down the chromatophores of stain pigments so it reflected more toward the *UV spectrum.*
To the naked eyes of a human, the fabric just turned whiter."

Nicolas thought that facing the Tengus alongside a fiery white bear and a bunch of mages three years ago was as weird as his life could get.
It did make the fact that they had Anderson the shapeshifter in their team as ordinary.
To observe a Lazarus with his own eyes,
both the kid's death and the resurrected body,
was something that never occurred in his imagination.
That made the Tengus Incursion incident *an ordinary event.*

"I didn't realize that you stopped screaming,"
said Daniel to Nicolas,
"since when?"

"What is that?" Inquired Nicolas, "who is he?"

"I gotta go," said Ostaupixtrilis Pontirijaris.

"Okay, see you around, grandpa," said Steven.

Daniel just waved his hand to Ostaupixtrilis Pontirijaris.
Ostaupixtrilis Pontirijaris returned the wave with a bright smile, and walked away.
Nicolas found himself waving his hand too.

"No one is going to answer my question?" Inquired Nicolas.

"He is a relative to Steven," said Daniel.

Steven nodded, and left them for his room.

That afternoon, the cup of tea was no longer in Daniel's thoughts.
The cup of tea spent its night all alone by the corner of a terrace table.
Bugs feasted on the content, along with some local fauna.
It left only a spoiled dirty cup of tea by the morning.

