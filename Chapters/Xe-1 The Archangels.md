# The Archangels.

18 August 2022, 14:05 WITA, a day after 77th Indonesian Independence Day Ceremony.
A very deep, trembling sirens could be heard miles away, coming from the sea.
A very large fleshy island emerged from beyond the bay, paddling its way to the bay entry.
Soon a greenish swarm emerges from its back.
It’s later evidently clear that it was a swarm of greenish dragons, six legs and two pair of wings.
They flew to the land.
From the ocean emerges a troop of six legged horse sized monsters, with their two slender, thorny manipulator limbs near their heads.
They traps people in sticky substances, almost like glue and/or hardened gums, that dried out the moment it reached the ground.
It’s almost impossible for men to break free from it.
Among them are few dinosaur-sized six legged armored creatures, they breathes fire.
Their branching thorny armor hosts some tens of eight legged climbers, almost like octopuses but with firmer tentacles, or legs.
They carry with them weapons, high velocity projectiles shot from them, and also long ranged version of glue-shots.
It’s almost a matter of time before Kendari’s bay area is almost entirely occupied with them.

Steven and Daniel could hear the roars of incoming dragons, and then they could hear a number of creatures swarmed their clinic’s yard.
It’s only a matter of time before one of the critter entered their clinic room.
Steven is well prepared, his weapons are ready.
It’s on his car.
Unfortunately they’re on the first floor.
So they have to go to the second floor first, away from the incoming critters.
Steven configured his phone to offensive mode, but it might not last long, as his phone has limited battery time, and it took the phone some time to charge between shots.
First room, a critter entered the infirmary, fortunately the wall is an OPA screen.
He set it to focus its output to the critter, with enough energy to pierce its legs.
They escaped, and now the phone is charged for five shots, and on the stairs they meet two more.
Daniel stabbed one of them in their mouth with his umbrella.
He was shocked as he just violated his hippocratic oath.
The other one got a shot from Steven’s phone, right on its eyes.
The creature fell from the stairs in pain.
Steven decided not to worry about his oath just now.
Steven almost dropped his phone, one of its side, the beaming side, is sufficiently hot it almost burn Steven’s finger from accidentally touching the beaming surface.
They reached the second floor and discovered that a pack of this critters on one side of it.
On the other side, the room with a large window in it, is where they want to go.
They charged to the room as the critters chase them.
Steven quickly grabbed the bag and Daniel tried to close the door, but he’s close on losing ground from the pressure exerted by the critters.
Steven opened the window, his car moved to just below the window and its roof folded clear, for them to jump in.
Steven instructed Daniel to leave the door and immediately jump.
Daniel almost couldn’t move, but he immediately jump, but not without first collide with Steven and accidentally made up Steven’s third kiss, by men (all of them are without Steven’s consent).
Both safely (and rather unpleasantly) land on the car.
The roof quickly fold itself close, and the car immediately accelerate out of the clinic’s perimeter, probably stomping some critters away.

Steven couldn’t believe it, today they were attacked by a large pack of eight limbed critters and the first wound he get was from Daniel’s teeth.
Daniel was equally shocked from the fact that it was his first time wounded by Steven’s teeth.
They laughed in relief, until the car asked them where should they go.
Inconveniently spread across almost equidistantly spaced points on the entire Kendari, are two of his sons’ schools, one is highschool of his oldest son, and another is jhs+elementary of his two sons, and his house, where his wife and his youngest son are.
Daniel gave his perhaps the most brilliant idea of the day: they’re all (except his wife) backed up already, so back me up as well and we fight these critters on our way to pick them all.
Steven agreed with his terms, and he injected Daniel with nanites, as his couch engulfed him, forming foamed corundumoid armor, with wings.
A brief introductory description is provided for Daniel as nanites explore his body and the suit scanned him, all while feeling the pain (slowly numbed down).
Steven’s couch also undergone similar transformation, completely engulf Steven.
They flew, Daniel rather sloppily at first, but some moment later he got a hang of it.

After fighting for some time, Steven declared that it’s just too many of them.
And then perhaps the second most brilliant idea of the day by Daniel is this: why don’t you let your car picked up your sons, it’s autonomous right?
Steven’s car then picked up his children, starting with the oldest.
