# Interrupted Approach

> In-story date is 20270405 1000-1040

## Chase
Heinrich, Xiangyu, Henokh, Anderson, some BAIK agents, and Oneiric Protocolers, were tracking whereabouts of Aditya and Co., currently thought to be doing something with the Black's Staff.
They walked by with concealment spells by Henokh and Xiangyu.

They discovered a lead, having Aditya seen somewhere in the street, heading to a mass transport.
When they decided to chase Aditya, a man killed himself, and due to shock, the spell broke, and they were seen by everyone.
Especially, Xiangyu.

## Interruption
Yam rarely manifest in the surface.
However when he did manifest in the surface world, he'd put something off the pattern.
This particular time, he timed his walk on the zebra cross close to the next green light, a car braked right after he walked in front of it, and he got a car behind him scratched his car.

The driver of that car is not in his best mood, today, and that accident ruined it.
On his workplace he complained all about the incident, and vent it out in one of the Junior.
That particular Junior had a fight with his parents last night when he comes out as a gay, and got scolded by his senior, stating that he's useless, and meaningless, that without him, nobody would miss him or anything.

Lacking the support he'd need during his lowest point on his life, the Junior decided that he got nothing more to lose.
He jumped from the top of the office building and hit a van of an esoteric researcher organization known as EPL, whose members were surprised and walked out of the car.
The EPL agents were even more surprised when he discovered a group of men with black uniform walked in a walkway left to the road, one of them wore black cowl, and had a tattoo on the right side of his face.
Another man of the group is a polar bear with uniform, walking upright.

The Reflexius spells that cause others to ignore oddities were broken due to the surprise of a man jumped off the building.
EPL members activated anti-reflexius field and called for support, detaining the group of men.
They failed to intrude to the place where Anthony and Aditya performed a ritual to summon Tiamat.

## EPL Agents
Keenan activated Reflexius Counter Measure, and so all reflexius capability within about ten meters around the car is nearly impossible.
Heinrich maintained a homeostasis within his volume, so he is not affected internally, but anything external to him, is beyond his control.
Xiangyu lost his ability to talk in human language.
Keenan and Lucia prepared their weapons, that are electrolaser and laser guns, quite a surprise for BAIK Agents.
Other than that, Keenan produced what appears to be a highly sophisticated walkie talkie with long antenna.
Keenan pushed a button and from the tip of the antenna, a chain of light manifested.
He snapped them to Heinrich, that tried to let it phase throught the cloud, but then the cloud dispersed around the chain.
He had lost 10% off his entire cloud that constitutes his being.

Heinrich recognized it directly that it is the EPL Reflexius Wand, that allows even an areflexi to perform reflexius.
Xiangyu ran farther away from the RCM range to conjure a stream of fire, toward the car, but Keenan snapped his legs and draw him closer to Keenan.

Lucia shot aluminium plates off her gun and Heinrich managed to trap the projectiles inside his body.
Heinrich discovered that he can control laminar flow inside his body, so he can accelerate projectiles to great speed within his body before ejecting it.
However he is limited to his energy reserve (worth only so much that the nanites inside him can sustain), and his lack of accuracy.

Then Xiangyu managed to rip off the RCM emitters off the car by physical means, just as the back up EPL agents arrived at the scene.
At split second, when he could gain his reflexius back, Heinrich destroyed all of their cars and transported them high to the roof of a skyscrapper.

They lost Aditya, and had no means to track them.
Xiangyu explained that EPL is on a cold war with WTF.
