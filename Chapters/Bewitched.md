# Bewitched

Ostaupixtrilis Pontirijaris, Adran, and Leaf were a new batch of questers to join the adventure on the surface of Stratos.
It was their first time meeting one another.
It was also the time they bonded in a relationship that lasts many millenia after.

Earth was an oddball in space with life so magnificent it was determined that Aucafidian life came from it.
Aucafidus was an oddball in space with life so magnificent it appeared just as recent as a quarter million years ago.
Because of that, Earth was coveted by many sovereign powers in the Aucafidian Sphere.

However, about fifteen thousand years ago, a rogue godling claimed Earth to be its sovereign, and pledged to take care of Earth the best it could.
The godling that assumed the role as Earth's caretaker was known to build and experiment with various habitable worlds within its region, producing extravagantly beautiful gardenworlds each.
All becomes famous tourist attractions, and among mortals, its works were praised with high regards.

It was expected to do the same to Earth, however it shut Earth off from the remaining of the civilized galaxy.
No visa was ever granted since some etoan tourists decided to play god and interfere directly with the affairs of humans.
It forever changes the course of human history, as shortly afterwards the human era begins.
Göbekli Tepe was built.

For all of the early history of humankind, they considered themselves to be a part of nature, they're just another animal, and other animals must be treated with respect.
It was the age where animism was prevalent and wasn't considered cultist movements.
For some human enclaves, those are the norm.

Until an Etoan decided to shot a stray animal that would be known as *Bison bonasus caucasicus* with his electrolaser gun.
Centuries later, it was observed that some local human enclaves started to worship a humanoid figure depicted with a club that could shot lightning.
Another set of centuries passed by, and Göbekli Tepe was built, first depicting a statue of a faceless human-like being as their God.

Stratos was furious, and ordered a full ban for any visitors to Earth.
And Ostaupixtrilis Pontirijaris hated that random stupid etoan for causing the entire ban to Earth.
Ostaupixtrilis Pontirijaris had been yearning to visit Earth and was in a long wait list to receive Earth's visa.
He was.

News had it that they got another chance to visit a recreated version of Earth on Stratos.
Ostaupixtrilis Pontirijaris decided that it was his shot to at least experience recreated version of Earth, as visiting the real Earth was not a possibility.
Ostaupixtrilis Pontirijaris was dissapointed at first, as the recreated Earth was not spherical, but cylindrical.
It wasn't even a rotating space habitat, it was a dynamically suported Supramundane Strip.

"Are you serious?" asked a pitch black humanoid figure sitting right in front of Ostaupixtrilis Pontirijaris.

"What?" responded Ostaupixtrilis Pontirijaris.

The pitch black humanoid figure slurped a thick and viscuous black juice.
It introduced itself as Adran earlier.

"Which part of 'recreated' did you not get?" asked Adran.

"No, I mean, I understand it completely," said Ostaupixtrilis Pontirijaris.

"Then why are you complaining?"

Adran's last sentence struck Ostaupixtrilis Pontirijaris hard.
Ostaupixtrilis Pontirijaris said nothing, his gaze turned to the mess hall, and to the ceilings.
On the transparent ceilings Ostaupixtrilis Pontirijaris observed four giant tree-like structures with plenty of small rings as their leaves.

"It was just that, a complaint."

The rings were magnetic traps, exerting forces to the infinitesimally point like warp drives.
The drives were actually bigger than its exterior size, which was practically pointlike because the entire engine is warped inside a pocket space-time bubble.
Because all of the mass-energy were contained inside an exterior that appears to be practically pointlike, for all intent and purposes, each drive behaves like a single smart particle.

Millions of such drives littered and coupled with the 'harness' of this ship, coupled with the harness via magnetic force.
To be coupled with a ring-like harness, the drives disturbs the balance of its magnetic field lines, essentially behaves like millions of tiny magnetlets that could move freely, and essentially reactionless.
When strapped with the harness, the entire ship could move reactionlessly.
It was like the entire ship is a giant space cart pulled by millions of invisible horses.

Ostaupixtrilis Pontirijaris had that line of thoughts disturbed by a slurp of Adran's black goo bowl.

"Aren't you going to eat your food? Can I get those?"

Ostaupixtrilis Pontirijaris had his gaze fixed at Adran, then to its bowl, then to Adran again, "can you digest it?"

"I can, although it is not as efficient as slurping this black goo."

"Then why would you want to eat my meal?"

"New experience. I was just been activated en-route. In fact, this is my very first time living my life."

Ostaupixtrilis Pontirijaris set aside some slices of meat for Adran, and Adran picked them, savoring all of them as if it had been through centuries of famine.

It smooched its fingers one by one.
It was weird to observe all of its demeanor, because to Ostaupixtrilis Pontirijaris, it was like seeing a shadow of someone eating, only that this one shadow was very vivid, and the demarcation of its blackness and the surrounding scenery was crystal clear.

"So you're not here by choice. Who put you here?"

"Yes, I was brought here while my mind was incubated," there was a pause.

A set of low pitched grumbles and pungent odors came to Ostaupixrilis Pontirijaris' own senses.
His exoself automatically convert those signals into something Ostaupixtrilis Pontirijaris could understand.

*I adopted it from the Ranensis Fabricator, decided to practice childbearing.*

Ostaupixtrilis Pontirijaris turned his gaze to his behind, where a green beast could be seen, "wait, what?"

The green beast stood with a pair of thick legs, supported with a third limb, which could be its tail.
They had two pairs of manipulator limbs near the center of its body, the lower pair was much smaller and carries a plate of molluscs-like critters.
The upper pair was as huge as their rear limbs, but with manipulator tentacles on its tip.
They used their fore limbs to support its body, trying to sit on a chair to my right.
After they were properly seated, they put the plate down to the desk.

*Usually my race just throw our fertilized eggs to the wild and after a couple of decades we select the most intelligent specimens of our offspring to be introduced to our society, and have them civilized, educated.*

With its lean middle limbs, it picked some molluscs-like critters on its plate, and inspecting it with what Ostaupixtrilis Pontirijaris believed to be its nose.
Ostaupixtrilis Pontirijaris took some moments to observe that their numerous amount of fingers, or appears to be the size of one's fingers, situated into rows along the length of their limbs's ventral surface and ended on a circular chest opening, where the rows across all of its eight limbs (head included) merged into a ring of finger rows.
Their 'nose' had some long sensory fingers, checking on the moluscs, and Ostaupixtrilis Pontirijaris was convinced that they were about to swallow the critters.
They were, but there were no mouth opening on their 'head', they swallowed the poor critters with their *chest opening*.

Ostaupixtrilis Pontirijaris gasped, and an infographic showed in his mind, courtesy of his exoself.
Solenadactilian, an intelligent species that evolved from a radially symetrical ancestor, originating from an exceptionally garden world captured moon, orbiting a gas giant, which also orbits on its star's habitable zone.
One of their traits are complete lack of emotion and/or empathy, driven completely by curiosity and caution.

Certainly not the kind of creature that decide to adopt other beings out of love.
Adran must be adopted for a completely different reason but a simple desire to pet something, thought Ostaupixtrilis Pontirijaris.

Adran slurped its bowl of black goo again, "yeah, imagine that, a logical being decided to adopt a yet another logical being."

"But why?"

The solenadactilian did not answer.
They continued to savor the poor live mucuous critters.

"I don't think there's a need for Leaf to answer your query."

"Leaf?"

"Their name is Leaf."

The green being inspected Ostaupixtrilis Pontirijaris' face, literally, by running their sensory fingers across his facial surface.
Ostaupixtrilis Pontirijaris backed away from the probing fingers.

*I am Leaf. Who are you?*

"I am Ostaupixtrilis Pontirijaris."

\***

Stratos is a gas giant with no solid surface and crushing gravitational acceleration no normal etoan could withstand.
It had moons, until the godling also named Stratos decided to strip some of its moons and construct a habitable strip above its equator.
A solid ring encircling the planet would normally fell with the slightest pertubation, cripling the entire structure.
However this habitable strip was supported with mass streams accelerated inside giant mass accelerator tracks underneath the habitable surface.

The mass streams were accelerated along the length of the accelerators, encircling the entire planet faster than its orbital speed, producing centrifugal force outwards from the planet's core.
The mass of the entire structure: the habitable surface, the mass accelerator tracks, and anything in between, pushes back against the stream with magnetic fields, hence balancing the centrifugal force with a counterpressure from its weight.
Essentially, the entire structure must be dynamically balanced continuously, as if the centrifugal force increased beyond the counterpressure from the structure's weight, the structure would be torn apart, and if the centrifugal force decreased below the counterpressure from the structure's weight, the entire structure would collapse into the planet surface.

The strip was located in a region where the surface acceleration from the underbody (Planet Stratos) is bearable to etoan like Ostaupixtrilis Pontirijaris, and theoretically, also humans.
The surface area of its strip habitat was much larger than Earth's surface area.
Exploring the entire area would consumes some centuries of free time.
Ostaupixtrilis Pontirijaris decided to explore only the Earth-like survival zone.
There was no need to explore every single zones of Stratos.
And for some reasons, Leaf and Adran decided to follow Ostaupixtrilis Pontirijaris.

It was also the time Ostaupixtrilis Pontirijaris realized that Adran was actually a living vehicle, and the humanoid shadowy figure was merely its avatar.
Leaf and Ostaupixtrilis Pontirijaris travel from the spaceport to their temporary accomodation inside Adran's main body.
As the Ranensis-line Autonomous Transport Vehicle (ATV) with its operational mass at around three metric tons roamed across the polymer-paved road, Ostaupixtrilis Pontirijaris thought that perhaps it was why Leaf adopted Adran: to aid them.
As Adran was activated with no initial wealth, Leaf would provide it with resources required for Adran to learn and pave its way to the society, before Leaf's guardianship would be revoked by law, and Adran must inherit a part of Leaf's wealth.
Ranensis Fabricator was well known for its obligation to revoke the guardianship status of its ATV adopter after about a hundred years passed since the date of adoption.

Reaching a city with a number vine-covered limestone skyscrappers spiking off it, a striped bat-dog flew from the entrance to Adran's hood.
Adran halted, then Leaf and Ostaupixtrilis exited Adran. Adran exited its avatar as well.

"A *cadviri*?"

*Oh, another inedible Aucafidian lifeform.*

The ten-kilogram bat-dog vocalized a perfect speech, "Greetings, guests. You're about to embark in an adventure that requires you all to be reincarnated as Earthly beings."

"What does it mean?" asked Adran.

"We prepared rent bodies for you to fit in. Please choose from storage, and transfer your mindstates to the body. Then you're going to be departed to the amusement site," said the bat-dog.
