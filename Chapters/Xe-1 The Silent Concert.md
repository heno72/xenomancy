# The Silent Concert

> *Any sufficiently advanced technology is indistinguishable from magic. - Arthur C. Clarke*

> *Rerum Flexius Lamina*, the reality bending layer, or known as the RFL Network, was an ever present and permeating network on the surface of the Earth.
> Set up by Lord El when he first came to the Earth twenty thousand years ago, it formed the basis of all magic-like activities on Earth.
> It wasn't magic though, it was just so sophisticated, so invasive, so elusive, and so ubiquitous that for all purposes and intent, it was indistinguishable from magic.

> It wasn't even supposed to be experienced by any normal humans, in the middle of an ordinary concert.
> It didn't matter which concert, even if the one singing at the stage was Ernie.
> It only mattered that it was an indoor concert.
> It mattered, because something was going to happen at the ceilings.

## What the hell, man?

Derictor and Henokh found themselves pushing against moving bodies.
The moving bodies pushed back, hard.
Loud music, shouts, and chants mixed together into one noise, vibrant in colors.
The spirit of the concert, the beats, all compelled them to jump, dance, and sing along.

They thought it wouldn't be worse than this.
They must be able to push further to the stage, to meet Ernie, an acquaintance of Anthony Matthias.
The farther they tried to get there, the farther they were pushed away.

They did not realize it at first, that they heard nothing.
They did not just ignore the noise of the crowd, the noise was simply nonexistent in an instant.
Nor was there Ernie's voice, which was visibly singing with all of her strength at the stage.

It wasn't just them that found it weird.
People around them were as confused.
They looked at each other, at Derictor and Henokh, and at each other again.
People far away danced still, shouted still.
They heard nothing.

A certain man wearing a denim jacket was thrown close to Derictor and Henokh.
He was dribbling with wounds, sweats, and his denim was torn here and there.
Derictor, Henokh, and the crowd around them, didn't hear anything.
But they saw it, an event that should produce quite a thump.

The denim man whimpered as he rose up.
He blasted lights from his weakened fist to the ceilings.
Derictor and Henokh, and the rest of the adjacent crowds, looked up.
Another man in black tuxedo stood upside down at the ceilings.

The blasts screamed inaudibly toward the man in tux.
He danced away, all of the blasts missed him.
It was his turn, and he turned a stance.
Firmly maintaining his stance, he thrusted a fist.
Firebolts jolted toward the denim man.
He shot firebolts, to the denim man, to their crowd.

The scene turned chaotic.
Their crowd tried to save themselves, but the surrounding crowds resisted.
They screamed hard that their throats were sore, but nothing was heard.
They were in a bubble of silence, while those around them weren't any wiser.

Beaten up, the denim man lied on the floor.
He rose again, much weaker than before.
But his will barely waver, his eyes filled with determination.
In between his wails, he stomped the ground, and the floor shook inaudibly.
Metal beams sprout off the cracked floor.
Those beams chased the man in tux like hungry snakes.

The man in tux spread his arms.
Metal frames of the ceilings strapped the metal beams from the floor.
They were entangled, intertwined, crushed with each other.
More and more frames extended from the ceilings, and they impaled the denim man.
Metal beams from the floor turned against the denim man, all impaled him.

It was dark, and something liquidy ran to the floor.
Derictor and Henokh knew it was blood.
Dribbled from the impaled dead body that was barely recognizable as the denim man, the blood soaked the floor.
It soaked their feet as well, the stench of the fresh blood, and the squeaky sensation of the wetted floor, all were very vivid for them.
It was impossible for them to even consider all of those as mere hallucination.

The man in tux gazed at Derictor, Henokh, and the crowd.
He topped off his hat, giving them a nod.
At that moment, only one voice was heard by them.

"We apologize for the inconveniences."

The dead body of the denim man was lifted off toward the ceilings.
As if hanged by an invisible rope, it followed the man in tux.
He walked away from them.
He did not look back.

The metal beams from the ceilings returned to its proper position.
The metal bars from the floor returned to its proper position.
The blood withdrew and floated up, following the dead body.
The concrete floor sealed back, its cracks dissipated.
As if nothing has ever happened to them.

After that, the noise of the chants, shouts, and loud music returned.
Even the atmosphere had gone back to normal.
Derictor, Henokh, and their crowd gazed at each other.
No one said anything.
They gazed at the former battle ground.
Only a blank area of the floor in the middle was left, but no trace of a fight.

The only traces left were in their memories.
But no one would believe them.
The surrounding crowds around them heard nothing but the concert.
They saw nothing but the concert.

Derictor and Henokh were walking away from the scene, trying to locate the man in tux, and the body of the denim man.
As they inspected the surroundings, someone pulled Henokh's shoulder.

Henokh turned to face the man that dared to pull his shoulder, "What the hell, man?"

"Yeah, what the hell, man," shouted Anderson, "what the hell are you doing here?"

It was more than enough for Henokh to realize that he messed up.
It was when Derictor regretted his decision to come along with Henokh.
It was the time Nurhayati looked at them from behind Anderson.
It was followed by a wave of Derictor's hand, aimed for Nurhayati.

"You're not supposed to be here", Anderson said as he stood closer to Henokh, while Nurhayati waved back at Derictor.

Henokh didn't answer, he shouldn't be there.
He realized that his former excuse: to meet an old friend, sounded silly at that moment.

Nurhayati looked around to locate Ernie, and when she spotted Ernie.
She was into her character: a young, delighted, energetic woman.
She left for Ernie, far away from Anderson, Henokh, and Derictor.
Derictor's feet dragged him toward Nurhayati, but Anderson's firm grasp held Derictor's wrist so hard that he almost screamed.

"We're not done here," said Anderson.

## How do you plead?

"You're kidding me," said Chandra Watthuprasongkh.

That elder man stood unwavering, as firm as a lamp post.
He was filled with rage.
His usual effervescence was gone, replaced with dim boiling anger that reddened his face.

"You're the Chief Executive Officer of the Department of Coordination, F-f," Chandra was about to curse, but he let out a sigh, "-for God's sake."

Having the Chief Executive Officer of the Department of Field Operations (DFO) of the Intelligence Agency shouting at him, was enough for Henokh to finally realize that he messed up.
Normally, it was the Department of Cooperation (DoC) that issued commands for the DFO to execute.
Having the CEO of the DoC, doing field's duty, was never considered an option since the institution of the Intelligence Agency.

That was exactly what Henokh did, and that was why Chandra could raise his voice against Henokh.
Henokh was not doing his duty as the CEO of the DoC, Henokh was acting against the very foundation of the Intelligence Agency, the Statute of Founding, that institutes the separation of duties between departments.

"As a privately funded intelligence agency, the Intelligence Agency needs that structure, for the purpose of compartmentalization.
Each department only concerns their own field of duties, always separate, always detached from each other.
Exchange of information only occurs at the need to know basis.
That way, any other institution would be having a hard time to pinpoint any commonality, any relationship between independent departments of the Intelligence Agency, and to learn about the very existence of the Intelligence Agency," explained Chandra.

As if it wasn't embarrassing enough that Chandra lectured him about the nature of the Intelligence Agency, the institution that Henokh himself was a founding father of, the CEOs of the other departments nodded in agreement to Chandra's speech.
Henokh knew he'd messed up since the beginning, when he decided to visit that concert, he was just not expecting that it could be brought up to the Council of the Chiefs of the Intelligence Agency.

"Now, how do you plead?" said Chandra.

Henokh didn't say a word.
The other CEOs whispered among each other.
They casted their votes.
It wasn't a pleasant meeting for Henokh.

## Not the worst evening

"You're lucky they just voted for a month's suspension from your duty," said Chandra, sipping his pitch dark coffee.

Henokh didn't reply.
He stirred his cappuccino.
Derictor sipped his vanilla milkshake in silence as well.

"And you, Derictor, I haven't decided what punishment I must give to you," continued Chandra.

"Look *Khun* Chandra, I asked him to join me," said Henokh.

*Khun* preceded a Thai given names in the same manner as "Mr., Ms., Mrs." were in English, unless they carry a higher degree, such as a doctor.

Chandra looked at Henokh, he sighed and rolled his eyes away, "you know that you should not be conducting a field operation. It is the job of my department."

"I," said Henokh, his voice lowered, "I'm sorry."

Chandra heaved, "I'll take over the search for this Anthony guy, for you." his index finger aimed at a space in between Henokh's eyebrows, his eyes stared deep into Henokh's soul, "just promise not to do it that way again," his gaze softened.

Chandra's fatherly gaze gave Henokh the courage to look back at Chandra's eyes, "thank you, *Khun* Chandra."

"And for you, *Rick*," Chandra turned his attention to Derictor.

"Don't do this again."

"Yes *Khun* Chandra," said Derictor.

It was not the worst evening, thought Henokh.
At least they were having a nice and warm chat afterwards.

## Dominic's small feathery body

In the middle of a white space, adorned with billions of black stars, Dominic found himself floating.
He felt no weight, almost like he was in a free fall.
His focus was drawn to one of the black stars he was approaching.
The black star was drawn closer to him, and its size grew in his field of view.

The black stars were a spherical hole each, analog to the usual two dimensional circular hole, but extended into three dimensions.
The closest thing that Dominic thought would be closer to what he was observing was a black hole, but there was no event horizon.
The sight from its behind was all visible, but it got distorted and squished around the edge of the hole.
Inside the hole, instead of a pitch black event horizon found in normal black holes, contained an entire separate world with black skies.

The hole looked like a spherical surface that displays the stars of an entire sky, looked from the inside.
However the spherical surface was torn inside out, and flattened to a circular area.
All the stars at the edge of the hole would get squished and distorted, almost like it was seen from a fish eye's lens.
That was what the black star looked like from Dominic's perspective.

Dominic was drawn closer to the globe, and the delineation of the globe and the white background dotted with black stars started to be his new horizon.
At the surface of the globe, he could see that the upper half of the sky was all white with black stars, and the lower half of the sky below the horizon was pitch black with white stars.
As he moved lower below the surface, he realized that the former white sky was then a globe in the black sky dotted with white stars, the reverse of what he observed before.

"A wormhole?" Dominic asked himself.
If it was a wormhole, the previous white sky was a separate universe to the current universe he entered.
The white globe would be the exit mouth of that wormhole.
He was drawn away from the exit mouth, and toward what appeared to be a normal star.
A planet was orbiting the star, and he fell toward it.

Dominic had heard about the RFL Network's ability to record and store information of everything that has ever happened on Earth.
All of the sufficiently sophisticated thinking beings, ranging from the infraorder Cetacea, the clade Elephantimorpha, the taxonomic families of Hominidae, Ursidae, and Corvidae, had their every single individuals recorded since the day of their conception to the day they cease to function.
The record that would later be named as the Synaptic Observation Upload Link (SOUL) would transcend the death of the specimen.
They would be subjected to rigorous tests before being selected by the Powers of Earth.

"Am I going to hell?"
A cold sensation crawled on the entire length of his spine, and to all of his bones.
In accordance with the Decree of Lord Baal-Hadad, all SOULs, bearing guilt, would be delivered to Hel, where they'd relive their guilt until they could move on.
In short, Hel is a laundry machine for the SOULs, before they could move to the next stages.
It was the last place Dominic wanted to be in.

Dominic heard his own scream as his body dragged lower to the planet surface.
The pain squeezed his entire body, and the pressure was most intense at the temple of his head.
A cold breeze was felt on the surface of his head above his temple, and fingers could be felt probing his exposed head.
He could not imagine what kind of horror was waiting for him to feel.
He heard that the torture at Hel would be the most imaginable kind of pain.

The pressure grew in intensity and the fingers probed deeper, pulling his head toward the breeze.
The pressure moved toward his face, his neck, his shoulder, and the bright light with intense coldness bombarded all of his senses.
He was born again.

Any SOUL that managed to leave Hel, no longer possessing a physical existence, would populate one of the countless Paramundi.
Paramundi, plural for Paramundus, referred to the virtual worlds existing inside the computing substrate of RFL Network.
It was colloquially referred simply as the spirit realm.
Those SOULs would populate the spirit realm along with the Volants, beings native to the spirit realm, where they would live on until either they die again in a spirit realm or migrate to another spirit realm.
Colloquially, the SOULs and the Volants were called simply as spirits.

At that moment, Dominic was relieved that he did not go to Hel.
He was directly transferred to one of the Paramundi as a newborn.
Never did he expect that a virtual world would feel just as real and as tangible as the real, tangible, physical world he used to be in.

"Behold, the birth of Sojobo Kuryama, the successor of the Daitengu of Mount Kurama," said a voice, Dominic's small feathery body was raised high.
The cold breeze was intense, the light was bright enough to hurt his eyes, and his eyes couldn't focus on anything.
A distant crowd cheered, the noise was so loud it was painful for him.
He stretched his beaks as his scream went out of the bottom of his lungs.

## Sojobo wanted to grin

Several weeks had passed in the real world, but it was many decades later in Paramundus Jagadlangit, the realm of the Sky.
A large number of tengus gathered in their tree-cities, flocking at the large branches surrounding the Central Tree that also function as the palace of the incumbent Daitengu, their leader.
The Tengus were the kind of spirits well known for their proficiency in battle, and had just recently united under the incumbent Daitengu of Mount Kurama.
However the incumbent Daitengu had his term come to an end and must resign from the office, as the new heir reached the legal age of majority.

The spectating resident Tengus with their red-feathered-faces that featured protruding beaks, cheered upon the upcoming ascension of their new Daitengu.
Their cheerings intensified as Prince Sojobo Kuryama flew toward the palace, accompanied with a fleet of Kurama Tengu Forces.
Sojobo landed at the ascension platform, and his wings transformed into hands.
He waved at the spectating crowds, and they were ecstatic to welcome their new leader.

Their non-stop chants intensified as the Imperial Robe was fitted into Sojobo's body, and he gave his first order.
His first executive policy was to expand the domain of the Tengus across all the Paramundi, and ultimately, the real world.
The conquest started with the neighboring worlds, and one of them was Paramundus Jagadpadang, the realm of the Savanna.

A large number of fleets of giant tree ships lifted off, carrying the Kurama Tengu Forces zooming high above the Land of the Rising Sun, up to the space.
The fleet zoomed farther away from their home planet, from their star, and approached the wormhole.
As they entered the wormhole, the black sky was consumed by the white sky, until the black sky behind them appeared only as a black globe.
They moved farther from the other end of the wormhole mouth that was their homeworld, and toward a nearby black star that was the gate to the Paramundus Jagadpadang.

Inside the command seat of the leading giant tree ship, Sojobo wanted to grin, a facial expression he could no longer produce due to the fact that he had beaks instead of lips.
It did not hinder the pleasure, and he giggled instead.
His ambition lay far ahead: after all of the Paramundi fell under his rule, the next realm he would invade would be the real world that was his homeworld.

## We did play mock battles

The clouds shone bright white in the sunless green sky of Paramundus Jagadpadang.
Three creatures wrestled, chased each other, and then lied together on the vast expanse of limitless savanna.
They laughed, and teased one another.

The one that looked like a lion was Barong Ket.
Another looked like a tiger, was Barong Macan.
The last one that looked like a babirusa, was Barong Bangkal.
Every one of them was barongs, the kind of highly respected benevolent spirits.
Barongs can be distinguished with their distinctive horn in their forehead, and beneath it, in between their eyebrows, a hexagonal mirror.
Their eyes were glowing alternating concentric bands of various colors.

It was like any other day in the Domain of the Balidwipa Barong Association, if they weren't chasing some feral critters either for food or for sport.
Occasionally, like any healthy Band of Seeders, they would court or be courted by a Band of Bearers or more.
If they were lucky, the Band of Bearers would be willing to be impregnated, and carry their babies.
At other times, fellow Band of Seeders would occasionally cross their path, and both Bands would perform ritualistic fights and play for some period of time before parting.

It was almost always peaceful, and it had been that way for countless daylight and nighttime cycles.
Until the sky breached open.
Gigantic floating branched tree barks barged in from a spherical hole in the sky.

From the branches, countless numbers of flying creatures dispersed like a swarm of locusts.
Lightnings barraged from every single one of them, casting fire to the infinite plane of Savanna that the Barongs called home.
Ket, Bangkal, and Macan looked at each other, all of their paws grasped Macan's body, and they vaporized into thin air.

In his castle, Barong Airlangga, the Dewata of the Balidwipa Barong Association at the time, held a meeting with the resident barongs.
Ket, Bangkal, and Macan materialized at the castle, and they didn't like what they saw.
Airlangga, a human barong, seemed anxious, his humanoid body meandered in the middle of the court.
He scratched the horn in his forehead, and then wiped his hexagonal mirror in between his eyebrows.

"Don't tell me we're being invaded," he mumbled.

"But we are," said Bangkal.

"I know, don't tell me. I know it already," Airlangga scratched his head.

Airlangga looked up, took a deep breath, and exhaled.
He closed his eyes, and continued his breathing exercise.
He let out a deep sigh.

"Okay, what can we do?" He said.

A countless number of barongs that populated the court murmured with each other.
A moment was spent, and more moments were spent.
No one came out with anything.

Airlangga clenched his fists.
"Have any of you had any fighting experience before?"

"We haven't had battle since *time immemorial*," said an elephant barong.

An orangutan barong stood up, "I remember that back then, we had a number of weapons to fight Rangda. Do we still have those weapons?"

"If there's such weapons, the previous Dewata didn't seem to bother to tell me where," Airlangga sighed.

Ket looked around, and then to his band, Macan and Bangkal.
They nodded, and Ket advanced.
"We are active members of the Samsara Task Force, in Paramundus Integra."

Macan advanced as well, "we fought the Apex Souls there, powerful resident souls with special perks."

It was Bangkal's turn, "and we won the battle in several accounts."

The remaining barongs murmured with each other again.

"That's all? Among all of us, only three of you have recent battle experiences?" Pleaded Airlangga.

A gazelle barong came up, "well, we did play mock battles occasionally, I hope it counts."

## Impressive indeed

Ket led a militia force that consisted of barongs that had experiences in battle, mock battles or otherwise.
They stood at the demarcation between the burned savannah and a newly conjured dense forest.
For the majority of them, it was the first time they realized that trees can grow close to each other, spending the majority of their time in the landscape that was scarcely populated with trees.
Ket gave the sign, and they barged into the forest.

It was a massacre, countless numbers of their people were being pulled up, and dropped from great heights by the tengus.
Those that managed to survive launched their attacks.
Most weren't strong enough to deal damage to the tengus.

Ket howled hard, his mane emanated some sort of invisible shield, holding the incoming tengus back.
Another howl and a number of tengus were thrown aback.
An advancing tengu flew straight toward him, and he slapped the tengu hard with his claws.
The tengu were thrown several meters to a tree that snapped upon impact.

Macan moved like a mist, vaporized at one place and solidified at other places.
He solidified above a tengu, and with his fangs, he crushed its wings.
They both fell, but before they reached the ground, Macan vaporized again.
From the point where he materialized, he could see the tengu that was bitten fell among the crowds.

Bangkal charged at great speed.
No single attack of the tengus can stop him.
He hit the great trees headon, and every single one of them fell right away.
His trail opened a path for the incoming barongs to barge in, swarming the forest floor.

Macan and Ket looked at each other, they knew it wasn't enough.
There were too much of the tengus, capable of moving not only at the forest floors, but through the airspace as well, while most of the barongs were constrained on the forest floors.
At this rate they would fail.
They would be slaughtered.

Ket and Macan solidified near Bangkal, joining him on his charge.

"We will find the leader," said Macan.

"To cut the snake at its head," continued Ket.

"Go, I'd keep on smashing the trees" said Bangkal.

Macan and Ket nodded at each other, and their claws grasped each other.
They vaporized, and solidified at a tree branch.
They analyzed the movements of the tengus, and the propagation of their coordination.
Their formations changed when a certain type of tengus: messenger tengus, came to their vicinity and squeaked.

Ket and Macan dispersed, vaporized and solidified between tree branches.
They followed the messenger tengus, to a giant tree deep in the forest.
Atop of it, was a giant hole carved in.
Numerous messenger tengus flew to and from the hole at a constant rate.

"That's it, it must be their command center," said Ket, "can you look at the interior?"

Macan sniffed hard, his eyes wide open.
His sight broadened, and focussed at the hole, and turned around the hole.
The inside of the hole were posts, each occupied with tengus.
A number of tengus, messenger tengus included, flew by from one post to another.
In the middle of the structure, was a suspended post that featured a throne.

"I think I saw the leader," said Macan.

"Can you take us there?" Requested Ket, "it was too far up, beyond my translocation range."

Macan patted Ket's back, and they both were vaporized.
They materialized in front of the throne, startling the leader.

"Interesting indeed," said the leader after he collected his senses, "welcome to my lair.
I am Sojobo Kuryama.
Submit to me, and you might be spared."

His red face, adorned with black beaks, was tilted up.
Ket and Macan were convinced that he was smiling.
His right wing deformed into a hand, a human hand covered with the sleeve of his coat.

"We want you to cease your invasion, and return to your world," said Ket.

Sojobo's eyes didn't look happy, he drew his longsword and held it with both of his hands.
"Then I don't see any reason to treat you nicely," he advanced and almost penetrated Ket's body.

Ket let out a high pitched howl and a rumbling growl, his mane was raised.
The longsword vibrated and repelled away from Ket's mane.
The tone of Ket's howl undulated, and an invisible force pushed Sojobo back.
Sojobo resisted, with his longsword in a defensive stance.

"Macan, bring Bangkal here!" Shouted Ket.

Macan nodded, and he vaporized in a puff of air.
A moment after, he materialized with a giant babirusa who was Bangkal, whose eyes glared with anger.
Bangkal charged toward Sojobo, who blocked Bangkal's tusks with his longsword.

Bangkal jumped and landed at a great force that the suspension of the platform snapped.
They were in free fall, and a number of tengus at the platform flew away.
Sojobo was about to fly too, but Macan and Ket held his wrists with their fangs, preventing it from reshaping into wings.

Moments before the platform crashed, Macan translocated himself with Ket and Bangkal away.
The platform crashed hard, shattered into unrecognizable pieces.
Sojobo rose from the rubbles, his robes were torn here and there.

Sojobo raised his hands, and a number of tengus surrounded them, with their swords drawn, and aimed at the three of them.
Sojobo charged toward them, and the tengus around them slashed their swords in circles.
Space around the tengus disintegrated by their swords and wobbled out of existence.

They were in a bright white space, with countless black stars surrounding them.
They were freefalling.
Translocation did not work here, no matter how hard Macan and Ket tried.

"We're in the *Antarabhava*, the space between Paramundi," said Macan, observing his surroundings, "those black stars, each of them are individual Paramundus."

"Where are you taking us?" Inquired Ket.

"To a place far away, that you would not be able to return," said Sojobo.

Sojobo flew closer to Ket, and thrusted his longsword.
Ket parried the sword with his claws.
As the sword halted, in a rapid set of moves, Ket pinched the blunt side of the blade in between his fingers and his palm.
Ket recognized the reflex patterns Sojobo exhibited.
A pattern he knew so well from his experience in Paramundus Integra, the world that was populated mainly by humans.
He then disarmed Sojobo with little effort.

"You were a human," remarked Ket.

"Impressive indeed," Sojobo's eyes glowed in satisfaction, "I didn't know any barong alive today would recognize a human."

With the blade of the sword gripped with both of his hands, Ket swung the sword that the crossguard hit Sojobo's head, a half-swording technique known as Mordhau, or murder-stroke.
Sojobo were knocked off, flung toward one of the tengus that surround them.
The rest of the tengus slashed a downstroke, then a wormhole appeared at some distance beneath them.

On the other end of the wormhole, was a space filled with humans.
Ket recognized it to be a ballroom of an unidentified building.
It was apparent that a performance was held there.
Ket recognized the atmosphere of the world of men.
It felt awfully similar to Paramundus Integra.

"Integra?" Said Ket, he looked at the wormhole, and then at Macan.

"No," said Macan, his eyes scanned the other end of the wormhole.
Macan knew it was something similar to Paramundus Integra, but it wasn't integra.
Other than Paramundus Integra, there was at least one more realm he knew that was also populated mainly by humans.

Macan closed his eyes and sniffed, his suspicion was confirmed.
It wasn't the feel of any Paramundus he knew.
It wasn't a Paramundus at all.
"It is the *Manent realm,* the realm of physical reality."

"You never ceased to impress me, barong," said Sojobo, wiping the blood off his head.
