# KUNJUNGAN

Oleh Hendrik Lie

| Label | Data |
|:--|:--|
| Originally Submitted to | PERS JEMPOL PNP <ukmperspolitekniknegeripadang@gmail.com> |
| Submission Date | 20190517 |
| Subject | Lomba Cerpen |

---

Ayah dan Ibu akan berangkat kerja pagi ini.
Dan hal tersebut berarti saya akan berada di rumah sendirian dengan Adik.
Ibu tidak biasanya ikut dengan Ayah, tapi Ayah bersikeras mengajak Ibu, ia meyakinkan Ibu bahwa Ibu tidak akan menyesal mengikutinya.

Tetapi bukan hanya Ayah yang bersikeras mengajak Ibu pergi.
Pada siang hari, Adik berusaha mengajakku ke hutan di belakang rumah.
Ia berkata ada hal yang ingin ditunjukkan kepadaku.
Hari libur, dan tidak ada orangtua di rumah adalah surga bagiku.
Aku memutuskan untuk tidur saja di rumah.
Jelas saja, tawaran Adik aku tolak.

"Kalau tidak mau ikut saya, saya akan pergi sendiri!"

Adik ketus meneriakiku, dan segera saja diikuti teriakanku,

"Pergi saja! Aku tidak peduli!"

***

Bro tidak pernah mengizinkan Sis untuk mengemudikan pesawatnya.
Sis sangat yakin bisa mengendalikan pesawat ini, setiap hari sejak Sis kecil, ia telah mempelajari cara mengendalikan pesawat, bahkan telah mengikuti simulasi-simulasi mengemudikan pesawat.
Di hari ini, Bro sedang keluar ke stasiun untuk menyiapkan dokumen untuk suatu pertemuan penting.
Sis memutuskan untuk berangkat sendiri dengan pesawatnya sebelum ia marahi.
Di akhir hari, bisa saja Bro mengakui kemampuan Sis mengemudikan pesawat.

***

"Mereka itu makhluk neraka" ketus Ibu.

Ayah mengernyitkan dahi, ia hanya bisa memelototi si Ibu, tapi akhirnya ia memutuskan bertanya.

"Maksud Ibu apa?"

"Ayah tadi bilang sendiri kan, saking panasnya suhu planet mereka, batu adalah benda cair, dan itu yang menjadi darah mereka.
Apa lagi itu kalau bukan setan dari neraka?"

Ayah menghembuskan nafasnya,

"Ibu jangan kemakan tahayul.
Mereka bukan setan, kebetulan saja planet mereka berada di dekat matahari mereka.
Tahu gak Bu, justru kehidupan di planet kita unik, bentuk kehidupan seperti mereka ternyata lebih sering muncul.
Di planet-planet yang posisinya sangat dekat dengan matahari, planet-planet yang saking panasnya darah kita mendidih di sana."

"Artinya banyak setan kan di luar sana.
Makanya Ibu tidak pernah setuju dengan program ruang angkasa.
Ketika setan datang ke dunia kita, tidak lama kemudian dunia kita akan hancur, jadi seperti api neraka di planet mereka."

Si Ayah memilih untuk diam.

***

Si Adik pergi ke lapangan di balik bukit dekat rumah mereka dengan berjalan kaki.
Pada waktu itu angin sepoi-sepoi dapat Adik merasakan bertiup di antara sela-sela cabang pepohonan, dedaunan bergemersik dengan alunan bisikan alam, dan rumput yang lembut dapat ia rasakan di setiap langkahnya.
Ia tiba ke tujuannya, sebuah bangunan terlantar di tengah hutan.
Di depan bangunan tersebut terdapat kolam yang cukup dalam.
Kolam tersebut sudah lama kosong, dan kebetulan saja, ada makhluk yang terjebak di dasar kolam.
Si Adik memutuskan untuk melihat si makhluk tersebut.

Makhluk tersebut sangat jelek, bermata satu dengan warna keemasan.
Matanya cukup besar, sebesar genggaman tangan si Adik.
Gerakannya sangat gesit dan cepat, dan ia melompat-lompat berusaha menggapai tepi kolam.
Si Adik tidak dapat menemukan satupun alat yang bisa digunakannya untuk membantu makhluk itu keluar dari dasar kolam.

Si Adik mendengarkan suara decakan yang sangat cepat, dan cempreng dari makhluk tersebut.
Makhluk tersebut menunjuk-nunjuk si Adik, kemudian kembali meneruskan usahanya untuk melompat ke tepi kolam, dan terus gagal.

Untuk membantunya, si Adik memutuskan untuk pulang ke rumah mengambil tali, dan decakan makhluk tersebut semakin menjadi-jadi ketika si Adik bergerak menjauh.

***

Sis berhasil mendarat dengan mulus di suatu planet.
Menurut standarnya, planet ini sangat dingin.
Jika bukan karena pakaian penghangatnya, Sis mungkin akan mati membeku di sini.
Pada suhu serendah itu, darahnya menjadi tidak ada bedanya dengan bebatuan dan pasir di planet ini.

Jika orang menganggap planet ini gersang, mereka salah besar.
Planet ini penuh dengan sejenis tumbuhan, dan berbagai macam hewan.
Ada yang terbang, ada yang merayap, ada yang memanjat di pohon.
Sis menghabiskan waktunya menikmati suasana di planet tersebut.

Suatu ketika ia menemukan suatu struktur, tampak seperti sebuah bangunan.
Ia memasuki pintunya, dan menyadari betapa besarnya pintu tersebut.
Dibandingkan ukuran badannya.
Ia mengamati sekelilingnya, ke atap, ke tembok, namun tidak melihat bahwa terdapat kolam kosong yang sangat dalam di depannya.
Ia terjatuh.

Sis meraung kesakitan karena jatuhnya.
Jika bukan karena planet ini memiliki gravitasi yang lebih rendah dari planet asalnya, Sis mungkin sudah patah tulang.
Di tengah tangisannya, ia menyadari bahwa ia sedang diamati, oleh suatu makhluk yang sangat jelek di tepi kolam tersebut.
Makhluk tersebut menatap Sis dengan kedua matanya.
Kedua mata makhluk tersebut menatap Sis, dengan rasa penasaran yang dalam, atau dengan nafsu membunuh yang kuat.

***

Ibu tetap merasa mereka harus berhati-hati, makhluk dari api neraka tempat di mana batu adalah benda cair jelas adalah tanda seru yang sangat besar.

"Perbedaan suhu tempat tinggal seharusnya menjadi tanda yang sangat baik, Ibu.
Artinya, mereka tidak akan berkompetisi akan sumber daya dengan ktia, kebutuhan mereka sangat jauh berbeda dengan kita."

"Tetap saja, setan adalah setan.
Bisa saja mereka sedang berusaha membodohi kita, kemudian planet kita dibuat menjadi seperti planet mereka."

"Ibu berpikir tidak pakai otak ya?"

Si Ayah tidak mengatakan hal tersebut, ia mengurungkan niatnya.

"Untuk apa menggeser planet kita lebih dekat ke matahari jika di dekat matahari kita juga ada planet yang cocok untuk mereka tinggali, Ibu.
Energi yang diperlukan untuk melambatkan planet kita ke orbit yang lebih dekat ke matahari adalah jauh lebih besar daripada keuntungan yang mereka dapat jika mereka meninggali planet-planet di dekat matahari."

Si Ibu tidak menjawab.

"Mungkin masalahnya hanya pada komunikasi.
Oh mengenai komunikasi, untung kita berhasil mengembangkan sistem komunikasi di antara dua bangsa kita.
Mereka menyediakan teknologi yang diperlukan untuk itu.
Kita bisa mengetikkan pesan dan akan otomatis diterjemahkan ke pesan yang mereka mengerti.
Mereka juga akan mengirimkan jawaban mereka ke kita dalam bahasa kita.
Menarik kan Bu?"

Si Ibu tidak menjawab.

"Masalahnya mungkin hanya pada fakta bahwa mereka sangat cepat, kurang lebih waktu berjalan sepuluh kali lebih cepat daripada kita.
Kita bisa menjawab secepat mungkin bagi kita, tapi bagi mereka, kita sangat lambat dalam merespon.
Saya harap itu bukan masalah."

"Artinya jika mereka memutuskan untuk membunuh kita, kita bisa mati duluan sebelum menyadari bahwa mereka telah membunuh kita."

"Ibu!"

***

Kakak resah, hari sudah sore dan Adik belum juga kembali ke rumah sejak terakhir ia mengambil tali.
Ia khawatir, bahwa si Adik nyasar di dekat hutan.
Ia ketakutan bukan karena nanti akan dimarahi oleh orangtuanya, namun karena tanpa ia sadari, ia merindukan Adik.
Ia tidak bermaksud memarahi si Adik, tapi tinggal bersama terus menerus bisa menumpulkan empatimu dengan orang terdekatmu.

Hal tersebut baru terasa ketika orang terdekatmu telah pergi.
Dan kalau si Adik tersesat atau bahkan mengalami bahaya di tengah hutan, sendirian, di senja, kau bisa saja kehilangan orang itu selamanya.
Kakak memutuskan untuk pergi mencari si Adik.

***

Sis merasa kelelahan, telah berhari-hari ia berada di dasar kolam kosong itu.
Baterai di pakaian pelindungnya akan segera habis dalam beberapa hari lagi.
Di planet ini, satu hari lokal baru saja akan berakhir.
Dehidrasi bukanlah masalah, karena pakaian ini menyerap sekresinya untuk merehidrasi dirinya lagi.
Kesepian adalah faktor utama, temannya hanyalah satu makhluk yang bergerak sangat lambat.

Dan tampaknya makhluk itu juga sangat bodoh, selama berhari-hari di sini dan makhluk itu belum juga menjatuhkan tali atau semacamnya untuk membantunya.
Ia yakin makhluk itu tidak berniat memakannya, atau bisa saja makhluk itu menunggu ia mati, baru akan memakannya.

Ia lebih memilih membayangkan makhluk itu bersahabat, karena ia telah terus menerus berbicara dengannya selama beberapa hari belakangan ini.
Kesepian bisa sangat mengerikan, dan jika si Sis bisa dapat teman cerita, ia rasa sudah cukup.
Makhluk itu, di sisi lain, tidak memberi respon yang berarti selain dengungan nada rendah.

***

Ayah bekerja di tower bandara, dan sejak sore ini ia berharap akan ada yang datang dari langit.
Radar tidak menunjukkan tanda-tanda ada yang akan datang, selain pesawat-pesawat biasa yang lalu lalang.
Ayah yakin para Pengunjung terlambat datang.

"Bisa saja si Pengunjung itu sedang memanggil teman-teman iblisnya untuk datang menyerang," komentar Ibu.

"Cukup Ibu, Ibu tunggu saja di ruang tunggu," kata Ayah seraya menahan amarahnya.

***

Si Kakak menemukan gedung tersebut, dan si Adik baru akan melemparkan tali ke suatu makhluk di dasar kolam, agar si makhluk itu bisa bangkit.
Tampak si makhluk itu melompat-lompat girang dan menantikan tali tersebut.
Si Kakak melihat makhluk tersebut, ia bergerak cepat sehingga gerakannya tampak kabur karena kegesatannya.

Makhluk tersebut sangat jelek, Kakak tidak pernah melihat makhluk seperti itu.
Lutut si kakak lemas, dan setelah berhasil mengumpulkan tenaganya ia berteriak sekeras-kerasnya:

"Dek menjauh Dek! Makhluk apa itu?"

Si Adik tertegun dan melihat si Kakak.

"Ini kak makhluk yang saya mau perlihatkan tadi.
Gerakannya gesit, lincah, padahal badannya kecil.
Kasihan dia sudah beberapa waktu terjebak di dasar kolam."

"Harusnya kau bunuh cepat makhluk itu! Makhluk bermata satu itu bisa saja membunuhmu!"

Si Adik melemparkan tali ke makhluk itu, dan hanya beberapa saat kemudian, makhluk tersebut telah keluar dari kolam kosong tersebut.
Si Kakak langsung mengambil sebatang kayu di dekat kakinya.

***

Sis mendengar raungan lain dari pojok ruangan.
Tali yang akan diturunkan makhluk tersebut pun tertunda.
Mereka tampak saling meraung, dan gerakan mereka sangat lambat.
Selang beberapa menit kemudian, tali baru terjatuh ke dasar kolam, ujung satunya digenggam si makhluk tadi.
Sis segera meraih tali tersebut, dan setelah merasa genggaman di lengan makhluk tersebut kuat, ia langsung memanjati tali tersebut.

Setibanya di permukaan, Sis melihat satu makhluk lainnya sedang "berlari" ke arahnya, memegang sejenis tongkat, dan tongkat tersebut diayunkan ke arahnya dengan pelan.

Sis memegang tongkat itu, "terima kasih."

Si makhluk kedua tampak mundur sedikit, dan lanjut mendorong Sis kembali ke kolam.
Sis terkejut dan tidak paham akan perbuatannya.
Dorongannya yang lambat memberi Sis waktu untuk bergeser, dan akhirnya si makhluk tersebut jatuh ke dasar kolam.

***

Pesawat si Pengunjung tersebut sangat gesit, seolah-olah pesawat-pesawat kita seperti sedang merayap di awan.
Si Ayah telah mengirimkan responnya, dan sekejap saja jawaban telah ia terima.
Diikuti dengan jawaban lain, dan jawaban lainnya.
Si Ayah mencoba memahami pesan tersebut yang dikirimkan lebih cepat daripada yang bisa dibacanya.
"Adik saya sepertinya ke planetmu duluan, apa kau ada pengunjung sebelum saya?" "Di mana adikku? Apa ada sama kalian?" dan pesan-pesan lainnya yang intinya bertanya soal keberadaan adiknya.

Si Ayah tidak merasa ada yang datang sebelum si Pengunjung, ia membalas pesan tersebut, dan dalam sekejab saja ada pesan lain, isinya mengancam akan memberitahu pemerintahnya jika mereka berani menyekap adiknya.
Diikuti dengan permohonan maaf, karena ia merasa panik adiknya menghilang.
Lalu pesan berikutnya mengenai ia menemukan tanda pesawat adiknya di arah hutan.
Si Ayah berhenti membaca setelah menemukan kata hutan.
Hutan yang dimaksud adalah hutan di dekat rumahnya.

***

Adik berteriak melihat si Kakak terjatuh.
Makhluk tersebut berkata sesuatu dalam nada tinggi, dan pergi meninggalkan mereka berdua.
Si Adik pun berusaha mengikat tali di tepi kolam dan turun ke bawah untuk menarik si Kakak.

Tak disangka, si makhluk tersebut kembali lagi di tepi kolam dengan kendaraannya.
Si makhluk berusaha membantu si Adik mengangkat si Kakak di atas kendaraannya.
Kendaraan itu kecil, seluruh atapnya hanya cukup untuk memuat badan si Kakak, si Adik harus berdiri di atas badan si Kakak.
Kendaraan tersebut memanjangkan "kaki"-nya dan merangkak keluar dari kolam tersebut.

***

"Anak sial kau Sis! Kau tidak tahu berapa biaya yang harus saya keluarkan untuk sewa pesawat dan mengejarmu?"

"Maaf Bro, saya hanya ingin," suara Sis mulai bergetar, matanya melembab, "saya hanya ingin tunjukkan kalau saya juga bisa kemudikan pesawat."

Bro memandangi Sis dengan tajam.
Sis tidak dapat melihat banyak dari balik helm Bro, tapi Sis yakin Bro menangis, dan hal tersebut cukup mengejutkan bagi Sis.
Tangisan Bro bukan satu-satunya yang mengejutkan Sis, karena setelah itu Bro memeluk Sis.

"Kau tidak tahu betapa khawatirnya saya, Sis.
Kukira saya akan kehilangan kau."

Sis tidak kuasa menahan tangisannya, dan mereka berdua menangis.

***

Ayah dan Ibu keluar dari pesawat si Pengunjung yang sudah disesuaikan dengan atmosfir planet mereka.
Hal pertama yang membuat Ibu teriak adalah Kakak yang terkujur pingsan di tanah, tubuhnya penuh luka.

"Kakak! Kakak, kau diapakan Kak?"

Ibu meraung-raung, dan Ayah berusaha menenangkannya.

"Saya yakin mereka bisa menjelaskannya,"

"CUKUP! Kau kawin saja sama para setan itu! Mereka melukai anak kita? Tidakkah kau lihat?"

Si Ayah berusaha menanyai Bro dan Sis mengenai Kakak, namun Adik sudah menjelaskan kejadiannya duluan.

***

Di pesawat mereka, Sis dan Bro melepas helm mereka.
Sis menyempatkan diri untuk menyisir rambutnya.
Bro membiarkan rambutnya apa adanya, dan bergerak menuju konsol kendali.

"Saya janji tidak akan pergi lagi tanpa izinmu, Bro.
Berhari-hari di dasar kolam memberi saya waktu untuk berpikir" kata Sis, "silahkan hukum aku Bro, aku memang salah."

"Tidak perlu lagi kau bilang itu, saya yakin kau sudah memperoleh hukumanmu di dasar kolam kosong itu" kata si Bro.

"Iya Bro, saya minta maaf, tapi saya sangat bersyukur kau datang.
Kalau tidak, mungkin saya akan mati sendirian di sini."

"Ya, kau bisa saja mati sendirian di sini.
di planet yang menyerupai Titan di Tata Surya.
Di mana saking dinginnya air adalah batu, dan darah makhluk di sini, jika di suhu seperti di Bumi, bakal menguap jadi gas berbau kentut."

"Ya, dan hanya ditemani oleh makhluk jelek, dengan empat kaki, empat tentakel, dua mata yang terletak di pundak mereka."

***

Ayah dan Ibu menutupi mata di kedua sisi pundak mereka dari angin dan debu akibat roket pesawat si Pengunjung dengan tentakel-tentakel mereka.

"Saya sudah bilang mereka itu orang yang baik-baik, Ibu tidak pernah mau dengar."

"Sebagai setan, mereka memang sangat jelek ya."

"Ibu! Cukup!"

***

Kakak dan Adik hanya melihat pesawat si Pengunjung mendaki langit dari jendela kamar mereka.
Kakak dan Adik saling melilitkan tentakel mereka, si Kakak telah belajar untuk lebih menghargai Adik, dan si Adik senang dengan pengalamannya bermain dengan makhluk tersebut, betapa jeleknya pun mereka.
