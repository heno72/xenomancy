# Inspections

## Henokh and The Operatives
> In-story date is 20210817 1000

A figure of a man picked up a smartphone from his pocket.
He stood from his work desk and his legs instinctively circulated him around the room.
The phone kissed his left ear, and produced a voice of another man.

“ is it *ko* Henokh?” the voice inquired.

"Ko" was a suffix literally meant male older brother in Chinese-Indonesian Indonesian dialect, derived from Chinese "*ge*" that meant the same.
However Chinese-Indonesian used it to describe their fellow Chinese-Indonesian seniors with age differences of just a few years, or the one that's close to the speaker concerned.
If the age difference was too far away, and/or the person was just an acquaintance, they'd describe it with what approximately sounded like "*Shuk*" which could be somewhat translated as uncle.
Steven's not, obviously a Chinese-Indonesian, he's a Chinese-Etoan.
Naturally he looked somewhat fairly chinese-like, so people identified him as Chinese-Indonesian as well.

“ Yes, what is it now? Steven?” answered Henokh.

“ Ko, Andre's here.
He's wounded and not disguised.”

“ What? How come?”

“ I don't know for sure.
Apparently he's chasing Aditya, and sighted a sea serpent of a sort, and got wounded in the process”

The name rang a bell in Henokh's head.
A man with black blazer gazed at him in his thoughts.
That man was his best friend, though he couldn't be sure if he could identify that man as such at that moment.
They had been separated into different path now.

“ Aditya?”

“ Yes, Aditya, that was -was- your soulmate.
I'm fairly certain that's what Andre mentioned, and he confirmed it, he said you've parted..,”

Earlier this morning Derictor told him that Aditya's present at one of Kukercorp's buildings.
They had the building surrounded at the time, but the bomb exploded and they couldn't find Aditya anywhere.
Bain died and Kuin enraged by his creator's death, uh, that was Bain.
Anderson went for a chase to the suspect, but Anderson didn't mention anything that he chased a man known as Aditya.

“ Yes, I know, that Aditya.
I'd get Anderson now” he interrupted.

He hung up the phone, took a moment to recollect himself.
The bomb that he assumed belong to Aditya, bombed a server room near his office.
His office was also destroyed.
He wasn't there at the time, he was at his apartment unit, on the other side of the building.
Was the bomb intended for him? If that's true, than Aditya must be there to bomb him.
If that's true, then Aditya's no longer his friend.

Another man entered Henokh's apartment and approached him.
He had a lean body frame, about a meter and seventy five centimeters tall, that makes him about ten centimeters shorter than Henokh.
With round glasses and neat hairdo with his bangs neatly covered his forehead, Henokh could tell that the man's hairstyle was popular roughly five years ago, or that his sense of style froze about five years ago.
Same could be said by his attire, slim fit navy blue hem and dark brown jeans.
No, Henokh thought, he was convinced that the clothing was popular about a decade ago.
Their eyes caught each other.

“ Derictor, glad you were  here.
Let's go to Steven's clinic.”

“ Eh, now? Why? I am about to inform you about the bomb remains,” Derictor's voice was that of soft and childlike, but the tone betrayed it by highly masculinized intonation.

He continued, “ there are carbon dust, and closer inspection revealed traces of copper, aluminium, lithium, silicate, platinum, and the fact that the surviving chunks were made of three dimensional graphenes, in foamed structures.
Not just ordinary structures made of foamed three dimensional graphenes, those chunks were gigantic, macroscopic molecules each! Our forensic analyst were inconclusive in what was the explosive, there were no traces of explosives.
They were equally flabbergasted on the manufacturing processes that could make a single-molecule complex structures as such.”

“ The, what? What do you mean that our forensic guys couldn't conclude how it was manufactured?” Henokh replied.

“ Yes.
It is impossible for us to make complex structures on microscopic levels as such.
Not to mention that inside each bubble of the foam contained traces of copper, aluminum, lithium, silicate, and platinum, which, in our preliminary analysis, forms complex structures as well.
Even if we know what it was, we would never be able to reproduce it anytime in the following decades.”

“ Unless they were  conjured.
Each molecules assembled in place mechanically,” said another man with a tattoo on his left face, that out of nowhere stood beside them.

“ Hendrik!” Henokh and Derictor gasped in chorus.

They checked the apartment door, which was closed and locked.
There's no additional shoes on the entrance.

“ couldn't you appear with more, um, human-like? I told you I am not used to this kind of appearance!” protested Henokh.

The man pointed his fingers at his facial tattoo, “ Manov here.
Hendrik's still in the scene,” said the man, giggling, then the eyebrows wrinkled, his entire face turned into curious expression, “ weirdly we couldn't detect any trace of reflexium particles in the shard's structure, usually that's a signature of conjured materials.”

“ Conjured? What do you mean by conjured?” inquired Daniel.

“ Wait, wasn't Hendrik a lawyer or something in everyday life? What's he doing now with the forensic guys?” interrupted Henokh.

“ He's a lawyer, but there's certain benefit of being a polymath you know.
He also studied physics, autodidactically, you know,” responded Manov, now turned his gaze toward Derictor, “ And for that, it would be a very long explanatory session for another day, if you really are that interested..,”

Derictor's eyes brighten, entertained with curiosity, his mouth gaped open as if expecting a kind of food to be inserted in, and smile at the same time.

“ That's not one of our main concerns now,” interrupted Henokh, Derictor's mouth ungaped, his gaze expressed disappointments, “ We need to get Anderson first! He might be able to produce a proper explanation of it,” Henokh continued.

Hendrik nodded at Derictor, who was currently expecting approval from him.
Derictor then produced a smartphone out of his left jeans pocket, made a diagonal arched path toward his right ear,

“ This was Agent Delapan, to Agent Hatta.
Get us Oneiric Protocolers and two units of protocoler cars to Kukercity Apartment, Tower F, we were  about to extract Agent Enam on rendezvous location,” commanded Derictor, “ authorization by Agent Sukarno's on the way,” he continued, then handed his phone to Manov, which granted his fingerprint to the phone's sensor.

Derictor took the phone again, and continued “ authorization provided.
Execute!”

They then descended down into Tower F's parking lot, where two cars and three G28 Officers and two Securion Security Officers who were signatory members of *Oneiric Protocol* waited for them.
All of them wore black facemask plus a nondescript black hat, and armed with Securion-issued modified *Soargun rifles*, almost a meter in length.

A Soargun rifle, originally S.O.A.R-gun, stands for Securion Operative Aluminium-projectiled Railgun, that, as the name suggests, produced a beam of high velocity aluminium projectile.
It was a popular and expensive alternative to normal guns, as it does not contain explosives.
The lead-filled aluminium projectile was accelerated through a track that deliver lorentz force through application of electric currents, which then exited the gun at very high velocity.

A modified Soargun rifle was a Soargun rifle, specifically produced by Securion for Oneiric Protocolers, that was also equipped with phased laser arrays capable of delivering a ten kilojoules light beam, almost similar to kinetic energy of a normal bullet.
Or it could also function as a long-distance taser gun, in electrolaser configuration, where the laser beam would ionize air molecules in its path, of which a very high voltage of current could be conducted through.

It was the most advanced gun man couldn't produce it at the time, as the technology to produce the phased laser array crystals and power packs sufficient to power it are proprietary property of Securion International Incorporated.
However the railgun components itself isn't that hard for man to develop.

They arrived at Steven's clinic, Sejahtera Pharmacy.
Manov had this very weird feeling about Steven's car, which appears normal.
What's weird was that he couldn't sense its inside.
He couldn't sense the car in fact.
Not the slightest sense of its engines, its tires, its seats, as if there's a black void with the shape of a car in place of Steven's car.
Paradoxically, he can see the car perfectly, and can peer inside, which appeared like any normal car of its size.

Manov decided to touch the car, just in case he can get a proper feel, and probably replace the void with the feel of an actual car.
He touched it, rubbed it, and slowly traced its curve.
He can't.
He still felt the void in place of the car.
He can feel the car's texture, which felt normal, like any normal car.
But that's not the kind of feel that Manov expected he'd feel in a normal car.
He's not a normal human after all, he should be able to sense the car, its entirety, like he would in any normal car.
He can feel Daniel's car, even without touching it.
He can sense the engine of Daniel's car, its electronics, its couches, its frames, its glasses, its tires.
Every centimeter cubic of Daniel's car could be felt by Manov, even if they were  separated by two or three meters.
But he couldn't do the same to Steven's car.

He was frustrated.
Then Derictor severed his fixation to Steven’s car,

“ Manov, what are you doing with Steven's car?”

“ What? Oh, nothing,” Manov replied.

He decided to keep the frustration to himself.
It's not like he can explain it in a way Derictor could understand anyway.
Then he remembered that, he also felt the same ways on the hearts of modified Soarguns, the phased laser array crystals.

Henokh, Derictor and Manov entered the clinic and knocked Steven's room.
Daniel opened the door instead, and his eyes caught Henokh's.

“ Ah, Daniel's here as well,” Henokh's back straightened, he heightened his neck, or so he felt.

“ Henokh,” replied Daniel.

Daniel's just few centimeters shorter than Henokh, and certainly disliked by Henokh for some reasons he couldn't fully comprehend as well.
Derictor just stared at Henokh, his brows wrinkled, his mouth shut tight.
Henokh noticed Derictor's gaze and backed down, his neck relaxed.

“ Hi Daniel?” Derictor greeted Daniel with a warm smile, “ Where's Anderson?”

“ Anderson's in,” Daniel replied, then slid to the side, “ Come in.”

Three of them entered.
Manov's frustration grew even more, as he felt like being in a void, like there's just Daniel, Derictor, and Henokh inside the room, and no walls, no floor, no ceiling, no furniture were present.
Paradoxically his eyes could clearly see all of the walls, furniture, the ceiling, the floor, all four of them, Steven, and Anderson.
Probably for the first time he could almost understand what it felt like to be a normal person.
He would completely understand that if Daniel, Derictor and Henokh weren't in the room as well.

He remembered, it's not just with the modified Soargun phased laser array crystals, it's also with Anderson.
He felt it when he was with Anderson, everytime they, that are either he, Ein or even Hendrik, meet Anderson.
And they rarely meet Anderson ever since Hendrik joined as a BAIK Agent about four years ago.

“ Hendrik? What are you looking at?” Steven gazed at the tattoo, but said nothing about it.

“ Manov here,” he didn’t say that.

“ Nothing, just, this room was very..., uh, clean,” he said.

“ Of course, a doctor's room should be clean, and sterile.
You don't want your patient to catch an infection, would you?”

Henokh tried to wake Anderson, and Anderson opened his eyes, then put his gaze at Henokh.

“ You are here,” Anderson stated.

“ Tell me, did you meet Aditya?” Henokh inquired without a pause.

Anderson took a moment to think,

“ Yes I did.”

“ The bomb, was the bomb originally intended to hurt me?”

A tingling, burning sensation glared out from Anderson's stomach, and spreads to his temples and to his entire body.
He maintained a calm gaze to Henokh, and went silent for some time.
Then he replied,

“ Supposedly”

“ Were you hurt?” inquired Derictor immediately, then gazed at Steven, “ What with all of the bandages?”

“ He got stabbed,” Steven paused, “ with that knife” he pointed at a clean zip plastic bag containing a ceramic knife head soaked in milk-like liquid, and a clean break point near where the handle should be.

“ Who stabbed him? What's the milk?” Derictor continued.

Daniel wasn't sure whether or not he would disclose the information.
He only gazed to Steven, with hope that Steven would disclose that instead.

“ Was Anderson's blood, yes” confirmed Steven, “ and the stabber, were, Aditya, I believe? Anderson?” he turned his gaze to Anderson.

“ Yes, Aditya did it.
And apparently, he's backed up with The Sea Serpent.”

Henokh and Derictor nodded, Daniel and Steven still intrigued with the unusual usage of particle 'The', as if they knew about it already.
Problem was, that denefasan's arrival on Earth was a new thing for Steven and Daniel.

Manov took the bag, and inspected the knife.
He couldn't feel it as well, albeit touching it and sensing the texture of the bag.

“ It appears like a milk to me” Manov added, “ Why?”

Daniel wanted it so much to tell them that Anderson was alien, but he didn't even sure that he was allowed to spread the word.

“ Well, he's an etoan superior, not a human,” Steven said.

“ He had different skeletal and muscular arrangements from a normal man,” Daniel bursted out the information subconsciously.

“ I'm a White Elf, or called as such by your kind, Manov” Anderson added, his eyes shut now.

“ Manov? He's Hendrik right?” asked Daniel, Steven agreed and gazed at Anderson as well.

“ I'm Manov, Hendrik's double.
Hendrik's busy right now on the bombing scene.”

A White Elf, that word was recognized by Manov, as the fabled Elven of The South Pole.
He never meets any of them, and certainly didn't recognize Anderson as such when they first meet.
They come from a world beyond the Shamayim, or Heaven.
It probably explained why he couldn't feel Anderson, because Anderson was not an Earthly being.
Then, Steven and his car weren't from Earth as well.
And the room...?

Manov decided to keep it to himself.

“ I'll explain later, ko Steven” said Manov.

Henokh called upon Oneiric Protocolers and they walked Anderson to one of their cars.

“ See you soon, guys” Daniel greeted them cheerfully as they left.

All reciprocated, but Henokh.

“ I wish not very soon,” commented Henokh, Daniel turned a sharp gaze to Henokh, and Derictor pushed Henokh's right elbow with his left.

As they boarded the other car, Derictor couldn't help but comment Henokh's behaviour.

“ What was that?”

“ What was what?” responded Henokh.

“ Daniel's trying to be friendly with you.
I mean, come on, it had been years and I believe Daniel's past that as well.
It's his family's fault, not him.
I know that his father betrayed your mother but-”

“ It's not about that anymore.
Was going to make up with Daniel when I learned that...,” Henokh paused.

“ ..., That what?”

“ That he's with,” Henokh was disgusted just by the very thought of him, “ he's with Fernando.”

“ But you were  good with Fernando.
What's the problem?”

Henokh took a moment or two to think.
Frankly he didn't even sure why he hated Daniel.
And then he came up with a decent reasoning, or so he thought.
In fact he wasn't even sure whether the reason was valid enough.

“ Fernando's a good friend of mine, couldn't help it.
Daniel isn't, he,” Henokh paused, “ ..., corrupted Fernando.”

“ That's it?”

“ Yeah, yes that's it.”

“ That's your best excuse? Hah,” Derictor broke eye contact with Henokh, he gazed at the road.

“ Guys, couldn't you stop talking about that?” Manov interrupted from between the two.

He decided that he couldn't withstood to be seated in the middle while two of them in his side arguing with each other.
He also pointed out toward the front seats.
The two Oneiric Protocolers on the front seats weren't exactly comfortable listening to them.
Henokh and Derictor turned silent.

Derictor gazed at the roadside again, “ you wanted to make a world free of discrimination, yet you yourself couldn't hold yourself together when facing Daniel, and Aditya.”

Henokh's face was as red as a rotten tomato.
His breaths were short and Manov believed he sensed escalated heartbeat and blood pressure in Henokh's body.
With a steady gaze toward the windshield, Manov also sensed tears built up in Derictor's tear glands, but not enough to make a single tear drops.
He imagined that Derictor's eyes were watery.
Concluding that it wasn't exactly a life-threatening situation, Manov did nothing.

## The Second Coming

“ Henokh's a jerk” said Daniel, “ I don't understand why he hated me.
I mean, well, yes we had family issues, my father mortgaged his mother's house and failed to redeem it.
But I believe our family solved it up eventually, and we were  supposedly in a good term ever since.
But why Henokh still hate me?”

“ Let's put that aside.
At least you tried to make peace with him.”

“ No, Steven.
I mean, did you see that? He ignored me just a moment ago.
For the entire time he spent in your room, never did he acknowledge my presence.
And, and that time, when we were to say goodbye to them? Did you hear what he said?”

Steven looked past Daniel and noticed a young man with spiky white hair and bright skin tone, wearing a gray jacket and bright blue leather pants.
By appearance and facial features one would guess his age to be at most a couple years below twenty, or even lower, probably around fifteen.

“ Grandpa!” Steven waved his arms.

Daniel turned and discovered the man, squirted his eyebrows.
Last time he meets the young man was about twenty years ago, and he was around ten back then, Daniel believed.

“ Grandpa? Grandpa Ostau?”

“ Niel! Steve!” the young man shouted in delight, he ran toward them.

The enthusiastic, energetic and cheerful gait was certainly of high school student gaits, but accompanied with coordination of what Daniel believe to be the coordination of an experienced athlete.
Either that or the coordination of an older man.
His gaze betrayed his apparent youthful body for a slight fraction of a second, Daniel believed he could see the eyes of an extremely calm old man, full of wisdom and experiences.
Then it lost into bright, childlike youthful gaze.

He hugged Daniel and Steven affectionately.
Daniel could feel by the pressure exerted by his arms that it was an energetic hug of a youth.
He remembered how Fernando used to hug him that way, when he was on his eighteen, or was it twenty? The smell of this particular young man definitely belong to a youth, Daniel believed.
Older men smelt different, he thought.
He returned the hug and took the chance to examine the young man's body.
The skin was tight and smooth under his attire, wrinkle-free and evenly toned.
The heartbeat belong to that of a young boy, generally fast-paced compared to older men.

The body's definitely that of a young man, probably less than a decade post-puberty.
But the gaze, the gait, the intonation of his words, occasionally betrayed his body's age.

At least Daniel's estimate was correct, though of different order of magnitude.
Ostaupixtrilis Pontirijaris, the young man, was about fifteen..., times a thousand years old.

He was born even before human civilization sparked into being, had walked on Earth countless times as a tourist from time to time.
He liked anything about Earth, and how the inhabitants evolved into striking resemblance to etoan natives.
He knew Earth and ancient civilization more than what an archeologist ever dream of.
He never missed an opportunity to be on Earth again.
He never even missed any virtual real-time show of Earth, and explored every bit of it remotely when Earth's administrator decided to ban further visa application to Earth, five millennia ago.
And just about seventy years ago, when Earth's tourist visa application opened again, he was among the first wave of applicants to settle on Earth.

And a policy change occurred in his present stay on Earth.
He was allowed to procreate, as etoan decided to make a permanent settlement on Antarctica, the city of Aucantica.
He didn't hesitate to set new records on his new citizenship.
He married Tudva to be the first etoan marriage on Earth.
Then Tudva birthed his son, Johannes Pontirijaris, practically the first etoan natural birth on Earth.
Then Johannes be the first to actually interbreed with a human woman named Rachel Sugianto, who's the first human that actually married an extraterrestrial (obviously all previous claims were false).
Their twin children, Alexander Pontirijaris and Steven Pontirijaris be the first human-etoan twins, breaking two records at once: First human-etoan hybrid, and first etoan-descended twins on Earth.
Even if that's not enough, Rachel bear the honor of being the first human to carry human-etoan hybrids, and delivered them successfully.

Obviously the sequence of firsts were eventually exhausted, that when Steven married Helena, and birthed his four (also hybrid) sons, it was a commonplace event already among etoan on Earth.

“ So you were  actually Grandpa Ostau, aren't you?” Daniel asked in slight disbelief as they walked toward a guest couch.

Ostaupixtrilis still had his arms on their shoulders.

“ He's Grandpa! He did recognize you,” Steven answered, then turned his gaze at Ostaupixtrilis, “ right? Grandpa?”

“ Can you please,” Ostaupixtrilis untangled his arms from their shoulders, “ not calling me Grandpa? It felt weird, especially with my current biological age,” he sat on the couch, gazing them.

Steven and Daniel gazed at each other, then to Ostaupixtrilis.

“ Then what should we call you?” Daniel inquired as he sat on adjacent single couch.

“ I've picked up a new name for my present incarnation.
Ashton, Daniel Ashton!” Ostaupixtrilis smiled wide, revealing his teeth, and surprisingly human-like gums.

Daniel shook his head, his right brows leaped up.

“ Ehm, why Daniel? I mean, are you copying my name?”

“ No, of course no.
Daniel means God is my judge, and Ashton means from the ashes.
I want to start over and be a better person, so I picked Daniel, to remind me to always do good things.
And because I'm starting over, and with a new body, it was like a phoenix, that rise from the ashes.
I rose from the ashes, so Ashton.” Ashton's gaze turned cold, calm, calculative, and full of determination, and then the gaze meets Daniel's, “ If you like to prevent confusion, then feel free to call me Ashton instead.
It's not a surname after all.
My surname was still Pontirijaris.
I just dropped it on my present Indonesian birth certificate, just to start a new.”

Daniel felt like the gaze reminded him of how old and ancient was the individual he's talking with, disregarding his biological age.

“ So why you came here? Grandpa- er, Ashton?” Steven paused, already seated himself on Ashton's side, “ er, mind you if I call you Ash instead?” he said with a softer voice.

“ Yeah, you can do that.
Cool isn't it? Steve, Niel, and Ash” Ashton cheerfully pointed his index finger to the name's possessor.

Then he continued, “ I came here because I'm worried about you guys.
You see, Etovexiri just announced, er, confirmed that Denefasan's here on Earth.
Even more, there's sighting of a sea serpent on Kendari, on your city.” he paused, caught a breath, then his eyebrows raised up, “ And then Andre got hurt, like, for the first time he got terribly injured! You wouldn't believe how it's a hot news in Kavnet! A superior, wounded by a normal, human man!”

Ashton turned his gaze to Daniel, “ Ehm, Niel my son, can you follow our conversation?”

“ Eh, yeah, sort of,” Daniel said, and his confidence kicked in, “ I treated Uncle Andre as well you know, got a brief explanation by Steve here about *Etohibas* and how they were  better at practically everything compared to normal man.”

“ Etobihas” said Ashton.

“ Excuse me?” replied Daniel, slightly frowned of confusion.

“ It's Etobihas, not Etohibas.
But the rest was good.
Steven haven't mentioned it, I believe, but we sometimes call them Tobias.
Get it? They sounded similar.
Probably would help you to memorize the name.”

Daniel nodded, his fingers swiped his thin beard on his chin thoughtfully.

“ Point is, good to know that you were  good!” Ashton turned his head slightly, as if trying to recollect what he was going to say, and returned the gaze to Steven and Daniel shortly after, “ Anyway yesterday night I downloaded some of my older memories from Kavnet, just been delivered from my original copy on Aucafidus.
And guess what I discovered?” Ashton continued.

“ Pardon me, but was this Kavnet some sort of alien equivalent of internet?” Daniel inquired, he gazed at Ashton and Steven alternately.

“ Yes, and no.
It's some sort of internet, but better, far better.
Different protocol, vastly different in order of magnitudes in terms of greatness, service quality, and content, I must say,” explained Steven, then he turned his gaze to Ashton, “ What did you get, Ash?”

“ Please guess, you'd be surprised!”

Daniel's in reality, clueless, but he hoped that he could keep up with Steven and Ashton.
He did hear Ashton said that he's in a new incarnation this time.
Probably that's one way to look on new body printing, a new incarnation? Daniel's slightly excited as he believe he could work it out, and he worked out an answer,

“ Was it, uhm, your memory from your past life?”

“ Close enough!” Ashton exclaimed.

“ Actually, pretty accurate! So yesterday I downloaded it and had my exoself integrated it, merged it with my current memory.
And found out that I was here on Earth two thousand years ago during which the Earth's administrator banned tourist visa on Earth.”

Daniel's ecstatically excited to know that he got it right, so he decided another shot to keep up the conversation with a real alien.

“ So, what did you do? Back then on Earth, two thousand years ago?”

Ashton smiled, “ couldn't you guess it? C'mon, it's a huge news!”

“ I'm clueless, honestly” Steven added.

Daniel felt a little snob this time, that Steven's clueless.
He thought really hard about what big thing happened two thousand years ago.
He thought really hard, and he thought, and he thought.
He's now the one that's clueless.

Ashton had their face read well and concluded, rather accurately that they were  clueless.
He was annoyed and slightly disappointed, and decided to give away a clue, his tone lowered and plainer,

“ I was tortured and crucified.”

“ What?” said Daniel and Steven in chorus.

Steven was honestly clueless, and surprised by the word torture, and had no idea what crucified means.
His response was honest, a single 'what' because he actually wanted to know.
He imagined a group of savage ancient tribes of Earthmen stoned his grandpa, broke his bones, sliced his skin, et cetera.
Then his exoself returned definition of crucify from human net, it was this: put (someone) to death by nailing or binding them to a cross, especially as an ancient punishment.

Daniel in the other hand, understand it very vividly, both the word torture and crucify, and what it implied had happened two thousand years ago.
It was the story, long time ago Daniel struggled on working its authenticity out, or rather, asked himself repetitively, if the story was true or just a fable or a heroic story.
It was the story of Son of God, that lived among men.
That faced trials by the devil when he fasted on a desert, that promised salvation, and did miracles, betrayed by one of his students, tortured, slain, and crucified.
Then he went to the world of the dead, saved the souls and by the third day rose from the grave.
It was a decent story, really.
A story of hope, inspiration, love of God to humanity, and salvation.

Daniel couldn't worked it out as a true story up to the moment the clue was provided by Ashton.
It was almost like a definite proof that the story was true.

Meanwhile Steven worked it out from the usage example, that it was what were believed to happen about two thousand years ago.
Its usage example was this: "two thieves were crucified with Jesus."

“ Were you Jesus?” Daniel and Steven asked in chorus again.

Ashton's face leaped up and down in excitement, he giggled enthusiastically, “ Correct! Correct! I was him, I was Jesus!”

“ How come?” Daniel asked.

“ Well, it was a request made directly by, our Archgod, Kavretojives, well, the first being from our civilization that passed the third, Transcendence Scale.
Pretty big figure I must say.
We mortals usually rated at very close to zero.”

“ Kavretojives asked you directly? You mean, personally?” Steven asked.

Ashton nodded, “ Yes.
An avatar of him came to me.
He said it was important, of course I comply with the request.
I mean, it's rare for beings such vast and powerful as Kavretojives personally requested a mortal like me, right?”

He let his words sank on their thoughts, then continuing,

“ That time, was the first time I encountered a war, in the front line,” he paused, “ It wasn't like the way you might imagined it, Niel.
It was a memetic war.
War of ideas.
The world was in chaos, Earth-based Old Deities, or Powers, or Lords, or gods.., was in a war.
Civilians were confused, new religions formed and faded.
Mankind was at a state of confusion.
My task was simple, to be a vector, a focus of a new memetic figure.
An influential figure.”

“ To pretend to be a deity?” Daniel's curiosity erupted, his intention wasn't as harsh as his wordings.

“ Depends on how you see it, I was an avatar of Kavretojives, and at the same time was a man, er, etoan.
My act was a direct act of Kavretojives.
He had his own intention I believe.
I believe whatever it was, it worked, however degraded my teachings -his teachings evolved within just two millennia.
Humanity now had this common belief system, belief system that'd benefit them when Kavretojives decided to harvest mankind, to introduce them into the civilized galaxy.”

“ Wait, what do you mean by Earth-based Old Deities? Were they gone? All I get was that Kavretojives took control afterwards.” interrupted Daniel.

“ The deities, they were  still here.
It's just that they turned silent toward mankind.
They just let humanity be, all the rules had been set already by the accord between them and Kavretojives' agents.
They ignored humanity entirely, er no, they becomes silent observer of humanity.
The fact is, they haven't had the interest to micromanage mankind.
Mankind was uplifted and guided by El, and El's dead for at least several millennia now.”

“ El? Elohim you mean?” interrupted Daniel again.

“ No.
Elohim means Sons of El.
They were  still here.
El, The Most High, was dead.
He was killed by other Powers, er, Deities, in a coup.
After his death, apparently his successor decided not to bother humanity.”

Steven wasn't able to keep up, and felt like Ashton digressed.

“ Wait, I lost you just now.
What was your point of telling us that? That happened very long time ago, right?”

Ashton gazed Steven, his eyes full of determination and frighteningly deep.

“ The problem is, that means Earth was supposedly a sovereign polity, let by the current Divine Council of Elohim.
They had everything in their power to stop Denefasan.
And they didn't.
If Denefasan's intention was to take over Earth, supposedly the Divine Council would be against it.
The fact that Denefasan could penetrate Earth's defense means at least one of the Powers granted them permit to enter Earth.”

Steven's brows wrinkled,

“ A Power decided to let Denefasan enter Earth? Why would they want to do that?”

“ What can we do about it?” Daniel could feel a cold sensation crawled on his back, he rationalized that whatever bad things that are going to happen, it must be long after his lifetime, but at the same time Ashton's gaze implied the otherwise, “ Wh, when will it happen?”

Ashton broke eye contacts with them, he closed his lids, and his eyes darted under his eyelids.
He opened it again, and one could tell that he concerned Earth's future by his gaze alone, “ I don't know which Power wanted to do it, or for what reasons.
I don't even know what can we do about it.
But I do know, that it might actually happen in our lifetime.
If I were to be allowed to enter Earth again, that means I was about to fulfill my promise, er, Kavretojives' promise.”

Daniel's tongue froze in fear, his heart drummed like it was about to jump out of his chest.
Sweat drops formed around his temples.
His mind knew where this conversation leads,

“ The second coming?”

Ashton didn't say a word, he shut his lips tight.
His gaze penetrated through Daniel's soul, then he nodded reluctantly.

Deep inside his thoughts, suspicion arises on which Power was behind all of this.
He had two candidate for the culprit: it was either Hadad, the son that rebelled against his father El, or Yam, sometimes associated with the Fallen Angel, the morning star.
However he decided to keep it for himself.
