# Incursion

"The first thing we need is something to find funding and to procure infrastructures for our operations," said Henokh.
The marker on Henokh's grasp danced around the board.
Figures, and diagrams it conjured, followed with some scribbles here and there, and some more schematics.

"My family business, the Kuker Group, can only afford so much, but our scale of operation would need to be much bigger," Henokh finished scribbling.

Anderson rose off the seat and inspected the scribbles on the whiteboard, "Securion Incorporated International can help in that. We were planning to expand to Indonesia, and we can invest in the Kuker Group."

Derictor observed the whiteboard, he thought hard, "so, we are going to make the world a better place, by making companies?"

"Exactly," Henokh waved at the whiteboard, his gaze aimed to Derictor, "those companies would be the one that provide us with infrastructures. Our frontends."

Derictor nodded.

Anderson seated himself, he held a hand on his chin, "it would be hard to develop large scale hidden bases, or develop infrastructures in a low-profile manner. It would draw attention to us."

"So," Derictor interrupted, "if the infrastructure is built for the purpose of the companies, it would not attract attentions."

Henokh shook his head, "more like, we don't have to explain ourselves."

"So, what kind of operation are we going to do anyway?" said Derictor.

"Networking," said Anderson.

Henokh nodded.

Derictor's eyes darted at Henokh, and then Anderson, "are we a multilevel marketing group or something?"

"Control over people is vital," said Anderson, "we control people, we control everything."

"Even reality," said Henokh.

An eyebrow jolted up on Derictor's face, "I believe many have done that before. Does it work?"

Derictor gazed at Anderson and Henokh.
He hoped any of them turned to their senses.
He was wrong, not because they did not return to their senses.
He was wrong, because he did not know where his senses should be.

Until Henokh bursted in laughter, then Anderson followed.
"I'm sorry, I'm joking," said Henokh.

Derictor didn't laugh.

"Not funny, huh?" said Henokh, his smile faded.
Awkwardness filled up afterwards.

"It wasn't entirely a joke," said Anderson, "we're doing networking, just not the same way as multilevel marketing. Perhaps a more accurate term would be, dispersion."

"That's completely unrelated to networking," said Derictor.

"Several departments, running separately from one another," said Anderson, "contact with one another was done solely on the need to know basis."

"That way, we would less likely be identified as a single group," continued Henokh, "and here's the networking part: each department would maintain networks of connections with key people in their own respective fields."

"That sounds more like it," said Derictor, "I'm in."

## New recruit of yours

Midday, the sky was clear and bright.
It was too bright, in fact, that Derictor had to squint his eyes just to be able to observe the road.
Just ten minutes prior, he was in his comfortably dim office, at the fifth floor of the mall.

Kang Hae-In, who was sitting in front of him, sipped on his iced strawberry milk tea.
His face was relieved for a brief escape of the hellish weather at that particular day.
A moment after he squinted hard, "*Apayo!*"

Derictor laughed hard, Kang Hae-In was so hurt he exclaimed in Korean.

Pounding the back of his neck with his face squinted, Kang Hae-In looked at Derictor, "Brain freeze!"

"Who told you to drink it that fast," Derictor's grin grew wider before he realized it.

Kang Hae-In massaged the temple of his head, and massaged the back of his neck, "I guess we're getting old."

"Nope, I'm still in my twenties, you're over forties," then Derictor took a big gulp of his vanilla milkshake.
Derictor, was in fact, just one year short from his thirties.

When Derictor had his own moment of brain freeze, Kang Hae-In laughed very hard that everyone around them turned.
Derictor turned his face, he blushed red and want to disappear that instant.
He couldn't just disappear, "S-so, where's this new recruit of yours? It's off by ten minutes already."

"She'd be here anytime soon," Kang Hae-In had to squint his eyes to scan the road, the bright sunlight wasn't very forgiving to them, "oh there she is."

A young woman, with light brown skin, and curled black hair, rushed to the pavilion Derictor and Kang Hae-In were on.
A very decent light gray blazer over her bright black shirt accompanied with her dark brown skirt really suited her, Derictor thought.
The outfit highlighted her natural skin tone even more.
Before he knew it, he was standing, his hand was with her hand, and they exchanged glances.

"Um, I'm Nurhayati," she blushed, but didn't break the eye contact with Derictor, "and you are?"

Kang Hae-In had to slap Derictor's head with a magazine, that Derictor realized they had been shaking hands for about half a minute by then.

Derictor broke the eye contact to gaze at Kang Hae-In, that with his eyes told Derictor to resume their eye contact.
Derictor returned his gaze at her shy gaze, "I'm Derictor, Derictor Wijaya."

Seven years since two thousand and nine he had been with the Intelligence Agency that Henokh and Anderson instituted, never did he think that someone like her would join.
Kang Hae-In did say that she was a member of a drama club and a paramilitary club of a university south of Surabaya, and that he picked her up for her acting and people skill.
Most importantly, her paramilitary training and her mastery over five languages.
A perfect infiltrator, Kang Hae-In said.
Kang Hae-In, however, never mentioned that she was beautiful.

## Ah, there he is

"Tomorrow's mission is quite straightforward," Kang Hae-In started their briefing, "to infiltrate this new movement that the Department of Surveillance managed to discover.
They seemed to be related to the Surabaya Bombing last year.
Our target is Aditya, Aditya Wijaya."

"Derictor here is a member of the fourth Division, Logistics.
He will assist you in this mission," Kang Hae-In turned to Derictor, and resisted a laugh, "for all purposes and intent, he's your chauffeur." Kang Hae-In's laugh finally bursted.

It was Derictor's turn to slap Kang Hae-In's face with the wrinkled magazine Kang Hae-In used to slap his head before.
Nervous with Nurhayati's response, Derictor caught her gaze, their brown eyes meet each other.
Barely a second passed, they broke their eye contacts, both were blushed.

Kang Hae-In scrubbed his forehead, red because of the magazine, "Anyway, I spoke with Djun," his other hand pointed at Derictor, "his boss.
Among all of us, only Derictor and Henokh had actually meet Aditya, I need him to be able to identify Aditya, in case he attends tomorrow's gathering.
Henokh must not join, he's in the Department of Coordination, and he shouldn't be doing field works like this."

"Isn't there...," Nurhayati was reluctant to continue.

"Yes? What is it? Just say it, I don't bite, HAHAHAHA" Kang Hae-In let out his laughter as if it was the last breath he had.

"I mean," Nurhayati collected every ounce of courage she had, "isn't there any photographs of him?" continued Nurhayati.

"We had his photographs, in our phones," said Derictor, "which, ever since our last contact with Aditya, through a series of misfortunes and accidents, we lost them all."

"Derictor's phone fell to a lake, Henokh's fell from his apartment's terrace, and the photo album that contained their pictures got accidentally thrown off by Henokh's maid," spurted Kang Hae-In, followed with another laughter.

"You..., don't have to explain in details," Derictor's face turned red.

Kang Hae-In's expression turned serious in an instant, "oh yes, other than you two, you'd be assisted with our senior infiltration officer," his smile grew as he faced Derictor, "Anderson."

It had been years since Derictor last met Anderson, the guy with crocodile eyes.
Anderson did not wish to be in the administrative duty at the Department of Coordination, where Henokh was stationed.
Instead, Anderson was gladly accepted by the Department of Field Operation, and was assigned under Kang Hae-In, the head of the first Division, Frontline Responder.

Never did Derictor observe Anderson in action ever since the Intelligence Agency was instituted.
Derictor's first thought when Kang Hae-In said that Anderson was their senior infiltration officer, was that Anderson's peculiar features would be very hard to hide.
How would you hide his peculiar pale skin tone with hints of dark dermal layer, silvery white hair, pointed ears, a very big and muscular posture, not to mention his crocodile eyes?

"A-a senior infiltration officer?" said Nurhayati, her gaze quivered.

Nurhayati being shy, was considered cute by Derictor.
Her demeanor, her gestures, all screamed for Derictor's attention.
Or it could be that Derictor's attention screamed for Nurhayati's attention.
Derictor could not distinguish them at the time.

"Don't worry, he is literally a very kind guy. Ask him anything if you ever need any help," said Kang Hae-In, then something piqued his attention, "ah, there he is," Kang Hae-In waved to an approaching young Chinese-Indonesian man, "Anderson."

A bang could be heard, everyone at that premise jolted their gaze to the source of the noise.

"WHAT?" Derictor found himself standing with his chair lying on the floor, his hands ached as they slammed the table.
He found himself gazed at the young Chinese-Indonesian man, and him being gazed upon by people around them.

## Ernest Winston

Anderson stepped out of the car, scanned around the premise.
It was not an impressive convenience store, almost drowned by the food court in front of it.
There were not many people around the store that particular morning.

Derictor came out from the driver's seat, and from the front passenger seat, Nurhayati looked around.
Derictor opened the door for Nurhayati, at the same time Nurhayati opened the door for herself.
Both were startled, as Derictor almost lost his balance because of that, and their gaze caught each other's gaze again.
Blushed, they broke the eye contact again.

"You, can come out now," said Derictor, he attempted to look at Nurhayati, but his gaze wouldn't aim for her.

Nurhayati came out of the car, her face turned in a hope that Derictor wouldn't caught her blushing, "is this the right place?"

Derictor wanted to answer, but his voice was stuck at his tongue.
Anderson answered instead, "yes, we just have to go to the third floor."
Anderson scanned the entrance of the convenience store.

"Look," said Anderson, "there's a non-functional escalator right at the left corner, beside the cashier."

Anderson walked straight to the non-functional escalator, along with a number of visitors.
Nurhayati and Derictor tailed Anderson.
Derictor couldn't believe it at the time, that the guy they were tailing, was Anderson.
The huge pale and white-haired crocodile-eyed guy, was in the form of a normal young Chinese-Indonesian boy, looked very lean and fragile.
Derictor was certain that Anderson was taller than him when he last met Anderson.

"Anderson?" asked Derictor.

"Yes?"

"Anderson Pondalissido?"

"Yes, why would you ask?"

Derictor didn't answer that, he just look at Anderson.
Anderson looked back at Derictor, "Oh, this body? Yes I can change my body physiology and control the growth and regression. It took me days, but I can transform to this form, to any human form, and my original form."

"How?"

"I told you, I'm an *Etoan superior*," Anderson returned his attention to the staircases.

Nurhayati didn't say anything, but her attention was drawn to a name listed at a brochure the greeter boys provided.
Derictor peeked at her, and she was checking her phone, typing something from the brochure.
*Did she find something interesting in the brochure?* thought Derictor.

What came after, surprised Derictor.
She was no longer the same, shy lady.
Full of confidence, and emanating a completely different aura, a lively young adult, with glares on her eyes.
She was in her acting mode.

No wonder Kang Hae-In said she's a perfect infiltrator: she could easily switch between personalities, blend into various social situations.
According to Steven Brown, acting causes a loss of deactivation in the precuneus of our brain, essentially a deliberate process of possession: a substitution of one's identity by the persona they choose to embody.[@BrownEtAl2019, p.16]
Her identity faded, while the persona she assumed surfaced, and people interact with that persona as if it was her.
Any stranger wouldn't be able to differentiate it, and to the ones that know her, it was like interacting with someone else that was inside her.

Derictor's thought process was interrupted by a poke from an old man right beside him.
Derictor gazed to him, his eyes wide open, and the old man started to speak, "don't use your cell now, and focus on the preacher."

Ernie Windiana, her birth name was Ernest Winston.
There was a certain grace in her gait, a sophisticated woman indeed.
The change of name was done because she felt uncomfortable with her name, as if it wasn't her at all.
Derictor thought, *of course she'd change her name, why would anyone want to name their female baby as Ernest Winston?*
It took Derictor a certain moment until he realized that she had an adam's apple.

"Oh," Derictor's voice came out before he realized it.

"Sshh!" shushed the old man beside him.

Derictor couldn't focus to whatever she was saying, ever since he realized the fact that her gender identity is the opposite of her biological gender.
Gender Identity Disorder is described as a persistent identification of the opposite sex by an individual, followed with a persistent discomfort of their own sex or sense of inappropriateness of the gender role of their sex.
The label was renamed to Gender Dysphoria past two thousand and thirteen, to remove the stigma associated with the term "disorder," and its diagnosis was removed from the sexual disorders category to a category of its own in the Diagnostic and Statistical Manual of Mental Disorders, the fifth edition (DSM-5).
Since it was no longer a disorder according to the DSM-5, the treatment involves supporting said individual into transitioning of their gender identity.
That was in an ideal case, which was not the case in a highly conservative and religious society that belong to a certain abrahamic religion group.

Derictor didn't know any of those, he was naturally surprised that he thought about all of those.
It was a short moment after that he realized, that those trains of thought came from her speeches, that he unknowingly followed through.
Derictor shook his head, and started to focus up on her preach.
It was mainly about "humanizing humans," as some people unknowingly "dehumanize humans."
It was about how, as people blinded by dubious, irrational standards, they focus only on the outliers, and marginalized minorities.
That being in a minority group, seemingly gave the majority a right to scold them.
It wasn't right, but it was the rule of majority.
In the end, it causes unnecessary pain.
It nullified the right of minorities to be themselves, and to live like the rest of the society, with respect and dignity.

Derictor's attention was caught by a name she mentioned, of how Aditya Wijaya approached her, "he told me, that I am worthy of love, and be loved. That I am as human as anyone else."

By Aditya Wijaya's mention, everyone gave their praises.
Their saviour, their role model, their leader, and many more honorifics they gave to the man known as Aditya Wijaya.
Derictor had his attention fully focussed, to obtain any relevant information about Aditya.
However one young boy approached her, whispered something.
She calmed down the mass, and wrapped up her preach.

## Surprisingly good

"So you managed to approach him, uh, her?" asked Derictor.

"Y-yes, yes I did," said Nurhayati, taking a sip to her iced green tea.
She was back to her shy self.

Derictor stirred his Vanilla milkshake, "and what did you learn about Aditya's whereabouts?"

"Apparently she's a close friend of Aditya.
So, she did fall for Aditya, but Aditya rejected."
Nurhayati sipped her iced green tea again, she took a breath,
"Aditya had to take care of his foster son,"
Nurhayati caught Derictor's eyes.
Blushed, she darted her gaze somewhere else,
"Anthony, t-that young boy that whispered to Ernie."

Derictor observed Nurhayati wiping her lips with a piece of tissue.
Her lipstick was wiped off the lips area, advancing slightly to her left cheek,
"So, if we approached Ernie, he'll, um, she'll lead us closer to Aditya?"

"Y-yes, I can, she asked me to join her at a bar."

"I'm sorry but," Derictor took a clean piece of napkin and wiped her runny lipstick.
Nurhayati was petrified, unable to decide how to react.
She decided to just enjoy the process.

There was a moment between them, as their gaze caught each other.
The distance between them was closing, and someone's coughs could be heard.

"Uh, Anderson," Derictor was taken aback, he took his straw off his glass for no reasons.
Unable to decide what to do with the straw, he wiped the straw clean with a napkin on his hand, a napkin that was on Nurhayati's lips.
Nurhayati took another sip of her iced green tea.

Anderson giggled, "you humans are funny."

"We were exchanging information about Aditya," said Derictor.
Nurhayati nodded while continuing her sip.

"I got this," Anderson produced a brochure off his pocket.

"Legion of Umbra Cahaya, a support group," said Derictor, reading the insignia by the top left corner of the brochure.

"It was like that they spread at the beginning of the mass, look at the names," said Nurhayati, switching to her confident persona, and pointed at the bottom right corner, "Aditya Wijaya, and Anthony Matthias."

"This could get us closer to Aditya," said Derictor.

"Not 'us,' Nurhayati and I would approach them.
We need to be careful, and you're not used to this kind of job," said Anderson, "you're too visible.
Mistakenly refer to her as he that often, would annoy her.
At best they'd hate you, at worst, you'd be in their radar."

Derictor looked down at his Vanilla milkshake, and a wiped-clean straw in his grasp.
He put the straw back to the Vanilla milkshake.

"We need to disperse, you understand that.
So you could assume other roles later without being associated with us.
In fact, we shouldn't be sitting together here."

"But I'm your chauffeur," said Derictor.

"Now, yes.
Not necessarily later."

"But I am required to identify Aditya," said Derictor, he looked at Nurhayati.
Nurhayati was about to say something, but she reclined.

"Actually, I thought that too, but Nurhayati is surprisingly good, that we can establish which one is Aditya, and Anthony is one way to get to him," said Anderson, he smiled, "interesting that Nurhayati's success means your service is no longer required."

"But," Derictor attempted to formulate more reasons for him to join this mission.
He wanted to do it with Nurhayati.

Anderson seemed to recognize Derictor's intent, "besides, I need you to be somewhere else."

"Where?"

"Henokh.
You have to deal with Henokh.
The last thing we want to have is Henokh doing something stupid."
Anderson's gaze turned stern.
He did not waver, and the authority of the tone implied that the order was imperative.
For some reasons Derictor couldn't say anything back.
Derictor just nodded.

## To meet an old friend

"There is this growing movement. Observe the names of their speakers," said Derictor.

Henokh took the brochure from Derictor's hand, "join the Legion of Umbra Cahaya, where we would lead you to the world of equality, and peace, and freedom...," Henokh didn't continue it.
His gaze affixed at the name of the speaker, *Aditya Wijaya and Anthony Matthias.*
He turned to Derictor.

"I've tracked Anthony Matthias. Almost no record, but we got an acquaintance of him," said Derictor.

"Who else knows about this?"

"Well, I learned this when doing a mission Kang Hae-In assigned me to, with Anderson, and a new recruit Nurhayati," said Derictor.

"Let's meet this acquaintance of Anthony Matthias, together."

"Do you want me to inform Anderson? Anderson and Nurhayati will engage with our contact tomorrow, perhaps we can convince Anderson to let us join."

"No," Henokh said, he thought hard, "not yet.
Let's meet this acquaintance first.
It is not like it would jeopardize the mission."

"Are you sure?"

"We just want to meet an old friend," Henokh said, he nodded, "yeah, just to meet an old friend."

Derictor knew that this was exactly why Anderson told him to deal with Henokh.
Henokh can be irrational sometimes, especially when it is related to Aditya.
However, Derictor let it pass, as he wanted to do the mission.
He could not determine, that it was because he wanted to meet an old friend of him, or because he wanted to be with Nurhayati.

"Let's do this then," said Derictor.

## It was no longer a wind

Dominic Muerte paced his steps on the corridor.
Security guards at the entrance reported a man in black tuxedo and a black high hat barged in, with some kind of supernatural power that they described as Kanuragan Style.

"What do you mean by Kanuragan Style?"
Dominic inquired through his intercom, increasing his pace, that he was almost running.
From the background he could hear the guards screamed, and some sort of thumping voice.
Cracks could be heard, that Dominic assumed to be broken bones.

In between his breaths, the surviving guard explained, "You know, that traditional martial art, that uses inner-energy to fight," he panted, "t-that guy, we barely able to touch him.
Everyone coming near him, they're just...."

"Just what?"

"Expelled- oh no."

Dominic was sure that he heard the guard cried, " please, please, please, I beg you, have mercy-"

The next thing Dominic heard was static noise.

Dominic stopped his pace.
An intense feeling could be felt straight through his spine.
He recognized that sensation.
A fellow ORCA.

ORCA stood for Organized Regiment of Cooperative Associates, a gang of some sort, with several thousand strong memberships.
Led by a mysterious young man named Michael Guntur or M.G., that taught his members a unique martial art, known as M.G. Style.
He knew it, as he was a member.

Tier one of the style was known as the mastery of one's body, basically just like any normal martial art.
Tier two, *metapresence*, was the ability to sense and manipulate the environment with your soul.
Tier three, *metaforming*, was the ability to transform one's soul and project it to the environment.

The way the guards described that man's power was consistent with a Tier three fighting style.
Those guards had no chance of fighting him at all.
He tightened his blue denim jacket, and loosen up his muscles.
He prepared his stance, hoping that his M.G. Style wasn't rusty already, and that the techniques he learned from the EPL, would give him an advantage.

The man in tux slammed the door open, without even touching it.
Dominic recognized the man in tux, his junior in ORCA, "Kiel?"

Ezekiel Tanputra was a direct apprentice to M.G., and was known to be very proficient, despite being five years younger than Dominic.
Dominic was sure he could win the battle against Ezekiel.

"Brother, you're here," greeted Ezekiel.

"How's M.G.?" replied Dominic.

"Good, he's expecting you to be back with us."

"Huh, is he? Tell him to suck up the *WTF* guys first."

The WTF stood for the Watchtower Foundation, an esoteric multi-species government-like organization.
Its function was mainly to regulate the conducts of its members.
Its members consisted of beings that were considered capable to bear the so-called "true person-hood."
One of the requirements to bear true person-hood according to them was the ability to command the environment, instead of being enslaved by it.

Humans not capable of commanding the environment according to their will, or basically the remaining ninety nine percent of humanity, were considered feral to the WTF, and must be treated like any wild animals due to the lack of true person-hood.
EPL, on the other hand, saw that all beings capable of free will, must be treated equally.
Because of that, EPL was considered to be an organization that interferes with the welfare of feral humans, by introducing unnecessary unnatural intervention to the life of feral humans.

To the best of Ezekiel's knowledge, the bad guys would be the EPL.
The rule of the WTF was crystal clear for him, feral humans were to be left in their natural state, unless by their own free will and capability, able to transcend the limitations.
Triggered by Dominic's words, Ezekiel's eyebrows twitched, "I gave you a chance, leave those EPL guys, and join back to us."

"You know I wouldn't want to be back to the WTF infested ORCA.
ORCA wasn't the same anymore since those fuckers interfered."

With a move almost like a dance that ends with a firm stance, a firebolt jolted off Ezekiel's palm.
Dominic blocked it with a rotating kick that dissipated the fire before it even touched his body.
Dominic was surprised, he didn't expect a fire to come out, it wasn't in the M.G. Style curriculum.

"Watch your mouth," threatened Ezekiel, "your hate to the WTF doesn't warrant you the right to hurt our fellow ORCAs."

"Oh, the bombing?
It was just a start.
A price for sucking up the WTF guys," Dominic strengthened his stance, and launched a fist.

From his fist, a green beam screamed at Ezekiel, who raised a defensive stance.
The beam dispersed around, and cracked the concrete.
That move of Ezekiel was an authentic M.G. Style, Dominic reckoned.

"Surprised? I learned a few tricks from the EPL as well," said Dominic, his grin grew wide.

"I don't think there's any reason to bring you back to us.
Termination from this world is a more proper punishment for you," said Ezekiel.

Ezekiel charged toward Dominic, and with a firm palm he pierced through Dominic's defense.
Their skin barely touched, but the pressure was real, that Dominic was thrown aback.
A kick to the ground forced Dominic's own center of mass toward Ezekiel, while his palm prepared to pierce Ezekiel's ribs.

Ezekiel shouted as another palm blocked Dominic's.
A strong force was felt among the entirety of Dominic's skin that faced Ezekiel.
Before he knew it, he was midair already, speeding toward the wall behind him.

From his stance, Ezekiel lifted a foot and rooted it hard to the ground, before transferring his entire strength from the ground to his elbow, aimed straight at Dominic.
It was no longer a wind, almost like a surge of water splashed hard toward him and the wall.
The wall cracked and he was thrown hard several meters to the back.

The last thing Ezekiel saw before he fell among the spectators of the indoor concert he penetrated in, was a move, clearly was not a M.G. Style, reckoned Dominic, a move that allowed a sphere of expanding silence to cover the explosion.
Dominic could barely hear his own body thumped hard at the floor.
The pain, the shock, and the bone pushing against the floor through his own flesh and skin, was real indeed, absent the voice.
