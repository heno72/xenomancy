# Recruitment

> In-story date is 20210506 1400

Masih belum mengetahui apa tujuan mereka mencuri artifak itu, Hendrik memutuskan untuk mencoba menanyai Edward mengenai hal tersebut.
Kali ini Anderson ikut.
Mereka mengajak Edward pergi makan bersama.
Edward setuju, dan setelahnya mereka pergi ke karaoke.
Memesan beberapa minum dan sebagainya, Edward mulai menanya-tanyai mereka, kenapa mereka bisa saling kenal, dan seberapa dekat mereka.
Anderson sempat menegur Edward, dan Edward agak terdiam.

Selama perjalanan pulang Edward minta maaf atas perbuatannya tadi, ia tidak bermaksud macam-macam.
Hendrik, merasa iba mengatakan hal tersebut tidak masalah.
Edward mulai berbicara bahwa dia sebenarnya kesepian dan mungkin butuh teman.
Keluarganya saat ini sedang dililit hutang, isterinya meninggalkannya dan anak-anaknya cukup boros.
Dia bertanggungjawab untuk membiayai anak-anaknya, sementara dirinya sendiri harus menghabiskan waktu sendirian di rumah.

Hendrik kemudian mengarahkan pembicaraan dengan mengatakan masalah serupa, dia kesulitan untuk membayar beberapa hutang terdahulu, dan dia mulai mengatakan mengenai adanya kelompok orang yang bersedia membantu.
Edward mengatakan mereka memberikan visi yang baru, sejenis pencerahan.
Dunia itu kejam kata mereka, dan banyak pihak yang berusaha memanfaatkanmu.
Posisi kita sekarang tidak lebih adalah posisi yang diberikan agar kita terus menerus menuruti mereka yang di atas kita.
Jika kita juga mau sukses, kita harus bisa melawan.
Edward mengajak Hendrik ikut dalam pergerakan baru tersebut.
Pergerakan tersebut adalah pergerakan Umbra Cahaya.

Hendrik mulai menanyakan apa yang dibuat kelompok Umbra Cahaya ini.
Dia menjelaskan bahwa pemimpin pergerakan ini, Arman, mengajak mereka untuk berkontribusi ke pergerakan Umbra Cahaya ini.
Kontribusinya adalah menyediakan beberapa informasi kepada mereka, dan mengajak orang-orang baru untuk ikut pergerakan ini.
Edward mengatakan mereka mencari orang-orang dengan keinginan untuk merubah dunia.

Perlahan-lahan pembicaraan ini mengarah ke akan datangnya pembantu ke dunia ini, suatu pasukan mahakuasa akan datang untuk menolong mereka yang berhasil bertahan melawan opresi dan penekanan dari pihak-pihak berkuasa.
Umbra Cahaya sedang menanti masa tersebut.
Hendrik menanyai, kenapa bisa kau percaya dengan hal seperti itu, entah kenapa terasa aneh.
Edward melanjutkan, kau tidak mengerti katanya.
Kau tidak mengerti, mereka sudah ada di dunia ini.
Arman, adalah bukti bahwa pembantu itu telah tiba di Bumi.
Lebih banyak akan datang, dan Edward mau menjadi bagian dari dunia yang baru.

Edward mengajak mereka ke misa Umbra Cahaya minggu ini.
Dia bilang mereka akan mengerti maksudnya setelah mengikuti sesi ini.
Arman, he said, is very persuasive.
