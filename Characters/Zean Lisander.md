## Zean Lisander

| Label | Value |
| :---- | :---- |
| Name | Zean Lisander |
| Code Name | The Dream Caster / The Sandman |
| Gender | male |
| Birthdate | May 11, 2006 |
| Father | [Henokh Lisander](Characters/Henokh%20Lisander.md) |
| Mother | [Sylvia Lusien](Characters/Sylvia%20Lusien.md) |
| Brother | [Callan Lusien](Characters/Callan%20Lusien.md) |
| IQ Score | 118 |
| Position | [KIA MTF Gamma-42 "Four Horsemen"](Notes/KIA%20MTF%20Gamma-42%20Four%20Horsemen.md) |

## Description
A friend of David since fresh year of junior high school, and Ashton since high school.
A rather passionate christian that often ask David and Ashton into his church.

## Inspiration 20200618
What about Zean?
What is his interest?
I haven't explored that yet.
He's the son of Henokh, but definitely that's not the only identity he has.
Let's see what we know of him so far.
He's easily startled, very attentive, and want to prove that he's good enough.
I think, he'd most probably ending up as a computer science student.
