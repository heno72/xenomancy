# Martha Williams

| Parameters | Values |
| ------ | ------ |
| Name | Martha Williams |
| Birthdate | 17 June 1987 |
| Husband | 양승민 (romanization: Yang Seung-Min) |
| Son | [Anthony Matthias](Characters/Anthony%20Matthias.md) (양기환, rom.: Yang Gi-Hwan) |
| Daughter | [Michelle Williams](Characters/Michelle%20Williams.md) (양치민, rom.: Yang Chi-Min) |
| Occupation | Pharmacy Clerk of Sejahtera Clinic (of [Steven](Characters/Steven%20Pontirijaris.md) and [Daniel](Characters/Daniel%20Lusien.md)) |

She’s the pharmacy’s clerk. She was married at a very young age to an abusive guy. Her son was left in the woods by her husband because he believed that the son was cursed, and when carrying her second child, she divorced him. It was the most liberating decision and she continued her pursuit by enrolling in a graduate school, ended up in Steven’s and Daniel’s clinic as a pharmacy clerk. Her daughter is Michelle Williams.

Her personal arc is to resolve her crush with Daniel, and to find her lost son. Her obstacles is Fernando, and the fact that she knew so little about her long lost son.

## oStorybook
A pharmachy's clerk on Steven's clinic. She's particularly close to Daniel, and was interested in a romantic relationship with Daniel. She didn't know that Daniel is with Lusien.

She was abused by her previous spouse when she is carrying Michelle Williams, and her husband left her afterwards. She considered that moment as the most liberating moment for her. Ever since, her life went into a glorious future when she managed to finish her pharmeutical study and becomes a true pharmacists, currently working on Steven's clinic, with generous pay and a pension plan.

Her daughter, Michelle Williams, rose to be a daughter that lack her father's love, and grow to be an ignorant teen, but with deep artistic feelings. However her social skills aren't developed enough due to her own decision to isolate herself.

The eye-opening moment in her life is when she discovered the existence of the Sea Serpent, that while searching for further references, led her path to cross with Anthony Matthias. Anthony Matthias promised her the future where such abuse her previous spouse did to her won't happen again. To her relief, Anthony showed her her husband's hell (he died of an accident because his new, younger spouse sabotaged his car's brakes), that consist of her torturing and abusing him constantly, with occasional glimpse that she might forgive him, but just "might".

She married Anthony Matthias later, despite the fact that he's about the same age as David is (Michelle's boyfriend).
