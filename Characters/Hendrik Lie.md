# Hendrik Lie

| Parameters | Values |
| ---------- | ------ |
| Birthdate  | August 9th, 1995 |
| Real Name | Hendrik Lie |
| Code Name | Sukarno |
| Joining Year | *(check first)* |
| Position | *(check first)* |

## Basic Description
A chief and an agent of BAIK, code named Agent Sukarno.
He’s a reflexior , a part of Trinity Compact with other Hendriks (essentially the compact is a group mind of clones) as the owner of the body and the original copy.
He's good at martial art and decent proficiency at manipulation reflexius, especially on moderate psychokinesis and mind altering techniques.
Master of Reflexiology from University of WTF .
His human profession is as a lawyer, specialized in corporate laws.
His traits were brave, opportunistic, and indifferent.
Perhaps slightly eccentric by human standard.
His motivation is curiosity, and desire of self improvement.

His personal arc is to find and stop the Tiamat.
Also to resolve what his true desire is.
His true desire is to be recognized as a person, so he seeks approval, however his main obstacle is his own low self esteem due to his inability to satisfy his own expectations.

## Background Story
Hendrik was the only mage in his horizon of social circles.
Hendrik was just a lone reflexior that didn't have any direct lineage to reflexior society in general.
He's just lucky enough to withdraw a genetic lottery that grant him an activated reflexior cluster.
He's not registered in the WTF, or EPL, and he had to self-study his weird psychic abilities.

To explore his psychic abilities he studied lucid dreaming and astral projection.
One way to achieve that is via studying lucid dreaming, and eventually, astral projection.
And then, communicating with your meta, that is, the soul subsystem, usually the part that connects you and the RFL network.
Most reflexiors did reflexius act without thinking, but via lucid dreaming and astral projection, Hendrik is able to communicate to the intermediary layer between his physical body and the RFL.

He's quite isolated because he finds it hard to relate to other human beings, hence the loneliness.
Until his 2nd year, around July 2011, a freshman came by, his junior.
His name is Calvin Gauss.
Calvin came to notice that Hendrik is different, especially after Hendrik tried to communicate with one of his friend on why he's often spaced out.
In his conversation with Hartanto (Hendrik's classmate), Hendrik communicated about his secondary personality.
Calvin immediately recognized it, that Hendrik is describing the experience of having an exoself

Calvin realized that it was SOUL subsystem of RFL, and not everyone can perform it.
With the knowledge that SOUL subsystem is basically analogue to his own exoself system, he assisted Hendrik on mastering it.
Eventually, Calvin taught Hendrik the technique of soul shearing.
Their interaction would explore the nature of RFL, and its interaction with mortals, drawing a parallel to etoan society.
Even exploring the main difference between etoan and homo sapiens.

With the original intent of creating copies of himself to be his friends, he used the soul shearing to create temporary copies of him to do his deeds.
But eventually, his copies refused most of his requests.
Pissed off, he complained to Calvin, which was then furious, because he didn't taught Hendrik Soul Shearing so that he would create instant copies only to do trivial deeds in a disposable manner.
He said by doing that, Hendrik is disrespecting himself, and dehumanizes his copies, no wonder they refused.

Then he added that he has another mission as an ambassador here on Earth, which means he'd have less time to contact Hendrik.
And then two things happened:
1.
Hendrik reflect on himself on how he treats his clones.
2.
Hendrik realized that the one thing he wanted the most, a friend that could understand his conditions, is Calvin, and Calvin is going away.

After Calvin's departure, Hendrik faced another problems because of his own limitations, his colorblindness.
Despite all of his sixth sense, human education system requires him to have a colorblindness test, and he couldn't pass it.

In making his latest clone, Manov, he wanted that he's free of that limitations, so he doesn't have to face the same thing as him.
So instead of making a copy to fulfil his dreams, he's making a child that doesn't have the limitations he has, but does not pressure Manov to do what he couldn't.
Because he learned to humanize his clone.

And that's how Manov came to be.

Later in Hendrik's life, Manov went to study medicine and toured to various worlds in Paramundi Complex.
Then Hendrik had to face reality at workplace.
Everyone is after everyone else.
Survival of the fittest.
Hendrik had his own share of being taken advantage of.
Hendrik just have no idea how to say no.
His own meta that is never severed, is no longer enough to motivate him to stay on track.
Frustrated, he decided to create one more clone, that wouldn't have his hesitation to say no.
That would be upfront, and dare to protect what is important for him.

And Heinrich is made.
