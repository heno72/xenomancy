# Fernando Suryantara

| Parameters | Values |
| ------ | ------ |
| Name | dr. Fernando Suryantara, SpB |
| Birthdate | 11 Mei 1985 |
| Partner | [Daniel Lusien](Characters/Daniel%20Lusien.md) |
| Father | Bartholomeus Suryantara |
| Mother | *(not determined)* |
| Cousins | 강해인 (romanization: Kang Hae-In); 양승민 (romanization: Yang Seung-Min) |
| Occupation | Was Military Doctor of the Indonesia's National Army (TNI); Resident at the Sejahtera Clinic |
| KIA Position | Member of [KIA MTF Alpha-19 "The Archangels"](Notes/KIA%20MTF%20Alpha-19%20The%20Archangels.md) |
| Eyes | Black |
| Hair | Black |
| Height | 185 cm |
| Weight | 70 kg |
| Skin Tone | Asian |
| Body | Well built |
| Race | Human |
| First Love | Steven Pontirijaris |

Fernando is the first one among his extended family to study abroad.
And he is a member of the [HD Club](Notes/HD%20Club.md).

Fernando Suryantara enjoys visit to gym, martial art classes, and swimming.
He has no qualms on about any food, generally describing himself as an omnivore, but given choice he always prefers authentic japanese food, not cooked, especially sashimi.
Usually with an inhumane portion (relative to his friends) of wasabi.
He even enjoyed eating wasabi by itself.
For drinks, he loves tea and milk, he can even discriminate the taste of different teas and milks with stunning accuracy.

His hobby, is also in automobile and motorbikes.
He was surprised to know that Steven's great grandpa also knew a lot about the topic, along with a firm grasp of various martial art styles.
In term of fellow automobile and motorbike anthusiasts, and fellow practitioners of martial arts, he and Ashton are best buddies.

An avid biker as well, he explored many parts of Surabaya.
Designed after Own, a friend of mine, his tagline was: Surabaya 374.8m^2, dan kamu di rumah aja?
So apparently, assuming that:
- a viewing range of a 10 m uninterrupted line of sight to each sides (the truth is far from ideal).
- average bike riding speed of 18-29 km/h.
- at least ~1 hrs a day spent biking.
- he spent about 20% of his time in unfamiliar terrain, while the remaining 80% would be places he had explored (just an assumption for simplicity's sake).

Fernando would have about 25 km (averaged, (18+29)/2) route a day biking.
It would include a sight of 10 meters for each sides, therefore the width of his viewing range would be 20 m.
This equals 20 m * 25 km = 0.02 km & 25 km = 0.5 km^2 a day.
Every day, he'd explore about 0.1 km^2 of new terrains.

Therefore, he'd explore Surabaya in 3748 days.
That is 10 years, 3 months, and 4 days (approx).
How long has he been in Surabaya?
