# Tee Arthit Watthuprasongkh

| Parameters | Values |
| ------ | ------ |
| Name | ตี๋ (by his father, romanization: Tee); 弟 (by his mother, romanization: Di) |
| Thai Full Name | ตี๋ อาทิตย์ วัตถุประสงค์ (Tee Arthit Watthuprasongkh) |
| Birthdate | 17 March 2002 |
| Father | จันทรา วัตถุประสงค์ (romanization: Chandra Watthuprasongkh); sometimes called เก้า (rom.: Kao, nine in Thai), เขา (rom.: Khao, a mountain or a hill) |
| Mother | 吳脚踏 (romanization: Go Jiao Ta) |
| Brother | ต๋อง (romanization: Tong) |
| Language Proficiency | Indonesian, English, Mandarin, Korean, Thai |
| IQ Score | 173 |

## Description
ตี๋ อาทิตย์ วัตถุประสงค์ ("Tee" Arthit Watthuprasongkh).
Supposedly lives in Sumberbulu Krajan.

Son of "จัน" จันทรา วัตถุประสงค์ ("Chan" Chandra Watthuprasongkh) and 吳脚踏 (Go Jiaota).
A friend of [Charles Lee](Characters/Daniel%20Ashton.md).
His nickname is Tee (ตี๋), which is a rare noun translation of Chinese boy.
His mom called him as such because he had an older brother nicknamed ต๋อง (Tong), and calling him Tee, sounds similar to Chinese calling for a little brother: 弟弟 (didi).

In messaging, his father always typed ตี๋ to refer to him, and his mother always typed 弟 to refer to him.
He always called his brother 哥, despite his father's request to call his brother พี่, partially because he felt more comfortable to call with chinese calling, as his father was on duty most of the time.

Fluent in Indonesian, English, Mandarin, Korean, and Thai.
Indonesian is obviously because he lives in Indonesia.
English because of the internet.
Mandarin because of his mother, Thai because of his father.
Korean, because, apparently, the sons of 강해인, his father's best friend, lives nearby, and they're good friends.

Because Charles found him to be engaging intellectually, he must be comparably a genius too.
Probably 170-180 IQ range, perhaps 173, a prime number.
Perhaps also reminding him of Anthony, so probably also a singer and is a good dancer, and speaks Korean.
Perhaps a k-pop lover? So what would be the manifestation of that high IQ range to a K-Pop lover? Perfect kinesthetic sense, can easily acquire languages just by watching shows? And eventually, mastering etoan language, and comprehending etoan culture far more than any humans, just in their short interaction.

So, that actually delve deeper into the psyche of a Charles Lee.
And eventually, as Tee grew more familiar with etoan culture and language, Tee becomes more etoan than Charles is, and Charles becomes more human than Tee is.
Until Tee reminded Charles of that, and Charles returned on track.

Post-Charles age, three of them joined OGRE Corporation.
Tee becomes a Management Trainee of OGRE Manufacturing in Medan, eventually crossed his path with Daniel Ashton during Medan insurgency.
Se-Chan becomes the Engineering Resident.
Se-Hyeong becomes the Public Relation Officer.

## The Mountaineer Squad
So, four of them usually hang out together:

| Name | IQ | Description |
| --- | --- | --- |
| Tee Arthit | 173 | A dance machine an a singer that loves to learn new things for fun, is described to be playful, love to tease others, and has a sweet personality. |
| 강세형 | 127 | A very vocal and expressive individual that is quite random and unpredictable, is described to be lively, love to laughs and make other laughs. |
| 강세찬 | 131 | A playful and a very creative problem solver, is described to love throw pranks to others, sometimes overly analytical, and love to tease his brain (though he didn't always understand it by the end). |
| Charles Lee | 185 | An avid motorbike and automotive fan that is also an environmentally conscious person, is described to be very matured, calm, but very social. |

The Mountaineer squad was known as the Prime Partners before, as the sum of the IQ scores of Sehyung, Sechan, and Tee is a prime number: 431.
However as Charles Lee's name wasn't a prime number, their total IQ SCore is no longer a prime number.
That's when Sechan came up with The Mountaineer Squad, as the next thing they have in common is they like to climb the mountains.

This might give Ashton the idea of using combined IQ scores as a mean to give their group a name, hence the Fibonacci Task Force.
It also was enforced to the fact that Steven did the same with his gang name.
