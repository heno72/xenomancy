# Steven Pontirijaris

| Parameters | Values |
| ---------- | ------ |
| Name | dr. Steven Pontirijaris, SpPD |
| Birthdate  | 9 August 1985 |
| Father | Johanes Pontirijaris |
| Mother | Rachel |
| Brother | Alex Pontirijaris |
| Spouse | [Helena Irawan](Characters/Helena%20Irawan.md) |
| Children | [David Pontirijaris](Characters/David%20Pontirijaris.md); [Peter Pontirijaris](Characters/Peter%20Pontirijaris.md); [Eden Pontirijaris](Characters/Eden%20Pontirijaris.md); [Hayden Pontirijaris](Characters/Hayden%20Pontirijaris.md) |
| IQ Score | 180 |
| Occupation | Resident and co-owner of the Sejahtera Clinic |
| KIA Position | Member of [KIA MTF Alpha-19 "The Archangels"](Notes/KIA%20MTF%20Alpha-19%20The%20Archangels.md) |

A half human half etoan, but he prefers to describe himself as an etoan . He’s a pacifist unless provoked, altruistic optimistic, and patriotic of his origin as etoan. Like a good doctor, he upheld Hippocratic Oath. A pontirijarisian, leftist liberal, toward collaborative anarchy. Loves humanity, probably one of his favorite world. Learned medic to obtain recognitions among baselines, a hobby he can be proud of. To increase his social standings. Married to Helena and be a father of four sons. His motivation is his family and friends, and their well beings.

His personal arc is about saving his loved ones without wounding others, to resolve who really is in Helena's heart, and about saving Earth that he loved. His obstacle is the fact that his enemy will do anything to hurt his family.

## Perks 20191002
Steven Pontirijaris considers himself to be a half human etoan.
He loves swimming and hiking, usually he wants to experience them with authentic human experiences and limitations.
He loved exotic etoan food such as stewed or fried miniature cadviri chests, assorted etoan peanuts, etoan milkfruits, and various synthetic food templates available on his fabricator.
His usual metric to judge worthiness of a food (especially human food) is tender and juicy texture for meat, crunchy and watery for fruits, and leaned more toward bitter and sweet taste in general.
For drinking, he is fond of citric products, such as Earth's lime juice.

## oStorybook
dr. Steven Pontirijaris, SpPD

Pacifistic unless provoked, altruistic optimisticsm, patriotic of his origin. Like a good doctor, he upheld Hippocratic Oath. A pontirijarisian, leftist liberal, toward collaborative anarchy.

Loves humanity, probably one of his favorite world.

Learned medic to obtain recognitions among baselines, a hobby he can be proud of. To increase his social standings.

He's very tolerant to gay people, as his best friends Fernando and Daniel are gay and bisexual respectively. His son Peter is also known to be gay.

However earlier on his life he's hating gay people, as first Fernando confessed his love to him, then followed by an attempt of rape by a well-known manhunter Alexander Armaniputra. Later he made up with Fernando and accept Fernando as is.
Married to Helena and be a father of four sons.

His motivation is his family and friends, and their well beings.
