Descendant of [Adran](/Characters/Adok%20Ranensis.md), and is a close associate with [David Pontirijaris](/Characters/David%20Pontirijaris.md).
Like, Adran, it is a polymorphic artificial entity optimized as a transport vehicle.
