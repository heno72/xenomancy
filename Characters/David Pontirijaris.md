# David Pontirijaris

| Parameters | Values |
| ---------- | ------ |
| Birthdate  | 18 October 2006 |
| Name | David Pontirijaris |
| Code Name | Black Angel |
| Father | [Steven Pontirijaris](Characters/Steven%20Pontirijaris.md) |
| Mother | [Helena Irawan](Characters/Helena%20Irawan.md) |
| Brother(s) | [Peter Pontirijaris](Characters/Peter%20Pontirijaris.md); [Eden Pontirijaris](Characters/Eden%20Pontirijaris.md); [Hayden Pontirijaris](Characters/Hayden%20Pontirijaris.md) |
| Cousin | [Anthony Matthias](Characters/Anthony%20Matthias.md) (foster son of Aditya) |
| IQ Score | 182 |
| Position | [KIA MTF Gamma-42 "Four Horsemen"](Notes/KIA%20MTF%20Gamma-42%20Four%20Horsemen.md) |

The first son of Steven.
Has an affinity to art, yet his father discouraged him on pursuing that.

## Inspiration 20200618
David can draw well, so I suppose he'd study arts.
Arts are something constant, even in high tech civilization such as etoan, and human arts aren't necessarily better or worse than etoan arts.
So studying it still offers new insights for David.
Or perhaps graphic design?
Yeah, graphic design is more likely.
