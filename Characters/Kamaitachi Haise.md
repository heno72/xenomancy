# Kamaitachi Haise

![Kamaitachi Haise](/Pictures/Kamaitachi_Haise.jpg)

A volant.
The name Haise is comprised of two letters, "Strong of pearls" and "World."

## Personality
Kamaitachi Haise sees Anthony as his liberator, so he owed Anthony his service.
This also means he'd only act on a direct order.
At certain moments, he wouldn't even initiatively protect Anthony if he's not ordered to.
Kamaitachi Haise would be indifferent on Anthony's life outside that of what Anthony specifically ordered.

## History
He was in hell because he died with the entire enterprise before they're even able to "graduate" their offsprings properly.
The leftover offsprings were left to their own devices, with no guarantee that they'd surprise.
It resulted in some sort of "regret" in his side (and his colleagues), that granted him a place in Hell.

However, when Anthony encountered him in a visit Anthony made to the hell, Anthony offered a way to help him out of hell, in expense of his freedom.
Agreeing with the term, Anthony peered to his minds and discovered a possible solution to that.
With that knowledge, Anthony went to visit Haise's homeworld and track his offsprings.

Through Anthony's survey, it is discovered that a vast majority of the younglings, despite not completing their trainings, are able to survive.
They managed to produce more generations of Kamaitachis with almost no problem, even they're considered a high-ranking member of enterprises.
Knowing that his offsprings turned out fine, his regret is alleviated and he's free off hell.

As a price to his freedom, he is free to live a second life off-hell, but must come to aid Anthony at the moment Anthony summoned him.
Ever since then, he had been summoned in several occassions to help Anthony out.
The latest one is the Dawn Strike event.

## Ability
In a fight, Kamaitachi's usual method is by spreading thin to strings, forming what looks like a 3D version of a spider's nest.
The string is infinitesimally thin on human scale, and if crossed, it might result in a very clean cut to the limb or body moving through a string, and also would provide an aftereffect such as unbearable pain.
The cut area is unusually sealed, not bleeding.

His core would travel through the strings, and may manifest at any point of the strings.
He would finally reap the injured body parts.
He would eat you piece by piece, while you experience all the pain, separate from the pain of cutting.
The decapitated body parts, albeit detached from the body, still deliver pain stimuli to the original body.

## Kamaitachi in general
Kamaitachi in their natural habitat, is mostly sessile, waiting for the prey to come to them.
They only move to other places if food is scarce.
Kamaitachi is usually territorial and does not hesitate to harm other beings or even their kinds.
Their kind does not form lasting social structures.
They encounter each other either to fight or to mate.

Kamaitachi's civilization is built of social units called Enterprises.
This is their mating strategy.
So a group would collect and mate in what we would consider as orgy, because multiple individuals engaged in a mutual connection with one another, forming a larger entity called an "Enterprise".
The Enterprise basically is a platform to generate and raise their younglings, tending them to adulthood, and releasing them to the wild. When all of the succesful younglings released to the wild, the "Enterprise" disbanded and they returned to their solitary life.

The Enterprise consists of many odd-numbered individuals, the largest one can reach some thousands of individuals.
At least three Kamaitachis become a single entity called an "Enterprise" to produce children and raise them.
More individuals means more randomness, random combination of traits for their children.
Each child is an unique mix of traits.
And by raising, it meant batteries of training sessions and selection tests.

The Enterprise would decide traits they see useful for their species' survival.
After the selected pool of traits were decided upon, they shuffled it randomly and produce new individuals (their younglings, also dubbed as "subjects") with various combination of traits.
A huge number of subjects they produced, each unique to one another, are first filtered for defects.
Then the viable subjects with no defects detected are filtered through a series of training and tests.
Non-viable subjects would be reabsorbed by the Enterprise for food of other viable subjects.

Usually, it is preferred to form enterprises with older Kamaitachi individuals, because then they'd be more experienced on determining training and tests useful to produce viable subjects.
Younger individuals may join as our equivalent of "internship" in the "enterprise," to study and preserve the knowledge of raising new subjects.
