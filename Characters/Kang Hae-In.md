# Kang Hae-In

| Parameters | Values |
| ---------- | ------ |
| Birthdate  | 19730822 (August 22nd, 1973) |
| Real Name | 강해인 (Kang Hae-In) |
| Code Name | Houston |
| Partner |   |
| Son(s) | 강세형 (romanization: Yang Se-Hyung), 강세찬 (romanization: Yang Se-Chan) |
| Cousin | Fernando Suryantara, 양승민 (romanization: Yang Seung-Min) |
| IQ Score | 155 |
| Joining Year | 2009 |
| Position | Head of the 1st Division of the DFO |
| Favorite Drinks | Iced Strawberry Milk Tea |

## Description
