# Romanov Dexter

Hendrik’s copy, a part of Trinity Compact, as an analyst officer.
He's resourceful and clever.
His reflexius strength is not in brute force but in details, precision and dexterity.
Full of tricks and hacks, and especially good with altering mind states through words and body gestures.
Can be very persuasive once he knows what the opposing party's inner motivation.

His attitude is friendly, playful and slightly hedonistic.
He loves neat environment and hates mess.
Quite perfectionist as well.
His motivation is thirst of experience.
Anything he had never tried might actually interest him, and he'd try it until he got bored with it.

He was born on May 2013, because Hendrik was so devastated when he can not enroll into the med school due to his colorblindness.
He was born without color blindness, and can see into a wider range of spectrum than a human could.
From 1000 nm to 100 nm.
Then as Hendrik enrolled in a law school, he enrolled in The WTF Institute of Medicine and Holistic Health.

Later he went for an internship on the Paramundus Mahatirto, and meet Purpose there.
They were having fun, until Purpose decided to say her feelings toward him.
Ever since that time, he tried so hard to find reasons why he disliked her.
However every reason he poured off, Purpose tried so hard to be with him that she seemingly find a way to shed off every single reason that makes him disliked her.

There was a moment at around 2016 where he decided to join a humanitarian mission to Paramundus Jagadpadang along with Cetacean Freedom Forces of Paramundus Mahatirto.
It was also the time he met Barong Ket, a skilled interspecies fighter on a domain known as Balidwipa Barong Association, at the time was a captive to the invading Tengu Forces of Paramundus Jagadlangit.
There, he bonded with Barong Ket, and becomes a honorary member of a newly band Barong Ket registered: Band Airlangga.

His personal arc is to redefine his relationship with Purpose and Barong Ket, and to show that Anthony is good.
His obstacles are his ego, who wishes to have free time and free of commitments.
That and because he can actually see goodness in people, even someone as bad as Anthony.
