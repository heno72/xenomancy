# Daniel Lusien

| Parameters         | Values                                                                                          |
| ------------------ | ----------------------------------------------------------------------------------------------- |
| Name               | dr. Daniel Lusien, SpOT                                                                         |
| Birthdate          | 24 April 1986                                                                                   |
| Father             | Sylvano Lusien                                                                                  |
| Mother             | Abigail Sophia                                                                                  |
| Sister             | Sylvia Lusien                                                                                   |
| Occupation         | Resident and co-owner of the Sejahtera Clinic                                                   |
| KIA Position       | Member of [KIA MTF Alpha-19 "The Archangels"](Notes/KIA%20MTF%20Alpha-19%20The%20Archangels.md) |
| Eyes               | Brown                                                                                           |
| Hair               | Black                                                                                           |
| Medical Conditions | Situs Inversus Totalis                                                                          |
| Hands              | Fine                                                                                            |
| Skin Tone          | Asian                                                                                           |
| Height             | 178 cm                                                                                          |
| Weight             | 63 kg                                                                                           |
| Race               | Human                                                                                           |
| First Love         | Helena Irawan                                                                                   |
| PoI Tag            | #poi/dan                                                                                        |

Is mostly selfless (to some degree), resourceful and loves science fiction (toward hard spectrum), a loving person and good friend to Adran, Steven, and Helena.
Is with Fernando Suryantara.
Tend to be liked by etoan artificials and volants.
His motivation is to make a brighter day.
He valued happiness and good friends.

His personal arc is about his parents that are against his decision to be with Fernando.
His parents threatened him to make him marry a normal lady and father a son.

That and to discover freedom of love and friendship.
He must find a way to resolve his sexual orientation issue, and what he really wants to be.

## Perks 20191002
Daniel Lusien loves swimming, prefers walking, and Makassar spicy food.
He hated Java spicy food because it does not taste good, and in his opinion, javanese spices are just straightforward spicy, there's no richness of its spiciness.
His favorite side dish is crispy fried breadfruit with makassar chili sauce.
His favorite meal is Makassar Noodle Dumplings.
He enjoyed Cendol and Coconut Water.

For clothing he prefers something comfortable to wear, usually sport clothes, and the color is usually neutral dark.
He dislikes accessories, and prefer to carry a small bag around.

## oStorybook notes:
dr.
Daniel Lusien, SpOT

Is mostly selfless (to some degree), resourceful and loves science fiction (toward hard spectrum), a loving person and good friend to Adran, Steven, and Helena.
Is lover of Fernando Suryantara.
Tend to be liked by etoan artificials and volants.

His motivation is to make a brighter day.
He valued happiness and good friends.

### Notes about Daniel Lusien on 20180719

Daniel is a person that Steven can trust.
He's somewhat resourceful, always wants to make a day brighter than ever.
he focussed so much on making others happy than to focus on himself.

[...]

He's always in Steven's side.
Daniel is a reliable supporting character.

[...]

His father denied his love with Fernando and told him to date a lady.
Perhaps this is his internal struggle.
Dating Derictor is Daniel's answer to his father's unacceptance to his sexuality.
This is his own storyline.

Also, on my facebook chat with Story Writing chatroom said:

He's at shock when he learned that Steven is half Etoan.
Not because alien civilization is a thing, but because he's disappointed at Steven, as he didn't tell Daniel that.
He exclaimed that "how didn't you tell me earlier? I mean all of the stuff I wished to exist, all of them is in front of me as my friend all of this time?"

### Notes:
He is that kind of guy that sometimes provide Steven with clever ideas and suggestions.
