# Derictor Wijaya

| Parameters      | Values                                                                               |
| --------------- | ------------------------------------------------------------------------------------ |
| Birthdate       | 19870810 (August 10th, 1987)                                                         |
| Real Name       | Derictor Wijaya                                                                      |
| Code Name       | Delapan                                                                              |
| IQ Score        | 171                                                                                  |
| Joining Year    | 2009                                                                                 |
| Position        | Infiltrator Finance and Treasury Manager (BAIK); Chief Executive Officer (Kukercorp) |
| Mission History | 2020: Sign for Executive Election on KukerCorp main company.                         |
| Morality        | He has special interest in [needs reworking].                                        |
| Favorite Drinks | Vanilla Milkshake                                                                    |
| PoI             | #poi/rik                                                                             |

Derictor controls the financial flow and (money) laundering task .

Together with Anderson, they launder a number of used materials and reforge them to build BAIK's custom weaponry, mostly electronics (rail guns and coil guns everybody) and zero explosives.

Notes:
Henokh's brother-in-law, and his trusted pal.

## Perks 20191002
Derictor is an avid knife thrower and has an extensive collection of various knives of various metal compositions and styles.
He enjoyed korean food mostly, but he treasured kimchi above all.
To add some korean taste to Indonesian food, he usually experiments by adding kimchi to food.
He knew that kimchi with rendang did not go well.
He loves milk with ginger extract.
