# Nurhayati Maulidia

| Parameters | Values |
| ------ | ------ |
| Name | Nurhayati Maulidia |
| Codename | Klandestin |
| Gender | female |
| Birthdate | _ |
| IQ Score | _ |
| Height | _ cm |
| IA Admission | 2016 |
| Position | [KIA MTF Psi-72 "Nine Tailed Fox"]() |
| Favorite Drinks | Iced Green Tea |

## Description

### Snipped Source: Xe-1 Incursion
> A young woman, with light brown skin, and curled black hair, rushed to the pavilion Derictor and Kang Hae-In were on.
> A very decent light gray blazer over her bright black shirt accompanied with her dark brown skirt really suited her, Derictor thought.
> The outfit highlighted her natural skin tone even more.
Before he knew it, he was standing, his hand was with her hand, and they exchanged glances.

> Seven years since two thousand and nine he had been with the Intelligence Agency that Henokh and Anderson instituted, never did he think that someone like her would join.
> Kang Hae-In did say that she was a member of a drama club and a paramilitary club of a university south of Surabaya, and that he picked her up for her acting and people skill.
> Most importantly, her paramilitary training and her mastery over five languages.
> A perfect infiltrator, Kang Hae-In said.
> Kang Hae-In, however, never mentioned that she was beautiful.
