# Arman Pondalissido

An Etoan superior (*Pondalissido hadeanensis*).

A member and high priest of the Umbra Cahaya.

Morality: Lawful Evil, advancement of Denefasan influence on Earth.
