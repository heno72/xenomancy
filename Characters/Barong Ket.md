# Barong Ket

![Barong Ket](/Pictures/Barong_Ket.jpg)

A volant, inhabitant of Paramundus Jagadpadang.

## History with Manov
As recent as year 2016 in real world, inhabitants of the Paramundus Jagadlangit, the Tengus, breached the perimeter of Paramundus Jagadpadang.
The Tengu Forces attacked any bands within some distance from the breach, and starts producing a large and dense forest with tall trees.
The Dewata of Balidwipa Barong Association, Barong Airlangga, led a resistance against the incoming Tengu Forces.

Barong Ket, Barong Macan, and Barong Bangkal, together as Band Banas Pati Raja, decided to join the resistance against the invading inhabitants of Paramundus Jagadlangit.
Their expertise in facing supramundus threats is what makes Band Banas Pati Raja valuable in such a conflict involving more than a single Paramundus.
Therefore, they were sent to the front line to scout and decide the best actions to get the Tengu Forces off Paramundus Jagadpadang.

However, during the scouting, one of the Tengu Force scouts discovered them, and they were soon surrounded by the Tengu Forces.
They were brought to the new capital of the Tengu Hierarchy, and was tortured to reveal the location of Barong Airlangga.
Refusing to reveal the location, they were sent into something akin to a gladiator, where they'd fight batteries of Tengu Fighters until their death.

Barong Bangkal died first, when the Tengu Fighter managed to outsmart his ruthless fierceness, leading Barong Bangkal to a trap.
Barong Macan fell from a Wooden Skyscraper as he took a wrong stance when climbing to capture a Tengu running away with flight.
Barong Ket almost died in a battle against a Tengu that blow a wind strong enough to lift him and caused a falling injury.

It was around the time the backup from Paramundus Mahatirto arrived, where a pack of Cetacean Freedom Forces stormed the forest Barong Ket was in.
Manov, was an intern of a medical school, taking the shape of a cetacean, approached Barong Ket and tend his wounds, before carrying him to the safety.
During his recovery, Manov aided him from day to day, even helping him to walk again.

Spending a lot of time together, they develop closeness more than just friends, as Manov walked him through his emotional pain of losing his peers.
He would sometimes fake his pain so that Manov would tend him, or at least put Manov's attention toward him.
Manov wasn't stupid either, but he was willing to provide his attention to Barong.

Weeks passed, and Manov decided to change his appearance to his humanform, and would sometimes join Barong Ket in a hunt when he wasn't tending the wounded.
That was when Barong Ket expressed his desire to form a new band with Manov.
Manov said yes, and even asked his fellow intern, Pride, a Higher Purpoise, to join the band.

It was one of the most memorable life Barong Ket had, spent with Manov and Pride, and along with the Barong Resistances and the Cetacean Freedom Forces, they managed to push the Tengu Forces off the Paramundus Jagadpadang, and back to their domain, Paramundus Jagadlangit.
It was also the time the Cetacean Freedom Forces were called off to return to their origin, Paramundus Mahatirto.
It was the time Barong Ket had to ask Manov to stay, as he was not technically a cetacean.

Manov wanted to stay, but he must go back to Paramundus Mahatirto to complete his medical school.
Barong Ket said he would enroll to the medical school Manov entered, but he wasn't able to pass the placement test.
Pride returned to Paramundus Mahatirto without any hesitation.

One night before Manov's departure, it was just Manov and Barong Ket lying in a grassfield.
Barong Ket knew how badly Manov wish to complete his medical study.
Manov knew how badly Barong Ket want to remain with him.

Barong Ket had a life here, he was a respected, and skilled fighter with plenty of experiences.
Manov had to complete his medical study, and had to return to Manentmundus, the physical realms, where his other doubles that fund his study waited.
In the end, Manov would spent a lot more time in Manentmundus, and Barong Ket must also stay in Paramundus Jagadpadang for his service to the Balidwipa Barong Association.

They made a pact, that even though they couldn't be in the same realm for a prolonged time, they will always belong to the same band they made.
Barong Ket filed a petition to the Dewata of the Balidwipa Barong Association, Airlangga, to dissolve the designation of Band Banas Pati Raja.
Then he filed a new petition to register the name of his new band, the Band of Two Realms, the Band Airlangga (jumping water).
The current Dewata of the Balidwipa Barong Association, Airlangga, granted the use of his name as Barong Ket's band name, under the condition that he'd accept apprentices to Band Airlangga, and that Manov is considered as a honorary member of Band Airlangga.

Long after Manov departed for Paramundus Mahatirto, and even after he left for Manentmundus, Barong Ket and Manov still frequently contact each other.
And in one occassion, Barong Ket received a pass to travel to Manentmundus.
It was around the time Manov was fighting Kamaitachi Haise.

## Barong in general
### Physical Traits
The head is full of radiant color, mostly bright red linings and gold base, and a large pair of intimidating eyes, a horn, and a large jaw.
From the mouth are four fangs, two above and two under.
The top fangs were long enough, resembling that of a smilodon, but not as long.
The lower fangs were protruding to the top and slightly to the sides, resembling that of babirusa lower fangs.
The mouth is large, a human head could fit in.

The body is covered in thick silky white hair-like furs, and a more pronounced head-neck hair like that of a lion.
The head-neck hair has some significant strands of bright red, patterned with bright gold, and ordinary silky white hair-like furs.
Every individuals has different patterning.
The lower limbs has alternating rings of bright red and gold fur coverings.

Walk in four slender and flexible limbs, it has a considerably long and flexible core body.
It can slither, with the limbs on the side, like that of a long-legged crocodile, or can walk normally like any normal feline.
The tail is prehensile, and there are red hairs on the tip.

Sometimes seen with armor coatings, usually gold-colored plates, covering the shoulder, the back, and sometimes the head.
Piercing can be seen in some individuals, but mostly prefer to not use any piercing.
Most often found naked, with no accessories attached.

### Behaviour
Barong's personality is childlike playfullness with strong curiosity tendency.
They love to experiment and to play, often found to prank their bands.
Despite their mischevious behavior, they are caring to each other, and would be distressed if one of their band members sustained serious injuries.

Usually friendly toward strangers, they often asked strangers to play with them.
They love to be patted and cuddles with each other or even with a friendly stranger.
However, at the very first sign of threat, they'd not be hesitant to be aggressive.

They are omnivorous, and are hunter-gatherers.
They rarely stay in one place, and would roam about to find good edible materials, be they animate or vegetative.
However, due to their friendly nature, they rarely eat live animals, most often corpses or aggressive animals.

### Social Structures
They are often found in bands of two to four, rarely up to ten.
All adult members of the bands are always of the same gender, with some very rare exceptions.
The bands are for life.

When two different bands of different genders meet, they may perform ritualistic fight in between the two bands.
If they're happy with their ritual opponents, they'd mate with the members of the opposite gender.
Afterwards the mixed bands would spend some time hunting or socializing together.
Then they will part into separate ways.

The Bearer band would then give birth to their offsprings and raises them.
Once their offsprings reach adulthood, the Seeders would leave the band to form a new band.
Those of the Bearer gender stay in their parent band.
When the parent Bearer band grew too big or approaching ten members, where they would split into smaller bands.

When encountering another band or bands of the same gender, they'd often socialize with them, spending some time as a mixed band.
After some time they'd part ways, and rarely, some members of the bands swapped between the two or more bands.

Despite having two different genders, there's little difference in traits in between the two genders.
The only difference is their ability to donate genetic information.
The genetic information donors are the seeders, and the genetic information recipients are the Bearer.
The Bearer bear the children and raises them.

### Native Environment
Being in volant-space, they're native to the domain of a virtual world known as Paramundus Jagadpadang.
If a manent inhabitant were to visit the paramundus, it would present itself as a large, relatively flat savana.
