# Bain Armaniputera

| Label | Value |
| :---- | :---- |
| First name | Bain |
| Last name | Armaniputera |
| Abbreviation | Lima |
| Gender | male |
| Category | Minor |
| Birthday | April 12, 1962 |
| Day of death | August 17, 2021 |
| Occupation | Kuker Corporation AI Researcher; BAIK Agent Lima |
