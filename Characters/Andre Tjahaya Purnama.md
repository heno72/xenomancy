# Andre Tjahaya Purnama

| Label | Value |
| :---- | :---- |
| Name | Andre Tjahaya Purnama |
| Nick | Kao (by Chandra) |
| Code Name | Empat |
| Gender | male |
| Birthdate | June 29, 1986 |
| IQ Score | 148 |
| Height | 185 cm |
| IA Admission | 2009 |
| Position | [KIA MTF Psi-72 "Nine Tailed Fox"]() |

## Description
He is huge, being 185cm tall.
Chandra called him Kao, or a mountain, because Anderson is sometimes called Andre as well.
Playful and strong, but somewhat calm and charismatic.
He is a good tactician, and love board games.

## Physical Appearances
Modelled after: Ohm Thitiwat.
