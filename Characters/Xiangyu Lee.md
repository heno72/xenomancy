# Xiangyu Lee

19920411

Occupation: Master of Pyrokinesis

Description:

*Ursus ignitus*, Fire Bear, also known as Yeti.

A yeti (my take on it is an uplifted polar bear) which is a master of pyrokinesis and manipulation of light, named Xiangyu.
He can fly by producing jet flames on his appendages, and can cause optical illusion with light manipulation.
-20181117

Heinrich learned his pyrokinesis by studying directly from the race that are the best in pyrokinesis, the Dragon Dancers (also known well by humans as Yeti, or in reflexior community as Himalayan Polar Bear).
So Xiangyu is a Master of Pyrokinesis that became Heinrich's mentor.

Xiangyu has an appearance not unlike that of a polar bear.
His pyrokinesis makes his fighting form be a fiery bear.
With his pyrokinesis he turned all his furs into fiery strands and also produced an intimidating sight of a bear made of fire.
-20181128

Notes:
Ursus ignitus, Fire Bear, also known as Yeti.
A master of pyrokinesis and manipulation of light.
His fighting form is a fiery bear (literally on fire).
