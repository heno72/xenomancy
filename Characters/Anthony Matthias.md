# Anthony Matthias

| Parameters | Values |
| ------ | ------ |
| Name | Anthony Matthias |
| Korean Name | 양기환 (romanization: Yang Gi-Hwan) |
| Birthdate | 31 January 2002 |
| Father | 양승민 (romanization: Yang Seung-Min) |
| Mother | [Martha Williams](Characters/Martha%20Williams.md) |
| Sister | [Michelle Williams](Characters/Michelle%20Williams.md) (양치민, rom.: Yang Chi-Min) |
| Reflexius Spectrum | Green |
| Specialization | Necromancy |

He is a necromancer, he can communicate with souls in hell.
He can force them to do his bidding in exchange for a way to escape hell.
Also, autodidactically he is able to do pyrokinesis, but he can not ignite a fire.
Was accidentally lost in the woods by his father when he learned that his son can do wicked things (reflexius).

His personal arc is to find his mother, and to resolve his nightmare.
His nightmare reveals more and more about who her parents are, over the course of the story, but intensifies after he meets Martha.

From Xenomancy at oStorybook:

> As a reflexior, his specialty in necromancy, the art of divination from the dead.
Per his specialization, he had this knowledge of the Hellgate.
Can divinate with the weather, by calling upon deeds of Hadad.
Somewhat similar power to that of Jesus during thunderstorm in the dead sea.
Can create short-lived scions with specific purpose.
The scions could originate from himself, or from any thinking being his soul-extension can reach.
Can summon and command volants via contracts.
Can bind volants into contract, given that the volant agreed with the terms.
Had many volants on his deeds.
Can do pyrokinesis, that he learned autodidactically, but can not ignite the fire by his own means.
He can only redirect fire and manipulate already existing flames.

> Apparently in my dream he's quite similar with Barry Allen.
Has this desire to help people, but in ways people might find eccentric.
He's super smart, with superpowers, yet bad with ladies.
