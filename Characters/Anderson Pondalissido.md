# Anderson Pondalissido

19501015

An Etoan superior (*Pondalissido hadeanensis*).
He’s unpredictable in nature.
Has somewhat alien point of view, sometimes bizarre, sometimes appear to be human like.
Mostly silent and calm.
His looks is intimidating for normal human.
However he could change minor features on his body, like skin color and eye irises at will.
His shapeshifting ability took some days to complete the change.

His personal arc is to stop the Antichrist, and to prevent Denefasan attack.
His obstacle is the fact that he hated hidden variable, and mistakenly conclude that Henokh is the Antichrist.
The fact is, he really liked Henokh, and hated it so much when he concluded that he's the Antichrist.
He had to kill Henokh, though he did not want to be directly killing him.

oStorybook Notes:
> Real Name: Anderson Pondalissido Joining Year: 2009 Position: Field Agent, Administrative Officer, Tactical Advisor.

> Mission History:

> -2019: Tracking Unit H -2021: Stopping threats from Aditya.
He fails, wounded.

> Morality: Loves to tinker with strategy and logic.
Everything has a high level system that organize things, and he'd love to play with the system to obtain certain results he so desire, within the rule of those systems.
That certain result, may be his own job (BAIK, resolution from House of Pontirijaris Council, or even Aucantica Authority's order), his own interest (playing Go, find a situation when he must use his logic and strategy the most, his own joke), or even to decide who to socialize with.
Some less pronounced trait is that he loves to role play, as it provides higher degree of leverage over his desires of logic and strategy, by having additional hindrances to himself so that he must use logic and strategy even more (explains his different attitudes between House of Pontirijaris members, corporate collegues, and BAIK Members, and why he loves to take infiltration related jobs).

> Unpredictable in nature.
Has somewhat alien point of view, sometimes bizarre, sometimes appear to be human like.
Mostly silent.
His looks is intimidating for normal human.
However he could change minor features on his body, like skin color and eye irises at will.
His shapeshifting ability took some days to complete the change.
