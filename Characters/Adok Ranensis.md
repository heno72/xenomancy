# Adok Ranensis

His birth (or awakening day) is 21 May 1956, or 67,488.7841 CL. However he's sent to, and printed on Earth in 2003. He's a Multimodal Autonomous Transport Vehicle, Ranensis Line of Admir Locomotive Clade, and officially a full citizen of Etoan Earth Colonization Initiative.

His body is comparable in size, mass, and ability with a Pajero, plus the intelligence of a transapient. He loves being in road, because the state/feel of roaming across country roads, scaling up and down hills, and on open space provides him some sense of spiritual trance.

He has a certain disliking to Helena's normal car (dumb car, as Adran put it). His preferred coloration is black, and texture of a smooth natural stone, but adopt normal black car texture on normal city environment. He prefers to use green lighting, and he likes the moon's UV radiance, though at day he also enjoys power surges from the sun. However, he dislikes very high intensity of light he has to work through, therefore he has a love-hate relationship with daylight. He naturally prefers driving on road than flying because of his strong tactile senses on his tires, but flying is not that hard for him to grasp.

Though he's self cleaning, he loves Daniel as he tends to cleanse him thoroughly, and with care.
