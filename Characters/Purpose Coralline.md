# Purpose Coralline

20010410

Uplifted Striped Dolphin (*Stenella coeruleoalba*).

A higher porpoise named Purpose (on the pun that a higher porpoise has a higher purpose), an uplifted dolphin, with the ability to manipulate sound, and psychokinesis on liquid spectrum.
She usually floats midair as if she's swimming on water.
-20181117

Manov and Purpose used to be a couple, some years ago.
They meet in a Paramundus (immersive virtual reality, spirit realm, name it) of the endless sea.
They were members of the same dolphin pod (Manov decided to use dolphin avatar at the time, and so did some of the participants), roaming through the endless sea in freedom and pursuit of fun.
A hedonistic group that focus on having fun all the time (and hunting for (virtual) fishes for food).
Manov however didn't realize that she's actually a dolphin in real life.

Purpose has freedom as her main trait.
She dislikes hindrance, and her magic reflected her trait.
She can swim on anything.
She can swim on the air, on the ground, and even on concrete.
Her magic is fluid, and she could make solid objects behave like fluid before returning them to their previous properties.
In a scene i have, she swam in the pavement, and caused enemy army to sink into the pavement, leaving only their upper body above ground.
-20181128

So Manov and Purpose once in a relationship, but back then Manov assumed the form of a dolphin, and he didn't know that Purpose is an actual dolphin.
Ever since they both learned that Manov isn't a dolphin, and Purpose is actually a dolphin, they realized that they can't really be together, especially if they decide to live together.
Other than that, apparently Purpose learned magic that enabled her to survive outside water for prolonged period of time, the magic of swimming through matter, so that she could be near Manov.
Perhaps this might be an interesting aspect of her character, wouldn't it?
And perhaps the most shocking revelation of Purpose is learning that Manov isn't even a real person, but a double of Hendrik.
-20181212
