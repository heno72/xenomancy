# Henokh Lisander

| Parameters | Values |
| ---------- | ------ |
| Birthdate  | 10 October 1984 |
| Real Name | Henokh Lisander |
| Code Name | Suharto |
| Spouse | [Sylvia Lusien](Characters/Sylvia%20Lusien.md) |
| Children | [Zean Lisander](Characters/Zean%20Lisander.md); [Callan Lusien](Characters/Callan%20Lusien.md) |
| Cousin | [Daniel Lusien](Characters/Daniel%20Lusien.md); [Derictor Wijaya](Characters/Derictor%20Wijaya.md) |
| Joining Year | 2009 |
| Position | Chief Executive Officer of the Intelligence Agency |

Agent of BAIK , code named Agent Suharto.
Wished solely on a better world of his idealistic imagery of a perfect society.
Driven by his experience being persecuted as a member of a minority ethnicity and religion.
In other worlds, he hated racism and society class separation/segregation.
His motivation is to eliminate segregation of social classes and discrimination.
His traits are Stubborn, persistent, deterministic, and occasionally emotional when faced with Aditya.

His personal arc is to meet and talk with Aditya, to ask him why he left him.
His main obstacle is the fact that Aditya is an insurgent (supposedly antagonist), and he's in the supposedly protagonistic side.
Due to that he had some trouble with his loved ones, can not really focus on people who love him.

## Perks 20191002
Henokh Liesander enjoys visit to gym, martial art classes, and skincares.
He usually eat healthy food with a significant portion of beans and vegetables, and fresh boiled fishes, but he does not mind occasional pizza party.
He disliked potato chips, and salted dried fishes.
For drinks, he loves assorted vegetable juices, mango juices, but hated avocado.
All of those are without added sugars.
