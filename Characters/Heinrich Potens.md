# Heinrich Potens

Hendrik’s copy, a part of Trinity Compact as the security officer.
He's exceptionally proficient at pyrokinesis and psychokinesis.
Full of strength and brute force, strong-willed and persistent.
Perhaps the most advanced magic he has is to materialize a physical body nearly equivalent in strength to Heavenly Hosts.

He's ruthless and indifferent in nature.
Pragmatic and cautious in decision-making.
His motivation is peace, he hated to be provoked, and wish to find some time for himself.

He was born on September 2015.
The reason of his creation is because Hendrik was shocked by the politics of the undergraduate student organizations.
Hendrik wanted a version of him that is ruthless, and can defend himself, especially to know when to say no.

On around November 2015, he enrolled to the Angel Academy, where he'd study hard to be the Angels, the police force of the Divine on mortal realms.
There, he becomes the lecturer assistant of a great pyrokinesis instructor: Master Xiangyu.
On around March 2019, he was graduated from the Academy and begins his internship as a Junior Associate of Master Xiangyu.

His personal arc is to protect Hendriks from danger, and to truly understand the rage inside him.
His main obstacles are his rage, suppressed emotions, control freakiness, and deep inability to really trust people.
