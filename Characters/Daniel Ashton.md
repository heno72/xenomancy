---
tags:
  - poi/ashton
---
# Daniel Ashton

![Ashton's illustration.](/Pictures/Ashton_v2_cropped.jpg)

| Parameters | Values |
| ------ | ------ |
| Etoan Name | Ostaupixtrilis Pontirijaris |
| Human Name(s) | Jesus of Nazareth; Calvin Gauss; Michael Guntur Putra Metusalah; Alfa Iskandar Putera; Christopher Agape Lumintan; Charles Lee; Daniel Ashton |
| Official Partner(s) | Leaf; [Adok Ranensis](Characters/Adok%20Ranensis.md); Tudva Pontiritarax |
| Son | Johanes Pontirijaris |
| Grandchildren | [Steven Pontirijaris](Characters/Steven%20Pontirijaris.md); Alex Pontirijaris |
| Great Grandchildren | [David Pontirijaris](Characters/David%20Pontirijaris.md); [Peter Pontirijaris](Characters/Peter%20Pontirijaris.md); [Eden Pontirijaris](Characters/Eden%20Pontirijaris.md); [Hayden Pontirijaris](Characters/Hayden%20Pontirijaris.md) |
| Birthday | 52,741.0295 CL (somewhere around mid 13,504 BCE) |
| Printed Date on Earth | 12 October 1950 |
| Latest Rejuvenation Date | 17 February 2003 (16th rejuvenation to biological age of 7 years old); 67,533.3680 CL |
| Hair | White |
| Eyes | White |
| Ears | Pointed |
| Skin tone | Pale White |
| Height | 178 cm |
| Weight | 75 kg |
| Body | Moderately built, youthful |
| Race | *Eto aucafidi* |

Presently a teenager.

Daniel as it means God's my judge, because he decided to be a good person in this life.
Ashton means from the ashes.
He assumed it was like the phoneix, that rise from the ashes, as he's rebirthed.
In principle by evoking his past memories as Jesus, he can gain his Divine Deeds, in short he can re-enact Jesus's miracles.

## Notes
He's the biological grandfather of **Steven Pontirijaris.**
Originally named **Ostaupixtrilis Pontirijaris.**
He was Jesus, roughly two millennia ago.
Originally printed on Earth at 12 October 1950.
His last rejuvenation was on 2003, back to 7 years old.
67,533.3680 CL to be exact, or 17 February 2003.

In 2011, he decided to return to Earth, but leaving his biologically fifteen years old instance to explore the Earth.
Later in August 2019, Earth time, he was destructively uploaded and beamed back to Earth, arriving on September 2019.
Over the span of one and a half years, his scion that encapsulated his Aucafidian instance bargained with his Earth instance.
The bargain was over by the end of March 2021, where a merging term was agreed upon, that maximises their success, and minimize detection by local powers.

## Keep Note
### 20200313
Ostaupixtrilis Pontirijaris on Earth circa 1950 prior his merger with his Aucafidian self to form Daniel Ashton, had been through quite a lot of life.
Circa 2011, he had been exploring many schools as a school student, in the body of a 15 years old.
I think it is wise to consider that he had been, on average, 2 years a school.

### 20200618
Ashton has a considerably energetic soul for an ancient man, and he loves large bikes.
He's multitalented, and very social.
I somewhat want to make Ashton a dirty grandpa type individual, considering that to him, sexual encounter is just as normal as any social interactions.
And there's this scene where he's having a conversation relating to consent in a sexual encounter, and he's reported by a more conservative classmate of him.
It is during the university years of Ashton.

Given that Ashton had been established to be a motorcyclist lover since about some 2013 era, I think it makes sense if he'd choose engineering.
Yes, I think Ashton would've preferred a mechanical engineering major.
His research interest would be, wait for it, origami!
Specifically foldable structures, and reconfigurable folds.
After all, he's [the Origami Knight](Notes/KIA%20MTF%20Gamma-42%20Four%20Horsemen.md).

### Timeline
| No. | Occupation Length | Assumed Name | Notes |
| --: | ----------------- | ------------ | ----- |
| 1 | Jul 2011-Aug 2013 (~2 years) | Calvin Gauss | Parents: Heinrich Gauss and Kartika. Intervene with Hendrik's life, and Derictor's life. Theme: What are you willing to lose? |
| 2 | Aug 2013-May 2016 (~3 years) | Michael Guntur Putra Metusalah | Parent: Keenan Metusalah Evans. Managed to graduate the school. Assembled Gang Orca, and taught them Etoan applied memetics. Theme: Be (like) God. |
| 3 | Jul 2016-Sep 2017 (~1 years) | Alfa Iskandar Putera | Parents: Jivus Pontiritarax and Istar Laupatura. The goal is to overlap with Xiangyu's life, and overthrow EPL occupation in the city. |
| 4 | Oct 2017-Sep 2019 (~2 years) | Christopher Agape Lumintan | Parents: Edward Kevlar and Patricia Gunawan. His life overlapped with Anthony's life here. |
| 5 | Sep 2019-Mar 2021 (~1.5 years) | Charles Lee | Guardian: Father Christopher Lee. Decided to settle near Ijen Crater, stagnating. Here, he meets Tee, and that renewed his fire. By the end of his stay here, he's merged with his version from Aucafidus. |
| 6 | Sep 2021-May 2024 (~3 years) | Daniel Ashton | Parents: Steven Pontirijaris and Helena Irawan. Attended the same class as his great grandson: David Pontirijaris. Assembling the Fibonacci Task Force. Managed to graduate the school, hence the last high school he attended on Earth. |


### 1st school, occupation time: ~2 years (July 2011-August 2013).
Calvin Gauss, living as a newcomer to Makassar, in a safehouse with a family of an established Etoan colonist named Heinrich Gauss and a human wife named Kartika.
His foster brother is Ferdi Gauss, in the same class as Hendrik.
Enrolled as a first year student, left as an early third year student.

The goal is to intervene with Hendrik's life, and to intervene with Derictor, Henokh, and the HD Club.
Theme: it is not about what you want to have, but about what you're willing to lose to get what you want to get.
His time overlapped with Hendrik's life since about the time Hendrik started to date Jie (July 21st proposed via text, December 21st, ended via text), to Hendrik's graduation.

One of the major conflict here is an internal battle.
Of his young body with its healthy sexual needs, and his composed thoughts.
Here, an aggressive young girl tried to persuade him into having sex with her.
And a mysterious young adult, Edward Kevlar, approached him.

The other problem is to help Derictor cope with the loss of his father, by continuing his father's passion, that Derictor shared.

Then HD Club had to help Daniel with his parents.
The solution is, through accepting himself.
Calvin here helps by suggesting that there's no need to conform with the notion of heteronormal life: to marry and raise children.
Also to help Fernando realize that there are many forms of love, so there's no need to frame love the way modern human society did.

### 2nd school, occupation time: ~3 years (August 2013-May 2016).
Michael Guntur Putra Metusalah, under the care of Keenan Metusalah Evans, an etoan philantropist and a director of an orphanage.
Enrolled as a first year transfer student, managed to graduate.

The goal here is to train on working as a team.
He assembled a motorist club, with an unorthodox way of achieving wins.
The motorist club is called the Gang Orca, with their signature coloration are black and white.
Theme: Jadilah terang dunia.
Be like Christ.
Be God, that is, be like God.
So people can directly observe God in action, when they observe our life.

Sub theme: live the present.

Remember the experience during the time my motorbike's oil was replaced, and the transmission chains along with the disk got replaced.
Might be useful for this.

### 3rd school, occupation time: ~1 years (July 2016-September 2017).
Alfa Iskandar Putera.
Adopted by an etoan colonist family: Jivus Pontiritarax (a relative to Tudva Pontiritarax) and Istar Laupatura.
Enrolled as a first year student.
Left on the early 11th grade.

Somewhere in the Central Sulawesi.
The goal here is to have his life overlap with Xiangyu.
His favorite past time was to sit atop a hill with his friends, and observe arriving and/or departing airplanes.
Theme: observe the soul, not the looks.

The issue here is a conflict between EPL and WTF, taking place in the outskirts of the city.
The city is an EPL stronghold (under the frontend of the Charismatic Church of Colins, that is highly respected by the local community), protecting them from a chaotic insurgency called the Freedom Force (actually a WTF task force).
CCC is modelled after GMS.

### 4th school, occupation time: ~2 years (October 2017-September 2019).
Christopher Agape Lumintan, adopted by Edward Kevlar and Patricia Gunawan, their domicile is in Kendari.
Enrolled as a first year transfer student, as a 12th grade student he left on the first mid term.

The aim here is for Kav to interact with Yam, negotiating their deals.
To do so, his life happen to overlap with the life of Aditya's foster son, Anthony Matthias (Yang Gi-hwan).
Theme: finding a partner of intelligence rigour is hard.

### 5th school, occupation time: ~1.5 years (September 2019-March 2021).
Charles Lee, taken care by Christopher Lee, a priest in a secluded chruch near Ijen Crater.
Enrolled as a 10th grade transfer student, left a month before the national exam.

The goal here is to prepare his merger with his Aucafidian self.
In this arc, Charles had becoming too human already.
He's stagnating, spiritually, and therefore, he needs to renew his inner fire.
An attempt to paint one's spiritual renewal, a rebirth.

The first day as this instance, he was caught in an orchestrated accident, concurrent with Angel's Job, even directly resulted from it.
He had to traverse the woods, and find the cottage that belongs to Father Chris.
Afterwards, he was taken care by Father Chris, and at that moment, his Aucafidian instance encapsulated in a scion was uploaded to him, but wasn't integrated.

He dreamed of Aucafidus, of someone he loved but couldn't remember.
Of someone so close but he couldn't recognize.
And he woke up, surprised to find Father Chris, which is also equally surprised.

Over the course of days, he just wanted to rest after his various action packed adventures in other schools.
This scion offered yet another mission, that he didn't want to do at the time.
They develop some sort of relationship, bugged with requests to allow the scion's payload be merged with him.

This scion can only contact him during his sleep, in a dream-like state.
The problem is that Charles already found his life here to be quite peaceful, and calm, contrary to his previous life.
His adoptive father, Christopher Lee, is caring, calm, and composed, and his friend, Tee (ตี๋), is actually quite engaging to interact with.
He is ready to settle in, at last.

Until Tee, now fluent in Hadeo Herika, and is familiar with etoan culture, and being informed about Charles's Aucafidian instance, changed his mind.
Tee told him, there is no point of running away, as sooner or later, they're all going to face it.
The mission that the scion wanted to accomplish, to redeem humanity, means also saving the very thing that makes us human.
Being human means living with the limitations, and work around those limitations, and prosper.
The mission aimed not only to protect that, but also to push humanity so that they'd work harder around the limitations.
The end result, is the proliferation of innovations, so the limitations of humanity as a whole expands, rapidly, and everywhere.

Humanity is also stagnating, Tee said.
Etoan is stagnating as well.
But helping humanity out of stagnation is easier than helping etoan out of stagnation.
And once humanity stops stagnating, etoan would benefit from it too.
Suddenly, there's a new field to expand, for both humanity and etoan.

That makes Charles realized, that he had been content with the limitations he had at the moment.
He is too, stagnating.
The scion, offered a way out, a way to not stagnate.
And Tee, taught him that.

That is when, he accepted the Scion's offer.

**Note:** According to Alteration 20200612, Abel's name is replaced with Charles.
The full name should be adjusted accordingly, from Abel Chrisputera Lee to Charles Lee.
Also, his friend's full name is ตี๋ อาทิตย์ วัตถุประสงค์ ("Tee" Arthit Watthuprasongkh).
Tee is the son of Chandra Watthuprasongkh.

Probably, this scene must be spread across the entire book, until the third book.
So, Ashton could finally remember his true intent when he started all of this.
And through that, he realized what Kav really wish to do.

### 6th school, occupation time: ~3 years (September 2021-May 2024).
Daniel Ashton, returned under the care of Steven Pontirijaris and Helena Irawan.
His "Foster brothers" are actually his great grandchildren.
This is his second high school incarnation that actually graduates from his high school.
He enrolled to the same grade as David, and they graduate together.
They'd eventually study in the university together as well.

His goal here is to assemble and to activate the Christ Cluster.
Especially on reaching Zean and Michelle, assembling their support system.

David was inspired by his dad's Hexadecimal Ducenti Club (HD Club), where its members combined IQ score is two hundred in hexadecimal (512 in decimal).

David calls the squad as the Fibonacci Task Force.
He realized that their combined IQ is 610, the 15th number in Fibonacci sequence.
Here's the composition:

| Rank | IQ Score | Name |
| ---: | -------: | ---- |
| 1 | 185 | Daniel Ashton |
| 2 | 182 | David Pontirijaris |
| 3 | 125 | Michelle Williams |
| 4 | 118 | Zean Lisander |
| **Sum** | **610** | **15th Fibonacci Number** |

#### Bedroom Setting
Ashton at the time occupy his own bedroom.
It was set to resemble his garden at Sidsido Ahidei, via the use of active *panomeson* (virtual reality) suite.
When we enter the room, it looks like this:

![Ashton at his park, in Sidsido Ahidei, Aucafidus.](/Pictures/Ashton_in_Aucafidus.jpg)
