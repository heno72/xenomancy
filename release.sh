#!/bin/bash
# release.sh is a file for us to easily export our chapters into
# a book form. Arranging the book chapters can be done just by
# modifying file list set in $manifest variable, and various
# configurations are condensed into a defaults file and metadata
# yaml files.
# Currently the script only handles Xe-1 releases, as Xe-2 possibly
# uses a more obscure html to pdf methods.
# To use: ./release.sh <version label> <target extension>

## Variables
fulltext="tmp/fulltext.md"
manifest="mods/xe-1.list"
defaults="xe-1-release"

## get manifest file and concatenate the contents.
if [ -d tmp ]; then
  cat $manifest | xargs cat - > $fulltext
else
  mkdir tmp
  cat $manifest | xargs cat - > $fulltext
fi

# Add a newline before all lv.1 heading
sed -i 's/^# /\n# /g' $fulltext

## Filter them through a default file
## Adds conditional as arguments to select export formats
case $2 in
  pdf)
    export format="pdf"
  ;;
  html)
    export format="html"
  ;;
  docx)
    export format="docx"
  ;;
  *)
    echo "Error! To use: ./release.sh <version label> <target extension>"
    exit 1
  ;;
esac

pandoc -d $defaults $fulltext -o exports/Xe-1-v$1.${format}
