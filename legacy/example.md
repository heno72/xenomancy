---
title: Lorem Ipsum
subtitle: Dolor sit amet, consectetur adipiscing elit.
author: Hendrik Lie
reference-section-title: Daftar Bacaan
abstract: |
  Lorem ipsum dolor sit amet, consectetur adipiscing elit.
  Mauris ac porttitor dolor.
  Nunc in sodales arcu, vel bibendum ex.
  Quisque eu turpis pulvinar, condimentum tortor id, maximus purus.
  Nunc feugiat sit amet risus ac porttitor.
  Sed a quam neque.
  Morbi dictum euismod urna ac vestibulum.
  Suspendisse varius massa nec lectus tincidunt pharetra.
  
  Pellentesque eget blandit lacus, quis aliquet turpis.
  In in dolor vehicula, tempor erat eu, imperdiet ex.
  Nullam ut tellus gravida, finibus eros et, pulvinar felis.
  Donec quis nisi ut felis congue finibus.
  Vivamus ut fringilla dui.
  Etiam blandit erat ligula, sit amet volutpat mi lobortis at.
  Nulla ac orci a risus hendrerit vehicula nec a magna.
  Donec finibus nibh pellentesque, egestas ligula in, efficitur mi.
  Fusce eget turpis ut ipsum suscipit sodales eu a erat.
  Morbi nec ligula tortor.
---

# Chapter 1
Based on this preliminary tests, I came with realization that I like `chicago-fullnote-bibliography-with-ibid.csl` a lot for my own writings.
I will just have to remember this when I'm writing texts for my nonfiction books.
I also stored a few other selections of csl for us to choose from.
You know, just so we wouldn't be bored.

I thank Ubaya and Unair to make me love extensive use of footnotes.
Now, I no longer need to worry a lot about citation, I just have to remember their citation tables.
That is a table of their citation *id* and their corresponding articles.
No more worrying about citation mistakes, no more extra work to check if we get the citation right.
All is going to be automated, we just have to worry about the content, and its relevant metadata.

Bibliographical information would be made with mendeley.
Meanwhile the task of collecting academic papers and notetaking should be a lot more easier.
All because of mendeley, and pandoc.
I can also convert it to pdf if needed, also a html if I so want it.
I can use it for my fictions, or for the planned nonfictions.
Perhaps we can adopt this for work documents as well.
I think I will be using it for years to come.
The next paragraph will explore how the footnote citation style works.

Lorem ipsum dolor sit amet, consectetur adipiscing elit.
Mauris ac porttitor dolor.
Nunc in sodales arcu, vel bibendum ex.
Quisque eu turpis pulvinar, condimentum tortor id, maximus purus.
Nunc feugiat sit amet risus ac porttitor.
Sed a quam neque.
Morbi dictum euismod urna ac vestibulum.
Suspendisse varius massa nec lectus tincidunt pharetra.

Pellentesque eget blandit lacus, quis aliquet turpis.
In in dolor vehicula, tempor erat eu, imperdiet ex.
Nullam ut tellus gravida, finibus eros et, pulvinar felis.
Donec quis nisi ut felis congue finibus.
Vivamus ut fringilla dui [@Prodjodikoro1991, p.11.].
Etiam blandit erat ligula, sit amet volutpat mi lobortis at.
Nulla ac orci a risus hendrerit vehicula nec a magna.
Donec finibus nibh pellentesque, egestas ligula in, efficitur mi.
Fusce eget turpis ut ipsum suscipit sodales eu a erat [@Hernoko2016, p.192.].
Morbi nec ligula tortor.

Mauris lacinia, nisl et efficitur pellentesque, ante lectus efficitur augue, non volutpat odio lorem quis diam [@Prodjodikoro1991, p.9.].
Donec purus tortor, ultricies eu porta convallis, mattis ut libero.
Curabitur eget aliquam purus.
Vivamus felis dolor, volutpat a pellentesque a, blandit ut lacus [@Koh1989, p.3.].
Curabitur pharetra nunc at velit rhoncus bibendum.
Maecenas malesuada lectus at leo iaculis sagittis [@Koh1989, p.3.].

> Donec sed ligula a orci euismod consectetur vitae quis nunc.
In sollicitudin urna nunc, eget accumsan lacus euismod in.
Donec pulvinar quis augue ut maximus.
Nullam vel placerat neque, at finibus mauris.
Proin mattis sit amet quam eu vulputate.
Aenean pulvinar auctor neque eu accumsan.
Pellentesque ut neque sapien.
Aenean quam massa, pellentesque ac feugiat sed, egestas vel purus.
Aenean dolor nisl, tristique vel elit ut, placerat posuere nulla.

Curabitur purus risus, hendrerit quis ultricies vel, aliquam id leo.
Mauris facilisis ligula a massa feugiat, vel consequat tortor maximus.[^babi] Pellentesque id convallis augue, malesuada feugiat turpis.
Etiam urna ante, iaculis sit amet mattis eu, accumsan ut dolor.
Mauris efficitur mauris ligula [@Hernoko2017, p.21.].
Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos.
Mauris vitae urna ac lacus luctus auctor nec sagittis lacus.
Maecenas sed odio nec tortor molestie malesuada[@Hernoko2016, p.121.].
Vivamus aliquet convallis nisi, nec viverra dui ultrices eget.
Aenean semper ligula in tellus lobortis, vitae pulvinar sapien tincidunt.
Donec auctor ut quam nec dapibus.
Pellentesque quis commodo mauris, sit amet facilisis risus.

[^babi]: Mauris vitae urna ac lacus luctus auctor nec sagittis lacus.
   Maecenas sed odio nec tortor molestie malesuada.
   Vivamus aliquet convallis nisi, nec viverra dui ultrices eget.
   Aenean semper ligula in tellus lobortis, vitae pulvinar sapien tincidunt.
   Donec auctor ut quam nec dapibus.
   Pellentesque quis commodo mauris, sit amet facilisis risus.

Krismen [@Krismen2014, pp.10-11.] said that "Nunc ut eros nunc." Lorem ipsum dolor sit amet, consectetur adipiscing elit.
Etiam eget arcu quis libero vestibulum consequat quis vel sem.
Nulla a leo at enim gravida volutpat.
Nunc vel aliquet nisl.
Aliquam ipsum neque, mattis a vestibulum non, consequat in ligula.
Phasellus euismod dapibus tempus.
In ut lobortis sem, at tristique ante [@Koh1989, p.3.].

Lorem ipsum dolor sit amet, consectetur adipiscing elit.
Mauris ac porttitor dolor.
Nunc in sodales arcu, vel bibendum ex.
Quisque eu turpis pulvinar, condimentum tortor id, maximus purus.
Nunc feugiat sit amet risus ac porttitor.
Sed a quam neque:
"Morbi dictum euismod urna ac vestibulum."[@Sjahdeini2007, p.9.]
Suspendisse varius massa nec lectus tincidunt pharetra.

Pellentesque eget blandit lacus, quis aliquet turpis.
In in dolor vehicula, tempor erat eu, imperdiet ex.
Nullam ut tellus gravida, finibus eros et, pulvinar felis.
Donec quis nisi ut felis congue finibus [@Isnaeni2016, p.181.].
Vivamus ut fringilla dui.
Etiam blandit erat ligula, sit amet volutpat mi lobortis at.
Nulla ac orci a risus hendrerit vehicula nec a magna.
Donec finibus nibh pellentesque, egestas ligula in, efficitur mi.
Fusce eget turpis ut ipsum suscipit sodales eu a erat [@Hernoko2010, p.171.].
Morbi nec ligula tortor.

Mauris lacinia, nisl et efficitur pellentesque, ante lectus efficitur augue, non volutpat odio lorem quis diam.
Donec purus tortor, ultricies eu porta convallis, mattis ut libero.
Curabitur eget aliquam purus [@Gazali2010, p.19.].
Vivamus felis dolor, volutpat a pellentesque a, blandit ut lacus.
Curabitur pharetra nunc at velit rhoncus bibendum.
Maecenas malesuada lectus at leo iaculis sagittis.
Donec sed ligula a orci euismod consectetur vitae quis nunc [@Jamal2016, p.10.].
In sollicitudin urna nunc, eget accumsan lacus euismod in.
Donec pulvinar quis augue ut maximus.
Nullam vel placerat neque, at finibus mauris.
Proin mattis sit amet quam eu vulputate [@Hernoko2010, p.13.].
Aenean pulvinar auctor neque eu accumsan.
Pellentesque ut neque sapien.
Aenean quam massa, pellentesque ac feugiat sed, egestas vel purus.
Aenean dolor nisl, tristique vel elit ut, placerat posuere nulla.

Curabitur purus risus, hendrerit quis ultricies vel, aliquam id leo.
Mauris facilisis ligula a massa feugiat, vel consequat tortor maximus.
Pellentesque id convallis augue, malesuada feugiat turpis.
Etiam urna ante, iaculis sit amet mattis eu, accumsan ut dolor.
Mauris efficitur mauris ligula [@Hernoko2010, p.13.].
Class aptent taciti sociosqu ad litora torquent per conubia nostra, per inceptos himenaeos.
Mauris vitae urna ac lacus luctus auctor nec sagittis lacus.
Maecenas sed odio nec tortor molestie malesuada.
Vivamus aliquet convallis nisi, nec viverra dui ultrices eget.
Aenean semper ligula in tellus lobortis, vitae pulvinar sapien tincidunt.
Donec auctor ut quam nec dapibus [@Isnaeni2016, p.181.].
Pellentesque quis commodo mauris, sit amet facilisis risus.

Nunc ut eros nunc.
Lorem ipsum dolor sit amet, consectetur adipiscing elit.
Etiam eget arcu quis libero vestibulum consequat quis vel sem.
Nulla a leo at enim gravida volutpat.
Nunc vel aliquet nisl.
Aliquam ipsum neque, mattis a vestibulum non, consequat in ligula.
Phasellus euismod dapibus tempus.
In ut lobortis sem, at tristique ante.

# Chapter 2
This is a paragraph with an *italic* word and a **bold** word in it.
And it can even reference an image.

![An image will render here.](Pictures/Barong_Ket.jpg)

The purpose of this file is to serve as an example of markdown language.
I need to learn more about it though.

This line is a replacement for the previous line.
That is this:

Remember that the new paragraph will be shown only if you had an empty enter between lines.

# Chapter 3: Extended features (?) {#E2}

Most of information found in this section can be found on [here](https://www.markdownguide.org/extended-syntax/) for extended syntax, and [here](https://www.markdownguide.org/basic-syntax/) for basic syntax.
You can even have a [`cheat sheet`](https://www.markdownguide.org/cheat-sheet/)

Again, this is just an *example*, handle with **care**.

| Syntax | Description |
| --- | ----------- |
| Header | Title |
| Paragraph | Text |

You can align text in the columns to the left, right, or center by adding a colon (:) to the left, right, or on both side of the hyphens within the header row.

| Syntax      | Description | Test Text     |
| :---        |    :----:   |          ---: |
| Header      | Title       | Here's this   |
| Paragraph   | Text        | And more      |

We can input codes too without indenting it with four spaces or a tab.

~~~
{
  "firstName": "John",
  "lastName": "Smith",
  "age": 25
}
~~~

Here's a simple footnote,[^1] and here's a longer one.[^bignote]

[^1]: This is the first footnote.

[^bignote]: Here's one with multiple paragraphs and code.

    Indent paragraphs to include them in the footnote.

    `{ my code }`

    Add as many paragraphs as you like.
It would be nice if this could editor could support markdown.

    Apparently this markdown does not support footnotes.

This can even have [limited hyperlinks?](#E1)
No, it doesn't work well.

We can even do ~~strikethrough~~ here.

Even checklists.

- [x] Write the press release
- [ ] Update the website
- [ ] Contact the media

Even more, we can do this:

> a block text is displayed here.
> Not just that, we can add nested blocknotes.

> > Here is an example.

> > > And here's more.

By the way, I did install `language-markdown` package here.

And we can actually put [reference-style][1] links.
Even those with a label, like [here][2].

Even lists like this:

1.
Food means makanan
2.
This too
  1.
And can be indented
  2.
Followed with something more
  3.
Interesting isn't it.
    - combined with this.
    - and this.
3.
ANd the list goes on.
4.
Like This.

And some unordered lists.

- First item
- Second item
- Third item
- Fourth item
- First item
* Second item
- Third item
  - Indented item
  - Indented item
+ Fourth item

*   This is the first list item.

   > it is always nice to have a blockquote.

   ~~~
   Or even a code block.
   ~~~

*   Here's the second list item.

    I need to add another paragraph below the second list item.

*   And here's the third list item.

[1]: <https://en.wikipedia.org/wiki/Hobbit#Lifestyle>
[2]: <https://en.wikipedia.org/wiki/Hobbit#Lifestyle> (This is the label)


