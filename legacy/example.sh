#!/bin/bash
## This is a script to concatenate all relevant files into a docx.
## To use, run this script with an argument for its identifier. For example:
# ./Xe-1.sh L725.0350
## It will result in a file of Xe-1-vL725.0350.docx
## Command model is like this:
# pandoc chap1.md chap2.md chap3.md metadata.yaml -s -o book.html
## But that was for a html. Let's adapt it for markdown
## We can use this model instead:
# pandoc --reference-doc=Documents/L723-reference.docx Chapters/'Xe-1 Incursion.md' -o exports/Xe-1-Incursion.docx
## To add a toc, use --toc switch, but I don't see it working in docx if we open
## from libreoffice, yet.

## To convert to docx
pandoc --reference-doc=Documents/L726-reference-tesis-like.docx \
--csl=CSL/chicago-fullnote-bibliography-with-ibid.csl \
--bibliography=example.bib \
--citeproc \
example.md \
-o \
exports/examples/example-v$1.docx \
--verbose

## To convert to pdf
pandoc --toc \
--csl=CSL/chicago-fullnote-bibliography-with-ibid.csl \
--bibliography=example.bib \
--citeproc \
example.md \
-o \
exports/examples/example-v$1.pdf \
--verbose
