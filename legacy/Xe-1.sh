#!/bin/bash
## This is a script to concatenate all relevant files into a docx.
## To use, run this script with an argument for its identifier. For example:
# ./Xe-1.sh L725.0350
## It will result in a file of Xe-1-vL725.0350.docx
## Command model is like this:
# pandoc chap1.md chap2.md chap3.md metadata.yaml -s -o book.html
## But that was for a html. Let's adapt it for markdown
## We can use this model instead:
# pandoc --reference-doc=Documents/L723-reference.docx Chapters/'Xe-1 Incursion.md' -o exports/Xe-1-Incursion.docx
## To add a toc, use --toc switch, but I don't see it working in docx if we open
## from libreoffice, yet.

## Combining up the script manually
echo "TASK: Build the composer"
echo "TASK: (1) pdf composer"
echo "## To convert to pdf" > mods/compose-pdf
cat mods/pandoc.head mods/xe-1.cite mods/xe-1.flist mods/xe-1.pdftail \
>> mods/compose-pdf
echo "TASK: (2) docx composer"
echo "## To convert to docx" > mods/compose-docx
cat mods/pandoc.head mods/xe-1.cite mods/xe-1.flist mods/xe-1.docxtail \
>> mods/compose-docx
echo "TASK: (3) html composer"
echo "## To convert to html" > mods/compose-html
cat mods/pandoc.head mods/xe-1.cite mods/xe-1.flist mods/xe-1.htmltail \
>> mods/compose-html
echo "TASK: Build the composer ... COMPLETED"
echo "TASK: Make the documents"
echo "DOCUMENT ID: $1"
echo "MAKE: pdf"
source mods/compose-pdf
echo "MAKE: docx"
source mods/compose-docx
echo "MAKE: html"
source mods/compose-html
echo "TASKS COMPLETED! Check exports Directory"
