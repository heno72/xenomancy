---
## Consider not having fullcaps for headings.
title: Inspiration
## We don't need a subtitle for now
subtitle: Xenomancy version of Barong
## Information about authorship(s)
author: Hendrik Lie
date: 29 January 2020
## Fonts and formatting.
fontfamily: times
fontsize: 12pt
linestretch: 1.25
indent: true
papersize: a4
## Numbersections
numbersections: true
secnumdepth: 2
---

> I was thinking of creating a Xenomancy version of Barong

Darren Tsukimi
(Leader) Yukiohara Series
> Okay. How would you do it?

(Writer) Xenomancy
> The head is full of radiant color, mostly bright red and gold, and a large pair of intimidating eyes, a horn, and a large jaw. The body is covered in thick silky white hair-like furs, and a more pronounced head-neck hair like that of a lion. Walk in four slender and flexible limbs, it has a considerably long and flexible core body.

Darren Tsukimi
(Leader) Yukiohara Series
> How about purpose in the story?

(Writer) Xenomancy
> It is not a physical creature, so the physical size could vary according to the projection, and could phase out of manent realm to volant realm easily.

> The purpose?

> A way to defeat Kamaitachi

Darren Tsukimi
(Leader) Yukiohara Series
> Who is that?

(Writer) Xenomancy
> Kamaitachi is Anthony's strongest ally, a formerly dead volant signing a contract with Anthony as a payment for his freedom from hell

Darren Tsukimi
(Leader) Yukiohara Series
> So it’s an antagonist?

(Writer) Xenomancy
> It is a creature of strings, forming a superficially shark-like appearance, and the strings it is made of can cause enormous pain such that of a potent jellyfish sting. Both for physical beings and volants

You replied to (Leader) Yukiohara Series
**  > So it’s an antagonist? **
  
> Anthony is basically the antagonist on the earlier part of the story

Darren Tsukimi
(Leader) Yukiohara Series
> Okay

(Writer) Xenomancy
> So Barong is depicted as the king of spirits, and is the symbol of good deeds

> And it is covered in thick furs, so it got some considerable cover from the stings of Kamaitachi

Darren Tsukimi
(Leader) Yukiohara Series
> What does it do?

(Writer) Xenomancy
> I suspect, that Barong, among with Purpose, was in a relationship with Manov at some point in the past.

> Manov is a pansexual with free-spirit, and apparently in his journey to discover his true self, he had been in many places, real world and volant worlds

> And over many places, he developed connections with various creatures of both physical realms (such as Purpose) and volant realms (such as Barong)

> So it is like the antithesis of Anthony's relationship with his familiars. Anthony's relationship with his familiars is that of master-slave, and Manov's relationship with his familiar friends, is that of deep intrapersonal connection

Darren Tsukimi
(Leader) Yukiohara Series
> What does that mean for the kamaitchi?

(Writer) Xenomancy
> Kamaitachi sees Anthony as his liberator, so he owed Anthony his service. This also means he'd only act on a direct order. At certain moments, he wouldn't even initiatively protect Anthony if he's not ordered to.

> Kamaitachi would be indifferent on Anthony's life outside that of what Anthony specifically ordered

> Kamaitachi is heartless and calculative

> His kind is not that kind that "feel" emotions

> He was in hell because he fail to accomplish his previous enterprise and died because of that. He was liberated by Anthony because Anthony showed him how his legacy actually worked out the problem via the use of his other enterprises.

> Barong, in the other hand, will actively protect Manov, even without being ordered.

> And Barong is still alive, because Manov is not a necromancer

Darren Tsukimi
(Leader) Yukiohara Series

> How do they interact with each other?

(Writer) Xenomancy
> In a fight, Kamaitachi's usual method is by spreading thin to strings, forming what looks like a 3D version of a spider's nest. The string is infinitesimally thin on human scale, and if crossed, it might result in a very clean cut to the limb or body moving through a string, and also would provide an aftereffect such as unbearable pain. The cut part is unusually sealed, not bleeding.

(Writer) Xenomancy
> Often, his core would travel through the strings, and may manifest at any point of the strings, and it would finally reap the injured body parts. The body parts, albeit detached, still deliver pain stimuli to the original body.

> He would eat you piece by piece, while you experience all the pain, separate from the pain of cutting.

> Barong's hair is of an equal strength to the strings, so given a proper angle of attack, Barong could went through the nest, with the strings of Kamaitachi unable to touch the layer underneath the furs.

> Its weak spots would obviously the face of Barong, where hair is minimal, and its palm. So with a move akin of martial arts, Barong would try its best to protect those parts from contact with the strings of Kamaitachi.

> Kamaitachi would actively create new strings to capture and trap Barong. But Barong would fight back with its martial arts, preventing the strings from contacting its weak spots.

> Though the face is weak, the inner side of its mouth is immune, so it would also start munching the strings, sending the pain back to Kamaitachi

> At least that is what I have in my thought for now.

Darren Tsukimi
(Leader) Yukiohara Series
> I meant do how do they talk to each other?

(Writer) Xenomancy
> Oh,

> Since they're both technically volants, they talk through the plane of volantspace. In manent realm (physical realm), it looked like they talk telepathically

(Writer) Xenomancy
> So, the plan is, since the fight between Kamaitachi and Barong exist in both world, through the perspective of Barong, I could foreshadow the existence of a plane other than the physical realm on the early part of Xenomancy. Especially since the fight with Kamaitachi occurred early in the story

Darren Tsukimi
(Leader) Yukiohara Series
> Is one jealous of the other? Do they argue all the time or do they get along? Does one feel superior to the other?

(Writer) Xenomancy
> Barong's personality is childlike playfullness with strong curiosity tendency, while Kamaitachi is like that of an octopus: curious, cautious, and calculative.

> So most likely, both would experiment various forms of interactions,

> Barong would try to ask it for a play, and Kamaitachi might took it as a genuine attack, but then realized that Barong has no intention to damage him, and he would be puzzled by Barong's intent.

> Then Kamaitachi would try to "look like" attacking Barong, while Barong would take it as an acceptance of its offer to play, and then would continue playing

> Only when Kamaitachi concluded that Barong might not be a threat, but rather quite annoying, Kamaitachi would try to eliminate Barong.

> When Barong realized that it is being purposely wounded by Kamaitachi, Barong would be very upset, and would then move to aggresive mode. It is a lion after all

> Barong, like a lion, would target the head, or in Kamaitachi's case, the core.

> It wished a swift, and decisive elimination of Kamaitachi.

Darren Tsukimi
(Leader) Yukiohara Series
> What’s their relationship with those around them?

(Writer) Xenomancy
> Kamaitachi in his natural habitat, is mostly sessile, waiting for the prey to come to them. They only move to other places if food is scarce. His kind does not form social structures. They encounter each other either for a fight or for mating.

(Writer) Xenomancy
> Barong is a social creature, and usually travel together within a pack of about two to four, usually on the same gender. When they meet other group of an opposite gender, they might ritualistically fight with the other gender, trying to seduce the potential mates out, and if they're lucky, all of them get to mate with the members of the other pack.

(Writer) Xenomancy
> After a group mating, the two groups part ways. The child bearer pack (or female in our term) would raise their children as a group. When the children reached sexual maturity, the bearer-gender (or female in biological term) would stay within the group, or the group would split, while the seeder-gender (the male in biological term) would roam about. When they meet other individuals of the same gender, they'd befriend each other. Once they're comfortable with each other, they'd travel together in a group.

(Writer) Xenomancy
> So naturally, Barong is very friendly

> While Kamaitachi usually is territorial and does not hesitate to harm other beings or even their kinds

(Writer) Xenomancy
> I did not use traditional genders for Barong because they have very little sexual dimorphism (so the bearer and the seeder gender has no difference in capability), and they're not biological, rather they're volant beings. Call it spirits if you like, but I like to think that they're just a data pattern.

(Writer) Xenomancy
> Or, digital life.

Seen by Billie Theodorus at 11:47 AM

> However, usually when we manifest in their realm, we would perceive it as a large savana

> While Kamaitachi's habitat would look like that of the deep sea, vibrant with bioluminescence

Darren Tsukimi
(Leader) Yukiohara Series
> They do or don’t have genders?

(Writer) Xenomancy
> Kamaitachi's civilization is built of social units called enterprises. This is their mating strategy. So a group would collect in what we would consider as orgy, because multiple individuals engaged in a mutual connection with one another, forming a larger entity called "enterprises", that basically becomes a platform to generate and raise their younglings, tending them to adulthood, and releasing them to the wild. When all of the succesful younglings released to the wild, the "enterprise" disbanded and they returned to their solitary life.

Darren Tsukimi
(Leader) Yukiohara Series
> So they don’t have genders?

You replied to (Leader) Yukiohara Series
**  > They do or don’t have genders?**
  
> They do have gender, but it is different than that we have in human culture

> Wait

> Barong has two gender, the bearer and the seeder.

> Each Kamaitachi individuals has all the gender, but needs other individuals to form "enterprises"

Darren Tsukimi
(Leader) Yukiohara Series replied to you
**  > Barong has two gender, the bearer and the seeder.**

> So he’s a flower?

> Please get that joke

(Writer) Xenomancy

> At least three Kamaitachi must merge into an "enterprise" to raise their younglings.

> I get the joke

> But I must attribute it to my poor explanation

> Barong as a species has two different genders, but in an individual, they only have one each.

You replied to (Leader) Yukiohara Series
**  > So he’s a flower?**

> Kamaitachi is a flower

> Because they have all the gender there is in a body

Darren Tsukimi
(Leader) Yukiohara Series
> Could they an own offspring by themselves?

(Writer) Xenomancy
> Neither Barong or Kamaitachi can do that. They always need other individuals to help them. In Barong's case, they need a seeder seeded a bearer for the bearer to become pregnant. For Kamaitachi, despite having all the gender, they need at least three to mate in a process called "enteprises"

Darren Tsukimi
(Leader) Yukiohara Series
> And how does work for them?

(Writer) Xenomancy
> Work?

> Do you mean, what do they do for living?

Darren Tsukimi
(Leader) Yukiohara Series
> I mean how does three of them work? For humans it takes two.

You replied to (Leader) Yukiohara Series
**  > I mean how does three of them work? For humans it takes two.**
> at least three Kamaitachi becomes a single entity called an "Enterprise" to produce children and raise them. Why they need three, is because they need diversity. More individuals means more randomness, random combination of traits for their children. Each child is an unique mix of traits. And by raising, I meant training and selections. The Enterprise would decide traits they see useful for their species' survival, and then a huge number of children they produced, each unique to one another, is selected, and then is filtered through a series of training and tests. Non-viable children would be reabsorbed by the Enterprise for food of other viable children.

(Writer) Xenomancy
> They need at least three, and often choose to form enterprises, whose total number of joining individuals would be odd. So when they need to take a vote on certain traits, there would never be a tie
The largest one would amounting to about a thousand individuals forming a single, massive enterprise

Darren Tsukimi
(Leader) Yukiohara Series
> How exactly does the creation process work?

You replied to (Leader) Yukiohara Series
**  > How exactly does the creation process work?**

> Since they tangled themselves with each other during the formation of an enterprise (remember, appearance-wise, they're just a collection of coiled strings), they'd be in contact with one another. They exchange informations, and traits that made their individuals were detached, restructured, and recombined into a new, unique combination, that would then buds off, and detached from their "enterprise," forming "subjects"

(Writer) Xenomancy
> "subjects" are then selected against defects and handicaps, and the viable "subjects" would undergo training and tests.

Darren Tsukimi
(Leader) Yukiohara Series
> I think I get it

(Writer) Xenomancy
> Yes,

> Something like that

> Usually, it is preferred to form enterprises with older Kamaitachi individuals, because then they'd be more experienced on determining training and tests useful to produce viable offsprings.
> Younger individuals may join as our equivalent of "internship" in the "enterprise"
