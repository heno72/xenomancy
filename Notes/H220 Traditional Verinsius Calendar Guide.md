---
title: Inspiration
## We don't need a subtitle for now
subtitle: Traditional Verinsius Calendar Guide
## Information about authorship(s)
author: Hendrik Lie
date: 20 February 2017
## Fonts and formatting.
fontfamily: times
fontsize: 12pt
linestretch: 1.25
indent: true
papersize: a4
## Numbersections
numbersections: true
secnumdepth: 2
---

- A solar year is 407.14 days long.
- An Evenyear is 408 days long. It is composed of Normal Quadrant, Deviant Quadrant, Normal Quadrant, Deviant Quadrant.
- An Oddyear is 406 days long. It is composed of Normal Quadrant, Normal Quadrant, Deviant Quadrant, Normal Quadrant.
- An Anomalous year is 407 days long. It is composed of Anomalous Quadrant, Deviant Quadrant, Normal Quadrant, Deviant Quadrant.
- A Normal Quadrant is 101 days long. It is composed of 4 subquadrants. The first one is 26 days long, and remaining three with 25 days each.
- A Deviant Quadrant is 103 days long. It is composed of 4 subquadrants. The first three are composed of 26 days each, and the last with 25 days.
- An Anomalous Quadrant is 100 days long. It is composed of 4 subquadrants, each is 25 days long. 
- A Quartergeneration is 7 years. A Quartergeneration is composed of Evenyear, Oddyear, Evenyear, Oddyear, Evenyear, Oddyear, Evenyear, and always is in this order. Unless when the Quartergeneration is anomalous, the first Evenyear is replaced with an Anomalous year.
- A Supercycle is a period of 350 solar years. It is composed of an Anomalous Quartergeneration followed by 49 normal Quartergeneration.
- A Generation is 28 years.
- A week is 8 days long.
