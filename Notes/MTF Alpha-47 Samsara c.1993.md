# MTF Alpha-47 "Samsara" in 1993
A task force of DoN, GaFE.
Specialized in dealing with the Apex Souls.

Led by Sergeant Chandra in 1993.

It was a section comprised of 3 fireteams.
The structure is as follows: a designated team leader, an automatic rifleman, a grenadier, a rifleman.

## 1st Fireteam (The Iron Fists).
  1. Sgt. Chandra "Chan" Watthuprasongkh ("จัน" จันทรา วัตถุประสงค์) (m, 26).
  He is very close with Michael (calling him Nong Mich), as they went into training at a similar time, and spend years together.
  Highly determined to do his jobs, very friendly to his squad, and knows every single one of them very well.
  However, he's very emotionally attached to his squad, that losing a squad member is almost always devastating for him.
  1. Pvt. 吳脚踏 (Go Jiaota) (f, 23), usually called Ms. Go.
  A calm, quiet, and stern person.
  Dislikes unorganized actions, and always executes everything with precision and determination.
  Wouldn't twich for every soul killed.
  Technically, she is also a medic.
  [KIA: by Juan Muerte, when she's stationed to guard one of the tunnel's opening where they extracted the body of A Ping and A Chen.]
  1. Pvt. 王实地 (Wang Shidi) (m, 24), usually called Wang.
  A highly reliable wingman when needed.
  However, he's quite messy when it comes to his own belongings.
  Does not hesitate to jump and save his colleagues, even when it may cost his life.
  [KIA: When Zarah shot a disturbtor beam toward Sgt. Chandra, he jumped in place of Sgt. Chandra and disintegrated into dusts].
  1. Pvt. Djunita (f, 24), usually called Djun.
  A bright and cheerful lady, always trying to liven up her squad by throwing jokes (often bad or fail, but, well).
  Never wish to hurt anyone, and always try to get close with everyone.
  [KIA: by Juan Muerte, when she's stationed to guard one of the tunnel's opening where they extracted the body of A Ping and A Chen.]

## 2nd Fireteam (The Wizards)
  1. Pvt. Michael "Mich" Carmichael (m, 24).
  Is Integra Hendrik's previous incarnation, and is a Brahma soul.
  An agile reality bender, that can perform small scale minor reality bending relatively quickly.
  A hard and wide scale reality bending requires more time of focusing.
  [KIA: When finally come face to face with Juan Muerte, a Shiva soul, they fight with their esoteric powers while their powers annihilate one another.]
  1. Barong Ket, usually called Ket.
  His human form is a lean but strong south asian male martial artist.
  His beast form is a male lion.
  Specializes in aural attack, projected energy, shielding techniques, and strategic planning.
  Can teleport, but only toward a spot in his direct line of sight.
  If he can't see it, he can't teleport there.
  1. Barong Bangkal, usually called Bangkal.
  His human form is a heavily built aryan male.
  His beast form is a babirusa.
  Specializes in strength, and brute force.
  1. Barong Macan, usually called Macan.
  His human form is a strong and fit javanese male.
  His beastform is a Bengal Tiger.
  Specializes in sensory acuity, clairvoyance, camouflage, and navigation.
  Can teleport in rapid succession, and is very precise at doing so.

## 3rd Fireteam (The Harem).
  1. Pvt. Kang Haein (강해인) (m, 20), usually called Haein (해인).
  A bright, cheerful, and playful person.
  Barely shows his emotions, always mask it with smiles and positive vibes.
  Very perceptive to other individuals, and highly sensitive to changes in mood, and emotional distress.
  1. Pvt. Angelica Gears (f, 22), usually called Gears.
  She is highly perceptive, a perfectionist, and like to tidy her belongings and appearances.
  Very adaptive and resourceful in battle.
  1. Pvt. Ninawati Berlina (f, 21), usually called Watt.
  A strong woman, and a very precise grenade thrower.
  [KIA: by Juan Muerte, as she shot him after he wounded Ket.]
  1. Pvt. Emma Bright (f, 20), usually called Emma.
  A hypersex and playful woman.
  At the same time being very emotionally matured, high self control (except for sex), and very intuitive.
  Also specifically trained to be a paramedic.
