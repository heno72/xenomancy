# Alteration 20200612
ตี๋ อาทิตย์ วัตถุประสงค์ ("Tee" Arthit Watthuprasongkh).
Sumberbulu Krajan.

Son of "จัน" จันทรา วัตถุประสงค์ ("Chan" Chandra Watthuprasongkh) and 吳脚踏 (Go Jiaota).
A friend of Charles Lee.
His nickname is Tee (ตี๋), which is a rare noun translation of Chinese boy.
His mom called him as such because he had an older brother nicknamed ต๋อง (Tong), and calling him Tee, sounds similar to Chinese calling for a little brother: 弟弟 (didi).

In messaging, his father always typed ตี๋ to refer to him, and his mother always typed 弟 to refer to him.
He always called his brother 哥, despite his father's request to call his brother พี่, partially because he felt more comfortable to call with chinese calling, as his father was on duty most of the time.

Fluent in Indonesian, English, Mandarin, Korean, and Thai.
Indonesian is obviously because he lives in Indonesia.
English because of the internet.
Mandarin because of his mother, Thai because of his father.
Korean, because, apparently, the sons of 강해인, his father's best friend, lives nearby, and they're good friends.

So, four of them usually hang out together:
- Charles Lee, Tee Arthit, 강세형, and 강세찬.
Charles, an avid motorbike and automotive fan that is also an environmentally conscious person, is described to be very matured, calm, but very social.
- Tee Arthit, a dance machine and a singer that loves to learn new things for fun, is described to be playful, love to tease others, and has a sweet personality.
- 강세형, a very vocal and expressive individual that is quite random and unpredictable, is described to be lively, love to laughs and make others laughs.
- 강세찬, a playful and a very creative problem solver, is described to love throw pranks to others, sometimes overly analytical, and love to tease his brain (though he didn't always understand it by the end).

Because Charles found him to be engaging intellectually, he must be comparably a genius too.
Probably 170-180 IQ range, perhaps 173, a prime number.
Perhaps also reminding him of Anthony, so probably also a singer and is a good dancer, and speaks Korean.
Perhaps a k-pop lover? So what would be the manifestation of that high IQ range to a K-Pop lover? Perfect kinesthetic sense, can easily acquire languages just by watching shows? And eventually, mastering etoan language, and comprehending etoan culture far more than any humans, just in their short interaction.

So, that actually delve deeper into the psyche of a Charles Lee.
And eventually, as Tee grew more familiar with etoan culture and language, Tee becomes more etoan than Charles is, and Charles becomes more human than Tee is.
Until Tee reminded Charles of that, and Charles returned on track.

Post-Charles age, three of them joined **OGRE Corporation.**
Tee becomes a Management Trainee of OGRE Manufacturing in Medan, eventually crossed his path with Daniel Ashton during Medan insurgency.
Se-Chan becomes the Engineering Resident.
Se-Hyeong becomes the Public Relation Officer.

Ps.: I think Abel should be changed to Charles.
