---
## Consider not having fullcaps for headings.
title: Inspiration
## We don't need a subtitle for now
subtitle: Daniel-Fernando
## Information about authorship(s)
author: Hendrik Lie
date: 29 August 2022
## Fonts and formatting.
fontfamily: times
fontsize: 12pt
linestretch: 1.25
indent: true
papersize: a4
## Numbersections
numbersections: true
secnumdepth: 2
---

Dbk but petekao swapped: Pete is Daniel, Kao is Fernando.

I think the concept would be, Fernando examines the likelihood of Steven likes him back with bayesian analysis. Daniel wasn't originally his goal. However Daniel keeps asking Fernando to help him get Helena.