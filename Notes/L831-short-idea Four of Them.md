---
title: Four of Them
author: Hendrik Lie
## Fonts and formatting.
fontfamily: times
fontsize: 12pt
linestretch: 1.25
indent: true
papersize: a4
---

"He's stupid!" Screamed Stone.

Steve wasn't sure how to respond.
Telling Stone to tone it down didn't sound appropriate.
At least not at the time.

# META

The aim is for something 2000-2500 words long.
The source is [Grandpa is My Classmate](../Chapters/Xe-1%20Grandpa%20Is%20My%20Classmate.md).
Therefore we must not digress too far.
The aim is then just to show, two kinds of friends.

One like Helena, and another like Steven.
One is not sincere, the other is sincere.
One is full of agenda, the other is just is.

Progression, I'd probably use *in media res*.
Start with the fight between Sun and Stone, on the preparation of Lena's birthday.
The four of them, sounds like a good title if not because it's been used already.

So, first, the characters.

1. Sun, he thought that he's doing it for the best of both of them. Little did he knows that he disregarded the sacrifice made by Stone.
2. Stone, he thought that of all people, Sun should know him better.
Little did he know that what Sun needs is just that Stone tell Sun directly.
3. Steven, is not very complicated.
If you're his friend, he'd love you with all of his heart.
If his friends need help, he'd give his all to help them.
A little puppy by heart.
4. Lena, is generally a good friend, if she's not having her own interest.
When she has her own interest though, she's willing to compromise for her own needs.

Okay, here's the progression:

1. Fight between Sun and Stone on the preparation of Lena's birthday party. Lena told Steven to go with Stone, while Lena accompanied Sun.
2. Sun and Lena, talking about why Sun decided that they should separate. Lena, still having feelings for Sun, decided to support Sun's decision to separate.
3. Stone and Steven in a dessert cafe, talking about how stupid Sun is, that Sun thought he's doing it for the best of them, disregarding what Stone felt about the situation. Steven, being a good listener, finally asked if Stone ever actually communicate what he did to Sun.
4. Sun overheard the conversation of Lena's and Steven's son and a friend about love. It brought him to Stone, and finally they reconcile as Stone told him about his choice.
5. Lena was not satisfied, as her act was fruitless. Steven is happy to help his friend, so decided to share the joy with his wife, on bed. Lena's heart, however, is somewhere else.

# RAW

Daniel and Fernando got into a conversation about their future, and how Daniel said, that if he had a child, he'd want to have a boy.
Daniel got a call from his parents, asking him how he's doing with finding a girl to marry, given that he's quite old already.
Daniel reminded his parents that he's in love with Fernando, but his parents refuse to acknowledge it.
Fernando overheard it, and decided that he should back down.
Daniel asked him why he's doing that, and Fernando keeps repeating that he could not give Daniel what he wanted, a boy.
Daniel said it doesn't matter, he's in love with Fernando anyway.
Fernando insisted that he's doing it for Daniel's own good.

On the preparation for her birthday, she requested help from Fernando and Daniel, but she realized that Fernando and Daniel are being distant with each other.
Daniel keeps looking for a chance to meet Fernando, while Fernando is making himself busy.
In the end, Helena requested Steven to take care of Daniel, and converse with him, while she conversed with Fernando.
Ultimately, Fernando expressed his concerns that Daniel wanted a boy, but same-sex couples in Indonesia could not take surrogate mothers, nor do Fernando's ethics agreed with it.
They couldn't even adopt a child legally, especially because they're nothing in front of the law, and that the law itself specifically mentioned that same-sex couple couldn't adopt a child.
Helena took it as a chance to converse at a great length with Fernando, and she's being greedy.
She realized it, but decided to close her eyes on this matter.
She said that she believed Fernando did what he thought was the best for both of him and Daniel.
Fernando nodded.
Helena convinced herself that she's just being there when he needed it, being his friend, and support whatever decision he made.
But her conscience said that she is just saying what he wanted to hear.
She dismissed that when Fernando hugged Helena and said thank you for listening to his rants, and for understanding him.
She is being greedy, her conscience shouted.
She's an animal, she wanted everything, and it wasn't her fault, it was her inner animal, she said to herself.
For a moment, she forgot that she had Steven, and she wanted more, but Fernando ended the hug, and continued helping her preparing the dinner menu.
She wanted more, but she's back to reality now, she couldn't get more.

Daniel and Steven went for a drink that day, given that it was saturday, and they have no need to attend anywhere as their clinic is closed that day.
After several lengthy conversation, Daniel explained to Steven that Fernando backed down because he said he couldn't provide a son for Daniel, and Daniel's parents, and that Daniel should be with a woman, before it is too late for him.
Steven asked if Daniel really wanted a child, and Daniel replied that four of them, after all of this years, have already raised four healthy boys, together.
Daniel is willing to take it as having children of his own, as it was the children of his best friends.
He said that it is sufficient for him already, as he already feel how it was like to be parents.
But Fernando couldn't see it that way, he thought Daniel wanted a son of his own.
Steven asked if he ever expressed that feelings to Fernando, on which Daniel said that he assumed Fernando knew it already.
They've been together for too long, Fernando should knew about it.
Steven said that the fact that this happens, means he's completely clueless about it, on which Daniel turned silent.
Steven said that exchanging codes is not the best method of communication, and Daniel nodded.
Daniel cried, and said that he'd kiss Steven if not because he's with Fernando, and they bursted into laughter.
They're having a good time that evening.

Helena's 36th birthday party (26 Maret 2022).
The family and friends are supposedly having dinner that day, and David couldn't run away but had to sit beside Ashton, as other seats were taken already.
Fernando, that was walking about, happen to overheard the conversation from outside David's room, and it intrigued him to stay there and listen more.
Fernando put his ears closer.

If you love someone, and that someone love you back, what stops you from being together?
Nothing.
Because it is wrong?
Love is never wrong.
Because of what others might say about your love?
Then the problem is not in their love, but on the fact that they let others ruin their love.

Fernando stunned.

Daniel happen to stumble on Fernando, and he noticed Fernando's crying, silently.
"Nando?"
"..., Daniel"
Fernando hugged Daniel before he could even say anything.
Fernando cried on Daniel's shoulder, and Daniel knows nothing more to do but to pat Fernando's back.
"I'm sorry, Daniel, I'm sorry"
Daniel nodded, though he realized Fernando wouldn't be able to see him nodded, and said, "Don't worry, I don't mind."
Fernando keeps apologizing, and Daniel just hugged Fernando tighter.
They gaze each other now, and kissed.
Daniel didn't say a word.
He's happy.

Helena happen to see Daniel and Fernando kissed on her way to the bathroom.
She said to herself, that she's happy seing them reconciling, they seemed happy.
But her heart aches, "but, why does it hurt so much?" she mumbled to herself.
"Mom, oh-" Peter came across Helena and saw Daniel and Fernando kissed.
Helena closed his eyes and dragged him out of the scene, while hiding her heartaches.
That night, Helena almost cried, realizing, again, that she never owns Fernando's heart.
Steven went out of the shower room, and in his robe, he approached Helena.
He is giggling.
Helena looked at her husband, and her husband revealed his whole package.
She didn't want it tonight, she said to herself, but replied with a smile.
Steven said that "it is up, what say you?"
"What say you?" Helena said, forcing a giggles.
She didn't feel like she deserved Steven.
She felt guilty for betraying this innocent, cute, and hot boy that called himself her husband.
He is.
They have four children already.
With his wide grin, Steven approached Helena, Helena backed slightly, but tried not to make it visible enough.
"Stop, Steven, I didn't deserve you," Helena said to herself, but didn't let it out.
But then Steven's lips melt into her lips.
Their tongues twisted against each other, rubbing against their central incisor, their superior labial frenulum, inferior labial frenulum, and then palpitating their lingual frenulum, their hard palate and transverse palatine folds.
She couldn't think a word, her mind is blank.
Steven's lips disengaged from her lips, but her lips asked more.
It was before Steven smooched her neck, toward her sternal notch, her manubrium, her sternum.
There was a turnaround near her pectoral muscles, and she could feel her lobule manipulated, and her areola.
Steven asked if she wanted him to continue on his way down, and Helena couldn't think anything but, "yes, continue, please"

She no longer feel the bed, not even the bedroom.
She felt like she's melting, and she could no longer differentiate between his tongue, his fingers, or his wiener.
She felt like in a wave, a warm current, and being covered in sprinkles.
Twitching, twirling, turning.
Palpating, pulsating, prostrating.

"I'm an animal, I want to have everything," she repeated in her thought.

"I want to have this carnal pleasure," her thought continued.

"I want to do it with Fernando," said her thought again.

When she's finally awake and sobber, Steven was lying beside her.
His face seemingly smiled on his sleep, something she'd recognize right away even in the middle of the dark.
She observed Steven's sweet, childish, and at the same time, strong and firm face.
She ran her finger across his well-defined jawbone.
His face is smooth, almost youthful, wrinkle-free.
People would believe her if she said he's her son.

She's probably the luckiest among her peers at around her age.
At her 36th birthday, she could still enjoy a flawless, high-performance sex from her husband.
But all she could feel is a deep regret, a guilt that tore her heart so deep.
All she could think of is Fernando.

Why, she said in her prayer, why would she want someone else when she's having a husband that every woman her age dreamed of?

"Why do I have my heart on someone else that's already been taken, not to this man here that love me with all of his heart?"

Steven stretched a bit, and asked her, "what are you doing?"

Startled, she said she wanted to go to the bathroom.

Before she could lift her body off the bed, Steven already mumbled, "I love you."

Helena smooched him on his forehead, "I love you too."

That word came out without hesitation, without resistance.

"Did I mean it?" she said to herself.

She could see that Steven smiled peacefully.

His innocent face torn her heart even more.

"oh God, what is it that I really want? What is it that you want me to do?"
