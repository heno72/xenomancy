# Climate Zones

## Precipitation

| High | Low |
| :--- | :--- |
Low pressure area (ITCZ, PF) | High pressure areaa (subtropical ridges)
Onshore winds | Offshore/parallel winds
Warm currents | Cold currents
Windward side of mountains | Leeward side of mountains
\- | Interiors

## Temperature

| High | Low |
| :--- | :--- |
Equator | Poles
Interiors | Coasts
Large temperature variation inland | Smaller temperature variation near coasts
Low altitude | High altitude

## Zones

| Zone | Temperature (summer) | Precipitation (winter) | Location (lat) |
| :--- | :--- | :--- | :--- |
Tropical Rainforest | Hot, wet | Hot, wet | 0-10
Tropical Monsoon | Hot, very wet | Warm, dry | 5-15/20
Tropical Savannah | Hot, wet | Warm, dry | 5-15/20
Hot Desert | Very hot, dry | Warm, dry | 10-30
Hot Steppe | Hot, low-dry | Warm, low-dry | 10-35
Humid continental | Warm-hot, moderate | Cold-very cold, low-dry | 40-60
Subarctic continental (boreal/taiga) | Cold-mild, moderate | Very cold, very low | 45-75
Mediterranean | Hot, dry | Mild, moderate | 30-45
Humid subtropical | Hot, wet | Warm-mild, moderate | 25-45
Oceanic (maritime/marine_ | Warm-mild, wet | Cold, wet | 40-60
Cold desert | Hot, dry | Cold, dtry | Interiors, rainshadows
Cold steppe | Warm, low-dry | Cold, low-dry | 25-50
Polar Tundra | Cold, low | very cold, dry | 60-80
Polar ice caps | Very cold, low | Very cold, dry | +75

