---
## Consider not having fullcaps for headings.
title: Inspiration
## We don't need a subtitle for now
subtitle: Derictor-Nurhayati Scene
## Information about authorship(s)
author: Hendrik Lie
date: 08 August 2022
## Fonts and formatting.
fontfamily: times
fontsize: 12pt
linestretch: 1.25
indent: true
papersize: a4
## Numbersections
numbersections: true
secnumdepth: 2
---

There's this interesting scene I had as an idea:

A villain came to held a woman, aiming his gun to her.

A protagonist came and confronted the villain, but the villain warned that he might or might not hurt her.

Then, in a calm face, the protagonist said: that's true. But consider this, if you kill her, I have nothing to stop me on killing you. If I shot you, there's a chance she might die. But her death is not the most immediate danger, but your death. Either way, she might die on your hand or on mine, but in both cases, I would have nothing to stop me from killing you.

The villain considers, and the protagonist said once more, "the most sensible action you can take right now is to let her go, and I might or might not hurt you."

Don't force me to kill her, said the villain, and the protagonist said, give it a try, and he'd find the answer.

As the villain is processing it, the protagonist shouted, "지금 치워야!" (get rid of it now) and the woman dislodged the gun's barrel and disarm the villain.

At first I could have Hendrik and Matahari living up the scene. But then, Matahari and Hendrik are reflexior, it makes little sense. Then I came across Derictor and Nurhayati as the next logical option. They're both so used with actions, and both are humans. It makes more sense.
