---
title: Inspiration
## We don't need a subtitle for now
subtitle: Aucafidian dating system
## Information about authorship(s)
author: Hendrik Lie
date: 13 October 2020
## Fonts and formatting.
fontfamily: times
fontsize: 12pt
linestretch: 1.25
indent: true
papersize: a4
## Numbersections
numbersections: true
secnumdepth: 2
---

I discovered that 0.0001CL is a convenient time unit, as it is roughly about an hour of our time. Below are some factoids I gathered so far:

- So 0.01 local day in Aucafidus is a unit time of about 813 seconds, or 13m33s.
- So 0.01 local day in Aucafidus is 2.45733099291101e-5 CL long.
- So 0.0001CL is a unit time of about 3308.46630334854 seconds, or about 55m8.47s
- So 0.0001CL is a unit time of about 0.040714 local day long. That makes a local day to be around 0.002456CL long.

As can be seen, I think in everyday life, 0.0001CL (0.1 permil) is a convenient time chunks. 1 percent d_au, that's about 13 and a half minutes, can also be used to describe a shorter time period. But societies that use them are different, though all do constitutes Aucafidian civilization.

## SPACEBOUND CIVILIZATIONS

Spacebound civilizations would tend to use CL time units, as it is a metric system with 1CL is exactly 407.14 d_au. There, a d_au is about 2.46 permil of a CL, usually described to be ~0.0025CL. I already described the Standard Verinsius Calendar, that divide a CL to be exactly 407 days, but this provides a new alternative. If a CL is to be described as exactly 400 days, a day is about 0.0025CL. Perhaps it is to be called a Convenient Time Unit.

I think we could also consider the nature of their numbering system. They use decimal system like us. They also have order of magnitudes (the {n}-jaris-rak-{m} system for n*10^m). But this requires another treatment beyond this treatment.

CL is traditionally called Verinsius, even if it is written as CL in Angin script. So multiples of 0.0001 is commonly used. It is 10^-4, so jarisrakdererenato. Popularly jarisrak is shortened to jarak, so Jarak Dere Ran. 0.0025CL is often called (grammatically inacurate) votirjar sin jarak dere ran Verinsius. Perhaps it is too inconvenient, we need to invent a new unit. 1e-4 of a Verinsius should be called Jadan, especially if it is commonly used for dating. So to say 0.0025CL, they'd say votirjar sin jadan (25 Jadan).

Note: There must be a guide for Etoan numbering system. I don't know the name of their equivalent of decimal convention. We can't just use the word "point" as we use in English. Oh, nevermind, I invent a new word for the concept, Horsid. So 25.79 would be called Votirjar Sin Horsid Ra Eni.

So a space-day (Askitos, lit: artificial day) is 25 Jadan (votirjar sin jadan) long. A jadan is divided into 5000 derajat. Derajat is an etoan time unit, of about 2/3 seconds long.

Their usual date-time convention is Year-ASK-JD, for example 27th September 2020, 10:57 WIB, or 67,550.1657CL, is written as: 67550-066-07, while in traditional calendar it is 67550-03-18.48. An extra-aucafidus individual would say (assuming the year is recent and doesn't have to be mentioned): Askitos daljar dal (66), Jadan ra (7).

Fun fact, if Askitos or Jadan is said before a number, they meant a specific date or time. If it is said after the number, they meant unit of time. For example, Jadan ra is the 7th Jadan in an Askitos, while ra Jadan means it is a period with the length of 7 Jadan.

It is similar to their numbering system, where for example 40 is 4x10, so is said ranjar, while 14 is 10+4, so is said jar ran. Likewise, 47 is 4x10+7, so is said ranjar ra. Jadan, or Askitos, is treated like 10 in the former 2 examples.

## MODERN SURFACE AUCAFIDIAN

Modern Aucafidus Surface Inhabitants would prefer to use traditional local days, so they also use Traditional Calendar. So their time units would be most likely based on fractions of a day. So they count a day with percentiles, 0.01 d_au is the preferred chunk of time. d_au is called tosit. So 0.01 tosit is called jaris rak dere votiri tosit, perhaps abbreviated to jarakdevotos. In modern day it would be jarwat. A tosit is comprised of 100 jarwat. A jarwat is comprised of 1000 arsin, each is about 0.813 seconds long.

Their usual date-time convention is Year-SQ-DD.JW (SQ: SubQuarter (not yet named); DD: Tosit; JW: Jarwat). For example 27th September 2020, 10:57 WIB, or 67,550.1657CL, is written as 67550-03-18.48. 

We don't have a way to say subquarter, so why don't we invent one? Jarwat 48 is easy to be said, and 18.48 could easily be said as Tosit 18 Jarwat 48. The 3rd Subquarter, is tricky. I think the closest I could get for subquarter, which is basically 1/16th part of a year, is horis {1/16th}, where cardinal version of 1/16th would be: atja-sido-hitme-jar-dal. Obviously, calling it: Horis-atja-sido-hitme-jar-dal (lit: bagian ke satu per enam-belas) is not very convenient.

Perhaps, Avit, or region, could be used instead. It could be constructed as "a year's region", so visius. Visius is generally considered an object, not a unit of time. And the subquadrant number is always considered a cardinal number, so we can say sid-los visius (3rd Visius), or atja-jar-voti visius (12th Visius), but never los Visius (3 visius as a length of time like we would say 3 months) or visius los (an incorrect way to say 3th Visius).

To say something like "we'd do it next month" they'd just say approximate time period in the future. For example, "we'd do it around two hundred days in the future" or "we'd get it done within two hundred days."

So let's get back to 67550-03-18.48. In full term, it would be expressed as:

Verinsius dal jarak-ran ra jarak-los sin-pontia sin-jar (lit: year 67550)
sid-los visius (lit: 3th subquadrant)
Tosit jarcan (lit: Day 18)
Jarwat ranjar can (lit: Jarwat 48)

Note that like Jadan and Askitos, if Jarwat or Tosit is is said before a number, they meant a specific date or time. If it is said after the number, they meant them as units of time. So, tosit jarcan is Day 18, but jarcan tosit is a period of 18 days long.

An exception is Verinsius. If it is said before a number, it always means traditional calendar year for aucafidus-surface dwellers. But if it is said after a number, it is always an astronomical year (407.14 d_au long). For space-bound etoan, Verinsius always mean a period of time that is 407.14 d_au long, and behaves just like Jadan, Askitos, Jarwat, and Tosit.

## COMPLICATIONS

I tried to find a practical unit of time for all of them, but apparently, it resulted into two line of time units, small enough it is comparable to a second. From space-bound etoan, we got derajat (around 2/3 seconds long), and from aucafidus-surface-inhabitants prefer to use arsin (about 0.813 seconds long). So for physics, they'd still use the standard time unit: sternus (about 1.23 seconds long).

Also, I discovered that the standard unit of time, Sternus, that is described to be around 1.23s long, might have used a wrong value of seconds. Because it was calculated with the assumption that a solar day in Aucafidus is 80625.2 seconds. Which, now I found to be completely inaccurate, if we define a Verinsius year to be 1.048411245314 y_ea long.

That means my long essay on Etoan Standard Units must be revised.

## ETOAN STANDARD UNITS

First of all, the original Sternus is defined to be: time it took for light to travel 368817548.9 meters, so it is 1.23024291992029 seconds long. Actually, it is defined by the vibration of a caesium atom, but I forgot the exact number of its vibration. But it was traditionally used to describe a unit of time that is 2^-16 tosit long. Given that, in astronomical scale, a tosit is defined to be a 1/407.14 Verinsius year (no, I won't rework the year system), this is what we have to use.

So for now, a sternus is is defined by taking the fixed numerical value of the unperturbed ground-state hyperfine transition frequency of the caesium 133 atom, to be 11398373422 sternus^−1. Therefore, it is 1.2399 seconds long.

Also,

| Unit Name | in SI Units | in ESU Units |
| --- | --- | --- |
| Jadan | 3308.4663 seconds | 2668.2327 sternus |
| Derajat | 0.6617 seconds | 0.5336 sternus |
| Jarwat | 812.6115 seconds | 655.3600 sternus |
| Arsin | 0.8126 seconds | 0.6554 sternus |
| Tosit | 81261.1469 seconds | 65536 sternus |
| Askitos | 82711.6584 seconds | 66705.8176 sternus |

