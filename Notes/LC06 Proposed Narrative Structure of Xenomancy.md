---
## Consider not having fullcaps for headings.
title: Inspiration
## We don't need a subtitle for now
subtitle: Proposed Narrative Structure of Xenomancy
## Information about authorship(s)
author: Hendrik Lie
date: 06 December 2021
## Fonts and formatting.
fontfamily: times
fontsize: 12pt
linestretch: 1.25
indent: true
papersize: a4
## Numbersections
numbersections: true
secnumdepth: 2
---

# GENERALS

As i read through Foundation series, I'm attracted to the way Asimov interweave the Encyclopedia Galactica to the story itself.
One thing that I'm concerned about in my work is that it is full of infodumps that would be hard to explain in my narration.
So why don't we segregate them from the story itself.

Therefore, that kind of information would be provided as a block text at the beginning of a chapter or a part, that should summarize a certain topic directly related to the following narrative.
Preferably, it should ends with something that could fluidly connect to the beginning of that part.

The aim is to lift off complicated subject from the narrative, therefore it would hopefully not slowing down the story pacing.
The narrative would then be focused solely on the actions and the plot of the story. Also we would have a nice stub for our wiki entry.

# SPECIFICS

At the moment, there are few things to change that i could think over my head:

- [ ] Explanation about how diamond is flammable
- [ ] Explanation about RFL
- [ ] Explanation about telepresence system of etoan (absurdly advanced treadmill)
- [ ] Explanation about MG Style
- [ ] Explanation about Ambassador
- [ ] Explanation about Divine Council of Earth
- [ ] Explanation about Powers
- [ ] Explanation about body printing
- [ ] Explanation about Paramundus
- [ ] Explanation about EPL
- [ ] Explanation about WTF
- [ ] Explanation about other Reflexior governing bodies
- [ ] Explanation about Angels

