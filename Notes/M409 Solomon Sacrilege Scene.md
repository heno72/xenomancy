---
title: Inspiration
subtitle: Solomon Sacrilege Scene
## Information about authorship(s)
author: Hendrik Lie
date: 06 May 2022
## Fonts and formatting
fontfamily: times
fontsize: 12pt
linestretch: 1.25
indent: true
papersize: a4
## Numbersections
numbersections: true
secnumdepth: 2
---

The fight will involve Bach Sonata No.
2,
Andante,
that features double stops,
playing both melody and accompaniment at the same time.

It's the price to be paid for Amadeus' contract.
He will act only if Solomon keeps playing,
and it must be a good piece.
The better the pieces are,
the more Amadeus is able to focus.

Why Hendrik can notice this,
is because he recognized two kinds of contracts between mortals and volants.
In short,
there are prepaids and there are postpaids.
It's only a manner of which one identifies the clauses.
The clauses could be something mundane,
to something ridiculous.
The point is,
for the volant to satisfy the desire of the master,
the master must give something that satisfies the volant.

In Anthony's case,
the clause is less obvious,
because it seems like Anthony didn't do anything of his ends.
It's because for him,
the price is already been paid (prepaid),
as he let them escape hell.
They could know that solely because of Manov,
that's close with Anthony.

In Solomon's case,
the clause is that she had to perform.

Why identifying the clause is important when fighting a volant master?
Because it's the weak point.
If the clause is known,
then the next step is simply to find a way for the clause to be left unfulfilled,
hence breaking the contract.

In Anthony's case,
it's nearly impossible to do so,
hence making his case a dangerous one.
In Solomon's case,
just make her stops playing.

When Hendrik realized it,
and made moves to stop her,
she switched her piece to Bach Sonata No 2,
Andante.
Amadeus turned more aggressive,
and Hendrik's shadows are having harder times to catch on.
Kuroari focussed on overwhelming Amadeus,
but Amadeus breathes fire.

That's when the line returns to: diamond is flammable.
And Hendrik would also explain that a volant exists in this world simply because they borrow their physical volume,
and hence,
they're bound to it in real world.
No two volants can exist in the same space at the same point because their states are stored in the volume where they exists.

Pyromancy works by using a different kind of reflexium,
the one that is resistant to fire: Boron Carbide Nitride.
It's the same trick used in Tengu Incident,
the veil of fire.
The problem is,
magic is like volants,
they occupied an inviolable space.
They have to set it up around Amadeus first,
but how.

Distractions,
and stroop effect,
along with transderivational search.
Makes them freezes by their own volition.
Four cardinal pillars erected by second transformation of Kuroari.
But they're imbued with pyromancy threads.
Then it was lit,
forming the surface of a cube,
with Amadeus being in the interior.
Unable to break free,
it is confined in the reflexiums inside the cube,
and will perish as the reflexiums can't resuply their power from other reflexiums.

Hence the art of war: an alternative to life or death: a compromise.
Basically Hendrik offered whether or not Amadeus is willing to be deported,
or completely perish,
permanently,
as the reflexiums in the cube's interior runs out of power.

It's a force majeure,
Amadeus has an escape clause,
a time where breaking the contract is permissible.
And Amadeus took it.

Losing her beast,
there's no other way for her to defeat them,
but to fight them herself.
Hence the Violin Shield and the Bow Sword.

Hendrik commented: how sacrilegious.
Violin is a delicate instrument,
and she made it a weapon.

Heinrich whispered,
indeed.
He nonchalantly erected Earth around her and bury her alive,
while directing the others about Bain,
Anderson,
and Aditya.

