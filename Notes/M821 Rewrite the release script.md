---
title: Alteration
subtitle: "Recreating `Xe-1.sh` (the release script)"
author: Hendrik Lie
date: 21 August 2022
## Fonts and formatting.
fontfamily: times
fontsize: 12pt
linestretch: 1.25
indent: true
papersize: a4
numbersections: true
secnumdepth: 2
---

The previous methods of generating pdf, docx, and html outputs are cumbersome to maintain, and are definitely far from simple.
It is not elegant, or so to say.
The main piece is the file `Xe-1.sh` that we use by typing the command, plus version numbers.
For example to generate the outputs, we typed:

`~$ Xe-1.sh M821`

The script would then output in the exports folder, three files:

```
exports/Xe-1-vM821.docx
exports/Xe-1-vM821.html
exports/Xe-1-vM821.pdf
```

Looks simple enough, isn't it?
Except that the main code looks like the following:

```
echo "TASK: Build the composer"
echo "TASK: (1) pdf composer"
echo "## To convert to pdf" > mods/compose-pdf
cat mods/pandoc.head mods/xe-1.cite mods/xe-1.flist mods/xe-1.pdftail \
>> mods/compose-pdf
echo "TASK: (2) docx composer"
echo "## To convert to docx" > mods/compose-docx
cat mods/pandoc.head mods/xe-1.cite mods/xe-1.flist mods/xe-1.docxtail \
>> mods/compose-docx
echo "TASK: (3) html composer"
echo "## To convert to html" > mods/compose-html
cat mods/pandoc.head mods/xe-1.cite mods/xe-1.flist mods/xe-1.htmltail \
>> mods/compose-html
echo "TASK: Build the composer ... COMPLETED"
echo "TASK: Make the documents"
echo "DOCUMENT ID: $1"
echo "MAKE: pdf"
source mods/compose-pdf
echo "MAKE: docx"
source mods/compose-docx
echo "MAKE: html"
source mods/compose-html
echo "TASKS COMPLETED! Check exports Directory"
```

Majority of its functions are just to concatenate files.
At first what it does was to process the `composers`, by having different pieces of code parts combined into actual composer scripts.
After it's done, the code basically sourced the composer scripts (`compose-pdf`, `compose-docx`, and `compose-html`), so not the actual script itself did it.

The composer script looks something like the following:

**`~$ cat mods/compose-pdf`**

```
## To convert to pdf
pandoc --verbose \
--citeproc \
Notes/xe-1-intro.md \
Chapters/'Xe-1 Polar Opposites.md' \
Chapters/'Xe-1 The Boy From The Woods.md' \
Chapters/'Xe-1 Incursion.md' \
Chapters/'Xe-1 The Silent Concert.md' \
Chapters/'Xe-1 The ORCA-strated Event.md' \
Chapters/'Xe-1 The Esoteric Business.md' \
Chapters/'Xe-1 Overtime.md' \
Chapters/'Xe-1 Facing The Forking Path.md' \
Chapters/'Xe-1 The Lost Son.md' \
Chapters/'Xe-1 Close Encounter.md' \
'Xe-1 metadata.yaml' \
--toc \
-o \
exports/Xe-1-v$1.pdf
```

It's a script that's constructed on the earlier part of the `Xe-1.sh` file, specifically by the following line:

```
cat mods/pandoc.head mods/xe-1.cite mods/xe-1.flist mods/xe-1.pdftail \
>> mods/compose-pdf
```

Here's a breakdown for each of the referred files:

**`~$ cat mods/pandoc.head`**

```
## To convert to pdf
pandoc --verbose \
```

**`~$ cat mods/xe-1.cite`**

```
--citeproc \
```

**`~$ cat mods/xe-1.flist`**

```
Notes/xe-1-intro.md \
Chapters/'Xe-1 Polar Opposites.md' \
Chapters/'Xe-1 The Boy From The Woods.md' \
Chapters/'Xe-1 Incursion.md' \
Chapters/'Xe-1 The Silent Concert.md' \
Chapters/'Xe-1 The ORCA-strated Event.md' \
Chapters/'Xe-1 The Esoteric Business.md' \
Chapters/'Xe-1 Overtime.md' \
Chapters/'Xe-1 Facing The Forking Path.md' \
Chapters/'Xe-1 The Lost Son.md' \
Chapters/'Xe-1 Close Encounter.md' \
'Xe-1 metadata.yaml' \
```

**`~$ cat mods/xe-1.pdftail`**

```
--toc \
-o \
exports/Xe-1-v$1.pdf
```

Not very tidy isn't it?
Especially since now we know we can use [Pandoc's `defaults` file](https://pandoc.org/MANUAL.html#defaults-files).
Hence the need of rewrites.

The code simplification can be achieved mainly by the following three topics: Pandoc's defaults file, concatenation of files taken from a single manifest file, and treating the preprocessed output with a stream editor.

## Pandoc's Defaults File

Most if not all of Pandoc's arguments can be subtituted with a defaults file.
It will take down the roles of `mods/pandoc.head`, `mods/xe-1.cite`, and `mods/xe-1.pdftail`.
Actually, even the last line of `mods/xe-1.flist` can be taken care of.[^refermeta]

[^refermeta]: The part where it refer to a metadata yaml file.

As said in the previous section, the magic done with a defaults file:

**`~$ cat xe-1-release.yaml`**

```
---
## Basic options
toc: true
toc-depth: 2
title-prefix: Xe-1
verbosity: INFO

## Xenomancy specific configs
citeproc: true
metadata-file: ${.}/meta/xe-1.yaml
reference-doc: ${.}/Documents/L726-reference-book.docx

## HTML output specifics
standalone: true
```

As seen, the file takes care of table of content, verbosity, citation, metadata, reference, and standalone options (for html).
Therefore, Pandoc can be invoked simply by the following line:

`~$ pandoc -d xe-1-release <input> -o <output>`

However, there is not just a single file for the `<input>` part.
In the previous methods, it is taken care of in the `mods/xe-1.flist`.
That file takes care of the sequencing and identification of files to be included.

The problem lies in the fact that the files that makes up the component of our `<output>` file has filenames that does not honor alphanumerical orders.
It was a design choice, so I can rearrange chapters as simple as rearranging the way they are included in the export script.
Therefore the file orders must be manually specified.

But how?

## Concatenation with `cat` and `xargs`

Basically all we need is a manifest file that takes the role of `mods/xe-1.flist` sans the metadata file.
Hence the file `mods/manifest.list` (the manifest file), that contains:

```
Notes/xe-1-intro.md
Chapters/'Xe-1 Polar Opposites.md'
Chapters/'Xe-1 The Boy From The Woods.md'
Chapters/'Xe-1 Incursion.md'
Chapters/'Xe-1 The Silent Concert.md'
Chapters/'Xe-1 The ORCA-strated Event.md'
Chapters/'Xe-1 The Esoteric Business.md'
Chapters/'Xe-1 Overtime.md'
Chapters/'Xe-1 Facing The Forking Path.md'
Chapters/'Xe-1 The Lost Son.md'
Chapters/'Xe-1 Close Encounter.md'
```

Then all we have to do, is to use `cat` to spew out the content of the manifest file.
Afterwards, with `xargs` we use the `stdout` as our arguments for `cat`, and *voila,* we get the entire contents of files referenced in the manifest file as a single fulltext.

`~$ cat mods/manifest.list | xargs cat - > <fulltext>`

However, the file is not ready for use, because of our previous conventions: our heading level 1 starts with no newline at the beginning.
After we concatenate them with `cat`, the end result is that all files are joined, except that the ending of each files immediately followed by the start of the next file.
Hence our level 1 headings has no newline separation with the previous file, while Pandoc's markdown requires that [the heading line must be separated with a newline](https://pandoc.org/MANUAL.html#extension-blank_before_header) from the body before it.

We want a neat hack to automatically treats the fulltext file and obtain a nicely pandoc markdown formatted document before parsing.

## Treating the fulltext with `sed`

Stream Editor, or `sed`, is a very useful command to edit a large stream of text with surgical precision.
However it only works if we give it a very specific instruction on what to edit.
Then it is very useful to know exactly how we want to modify the file to satisfy our requirements.

For our very specific purposes, we have to consider how each chapter files are structured.
Because of our conventions, we only have two levels of headings for our xenomancy.
How so? We only need level one to cover the chapter titles, and level two for our subchapters.

At first instinct, all we need to do is to add newline before all lv.1 heading

`~$ sed -i 's/# /\n# /g' $fulltext`

However, we ended up destroying our level two headings.
What we need is to have the editor to change all `# ` to `\n# `, but a level two heading also has that.
I can't find an easy way to convert the pattern `\n#\n#` to `\n##` with `sed`, so we have to get creative.

The first step is to convert all level two headings to another substitute pattern in the time being.
Then we can add newlines before `# `, as only level ones have that pattern now.
Finally, after all level ones are treated, we want to change the substitute pattern back to the level two markers.
We ended up with the following piece:

```
# First step, change all lv.2 heading to ++
sed -i 's/## /++ /g' $fulltext
# Second step, add newline before all lv.1 heading
sed -i 's/# /\n# /g' $fulltext
# Third step, convert ++ back to lv.2 heading
sed -i 's/++ /## /g' $fulltext
```

It is not a clean hack, but it is a dirty cheap hack.

## Conclusion

There we have it.
Cleaning up the code can be done with just three main components.
The first step is to concantenate all the chapters in their specified order.
Then we treat the file to get a cleanly formatted concatenated file before parsing it to an output.
The output would have its arguments taken from a defaults file.
Then we can have a clean and tidy instruction referring only to the defaults file.

How is it like then, the clean and tidy solution we have?

```
#!/bin/bash

## Variables
fulltext="tmp/fulltext.md"
manifest="mods/xe-1.list"
defaults="xe-1-release"

## get manifest file and concatenate the contents.
if [ -d tmp ]; then
  cat $manifest | xargs cat - > $fulltext
else
  mkdir tmp
  cat $manifest | xargs cat - > $fulltext
fi

## Append a newline before the hashtag
# First step, change all lv.2 heading to ++
sed -i 's/## /++ /g' $fulltext
# Second step, add newline before all lv.1 heading
sed -i 's/# /\n# /g' $fulltext
# Third step, convert ++ back to lv.2 heading
sed -i 's/++ /## /g' $fulltext

## Filter them through a default file
pandoc -d $defaults $fulltext -o exports/Xe-1-v$1.pdf
pandoc -d $defaults $fulltext -o exports/Xe-1-v$1.html
pandoc -d $defaults $fulltext -o exports/Xe-1-v$1.docx
```

We can clean up the code more later, but right now let's settle with this.

