---
## Consider not having fullcaps for headings.
title: Inspiration
## We don't need a subtitle for now
subtitle: Daniel Ashton at 2kyo
## Information about authorship(s)
author: Hendrik Lie
date: 25 September 2021
## Fonts and formatting.
fontfamily: times
fontsize: 12pt
linestretch: 1.25
indent: true
papersize: a4
## Numbersections
numbersections: true
secnumdepth: 2
---

When [Ashton](/Characters/Daniel%20Ashton.md) was 2k yo, he was in Sidsido Ahidei, with someone around his age.
That someone is a rather simple person.
They'd wake up every morning, prepare their ingredients, and then made all the broth, before opening their restaurant.
People would come, and enjoy their meal.
Their meal was magnificent.
Ashton was a frequent visitor.

They'd know each other for hundreds of years, until one day, Ashton said he wanted to explore the cosmos.
They encouraged Ashton to pursue his dreams.
Ashton wanted to invite them to his journey, but he was afraid.
To him, back then, the person was happy with their life at that time.
There were hesistance in their eyes.
Ashton didn't say the word.
So Ashton left.

Centuries after, he returned to find them.
But he discovered, that shortly after his departure, they decided to retire.
Retirement in etoan society is death.
A permanent getaway.

It came to his knowledge that he was indeed a simple person, didn't dream big, and already thought they had seen everything there is to see in this world.
Got bored, trapped in a routine, and decided that other things wouldn't satisfy them anyway.
They retired.

But to Ashton, maybe, most likely, it was just that they hadn't found anyone to leave their comfort zone together.
Maybe, if Ashton invited them back then, they'd still be alive.
Maybe in our history, there wouldn't be just a single Jesus.
But Jesus and Maryam.

Ashton concluded that, "if you want someone to be in your life, invite them.
Because maybe that's the last chance you got to invite them in your life."

Because of that, many millennia later, he decided to join Kav's skirmishes on Earth, as a diplomat.
He wanted to enlighten people, more and more.

Ashton also said that ever now and then, sometimes they returned to his thoughts, and so the feelings he had felt like it was just yesterday.
And how he really wants to share his experiences in the past 13k to them, a person that no longer exists.

Expansion of this character with the help of Gemini is available [here](/CWG/O320-1426-ashtons-encounter-with-black-red-and-litters.md).
