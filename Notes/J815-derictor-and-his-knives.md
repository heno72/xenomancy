---
title: Derictor and His Knives
author: Hendrik Lie
date: 2019-08-15
tags:
  - alteration
  - poi/rik
---

Continues from [Daniel and Derictor](/Notes/J617-daniel-and-derictor.md)

# Derictor and His Knives

Derictor inheerited his father's love of knives, even though the second safe house (that's revealed after his death) and most of the knives were sold by the mother, he managed to sneak some of his favorites and his dad's favorite knives.

He joined a circus board for his extracurricular activities on high school and his university, and he's a prolific knife thrower.
After years of perfecting his knife-throwing skills, he can accurately throw most knives to a narrow target meters apart.

He maintained a fairly normal (though slightly eccentric) social life, and his fascination of knives are well known among his friends.
Most agreed that the knives he had in his collections are fascinating.

Unlike his father that's fairly easy to startle, he's quite calm and composed.
Little is there to startle him, and he would be very adamant against surprises.

He studies accountancy as well, and is able to be a successful one over time.
