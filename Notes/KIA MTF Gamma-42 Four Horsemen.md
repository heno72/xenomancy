# KIA MTF Gamma-42 "Four Horsemen"
All of the members are from the [Fibonacci Task Force](Notes/Fibonacci%20Task%20Force.md).
Armed with etoantech weaponry.
Ashton made them join the Intelligence Agency, as Anderson's influence wither due to his attempt to assasinate Henokh and Aditya.
The Four Horsement specializes with the protection of civilians.

- Daniel Ashton, **The Origami Knight.**
His armor comes in form of sheets extruding from his body, and folds in a manner people could only describe as origami folds.
Highly proficient in hand-to-hand combat, very calm and precise.
His face barely twitch in a fight, always focused and attentive to the enemy.
- David Pontirijaris, **The Black Angel.**
His preferred weapon is Midnight, a tweaked version of Adran.
It can reshape at will, but recurrent shapes are David's e-bike and The Twin Geniuses.
I believe David is actually quite easy to be agitated.
In Smartphonology, Ashton is the one that often calmed him down.
He's well prepared, but could get irritated when provoked.
It is also evident in David-Anthony arc, when they're in the cinema with Michelle, they got into fight because of a heated argument.
Ashton brought them back to their senses in the aftermath.
- Michelle Williams, **Sun Wukong.**
Her preferred weapon is Ruyi Bang, a telescoping smartmetal pole, that could change its width and length at will (max length: 15 m, min length: 2 m).
Her initial design was that of an eccentric person, and quite unpredictable.
Perhaps she's also like Inggrid, like to ship men.
- Zean Lisander.
**The Dream Caster**, or **The Sandman.**
His weapon of choice is the Oneirobot, a swarm of utility sandlets.
They could swarm into various shapes and forms as required or as commanded.
They can also project holograms.
