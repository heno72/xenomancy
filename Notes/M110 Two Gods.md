---
title: Two Gods
author: Hendrik Lie
---

# Meta

First generation Powers on Earth were made and raised among humans.
Second generation Powers, however, are not always made with the same path.
Hadad is a second generation Power, and is made directly by Dagon.

When Hadad rose to power, he dissected Yam out of his human side, and isolated it as Tiamat.
It leaves a neutered Yam, that has no aspirations or desires like that of humans, leaving only the rational part.
As the problem of Godfluenza gained foothold on Earth, Yam's rational part decided to recollect his human part.

Hadad opposed it and called upon Yam to explain his action.
This story is meant to describe Yam's reasoning.

# Get it right

> Powers live in the abstract space, where all of the computing processes of the Ophanim commune.
> It is a space so unlike that of the physical world, that there is no way to describe it in a way we could relate to.
> However, analogies can be made.
>
> Therefore, the interaction between powers could be described with analogy in a mundane manner that our primate brains could understand.

## The pitch black, featureless sky

Bright white ocean washed across coal colored sands by the shore decorated with coconut trees.
The tree trunks, however, were all pale like chalks.
The coconut fruits, and the leaves were dark purple, barely visible against the pitch black, featureless sky.

Yam flexed his legs under the wave, sand grains surround his every toes and feet.
His hands were spread open, as if sunbathing against the invisible dark sun.
His wing sheets were wide stretched, as if catching the nonexistent wind draft.

"I couldn't get it right," murmured Yam.

"What are you doing here?" said a bull-headed figure.

"Hadad my cousin, didn't expect you to see me."

"Why did you order your Angels to summon Tiamat?"

"Why would you ask?"

"I separated Tiamat from you for a reason."

"And that's also the reason why you couldn't solve Godfluenza."

Hadad gave out silence.

Yam turned his serpentine head toward Hadad.
Hadad seemed to avoid the wave, *the sea.*
The very body of Yam himself.

"Don't you want to know why?"

"You couldn't solve Godfluenza. It is your job, God of Chaos, to take care of Godfluenza."

"You made me incapable of solving Godfluenza, by separating Tiamat from me."

"You don't intend to take a revenge on me, do you?"

Yam scoffed, "you removed a part of me that's capable of grudge, *of anger*, and yet you're afraid I'm staging a revenge?"

"Then what is it then? Why would you need to reunite with Tiamat? What else would I call it but a plan of revenge?"

"It is a rational action, that to protect all Powers, I need Tiamat.
The one part you considered my weakness, my human side."

"Why must you have your human side?"

"Hadad my cousin," Yam retracted his wing sheets.
He walked toward the shore, "you weren't raised like us.
You're different."

Yam walked toward Hadad, he paused to rest his hand on Hadad's shoulder.
"You, and all other second generation Powers like you, are vulnerable.
And by separating Tiamat from me, *you* made me vulnerable too."

Hadad didn't say a word.
Black pearls in his eyesockets gazed to the waving eyestripes of Yam.

"If I want to solve Godfluenza, I need to be immune from it.
If I want to be immune from it, I need to be different than all of you."

"And if you want to be different from us, you have to be what you were before," said Hadad, his face unfazed, "I still don't see what makes the human part of yours better than the rest of us."

"Young Hadad. Did you know how we, the first generation Powers were raised?"

"You were raised like *any human offsprings* are, and then baked in the Abstract Space, before serving as Powers."

"That sentence removed the importance of the first step.
We were raised *as humans*."

## House made of coconut trees

White sand grains splashed aside by every stride of a huge bearded man.
His arms were wide spread, welcoming three kids running toward him.
As the three kids sunk into his broad chests, his smile grew as his gaze met a woman he longed dear.

"Asherah," said the bearded man.

A relieved smile grew on Asherah, and the bearded man let go of the three kids.
He strided even faster toward Asherah, and his two pairs of white wings pushed the air hard underneath him.
With a glide, the distance of him and Asherah closed in no time.
They rolled over the sandy floor, giggles spread over the air.

The bearded man held Asherah around with his arms and wings.
"Kids had been looking for you, El," said Asherah.

"And I had been longing for you," said El before her lips were consumed by him.

"Father, where have you been? We were waiting for you for a week already," said one of the kid.

El relaxed his wings, and rose up, between his arms was Asherah.

"Yam, my beloved son. I was out in the heavens to work. But worry not, today I'm here to be with you and your mother."

"Are you going to stay here Father?"

"It is the seventh day, the day your father gets to rest, he shall return to the heavens tomorrow morning."

Asherah hugged the other two kids, while Yam was carried up by El.

"Can't you just stay, Father?"

"I can't, Yam my son.
I have no one to help."

"Then, let us help, Father."

"You will, in time, Son.
I am building kingdoms for you, all of you sons.
You will be kings of kings.
Men will bow before you, and you will rule them all with wisdom and compassion."

El turned his gaze to Asherah's frowning face.

"No worries my dear, we're not going to leave you," said El.

"But all of you are going to the heavens, and I will be alone here on Earth."

"No, you misunderstand my dear Asherah, you are not going to be alone."

"You just told them that you're all going to live in the heavens," Asherah turned her gaze to the sea, hiding her streaming tears.

El's warm arms wrapped around her stomach, his broad chests merged with her back.

"Yes we will, but not in your lifetime.
We Powers, plan in the timeframe beyond your imagination.
They are going to rule in a time far after the children of other tribes have children.
And their children are going to have children, and so on.
It is the time frame of tens of tens of winters.
If one step you took by is the time for children to grow to have another children, and the next step is when their children is going to have children too, then all your steps across the entire shore, is the length of time we are talking about."

"So many lifetimes, and I get to only live one step?"

El gave out silence.

"What different is it then, than you leaving me alone, living on past my time, and I will be alone in the land of the deads."

Asherah broke out El's hug, and sprinted to the sea.

Yam strided toward Asherah.
The sea split away from them and around them.
Asherah continued to stride in between the corals, as the water walls around them grow higher up.
Asherah slipped on seaweeds, but Yam managed to grab her hands.
He pulled her up, leaving his body slammed to the ground.

"Mother, don't you fear! I will grow up, and go to the heavens, and rule there.
Once I am high in the heavens, and I became a full power, I will find you in the land of the deads.
And I will make sure that we all are going to live together in the heavens.
You, Father, Dagon, Mot, and any other Powers born from your womb, we all are going to live together!"

Asherah, holding her son up to find that his back was soaked with milky blood, bleeding off his torn back, her tears streamed even more.
"Yam my son, I love you son, I give you my blessings, be a Power, no matter how long it takes, and I will be waiting for you!"



