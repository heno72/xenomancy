---
title: BdKSv3
subtitle: Reference Paper
date: 10 January 2019
author: Hendrik Lie

## Fonts and formatting.
fontfamily: times
fontsize: 12pt
linestretch: 1.25
indent: true
papersize: a4

## Numbersections variable will add section numberings. Perhaps
## we have to consider removing manual heading title numberings.
numbersections: true
secnumdepth: 1
# part, chapter, section, or default:
top-level-division: chapter

## Related to Table of Contents.
## Right now, let's just use --toc parameter instead of a yaml
## metadata for toc generation.
toc: true
toc-depth: 2
hyperrefoptions: linktoc=all
# reference-section-title: Bibliography

## More informations about the document.
## I noted that if I use documentclass book, then I can't have the
## abstract parameter set.
documentclass: book
---

<style>body {text-align: justify}</style>

# PREMISE

A half alien and friends were involved in a fight between gods.

# SHORT OUTLINE

Steven and Daniel aided Anderson who's wounded by Aditya, then learns
about the existence of an alien sea serpent, and something about the end
of the world is coming. Steven and Daniel were present on the arrival of
demo Denefasan wave, led by this Aditya, then they fight Aditya and the
Denefasan troop. Days later, Aditya decided to eliminate Daniel and
Steven, then Steven managed to defeat Aditya's men and save Daniel.
Steven, his friends and family decided to move, aided with Henokh and
Anderson, yet Aditya discovered him again. Steven defeated Aditya, and
then he's detained by G28 and Henokh. **-insecurity**

Anderson intended to kill Henokh, but Aditya blocked and wounded him,
then Aditya's escape is aided by a sea serpent. Henokh, alive, managed
to track Aditya and ambushed his facility, yet Aditya's not here.
Aditya's men breached Securion buildings and stole an aucafidian
artifact stored there, despite G28's attempt to prevent this. Henokh
investigated a new movement called Umbra Cahaya, that's related to
Aditya's organization, that lead to Aditya's whereabout, then confronted
him. Aditya, managed to run, later to perform insurgency on Medan (one
of the objective is to destroy Kuin), but defeated by Steven. Aditya
revealed Anderson's attempt to kill Henokh years ago, resulting in
Anderson revealing his reasoning, that's now obsolete. **-distrust**

Hendrik discovered a list containing names, including his that's killed
by the list's sequence. Hendrik captured the girl that's killing people
up to prior to his name in a shopping mall, and brought her to BSW's
facility. Following the lead, Hendrik and co. visited Paramundus Integra
to save Hendrik in that world from being killed. Due to his action, the
enemy obtained a fatal exploit of RFL to enter and invade manent realm.
Together, they (the protagonists) then visited the Paramundus Shamayim
to defeat the old gods. Now, their action disabled earth's defense, and
Denefasan invaded earth. **-twist**

post-Ecorapture, Aditya helped Henokh to preserve remaining humanity
from Denefasan's attack, and occasional unknown demons spawned from the
ground. (?)

# CHARACTERS

1.  **Steven Pontirijaris.** A half human half etoan, but he prefers to
    describe himself as an etoan[^1]. He's a pacifist unless provoked,
    altruistic optimistic, and patriotic of his origin as etoan. Like
    a good doctor, he upheld Hippocratic Oath. A pontirijarisian,
    leftist liberal, toward collaborative anarchy. Loves humanity,
    probably one of his favorite world. Learned medic to obtain
    recognitions among baselines, a hobby he can be proud of. To
    increase his social standings. Married to Helena and be a father
    of four sons. His motivation is his family and friends, and their
    well beings.

    His personal arc is about saving his loved ones without wounding
    others, to resolve who really is in Helena\'s heart, and about saving
    Earth that he loved. His obstacle is the fact that his enemy will do
    anything to hurt his family.

2.  **Daniel Lusien.** Is mostly selfless (to some degree), resourceful
    and loves science fiction (toward hard spectrum), a loving person
    and good friend to Adran, Steven, and Helena. Is with Fernando
    Suryantara. Tend to be liked by etoan artificials and volants. His
    motivation is to make a brighter day. He valued happiness and good
    friends.

    His personal arc is about his parents that are against his decision to
    be with Fernando. His parents threatened him to make him marry a
    normal lady and father a son.

    That and to discover freedom of love and friendship. He must find a
    way to resolve his sexual orientation issue, and what he really wants
    to be.

3.  **Helena Irawan.** Is Steven's wife. A caring mother and secretly
    had a crush with Fernando. She wished to resolve the issue and
    apologize to Steven, it is resolved when she finally communicate
    with Steven about it, in hell.

4.  **Adok Ranensis.** (nick: Adran) His birth (or awakening day) is 21
    May 1956, or 67,488.7841 CL. However he\'s sent to, and printed on
    earth on 2003. He\'s an Multimodal Autonomous Transport Vehicle,
    Ranensis Line of Admir Locomotive Clade, and officially a full
    citizen of Etoan Earth Colonization Initiative.

    His body is comparable in size, mass, and ability with a Pajero, plus
    intelligence of a transapient. He loves being in road, because the
    state/feel of roaming across country roads, scaling up and down on
    hills, and on open space provides him some sense of spiritual trance.

    He has a certain disliking to Helena\'s normal car (dumb car, as Adran
    put it). His preferred coloration is black, and texture of a smooth
    natural stone, but adopt normal black car texture on normal city
    environment. He prefer to use green lighting, likes the moon\'s UV
    radiance, though at day he also enjoys power surges from the sun.
    However, he dislikes very high intensity of light he has to work
    through, therefore he has a love-hate relationship with daylight. He
    naturally prefers driving on road than flying because of his strong
    tactile senses on his tires, but flying is not that hard for him to
    grasp.

    Though he\'s self cleaning, he loves Daniel as he tends to cleanse him
    thoroughly, and with care.

5.  **Henokh Lisander.** Agent of BAIK[^2], code named Agent Suharto.
    Wished solely on a better world of his idealistic imagery of a
    perfect society. Driven by his experience being persecuted as a
    member of a minority ethnicity and religion. In other worlds, he
    hated racism and society class separation/segregation. His
    motivation is to eliminate segregation of social classes and
    discrimination. His traits are Stubborn, persistent,
    deterministic, and occasionally emotional when faced with Aditya.

    His personal arc is to meet and talk with Aditya, to ask him why he
    left him. His main obstacle is the fact that Aditya is an insurgent
    (supposedly antagonist), and he\'s in the supposedly protagonistic
    side. Due to that he had some trouble with his loved ones, can not
    really focus on people who love him.

6.  **Anderson Pondalissido.** An Etoan superior[^3]. He's unpredictable
    in nature. Has somewhat alien point of view, sometimes bizarre,
    sometimes appear to be human like. Mostly silent and calm. His
    looks is intimidating for normal human. However he could change
    minor features on his body, like skin color and eye irises at
    will. His shapeshifting ability took some days to complete the
    change.

    His personal arc is to stop the Antichrist, and to prevent Denefasan
    attack. His obstacle is the fact that he hated hidden variable, and
    mistakenly conclude that Henokh is the Antichrist. The fact is, he
    really liked Henokh, and hated it so much when he concluded that he\'s
    the Antichrist. He had to kill Henokh, though he did not want to be
    directly killing him.

7.  **Hendrik Lie.** A chief and an agent of BAIK, code named Agent
    Sukarno. He's a reflexior[^4], a part of Trinity Compact with
    other Hendriks (essentially the compact is a group mind of clones)
    as the owner of the body and the original copy. He\'s good at
    martial art and decent proficiency at manipulation reflexius,
    especially on moderate psychokinesis and mind altering techniques.
    Master of Reflexiology from University of WTF[^5]. His human
    profession is as a lawyer, specialized in corporate laws. His
    traits were brave, opportunistic, and indifferent. Perhaps
    slightly eccentric by human standard. His motivation is curiosity,
    and desire of self improvement.

    His personal arc is to find and stop the Tiamat. Also to resolve what
    his true desire is. His true desire is to be recognized as a person,
    so he seeks approval, however his main obstacle is his own low self
    esteem due to his inability to satisfy his own expectations.

8.  **Romanov Dexter.** (short: ***Manov***) Hendrik's copy, a part of
    Trinity Compact, as an analyst officer. He\'s resourceful and
    clever. His reflexius strength is not in brute force but in
    details, precision and dexterity. Full of tricks and hacks, and
    especially good with altering mindstates through words and body
    gestures. Can be very persuasive once he knows what the opposing
    party\'s inner motivation. His attitude is friendly, playful and
    slightly hedonistic. He loves neat environment and hates mess.
    Quite perfectionist as well. His motivation is thirst of
    experience. Anything he had never tried might actually interest
    him, and he\'d try it until he got bored with it.

    He was born on May 2013, because Hendrik was so devastated when he can
    not enroll into the med school due to his colorblindness. He was born
    without color blindness, and can see into a wider range of spectrum
    than a human could. From 1000 nm to 100 nm. Then as Hendrik enrolled
    in a law school, he enrolled in The WTF Institute of Medic and
    Holistic Health.

    Later discovered a SeaWorld paramundus, and meet Purpose there. They
    were having fun, until Purpose decided to say her feelings toward him.
    Ever since that time, he tried so hard to find reasons why he disliked
    her. However every reasons he poured off, Purpose tried so hard to be
    with him that she seemingly find a way to shed off every single reason
    that makes him disliked her.

    His personal arc is to redefine his relationship with Purpose, and to
    show that Anthony is good. His obstacles are his ego, who wishes to
    have free time and free of commitments. That and because he can
    actually see goodness in people, even someone as bad as Anthony..

9.  **Heinrich Potens.** (short: ***Ein***) Hendrik's copy, a part of
    Trinity Compact as the security officer. He\'s exceptionally
    proficient at pyrokinesis and psychokinesis. Full of strength and
    brute force, strong-willed and persistent. He\'s ruthless and
    indifferent in nature. Pragmatic and cautious in decision-making.
    Perhaps the most advanced magic he has is to materialize a
    physical body nearly equivalent in strength to Heavenly Hosts. His
    motivation is peace, he hated to be provoked, and wish to find
    some time for himself.

    His personal arc is to protect Hendriks from danger, and to truly
    understand the rage inside him. His main obstacles are his rage,
    suppressed emotions, control freakiness, and deep inability to really
    trust people.

10. **Anthony Matthias.** He is a necromancer, he can communicate with
    souls in hell. He can force them to do his biddings in exchange
    for a way to escape hell. Also, autodidactically he is able to do
    pyrokinesis, but he can not ignite a fire. Was accidentally lost
    in the woods by his father when he learned that his son can do
    wicked things (reflexius).

    His personal arc is to find his mother, and to resolve his nightmare.
    His nightmare reveals more and more about who her parents are, over
    the course of the story, but intensifies after he meets Martha.

11. **Aditya Wijaya.** A leader of Unit H, a revolutionary group that
    seeks to reboot the society, by growing a new one after first
    setting up an anarchy. Was 18 years apart from Anthony Matthias,
    and he picked up Anthony as a caretaker, and let him grow as his
    son and as his brother since Anthony was 5 years old. Was
    generally control freak, and had a strong ambition to achieve his
    goal.

    Aditya has some ties with Denefasan Embassy, and Yam. This is where he
    made a pact with Earthen Powers to enact anarchy to the world, where
    he can rebuild the world. Denefasan promised to help him to reboot
    human civilization, and Yam promised him a means to do just that,
    telling him about something called Black's Staff. This plot ended when
    he successfully collect Black\'s Staff.

    His obstacles were the BAIK, and had a personal problem with Steven.

12. **Martha Williams**, the pharmacy's clerk. She was married at a very
    young age to an abusive guy. Her son was left in the woods by her
    husband because he believed that the son was cursed, and when
    carrying her second child, she divorced him. It was the most
    liberating decision and she continued her pursuit by enrolling in
    a graduate school, ended up in Steven's and Daniel's clinic as a
    pharmacy clerk. Her daughter is Michelle Williams.

    Her personal arc is to resolve her crush with Daniel, and to find her
    lost son. Her obstacles is Fernando, and the fact that she knew so
    little about her long lost son.

13. Kuin.

# OUTLINE

## Part 1

Usual routine of the morning, at 17 August 2021. News: five brightly
glowing unidentified flying objects on the coasts of Indonesian cities
on the midnight. Another news: KukerCorp's Kendari branch office were
bombed. Then the third one, that Steven barely pay any attention to:
Some experts believe that the bombing might be related to radicalist
movements that oppose the rise of artificial intelligence due to the
fear that they will take over and fight humanity, and that the rise of
artificial intelligence were associated with the so called Dajjals, or,
some even more minority group claimed that as the rise of Antichrist.
Steven woke up, then get his sons ready for today's 76th Indonesian
Independence Day Ceremony. Afterwards he went to his clinic, and meet
his friend Daniel. They talked about the bombing, Fernando who's joining
G28, medic division, and five unidentified flying objects all around
Indonesia. Then Anderson entered, and collapsed. His gray-colored blood
spills out from his back. Steven and Daniel treated him, and he warned
them about what he had just seen. Aditya's with the sea serpent,
probably related with the unidentified flying objects. He said it signs
Denefasan custom on invading new worlds: warning. Steven contacted
Henokh, and soon after he came and picked up Anderson with G28
operatives who are part of Oneiric Protocol. Steven informed Henokh that
a Sea Serpent is sighted, and what it means. Meanwhile Henokh said that
it's not his main concern now, he was almost killed by the bomb, and
Bain is dead. Then Daniel said that Aditya is with the sea serpent. Now
he looked on Daniel, almost like he's looking him rather seriously.
Henokh said that he'd like to purchase this clinic just to fire Daniel.
Daniel said get lost and Henokh left.

Then Ostaupixtrilis Pontirijaris, a teen is seen walking toward the
clinic. Daniel greeted him, called him grandpa. Steven called him
grandpa as well. Ostau greeted them, then asked them just to call him by
name, because it felt awkward to be called as such in his younger body.
Then they talked, especially about why he came. He was worried about his
grandson and his grandson's friends, when he heard the news about
confirmed denefasan visitation, especially on Kendari. He warned them,
as he suddenly remembered that he had been on earth once, and that he
made a promise that he'll come again to earth, and that's the time where
earth would enter judgement day. He didn't realize it at first, back
then in 1950, that he instinctively sign to colonize earth to fulfill
his long forgotten promise. His excuse was that back then, the Earth was
called Ars. Daniel realized that he was referring to Jesus, and asked
him, "were you Jesus?". Ostau then said that yes he kinda was. It was a
fierce memetic war he said, and he remember that he felt the pain when
he was crucified, but now he couldn't remember the pain, he just know
that he was in pain. He was proud to know that his contribution lasts up
to current era, however deformed they were from his original teachings.
Daniel asked again, "So, you're Son of God?", and he said, "Depends, on
how you interpret it" Then he continued, "and yes, I think the so called
judgement day is near. I used to know what it means, but not anymore. I
don't know when, I just know that it is very soon. My arrival here, and
the confirmed arrival of Denefasan forces here on earth are clear signs
of that. Only God know when. And when I said God, I mean ours,
Kavretojives."

18 August 2022, 14:05 WITA, a day after 77th Indonesian Independence Day
Ceremony. A very deep, trembling sirens could be heard miles away,
coming from the sea. A very large fleshy island emerged from beyond the
bay, paddling its way to the bay entry. Soon a greenish swarm emerges
from its back. It's later evidently clear that it was a swarm of
greenish dragons, six legs and two pair of wings. They flew to the land.
From the ocean emerges a troop of six legged horse sized monsters, with
their two slender, thorny manipulator limbs near their heads. They traps
people in sticky substances, almost like glue and/or hardened gums, that
dried out the moment it reached the ground. It's almost impossible for
men to break free from it. Among them are few dinosaur-sized six legged
armored creatures, they breathes fire. Their branching thorny armor
hosts some tens of eight legged climbers, almost like octopuses but with
firmer tentacles, or legs. They carry with them weapons, high velocity
projectiles shot from them, and also long ranged version of glue-shots.
It's almost a matter of time before Kendari's bay area is almost
entirely occupied with them.

Steven and Daniel could hear the roars of incoming dragons, and then
they could hear a number of creatures swarmed their clinic's yard. It's
only a matter of time before one of the critter entered their clinic
room. Steven is well prepared, his weapons are ready. It's on his car.
Unfortunately they're on the first floor. So they have to go to the
second floor first, away from the incoming critters. Steven configured
his phone to offensive mode, but it might not last long, as his phone
has limited battery time, and it took the phone some time to charge
between shots. First room, a critter entered the infirmary, fortunately
the wall is an OPA screen. He set it to focus its output to the critter,
with enough energy to pierce its legs. They escaped, and now the phone
is charged for five shots, and on the stairs they meet two more. Daniel
stabbed one of them in their mouth with his umbrella. He was shocked as
he just violated his hippocratic oath. The other one got a shot from
Steven's phone, right on its eyes. The creature fell from the stairs in
pain. Steven decided not to worry about his oath just now. Steven almost
dropped his phone, one of its side, the beaming side, is sufficiently
hot it almost burn Steven's finger from accidentally touching the
beaming surface. They reached the second floor and discovered that a
pack of this critters on one side of it. On the other side, the room
with a large window in it, is where they want to go. They charged to the
room as the critters chase them. Steven quickly grabbed the bag and
Daniel tried to close the door, but he's close on losing ground from the
pressure exerted by the critters. Steven opened the window, his car
moved to just below the window and its roof folded clear, for them to
jump in. Steven instructed Daniel to leave the door and immediately
jump. Daniel almost couldn't move, but he immediately jump, but not
without first collide with Steven and accidentally made up Steven's
third kiss, by men (all of them are without Steven's consent). Both
safely (and rather unpleasantly) land on the car. The roof quickly fold
itself close, and the car immediately accelerate out of the clinic's
perimeter, probably stomping some critters away.

Steven couldn't believe it, today they were attacked by a large pack of
eight limbed critters and the first wound he get was from Daniel's
teeth. Daniel was equally shocked from the fact that it was his first
time wounded by Steven's teeth. They laughed in relief, until the car
asked them where should they go. Inconveniently spread across almost
equidistantly spaced points on the entire Kendari, are two of his sons'
schools, one is highschool of his oldest son, and another is
jhs+elementary of his two sons, and his house, where his wife and his
youngest son are. Daniel gave his perhaps the most brilliant idea of the
day: they're all (except his wife) backed up already, so back me up as
well and we fight these critters on our way to pick them all. Steven
agreed with his terms, and he injected Daniel with nanites, as his couch
engulfed him, forming foamed corundumoid armor, with wings. A brief
introductory description is provided for Daniel as nanites explore his
body and the suit scanned him, all while feeling the pain (slowly numbed
down). Steven's couch also undergone similar transformation, completely
engulf Steven. They flew, Daniel rather sloppily at first, but some
moment later he got a hang of it.

After fighting for some time, Steven declared that it's just too many of
them. And then perhaps the second most brilliant idea of the day by
Daniel is this: why don't you let your car picked up your sons, it's
autonomous right? Steven's car then picked up his children, starting
with the oldest. The car rolled, sledding, turned, and zoomed over the
cityscape, happily stomped upon denefasan critters, hitting some
denefasan horses that blocked his way, and avoiding denefasan elephants.
Arriving at David's school, it discovered that David's kinda okay,
hiding in the corridors of the school with his girlfriend, shooting up
some approaching denefasan critters with his phone, and so on. David
boarded the car with his girlfriend, and the car quickly zoomed off into
his brothers' school. A little bit chaotic, it took the car some time to
locate Peter and Eden, that hides in the school's toilet, their phone's
batteries are dried up. The car stomped upon a critter in front of the
toilet. Peter and Eden boarded the car and they zoomed up into the
house. The house's defense system closed up entirely, and the house told
the car that Helena and Hayden isn't here. They're on duty on nearby
hospital. Arguing with the house, the house granted the car four
reactionless disks. The car happily flew to the hospital to pick Helena
up. Surprisingly, the hospital isn't attacked, the swarms just past
through the hospital. Wounded, injured, and scared people gathered in
the hospital. The car decided that it is a safe place, they were
unloaded there, and the car flew again to its master.

Steven and Daniel fought for about an hour now. They decided that it's
almost impossible to stop them all. They barely reduce the number of the
invading aliens. They noticed two things during that time, however.
First, there's a group of human observing the invasion but not touched
by the aliens, and they're doing it from a raised platform above an
unknown model of very large, half submerged submarine. Second is that
the very large fleshy island is actually a very large whale with six
fins underwater. Either one of them could be the controlling mind, or at
least command center of all of these aliens. Expecting unexpected, they
decided to try attacking the first. As they flew closer to the
submarine, they could almost identify one of them, but they couldn't
really remember who he was. Closer they got toward the submarine, it is
apparent that one of them is Steven's cousin in law, Aditya. Perhaps
that was the second most surprising thing Steven realized. The first is
the fact that when Anderson said that he was attacked by Aditya, Steven
never considered his cousin in law as that Aditya, but then right now
deep inside his gut he knew that his cousin in law is the one that
wounded Anderson. Anderson was a man that's attacked by a gang of
violent motorcyclists and sent them all into infirmary, without himself
got any scratches. This is the third thing that surprised him at the
moment. Aditya flew with a pair of feathery green wings, and at that
moment Steven realized that he was covered with some kind of green scaly
armor, probably biological equivalent of his and Daniel's armor.

Eagles fought with a rather weird posture, they grabbed each other's
talons and they flew, rotating rapidly in the air until either one of
them fainted. That's what Aditya's doing with Daniel right now. Daniel,
not surprisingly, fainted soon after. Human body isn't meant to survive
high gees of acceleration. In fact Aditya should've fainted as well, but
he didn't. Steven thought that it is the fourth most surprising fact
about him. At that moment Aditya flew to Steven and quickly did that
maneuver to him. It is a rather lucky coincidence that Steven's ancestor
were designed to survive high gee world of Aucafidus, that he could
match Aditya, they both passed almost simultaneously. It doesn't matter
who fainted first, as such that short slice of time isn't enough for
either of them to take advantage of the situation. They both fell, until
their armor turned autopilot and get them into level position. That's
what happens to Daniel back then as well, if anybody thinks I forget
about him. If you think they were just toying around, then you're
probably right, and as equally, probably wrong. The next fights are more
serious, as Steven's left arm armor cracks, Daniel's (coincidentally
also his left) wing torn midway, almost, and Aditya's chest ripped
off...., okay it was his armor's chest anyway. They fought in the sky,
underwater, among buildings, atop a dragon, and suddenly Aditya flew
back to the submarine. Just as they thought they were winning, Aditya
ordered his men to shot two silver fiery darts (read: misiles) to them.
Perhaps Aditya decided to stop toying around and be practical. Either
way one of the missiles were exploded prematurely by a pair of green
laser beams from Steven's car who caught Aditya's attention by surprise
(hey, it's a Flying car!). Another one is redirected by Steven into the
submarine. The submarine sank, Aditya's men flew with similar armor.
Angered, they flew toward Steven's car and barely scathed it. Then they
decided to aim Steven and Daniel instead. The car was about to help when
suddenly a flock of dragons bite the car, flung it, and whipped it
rather violently that it's almost shapeless, not because they were
ordered by Aditya's men, but rather because on its way to Steven, the
car hit the flock leader's head and knock him into the sea, rather
embarrassingly. (The dragons are sentient! Was probably the car's
thought at the moment). The reactionless drives still held it stationary
anyway. Perhaps it is a car's equivalent of shock.

Steven and Daniel now fought Aditya's men, they barely managed to
escape, when finally they agreed that they should have aimed for the
giant whale instead. Obviously outnumbered (they were as well when they
fought the dragons, but oh well), and unable to break free, Daniel gave
Steven his third most brilliant advice of the day: order your car to
fight for the whale instead! But at that moment Steven decided to make
his own, most brilliant idea of the day: detach one of the drive and at
close proximity to the whale, accelerate itself into relativistic
velocity and let its surface fuse with the air and the skin of the whale
to do the rest of the job. The whale blew rather spectacularly and the
swarm that occupying Kendari were instantly disoriented. Dragons collide
with one another, denefasan horses and denefasan critters stomped by
denefasan elephants, denefasan elephants burn each other, et cetera.
Soon after, Indonesia's first AI has its troop of drones arrived on
Kendari and do the rest of the cleaning job. The headline of the day
reads: Kuin: First AI of Indonesia save the day! Glorious defeat of the
aliens by KukerCorp's AI raises public trust to Kuin. The next page has
a small column referring to Steven and Daniel only as this headline: Two
Angels Fought Few Green Demons in the Middle of Alien Invasion: Couldn't
They Saved Us Instead? The rest of the column only touched the topic of
whether or not the demons related to the aliens, and whether or not it's
not the other way around: the green ones are the good guys and the
bright angels are actually the bad guys. Aditya's men flew away and
Steven and Daniel couldn't care less about them at the moment. Daniel
regretted their decision to attack the submarine. It is obvious that the
whale is the one that control the fleet, and they suffered more because
of their belief that it couldn't be that obvious. Steven regretted the
use of that reactionless drive, and now thinking how to pay for a
replacement of this drive whose cores were forged in the stars. Also it
is the day where David got his first kiss with his girlfriend, as a
reward for David's heroic act of saving her. Meanwhile Helena commented
on how jealous she was knowing that Steven and Daniel kissed so hard
that they both got teeth marks on their lips, in the first ever alien
invasion of earth. Fernando, joining the incoming Kuin's fleet, visited
Helena's hospital (not hers, rather where she works) and the first thing
he did was burying himself in Daniel's hug, and the second thing he did
was throwing exactly the same comment as Helena's.

In a secret office of a secret organization where Henokh and Anderson
are members of it, Anderson presented the column about Steven and Daniel
to black tuxedoed audience. Henokh rolled his eyes at first, and
Henokh's best friend Derictor asked him what it was. Nicolas, whose
father died last year due to the bombing almost understand what Anderson
want to say, but doubtfully asked Anderson instead. Anderson said that
it is a good marketing to KukerStudio's box office franchise,
Archangels, and a way to keep public eyes away from their secret
organization's activities. Also in another secret office of yet another
secret organization led by Aditya, Aditya decided to scheme a revenge
for Steven, the one that ruined his plans and that destroyed his
favorite submarine. Days after, things appeared to reach normalcy,
except for the post-war landscape of Kendari. Economy collapsed,
locally, schools damaged, and some teachers went missing. However,
(un)fortunately for the students, schools are still being held in ad hoc
classes. It's almost lawless. If there's any time where people could be
killed without much media coverage, it is now. And exactly that is why
Aditya decided to dispatch his men on killing Steven and Daniel.
Daniel's the easiest target at the time.

Steven decided to drop by Daniel's house, and Fernando's the one that
open the door. He wore only his boxer, and a towel hugging his back,
apparently just bathing. Steven asked him where Daniel was, and he said
Daniel's still sleeping upstairs. Steven proceeded to his room
accompanied with Fernando, and discovered Daniel still on the bed, naked
inside his blanket. Steven woke him up, and Daniel was so surprised he
tried to cover up his body. Fernando and Steven laughed, and Steven
remarked how lucky Daniel was that Steven didn't bring Helena. They
talked for some time and Daniel cuddled with Fernando, remarking how
hungry he were, when, suddenly, he realized that Steven's still in this
room. Awkwardly, Fernando tried to convince Steven that Daniel really
meant literal hunger. Steven cheerfully asked Fernando to eat and buy
some food for Daniel, while Daniel get himself ready. Fernando then wore
the clothes, subtly reluctantly, while a disappointed expression hinted
on Daniel's face. Steven just smiled at Daniel. Suddenly Daniel felt, or
rather, reminded, how alien Steven was. Steven was just so happy that he
thinks he's doing Daniel a favor. Steven seriously consider that Daniel
meant to ask for some time alone to have a good time, so he decided to
bring Fernando along to ease Daniel having a good time, alone. Perhaps
Steven's pure honesty at unusual time like this occasion that makes
Daniel sometimes fails to understand Steven. As they leave the room,
Fernando put a sorry face to Daniel, and Daniel was now alone in his
room.

In the road home (as Steven's car happily drive itself) after they
finished their meal, Fernando asked Steven, whether or not Steven
actually understand what Daniel meant back then, Steven explained just
like that, and adds a remark on how good Fernando was to pick up
Daniel's sign and pretend that Daniel really meant to be hungry. That's
why Steven decided to pretend that he believe Fernando. Steven said that
he reads that sometimes human male needs some time alone to have a good
time with himself, even if he's in a relationship. That's perhaps one
thing Steven didn't really understand about humans. Fernando wondered
how Steven's mind works, but decided not to question it further. Perhaps
the most surprising fact unbeknownst to both of them is that how
Steven's actually far more human-like than Fernando is to aliens, mainly
due to the fact that he's only half Etoan: his father is Etoan, and his
mother is human. However Steven prefer to be likened to Etoan than to
human, especially because of his innate ability to interact with Etoan
technology in a way is impossible for human: Steven can, innately,
receive and transmit radio frequency. That and the fact that Steven's
heart has seven chambers instead of four. As they approached Daniel's
house, both of them (yes, the car included) are startled to discover a
black range rover on the yard, and the front door was left open. The car
confirmed to Steven that it was locked when they left. Steven decided
that it's also better to back up Fernando as well. Things are going
crazy lately, and he couldn't afford to lose his friends. Fernando
agreed, even allowing Steven to do so. A second later Fernando went
through similar experiences Daniel experienced days ago as the couch
engulfed Fernando. Two angels exited the car, and carefully, silently
entered the house. Steven located and seized a guy on the kitchen,
Fernando went upstairs to Daniel's room.

As Fernando entered the room, the guy's surprised and release a shot to
Daniel's left chest. Steven, receiving feed from Fernando's armor, was
terrified as well. He drags the man he petrified on the kitchen up as
fast as possible, and remotely ordered Fernando's armor to seize the
second guy as well, then tend Daniel as quickly as possible. The car
ejected a nanomedic capsule to the window, which Steven quickly catched
and injected it to Daniel's wound. Perhaps worth noting is that
miraculously Daniel has a rare condition known as *Situs Inversus
Totalis*, meaning that his morphology is mirrored entirely from left to
right. In short, his heart is on his right side rather than left. Also
worth noting that this makes Daniel technically left-handed, as his
dominant arm is his right, if chirality of his anatomy is to be
considered, a fact that most people wouldn't notice and assume that he's
a normal right-handed person. Also worth noting that shortly after,
Steven realized that he needs not worry so much about the fatality of
his wound, as Daniel is backed up already, as recent as during his
sleep, and he could resurrect Daniel again should he died at the moment.
Still that fact has no meaning for Fernando, as his lover is in terrible
pain, and mainly because his blood spilt rather messily. And that didn't
prevent Steven to be fueled with fury and anger to the ones hurting his
best friend. Crushing their guns in front of them was, surprisingly,
sufficient to make them spilt out required informations Steven might
need, plus they peed in their pants.

More and more Aditya's men converged at Daniel's house, then Fernando,
carrying Daniel in his arms, jumped to the car, now stationed right
below the window of Daniel's room. Steven jumped down as well, releasing
green laser shots toward Aditya's men. G28 and police officers, called
by Steven, was also on the way, converging to exactly the same spot, by
a delay of five minutes. At that time, however, Aditya's men already
recovered their two comrades, and left the site. That wasn't Steven's
main concern at the time. As they're zooming through the roads, Steven
knew that another group of men were converging at David's school and
another group on Peter and Eden's school. Even at the time like this,
Daniel still managed to give the most brilliant idea of the day: Steven
to David, and Fernando to Peter and Eden, while the car, after
contacting Helena, delivered Daniel to Helena for further treatment.

Steven couldn't locate David, and he discovered David's utility tablet
laid among rubbles on the school's gate. A notification by Fernando that
Peter and Eden are okay was a short-lived relief that ended when he
asked Fernando to bring them with Helena and Daniel, so that they could
board Steven's ship under his house, for emergency occasion. Scribbled
on the screen of David's utility tablet by waterproof black inks was an
address. He followed that lead into an abandoned building. A group of
men surrounded a boy, which quickly recognized by Steven as David.
Aditya was on David's side, holding a dagger positioned on David's neck.
Aditya opened his speech with how delicate the plan was, and how he was
planning to liberate earth from corruptions. Then Steven arrived and
messed up all of his delicate plans. He then argued that however he
disliked it, he decided to give Steven a lesson to mind his own business
than to interfere with other's business. Steven then realized that all
of them (but David, obviously) wore the armor they worn days ago during
the invasion. Steven's alone, so he couldn't possibly win against all of
them. Steven was given two options: to let Aditya kill David, or for
Steven to kill David instead. Steven was devastated. David was even
more, that his expression is blank. Bruises could be seen all over his
face, and his arms. His uniform torn, his undershirt tainted with specks
of bloods. He seemed to have some difficulties on breathing. Steven's
pretty sure that some of his ribs were fractured. Anger lit up in his
heart's furnace, but at the same time, he could almost feels David's
pain. No, he felt it in a radio band, that belong to David. Regrets
flooded in, should he arrived sooner, David wouldn't be through this
pain. Aditya asked Steven again to choose. Aditya added, that Steven
should also undone his armor. Steven was petrified. To motivate Steven,
Aditya sliced David's shoulder to the bone. David heard himself
screaming, and Steven's cry: You crazy bastard! He's your nephew! He's
your cousin's son! Aditya laughed. Aditya argued that he didn't even
consider David a human. David's not. David's an etoan. Etoan, Aditya
said, treated humans as if humans were animals on the zoo. He had no
reason to treat etoan as humans. It'll be the same, he said, that when
he slain David, it'll be the same as if he slain a lamb. Aditya said
he's being considerate, in giving Steven a choice to slain his own son,
rather than doing it himself. The fact that Aditya know that Steven has
etoan descent didn't bother him. His son's pain is all in his mind right
now.

He undone his armor, shakingly approaching his son, he took the dagger
from Aditya's firm grasp. David transmitted this, the feeling of
surrendering his fate to his trusted father. David knew both of them
couldn't do much, and if they defy Aditya's demand, both would have died
anyway. David wished that at least his dad would survive. David also
notified his dad that he had been backed up this morning. David might
have said that he's okay and he's ready, and that he's fine as he had a
recent backup of himself. If he were to be resurrected later, he
wouldn't feel this pain anyway. But this thought occurred to Steven:
That would be a facsimile of his son. He wanted his son, this son he was
going to slain. All of this time he brags about the wonder of Aucafidian
technology, that everybody is practically immortal. If you die, you
could be resurrected. Death is voluntary. But right now, he said to
himself, David's death wouldn't be voluntary, they were forced. Even
with all of Aucafidian tech's wonder, what happened right now is far
from what makes Aucafidian great: freedom of choice. This kind of
thought repeated over and over in his head. A thought occurred to him,
that he would just have to slain David right now, then himself, then
when they both resurrected in Aucantica, with the rest of his family and
his friends, they wouldn't remember this moment, this pain, this agony.
A good idea, indeed, thought Steven. He sank in his thoughts, he didn't
even realize how many tears he had shed, not even David's, not even to
the fact that they're hugging each other right now, both crying like
babies, not even to the fact that the dagger in his hand had been in
David's flesh, halfway to David's heart. A second later, Aditya heard
three screams: Steven's, David's, and his. The dagger didn't make it to
David's heart. It's in Aditya's neck. Steven throw it in the last
seconds. With a command Steven's armor moved and wrapped itself around
David, protecting him inside and tended the wound.

His armor immediately flew, away from them, straight to his house, where
his ship's waiting. Aditya furiously ordered his men to eliminate
Steven, and Steven, equally furiously, evaded their attacks and rushed
directly to Aditya. He caught Aditya to the ground, pulling his dagger
out of Aditya's neck, without even care how miraculous it was that
Aditya's still alive after such that wound in his neck, and he prepared
to stab Aditya in his eyes. His eyes, Aditya's eyes gleamed a deep
regret. Aditya pleaded: Steven! You're a doctor! You're sworn to help
people, not to harm them! Steven answered Aditya, soon after, in a cold,
heartless tone: Oh, him? You killed him when you forced him to slain his
own child. Perhaps the last expression shown by Aditya's left eye was
that of fear, ultimate horror realization of waking up a sleeping beast.
That day, Aditya lost his left eyes, and severely damaged his optical
nerve when the dagger reached deep into his eye socket, and straight
into his visual cortex. Before he could even pull the dagger to break
more nerves in Aditya's head, his car zoomed into the building, leapt,
and almost impossibly morphed in a shape to catch Steven and put him
neatly in his seat and then, touchdown and left the scene as fast as it
entered.

The event scarred Steven quite terribly, even after his family and his
friends comforted him. Treatments were offered by local etoan doctors,
that includes the most popular of the inner sphere: his current copy
were retired, and he would be resurrected from the latest backup prior
to the event, and feeded third person POV recreation of the event,
complete with detailed information of his mind states at the moment,
back to his memory. It was said that the experience would be like
watching a movie, he would understand what he felt at that moment, would
sympathize his old self, but without such traumatic feeling emerges
(well, in most of the cases). It's worth noting that etoan doctors, with
all of the wonders of Aucafidian technologies, were more like a sales
person, to let his patients choose between large arrays of products, and
changes were made appropriately by technology, than by laboriously
performing operations, therapeutics, et cetera. The doctor's job is to
find the best treatment options, and fine tunings were done by
appropriate expert systems. In a way, Steven's actually a more competent
doctor than etoan doctor examining him. In fact, Fernando and Daniel,
Steven's friends, are also doctors with far more competency in actually
treating people, even with comparatively ancient age medicine (against
etoan medicine). Having traumatic experience with considering backup
technology, Steven resented on having his current copy retired and
resurrected from earlier point of time. However, that feeling of pure
villainous desire to kill Aditya he felt back them, bothered his
thoughts. He violated hippocratic oath. Interestingly, it's not in the
"do not harm" part, as commonly believed to be taken from hippocratic
oath. In fact that phrase never appeared in the original text. Perhaps
that was a shortened version of this phrase: "Into whatsoever houses I
enter, I will enter to help the sick, and I will abstain from all
intentional wrong-doing and harm." Steven decided that he would no
longer practice medic.

Fourth quarter of 2024, Daniel was urged by his family to immediately
marry, and stop toying around with Fernando, as he reached age 39
(closing to 40). They've found a suitable mate for Daniel, that was
Danielle Nandaputri (35yo, okay his family do have this weird sense of
humor), a widowed lady with a 4yo son, husband killed in 2022 attack.
The family arranged their engagement soon after. At first Daniel
strongly objected his family's act, but his father threatened that he'd
be denied of his birthrights, and his mother cried as he's the only son,
and his mother wished him to have a family, a normal one. Fernando do
understand that eventually Daniel has to marry, to get children he
always wished to (Fernando and Daniel can't legally adopt a child
anyway). For some weeks they're struggling with it and Fernando decided
to back away, despite Steven's and Helena's suggestion to not leave
Daniel. Daniel can't decide what he wants the most, children or
Fernando. 16 November 2024, on Daniel and Fernando's anniversary,
Fernando suggested that they should finally break up. Daniel asked why,
and that he can't. Fernando said that they've been together for 21
years, if they could have children, they'd have enrolled to a decent
university. However Fernando and Daniel can't have children, and
Fernando knows that Daniel really love to have children. He said that
Daniel sacrificed a lot for him, for a relationship they both know
wouldn't be recognized even by the state. This time, Fernando wished to
return the favor, he'd sacrifice his love to Daniel so that Daniel could
have children and decent family he always dreamed of. Fernando then was
going to kiss Daniel one last time, but enraged, Daniel rejected it and
told Fernando to get out. Daniel's tears spilled. He felt like being
betrayed, as all of his sacrifices were made so that he could be with
Fernando, but now Fernando easily said that they need to break up so
that Daniel could recover what he had sacrificed. Daniel at the moment
knows that he wants Fernando, and Fernando alone.

Helena followed Fernando and asked him why he said that to Daniel.
Helena said that Daniel would be extremely devastated if Fernando left.
Helena said that she doesn't want to let Daniel feel the same way she
felt. Her love didn't reciprocate, so she married another man, but
carried with her was guilt, of loving someone else, buried deep inside.
She kissed Fernando. Fernando asked her, what was the meaning of that?
Helena said, she doesn't know. Perhaps after all of these times, she
still loves him, but at the same time she felt guilty of cheating
Steven. Fernando said then please don't, Steven will be hurt to know
that. Helena argued that Steven and Daniel were fortunate to get their
first love as their dates, but Fernando and her aren't. Fernando loved
Steven first, and Helena loves Fernando first. She's afraid that if
Fernando leaves Daniel, he might have married, but he'll always have a
special place for him in his heart. And Helena said that it doesn't feel
right. She asked him, she believe Fernando understand what she felt, as
Fernando once loved Steven. Fernando said yes, he loved Steven, and that
he has a special place for Steven on his heart, but that just means he
loved him once, and people change, so do feelings. Helena cried that
he's lying, Fernando said that Helena should've let him go before she
even consider to date Steven. Also, Daniel's first love is Helena, and
he did move on in the end, which proves Fernando's point. Helena was
surprised. Fernando continued, that this means he can love women.
Fernando can't, but Daniel can. He believes that Daniel could move on
and be happy with his new wife. Also he recommended Helena to go to a
therapist or marriage counselors. He left.

Even as if that's not enough, insurgents (known as KERNoK) do another
harm to civilians by bombing a commercial airplane. What matters,
however, is the fact that Helena was on that flight. Perhaps the second
thing to note was the fact that Aditya planned and ordered the bombing.
Perhaps the third, Aditya didn't meant to kill Helena: she was his
cousin. Lastly, however, Aditya knew Helena was on the flight, but that
day he made a decision, his agenda first, then everything else seconds.
Helena's death was a major factor on Steven's decision to start anew, in
Medan. He opened a grocery store there. Daniel, kindly join him to
Medan, and instead opened a new clinic right beside Steven's grocery
store. Fernando, on the other hand, decided to go to Venus with
initiatives of KukerVenus Program, KukerCorp's venus colonization
program. Steven's new life proceeded as usual, and rather peacefully. He
almost moved on from his trauma with Aditya, he almost made peace with
himself, when, at that time, April Fool of 2027, Aditya visited his
grocery store.

It was dawn, Aditya discovered a suitable building to be his central
base on his next goal: Military take over of Medan. At that time, Aditya
dispatched some of his men to ambush Steven's sons, on their schools
(Peter, Eden, and Hayden schools on different institutions). Right at
the moment, Daniel, David, and Steven arrived at the lot. They weren't
there because of Aditya's presence, rather, they're there to open the
store, which, obviously, couldn't be practically done at the moment (or
rather they can, but opening their store aren't their number one
priority). Quickly realizing the severity of current situation (and
what's left of Aditya was there), they bury themselves inside their
armors. They exited their car, and Aditya greeted them. With them were
attributes of KERNoK, attributes so clear in Steven's head, as
attributes of those claiming responsibilities of the bombing of his
wife's airplane. David realized that too. Actually, all three of them
knew that. Steven cried: you crazy psychopath! You let your cousin die!
Aditya remained silent, and Steven realized something. His eyes were now
non human eyes, and more bionic modifications were pronounced more
across his bodies. His bionic armors appeared to be fused with his body.
Daniel could feel Aditya's lack of humanity at the moment. David quickly
charged toward Aditya, on which Aditya responded with a sudden jump.
They fought midair, and eventually David's thrown back to land. Daniel
and Steven quickly flew to David's position, Steven ordered him to
quickly help his brothers, and he left the scene reluctantly. Aditya
told his men to intercept, but Steven's car blocked them, all weapons
sprouted out. Aditya congratulated Steven that his car was now fully
armed, instead of just stomping and hitting opponents like the way it
were. They fought. However after some time Daniel told Steven that it is
probably better for the car to join David instead, so that his sons
could board it and safely transported to his house. Daniel, as usual,
gave Steven the most brilliant idea of the day.

To orchestrate decisive attacks of a city, Aditya knows that he'd need a
very powerful and secure communication network, something he couldn't
usually get on most ISPs in Indonesia, but one, very unpopular (or even
almost unknown) ISP known as KavNet, with internet links up to some
hundreds of terabytes per second. This particular ISP is not very
popular among humans, because its customers aren't for individual human
or groups anyway. Monstrously lightning fast internet connection KavNet
provide wasn't meant to access human internet (also known as World Wide
Web, or The Internet) as well, and connection to The Internet comprises
a very small, nearly negligible fraction of its second-to-second
operation. It's solely for etoan customers on earth, and two
mixed-ethnicity corporate group known as Securion Incorporated and
KukerCorp. Incidentally, KukerCorp was the founder of G28 Private
Security Firm, which also benefits from this ISP. KukerCorp, was also
the legal possessor of Kuin, which is a major factor of KukerCorp's need
to benefit from this network. There were talks among high-ranked
masterminds of KERNoK to seize KukerCorp Medan Branch Office and G28
Medan Branch Office instead, but they were obviously highly guarded by
G28 officers, and Kuin's security drones. G28 Medan Branch Office sat
nicely with all of its facilities, 655 cars and about six thousand
strong officers. Local national police offices does have more officers,
about twenty thousands, but the power of capitalism bestowed private
police firms like G28 with more funds, more facilities, and higher
standard of officers. Of course not all private police firms are that
successful, this one firm was just extraordinarily lucky, they have
Kuin. This left KERNoK's options open to just two other buildings
possessed by etoans: Steven's house and Steven's grocery store. They
opted for Steven's grocery store instead. Incidentally, Kuin also knew
all of this and the fact that Aditya would choose Steven's grocery
store. It dispatched twenty four highly armed officers by three heavily
armored cars toward Steven's grocery store.

Rafael is a very fine and skilled young private police force officer,
almost to Mary Sue's level of perfection. He's a member of private
police firm known as G28. He's also a signatory member of Oneiric
Protocol. This particular protocol thrilled him at first as it promised
actions and secret missions, perhaps almost bond-esque feeling that he's
after. However four month after signing the protocol his expectation
dwindled down as nothing happens. He started to think that this protocol
is a scam. However at this particular day his phone that was provided
when he signed the protocol, rang. A coup is on the run, and he's tasked
to examine the situation and wait for agents to arrive, meanwhile act
like any other private police officers. Perhaps a thrill the protocol
promised he thought. His best friend, Jonathan, also a member of this
protocol, were with him at the time. Jonathan signed the protocol
yesterday. Perhaps a very important foreshadow, that today, one of them
will die and will be resurrected, by something neither one of them
really understand. In fact, anything that's going to happen on the field
today is inscrutable to all of them, atop this coup-in-action cover.

Rafael and Jonathan were on a vehicle, one of G28's armored car. They
arrived at this designated area, only to be stunned by a motorcycle
falling in front of their car. Soon after they observed the thrower of
this motorcycle, a humanoid winged monster, fighting what appeared to be
another black crystalline winged monster. The car's built-in camera only
detect the crystalline monster as a bright bluish object with rough
outlines similar to what's visually apparent. Jonathan said it was an
angel fighting a demon. Rafael refused to believe that, and took his
Oneiric-only weaponry online, then drops down from the car. All other
apparently are doing the same. Yet they have no idea which one to be
shoot down. The humanoid winged monster has many more minions, and
apparently able to command a human army, so he's clearly the so-called
insurgent leader. But the crystalline 'angel' is somewhat confusing,
it's fighting the insurgent leader, but does it mean that the 'angel' is
in their side? With an order from current PIC, Rafael then took a good
position on a parking lot and shot the monster instead. They all shot
the monster and its armies, who are also shooting them, with clearly far
more advanced weapons. They're almost outnumbered, and overwhelmed,
until Rafael got a clear line of sight to one of this monster's four
eyes. He pulled the trigger, and the monster avoided it. The monster
then flew to the parking lot, releasing some fatal spike shots, which
Rafael barely able to evade them all. He was relieved until he realized
that Jonathan is still standing, with spikes penetrated into his chest.
He looked at Rafael, and coughed blood, then fell. Definitely this kind
of action scenes are what Rafael expect, but not the death of his best
friend. He's yet to learn that this kind of scenes would cost him
plenty. He's prepared to charge toward the monster, but apparently one
of the 'angels' (only now he realized that there are two of them) jumped
in front of him, while another is wrestling with the monster up in the
sky. This one angel then asked him when's Jonathan shot, and Rafael said
just recently. He said let us wish he could still make it, and then
produced a number of magazines out of his left arm, and pulling one with
his right, injecting its contents into Jonathan's wounds. The wounds
sealed with whitish gel. Then the angel flew away after making sure that
Rafael isn't wounded.

Steven's armor is cracking there and there, faster than its nano could
repair itself. But definitely Aditya's slowing down, perhaps due to
fatigues, as his armor is biological, so eventually they'd need to
recover after some time. With a cue, he and Daniel managed to drive (or
push?) a truck car and thrusting it against Aditya. His armor can handle
it, but still he'd having difficulties moving. At least that's what they
thought. Aditya crawled from below the car and to the side, pulling
Steven from its wheel and managed to get Steven few meters along the
ground.

Aditya's take over of Medan. Steven and co is here, revelation of G28
and Oneiric Protocol. Aditya ambushed his sons, and him as well. Pretty
exhaustive fights and wins. It is revealed that Aditya's not just a
normal human, and his affiliation with Denefasan is to be pronounced. In
the end Steven detained him.

## Part 2

Aditya's raiding the Securion's security fault, where Black's Staff is
stored, BAIK agents are trying to stop it. In the end however, they
managed to capture two or three of Aditya's men, yet Aditya managed to
run with the Black's Staff. With that information obtained from Aditya's
men, they are able to locate Aditya's base.

Search for the serpent, then it led to a cult of the Umbra Cahaya, about
the incoming Sea Serpent, Lord Yam as the Helper prophesied by Jesus.
Going deeper, Henokh and co surprised as discovery about their motive is
made, that is to steal Black's Staff to trigger Judgement Day. It's
quite obvious at the time that they must stop it.

Anderson's attempt to stop Aditya and the death of Bain at the same
time. Here Anderson notified about possible Antichrist by Etovexiri. He
decided to take the matter all by his own. Then he put an explosive to
corporate\'s car that Henokh is going to take. Aditya got a warning by
one of his agents on KukerCorp, decided to take a move. Anderson checked
the bomb again at post midnight, but found out that the explosives had
been removed. Aditya faced Anderson.

Derictor wakes Henokh up, into a meeting with Hendrik. Derictor briefed
that Klandestin reported a significant activity is detected within
Gedung B, Headquarter of KukerCorp, and that from security footages
there\'s Aditya and Anderson. Hendrik briefed the possibility of
Aditya\'s association with Reflexiors, his kind, due to increased
activity of reflexium on the building. Hendrik then ordered them to
quickly prepare and go to the building, and that Jokowi, Tujuh, and
Sembilan were ready near the building.

Anderson asked Aditya to return the explosive, and they went into a
fight. Involved in a fight with Aditya, Anderson got hurt. Here it is
revealed that Aditya's not just a normal human, as Anderson isn't as
well. Aditya triggerred the bomb, and a part of the building exploded.
Bain is dead. Continued with a pursuit through the city. Cornered,
Aditya jumped off the bridge and a giant sea serpent took him into the
water.

Ambush to Aditya's base, pretty neat fight and revelation that he had
been working with secret society known as Reflexiors. Anyway they didn't
find Aditya there, and interrogating his men resulted in revelation that
Aditya's on a takeover of Medan. As they arrive, however, Aditya had
been detained by Steven.

Steven grant Aditya to Henokh for him to process. Henokh interrogated
Aditya and also it is revealed that Anderson once tried to kill Henokh,
as Henokh is suspected to be this so called Antichrist, yet is stopped
by Aditya, which resulted in Bain's death. Later, however, Anderson
doubted it and felt guilty of it.

Aditya were rescued by a dragonfly, Hendrik went for pursuit, yet in the
end his body got hurt and almost died. Steven and Daniel who's in
pursuit as well tried to preserve Hendrik's body, and they went for
Medan's G28 Branch Office, Rafael's office, and regenerate his body.

## Part 3

During his body's coma, Ein, his paralel, were called upon to a
nondescript paramundus, by Master Leicht. Here Master Leicht informed
him that the Divine Council shows escalated activity, on Shamayim. Yam,
Lord of the Sea is out of reach lately, and Hadad, Lord of the Sky,
issued a high level warning about predicted second arrival of an agents
of Kavretojives. What's worrying, is that El, The First Lord, or also
known as Tarax, had prepared a factor X, an Antichrist, to counter
agents of Kavretojives. The so called Antichrist is already here, living
on Earth as well. This coincided with intel Anderson got that Henokh
might be the Antichrist. Ein asked Master Leicht what to do about this
situation. Master Leicht said that he'll explain after both of them and
Manov reunited.

Aditya, now rescued, prepared a ritual to activate Black's Staff.
Contacting Lord Yam for instructions, and with the help of the Black's
Staff preparing their assault on the last obstacle to his plan, Earth's
immune system.

Manov, Hendrik's paralel, meanwhile, along with Henokh, investigated a
new lead to the Umbra Cahaya's secret temple, and gain insight that a
ritual were performed here. What's worrying Manov however, is the sign
of Yam, the Lord of Sea. From Ein he knows that Yam is supposedly
unreachable, but by the sign on a very large deep blue sapphire with
very intricate patterns traced on it deep to molecular level, he knows
that Yam is successfully contacted.

Ein and Master Leicht gathered with Manov, and to their conclusion,
Master Leicht explained that they need three keys to open the gate to
Paramundus Sheol, and within it is Hell. This isolated paramundus
contains imprisoned Old Gods/Powers/Deities/Lords. The first of the
three keys is The Heart of Mot (Lord of Underworld and Archival), or
known well as Black's Staff. This is to unlock the "body" of the prison.
The second is to unlock the "soul", which uses The Consent of Yam, which
comes in the form of Yam's signature of approval, the Blue Sapphire
Mark. The third is, to unlock the "spirit" part, The Essence of Hadad,
which in form of the computing matrix[^6] which is the basis of an
individual known as Cain, the first Reflexior to commit murder.[^7]
Currently as a part of his curse to live forever to the end of the
world. Within the last century, he asked The Foundation[^8] to contain
him in a safe place, as he is afraid that some Powers might decide to
rip off his soul to obtain the Essence of Hadad, and to use it for
malicious purposes. The Foundation then transferred him into Paramundus
Integra, the safest haven in all Paramundi Complex, equivalent in
security to the Paramundus Sheol (abode of the dead).

What Master Leicht is not aware of is why KERNoK must unlock Paramundus
Sheol, though he knew that it might be related to the Old Lords.
Probably to release them, but for what? Master Leicht's concerned that
the Old Lords might be too dangerous to let go, and that they must do
must they can to stop the Old Lords from taking over the present Divine
Council, currently led by Lord Hadad. Especially from El, the former
King of Lords.

## Part 4

Hendrik got a list of names, related to seemingly unrelated random
recent killings across the world. Investigating it leads to a
volant-possessed girl. The girl gave him remarks that the armageddon is
starting. Hendrik managed to extract out required intels, and decided to
follow the next lead, a paramundus known as Integra.

Hendrik consulted with Master Leicht, and then told that to enter
Integra, he must be on similar order of magnitude of a normal men's data
stream, that is about 1e19 bits, but due to his other two selves he's
now about 1e20 bits long to describe. Master Leicht's solution is to
have him piggy-backing with other normal men as his buffer. As he's
about to call his volant friends, Master Leicht advised normal men to
aid him, as they're relatively simple, and will grant sufficiently
generous amount of computing cycles available for his codes. He chooses
Henokh and Derictor to aid him in this adventure. However later he asked
Steven, and Daniel, as they have this etoan augmentations that might be
helpful in interfacing with RFL, as RFL is also etoan tech, albeit
outdated for some 20k years.

Hendrik, accompanied with Henokh, Derictor, Steven, and Daniel, went to
Integra and discover it to be entirely alternative version of earth
where earth is the only host of intelligent life. There's no El's
influence, there's no denefasan and such, just earth and humans. They
entered through a portal, an anomalous door connecting the rest of
Paramundi to Paramundus Integra, and are the only way in. There they
meet a Global Foundation of Esotericism (GaFE), apparently Reflexior's
attempt to gain a foothold in this world. There they explained that
anomalous objects were bound to be created in this world for a
sufficiently large contamination from outside world. They've been
keeping this world more-or-less pure by containing most of anomalous
objects, which, ironically, in doing so, they utilize anomalous objects,
like this door that connect other paramundi with Integra. The Foundation
has about a hundred of them, but it is estimated that there are about
fifty or more outside their reach.

To defend themselves, they were equipped with ironically anomalous gun
that have no magazines but will shot a stream of kill command, to kill
target processes. They just have to aim it like a normal gun. They also
equip themselves with Napkin of Lost Stuffs (NLS), which could grant
them in-world-objects that's presently not not observed by any
individuals at all. Then a Unseen Wand, to grant them limited control to
objects not in people's observation scope. Then a zipper bag that could
take inanimate objects of any size within, and could later be used to
clone said objects (should animated objects enter, they'd be deleted
instead). Then a Magician's hat, that could be used to spawn any
animated object that could pass through the hat's opening. That's all to
make do in this world, and for emergency occasion (as there might be
Lords involved), they were given a Fiat Ring, one of three, the most
powerful item in this world, that could create miracles. However they
must not use it for a world-scale anomaly, because that will surely
create yet another powerful anomalous object, powerful enough to destroy
this world. Anyway its usage is limited by their shared computing cycle
quota, that, must be no more than 2e19bps per person (in total for six
people there's 1.2e20 bps, but Derictor and Henokh uses up 2e19bps
collectively, Steven Daniel Fernando used up 5e19 bps collectively,
leaving only 5e19 bps for Hendrik, which even without his other two
selves, have 3e19bps). This one Fiat Ring, is however in form of a
smartphone.

They then left the office, which is in form of an old DVD rent store. To
navigate the world, they must find some items to lubricate their way up.
First they'd need ID. This one is tricky, as apparently only Hendrik has
in-world equivalent (Hendrik looked it up on Fiat Ring), and with NLS he
grab this world's Hendrik's wallet and phone (certainly not a good day
for this world's Hendrik). Henokh has no equivalent, as his father
married other lady, and his mother is an old virgin. The closest they
get is Henokh's biological sibling (which isn't in real world) called
Sylvester Lisander, which is mad, even by Henokh's standard. Hendrik
grabbed his phone and wallet (and a car key) when Sylvester's bathing.
Steven is not even supposed to be present as he's half etoan, and etoan
has no place in this world. Daniel happened to be present there, but his
family is poor in this world, hence having a relatively cheap phone and
not so much cash in his counterpart's wallet (by mercy he asked Hendrik
to return his phone to his counterpart). Out of curiosity, Daniel asked
what about Fernando's (was Daniel's partner, in real world he joined
Venus Colonization Initiative) counterpart in this world. Hendrik said
he's is attempting suicide presently and nobody know about it yet.
Daniel urged Hendrik to help him, but Hendrik refused, saying that
what'd be the use? Daniel was thinking for a moment, and then said that
they'd need more than a car. Five person in a car Sylvester own, would
be very cramped. On the plus side, they'd save a soul. Hendrik said that
it was fair enough.

Within a parking lot, Hendrik then conjured, with Unseen Wand and NLS,
to make Sylvester's car be present in this parking lot, which happen to
be unobserved by any person at the time. They summoned Fernando's car as
it sank in the river, and ressurected Fernando. Hendrik, Henokh, and
Derictor entered Sylvester's car. Steven, Daniel, and this world's
Fernando entered Fernando's car (now fixed). Fernando is tended by
Daniel, and is catching up on all bizzare things that just happened. And
they decided to go to Fernando's counterpart's house (he lived alone!).
With a working internet connection in Fernando's house, they take their
time to find out more about this world.

Hendrik's looking for Cain, and on attempt to locate him, he discovered
that his version on this world is a writer, that wrote about Steven's
story, Henokh's story, to certain degree of accuracy, and currently his
webseries entry is about Hendrik's story of detaining a girl that's
possessed by volants. Perhaps what surprised them the most is the fact
that there's a separate story for Steven, Daniel and Fernando's
university life! This also includes something that Hendrik didn't know,
such as that the girl is in fact a trojan horse for a malicious volant.
They questioned what it means, because then Integra is more connected to
the outside world than they thought it was. Probably a result of anomaly
or something. Probably it's just Mot's bad sense of humor.

Steven, Hendrik and Henokh entered Henokh's (distant) counterpart's car,
and Fernando, Daniel and Derictor entered Fernando's counterpart's car.
They aimed for Hendrik counterpart's residence, but during their travel,
they were chased by a criminal gang for unknown reasons. Certainly
they're an occult team when one of their shot caused Fernando's car to
disappear, and three of them fell on the road. Hendrik stopped and
confronted them with their also anomalous gun. He called for
reinforcements from Global Foundation of Esotericism, however they need
ten minutes of dispatch time. They were then involved in a gunfight with
the gang, a conflict that the natives interpret as a gang war. They were
baffled at who is the head of the gang, and why would they attack them,
specifically. All of them were puzzled, until Hendrik noticed the face
of the gang's leader. It was Gabriel. Talking with Gabriel revealed that
they want to find Hendrik's counterpart first before the protagonists
got him. Apparently Hadad wished to obtain Cain first and ordered
Gabriel to fetch him. Hendrik suspected that Hadad might be behind this
attempt, probably to retake his essence off Cain, for the purpose of
releasing the Old Lords.

As a drastic measure is required, before they shot their anomalous
weapon again and risking either one of them puffed into thin air,
Hendrik throw the magic hat after ordering it to eject copious amount of
kittens to distract them, he used the Unseen Wand to conjure a lift to
open and all of them entered. They struggled to enter but some of the
Gabriel's men managed to stop the lift door to close. Gabriel urged them
to cooperate, as they've misunderstood the situation, and offered an
explanation. Hendrik, too adamant to not hearing Gabriel. With Fiat Ring
Hendrik conjured so that the locking mechanism and the lift mechanism to
fail and they fell down, breaking the men's limbs in a bloody mess, and
Hendrik do an anomalous attempt to save them, making the shaft fall for
extended period of time, and they exited the shaft soon after, high
above the city (the lift reached a depth where a buffer overflow is to
happen and their altitude parameter reset, from lowest point rendered by
the world's virtual rule to the highest point rendered). They're free
falling a hundred km above the parking lot. Panic, Hendrik make the lift
into a shuttle. They reenter the atmosphere safely but with worldwide
eyes on them. They have to hide eventually, but a report shows up about
dangerous, anomalous weather phenomenon.

The Fiat Ring displayed a picture of a shark, and a word, Kamaitachi.
They've caused a very dangerous anomaly to appear in this world. Below,
as they reached lower atmosphere and are now gliding, they could see
buildings to shatter with clean horizontal cuts.

They then visited Hendrik of this world, and with his help, they can
locate Cain. Cain denied Hendrik's offer to return to the real world, as
Paramundi Complex is about to be jeopardized by the old lords. However
Cain said that it is what the Lords, whichever Lord orchestrating this,
wanted Hendrik to do.

In an attempt to secure Cain, they forcefully took Cain to real world,
without realizing that this action allows a loophole that's exploited by
the trojan to copy Cain during transfer. Now the Lords have what they
want to have, and within minutes, The Ophanim, the ring that contained
Shamayim, lowered the Heaven's Ladder, a very bright bow of light
descended above Earth's terminator. Heavenly Hosts flew from it toward
the entire world.

Desperate to stop it, Hendrik reasoned that now they need to go to
Shamayim. Master Leicht materialized near Hendrik, and revealed an
intel. They need to find Kothar-wa-Khasis. Kothar was a good friend of
Lord Yam, and was also the Lord of Craftsmanship, the wise inventor and
architect. Apparently, millenia ago Kothar was seduced by Hadad and
Yahweh to betray Yam. Master Leicht Kothar might felt guilty about it.
Hendrik asked if Lords could feel guilt as well, but Master Leicht
insisted that that might be their only chance to defeat the Lords. After
all, Kothar was the creator of a weapon that could kill a Lord, and was
used to defeat Yam long time ago.

As of the means to get to Shamayim, Master Leicht stated that, Shamayim
might be very restrictive on earthen men to visit it, and visa is of
exorbitant cost. But for Etoan, they're opening a channel for tourist
visas. Steven is an etoan, so they might be able to sneak in. With
Adran, Steven's car, they flew to Shamayim. To decrease suspicion,
Steven and Adran injected etoan nanos to the rest of the group but
Daniel (that had been injected with the nano for years now). That way
they could act as etoan tourists so eager to experience life as a human,
in a (supposedly) newly printed human body.

Kothar-wa-Khasis wasn't that hard to find, as it is right on the
entrance of Araboth. A pyramid. Steven asked the guard, saying that
they're a fan of Kothar, and wished to meet Kothar in person. The guard
said that the Divine Council is in session. But then an angel landed
nearby, apparently an avatar of Kothar, greeted them. The angel backed
off, after Kothar's eye signal. Kothar escorted them to the lowest point
of the pyramid, that shows a hologram projection of the Ophanim's
underside. Manov then shamelessly asked Kothar to grant them with
Yagrush and Aymur. Kothar laughed, stated that the weapons were designed
for the Lords, not for the mortals. Even if a mortal can operate it with
a grain-sized faith strong enough to move a mountain, it's almost
impossible for a mortal to have that much of faith. Manov then
challenged him, "what if we can?". Kothar was rather displeased with
that, and said that many have said so, but later they disintegrated and
turned into data noise, for Mot to delete instead of entering Paramundus
Sheol. But then Kothar said "Even if you can, what's your worth to bear
it? No, I mean, what's it for me if you can wield it, why should I give
it to you?" Hendrik frustrated, and decided to attack Kothar personally,
"This is a chance for you to make it right with Lord Yam."

Even with his vast mind as a Power, there's a delay, he's actually
thinking about it. "So you know." He smiled, however his eyes projected
sadness. Intrigued, Daniel asked what's the deal between Yam and Kothar
anyway. Kothar looked at the reflection of earth's surface at the
collector arrays of Ophanim. "I had, no, have a crush with Yam, and
millenia ago it was almost realized into something else. If not because
of my change of heart, to Hadad." Hendrik stated, "You helped him to
kill Yam". Kothar: "And I still regretted it. This regret, was also the
cause Hadad killed me, to prevent me from betraying him with this
guilt." He gazed to Hendrik. His eyes projected pain, held behind his
expression. "You were in hell afterwards" Hendrik responded. Kothar's
painful gaze faded, "Correct. Ironically, I built Hell. And after
Hadad's reign, the rule of torture was, to use one's guilt, in their
conscience, to torture a soul. It was designed for mortals and lower
powers. Surprisingly I'm too good that the rule I set worked very
effectively for Lords as well." Derictor interrupted, "Until Hadad
decided to break through Hell and released all of you." Kothar turned
his gaze to Derictor, "No, it wasn't Hadad. Yam personally entered my
torture chamber, and dragged me out. But that's it. Yam gave me a last
gaze of his eyes when he entered, and no more communication with him I
have afterwards." Hendrik felt something is off, "But, wasn't that Hadad
that wanted to release all old gods?" Kothar replied, "No, Yam did it,
to confuse Hadad, and to remove him from the throne. I don't know why,
Yam never told me to. But I believe Yam's still angry with him, or
something else. I don't know. But I warn you, Hadad is a dangerous
being. He must be charismatic, and I made a mistake once by believing
him. I urged you don't. You must stop him." Kothar granted them Yagrush
and Aymur, even explaining what they are, basically. Yagrush, manifested
as a large, fiery wolf, bounded by a harness on its neck, that's tied to
a left hand gauntlet, is a seraphim, whose purpose is to hunt a target,
any target assigned to it, and impart damage to it. Aymur, manifested as
a right hand gauntlet, is a cherub, is a tool to change the world, it
affects the hardware, the operating environment, the ultimate fiat
engine. If the user could operate them properly, these two weapons can
do everything.

After they received the weapons, now it's Steven's turn to ask, "Ehm,
how to use it?" Kothar just said, "Surprise me." Adran prepared the link
between five of them, to help them prepare their mind for controlling
the gauntlets. Daniel wielded Yagrush. The fiery wolf exploded into a
pack of fiery wolves, they're sniffing the room, searching for a target
yet to set. Confident enough that they could manage to wield the two
weapons, Hendrik wielded Aymur. They flew into the void, and lost their
consciousness.

They woke up in a city. A lot of men were around. \[Adaptation of Dream
20131229\]

The Protagonists were visited by Steven's children in their pocket
paramundus, being reminded of who they were, and gained control. The
Protagonists use the weapon against the Divine Council, Yam and Kothar
escaped the council. Yam took a moment with Hadad and El, they were
arguing. Yam wanted to see Hadad's destruction as a revenge of what he
did to him, when Yam was about to be coronated as King of Lords. To his
defense, Hadad said that Yam was too chaotic, too dangerous and too
powerful, and his approach, all men entered heaven effortlessly, is not
an act of mercy, but madness. All wrongdoings must be punished. And Yam
wanted to make El witness his second fall, and how much he hated El, so
much that he managed to make El favored him over all other Lords and
wished to coronate Yam. Yam explained why: El is too perfectionist,
demanding impossible perfection of mankind that no one is worthy of
salvation by El's standard, and Hadad put humanity in confusion, by
cutting off communication with mankind, while torturing them based on
their guilt. Many were confused and tortured in hell only because their
conscience isn't clear, even if they're not doing anything wrong.

//FILL//

Divine Council is no more, Hadad, Yahweh, El, and many other Lords were
defeated, The protagonists relieved, and they decided to return home.
Things seemed to return to normalcy on earth's surface, and the sky's
now clear. The sky's now too clear, actually. Humans are now able to see
it clearly that something's wrong about the sky. Astronomers are first
to discover this anomaly. Stars changed, some are gone, some are
modified, some are disassembled, and the sky's suddenly filled with
organized noises of civilized galaxies. Torches of space drives could be
observed all around, metric disturbances found, and the world really is
turned normal. The RFL's Ignorance Inducer Arrays were broken beyond
repair. Among that, one could observe the incoming fleet of alien ships,
currently on earth's orbit. It descends down with loud roars, all over
the world, simultaneously.

[^1]: Etoan is a species of humanoid surprisingly close genetically with
    hominin, especially on genus Homo, that they could interbreed with
    normal Homo sapiens, though female etoan and male human pair
    resulted in disabled, infertile, and/or nonviable offspring. Viable
    and fertile human-etoan hybrid is always male as female offspring
    always fail to develop. Basically similar in capability and
    intellect with a normal human, however they're adapted for slightly
    higher gravity than earth. Their darker muscle tissue containing
    more myoglobin not unlike that on marine mammal, and blood composed
    of polyhemeobtan matrix particles in place of normal blood cells,
    which is the main reason of their milk-like consistency and color of
    their blood. They also possess five chambered heart, very efficient
    lung, and a group of specialized neuron classes covered with thin
    iron meshes capable of transmitting and receiving radio waves,
    clustered near their parietal and temporal lobes, dispersed in the
    cartilage tissues of their pointed ear tips, vertebral lining, and
    their rib cages. They can see near IR and UV.

[^2]: Badan Intelijen Kukercorp, or Kukercorp Intelligence Agency, aka
    KIA. Is a private intelligence agency, mainly for commercial
    stability in states or nations where Kukercorp is present.

[^3]: Etoan superior is a type of etoan with far more capable and
    optimized body and intellect compared to normal etoan such as
    Steven. For all intent and purpose they're superior to normal etoan
    in all aspects.

[^4]: Reflexior is a normal human with specific mutation where Brodmann
    Area 44 and 45 of both sides of their brain were anomalously well
    developed. The dominant hemisphere side of BA44 and BA45 is the
    usual Broca's Area, human's language center, and the same part in
    the non-dominant cerebral hemisphere, usually not very developed in
    normal human, is highly developed into Reflexius Area. The effect is
    reflexior's natural ability to interact with RFL network of Earth.
    RFL stands for Rerum Flexius Lamina, or World Bending Layer, is a
    massive network of crustal sapphiroid computronium and surface
    reflexium nanite effector arrays. In short, the presence of
    Reflexius Area in a reflexior enabled them to do "magic".

[^5]: WTF stands for the Watchtower Foundation, is a governing body of
    reflexius somewhat similar to professional organization and/or
    nation/state that governs ethical reflexius and general reflexius
    policy.

[^6]: akin to soul

[^7]: the victim is Abel, his own brother

[^8]: The Watchtower Foundation, also known as The WTF, an organization
    of power, akin to a state, that oversees administration of
    Reflexiors and maintaining secrecy toward areflexi, non reflexior
    humans
