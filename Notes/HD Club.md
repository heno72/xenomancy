# Hexadecimal ducenti

Slogan: Duplex centum

So here's the IQ scores of the triad.

| Rank | IQ | Name | Note |
| ---: | -------: | ---- | ---- |
| 1 | 180 | [Steven Pontirijaris](Characters/Steven%20Pontirijaris.md) | An unaugmented Etoan. He's visual and kinesthetic learner. |
| 2 | 171 | [Daniel Lusien](Characters/Daniel%20Lusien.md) | He's visual and auditory learner. |
| 3 | 161 | [Fernando Suryantara](Characters/Fernando%20Suryantara.md) | Near perfect kinesthetic sense for a human, also strong in auditory learning. |
| **Sum** | **512** | - | **200 in Hexadecimal** |

The first time they took an IQ test when they enroll to their high school, they quickly discover that their accumulated IQ score is 512 in decimal, and 200 in hexadecimal.
So they come up with the gang name: HD Triad (later referred only as The HD Club).

HD stood for Hexadecimal ducenti, or two hundred in Hexadecimal.
Whenever they want to encourage each other, they'd say duplex centum.

Perhaps after they learned that Helena has an IQ of 154, they were stupefied on how to integrate Helena to the club.
It makes their combined IQ score be 666.
And Helena is a devout Christian.
It doesn't even sound good in Hexadecimal: 29A.
So they rarely mention it in front of Helena Irawan ever since.
