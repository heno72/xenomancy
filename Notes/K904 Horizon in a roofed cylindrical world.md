# (Counter)Rotationward Horizon in a roofed cylindrical world

![Problem](/Pictures/0904-problem.jpeg)

## Problem K904
Imagine two concentric circles of a shared central point ```c```, where the larger one has a radius of ```R```, and the smaller one has a radius of ```r```.
A line that we would call ```x``` intersect the surface of the smaller circle, tangential to the surface.
Naturally, the line intersect the larger circles at two points, let's call them ```a``` and ```b```.
Line ```x``` divides the larger circle's circumference ```2*pi*R``` into two, that is ```Q``` and ```q```.
As long as ```r!=0```, they observe ```2*pi*R = Q + q``` where ```Q``` > ```q```.

Now, consider that another line is intersecting point ```a``` tangential to the larger circle.
With that line intersecting at ```a``` represents the normal horizon of an observer standing at point ```a```, said observer is observing point ```b```.
The observer discovers that point ```b``` has an altitude of ```B``` degrees.

With only ```R``` and ```r``` are known, draw problem diagrams and construct equations to calculate:

- ```q```
- the length of ```x``` in between point ```a``` and point ```b```
- the angle of ```B``` in degrees

Provide your reasoning and steps in the answer sheets, of how you come up with the solutions.

## Solution K904
To know ```q``` we must also know that a circle is considered a full turn.
So to know ```q```, we must know the total circumference multiplied by "turns" of ```q```.
To know the turns, we must first know the angle that point ```a``` and ```b``` made against ```c```.

The angle at ```c``` could be obtained by considering ```a, b,``` and ```c``` as an isosceles triangle, with the height of ```r```, and the base length of ```x```.
Because the triangle is an isosceles triangle with ```c-a``` and ```c-b``` lengths are equal, we can split them into two identical right-angle triangles.
Examining either one of them gave us a right-angle triangle with height of ```r```, hypotenuse of ```R```, and base of ```w = x/2```.
Knowing the height and the hypotenuse, the base ```w``` can be solved with Pythagorean theorem, then ```x``` can be acquired by ```x = 2w```.

The next problem would be to discover the angle at ```c```.
Actually, the previous step to find ```x``` is not necessary to get the angle at ```c``` of the ```abc``` triangle, which we would call ```T```.
As discussed before, the ```abc``` triangle can be divided into two right angle triangles, the angle at ```c``` of each right-angle triangles, denoted as ```A``` (they're identical) would be ```T=2A```.
To get ```A```, it is quite trivial: ```cos A = r/R```, therefore ```sec (r/R) = A```.
As seen, to obtain ```A``` we just need ```r``` and ```R```.

The result is then converted to radians, and then to turns:

```
A[radians] = A[degrees] * pi / 180 [degrees]
A[turns] = A[radians] / 2 * pi
```

Remember that ```A``` is half the angle of the original ```abc``` triangle at corner ```c```, therefore ```T[turns] = 2A[turns]```.

The expanded equation would be:

```
q = 2 * R * sec (r/R) * pi / 180[degrees]
```

For the second question, considering ```x = 2w```, the length of ```x``` can be acquired by:

```
x = 2 * ( R^2 - r^2 )^0.5
```


For the third question, we must remember that the contact between ```R``` and the line at ```a``` is tangent to the surface of circle ```R```.
Line ```a``` would then form an angle with the line that is tangent to the surface of circle ```r``` and intersect circle ```R``` at ```a``` and ```b```.
Let's call this angle at point ```a``` between line ```a``` and line ```b``` as ```B```, per the problem's requirements.

Assuming a flat euclidian geometry, the sum of all angles inside the```abc``` triangle is ```180[degrees]```.
Knowing, that the angle at point ```a``` of the ```abc``` triangle can be acquired by knowing either ```A``` or ```T```.
Since we already know that ```A[degrees] = sec (r/R)```, and ```T = 2A```, we can get ```T = 2 * sec (r/R)```.
Being an isosceles triangle, ```abc``` triangle has 2 corners with identical angles we call ```G```, excluding ```T```.
Therefore, we get the following relationship: ```180[degrees] = T + 2G```.

Knowing that ```R``` connects to ```x``` at an angle, but ```a``` is tangent to ```R```, the angle between ```R``` and line at ```a``` is ```90[degrees]```.
Therefore, ```G + B = 90[degrees]```, so ```B = 90[degrees] - G```.
As we know from the previous paragraph:

```
G = (180[degrees] - T) / 2
  = (180[degrees] - 2 * sec (r/R)) / 2
```

Therefore:

```
B[degrees] = 90[degrees] - ((180[degrees] - 2 * sec (r/R) / 2)
```

### Answer:
- ```q = 2 * R * sec (r/R) * pi / 180[degrees]```
- length of ```x = 2 * ( R^2 - r^2 )^0.5```
- ```B[degrees] = 90[degrees] - ((180[degrees] - 2 * sec (r/R) / 2)```

## Solution Francis D.L. KA30
Apparently I was wrong on the definition of a secant. Hence, after a discussion with Francis, he told me that the solution should be:

From cosine law:

```
r^2 = R^2 + (a*x)^2 - 2*R*a*x*cos(90-B)
R^2 = R^2 + (a*b)^2 - 2*R*a*b*cos(90-B)

R^2 - R^2 = (a*b)^2 - 2*R*a*b*cos(90-B)
0 = (a*b)^2 - 2*R*a*b*cos(90-B)
(a*b)^2 = 2*R*a*b*cos(90-B)
((a*b)^2) / (2*R*a*b) = cos(90-B)

r^2 = R^2 + (a*x)^2 - 2*R*a*x*cos(90-B)
r^2 = R^2 + (a*x)^2 - 2*R*(a*x)((a*b)^2 / (2*R*a*b))
```

but: ```a*x = b*x```

therefore:
```
a*b = 2*a*x
a*x = (a*b)/2

r^2 = R^2 + ((a*b)/2)^2 - 2*R*((a*b)/2)*((a*b)^2/(2*R*a*b))
r^2 = R^2 + ((a*b)/2)^2 - ((a*b)^2/2)
r^2 = R^2 + ((a*b)^2/4) - ((a*b)^2/2)
r^2 = R^2 - 0.25*(a*b)^2
0.25*(a*b)^2 = R^2 - r^2
(a*b)^2 = (R^2 - r^2)*4
a*b = 2*(R^2 - r^2)^0.5
```

Since Line ```x``` = ```a*b```, we get:

```
x = 2*(R^2 - r^2)^0.5
```

Then,

```
sin(B) = x/(2*R) >> R = x/(2*Sin(B))
sin(90-B) = r/R
sin(90)*cos(B) - cos(90)*sin(B) = r/R
cos(B) = r/R >> R = r/cos(B)

R = R
x/(2*sin(B)) = r/cos(B)
x/(2*r) = tan(B)

x = 2*(R^2 - r^2)^0.5

tan(B) = (2*(R^2 - r^2)^0.5)/(2*r)
tan(B) = (R^2-r^2)^0.5 / r
```

Therefore, ```B[degrees] = arctan((R^2-r^2)^0.5 / r)```.

Then, as ```q = 2*pi*R*((2*theta_1)/360[degrees])```
we get ```theta_1 = 2*B[degrees]```. As ```theta_2 = 360[degrees] - 2*B[degrees]```, we get:

```
q = 2*pi*R*((2*B[degrees])/360[degrees])
Q = 2*pi*R*((360[degrees]-2*B[degrees])/360[degrees])

```

### Answer:
* ```q = 2*pi*R*((2*B[degrees])/360[degrees])```
* Length of ```x = 2*(R^2 - r^2)^0.5```
* ```B[degrees] = arctan((R^2-r^2)^0.5 / r)```

### Attachments:
Francis provided me documents of his solution, in both [pdf](Documents/KA30%20Francis%20D.L.pdf) and [docx](Documents/KA30%20Francis%20D.L.docx) file format.
