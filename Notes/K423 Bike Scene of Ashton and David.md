# 20200423 Bike Scene of Ashton and David.
Given that Daniel Ashton is legally 15 years old on around April 2021, the same for David, they can't bring their own vehicles just yet.
The minimum age to get a driving license is 16 years old for motorbikes.

Then it must be around 2022, near the time the invasion took place.
It also means, by the time David was delivered to his school, Ashton was already a part of Steven's household.
Adjust accordingly.

David prefers his electric motor, that is actually, his companion, **Midnight.** While Ashton prefers his internal combustion engine motorbike, akin to XSR900.
Actually, modified XSR900, manufactured by the multifab at their home, instead of bought.
And the fuel it uses, is actually captured carbon dioxide from the atmosphere and mechanochemically rearrange them with hydrogen into synthetic hydrocarbon fuel.
In Ashton's words, his motorcycle is carbon-neutral.
It neither adds nor reduce carbon dioxide level of the atmosphere.

**Guntur's Synthetic Fuel Combuster (XFC)** base model was the model Migun designed during his lifetime.
256 XFC158 "Hydra" were produced in August 2015 for core members of the **ORCA Gang,** along with **Carbon Capture Synfuel (CCXF) Generator,** now belong to the **Orca Group Renewable Energy (OGRE) Corporation.** OGRE Corporation is now a contender to Kukercorp's sister company, **Karbonyte Inc.** (Edit 20200614: Orca Green Energy replaced with Orca Group Renewable Energy)

ASC Line, where ASC stood for Ashton's Synfuel Combuster, is actually a modified XFC158, with the addition of several improvements in fuel usage efficiency, composite nanotube frame and suspension system.
Also the difference between Ashton's ride (dubbed **ASC900 "Sequent"**) and David's Midnight, is that Sequent is entirely manual, while Midnight is practically self-driving.
(Edit 20200612: There's two different term, ASC and AXC.
Stick with ASC)

Ashton convinced David to take a manual motorcycle riding training with him.
It was meant to recreate my experience on practicing motorcycle riding.
