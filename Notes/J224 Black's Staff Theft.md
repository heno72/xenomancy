---
## Consider not having fullcaps for headings.
title: Inspiration
## We don't need a subtitle for now
subtitle: Black's Staff Theft
## Information about authorship(s)
author: Hendrik Lie
date: 24 February 2019
## Fonts and formatting.
fontfamily: times
fontsize: 12pt
linestretch: 1.25
indent: true
papersize: a4
## Numbersections
numbersections: true
secnumdepth: 2
---

Hendrik's attempt to uncover plots of Aditya's use of the Black's Staff, might start with a contact information of Edward Kevlar's workstation.
It might connect to someone, a transgender female, her birth name is Ernest Winston, but her current name is Ernie Windiana.

So first Hendrik will be unravelling the nature of Edward's relationship with Ernie, by meeting them separately.
On this Hendrik pretends to be a businessman and approached Edward, supposedly to make a business deal.
Edward's belongings and attributes revealed connection to a new movement, known only as LUC.
However every mention to this insignia resulted in Edward's denial and insistence to change the topic.
Hendrik knows that he shouldn't be pushing.
What's worrying Hendrik the most is Edward's odd gesture, of always having skinship whenever there's a chance to.

Then Hendrik is assigned to attend a wedding party on behalf of Callan Libston, supposedly his relative, while in reality it is just one of Anderson's persona.
There she comes, Ernie as a performer, a living joke of the party.
It turned out, Anderson is now the mc of this show, as his other persona Mr.
Victor.
He's assigned to meet someone else related to Ernie, her contact on the event organizer.
To attract Ernie's attention, despite his disgust of her demeaning performance, Hendrik maintained eye contact with her.
That in one segment of it she approached him, and teased him accordingly.
He's repulsive, but over the course of her persuasion, Hendrik comes to the conclusion that life must be hard on her.

After her segment is over, Hendrik waited outside pretend to answer some phone calls.
She was stunned to find him, as she was preparing to leave.
A glimpse of guilt occured in her eyes before she returned a smile and broke eye contact to him.
Acting on that cue, Hendrik said that it was okay.
She was stunned again, and he said again that it was okay, he understands that life must be hard for her.
Hendrik approached her, and said that despite the hardship she's having, she's doing a good job to keep herself together.
Her eyes went watery, and Hendrik forced a smile, and gave her his handkerchief.
Hendrik decided to let her have the handkerchief.
He asked her for a walk to her car.
She said that she's about to order her own means of transport.
On that Hendrik insisted to drive her home.

In the car, Hendrik thought hard on how to get her to talk, and thought that LGBT related conversation might get her going.
Hendrik decided to act as a gay man.
He said that he was confused there on the stage, because admittedly, Hendrik is interested in man, and she's neither a man or a woman.
She laughed a bit, and she said that it was the most unexpected answer she ever had.
Hendrik said that it was hard to find his other half, and he imagined it must be harder for her to find true love.
She was astounded, she said that all he needs to do is to go to a gay bar.
Hendrik is now astounded, he never heard of that.
He tried to keep it on track, and so he mentioned that he knew a man that might like her, stating that she's exactly his type, and finally she's asking who was that.
He said Edward, a friend of him.
She said that she knew him, and they were in a relationship, and whispered that it was an affair.

Hendrik took certain moment to process it, his sense of Justice kicked in, and slipped, saying aren't you two cheating on her that way? She was insulted at first, and realizing his mistake he rephrased it, I mean, I have this dilemma too, we have to pretend like we're normal, aren't we? She smiled, and said that despite her own moral quail on that matter, she realized that what is important is that we get to express ourselves.
She and Edward are together because of their own choice, to express themselves, something he can't do with his wife and children.

She thought of herself as if she's the aides to help him express himself.
She implored Hendrik to do the same, to accept who he is, a gay.
Hendrik regretted his decision to pretend to be gay, but said nothing.
She insisted that they're to stop in a gay bar she knew, at that moment.
Hendrik reluctantly denied that, but she said there's nothing hurt in trying, and if he didn't like it there, he could just go away and never go there again.
However she said that the important thing is for him to give it a try.

The name of the gay bar intrigued him, as it was LUC Bar.
Most of those within are biologically men, but they fit into various gender roles.
Any spectrum between a man and a transgender was there.
That until he realized a smaller percentage of transgender men and lesbians.
He got a call from Anderson, saying that her contact was a Heavenly Host, he's currently in pursuit.
Now as the mystery is unravelling, that there is a Heavenly Host involved in this case, he wish to know more.
Hendrik asked her what does LUC stand for? She said it was the Laskar Umbra Cahaya (Legion of Light's Shadow), a movement aiming to mankind liberation from themselves.
He asked again whether Edward knows about it, and she said that he was an activist as well.

There, he spotted Anthony Matthias, acting as a bartender.
He said to her that that man seemed attractive, and she said with a fair amount of laughs, good luck with that, as he's a straight male, only one of few straights that work here for living.
Hendrik joked that perhaps my sense is attuned solely for straight men, or just for men like him.
He excused himself and approached Anthony.

He ordered himself a drink.
Anthony smiled at him, he doesn't know what else to respond, so he smiled back.
Anthony poured a drink and waited there as he drank it up.
Hendrik began asking him why would a boy like him decided to work in a gay bar.
Anthony smiled and said very good, to Hendrik's surprise.
Anthony asked why would he pretend to forget? And said that his face looked better without his tattoo.

Hendrik squished his barely visible eyebrows, and asked what? Anthony smiled again, saying that he's a good actor.
He touched Hendrik's face on the left side with his right palm, saying that there's no scar left.
He said that he's jealous of volants with the ability to change their body at will, but then his expression changed as their skins touched.
In a surprise, Anthony said with confusion, that it is a real flesh and blood, and Hendrik, angered, asking to Anthony, you know Manov? Anthony was confused for some moment, he stoned there, his palm is still in Hendrik's cheek.
Hendrik swiped his palm away from his face, slammed the bar, looked to the side and sighed.
He took a deep breath and gazed up.
He returned to the still stoned Anthony, I said, do you know Manov? Anthony replied with just, "you're not him?"

We need to talk, outside, said Hendrik.
Anthony stepped back, "can't, I'm working" he said.
Well, then we will be talking here, said Hendrik.
He conjured sparkling strands of fire and flames from an insignia he drew on air, and they're all chasing Anthony.
Anthony called Aurelia, and a cloud surrounded the entire bar, that melt before their eyes, and they're lifted up to the rooftop.
Anthony bent the strings that come for him around him and dissipated them in a circular move.
The clouds coalesced into a dolphin form after it closed the hole they made when they're lifted to the rooftop, returning the bar as if there was nothing happened.
It flew toward a water tank on the rooftop.
Water inside was poured up, and coalesced into dolphins, and they swam toward Hendrik in unison.
Hendrik deflected the incoming stream of dolphins, and draw more strings of flames from air around him, that then coalesced into a fire ball, accelerating toward Anthony.
Anthony made another circular movements and the fire bent around him and dissipated.

Hendrik closed the gap between them and land a punch, which was quickly countered and they're now exchanging punches and blocks.
A spark the shape of a knife formed on Hendrik's palm and he aimed for his neck, when a water stream blocked his palm off its way to Anthony's neck.
Anthony managed to punch Hendrik right on his jaw.
Hendrik fell, and strings of fire coalesced again around them, but then Manov appeared between them, closing his gap toward Hendrik, pulled his collar, and they're lifted up in a rainbow bridge through the air and into another rooftop of a highrise building.

Hendrik inquired Manov about Anthony, and he said that Anthony was a common acquittance of him and Purpose Coralline, the Higher Porpoise, his best friend.
Mainly because Aurelia was a pod member of Purpose before she died in an accident involving a human boat, and that being a necromancer, Anthony made a pact with Aurelia, and made her his familiar in exchange of her safe passage off hell.
Hendrik said that Anthony is an associate of Aditya, their enemy, and Manov said it was another reason for him not to tell Hendrik about Anthony.

Frustrated, Hendrik Inquired, on whether or not Manov is involved with the LUC, on which Manov stated it clearly that he isn't involved in any way with LUC.
Hendrik changed the question, he inquired if Manov know something about Aditya's plan on stealing the Black's Staff.
On that Manov said that he did not know.

Anthony managed to leap to their place, and Hendrik went into ready stance.
Manov stepped in between them, asking them to stop.
Hendrik yelled that they need to know what is Aditya doing with the Black's Staff, and he'd going to torture Anthony to get the answer.
Anthony stepped back, surprised.
Manov asked why, and Anthony said if that's all he wish to know, he could've just ask him, than to start a fight.
Hendrik now stepped back, surprised.
He said that he thought it was a secret of the movement.
Anthony laughed, saying that it was not a secret.
It would be if by knowing that, the opposing party could affect the result.
It wouldn't be, if even by knowing that, the opposing party couldn't affect the result.
Hendrik does not understand, and Anthony said that they're going to use Black's Staff to call upon the source of the serpents, The Goddess Tiamat.

Then, that's why there is a Heavenly Host involved, as the Powers on Earth were involved in this scheme: at least one of the Powers wishes to resurrect the Goddess Tiamat.
Hendrik asked again, which Power wish to resurrect her, and Anthony said that for that he does not know, they were given an instruction, a specific instruction: Envoy The Serpents, and that's all they know.
And then Hendrik asked why would they want to do it anyway? And Anthony said that it will unleash chaos to the world, and by doing that, resetting the civilization, therefore advancing EPL's cause: start over a new chapter in the book of humanity, he said simply.

Hendrik was going to unleash his anger to Anthony, when suddenly he fell to a hole just recently formed in the floor: the building is collapsing on Hendrik's side.
Manov managed to held Hendrik midair, and Anthony stepped back and conjured a rainbow gate.
Below them, was Anderson with his fighting suit, and a Heavenly Host, complete with its blazing white wings and uniforms.
Anderson noticed them, quite surprised by it, and nodded a sign that he'd need reinforcement.
Hendrik summoned Heinrich, and the triads engaged in the fight.
