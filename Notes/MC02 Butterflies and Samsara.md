# Red Thread - The Butterflies and the Samsara Mobile Task Force

<!-- The TOC bit is for gitlab webviewer -->

[TOC]

## Intro

Through the course of the story, we want to present an idea, of how we walk
along the course of fate, doing what we think is right. But in the end, which
side is the right side?

From the Butterfly Initiative, their goals are to unite all Apex Souls so they
can commune and wake up together from the collective dreams of all mankind.
To be whole again.
The name implies Zhuangzi's Dream argument.
It also hints on how Integra really is a prison of a God, whose soul is
scattered among the inhabitants.
For this purpose we should explore the importance, and present proofs, of how
extraordinary souls like Apex Souls emerge and gain their powers.

From the Samsara Task Force side, we should explore the importance of preventing
the union of all Apex Souls, as their union would result in the end of existence
of everything that they know of.
They don't want union to be some sort of broken god, they want normalcy.
It should also be explored on why they continue the ritual of capturing and
releasing Sperry-Vishnu on every cycle, and how they use those two souls to
maintain normalcy in the world.

From the Sperry-Vishnu side, we should explore their twisted fate to always find
each other in every cycle, and then the chase begins where they are chased until
all options are exhausted but of mutual suicide, where they hope for a better
cycle where they don't have to face all of this.
We wanted to show how simple their desire is, to be reunited with their love,
and rightfully earn their freedom together.

## Rough timeline

Events in the Red Thread of Fate more or less unravel like this:

1. Meeting of Sperry and Vishnu in modern day, alarm of GaFE is triggered.
2. Sperry and Vishnu had a dream about their past life, they came to learn about
   the on-going ritual involving their incarnation cycle
3. Introduction to the Samsara Task Force, that handles affairs related to Apex
   Souls. Technically it is a flashback from two to three decades ago.
4. Initial contact between Sperry-Vishnu and the Butterfly Initiative's Apex
   Souls: Shiva (Juan) and Fermion (Zarah), interrupted with the Samsara Task
   Force.
5. The Butterfly Initiative's advance to resume contact with Sperry-Vishnu, and
   the Samsara Task Force is catching up.
6. The Samsara Task Force interferes with the Butterfly Initiative again,
   Sperry-Vishnu managed to escape.
7. The Samsara Task Force wins, they continued to chase after Sperry-Vishnu,
   waiting for their death.
8. The Samsara Task Force arrives at the site of Sperry-Vishnu to retrieve their
   bodies, however the Butterfly Initiative managed to catch up, and surrounding
   the Samsara Task Force.
9. The fight of the Samsara Task Force to hold on Butterfly Initiative Apex
   Souls from reaching the bodies of Sperry-Vishnu, while the extraction is on
   the way.
10. The extraction is completed, but they still have to get past the Butterfly
    Initiative, so a sacrifice must be made from the resident Brahma.
11. The surviving members of the Samsara Task Force, present day, the leader
    woke up from the dream about their previous cycle, while the second in
    command informed that they might need to start the extraction again, now
    with new members.
12. Present day Brahma soul woke up from the same dream as the leader of the
    Samsara Task Force, and document the dreams in a writing, while also sending
    a message to a friend on the other side of the globe, which turns out to be
    a Shiva, and the Shiva remembers everything.

## Further readings

More about Zhuangzi: https://en.wikipedia.org/wiki/Zhuangzi_(book)
Dream argument: https://en.wikipedia.org/wiki/Dream_argument

Zhuangzi's butterfly dream:

> 昔者莊周夢為胡蝶，栩栩然胡蝶也，自喻適志與。不知周也。
> 
> Once, Zhuang Zhou dreamed he was a butterfly, a butterfly flitting and
fluttering about, happy with himself and doing as he pleased.
He didn't know that he was Zhuang Zhou.
> 
> 俄然覺，則蘧蘧然周也。不知周之夢為胡蝶與，胡蝶之夢為周與。周與胡蝶，則必有分矣。此之謂物化。
> 
> Suddenly he woke up and there he was, solid and unmistakable Zhuang Zhou.
But he didn't know if he was Zhuang Zhou who had dreamt he was a butterfly,
or a butterfly dreaming that he was Zhuang Zhou.
Between Zhuang Zhou and the butterfly there must be some distinction!
This is called the Transformation of Things.
> 
> — Zhuangzi, chapter 2 (Watson translation)

## Chapter Treatments

In this section I will present development notes of the chapters in this part.

### Part 1: Meeting of (modern day) Sperry and Vishnu

This part (or chapter?) should explore the encounter of Sperry and Vishnu
present day incarnation.
Their meeting should induce some sort of alarm by field agents of GaFE, hence
notifying them that the new cycle starts.

The idea is that the first meeting would be mundane, and normal.
It is an attempt to draw readers from their mundane life as their anchor before
we start to expand into the improbables.

I should put an emphasis first, by the use of prolog or some quotes in the
beginning, maybe even referencing to Zhuangzi's dream argument.

Here's how I wanted to present it:

It is a new day, and Ferdi is happy to enroll to the class, and make some
friends (I am sorry but they are not relevant later on anyway), and is about to
enter his first class.

The professor is not available that day, so the assistant professor was in on
the professor's place.
It was when Ferdi realized, something is different, all of his attention is on
the assistant professor.

The scene changes from the perspective of the assistant professor (Elbert),
he was a senior student, and the training for assistant professor was just done
last week.
He had prepared all the materials, introductions, and the syllabus.
He thought it would be just a normal experience, sans the stage-fright he might
had later on the class.
However he felt the air is different, and deep inside he is expecting something
extraordinary today.

In the class, he can already tell something is different.
As if his soul had a compass, he realized he really want to enter the class.
He realized he really want to look at a particular direction.
But he thought it was the start of the stage-fright.
He said to himself, don't embarrass yourself.
He pretended that everyone is a vegetable.
It was a bunch of fresh vegetables he would practice his oration on.
Until they aren't vegetables anymore.

At first he didn't pay attention, and the roll call begins.
until it was the turn for Ferdi.
Elbert couldn't deny the screaming of his heart, that Ferdi is the one.
He was taken aback.
Now everyone is no longer vegetables.
He had to face them all, but the fright was insignificant, as he had found his
significant other, well, at least his heart had.

