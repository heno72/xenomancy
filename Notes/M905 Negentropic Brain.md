---
title: Negentropic Brain
subtitle: Digital Domain Core
---

# Description

Digital Domain Core (DDC) is a spherical core with a diameter of 7 cm and mass of about 1.5 kg.
Its outer surface is a perforated beryllium bronze shell covered in Boron-Carbon-Nitrogen (BCN) based light emitters.
The interior is composed primarily of platinum-iridium endofullorenes programmable quantum dots arrays suspended in diamondoid matrices covering fine branching beryllium-bronze wires that converges at the center of the structure.

DDC is mainly used as the brain of Kuker Corporation's Kolega Robots (colloquially, Kolega Robots are often referred simply as "robots").
Due to the nature of its software that strictly obeys the Four Laws of Negentropy, DDC is often referred as a "Negentropic Brain," a practice often supported with Kuker Corporation's marketing plan of the Kolega Robots.
It is often claimed in marketing materials of Kolega Robots that due to its negentropic nature, any robot with Negentropic Brain installed on them would be strictly incapable of cause harm to human beings, or incapable to allow harm to fell on human beings.

# Operation

Each Digital Domain Core operates at around 30 watts of power.
It is capable of processing speed at 1.72E+18 bps.
Information can be encoded and retrieved inside its structure with a storage space of 2.74E+25 bits per DDC.
Information transmission rate to and from the DDC is done via solid state UV-C optical wireless communication (OWC) interface with theoretical maximum transmission rate of 1.37E+22 bps.
In practice the transmission rate is about the same rate as its processing speed.

Power is supplied through six interfaces spread equidistantly on the surface of the sphere.
Six connectors made of beryllium bronze alloy rods from the robot body are inserted into the DDC power interface during operation.
Power transmission appears to be done wirelessly via resonant inductive coupling interfaces between the connectors and the interfaces.

The core is situated inside a sealed chasis that function as OWC interface with, supply power to, and provide cooling mechanism for the core.
Cooling is done with water-based coolants that entirely submerge the core.
Heat is directly removed by circulating the coolant to and from the chasis interior, then the heat-containing coolant is pumped to radiators housed on the body of the robot.

# Structure

The main computing structure is comprised of 67% quantum dots cellular automaton (QDCA) as its main logic circuits and 34% beryllium-bronze alloy meshes that function as inter-connective circuitry support structures.
The QDCA units are comprised of diamondoid structures infused with platinum-iridium enriched endofullorene quantum dots.
The beryllium-bronze alloy meshes are structured into an acyclic tree from the center to the periphery, with connections between branches facilitated with clusters of specialized QDCA bridge circuits of its neighboring branches.
The branches of the beryllium-bronze meshes that aren't connected to bridge-circuits end in a dense cluster general-purpose QDCA logic units that forms the logic circuits.
The beryllium bronze branches are not the actual wiring, as the wiring is apparently facilitated with internal lightways comprised of diamondoid fibers that transport data packets in form of modulated light pulses of various wavelengths.

The configuration allows information to be transferred efficiently from one logic circuits to the next by travelling away from or toward the root branch.
Alternately inter-branch connections are possible by travelling sideways across the bridge circuits.
It allows the system to to not get congested by downstream (rootward) or upstream (tipward) data transfers, as information packets can be routed through the bridge-circuits up to the target branch.

The main computing circuitry has a density of 3.41 g/cm³.
However, the tree structure has open gaps in between branches that allows circulation of water coolants to get rid of waste heat.
Therefore the computing circuitry actual density (30% empty volume included) is 2.39 g/cm³.
As its specific information is 7.2631978773084E−24 grams per bit, and the QDCA is basically 67% by volume with composite rho of 0.9g/cc * 67% = 0.6g/cc, we get that its information density is its composite rho times its specific information = 0.6g/cc * 7.2631978773084E−24 g/bit = 8.3E+22 bits/cc.
However, the computing core is further foamed with about 30% of empty space, so we also multiply the information density with 70% = 5.81E+22 bits/cc.

A standard Digital Domain Core is comprised of a shell and a computing core.
The core has a diameter of 6.7 cm.
The Shell is made of perforated beryllium bronze layer covered in 1 mm thick BCN-based light transceivers on the inner side and the outer side.
The total shell thickness is therefore 15 mm.

As it is perforated, its density would also be multiplied with 70%.
Given that it is comprised of 2 mm thick BCN emitter arrays with density of 2 g/cc, and 13mm thick beryllium bronze shell with density of 8.25 g/cc, and that it is situated from r=3.35cm to r=3.5cm, its aggregate density is actually 7.42 g/cc.
With its perforation, the aggregate density is multiplied with 70%, therefore resulting in a bulk density of 5.19 g/cc for the shell.

# Quantum Dot Cellular Automaton (QDCA) Logic Circuits

DDC's computing core is likely to be a molecular Quantum Dot Cellular Automaton (QDCA) based logic circuits.
QDCA based logic gates has the potential to achieve switching speed on the gigahertz range, while consuming a lot less energy when compared to the current MOSFETs (metal-oxide-semiconducter field-effect transistors) based logic gates.
The logic circuits appear to be constructed entirely in diamond-like crystal lattices grouped into cells.

The cells are each comprised of four quantum dots that can encode at least two states: 0 and 1 depends on the positioning of electrons and quantum dots.
The state of quantum dots on one cell can alter the state of neighboring cells in a manner similar to cellular automata, therefore allowing processing of information encoded on the grid of cells.
Then those cells can be arranged to form logic gates in a grid with size of about 9x9 cells.
There might be anywhere between one to three such groups for each gate (the mean is around two cells per gate).

The quantum dots appears to be made of endohedral fullorenes, a type of carbon molecule forming a spherical cage encasing additional atoms, ions, or clusters within.
The clusters inside each quantum dots appear to be a mixture of platinum and iridium compounds.
The quantum dots are suspended in a 10x10 grid along with normal fullorenes, with quantum dots located around one fullorene away from the edge of each cell.
The cell is also sandwidched by one layer of 10x10 fullorene grid on the upper surface and the lower surface, making each cell comprised of 10x10x3 fullorene block, four of the fullorenes are endohedral fullorenes acting as quantum dots.

# Tree Analogy

The computing core is constructed somewhat similar to a tree, with a root is located at the center of the structure.
The bark is made of beryllium-bronze alloy tubes with diamondoid optic fibers at the interior of the tube.
The bark may further divide into branches, and each branches may further divided into twigs or even more.
The branch division may continue in multiple higher level divisons.

The end of each tube and the base of each division have openings, from which diamondoid optic fibers sprout and connect with a cluster of QDCA logic circuit "leaves" or "fruits."
There are some specialized logic circuits that act as bridge-circuits, connecting each branch to its neighboring branches.
A bridge-circuit might connect to more than two independent branches, though rarely more than six branches at the same time.

Unlike terrestrial trees, the tree-like structure in the computing core actually produce "barks" from the central point (the root) to all directions instead of just a single "bark" upward.
In general there are about twenty seven barks from the root extending roughly equidistantly from one another.
The branches tend to spread outwards away from the center, rarely tracing backwards toward the core.

# Lightways

Information flow to and from logic circuits are carried through the diamondoid fiber optics to other logic circuits.
Information appears to be encoded as modulated light pulses.
Multiplexing appears to be done by varying signal frequencies, therefore allowing one optic fiber to carry more than a single signal, and often multiple signals may travel on either direction of the tube.

Conversion of in-circuit states into light signals and vice versa appears to be done by changing energy levels of quantum dots on the interface between the circuit and the lightway endpoints.
Quantum dots vary their optoelectronic properties as a function of both size and shape, therefore each one of them has limited range of photon wavelengths they can receive or transmit.
Therefore quantum dots of various size and shape are situated on the interface to allow transmission and reception of multiple wavelengths.

# Shell Interface

The term does not refer to command line interface commonly used in ordinary computers.
Shell interface is an interface conducted between the computing core and the DDC's shell.
The interface is conducted via direct lightways from the outer branches of the computing core to the inner surface of the shell.
Outgoing data packets (signals from the computing core to the robot body) are directly processed by the inner surface, that is then transmitted to the outer emitter surface for transmission to the body interface.
Incoming data packets (signals from the robot body for the computing core) is processed directly by the outer surface, that is then transmitted to the inner emitter surface for transmission to the lightways.

# Production and Construction

The means to produce intricate structures of the QDCA logic circuits, Lightway Routers and the Shell Interface inside a DDC is not invented by any human being.
It was first conceived by KukerBrain, the first widely acknowledged Artificial General Intelligence (AGI) constructed by the Kuker Group.
Though it is possible for human beings to understand and comprehend the general workflow to produce and construct a working DDC, it can not currently be practically produced by human means of production.

To effectively produce a DDC, requires at least several separate processes:

a. Chemically "grows" the beryllium-bronze microtubular skeleton structure.
   The process appears to take advantage of the controlled crystalization of beryllium-bronze vapor over a core as its starting attachment point.
   According to Kuin (a familiar name of KukerBrain), the process is analogous with the creation of designer snowlakes, all Kuin has to do is to control the formation conditions to produce desired structures.
   
   The processes and contraptions to perform it is practically public domain,
   however the step is not currently viably reproducible by human operators.
   This mechanism is still being studied by human researchers.

a. Chemically "seeds" the diamondoid structures for lightways and quantum dots on the interior and the exterior of the beryllium-bronze skeleton.
   This step includes enriching the structure with platinum-iridium clusters containing endohedral fullorenes.
   
   The process involves similar processes as in synthesis of carbon nanotubes, mainly done with chemical vapor deposition (CVD).
   After the growth of the beryllium-bronze skeleton is completed, the reaction chamber has its interior atmosphere vented out and replaced with process gas and carbon containing gases.
   Catalyst cores are inserted strategically to select sites of the skeleton, then the chamber is heated to around 700 °C.
   Diamondoid structures would then grow from the catalyst cores and directed through and around the tubes of the skeleton.
   
   A series of metal nanofillaments are situated close to the growing diamondoid structures.
   The nanofillaments appear to function as depositor of platinum-iridium containing endohedral fullorenes to the growing diamondoid lattices.
   Kuin is unable to reproduce human-understandable description of the depositing processes, though inevitably some commetators argue that Kuin refuses to share the secret with human beings.

a. Produce the perforated beryllium-bronze shell.
   This step is the only step that is conceivably reproducible by human beings with known material engineering processes.
   However the reason of why DDC requires beryllium-bronze alloys as the shell material is unknown.
   Kuin is unable to provide human-understandable description of the reasoning.
   
   Experiments show that replacing the shell material with other alloys or compounds would simply result in either nonfunctional emitter surfaces or failure in the formation of the BCN surface.

a. Grow the BCN-based transceiver cells on the interior and exterior surfaces of the beryllium-bronze shell.
   Kuin describes the synthesis process as analogous with the production of QDCA circuitry, as the BCN-based crystal lattices are also infused with metal nanoparticle deposits for its quantum dot based emitters and receivers.
   The process is effectively public domain, and reverse engineering of its synthesis is currently on-going.

a. Disassembling of the shell with laser cutting under hard vacuum to facilitate insertion of the computing core.
   The reason behind why the process requires disassembling of a fully completed shell instead of creating the shell components separately is unknown.
   When requested for comments, Kuin insisted that the reason is simply because a complete shell is necessary for proper formation of the BCN surface, and that the shell formation process is harmful for existing computing core structures.

a. Wedding the disassembled shell with the computing core under hard vacuum.
   Care must be taken so that proper lightways contact points align with the appropriate shell interior surface interfaces.
   The shell is assembled together via vacuum welding.

The end product is a bare, empty or *blankslate* DDC.
It has no other value but as an extremely fancy and expensive metal sphere, unless it is first "initialized."
The "initialization" process is also done in hard vacuum during a period that Kuin described as a "curing-process."

The finished *blankslate* DDC is brought to a temporary chasis that possess an interior surface completely covered with BCN-based light emitting diodes.
The "curing" process begins by first powering up the DDC unit, and data transfer is initiated by beaming pulsed light signals to the outer surface of the unit from the inner surface of the "curing" chasis.
It is believed that the process essentially "flashed" the newly produced DDC unit with the equivalent of a "bootloader," that would spontaneously produce basic consciousness.

The DDC unit is then subjected to a battery of diagnostic processes and training processes directly supervised by Kuin.
During this stage, production defects and errors are identified, and defective units are discarded and recycled for raw materials.
The process take place in about two weeks to three months (the curing period appears to be unit-specific, with each unit receiving specialized trainings depends on its initial state-makeups).

The "cured" DDC is what commonly known as a "Negentropic Brain" by public.

# Packaging

After a negentropic brain is successfully manufactured, the unit is then prepared for packaging.
The unit is first covered in three layers of water-soluble sealed sheets, then further covered with about a centimeter thick water-soluble protein-based gel.
The fully gel-covered unit is then submerged in a biodegradable oil solution, and then canned.

Distribution of canned units then proceeds to deliver them to robot factories, directly to end-users or service stations for robot units requiring negentropic brain replacement, or to storage.
Expiration date is added for the gel and container oils, usually around two to five years.
The unit itself is theoretically able to function after hundreds of years in inactivity, provided that it is not physically damaged.

# Installation Preparations

1. A special detergen solution is to be prepared in a suitably sized bowl of warm water.
1. Open the can and drain the suspension liquid without removing the gel-covered unit.
   The liquid is a nontoxic biodegradable solution.
1. Carefully extract the gel-covered unit and submerge it to the detergent solution.
1. Gently rub the gel and the wrapper off with the detergent solution.
   Drain and replenish the detergent solution as necessary.
1. The cleaned up unit is then dried with soft microfiber fabric.
   Make sure to not leave any fingerprint marks.
1. Allow the unit to settle on a dry and shaded area for at least two days in clean and soft fabric wrapping.
   Put some silica gel packages to absorb moisture as necessary.
1. After it is thoroughly cleaned and dried, identify the power ports.
1. Use the handheld installer clamp and position the three fitting ports to clamp securely on the identified power ports.
1. Open the head casing of the robot and carefully remove the bolts of the brain chasis cap.
1. After the cap is removed, pull the chasis lock until an audible click is heard.
   Then let the internal mechanism to drain the interior, and the power ports to unwind.
   The chasis should deflower and expose the old or damaged brain unit promptly.
1. Remove the old brain unit.
   You may use your hand or another spare handheld installer clamp to remove it.
1. Carry the handheld installer clamp with a new brain unit to the correct position as illustrated on the container or the container box instruction card.
1. After the installer clamp is positioned on the correct position, remove the safety lever from the clamp, and the new brain unit should slowly be fitted on the chasis.
1. Remove the clamp from the head of the robot after the brain unit and the chasis produce an audible click.
   The chasis should automatically contracts and close, while the power chords should rewind to the correct position.
1. After the parts stop moving (there is no more whirring sound), reinstall the chasis cap and bold it properly.
1. Power your robot.
   You should be able to hear the pump to activate and fill the chasis chamber with coolant liquids.
   Your robot should activate within ten minutes after being powered on.
1. Send the old brain unit back to the nearest Kuker Group affiliates or branch offices.

