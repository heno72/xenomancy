# Superconducting Loop Battery

## J629

![A torus diagram.]()

$$
b = \frac{A}{4\cdot\pi^2\cdot a}
$$
$$
a = \frac{A}{4\cdot\pi^2\cdot b}
$$

The design is a torus shell, so the inner volume is described as $V = \left(\pi\cdot a^2\right)\left(2\cdot\pi\cdot a\right)$, where minor radius is `a`, and major radius is `b`.
Its surface area is described as $A = \left(2\cdot\pi\cdot b\right)\left(2\cdot\pi\cdot a\right)$.

Pressure in a fluid might be considered to be a measure of energy per unit volume or energy density:[^pressure]

$$
P = \frac{F}{A} = \frac{F\cdot d}{A\cdot d} = \frac{W}{V}
$$

[^pressure]: source: http://hyperphysics.phy-astr.gsu.edu/hbase/press.html

The magnetic pressure `P` is given in SI Units (`P` in `Pa`, `B` in `T`, $\mu_o$ in `H/m`) by:

$$
P_B = \frac{B^2}{2\cdot\mu_o}
$$

In practical units:[^magpres]

$$
P_{B_[Bar]} = \left(\frac{B_[Bar]}{0.501}\right)^2
$$

[^magpres]: source: Wikipedia, Magnetic Pressure

Note that `1 bar = 100kPa`

Solving for $P_B$ in `bar`, we got:

$$
P_B = \left(\frac{B}{0.501}\right)^2 \cdot 10^5 Pa = B^2\cdot0.501^{-2}\cdot10^5 = B^2\cdot398405
$$

So, $B = \left(\frac{P_B}{398405}\right)^{0.5}$

## 20190702

Longitudinal stress ($\sigma_L$) and tangential stress ($\sigma_t$, circumferential stress).[^longtanpress]

![Schematics for longitudinal and tangential stresses.]()

[^longtanpress]: source: https://www.mathalino.com/reviewer/mechanics-and-strength-of-materials/thin-walled-pressure-vessels

The forces acting are the total pressures caused by the internal pressure `P` and the total tension in the wall `T`,

$$
\sigma_t = \frac{P\cdot D}{2\cdot t} = \frac{P \cdot 2\cdot r}{2\cdot t} = \frac{P\cdot r}{t}
$$

With `P` be the differential of internal and external pressure $\Sigma_P = P_i - P_o$, where $P_i$ is internal pressure, and $P_o$ is external pressure.
Meanwhile, longitudinal stress is the total force acting at the rear of the tank.
`F` must equal to the total longitudinal stress on the wall ($P_T= \sigma_L\cdot A_{wall}$).

![source: Mathalino.com, longitudinal stress schematic.]()

Area of the wall is close to $\pi\cdot D\cdot t$, as `t` will be thin, compared to `D`.

$$
\sigma_L = \frac{P\cdot D}{4\cdot t} \frac{P\cdot 2\cdot r}{4\cdot t} = \frac{P\cdot r}{2\cdot t}
$$

Also: $\sigma_t = 2\cdot\sigma_L$

I think it means we only need to calculate $\sigma_t$, as if the material can handle $\sigma_t$, it will definitely be able to handle $\sigma_L$.

Spherical tank behaves as $\sigma_t = \frac{P\cdot D}{4\cdot t} = \frac{P\cdot r}{2\cdot t}$

So as $\sigma_t = \frac{P\cdot r}{t}$, given only `P` is known, $P = \frac{\sigma_t\cdot t}{r}$

If, we want to know, at which point would the mechanism break, if $\sigma_t$ is limited to $10^{11} Pa$, thickness is `1m` and radius is `10m`, we get $P = 10^{10} Pa$.
So $10^{10} Pa$ is the limit, and therefore its energy density is $10^{10} Jm^{-3}$, or `158 T`.

If we scale it, $P = \frac{10^{11} Pa \times 0.2}{0.5} = 4\times 10^{10} Pa$, it is `317T`.
Then, $P = \frac{10^{11}Pa\times0.1m}{1m} = 10^{10}Pa, 158T$.
Apparently I forgot that what's matter is the ratio of the radius and its wall thickness.

Suppose a circle area, $A=\pi\cdot r^2$, we must to have a circle with a hole $\frac{1}{3}$ of its area, so:

$$
A_1 = 3\cdot A_3
$$
$$
\pi\cdot R^2 = 3\cdot \left(\pi\cdot r^2\right)
$$
$$
R^{\frac{2}{3}} = r^2
$$
$$
r = \left(R^{\frac{2}{3}}\right)^{0.5}
$$
$$
r = \frac{R\cdot1}{3^{0.5}} = \frac{R}{3^{0.5}}
$$

Suppose we want a storage with `R = 1m`, `r = 3^-0.5 m`, then `t` is defined as:

$$
R - r = 1-3^{-0.5} = \frac{3^{0.5}}{3^{0.5}} - \frac{1}{3^{0.5}} = \frac{3^{0.5}-1}{3^{0.5}}
$$
$$
t= 1-\frac{1}{3^{0.5}} m = 0.42 m
$$
$$
r= 3^{-0.5} m = 0.58 m
$$
$$
P = \frac{10^{11} Pa\cdot \left(1-\frac{1}{3^{0.5}}\right)}{3^{-0.5}} = 7.32\times 10^{10} Pa; ~430T; 7.32\times10^{10} Jm^{-3}
$$

So, perhaps our superconductors were rated for `~400T`, with 1/3 of its entire structural volume is to store energy.

![The figure showed us a design where each superconducting loop cell is a torus with exterior size is defined by its major and its minor radii being equal.]()

The inner side, which is the vacuum that is where the magnetic field will be pushing against, is defined to have minor radius of `c`, and major radius equal to `b`.
`c` is defined as $c = \frac{a}{3^{0.5}}$

So internal volume is $V_{in} = \left(\pi\cdot c^2\right)\left(2\cdot\pi\cdot b\right)$.
Total material volume is $\Sigma_{V_s} = V_{out} - V_{in}$.
While $V_{out} = \left(\pi\cdot a^2\right)\left(2\pi\cdot b\right)$.
However, `a=b`, so:

$$
\Sigma_{V_s} = \left(\pi a^2\right)\left(2\pi a\right) - \left(\pi c^2\right)\left(2\pi a\right)
$$

Then we get that:

$$
f\left(V_s\right) = 2\pi^2 a \left(a^2 - c^2\right)
$$
$$
f\left(V_s\right) =\frac{4}{3}\pi^2 a^3
$$

> Comment: This equation has been tested and provides equal results compared to the original equation, for a condition where the major radius and the minor radius are equal, and the inner hollow space occupy a third of the entire torus volume.

This constitutes the entire material used for this SCL battery cell.
The rest is vacuum.
So let us see how well do they perform.
From the previous equations, we can get its total support/backend volume, therefore it is only a matter of multiplying it with its density to get its mass, considering that the superconducting film has a negligible mass compared to the support.

First, let's consider its dimension.
The shape is a torus, so it has a major radius `b` and a minor radius `a`.
On the previous sections we discussed the case where `a=b`.
But they could be different.

Then its inner minor radius, let's call it `c`, is the radius of vacuum that will store the magnetic fields.
The next thing to consider is its back end maximum strength, in pascals.
Then its back end material density.

After knowing all of those, we first need to discover its external volume.
With that, we subtract the vacuum volume to get total material volume.
Multiplying that material volume with the material density we obtain the back end mass.

We can get the empty volume per external volume to determine its empty space ratio.
Thickness can be obtained by subtracting minor radius of the exterior and minor radius of the cavity.

Its maximum pressure before breaking is calculated with $P = \frac{\sigma t}{c}$ where $\sigma$ is stress, `t` is thickness, `c` is inner cavity radius.
It also doubles as energy density, as they're dimensionally equivalent.
Magnetic field strength can be obtained by $B = \frac{P}{398405}$, note that `398405` is $\frac{10^5 Pa}{0.501^2}$.

Averaged torus density can be obtained with $\rho_{avg_t} = \frac{m}{V_{ext}}$, where `m` is the back end mass, and $V_{ext}$ is the external volume.

Maximum energy capacity can be obtained by: $E_{max} = \rho_{energy}\times V_{int}$, where $\rho_{energy} = P$, and $V_{int}$ is the volume of the internal cavity.

Specific energy can be obtained with: $S_E = \frac{E_{max}}{m}$.

Disregarding all of other parameters but the ratio of internal cavity vs external torus cavity, I got that they'd provide similar $S_E$ and $\rho_E$, which will make its computations easier.
However, to date (2020-09-08), I have not formulate an equation that requires only the ratio to calculate both $S_E$ and $\rho_E$.

At the following ratios, a carbon-nanotube back end with $\rho=2000kgm^{-3}$ and $\sigma=10^{11}Pa$, will yields:

| Ratio | Avg $\rho_E$ | $S_E$ | B | c |
|---:|---:|---:|---:|---:|
| ~0.0 | ~100MJ/m^3 | 50kJ/kg^1 | 15,835 T | ~0.0a |
| 0.06 | 18.8GJ/m^3 | 10MJ/kg^1 | 868 T | 0.25a |
| 0.25 | **25.0GJ/m^3** | 16.7MJ/kg^1 | 501 T | **0.50a** |
| 0.56 | 18.8GJ/m^3 | 21.4MJ/kg^1 | 289 T | 0.75a |
| ~1.0 | ~100MJ/m^3 | **25MJ/kg^1** | 16 T | **~1.0a** |

Table: Ratio in this table is the ratio of its inner cavity volume per its external volume. `c` is in the fractions of `a`.

As seen, the thinner the wall is (the ratio of cavity volume and external volume approaches 1), the highest its $S_E$.
Meanwhile, averaged energy density (energy per external volume) peaked when the ratio is at `0.25`, or when the wall thickness `t` or the cavity minor radius `c` is half as thick as the external minor radius `a`.

## 20190802

![A torus from above, the minor and major radii are demonstrated.]()

Since they will be packed in cells for a more convenient and smaller cells, we have to stack them together.
I think we should treat them like a pack of long tubes, each torus stacked above another along the axis perpendicular to their major radii (fig.2), so each cell is as tall as its minor radius if `r` and `R` is equal and the exterior radius is `2R`.

![A schematic demonstrating minor radius `r` and major radius `R`]()

Let the major radius be `b`, and the minor radius be `a`, and that `a=b=x`.
Then, the approximate cylinder has `r=2x`, and `h=2x`.
`h=2x` as `h` is equal to the diameter of the minor radius, and `D=2r`.

Therefore, the volume of the cylinder is $V=\pi r^2 h$.
Continuing with the previous definition of `r` and `h`, we get $V=8\pi x^3$ per cell.
But they won't be stacked in a very long cylinder.
And the best way to store a bunch of circles in a plane is in a hexagonal grid.
Then, we can know that each torus occupy the space within, no more than the smallest width of the the hexagon (see fig.6), as each sides are the base of an equilateral triangle.
See fig.7 for a clearer representation.

![A hexagon filled with the outline of a torus from inside, with `a=b`.]()

![A hexagon is divided into six equilateral triangles. Its features such as the angles, and the ratios between its sides and an individual triangle height is highlighted as well.]()

If a hexagon is split into six equilateral triangles, the largest width of that hexagonal tile would be the sides of its component equilateral triangles times two.
The smallest width of that hexagonal tile must be calculated by knowing the height of each component equilateral triangle.

Suppose, we took a right angle triangle out of one equilateral triangle, with each of its side is 1 unit length.
That right angle triangle would have the base of 0.5 unit length, its hypotenuse is 1 unit length, and its height will be `h`.

We can just use the Pythagorean theorem where $a^2+b^2=c^2$, with `a` and `c` are known, so $b^2=c^2-a^2$, then we get $b = \left(\frac{3}{4}\right)^{0.5} = \frac{3^{0.5}}{2}$.
So `b` is the `h` we're looking for, and the smallest width of a hexagon is `2h`.

Then `2h=2r`, `h=r=2x`, and if the sides of a hexagon has a length of `s`, we get that $h=r=\left(\frac{3^{0.5}}{2}\right)s=2x$.
So,

$$
\left(\frac{3^{0.5}}{2}\right)s = 2x
$$
$$
s = 2\cdot 2\cdot x\cdot 3^{-0.5}
$$
$$
s = 4x3^{-0.5}
$$
$$
h = \left(\frac{3^{0.5}}{2}\right) s
$$

Therefore we get `Eq.A`:
$$
s = 2h3^{-0.5}
$$

![A right angle triangle `hbs` with the angle at the `bs` corner is $60^{\circ}$. `s` is the hypotenuse, `h` is the height, and `b` is the base.]()

> **Note 20190927**
> Given that the base is `b`, the side `s`, the height is `h`, `s` can be discovered by: $s=\left(h^2+b^2\right)^{0.5}$.
> Now, `b=0.5s`, but `s` is unknown.
> Turning the triangle around, we can discover `b` through trigonometry: $b = h \sin 30^{\circ}$

> Since $\sin 30^{\circ} = 0.5$, `s=h`, wait, what?

> Actually, if we consider `s` as a vector $60^{\circ}$ from the x axis, where `b` is perpendicular to, hence `h` is perpendicular to the y axis.
> Therefore, knowing y component of the vector is sufficient to find the sum of the vector by: $h=s \sin 60^{\circ}$, so $s=\frac{h}{\sin 60^{\circ}}$.

> Since $\sin 60^{\circ}=0.5\cdot3^{0.5}$, then $s = 2h3^{-0.5}$, *equivalent to the Eq.A.*
> Apparently my previous dirty solution somehow works just fine.

![A picture of a hexagonal prism, which encapsulated an entire torus. There is a caption: `s = a+b`]()

So for our torus, `a=b=x`, which, accordingly, has a height of `2x`, which is he diameter of the minor radius.
So the height is also the radius of the perimeter of the torus, say `R=2x`.
It happens to be the smallest radius of the hexagon, denoted as `h`, so `R=h=2x`.

The hexagon, has its smallest radius as `h`, and its largest radius is `s`.
Then by the Pythagorean law, its largest radius, which is also the sides of the hexagon, has this following relationship with its smallest radius: $h=\left(\frac{3^{0.5}}{2}\right)s$, therefore $s=2h3^{-0.5} = 4x3^{-0.5}$

It happens that the hexagon has the thickness that is equivalent to the diameter of the minor radius, and therefore equal to `h`.

So the area of the hexagon `A`, to its side `s`, has the following relationship:

$$
A = \left( \frac{3\cdot 3^{0.5}}{2} \right) s^2
$$

Volume is then `V=hA`.
Insert our previous definitions, we obtain:
