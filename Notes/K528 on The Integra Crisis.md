# Alteration 20200528 on The Integra Crisis

Because now The Integra Crisis is included as The List in the Book 2: The Oneironauts, some changes needs to be done.
They're now a part of [MTF Samsara](/Notes/MTF%20Alpha-47%20Samsara%20c.2027.md), and therefore must move along with the MTF.
Therefore, the previous plot where they are collecting IDs over their counterparts, might not be necessary.
They'd also probably get some help from the MTF to carry on some missions.
Therefore it evens out the engagement with Tiamat and Hadad.

I don't think it is necessary to have multiple Hadads and multiple Tiamats, if this is the case, as we already have the Butterfly Initiatives, an international syndicate that believe in the ascension of the souls, via the gathering of all 9 Apex souls.
Even though they're currently in the possession of 2 Apex souls, they'd most likely exploit them, while providing them an army for each to protect those souls while doing their actions.

Also, should it also include the versions of Helena, Daniel, and Fernando on Integra?

What about their use of Fernando's house and Sylvester's house? I mean, they have the MTF already.
Unless, of course, Fernando has some ties with the task force.
We already established (not yet canon though) that Sylvester is Col.
Chandra's to-be son-in-law.
