# Alteration 20200730: ORCA, Migun Style, and WTF Interference

## Migun Style Teaching
**Migun Style Teaching**, is a teaching that incorporates heavy use of mental tools to augment one's ability. One of it is the induced synesthesia, especially the incorporation of visualization and kinesthetic sense. The end result, for majority of them is the augmented perfect kinesthetic sense. That's the *Tier one: mastery over body*. Followed with *the Tier two: mastery over soul* where they'd try to interact and interface through their meta (in Migun's words, or SOUL in Hendrik's words). The next stage is *the Tier three: mastery over spirit*, where the practitioner would utilize soul transformation to achieve transcendental forms, into a limitless form, no longer constrained to the shape of their body. A true reflexius act.

### Tiers Data Panel
| Tier | Label | Aim | Methods |
| ---- | ----- | --- | ------- |
| Tier 1 | Mastery of body | Perfect Kinesthetic Sense | Advanced integration of audio-visual-tactile stimuly with kinesthetic senses |
| Tier 2 | Mastery of soul | Metasensory and Metakinesis (Metapresence) | Altered states of consciousness via meditation or lucid dreaming |
| Tier 3 | Mastery of spirit | Meta Manipulation / Metaforming | Transitioning of the projection of self to the environment |
| Tier 4\* | Soul Shearing | Meta replication | Creation of a semipermanent or permanent separate instance of self |

*\*) Note that Tier 4 is not taught in ORCA, and so far, it had only been demonstrated by Hendrik.
In all technicality, it can be argued that Tier 4 is not exactly Migun-style, as it was developed independently by Hendrik, with the help of Calvin Gauss (Migun's previous incarnation). Perhaps more accurately it could be said that Tier 4 is a Lie-Gauss Style.*

### Tier 1
*Tier one teaching* is taught in a sequence of stages. The first stage is a brief introduction to stroop effects, and how we're usually having competing thought processes that would hinder our ability to reshape our perception, because there must be a consensus. The example case would be trying to say the color of a set of letters, arranged to be a completely different color name.

As we try to say the color, another part of our thought processes wanted to say the color name told in the letters instead of the color of the letters. There are competing forces. So the first step would be to utilize ericksonian hypnosis and reduce the interferences of the competing thought processes.

The training starts with the participants hypnotized in a sensory deprivation chamber. The chamber is situated in one of the etoan settlements local to the city. Usually a private housing complex or (more often) an apartment, that all residents are etoans. The site somehow never attracts attention from non-etoans, mostly dismissed to be just another apartment, that belong to someone else's. There, Migun would instruct them to observe a (virtual) object as it moved around, and was told to imagine, that if it moves closer to them, a probability cloud would extend to them and they may perceive it as blue. The faster it moves toward you, the bluer it gets. If it moves farther away, it gets red. The same if it moves up or down, left or right, relative to your perception: the side that is paralel to the displacement vector would turn bluer, while the side that is antiparallel to the displacement vector (read: opposite to), would turn redder.

Once they got a hang of it, they'd enter a sparring class, where with their now altered perceptions, they're instructed to be either in defense or offense, alternately. The offending party were told to extend their *blue clouds* to intersect with the part of the defending body that wouldn't be able to move away from it. The defending party were told to not let their body be too deep in the *blue clouds* of the offending party. To the outsider, it looks like a normal sparring, though over time they moves more in a fluidic manner.

After they're able to pass the previous stage, they'd enter the next stage where they'd be deprived of sight, and must incorporate other senses into their kinesthetic sense. They're told to move closer to one another, ina more close-quartered sparring, where they must not touch each other, but must stay within just less than an inch from each other's limbs. The offending party's *blue clouds* must be balanced by the exact opposition's *red clouds*. It was almost like dancing with their eyes covered, to the outsiders. By the end of this stage, they can predict the movements of their enemies, and also their body movements, without seeing. A complete mastery of one's body: a perfect kinesthetic sense.

Most stayed in this level, as advancing to the next Tier, requires more intense focus. Enter the tier two.

### Tier 2
*Tier two* of his teachings, mastery over one's soul, started with a rigorous session of meditation. If meditation doesn't work, or if you couldn't do it for religious reasons, they'd be allowed to enter the alternative: lucid dreaming. This stage's goal is to be free from their bodily constraints, to the oneiric space, the stage where one's consciousness had a less constrained access to their meta. Eventually, the goal is to jump their consciousness from their real body to their meta, and vice versa.

The first stage is to perceive not only from the body, but also from the meta body. The second stage is to detach the meta body from the real body, and in essence: an astral projection. Eventually, in the third stage, they'd be able to manipulate remote objects via their meta, or even interact with someone else's meta directly.

The next tier not only requires an intense focus, but it requires their mental image of self to expand not only to themselves, but to a boundless form. Enter Tier three.

### Tier 3
*Tier three* of Migun Style, is in the domain of a true reflexious act. The transformation of their meta. Not only that they can move around with their meta where their body can't, they can also transform the form of their meta. For example, in the Tier two, they can walk to a tree with their meta, and climb the tree, then took an apple off, and deliver it to their real body. In the Tier three, they can transform their meta, to be one with the tree, and directly let the tree to drop all of its fruits. Their meta can be a cloud around them and manipulate objects directly, can be an additional limb that can hinder attacks from reaching their main body, or even to completely block something. In a sense, at this stage they're indistinguishable from a true reflexior.

## WTF Interference
As more and more of his followers reached Tier three, up to about a couple dozen of them, RFL started to register them as unregistered reflexiors. That a WTF task force was instructed to investigate. On which, Hendrik and Heinrich are the members of the task force.

Hendrik had always believe that areflexis could be considered non-reflexior persons. But the definition of a being that can bear personhood is that they must not be enslaved by the environment. A person is those that can command their environment, not enslaved by it. Given that ORCA's Tier 3 members fit the requirements, they have the right to be entitled with personhood.

Enter the Consortium Council of Persons of the Watchtower Foundation. It is comprised of the subspecies Homo sapiens proreflexi, the infraorder Cetacea, the clade Elephantimorpha, the family Ursidae, and the family Corvidae. One of their concern is regarding ethics, as Homo sapiens sapiens (common humans), the only surviving members of Homo other than Homo sapiens proreflexi (human reflexiors), is actually considered ferals, due to their inability to command their environment (via RFL). Hendrik argued, that since common humans have the capacity of free will as well, they can choose to practice Migun's style up to Tier 3, and therefore becomes a reflexior by choice. It does not violate the ethics as they're not forced to do so. After the voting was casted, with a very small margin but still above the legal limits, it is in favor that ORCA can continue their practice and generate new reflexiors. Under the condition that any reflexiors generated by ORCA will be registered as the citizens of the WTF. Therefore, all reflexiors originated from ORCA must also adhere to the rule sets provided by the WTF, along with the appropriate punishments for mischiefs according to the WTF laws.

## Membership of ORCA
The movement starts at around 2014. It started out with about a hundred member. Each month, about 1 new members added for every 5 members. So it is an increase of 20% per month. In a year, from the original ```P_o = 100 members```, via ```P_f = Po * exp( t * a )```, and that ```t = 12 months```, and ```a = 0.2```, we have ```P_f = 1100 members```. In year 2016, it is 12200 members.

I think from the second year onwards, as the movement is further organized, split, and regulated, it stabilizes, so the value of ```a = 0.02```. Population, about a third of them exited the movements to join EPL. So about 4067 members left, remaining ```P_o_2016 = 8133```. So by 2022, they have 55.5k members. By 2027, they have 114k members.

About 34% of thems reach Tier 2, and about 30% of Tier 2 becomes Tier 3. So Tier 3 is is about 10% of Tier 1 members. So on average, the composition of ORCA memberships would be: 66% Tier 1; 24% Tier 2; and 10% Tier 3.

| Year | Tier 1 | Tier 2 | Tier 3 | **Total** |
| ---: | -----: | -----: | -----: | ------: |
| 2014 | 66 | 24 | 10 | 100 |
| 2016 | 8k | 2.9k | 1.2k | 12.2k |
| 2022 | 36.6k | 13.3k | 5.6k | 55.5k |
| 2027 | 75.2k | 27.4k | 11.4k | 114k |

## ORCA Pods
ORCA members are grouped into pods of about 500 individuals. They're divided into about four divisions of ~125 members each, organized into 10 task forces, each task forces is comprised of about 12 individuals. Each task forces are divided into three teams of around 4/team. Each team is led by a Team Leader. Distribution of Tier 1 members, Tier 2 members, and Tier 3 members varies on each pods.

| Year | Pods | Members |
| ---: | ---: | ------: |
| 2014 | 1 | 100 |
| 2016 | 24 | 12.2k |
| 2022 | 111 | 55.5k |
| 2027 | 228 | 114k |

Each pods are independent enclaves, sharing the same generic doctrines of the ORCA teachings. The basic principles that they all held would be:
1. Welfare of associates as comrades.
1. Promote merits to outsiders.

Every year, representatives of each pods would gather in the ORCA Consortium of Pods, mainly to maintain cohesion between ORCA Pods. Games and events are usually planned and held among the members of the Consortium. Crowdfunding among peers, and charity to outsiders are often held as well.

| Year | Pod(s) | Initiative Name | Brand(s) | Description |
| ---: | ----------------: | --------------- | -------- | ----------- |
| 2016 | 14 | Guntur Xynfuel Combuster Industry (GXCI) Cooperative | XFC; CCXF-3G | GXCI Cooperative produces XFC-brand vehicles: XFC100 lines for motorcycles, XFC400 lines for family vehicles, XFC900 for industrial vehicles, and as recent as 2024, XFC2200 lines for airplanes, and XFC4700 lines for sealiners. It also features XFC7000 to XFC9000 lines for military vehicles. |
| 2019 | 4 | ORCA Group Renewable Energy (OGRE) Cooperative | Xynfuel / XF | It is comprised of engineering-pods. One of their latest products would be the Xynfuel XF173, a carbon neutral fuel source. Produced with their proprietarily owned 3rd Generation Carbon Capture Xynfuel (CCXF-3G) Generator. |
