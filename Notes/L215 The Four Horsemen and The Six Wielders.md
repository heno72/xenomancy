# Alteration L215 - The Four Horsemen and The Six Wielders

AID FROM THE FOUR HORSEMEN
So when the six protagonists wield Yagrush and Aymuri, and entered Oneiromundus, Adran informed Midnight and Ashton. So they beamed their consciousness to Adran's instance in Shamayim, and entered the Oneiromundus. That's when they discover the weird landscape with its five states:
1. SMP: The State of Metal Petals, led by Prick Petal Proctor.
2. HH: The Hypnocratic Hypernation, led by Lead Manu Deisono and Firm Felix Redstone.
3. FLS: The Free Land of the Sun, led by Tawan Viraloneiro.
4. PSUL: The Police State of the Umbra Legion, led by Kao Ninekao Waikabkwamrak.
5. FSP: The Free States of People, led by Cad Aleirer Cadviri.

They first land in the FLS, on which they didn't realize anything weird, except that there are more dogs, and those dogs are also citizens, instead of pets. It took them months to adapt in the state, and during that time they saw on TV: Fernando hosted programs. He is apparently an entertainer that is loved by everyone, people and dogs alike. Hence the quest to reach out Fernando. Here we explored various ways to reach out idols.

The second stage, they're using Tawan's connection for a tour to other countries. First stop is HH, where they're going to find Henokh and Daniel. Loosely based on The Gifted's Supot in term of power, they discover that the society has certain taboo they couldn't talk about or do even though they want it. And they're almost always want to go to sleep when it's time to do so. They discover the peculiarity that there's always a screen in every room, that would show Henokh, providing guidelines on day to day life. When it's night, Daniel would appear, answering phones from fans and citizens, listening to their concerns and giving advices, and people would listen to Daniel's songs to rest. Tawan had a strong feeling to Daniel, until he let out a word, "Hin," the one he's looking for all of this time. During their struggle to go to the Capital, Tawan reacquired his power from Integra. The target here is to obtain an agreement to develop engines and technologies to alter the world. Geoengineering, and nuclear reactors.

The third stage, is to go to SMP to negotiate mining deals required for the engines. This is when they could feel that the world is reacting. Bulls ravaging the civilized lands, and storms becomes more severe. However the ocean seemed to be inviting, as fishes flourishes for citizens to capture, with prolific growth of kelps and algae on the ocean. Ashton knew what is happening, the fight between Powers has started, as Tiamat returned to Yam after El was awakened. Ashton reasoned that the fight is affecting this world. They need to find a way out of this world, and that is by reuniting the six wielders of Yagrush and Aymuri. Alas, Prick thought that the calamity is caused by the engine that HH is building, and stepped into the affairs. Lead and Firm are not equipped to fight Prick, but Tawan is very capable, as both of their powers are in the level of environmental manipulation.

The fourth stage, they went to Mount Sinai, and discovered a pack of wolves along with humans that can transform into wolves. The FSP was led by Cad, that at first thought the incoming group was a part of PSUL. PSUL had been more aggressive lately as the nature started to behave.
