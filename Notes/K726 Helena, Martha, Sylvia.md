---
## Consider not having fullcaps for headings.
title: Inspiration
## We don't need a subtitle for now
subtitle: Helena, Martha, Sylvia
## Information about authorship(s)
author: Hendrik Lie
date: 26 July 2020
## Fonts and formatting.
fontfamily: times
fontsize: 12pt
linestretch: 1.25
indent: true
papersize: a4
## Numbersections
numbersections: true
secnumdepth: 2
---

# Introduction
Helena, Sylvia, and Martha might be friends.
Helena a neurosurgeon in K-Med Hospital, Sylvia the board of directors of the K-Med Hospital, and Martha a pharmacy clerks of Sejahtera Medica.
Sylvia had this hobby: writing.

1. Helena is a neurosurgeon, she took the same med school as Steven, Daniel and Fernando.
A resident in the K-Med Hospital.
2. Sylvia studied law, she's the board directors of the K-Med Hospital.
Her hobby is fiction writing.
3. Martha studied pharmacy, and is a pharmacy clerks of Sejahtera Medica.

Helena is born in 26 Maret 1986.
Sylvia is born in 26 November 1989.
Martha is born in 17 June 1987.

Quite a diverse age group.
Could they be neighbors? But then Helena would be neighbor with Daniel as well.
Not ideal.
I think it is most likely that they knew each other through a university organization.
Perhaps they're a part of the Student's Union organization.
Perhaps, Martha would be the treasurer of the Student's Union, during the reign of the President of Student's Union Helena and Vice President of Student's Union Sylvia.
Most likely.

Or, that Helena is the former President of the Student's Union, and Sylvia is her successor, or a third generation successor.
Or perhaps a staff.
Makes more sense.

# Semicanon
Here's what we would establish: In 2005, Helena is the President of the Student's Union.
At the same time, Martha is one of her staff.
Despite her pain that her husband threw their baby in the woods (she said to the rest of her friends that they lost him in a trip).
Sylvia, is still a high school student lol.

Sylvia enrolled to the university in 2007, majoring in law.
So she's very unlikely to be friends with Helena and Martha during university years.
I think it makes sense if Sylvia and Helena knew each other because she's Daniel's sister after all.
Because Helena is close with Daniel as well, she'd be considerably close with his sister, barring any major conflict.

Remember at around 2002, Martha had a son already.
Most likely before 2006, Martha'd split with her abusive husband.
Perhaps around the same time Helena is getting married.

# Accepted Background
So perhaps, considering that Martha and Seungmin broke up at the end of 2005, she'd find Daniel attractive, as a very kind guy, and such.
They're best friends as well because they both knew their inner secret: their secret loves are actually lovers to each other.
Helena loved Fernando, and Martha loved Daniel.
Buth Fernando is with Daniel.
So messed up.
Perhaps their sisterhood is called The Pelakor (Perebut Laki Orang, Someone Else's Husband Hunter) as a cruel joke to their conditions.

Sylvia happen to get along with them too, but never knew about their secret crushes.
They just get along.
It just happens that Sylvia get into their life and they get used with it.
And they're having a great time.
Perhaps it is also because, secretly, they're having the same hobby: storywriting.

So Helena and Martha had been friends because they love to write fan fictions.
Sylvia is also a writer, but she is more an original work type.
She didn't do fan fiction, nevertheless she loved to enjoy the company of other writers, fanfic or not.
That's why they occasionally meet and discuss about writing and such.
After all, good stories are there to share, fanfic or not.
Yep, that's it, that makes more sense.

# BATTLE OF KENDARI
So at a nice morning in The Battle of Kendari, right before the aliens invaded the city, they'd converse about the Bechdel–Wallace test, that they want to modify, so as not to violate itself when it is talked about in a story.
She called her variation of the test as Bechdel-Wallace-Lusien test.

1. At least 2 named women,
2. are conversing with each other,
3. about anything but their love life.

Helena critiqued that the purpose of the test is to break the bias that women exists solely for men.
So the third requirement were set so that they'd be depicted as having their life not only revolves about a man or something.
Sylvia said that it was important to also include all of her love life, because it is usuallya associated with men.
So by excluding their love life would break the view that women are just there for love interest.
Martha just say that this conversation by itself, should it be a part of a story, already break the rule.

I think that would be an opening.
Martha said, that since the rule is broken already, she wanted to talk about this young guy she meets.
A sweet guy, but very young.
However she felt some sort of familiarity with him, and she though she fell for him.
Helena and Sylvia teased her for having a "brondong" (a love interest, a boyfriend, or a husband that is much younger than the woman).
Martha smiled and blushed.

Sylvia changed her bully subject from Martha to Helena, saying that Helena's husband is literally

# INTEGRA CRISIS
Later on, when Henokh and co.
went to Integra, Martha and Sylvia meets, and Sylvia mentioned to Martha about a theory, that perhaps a story is actually a real story, but in another world.
That the author obtained the basic idea of the story from a real story of another world.
Martha is sceptical that such things could happen.
Martha said that to believe there's another world is a thing, but to assume that information from one world to pass to another is another thing.
She didn't believe in the former, stating that if such connection can occur, it would be well-known by today (also referenced the time traveller paradox).

Then the story progressed by showing that Hendrik (In) has the knowledge of Xe events.

