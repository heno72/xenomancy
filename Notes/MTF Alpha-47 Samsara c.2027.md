# MTF Alpha-47 "Samsara" in 2027

A task force of DoN, GaFE.
Specialized in dealing with the Apex Souls.

Led by Sergeant Pride in 2027.

It was a section comprised of 2 fireteams.
Barong Asu was 2nd fireteam's rifleman, but was transferred to the 1st team, as the team was comprised only of Sgt.
Pride and three visitors.

## Section Composition
### 1st Fireteam (The Iron Fists)
  1. Sgt. Pride Loudgulf, a Higher Purpoise.
  His human form is a medium built, hairless pale albino man, with bluish gray irises.
  1. Steven Pontirijaris.
  1. Fernando Suryantara.
  1. Daniel Lusien.
  1. Barong Asu.

### 2nd Fireteam (Band Airlangga)
  1. Barong Ket, usually called Ket.
  His human form is a lean but strong south asian male martial artist.
  His beast form is a male lion.
  Specializes in aural attack, projected energy, shielding techniques, and strategic planning.
  Can teleport, but only toward a spot in his direct line of sight.
  If he can't see it, he can't teleport there.
  1. Romanov Dexter.
  1. Barong Gajah.
  1. Henokh Lisander.
  1. Derictor Wijaya.

## Promoted:
1. Col. Chandra "Chan" Watthuprasongkh ("จัน" จันทรา วัตถุประสงค์) (m, 60).
He was a close friend with Michael (calling him Nong Mich), as they went into training at a similar time, and spend years together.
Highly determined to do his jobs, very friendly to his squad, and knows every single one of them very well.
Currently the head of the Department of Normalcy, Global Foundation of Esotericism.
1. LtCol Kang Haein (강해인) (m, 54), usually called Haein (해인).
A bright, cheerful, and playful person.
Barely shows his emotions, always mask it with smiles and positive vibes.
Very perceptive to other individuals, and highly sensitive to changes in mood, and emotional distress.
Currently the trusted person and the second in command of Col.
Chandra.

## Transferred to beyond DoN Field Operations:
1. Dr. Angelica Gears (f, 56), usually called Gears.
She is highly perceptive, a perfectionist, and like to tidy her belongings and appearances.
Very adaptive and resourceful.
Currently the head of the R&D of DoN, specialized in operational equipments development.
1. Dr. Emma Bright (f, 54), usually called Emma.
A hypersex and playful woman.
At the same time being very emotionally matured, high self control (except for sex), and very intuitive.
Currently the head of the Project Antarabhava, and maintained a transfer room to deliver souls from this world to their counterpart in manent realm.

## Deceased:
1. Pvt. Ninawati Berlina (f, 21), usually called Watt.
A strong woman, and a very precise grenade thrower.
[KIA in 1993: by Juan Muerte, as she shot him after he wounded Ket.]
1. Pvt. Michael "Mich" Carmichael (m, 24).
Is Integra Hendrik's previous incarnation, and is a Brahma soul.
An agile reality bender, that can perform small scale minor reality bending relatively quickly.
A hard and wide scale reality bending requires more time of focusing.
[KIA in 1993: When finally come face to face with Juan Muerte, a Shiva soul, they fight with their esoteric powers while their powers annihilate one another.]
1. Pvt. 王实地 (Wang Shidi) (m, 24), usually called Wang.
A highly reliable wingman when needed.
However, he's quite messy when it comes to his own belongings.
Does not hesitate to jump and save his colleagues, even when it may cost his life.
[KIA in 1993: When Zarah shot a disturbtor beam toward Sgt.
Chandra, he jumped in place of Sgt.
Chandra and disintegrated into dusts].
1. Pvt. Djunita (f, 24), usually called Djun.
A bright and cheerful lady, always trying to liven up her squad by throwing jokes (often bad or fail, but, well).
Never wish to hurt anyone, and always try to get close with everyone.
[KIA: by Juan Muerte, when she's stationed to guard one of the tunnel's opening where they extracted the body of A Ping and A Chen.]
1. Pvt. 吳脚踏 (Go Jiaota) (f, 23), usually called Ms.
Go.
A calm, quiet, and stern person.
Dislikes unorganized actions, and always executes everything with precision and determination.
Wouldn't twich for every soul killed.
Technically, she is also a medic.
[KIA: by Juan Muerte, when she's stationed to guard one of the tunnel's opening where they extracted the body of A Ping and A Chen.]
1. Barong Bangkal, usually called Bangkal.
His human form is a heavily built aryan male.
His beast form is a babirusa.
Specializes in strength, and brute force.
[Deceased in 2016, unrelated to the mission]
1. Barong Macan, usually called Macan.
His human form is a strong and fit javanese male.
His beastform is a Bengal Tiger.
Specializes in sensory acuity, clairvoyance, camouflage, and navigation.
Can teleport in rapid succession, and is very precise at doing so.
[Deceased in 2016, unrelated to the mission]
