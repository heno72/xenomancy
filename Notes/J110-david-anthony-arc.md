# 20190110
Created Friday 11 January 2019
[[Project Hub]()][[Alteration]()]

Alteration Texts
----------------
There is time where Steven stabbed Aditya, and then Aditya and his team retreated.
I forget to consider that Anthony is present now.
That means he'd do everything there is to ensure Aditya's survival.
So he called Manov to help.
Apparently, in Reflexius medic adequacy, WTF is far advanced than EPL.
And he'd consider it a favor



* [ ] **Scene 01**

  Aditya got his eye stabbed by Steven in anger, and Adran took Steven away just in time before he completely kill Aditya.
  Anthony is second in command, and was in a car chased by BAIK.
  Inside the chasing party was Manov.
  After being notified by other Unit H members about the condition of Aditya, Anthony send Aurelia to Aditya's place, and froze his body with Aurelia's reflexius, and dropped off the car, then conjure Vespa in front of him.
  The car split into two as the swarm concentrated in a narrow formation in front of him.
  Anthony grabbed Manov from the car, crying, asking him to help his *hyungnim*.
  Hendrik and Heinrich come out from the other car, and looked at Manov and Anthony.
  Manov looked at them, then to Anthony, he ordered a Luftunellis trip and they're gone.
  Hendrik said that finally Manov betrayed *them* for his boyfriend.
  Heinrich just held his forehead with his palm, and sighed



* [ ] **Scene 02**

  Protected inside a freezing field of Aurelia, was Aditya.
  Anthony and Manov beamed in that room, and the Unit H members run to him, asking what to do next.
  Anthony said to Manov, please help *hyungnim*, and Manov said to the members to go away.
  Confused, Anthony gave a nod and they left the room.
  Manov said that Aditya's condition is not good, as his visual cortex is badly damaged, and his left eye is destroyed entirely.
  He knew that EPL Infirmary wouldn't be able to restore his sight.
  Anthony said that is why he asked Manov to do it, a WTF Institute's medic graduate.
  Anthony asked what treatments would he need, and Manov said that he's going to have Manov's eye as a replacement, and it wasn't going to be cheap.
  Anthony said that he'd consider it a favor, and a handshake ensued between them.
  Manov said that he did this because he is Anthony's friend.
  Manov mumbled that Heinrich and Hendrik would be extremely angry to find out that he withdraw a significant fraction of their shared balance, and raised his arm to conjure a sigil.
  He treated Aditya and said that he'd need about some weeks of rest and adaptation



* [ ] **Scene 03**

  Anthony and Aditya decided to carry on treatments from Martha's house, as she is the closest and trusted member of Umbra Cahaya.
  Michelle wasn't home at the time, and Martha said she was on Steven's house.
  Martha asked what would Aditya need, and Anthony said a room, Martha led them to a guest room.
  After Aditya is well and rested, Anthony cried, and Martha come to comfort him.
  Anthony said that *hyungnim* is the only family he got.
  Martha asked what is his relationship with Aditya, and he said that Aditya is his foster dad.
  He cried and cried, Martha just held him in her hug



* [ ] **Scene 04**

  Anthony was asleep in the couch, and Martha didn't want to wake him up.
  Martha put a blanket on Anthony, and she held his palm affectionately, and looked at his face.
  Martha's heart raced, and she decided to move her face closer to his, then she can almost feel his breath, and he winced a bit, and groaned, as if having a nightmare.
  Martha snapped off, and went to her room.
  She thought she must be crazy, and she remembered her abusive husband, and keep reminding herself that man can be very dangerous.
  She looked at her desk, where there's a photograph of Sejahtera Medica Team.
  Within are herself and Daniel.
  She started to question herself, who’s in her heart, Anthony or Daniel.
  She scrubbed her hair and decided to just go to sleep



* [ ] **Scene 05**

  Anthony was in a car.
  Then a man carried him out and was put into the woods.
  The man said that he should explore around, and he chased a butterfly.
  He chased it deep into the woods, and decided that he should return to the man.
  However the man wasn't there, and so do the car.
  He cried in the middle of the woods.
  He jerked up, and noticed a tear dropped on his face.
  His heart raced, and his body sweats.
  He discovered that he was covered in a warm blanket.
  He rose and Michelle just entered home.
  It was late in the night.
  Noticing his messed up expression, Michelle sat down next to him, and asked what is up



* [ ] **Scene 06**

  Anthony would be in his lowest point in life, and apparently Michelle was there to talk with him about everything.
  Anthony had his tears dropped again and told her that his foster father and brother had been badly wounded.
  He said he was lucky that one of his friend was able to at least restore his eyesight, and was so close to death.
  He couldn't imagine his life without Aditya.
  Michelle didn't know what to say and hugged him instead.
  She cried as well, when she remembered that her boyfriend could've died in the hand of an insurgent.
  She said she understand how it feels, to have someone you cared about faced near-death situation.
  Then she added that what matters is that they're now okay.
  Anthony thanked her, and he kissed her.
  She was surprised, but accepted it anyway



* [ ] **Scene 07**

  Anthony went to Aditya's room to sleep, at last.
  Michelle decided to go to her room.
  However David knocked from her window, in the second floor.
  David was on her terrace.
  Michelle asked how could he got there, and he showed Adran, with long legs supporting it at level high from the ground, currently camouflaged.
  Michelle didn't think much and reached for his chest.
  She examined his wound, which is filled with something like a white fabric.
  He said that it is the medic nano, he is okay he said, the nano would have repaired the wound by tomorrow, but he had to pretend as if it's still bad if they're going to school.
  He wont be schooling tomorrow, and she said she envied him
  
  David kissed her, and she took it.
  David bursted in tears, and Michelle asked what's wrong? David was in his lowest point in life, and his dad is traumatized for severely wounding Aditya.
  Michelle was there too for him to talk about everything.
  That night they just cuddled in her room, and as he felt better, he decided to go home, after giving her a good night kiss.
  Adran rose highher and out of somewhere in his chassis, a pair of black sheets that act as its wings were deployed, and it swiftly fly away silently
  
  Michelle stand in her terrace for some time, processing her feelings.
  She keeps reminding herself that she is David's boyfriend, but she got herself think hard on her kiss with Anthony.
  Anthony is in such pain that she didn't know what to do if she is to abandon him.
  It wasn't an easy option for her



* [ ] **Scene 08**

  That night, Anthony had another nightmare.
  He found himself in the car again, and this man, now his face is clearer, and definitely a korean.
  He found himself called that man *abeoji*.
  That man speaks in korean, and ask him if he'd like to play in the woods.
  He was happy.
  He noticed that he sat in the back seat of that car.
  That *Abeoji* smiled, and beside him was a woman.
  Anthony asked that woman, "*eomma, sup-eseo nol su issseubnikka?*" (can I play in the woods, mom?) That woman softly replied with *ye*.
  For some reasons that woman was about to cry.
  That man left the car, and carry Anthony with him.
  Anthony was about four or five years old.
  A strange voice could be heard from the woods.
  Anthony asked, "appa, sup-ui mwoga deul-eoss ni?" (Dad, what did you hear in the forest?).
  That man put him down to the woods, and then says "nado molla, adeul.
  gaseo gaseo hwag-inhae boneunge eottae?" (I do not know, son.
  Why do not you go check it out?)
  
  "Seung-Min, I think we need to reconsider...," she said from the car
  
  Her voice was weak and trembling
  
  The man dropped Anthony, and said "ga, Ki-Hwan" (Go, Ki-Hwan)
  
  Anthony looked back at the car briefly, and found a butterfly.
  He feels the urge to chase the butterfly, but when he looked back, the car has gone.
  He cried again in the middle of the woods
  
  Anthony heard a faint voice in the background, "Tony?"
  Aditya tried to calm Anthony, who is currently crying in his sleep.
  A bandage over his eyes was quite a distraction, but he managed to reach Anthony after kicking one or two furnitures.
  Anthony opened his eyes and jumped right to him, shouting "uli appaui ileum-eun Seungmin-iya!"
  Aditya winced a bit, "what?"
  "seungmin-i geuui ileum-ida."
  Anthony's eyes opened wide, until he realized that Aditya can't see, yet, and that Aditya does not understand Korean
  
  "Yang Seungmin, is my father's name" he said, after collecting enough of himself.
  He wiped his tears
  
  "What?" Aditya asked again,
  Anthony was still confused, and he was afraid that he's still talking in korean, trying to repeat it, "My father's name is..."
  "No, I mean, how do you know?"
  "..., Remember that nightmare?"
  "Yes, I do."
  "It gets clearer, and I was about four to five years old in that dream.
  There are a woman and a man, apparently my parents.
  And the woman called that man Seung-min, and that man called me Ki-Hwan."
  Aditya winced again, and asked, so is it a memory? Anthony said he wasn't sure.
  Aditya said that when he first meet Anthony, he was lost in the woods, and can only speak Korean.
  And apparently he identify himself as Ki-Hwan before Aditya gave him the name Anthony
  
  There was a silence, then Aditya asked, who is the woman then.
  Anthony said he couldn't remember



* [ ] **Scene 09**

  Aditya can now integrate input from his new eye to his sensorium.
  He realized that he can see weird glow from smartphones, from access points, and he can see people's "aura", clearer at midnight.
  He realized that the sky brighten considerably, and that he can control aperture of his new eye.
  He concluded that he can now see to the UV and deep infrared, toward radio frequency of wavelength up to several centimeters



* [ ] **Scene 0A**

  David finally brought her to his house, and introduce her to his family.
  They were quite welcoming, and there she saw Steven, broken, and he just gazed around.
  Fernando and Daniel tend him occasionally, where he'd break and cry to them



* [ ] **Scene 0B**

  Daniel started to talk about how he was such an innocent person, and so clueless about the human life.
  He said Steven was completely honest that he can't bathe normally.
  Steven laughed a bit.
  Fernando said that he couldn't think clearly back then, he must admit that Steven got him aroused.
  Daniel had his expression changed, and Fernando gave him a friendly punch and laughed.
  Steven said that he must be very attractive back then, and Fernando nods.
  Daniel had this uneasy expression, and now Steven and Fernando teased him.
  Steven laughed.
  That fact alone made Daniel and Fernando happy



* [ ] **Scene 0C**

  Helena looked from the kitchen, when Michelle is helping her to cleanse the dishes.
  Michelle noticed that Helena couldn't leave her gaze away from three of them.
  Michelle tried to draw her attention, and as Helena snapped, she said that she was just happy.
  Steven finally laughed.
  She said that she was happy he had such great friends.
  Michelle asked how long had they been friends, and she said that they had been friends since high school.
  When she finally get to know them at the med school, they're already been a single package.
  Helena mentioned how happy he is now, she sometimes envy their bond



* [ ] **Scene 0D**

  That gave her an idea, David wasn't particularly close with his brothers, they tend to fight one another.
  The closest he had is with Peter.
  Perhaps, Michelle thought, David is going to need a best friend that could understand his pain.
  Michelle planned to make him meet Anthony



* [ ] **Scene 0E**

  Fernando started to talk about his guilt.
  He said that guilt is something that is hard to let go, he knew it he said.
  That time when he confessed his love to Steven, is one of his greatest guilt he said.
  Daniel mentioned I was nearly killed that time, just when I try to help you two solve the issue, without knowing the issue.
  Fernando said, that time when Daniel almost got killed, is the day he had to face that guilt again.
  Fernando said imagine, you're about to be raped by a manhunter.
  He knew that if that man raped him, he won't forgive Fernando, ever.
  However he had doubt to help that day, because he isn't even sure if Steven is going to forgive him if he did save him



* [ ] **Scene 0F**

  Fernando said it was like a chance, for me to choose, a chance for me to redeem my guilt, he said.
  Steven finally mentioned, a chance to redeem my guilt? Fernando said that, at the time he choose to solve this problem and save Steven no matter what, he didn't want to ruin Steven's life.
  He wants to make it all right.
  Then he showed up, and punch that rapist in his face, after Steven pushed that man off into the bar.
  Daniel said that the will to make it right is what is important here.
  Guilt is okay, but if we didn't do something to make it right, it is at our fault, he said.
  Fernando said that Steven might feel guilty, but that is okay, what is important is that when there is a chance to make it right, are you willing to make it right?


* [ ] **Scene 10**

  Anthony is happy that Aditya can finally recover.
  And he was so happy, he want to share the moment with Michelle.
  So he asked Michelle out.
  Michelle said it was a perfect timing, she was about to ask him out as well.
  Anthony, flooded with joy, told Aditya, and Aditya just nods.
  He prepared and went out with her to a mall.
  He even conjured his gyrocar.
  It was an open cap, and she appears to enjoy it.
  He asked her what is their agenda, because he had no idea how human hanging out session is like.
  She said they'd watch a movie, play some arcade games and such.
  Probably going to eat something fancy



* [ ] **Scene 11**

  There they are in KukerMall, the only mall open at the time.
  It was regaining visitors just about a week after the insurgency.
  She said that they'll be waiting for her friend.
  Anthony said that they're going out with someone else? Michelle said it was such a good idea, this someone endured similar pain and struggle as he is, and she wished them to be the bestest friends.
  So that whenever he's feeling down like days ago, he could has a support system for him.
  It was quite awkward for Anthony, but he tried to say yes



* [ ] **Scene 12**

  It was far more awkward when David showed up at the mall.
  David had his gaze piercing Anthony's gaze, that responded similarly, and Michelle asked if anything went wrong.
  David said that this man is a friend of someone that forced his dad to kill him, and that his dad is traumatized for wounding that man.
  Anthony said that this man is the son of someone that severely damaged Aditya Hyungnim's optical nerve.
  Michelle was in shock, so Aditya and Steven are related.
  However she decided to stick to the plan.
  She requested them to put the differences aside and just have fun today.
  Hyungnim's problem isn't all Anthony's problem, and Steven's problem is not David's problem.
  They fight, parents do fight with other parents sometimes, but let their issues not be their sons problems.
  They're at fault, not the children



* [ ] **Scene 13**

  So there they are, watching a movie together.
  Michelle sit in the middle, and on either side of her are Anthony and David.
  Michelle held a large container of popcorns on her lap.
  Anthony's hand touched her hand, and he grabbed her palm.
  However when he gazed at her, he discovered that David had her hand in his grasp as well, right at the moment David realized the same.
  They gazed for a while, and Michelle requested them to just enjoy the movie.
  They reached for the popcorn yet each discovered that there is another palm inside, and each grasped it, both thinking that it was Michelle's.
  The palms squeezed harder, until both noticed how odd the position of that palm was.
  Then Michelle chuckled.
  When they both looked at the palm, Anthony and David are holding each other's hand.
  Each cursed.
  They rose from the seat and glared at each other.
  Anthony said that she doesn't deserve the son of a killer, and David repeated that, saying that Aditya is a psychopath, and that Anthony being his son means Anthony is as well.
  Anthony said he was adopted, and David said it means Anthony agreed that Aditya is a psychopath.
  Michelle screamed and asked them to stop



* [ ] **Scene 14**

  Security personnels approached them to escort them out, and Anthony called Vespa, the spirit of an intelligent bee colony to attack David, and David had his armor ready.
  Anthony called upon the spirit of Luciola and had her ability to camouflage him and approached David from his behind, but David's sensors identified him and they fought.
  Anthony called Aurelia and had the seats to collapse and they're now in a store below the cinema, fighting.
  Aurelia tried to damage him with Sonic weaponry, and David's armor shot a laser beam to disperse the cloud that constitute Aurelia.
  They held each other and rolled in the parking lot, and then they punched and draw some car shrapnel to wound each other, then Ashton had them electrocuted, and beside him was Michelle, crying



* [ ] **Scene 15**

  Anthony, after recovering from the stun, asked her which of them does she choose, David or Anthony.
  They can't be like this, he said, David reminded her that they're in a relationship.
  Then David punched Anthony saying that Michelle doesn't deserve him, and Anthony returned the punch, saying that Michelle belong to him.
  Michelle said enough, if choosing one or both just cause them to fight, then it is better for her to be with neither of them, she left.
  Ashton was quite astounded for their ego.
  He asked, do you two think of her like a trophy? They both said in unison a solid no.
  But then Ashton said that their behavior didn't imply that



* [ ] **Scene 16**

  Michelle arrived home alone, and she went straight to her room, crying.
  David and Anthony watch over Manov, Purpose, and Xiangyu doing clean up at the mall.
  Ashton asked Manov, wouldn't he call Anderson that Anthony is here? Manov looked at Anthony, and said "it wasn't related to the insurgency after all, it is two guys fighting over a lady, surely I won't bother Anderson with that issue." Anthony said thank you to Manov.
  David stated that Ashton might be right, that they thought each of them deserves Michelle, but they didn't take into account what Michelle might feel about it.
  Anthony nods.
  Anthony proposed that they should start over with Michelle in a fair game, then David interrupted that it must be her choice, whoever she wishes to be her boyfriend.
  Anthony agreed and handshakes followed in between them.
  Purpose asked Anthony if she could see Aurelia, her long dead bestfriend, however he said it wasn't a part of the deal with the dead to let the living to talk with them outside the contract, Purpose seemed sad, and Anthony said that perhaps he needs to give aid on restoring the mall into its former glory, and called Aurelia.
  Purpose excited in joy



* [ ] **Scene 17**

  Anthony and David arrived at Michelle's house.
  Aditya said thank you to both Martha and Michelle, and was about to leave.
  He looked at Anthony and said they're going now, then was rather surprised with the presence of David.
  Aditya looked away from David.
  David flenched his fist, then Anthony held David's arm to calm him down.
  Anthony said that he'd like to stay here for a little bit longer, but Aditya said he felt that they're better going.
  Anthony insisted, and Aditya asked whether he would need Aditya to stay as well.
  Anthony looked at David, and said that in his opinion Aditya should leave.
  Aditya nodded and left with Anthony's gyrocar.
  Michelle run upstairs



* [ ] **Scene 18**

  David and Anthony eat together with Martha.
  She asked whether or not they're in a fight.
  Anthony couldn't answer.
  She decided that it is best not to poke further.
  Instead asked how did David know Anthony, she was glad that they know one another.
  Anthony answered that they meet through Michelle, and David is Michelle's school mate.
  Martha said she knew that David is Michelle's school mate.
  She works at Steven's clinic after all, and she intentionally put her daughter in the same school as Steven's son, so that they could watch each other.
  She laughed, and Anthony took some time to process it.
  Anthony asked again, "did you work at Sejahtera Clinic?" She said yes.
  She went to the kitchen to cleanse the dishes
  
  Anthony whispered to David, "so, did I, um, let your father's stabber in the house of his employee?"
  David gazed at Anthony, "apparently yes," he said.
  He was about to burst into anger, when Anthony apologized.
  He said that his foster father was wounded and he had to hide him in a trusted member of the group.
  Anthony's eyes went teary, and David feels his anger fly away.
  "It wasn't your fault."
  David smiled instead, and he gazed away from Anthony.
  He couldn't imagine that the foster son of a psychopath like Aditya is this caring and sensitive man.
  Anthony asked why is he laughing, and David said nothing, yet smiled wider to Anthony, he started to giggle.
  Anthony asked if David is laughing because he almost cried, and David said "you figure."
  Anthony couldn't contain his embarrassment and gave David a friendly punch, "don't you dare to tell people."
  Martha come out of the kitchen and observed David and Anthony laughing together, "And apparently you two are close."
  She tried to smile



* [ ] **Scene 19**

  Jealousy aroused in her stomach, for reasons she couldn't comprehend.
  Anthony is close with someone else.
  She took a moment back at the kitchen to compose herself.
  It made her curious, and she want to know what is their relationship, and her imagination run wild.
  The first thing she asked to them is "are you two a couple?" before she even realized that she's actually asking it.
  An awkward atmosphere develop as time spend in silence increases.
  She forced a laugh, and Anthony and David followed, awkwardly.
  David answered, "No, we're not.
  We're just..." he looked at Anthony, "..., friends, apparently."
  There's another silence, "I mean, I'm Michelle's boyfriend," continued David, and three of them laughed, awkwardly.
  She excused herself into her room, and asked, given that this is very late already, that David would like to have a sleepover.
  David said yes, he needs to resolve this fight with Michelle anyway.
  Martha said he could sleep in Anthony's room.
  Martha slammed her room's door shut, her heart raced.
  She couldn't believe herself that she could make this situation very awkward.
  At least it is resolved that Anthony is not in a relationship with David, no matter how stupid the allegation was.
  She is in panic as not knowing what face to put in the next morning



* [ ] **Scene 1A**

  In Anthony's room, was a single large bed, and a couch.
  Anthony asked where would David want to sleep, the couch or the bed.
  David said, he doesn't think that they'd fit in the couch, so obviously they're sleeping in the bed.
  For a moment he realized that David is a half alien.
  Any normal human male would opt to not sleep together in a single bed if other option is available, Anthony thought.
  David asked if Aditya is to sleep there in the bed, would Anthony sleep in that bed? Anthony said yes, but Aditya was his foster father.
  David winced, Anthony realized that David is in genuine confusion.
  Anthony asked, did you and your brother share a bed? David nodded, and asked "Isn't it normally that way? I even share a bed with my grandpa, Ashton, ever since he enrolled to my school."
  They were going to brush their teeth, and it was quite a surprise that David literally produced the brush from his clothes (a brush formed by the fabrics of his clothing), that had been, in whatever means, changed into a sleeping pajamas.
  Anthony, feeling challenged, conjured a teethbrush out of thin air.
  David looked at that, "This nano, that reflexiors use, uses Aucafidian technology from about twenty thousand years ago, don't you know?" he asked casually
  
  "What?"
  "Don't you know? Technically, infrastructure you all use is of Aucafidian origin," he brushed his teeth
  
  "But your kind, white elves, just arrive here on Earth since 1950."
  "We, etoan, arrived here on 1950.
  Etoan is just one civilization of many Aucafidian civilizations" he said, and continued his teeth-brushing
  
  "So what came before your kind?"
  "Elohim came here first,"
  "Elohim?"
  "Yeah, El and his sons came here on Earth and consider it a world under his care.
  You know, they enacted the Divine Council of Earth.
  Some millennia later he restricted visa from Earth, and then at early twentieth century on human calendar, the visa is open again.
  That is when Grandpa decided to move to Earth."
  "Grandpa? Your Grandpa Ashton? How old is he? He's like about our age"
  "Oh yes, he's about our age, times a thousand."
  "...., I thought you were cousins, and was just joking around when you call him grandpa"


* [ ] **Scene 1B**

  Anthony and David finished brushing their teeth, and decided that they should be sleeping.
  They sleep as agreed, in the bed.
  Anthony asked what should they ask to Michelle tomorrow.
  David said they'd let her to choose.
  David, however, worried that Michelle wouldn't let them talk.
  Anthony agreed.
  There was a growing awkwardness
  
  Anthony broke that awkwardness by thanking David, for what he did in the dining room, that it wasn't his fault that Aditya did everything he did.
  David smiled, he said that in his opinion, whatever done by their parents are theirs, and theirs alone, and that it isn't fair to tag their children for their own fault.
  Anthony complimented that David is wise.
  David said Grandpa Ashton taught him that
  
  After some moment of silence, David voiced, that it is still hard to believe though, that he's befriending the son of his dad's enemy.
  Anthony said that Aditya is a control freak, so he must be pissed off when his plan is canceled by Steven, and he'd seek revenge.
  Anthony apologize that, for whatever reason, he involved David in his personal issues, solely to break Steven, by forcing him to kill David.
  David said once more, that it wasn't Anthony's fault



* [ ] **Scene 1C**

  Anthony is in that car again.
  However now the car is running, and he was sleeping.
  The woman was talking with that man.
  Anthony couldn't remember their exact wordings, but somewhat he understand what they mean
  
  "I think we should reconsider our decision, Seung-Min.
  He's our son, he's my Ki-Hwan" said that woman
  
  "No, *Martha*.
  We've been over this!"
  Anthony was awake now, he felt fear, and decided to pretend that he's still sleeping
  
  "Seung-Min, I..., I'm pregnant again.
  What if he, or she, born to be an indigo like Ki-Hwan? W-would you do the same again?"
  Anthony somehow could know that she's crying.
  He was pretty darn sure that he's under the blanket.
  Perhaps he's peeking, but he wasn't sure as well.
  It is as if he's a camera, flying in the dashboard, and facing the man and the woman.
  She sobbed, Anthony couldn't see her face, but he feels that the voice sounds familiar
  
  "This is your fault! You give birth to witches, wizards, children of evil!" that man yelled
  
  He raised his left arm, as if to beat her.
  She flinched, he flinched slightly as well.
  However the arm never reached her, it returned to the driving wheel
  
  Anthony noticed, that the car is Indonesian, as the driving seat is on the right side.
  If it is a korean car, the driving wheel would be on the other side
  
  "Is he awake?" asked that man, nervously
  
  The woman is still sobbing, and she checked on the back, where the boy, that Anthony recognized as himself, was still sleeping.
  Tears hidden under the blanket, she didn't notice it, but Anthony knew he was crying
  
  "Is he awake? Martha? I'm asking you" he raised his voice
  
  "I, I think he's sleeping" said Martha
  
  "Huh, I hope he didn't cause any trouble by summoning any *devil* on board" that man said
  
  They arrived in the woods.
  That man asked Anthony if he'd like to play in the woods.
  He was smiling his biggest smile, his grin might as well reached his ears
  
  Anthony decided that he must act now.
  He ran toward the woman, and hugged her with all of his might, yelling, "Eomma, nal bonaejuji ma!" (Mom! Don't let me go!)
  The strongest arm he had ever feel grabbed his legs, he couldn't move.
  The man started to beat him and her.
  They were in a room now.
  Apparently in a bathroom or something.
  They were wet.
  That man poured some water, and continued to beat her in her back, she was hugging, protecting Anthony, while screaming in pain
  
  Anthony called a name, he yelled, "Aurelius!"
  The water around them coalesced into a dolphin and streamed a mass of water toward the man.
  That man was sitting in the floor, now wet.
  He was shaking, his gaze jolted from one corner to another.
  Behind him, the mass of water coalesced again, and he screamed in all of his might
  
  "Aurelius, help us!" Anthony screamed
  
  A strong hug felt on his body, and another familiar male voice could be heard, "Anthony! Anthony! Wake up!"
  It was David's
  
  He opened his eyes, and discovered that he and David were floating inside a torrentous mass of cloud that fill the entire floor and the bed.
  It were swirling with great force that all furnitures were flying around.
  Michelle and Martha was in one corner of the room near the door, they were in panic, trying to cover their head from the streaming furnitures.
  David tried to brace him, and Anthony bursted in tears
  
  David gave him a hug, trying to calm him, as he always did to his grandpa, or his brothers, when they're crying.
  "It's okay, it's okay," he said.
  His voice is admittedly calming for Anthony, the intensity of the stream lowered
  
  "Calm down okay?" David put some distance from their chests, and he gazed at David
  
  "We're here with you," David continued
  
  "Aurelius, you can go now," commanded Anthony
  
  The clouds were gone, and all of the flying furnitures dropped back to the floor.
  Most of them were broken.
  David and Anthony stood in the middle of the bed, that is the only furniture not broken nor scratched
  
  "On the second thought, can you repair this room for me, Aurelius?" he commanded again
  
  The cloud manifested again, and now a body of a dolphin coalesced from the cloud.
  Then it dispersed again and the cloud moves all furnitures back to their proper places, and all broken parts were repaired, with no sign of it ever breaking within just a count of seconds.
  The cloud was gone as the last parts repaired
  
  "What was that?" screamed Michelle in tears
  
  Martha gazed at Anthony, her knowing gaze is familiar to Anthony
  
  "Ki..., Ki-Hwan? My Ki-Hwan?" she said, her tears streamed
  
  "Eomma?" Anthony gazed to the face he recognized
  
  Martha closed her mouth with her arms, her tears screamed joy
  
  "Ki-Hwan! My baby Ki-Hwan!" she said, running toward Anthony
  
  The two of them hugged in the bed, on awkwardness, David descended down from the bed
  
  "What was that?" he commented
  
  Michelle shaked her head, her tears stopped streaming, her gaze is that of curiosity now
  
  "Who is Ki-Hwan? Mom?" asked Michelle
  
  Martha tried to wipe her tears, Anthony still crying in her hug.
  Martha looked at her, her face is with joy, previously unseen to any eyes ever since her divorce, "He's your older brother, that your father dumped in the woods."
  "What?" asked Martha and David in unison


