# 20200414 Trip to Integra
Manov, Henokh, Derictor, Steven, Daniel, and Fernando went to Integra.
To do so, they need the help of Adran, which would be their "suit" to render them in Integra.
Within Integra, Adran will take the shape of three phones, Steven's, Daniel's, and Fernando's.

Integra is a virtual environment, so only Steven and Manov are familiar with it, and they enter as "etoan tourists" via Adran's projection.
Both Steven and Manov will be the "admins" of their projection, with some limited ability to "bend" the elements of their immediate environment.
The "bending" consist of identifying the threads and manipulate it.
So they have to know and identify the threads first.

Derictor and Fernando will be equipped with offensive capability.
Derictor is given the ability to control and move knives tagged with his touch.
Fernando is given the ability to push or pull objects to and from his body as long as he has seen and remember the objects.

Daniel and Henokh will be given the ability of altering the mindstates of Integra inhabitants.
Henokh can create a strong suggestion for others to do something he ordered.
Daniel can alter the state of consciousness of someone.

Adran is their secret weapon, it can alter their projection or any projection, and function as a pass to take a process and remove or add to the integra.
But it is a single use each.
If those actions are taken, each action costs a manifestation of him.
The amount of his manifestation is proportional to their ability to use their powers.
When expended, their power reduces significantly, and when all of its instances is expanded, they'd be removed from Integra.

Why the separations? Integra is a prison.
The operating system is what Manov and Steven interacted with, as well as what Adran interact with.
Then there are subjects, and objects of the Integra.
The subjects are inhabitants of Integra, the El's pieces, they're the ones locked here.
Henokh and Daniel are provided with a means of controlling the subjects to some extent.
The objects are the environment that the subjects live in, that must be consistent among any subjects.
This is what Derictor and Fernando can control.

I think we should also consider that Hadad and Tiamat can do something similar to that.
Hadad would be most likely more organized:
+ Bouncers, are those of Hadad's manifestation that can move objects around.
Similar to what Fernando and Derictor can do, sometimes significantly more elaborate.
+ Mentalists, are those that can alter inhabitant's consciousness, like Daniel's and Henokh's power.
Hadad focuses it to reduce the impact of anomalies to the inhabitants.
+ Planners, are those gifted with intellectual prowess to help them navigate this world, their "brain".
+ Editors, are those that modify the reality as required by the Planners. Their power is equivalent to Manov's and Steven's.
