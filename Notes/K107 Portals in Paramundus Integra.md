---
## Consider not having fullcaps for headings.
title: Inspiration
## We don't need a subtitle for now
subtitle: Portals in Paramundus Integra
## Information about authorship(s)
author: Hendrik Lie
date: 7 January 2020
## Fonts and formatting.
fontfamily: times
fontsize: 12pt
linestretch: 1.25
indent: true
papersize: a4
## Numbersections
numbersections: true
secnumdepth: 2
---

Paramundus Integra has portals leading to the interparamundi complex, a datasphere where all paramundus and manent realm are accessible.
Going in and out of Integra is easiest through the portals, though it is not impossible to enter from anywhere.

One of the portal would be a room inside the DVD rental they appeared.
To let them move from another portal, I think we have to have the first portal they appeared from to be destroyed by the Men of Tiamat.
Tiamat might want to follow Hadad's order to close the portals from and to Integra, to prevent the awakening of El.
Tiamat might play along, but it was her plans since the beginning, and the aim is to make it clear for the protagonist team that they must leave the world quick.
Tiamat aimed to create a pressure for the protagonist team.

The gate guardians, from WTF, was instructed to protect the gates, because WTF is independent, even though some of the citizens were Angels to the Divines.
Also, because WTF is affiliated with the Etoan Colony of Aucantica.

Etoan wished El to wake up because Kavretojives needs him to wake up.
Kavretojives wants El to wake up so that El can stop Hadad from continuing ruling.
Afterwards, Yam can defeat him and El, and then the Earth's immune system would fail.
Therefore Denefasan's attack could be commenced, and then ecorapture would take place.

Hadad wished El to remain asleep, and so he'd continue to rule Earth, and so would prevent Denefasan from attacking.
Hadad ordered Tiamat to do it, because apparently it is his only hope, despite the danger of her joining Yam again as his heart.
Tiamat did Hadad's order because it had been the plan since the last Baal Cycle, that Yam put in his heart.

The battle between the Gate Guardians from WTF and Men of Tiamat from Divine Council that take place in Integra would be the battle fought in the fabrics of Integra.
It would be perceived as anomalous events near the gate.
How anomalous?
Objects wouldn't behave logically inside the battlefield, time warps, and space doesn't make sense.
Reality shattered around the fight zone.

Both parties can bend reality, but in this case, the Gate Guardians would employ the Integra Reality Anchor (IRA), a powerful reality stitching device that works by observing and maintaining normalcy through counter reality bending.
In its core is the brains of the inhabitants of Integra wired up to the circuitry and perform constant observations, and then the entire assembly is possessed by the volantforms of the GaFE officers to produce counter reality bending ability, that reacts to the anomaly perceived by the inhabitant brains.

## HENDRIk INTEGRA
Hendrik Integra had his own journey before he is fully awakened.
Turns of events must occur in his life path before a piece of El is awakened inside him.
The plan is as follows:

+ Knowing that he is sinning is one thing, realizing that he must change is another. On his final days as a JHS student, he decided to look into the bible, and start reading it. It wasn't completed, but he is doing it anyway.
+ He felt sudden emptiness and longing inside his heart during his lone time studying far from home on his High School time. At the time, he felt God in him. God reassured him, and touched him, he cried hard.
+ He had been renewed, according to himself. And he meets an agnostic girl that he loved. He had not been treating her well, because at the same time he got a taste of friendship. Then he meets another woman, under the hood of friendship, they becomes closer. He was practically dating two different girls, and they offers different taste.
+ Near the end of his high school, the first girlfriend broke up with him. It scarred him a lot, and realizes how stupid he was. Signs had been provided, but he didn't see it in time.
+ As he's about to enroll to the university, his second girlfriend broke up with him because of his parents. His parents keeps pressuring him to stop, because the second girl is not a Chinese descended person, and that she's of a different religion. And that the parents accused him of doing something inappropriate with her, that he denied, despite actually doing it.
+ Then, he was afraid of the sins, and prayed hard in a mass one time. He almost felt it, the cry, the pressure, the holy spirit. If they're actually sins, he thought, he'd cry hard when the holy spirit cleansed it. But there isn't anything afterwards. Nothing. If only God answered it clearly, that it was a sin, it would be easy. Whatever God there was, he didn't make it easy for him. He questioned the entire basis of his faith.
+ Through his way toward atheism, something held him back: the sensation of meeting God he had in his high school year. He stays in agnotism for some time afterwards, can't truly convert to Atheism.
+ The next part of his life were spent toward being religious again, but now he's transformed, he let himself open to new ideas, and does not dare to question everything.
+ Then Xenomancy sprang into reality. A fictional worldbuilding he did actually exists. And the guys from Xenomancy told him that he's in Integra. And his world flipped hard, real hard. He's in a virtual world in his own fictional setting. Or is his fictional setting not fictional at all?
