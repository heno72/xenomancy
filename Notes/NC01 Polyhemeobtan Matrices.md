---
title: Polyhemeobtan Matrices
subtitle: Alternative blood for Etoan optimus
author: Hendrik Lie
date: 01 December 2023
fontfamily: times
fontsize: 12pt
linestretch: 1.25
indent: true
papersize: a4
numbersections: true
secnumdepth: 2
---

# Article

Polyhemeobtan matrix is a class of programmable single-molecule macro-polymer robots present in the body of typical *Etoan optimus* species.
They function as blood replacement in absence of *Etoan optimus*' blood cells.
Their programmable nature enables them to take over natural blood cells such as leukocytes, thrombocytes, and erythrocytes.

Polyhemeobtan matrix is artificial in origin, and is virtually present in all modern *Etoan optimus* as a part of modifications made by Kavretojives during the Great Suffering Age at around 70kya.
The modification enables resistance against malevolent nanocytes from other powers during that period.
It was retained to the modern age due to its beneficial properties such as extremely resilient immune system, damaged tissue repairs, and hyper-efficient nutrients and oxygen transport capacities.

# Research notes

- Red blood cell weighs roughly 27 picograms per cell, or about `2.7e-14 kg`.
- The discocyte shape of human red blood cells are approximately 7.5 to 8.7 nm in diameter and 1.7 to 2.2 nm in thickness.
- About a third of red blood cell mass (with water) is comprised of hemoglobin, or about 96% of its dry mass. Hemoglobin has an oxygen-binding capacity of 1.34 mL of O2 per gram,[6](https://en.wikipedia.org/wiki/Hemoglobin#cite_note-7) which increases the total [blood oxygen capacity](https://en.wikipedia.org/wiki/Blood_oxygen_capacity "Blood oxygen capacity") seventy-fold compared to dissolved oxygen in blood plasma alone.[7](https://en.wikipedia.org/wiki/Hemoglobin#cite_note-8) The mammalian hemoglobin molecule can bind and transport up to four oxygen molecules.[8](https://en.wikipedia.org/wiki/Hemoglobin#cite_note-brsphys-9)

# Reading materials

- https://en.wikipedia.org/wiki/Red_blood_cell
- https://en.wikipedia.org/wiki/Blood_substitute
- https://en.wikipedia.org/wiki/Hemoglobin
- https://www.ncbi.nlm.nih.gov/pmc/articles/PMC2998922/
- https://physics.aps.org/articles/v5/s140

# PDFs

- ![[Documents/nihms205981.pdf|nihms205981]]
