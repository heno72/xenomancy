---
## Consider not having fullcaps for headings.
title: Inspiration
## We don't need a subtitle for now
subtitle: Sylvia Lusien and 2003 Storyline
## Information about authorship(s)
author: Hendrik Lie
date: 19 May 2022
## Fonts and formatting.
fontfamily: times
fontsize: 12pt
linestretch: 1.25
indent: true
papersize: a4
## Numbersections
numbersections: true
secnumdepth: 2
---

# Introduction

Considering that in Inspiration M506 might be at play, Sylvia Lusien is Tudva, and Anderson Pondalissido is a Transderivational Entity, derived directly from Leaf.
Then, following on, we can then derive the idea that they must be after something in 2003 era.
The most probable thing they're after is then the beginning of the conflict: Godfluenza.

As stated in Inspiration M506:

> Maybe, if Ashton is the cure, she's the antibody.

We want to accomplish two things at the moment:

1. To establish Sylvia's role and Anderson's role in 2003 storyline; and

2. To draft out Leaf's characteristics that governs their behavior.

# The Role of Sylvia in 2003 Storyline

## Characters

Oh damn, I don't really know them really well.

### Antagonist

1. Victor Axel Armaniputera (Economy), Nico's brother, is apparently an independently raised, isolated reflexior born of human parents.

### The Agents

1. Sylvia Lusien (Junior High), is an adopted younger sister of Daniel Lusien, and is the biological grandmother of Steven.
1. Anderson Pondalissido (A Law Professor), is a Transderivational Entity derived from Leaf, and a partner of Tudva (Sylvia), Ashton, and Adran.
1. Alex Pontirijaris (Law), a young son of a family that lives in a post-scarcity society, a twin of Steven, an athletic and competitive, friend of Aditya, Henokh, and Derictor.

### HD Club

1. Steven Pontirijaris (Med), a young son of a family that lives in a post-scarcity society, friend of Daniel and Fernando.
   He is a son of a family that lives in a post-scarcity society.
1. Daniel Lusien (Med), adoptive brother of Sylvia Lusien, and a son of a rich family.
   Also known as Mr. Stone (for his stonehard body and posture).
   Also an inquisitive, observant, and avid sci-fi fan.
   He is a cousin of Derictor Wijaya and Henokh Lisander.
1. Fernando Suryantara (Med), also known as Mr. Sun (from "Surya"), is the first one among his extended family to study abroad.

### Three Ways Club

1. Henokh Lisander (Economy), an Uber rich arrogant boy, a cousin of Daniel.
1. Derictor Wijaya (Law), a sexually passionate but seemingly innocent guy.
1. Aditya Wijaya (Law), an assertive, observant, and coldheaded guy.

### Others

1. Helena Irawan (Med),
1. Martha Williams (Pharmacy), also see **"K726 Helena, Martha, Sylvia."**
1. Nicolas Armaniputera (Med),

## Setting

See **"K726 Helena, Martha, Sylvia."**

## Plot

The basic premise is that Victor is an untrained Reflexior of not so noteworthy power, able to control clothes or fabrics.
However, he's also rather adept in using deductive telepathy.

However, Anderson and Sylvia, using socionomic analysis, come up with a conclusion that Victor is a danger to Earth, being a carrier of memetic virus Godfluenza.

There are several ideas I have:

1. Time is when Steven ran away and is probably with Victor.
   Alex came up with explanation of how severe the situation is to Daniel and Fernando, and how, Steven is not normal in etoan society.
   Daniel expressed that etoan is the one that is not normal, being pan majority, unlike human's straight majority.
   Alex explains that it's due to the fact partners can be of either or neither sex to have offspring, that society reaches equilibrium when sex is no longer limited to opposite genders, or of the same species.
   However, being straight, Steven is confused, especially when, as Alex pointed out, Fernando confessed his love to Steven.
   Fernando objects, said it is not wrong to confess, on which Alex said yes, but Steven is confused.
   And a confused Steven wouldn't do any good in this situation.

1. Alex explained Victor's power, on which he can control fabrics, and is also very persuasive.
   Fernando corroborates that Victor is indeed a very persuasive man.
   Looking back retrospectively, that's the kind of manipulation Victor did to coerce his victims when they don't cooperate.
   It is also the kind of tricks he played on to convince his victims that they're feeling something to him.
   
   Meanwhile, there is a cutscene where Derictor, in his room, is talking with Victor.
   They talked about politics mostly, and philosopy maybe.
   They were agreeing that the current government is corrupt and order is blurry at the higher echelon.
   Therefore law only being enforced to helpless citizens, while higher echelons enjoy lawfree estates.
   Derictor felt something crawling on his skin, almost like something gently touched him about.
   The distance between him and Victor closes.
   
   Alex also demonstrates his etoan suit, and said that Victor's power has no effect on his suit because of how magic-like manifestation affects the world.
   Sans reflexium particles, Victor's power would be meaningless.
   Daniel said that it means to fight Victor, they might need to use the suit too.
   However Alex said it is a bit tricky because they're human, and the suit is designed to be operated by Etoan.
   
1. Sylvia came to explain that the persuasive power Victor has is not only from his ability to play tricks with clothes.
   It is also from his Deductive Telepathy.
   Daniel took a moment to consider but then asked her, what she knows about this entire ordeal.
   She said that Steven is her grandson, on which Daniel said it was all madness.
   She demonstrated her suit, and there's no further question from Daniel.
   
   She continued that compared to *most* animals, humans have superpowers.
   That is, the ability to transmit concepts and ideas through verbal communication.
   Animals, however, are limited to emotional cues and body gestures to communicate with each other.
   However, it is also our weak points, because we are used to rely mostly on our verbal communications.
   If one could utilize both emotional cues and verbal communications, unprepared victim would find it hard to resist the perpetrator's wishes.
   
   Cutscene, Derictor find weird sensation across his body, almost as if his shirt slowly pull him toward Victor.
   As their conversation heated, and Derictor gazed at Victor's passionate eyes, he couldn't understand what he felt.
   Victor pulled Derictor's hand and place it to his chest, and Derictor could feel Victor's beats, and he could also feel his own beats.
   Victor uses that in his argument that they're feeling good about each other.
   They kissed.
   
1. It is emphasized though, that neither Alex, Steven, or Sylvia lied about it.
   The fact that they're etoan is never hidden.
   Daniel raises a question of don't they need to hide anything from the public?
   Sylvia said that there is no need to lie about their nature.
   No one would naturally believe them anyway.
   Fernando asked them on whether they'd say the same about the government, and she said that they got The Government's permit to be able to reside here.
   Daniel clarified, Indonesian government?
   Alex said no, it is not the Government Sylvia is talking about.
   Sylvia said they got their permit from the Divine Council of Earth.
   Daniel and Fernando are lost.
   Alex explained that perhaps the closest approximation in judeochristian mythology, it is The Throne.
   Daniel asked, God?
   More or less, said Alex.
   It is complicated, said Sylvia, but that will make do for now.
   
1. Fernando raised the question, how then, we are going to combat an emotional controlling fashion wizard?
   Sylvia raised up the fact that that technique does have a weak point.
   That is, it requires the victim to be constantly in focus to the actor.
   Therefore, a distraction would almost certainly broke the spell.
   
   Derictor and Victor are getting comfortable with one another, carressing one another, and stroking one another.
   They are lost in their own rhythm, and to them the world is pulsating around them.
   Aditya entered to Derictor's room and asked the whereabouts of Henokh.
   
   On which all three of them freezes.
   Aditya for he is completely unprepared to face such situation with his half naked friends.
   Derictor for he snapped out of the trance and realizes what they're doing at the moment.
   Victor for he realizes there is no way he could steer Derictor's emotion to continue their ordeal.
   
1. Daniel asked, can't the perpetrator just re-steer the victim into the "zone" and continue whatever spell they put in place?
   To that, Sylvia said that it's what in human term call Ericsonian Hypnosis.
   One can not be forced to do something they don't think they want to do.
   However, one can be redirected carefully into thinking that the idea to do certain things came from within themselves.
   In that statement, Fernando said that in a hindsight, that's exactly what happened whenever he's around Victor.
   Somewhat, at the moment, the thought always appear to come from Fernando's own mind, but if Fernando think of it clearly,
   all are first suggested by Victor, though in a way that's merely asking for confirmation, and the answer of that comes first from Fernando,
   or rather, the victim.
   Sylvia continues that it requires you first trust the perpetrator, and in trust, you'd willingly accept their suggestions as yours.
   However, when one's surprised, or the contact is broken, so to say, the first thing you'd do is to reflect on yourself, of what the hell
   had you done.
   At that state, we are less suceptible to suggestions, as the first priority we have at that moment is to understand our train of thoughts.
   
   Aditya broke the contact and said, he'd find Henokh by his own.
   Derictor froze, and Victor realizes he's tied up, there isn't much he can suggest in a manner that would allow future encounter with Derictor.
   There's just one way at the moment to gain Derictor's trust: To make it as if he's helping Derictor along the way, and that he is well-intentioned.
   Victor said go talk to Aditya, while Victor would see himself out of Derictor's room.
   So they snatched up their clothes, Derictor then wear them and run to Aditya's room.
   
   Derictor found Aditya alone in his room, processing what he just witnessed, and let Derictor enter.
   However, Derictor can't say a single words.
   Even though, in Derictor's head, he's saying a lot of things at the same time: it is just a misunderstanding, I am not a gay, or am I? I mean, what I did is not, I didn't mean it but.
   We decided to do it together but, but.
   What but?
   
   Aditya, somewhat recognizes that looks in Derictor's eyes, said that it's fine, I don't mind.
   Derictor cried like a baby and came to Aditya's chest.
   Aditya continues to calm him down, and said "that's okay, I understand, you don't have to say anything."
   Derictor continued to cry his chest out.

1. Sylvia opened up to say that she's a socionomist, and her field of study is socionomy.
   It is a study of interpersonal relationships and its effect to the broader sociopolitical system of a particular society.
   It is a mature applied science, unlike its human counterpart sociology.
   And they reached the point where they can make predictions with the framework, and is basically an everyday tech, just like material engineering is today.
   It is applied in countless of worlds in the entire civilized galaxy.
   That is also how she's becoming a well known author, because of her deep understanding of socionomist also gave her a side effect to be a very good storyteller.
   
   However, the importance of her study, she said, is that she diagnosed what is known as a Godfluenza.
   Applying socionomical analysis on Victor gave her a positive.
   It makes him a dangerous individual, she said.
   Daniel asked her, what the hell is Godfluenza?

   As Derictor calmed down, Aditya asked him what's going on.
   Derictor start to explain that it starts out as nothing.
   It's just a normal discussion, about politics mostly, probably also about philosophy.
   Of how, despite they're being proud to be the state of law, where the supremacy of law is said to be held high,
   in fact chaos spreads anyway.
   Corruption is rampant, higher echelons are free from the law, and normal, helpless people are being suppressed by law.
   He said that thing's don't have to be like this, there must be another way.
   
   Sylvia explained that Godfluenza is not a biological virus, but rather, a memetic one.
   It can replicate, mutate, and evolve along the way to effects unforeseen before.
   However, with proper socionomical analysis, there's a possibility of it being a major problem.
   Especially when the strain could be identified, and is similar to one known variant of Godfluenza.
   She said that this particular strain of Godfluenza is known to cause minor malfunction to gods, when a sufficiently large amount of its
   subjects are infected by it.
   
   How does it spread? Fernando said, like, its infection vector?
   Thoughts, say Sylvia.
   It could be as simple as a brief exchange of idea.
   From that, it would then spread to the next person, and to the next, and to the next.
   When it arrives at the correct person, it might eventually develop further into a more impactful strain, that then we would start to see
   symptoms on gods.
   
   We are the cells, said Daniel, and the virus took advantage of our thoughts to spread.
   The body is then gods, that would start to show symptoms when there are large enough of cells infected with it.
   Exactly, said Sylvia.
   
   I don't see how talking about that leads to what I saw, said Aditya, but he lower the tone when he saw Derictor's face.
   I don't know what happened, I think I find him admirable, and he's freaking handsome.
   Also, I think I might've develop crush on him.
   How so? Said Aditya.
   I don't know, at first I thought I admired him, but I feel something else, I feel obsessed about him.
   Then you did what I saw back then? Continued Aditya.
   Yes, something like that.
   
   Dear Derictor, said Aditya.
   I think you're being used by Victor.
   I know for a start, that you're not gay.
   I am not?
   Yes you are not.
   How could you be sure?
   I mean I feel like maybe I'm something like that.
   Under normal circumstances, you wouldn't fall for men.
   I do?
   Why don't we do this, I offer myself to you, would you take that?
   Ew no, please.
   See?
   No, I don't?
   Am I not attractive enough?
   Yes you are but, but.
   See?
   Yes, I think?
   He's doing it, toying around, and you're confused, and he's taking advantage of that confusion.
   Oh damn.
   Yes right, oh damn.
   
   Damn him, and it wasn't a voice of either of them.
   It was Henokh's voice.
   There was silence.
   
   Since when did you here?
   Long enough to know that Victor is a bastard.
   How dare he toy with our Derictor.
   I tell you what, he's playing with the wrong group.


Deductive Telepathy.

# Anderson as a Transderivational Entity

In Solenadactilian notes, they are governed solely by two major emotions (or what they consider emotions anyway): curiosity and caution.
If in pursuing their curiosity, they would not come to harm, they would proceed it first.
If they judge that their curiosity would cause them more harm than benefits they would get, they would simply not do it.

Also, physiologically, they are vastly different from that of humanoid body plan.
First, they have eight limbs, each of them have complete set of organs in them, but some are specialized.
For example the head is specialized for sensory organs, the tail for balance, the two major pairs of limbs for locomotion, and the middle pair is for procreation.
Their brain encircles their mouth, that is located at the ventral region of their central disk.

When they have to be crammed into a humanoid body, their brain is relocated further away from their mouth, their radial oral digits have to be conjoined into lips and tongues.
Then their major sensory limb must be crammed into a humanoid head, their frontal major limbs into manipulator limbs of a humanoid body plan, while their rear major limbs be crammed into legs.
Their genitalia and their tail limb are then rejoined as a humanoid penis.
It is so happen that our penis is comprised of three tubes, hence the urethra is played by its tail limb.

