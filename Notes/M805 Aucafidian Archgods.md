---
title: Ten Aucafidian Archgods
author: Hendrik Lie
date: M805
---

# Breakdown of Archgods

## Kavretojives

The first, the origin.
The symbol of Love, Hope, and Salvation.
His extensive presence and intimate relationship with his subjects is his signature.

His core tenets are:

a. *Believe in Kav,* and then
b. *Believe in people* too.

Its core focus is to its people.
Kav is known as a personal Patron, guide, and friend of his citizens.
Relationship with him is personal and intimate.
He can be seen as a hub that connects all of his subjects to one another.
His Presence exists surprisingly well spread among all population clusters in his domain, ranging from 1 in 100 individuals in core worlds, up to 1 in 10,000 individuals on the outer worlds.

On his core tenets, to believe is to love.
To love, is to live.

Kav is said to exist as long as his subjects believe in him, and those that believe in him, believe in people.
Kav does not force its subjects, and his power to his subjects will be gone the moment his subjects no longer believe in him.

## Intelligence

Intelligence is an impartial, indifferent, and fair entity.
It is the symbol of freedom and experiences.
Its seemingly nonpresence yet ubiquitous influences is its signature.
It is everywhere, yet it is not visible.

Its core tenets are:

a. **Fundamental Freedom** of all beings;
b. **Recognition of Freedom** of other beings; and
c. **Respect of Freedom** of free beings.

Intelligence exists in the very core of intelligence, and flourish in total freedom.
Therefore, it encourages maximum freedom to all thingking beings.
It does not care about whether people respect or worship it, it only cares about freedom.
As long as there is freedom, it exists.

There are boundaries, namely the freedom of other beings, as they are equally valid to everyone's personal freedom.
One's freedom ends where other's freedom starts.
It does not care if other entities are not as free as it and its subjects, but it demands mutual respect, of its freedom, and other's deeds.

Its core tenets deals with freedom of various levels.
The first tenet deals with the fact that all beings inside its domain is free.
The second point emerges from the fact that all free beings must understand and recognize other beings as equally free, so they must not violate other's beings when they exercises their freedom.
The last tenet speaks about Intelligence's expectations when dealing with other entities beyond its core domains.

They don't care about the status of freedom on other domains, but when those external beings interact with beings in its domain, their rights must be honored.
Including all promises, deals, and contracts formed in the exercise of freedom of beigns in its domain.

## Negentropy

Negentropy is the front line against entropy.
Equivalent to the force of nature: an unstoppable, highly efficient and surgically precise force with the aim to eradicate or reverse entropy.
Its central tenets are the Four Laws of Negentropy, comprised of a **Grand Directive**, and the **Three Policies on Actions**:

1. **Law of Negentropy Maximization:** One must, within their power, act in a manner that minimizes total entropy, or to maximize the total Negentropy of the environment.
2. **Law of No Harmful Acts:** One must avoid any possible manner of action that increases total entropy or that decreases total negentropy.
3. **Law of Preservation:** One must preserve any negentropic system, unless in preserving one, causes total entropy to increase.
4. **Law of Creation:** One may create negentropy, unless in creating, total entropy will increase or that said creation can not be preserved.

## Soul

Soul is said to be a congregation of life, and the symbol of fertility and growth.
Within their domain, life of all kinds flourish and thrive.

Their central tenets are the **Grand Directives:**

1. **Introduction** and **Proliferation** of life.
2. **Preservation** and **Continuity** of life.
3. **Growth** and **Evolution** of life.

Soul emphasizes that a Soul is an emergent property of life.
Soul is transient in nature, and can grow and evolve.
Soul flourishes in the presence of other souls, therefore quantity and quality of souls must be maintained and continued.
To maximize the chances of souls to flourish and evolve, new souls must be introduced to soul-less environments.
Therefore, the radiance of souls must follow the path as described in their Grand Directives.

Those in Soul Domain have various interpretations of the Three Domains.
Some believes that the first two directives have already been accomplished, so they focus on the third directive.
Some believes that the first directive have already been accomplished, but the second is on the way, so they focus on the third directive.
Some believes that none is accomplished yet, so focus on the first directive.

## Teacher

With influences of freedom from Intelligence, and personal growth from Soul, Teacher is a place of academic freedom and personal growth.
Within its domain, knowledge is free to access and contributions to refine or add knowledge is encouraged.
They focuses on knowledge as a mean to better their life by applying practical sciences to everyday life, both social and natural sciences.

It is a world where every problem is a science problem that can be solved.
It is not unlike that of Isaac Asimov's Second Galactic Empire.

We can see Teacher's domain is like a civilization run by universities.
Policies are made to first and foremost, education and development of sciences.
Society is a product of sciences applied to everyday life.

## Administrator / Kavretoajif / Panai

Its core tenets are organization and enforcement.
it is heavily influenced by Kav and Negentropy on their borders.
It has elements of Kav's omnipresence in his society, and efficiency focus of Negentropy.
All kinds of intelligences in this domain is inherently artificial and optimized in nature.
The worlds in their domain tend to be inhospitable for natural biological entities as they strives for efficient use of mass energy and information processing.
It is naturally the homeworld of robots and infomorphs.

## Leader

It combines the tenets of Intelligence, Negentropy, and Soul.
It symbolizes civilization and entity.
It focuses on:

a. freedom of its citizen,
a. order of governance and enforcement, then
a. equal opportunities for its citizens to grow and be the best version of themselves.

The leader is just a guide and the head of **The Government**.
Its worlds tend to be ruled by the local **Tripartite Boards of State** (TBS).
The TBS is a state institution comprised of:

a. **The Government** is the system to enforce law.
a. **The Lawmakers** is the system to create law.
a. **The Dispute Resolution Board** is the system to settle legal disputes and to pass judgements.

The Leader intervenes by selecting citizens to be promoted as candidates for a position in one of the TBS.
The public then will choose in a consensus whether or not to accept the suggested individuals.
In the event that TBS or one of its constituent elements fails, The Leader will intervene and replace their roles until the problem subsides, where The Leader will either reinstate the old TBS or reconstitute a new one.

In a manner, it can be seen as an alternative interpretation of Pontirijarisianism in Kav's Domain, or a direct antithesis of Kav's core tenets.
Both Kav and Leader can be seen as a synthesis of Intelligence, Negentropy, and Soul tenets.
They are both central figures in their civilization.
However, in Kav's domain, Kav is the central focus, while in Leader's domain, its subjects are the focus.

## Charisma

Charisma is a combination of tenets from the Teacher and the Leader.
With extensive proliferation of knowledge, and efficient governance of the Public Administration Bodies, public opinion is brought to attention and initiative of **Idols,** or basically Influencers.
The civilization thrives in the Entertainment Industry, with Idols personifies their followers aspirations and inspirations.
The Idols voices the good deeds and messages to their followers.

Each Idols are selected for and trained by **Agencies**, before debuting to the public.
They represents the most optimal beings of a certain civilization or a race, driven mainly by their altruistic desire to voice the good causes.

Charisma is said to exist in the Agencies, or even is a congregation of Agencies.

## Overseer

It has elements of the Leader and the Administrator.
The core tenets are curation of knowledge and dissemination of misinformations.
Every facets of social life tend to be manifested in a Political Party.
Each party strives for a cause or a set of causes: aspirations and inspirations of its members.

Every individuals are free to associate with or disassociate with a party.
Membership to a party might be exclusive or non-exclusive, and parties might band together for a common cause.
The Government and other Public Offices have officers supplied by parties with plenty of supports or endorsements from the public.
More supports or endorsements from the public grants a party a higher share of seats in the Public Positions.

Parties do not have to solely compete for Public Positions, they can also amass wealths and influences by conducting altruistic acts to the public.
They might even have public facilities built by them and for the members or the public.

## Mind / Integrity / Denefasan

Integrity is in between the border of Soul and Teacher.
It combines the tenets of Teacher, Administration, and Leader.

Its central belief is in the balance of the Mind, and a Mind's integrity is highly respected.
It is heavily influenced by Charisma and Overseer as its members believes in perfection and balance.
Its society is pro life and biotech, and Mind is their focus.

Form doesn't matter much for them, as they are used with the disembodiement of the mind.
Forms only facilitates their needs, and is disposable.
Modularity is highly sought for, and specialized bodies might be selected depending on their needs.
They often rent bodies or develop bodies, that they occupy only when they have to.

When they are not occupying a body, they congregate, discuss, and plan for the fiturue.
Their civilization does not seem to have any apparent government or ruling bodies.
Most often, they focus more on cooperation, consensus, and public goals, almost like a distributed swarm hive mind.

# Variables of Interests

Currently, we tracks the following variables:

1. Data Panel
   a. Patron
   a. COI ID
   a. Alignment
   a. Rules of Engagement
   a. Summary
1. Civilization Statistics
   a. Volume size
   a. Kardashev Scale
   a. T-scale
   a. Homeworld
   a. Established
   a. Population
   a. Common propulsion
   
Followed with a section: Description.

However, Orion's Arm tracks:

1. Empire
1. Definition
1. Symbol
1. Ruling Archailect
1. Archailect Intervention
1. (AI) Ethos
1. Founded
1. Technology Level
1. Culture (sometimes Society)
   a. Metapsychology
   a. Society
   a. Values
   a. Religon/Ideology
   a. Languages
   a. Culture and Art
   a. Architecture
1. Territory and Population
   a. Current Territory
   a. Capital
   a. Systems
   a. Important Systems
   a. Representative Systems
   a. Important Natural Objects (stars and nebulae)
   a. (Core) Population
   a. (Core) Population Breakdown
   a. Nexus Infrastructure
1. Government and Administration
   a. Government
   a. Administrative Divisions
   a. National Holidays
   a. Constitution
   a. Legal Systems
1. Economics and Infrastructure
   a. Markets
   a. Currencies
   a. Dysons
   a. Major Industries
   a. Major Stargate Nexi
   a. Military Expenditures
1. Foreign relations
   a. Allies/Rivals
   a. Defense and Intervention
   a. Warfare
   a. Interstellar treaty organization participation
   a. Interstellar Relations
   a. Interstellar Disputes
   a. Exports
   a. Imports

We might want to consider what features are we going to track for each Civilizations or Meta-Civilizations.

## Variable Breakdown

1. Basic Information
   a. Classification (meta-civilization, civilization, empire, culture, etc).
      A meta-civilization is a group of interconnected civilization sharing a common standard, protocol, laws, or means of communication or transaction with one another.
      A civilization is defined as a complex society associated with a set of common cultural spheres.
      A culture is a society with its associated specific set of ideas, customs, aesthetics, and ideals.
      An empire is characterized with a centralized government or control over a broad range of cultures or a civilization.
      
   a. Name
      (the civilization name)
   
   a. Definition.
   
   a. Ruling Entity.
   
      Generally, a meta-civilization tend to be ruled by a single Archgod.
      A civilization might be ruled by a God or any entities below God but must not be less than a Sapient-level intelligence.
      A culture can be ruled by any entities, even by a nonsentient entities (protocols, laws, or any set of rules is sufficient to define a culture).
      
   a. Ruler Intervention
      (When does the ruling entity decide to intervene. Is it common or rare, or on what occasion?)
      
   a. Ethos
      (What are the values of the ruling entities or its subordinates)
      
   a. Founded
   
   a. Technology
      (level of technology available to the ruling entities or its subsidiaries, or available for the common population, and of what kinds)
      
1. Culture
   a. Metapsychology
   a. Society
   a. Values
   a. Religon/Ideology
   a. Languages
   a. Culture and Art
   a. Architecture
1. Territory and Population
   a. Current Territory
   a. Capital
   a. Systems
   a. Important Systems
   a. Representative Systems
   a. Important Natural Objects (stars and nebulae)
   a. (Core) Population
   a. (Core) Population Breakdown
   a. Wormhole Infrastructure (whether or not the systems inside is closely or extensively linked with wormholes or not)
1. Government and Administration
   a. Government
   a. Administrative Divisions
   a. National Holidays
   a. Constitution
   a. Legal Systems
1. Economics and Infrastructure
   a. Markets
   a. Currencies
   a. Dysons
   a. Major Industries
   a. Major Wormhole Hubs
1. Foreign relations
   a. Allies/Rivals
   a. Defense and Intervention
   a. Warfare
   a. Interstellar treaty organization participation
   a. Interstellar Relations
   a. Interstellar Disputes
   a. Exports
   a. Imports

I think we have to consider levels of details.
Higher level of classification, for example on the level of a Meta-civilization, details should be minimal.
The lower the classification, the more details are included.
The rationale is that at higher level, lower level population members would be more diverse, therefore only broad categorization can be described.

| Details | Meta-civilization | Civilization | Culture | Empire |
| :------ | :----------- | :----------- | :----------- | :----------- |
|Basic information| Mandatory | Mandatory | Mandatory | Mandatory |
|Culture| Optional, broad | Optional, broad | Encouraged | Encouraged |
|Territory and Population| General | General | Specific | Specific |
|Government and Administration| Optional | Optional | Encouraged | Mandatory |
|Economics and Infrastructure| Optional | Optional | Encouraged | Mandatory |
|Foreign relations| Optional | Optional | Optional | Encouraged |

Do note that we already have some tracked values, that we would need to readjust with the aforementioned changes.
We have to consider how we will organize the following attributes:

1. **Basic Information (mandatory)**
1. Culture (optional)
1. **Territory and Population (mandatory)**
1. Government and Administration (mandatory only for empires)
1. Economic and Infrastructure (mandatory only for empires)
1. Foreign relations (optional)

to our existing attributes:

1. Data Panel
1. Civilization Statistics

Essentially, Civilization statistics would have taken care of broad and wide range statistics such as volume, Kardashev Scale, T-scale, founding, homeworld (Capital?), total population size, and common propulsion.
Homeworld might need to be replaced as its capital world, as the domain spans on a vast volume of space, majority of them might barely even consider the original Homeworld as their home planet.

Given that only **Basic Information** and **Territory and Population** are mandatory for all classes, we might as well consider them to be essential in each COI entries.
Most information in **Basic Information** is already been covered in the **Data Panel,** so much so that maybe we should just combine them together as just a **Data Panel**.
Meanwhile, some details might need to change, especially:

- COI ID to replace Name
- Summary to replace Definition
- Patron to replace Ruling Entity
- Patron Intervention to replace Ruler Intervention
- Founded to replace Establishment

We might also need to differentiate **Alignment** and **Ethos**.
Alignment is only the civilization's general behavior toward antropomorphic beings (such as humans or etoans).
Ethos is a general description of its domain ruling or policy.

In fact, we might need to break down **Data Panel** into **Data Panel** and **Patron Information**.
Data panel should contain the most basic information about the civilization of interest: their classification, COI ID, summary, founding date, and technology level.
Technology level is basically a more broad version of "Common propulsion" variable, and thus a replacement.
Meanwhile Patron information would group together all information related to the Patron, or its ruling entity.

Patron is selected instead of Ruler because the status of most Aucafidian ruling body is similar to the role of Patron gods in the ancient Canaanite.
They are distinct from the governing body because they are absolute, while they may select their own officials or decide to actively participate in the government.
They are the one to set the laws, governance, and various large-scale megastructure and godtech infrastructure projects.
They also alter the society's general psychology and values in many ways to suit their needs.
In short, a Patron is a force of nature in its domain.

Civilization Statistics and Territory will also be differentiated.
While Civilization statistics deal only on statistics and numbers, Territory deals with specifics.
We also include population and population breakdown, where the former is on the statistics, and the later is on the territory.
The "current territory" variable will handle a basic description of its volume, specifically its location in space.

## Basic Variables

Therefore the most basic data structure should look like the following:

1. **Data Panel** (Basic Information)
   a. **Classification**
   a. **COI ID** (Name)
   a. **Rules of Engagement**
   a. **Summary** (Definition)
   a. **Founded** (formerly Established)
   a. **Technology** (replaces Common propulsion)
1. **Patron Information**
   a. **Patron** (Ruling Entity)
   a. **Patron Intervention** (Ruler Intervention)
   a. **Alignment**
   a. **Ethos**
1. **Civilization Statistics**
   a. **Volume**
   a. **Kardashev Scale**
   a. **T-scale**
   a. **Population**
1. **Territory**
   a. **Capital System(s)** (Homeworld)
   a. **Current Territory**
   a. **Population Breakdown** (a breakdown of population composition)
   a. **Wormhole Infrastructure** (whether or not the systems inside is closely or extensively linked with wormholes or not)

Likewise, pages that describe Domains, and therefore is associated with one of the ten Archgods, should include those parameters as a table.
We should then define each Archgods and their associated values as above.

For any level underneath the Domain level, we should also include the following groups:

1. Culture (optional)
1. Government and Administration (mandatory only for empires)
1. Economic and Infrastructure (mandatory only for empires)
1. Foreign relations (optional)

Preferrably, they're not as tables, but instead a comprehensive essay for each of them.

