---
title: Inspiration
subtitle: Deductive Telepathy
## Information about authorship(s)
author: Hendrik Lie
date: 22 March 2022
## Fonts and formatting
fontfamily: times
fontsize: 12pt
linestretch: 1.25
indent: true
papersize: a4
## Numbersections
numbersections: true
secnumdepth: 2
---

I think Anderson has some powers similar to that of the mentalics in Asimov.
Though it would be more materialistic in nature.

Deductive telepathy is the ability to deduce mental states of one person by analyzing their movements, expressions, responses, and the manner of speaking.
In short, it is the ability to make guesses of one's mind, based on unconscious cues one involuntarily provides.

It's not only one way.
By deliberately giving subliminal cues on our expression, wordings, and movements, we can somewhat condition one's unconsciousness to our likings.
Often manifested in a way where the victim involuntarily do something or decide on something without even realizing that we're the one "suggesting" them to do it.

If Anderson is to describe it, he'd say "the natural language of animals." It works for most modern mammals, therefore making him able to somehow direct animals to a certain degree.

Ashton would be quite adept to realize deliberate reconditions Anderson can do, and once he realized it, it'll be rendered useless for Ashton.
At some limited extent, Ashton can do it too, albeit severely limited.
It's simply because of his age, and educational though.
It's also a part of his curriculum for ORCA.

Though in no way ORCA or Ashton can reach the level of Anderson's deductive telepathy, it is more than enough to give them advantages over most people.

