---
tags:
  - poi/steve
  - poi/lena
  - poi/dan
  - poi/sun
---

# Alteration 20200708: Character Developments

How they make love can also tell us more about their characters.
It can tell us what is important for them in having fun, or intimate moments they have.

To **Steven**, it is a game.
Given that he could control his performance better than average joe (a lot of our physiology can be voluntary to etoan, just like our breathing), he's almost certainly outrun a lot of human performers.
Given their unique heart, with higher blood pressures for their body (their central and vetebral nervous system excluded), they can remove waste and replenish nutrients to their muscles faster than us.
So, he'd be mostly focussing on how Helena is feeling.
Maintaining rythm, managing variations and changes, he'd easily made her crazy.
This is unfair, yes, but that's where Steven is lacking.

**Helena** does enjoy foreplay, and Steven is inhumanly good for it.
Because she's always been provided with excellent services, which is a luxury for most women, deep inside, she just wished an ordinary bed game.
I think she cared more on normalcy, given how she insisted human appliances to stay, despite their etoan counterparts worked much better.
And how she heard Fernando and Daniel joked about their bed game mishaps every now and then.
She expected that.
She expected bed game dramas.
She expected surprises, unexpected turns.
As, despite Steven being almost flawless in his bed performances, the result is almost always good, so in a way, predictable.
Perhaps she wanted to cling to whatever humanity remained that she had.
A consequence of having an alien husband and a bunch of alien children, is that sometimes you could forget that you're human.
Which is why she loves to hang out with her human friends.
I think she's quite close with Martha, and a bunch of other women.
Perhaps also friends with Go Jiaota, as they're fellow medicine practitioner.

**Daniel** tend to focus on objective cues.
So he'd always have a clock around, to see how many minutes have passed and acted based on it.
**Fernando**, in the other hand, taking cues from his partner when having a bed game.
Fernando's focus on whether or not the other person is feeling good.
While Daniel approached it more methodically.
He'd arrange their bed game in time marks, to know when to change positions, when to advance, and when to prepare for finishing.
As a result, sometimes, when Fernando is being too good with his moves, Daniel lose track of time.
And when Daniel is being too good with timing, Fernando lose track of the emotional cues.
There ought to be mishaps, but they're both know they wanted the best for each other.
So, often it ended in a laugh.
They had their earlier years fighting a lot, but as they grew older, they tolerates it, as a part of their humanity.
