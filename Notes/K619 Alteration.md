# Alteration 20200619

## MTF Samsara membership of Manov
There is an inconsistency between Xe-2 Part 11 and Part 13.
Part 11 said Manov is already an active member of the [MTF Samsara](Notes/MTF%20Alpha-47%20Samsara%20c.2027.md), while in Part 13, when they arrive, Ket had to persuade Chan to let Manov and co. enter MTF Samsara.
To resolve that, I think it is more sensible that in Part 11, Manov get the information that he had a counterpart in Integra from Ket.
This possibly gives us a hint that they're at least still communicating.

## Fernando is the first in his extended family to study abroad?
Obviously, Fernando's extended family spans to the Korea as well, especially with the existence of Kang Hae-In and Yang Seung-Min.
I oversaw that aspect of him.
Therefore, this detail must be removed.

On another note, perhaps Kang Hae-In and Yang Seung-Min are koreans that are naturalized when they're little.
So they're, for all purpose and intent, are naturalzied Indonesian.
They might live in Kampung Cia-Cia, Buton, so they can keep using their korean name there.

## About etoan on Earth, do they need to be very secretive about it?
I think, etoan by themselves didn't really care if they get known.
We humans didn't usually observe secrecy when we invaded wildlife area anyway.
They might need to observe our society in secret at times, but most of the time, there's simply no need.

Likewise, there's simply no need to tell us either.
So most of the time, they'd just carry on.
Unless someone asked them specifically.

But Divine Council of Earth ruled out that media exposure of etoan on Earth must be supressed.
That's why they're protected with the same protections as reflexiors here on Earth, the ignorance field.
Hence most humans wouldn't really bother to ask them.
Some were even provided with fabricated backstories.
They'd have to adhere the local rules anyway, as they're in a foreign world.

However, the Denefasan first wave is something that can not be easily removed from human history, so it gave a chance to turn the table around.
The Archangels, the Denefasan biosoldiers, and The Four Horsemen, they fell into the public face.
This means, Ashton might picked it up as a chance to just roll with openness.

After the event, they'd be known by all of their classmates, and schoolmates, and teachers, and some even recorded it.
It'd be hard to just erase all of that without providing an event gap between them trapped at school, and them able to evacuate it.
Then, most likely, the Divine Council would just let it be.

Then, after the event, especially the Four Horsemen, they'd be public figures.
Some sort of celebrity, I think.
Their popularity might be equivalent to young idols.
Some would believe in their story, some wouldn't.
Some tied it to the conspiracy theory.

I must say, it is quite similar to the approach of the Avengers.
Especially Mr. Stark.
In this modern age, it is almost impossible to hide things like that.
Already, it is good enough that The Archangels uses full body armor, that they're not immediately recognized.
But Ashton and co., they'd be famous.

The Intelligence Agency would also noticed it.
Most likely, they'd piggy back on it, to turn the public attention focussed on those young celebs.
Perhaps that's one of the reasons they made Four Horsemen to join as a task force.
The Four Horsemen are their public face, public front-end.

Perhaps it starts at around Xe-1:19.
Because at around that time, our focus is turned to David, Michelle, and Anthony.
Ashton would be conducting a livestream concurrently.
Ashton might probably confirm that they know The Archangels.
He did not reveal who the Archangels are though, especially telling them, "one of them is not in a bad shape," referring to Steven.

When asked by David, Ashton said that it is better to step on the public face to control what they might understand of what happens.
Otherwise, by keeping public in the dark, it'll result in a wilder speculation and things.
People always seeks to fulfil their thirst, especially on what they don't know.
It might snowball into something completely unexpected. So, in an extraordinary event like this, secrecy isn't the wisest thing to do.

I'm going to assume that Ashton is experienced enough to handle this, because he literally is older than the entire human civilization.
He must know about crowd management, and trends in gossips, etc, over the span of millennia.
I also assume that what Ashton is saying is true, that if they keep it in secret, public would have split their opinions, into pro-us, and con-us.
So in the battlefields, some would trust them, some wouldn't.
The result would be catasthropic, and chaotic, and uncontrollable.

By appearing in public, it makes public more willing to hear their side of story, or at least they'd hear about it.
It'll make their actions more transparent, and more accepted by the public.
It is particularly because they earned the public's respect, by also respecting the publics.
