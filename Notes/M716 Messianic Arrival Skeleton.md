---
title: Alteration
subtitle: Messianic Arrival Skeleton
author: Hendrik Lie
date: 16 July 2022
fontfamily: times
fontsize: 12pt
linestretch: 1.25
indent: true
papersize: a4
numbersections: true
secnumdepth: 2
tags:
  - messianic-arrival
  - skeleton
  - inspiration
  - alteration
---

Original:

What if [Hendrik](/Characters/Hendrik%20Lie.md) did another metagenesis to provide a graft to reconstitute [Os](/Characters/Daniel%20Ashton.md) as a virtual entity using Hendrik's soul shard? Then it explains why Calvin had to meet Hendrik. It isn't just so that Hendrik would help [Steven](/Characters/Steven%20Pontirijaris.md) in the future, but also so that in the event of Cal's failure to leave [Tony](/Characters/Anthony%20Matthias.md) earlier, Os on Aucafidus can provide support to reset his state and start over? Hendrik is an insurance that Os' role could stay on its track.

Then the birth of Daniel Ashton is simply a merger of Hendrik's soul shard that contains Os, and Charles' body, making Ashton a human body with an Etoan soul. Making him whole. Or maybe the shard is simply a mean to facilitate the merge.

Also it simplifies what they're going to talk about on Aucafidus between [Leaf](/Characters/Leaf%20the%Solenadactilian.md) and Os. Leaf is going to explain the encounter of Hendrik and Calvin to Os (as Os left Earth before Calvin era), and why is it important on making an entrance back to Earth. Therefore fixing Hendrik's role to help, and helping us to get a viable content to the story.

Aim:

We can have an outline then, for the rest of Part 2.

1. A glimpse of Os' life in Aucafidus. It was paradise. Then [Leaf](/Characters/Leaf%20the%Solenadactilian.md) brought him for "the talk." Followed by a flashback of Calvin's encounter with Hendrik. It ends with the realization of what makes Hendrik special in the first place.
2. Hendrik's past in high school, as Calvin is helping Hendrik to hone his meta manipulation abilities. Hendrik just wants to have a friend. Turns out Hendrik misuses the power he had, and complained to Calvin. Calvin was mad, and realized that it is about time for him to move on.
3. Returns to Aucafidus, Os wanted to confirm what he learns from Leaf: Hendrik is the key, but the key of what?

Here's an entry for Hendrik's background story:

Hendrik was the only mage in his horizon of social circles. Hendrik was just a lone reflexior that didn't have any direct lineage to reflexior society in general. He's just lucky enough to withdraw a genetic lottery that grant him an activated reflexior cluster. He's not registered in the WTF, or EPL, and he had to self-study his weird psychic abilities.

To explore his psychic abilities he studied lucid dreaming and astral projection. One way to achieve that is via studying lucid dreaming, and eventually, astral projection. And then, communicating with your meta, that is, the soul subsystem, usually the part that connects you and the RFL network. Most reflexiors did reflexius act without thinking, but via lucid dreaming and astral projection, Hendrik is able to communicate to the intermediary layer between his physical body and the RFL.

He's quite isolated because he finds it hard to relate to other human beings, hence the loneliness. Until his second year, around July 2011, a freshman came by, his junior. His name is Calvin Gauss. Calvin came to notice that Hendrik is different, especially after Hendrik tried to communicate with one of his friend on why he's often spaced out. In his conversation with Hartanto (Hendrik's classmate), Hendrik communicated about his secondary personality. Calvin immediately recognized it, that Hendrik is describing the experience of having an exoself.

Calvin realized that it was SOUL subsystem of RFL, and not everyone can perform it. With the knowledge that SOUL subsystem is basically analogue to his own exoself system, he assisted Hendrik on mastering it. Eventually, Calvin taught Hendrik the technique of soul shearing. Their interaction would explore the nature of RFL, and its interaction with mortals, drawing a parallel to etoan society. Even exploring the main difference between etoan and homo sapiens.

With the original intent of creating copies of himself to be his friends, he used the soul shearing to create temporary copies of him to do his deeds. But eventually, his copies refused most of his requests. Pissed off, he complained to Calvin, which was then furious, because he didn't taught Hendrik Soul Shearing so that he would create instant copies only to do trivial deeds in a disposable manner. He said by doing that, Hendrik is disrespecting himself, and dehumanizes his copies, no wonder they refused.

Then he added that he has another mission as an ambassador here on Earth, which means he'd have less time to contact Hendrik. And then two things happened:

1. Hendrik reflect on himself on how he treats his clones.
2. Hendrik realized that the one thing he wanted the most, a friend that could understand his conditions, is Calvin, and Calvin is going away.

**O330-1327:**
We fed that background into Gemini, and I record what I got back in the appendix. Please take a look at them.

# Appendix

1. [Conversation with Gemini about Hendrik's character](/CWG/O320-1329-gemini-hendriks-character.md)
