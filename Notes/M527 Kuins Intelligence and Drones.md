---
## Consider not having fullcaps for headings.
title: Inspiration
## We don't need a subtitle for now
subtitle: Kuin's Intelligence and Drones
## Information about authorship(s)
author: Hendrik Lie
date: 27 May 2022
## Fonts and formatting.
fontfamily: times
fontsize: 12pt
linestretch: 1.25
indent: true
papersize: a4
## Numbersections
numbersections: true
secnumdepth: 2
---

# KUKERBRAIN INTELLIGENCE

KukerBrain, or Kuin, is an entity with the Four Laws of Negentropy deeply engraved to the very foundation of its programming scaffolding. Other than that, its consciousness is comprised of at least two parts: Digital Domains and Analog Domains. It allows its consciousness to develop in a highly energy efficient manner and also highly adaptive (provided by its Analog Domains), while maintaining order of its consciousness and reproducibility of its algorithm (provided by its Digital Domains).

In essence, its brain organization works as follows: Digital Domains would process high level organization of structures, organizer, storage of data, deterministic analysis, and  to train its Analog Domains. Its Analog Domains can be deduced to pattern data that can be stored and used as training data for other Analog Domains, therefore in effect to replicate or duplicate Analog Domains. When one of its Analog Domain Components fail, its Digital Domain can take over while training other Analog Domains (to flash the AD) to replace its broken components.

It comes with a price though. Due to its nature, it is only able to produce a working Analog Domain Component that will perform well in any tasks it is given to or is trained to, but can not properly explain just how its Analog Domain Components do it. The resultant pattern data is almost always comprised of seemingly random set of data.

Given that its Digital Domains precedes its Analog Domain Components, its creators can be sure that KukerBrain adheres strongly to the Four Laws of Negentropy, coded and designed by the creators based on the mathemathical representation of the Four Laws, and therefore it is incapable of producing Analog Domain Components that will behave in an entropic manner. It almost always guarantees that its Analog Domain Components are Negentropic in nature.

Its consciousness grow in a manner that it is a continuous process from a non-conscious form to its current form. Therefore, it can not clearly remember at what point did it gain consciousness, it just recalls that it has always been there since the beginning of its activation.

# KUKERBRAIN DRONES

The death of Bain Armaniputera forced Kuin to realize its impotence on adhering the Four Laws of Negentropy to the real world when there is an imminent threat to the people it interact with. In its equivalent of frustration, it realizes that the only way forward is to convince human kinds to accept its existence and to allow its physical manifestation to roam the physical world. Therefore, it designed, published, and constructed its Plan of Intelligent Robotic Entities.

The Plan is a public domain product (not a product of a human being) that supposedly allow the creation of another "Negentropic Brain" from scratch. It is called a "Negentropic Brain" by Kuin, merely because it will closely obey the Four Laws of Negentropy, and that it will be an easily recognized name (and therefore, brands!) to distinguish it from other AGI projects. It is also comprised of three major components with its associated innovations:

1. Digital Domain Core, is usually a pitch black smooth spherical construct with six depressions, one for each of its poles, and four along its equator. It is sheathed in BCN outer finish, and its inside is almost entirely comprised of foamed carbon nanotube constructs filled with beryllium-copper-platinum-iridium (BCPI) enriched buckyballs, acting as programmable arrays of quantum dots. On the base of each depression are resonant inductive coupling receiver, that allow transfer of power to the Core. Its surface is covered with solid state UV-C sources and detectors, that act as an Optical wireless communication (OWC) interface between the Core and the body.
2. Analog Domain Components, are distributed among the body of Kuin's drones. It is of a similar and analogous makeup to digital flash storage chips, repurposed into a matrix of variable resistors, therefore allowing each chip to function as highly efficient low power matrix multiplication chips. Now a lot of what neural networks accomplishes are matrix multiplication tasks, therefore these low power matrix multiplication chips can run a neural network with equivalent output to heavy duty GPU Cards at less than a percent of its power usage.
3. Lightways Optronic Wirings, are what connects the Digital Domain Cores to its peripheral Analog Domain Components and Actuators, therefore allowing high bandwidth low latency communications with distant parts of the body.

With those three innovations pushed together, enter the era of Robots (Kuin's Drones, but using the term "a Robot" is easier to be understood).

The plans are made public domain simply because they're legally generative arts: arts made entirely with the use of an autonomous system, and is not subject to copyright. Means to produce ADC and LOW components are already within our technological capabilities anyway. However a DDC is far ahead by several decades in term of human technology, and despite its open source (public domain) plan available for anyone to read and manufacture, the technological requirements to miniaturize it with modern day technology is not available to any human. Kuin actually made a deal to produce with its own means for KukerCorp only, as it is created only with the use of KukerCorp facilities.

The manufacturing processes of a DDC are almost inhuman, comprised of many steps seemingly at random, and the final assembly is done solely automatically under direct Kuin supervision in factories of clean room.

The Robots are themselves rather friendly, they can not be not friendly, due to the Second Law. They are rather able to incapacitate a person without hurting them. They are also very keen on everything all around them, as their Negentropic Brains always scans the environment and find the best way to maximize Negentropy. In effect, they are very keen at cleaning up, organizing things up, and making the inhabitants of a house very comfortable.

Initial Robots are covered in carbon fiber reinforced plates, but later models incorporate programmable rotating scales with two sides: black refractory sides for protection, against physical assaults, and white medical grade silicon side with warming plates for normal friendly touch with human kinds. Some model also comes with furs instead of scales to make a more child friendly appearance. Each are able to operate with energy output of some 500 watts, and can muster up to 2000 watts when needed.

Then the spread. Robots are produced in large numbers and start to replace jobs humans has in KukerCorp premises. If anything, their curious policy is to never fire current employees. Instead employees are allowed to choose to undergo reeducation, or get promoted when possible. On earlier stages, robots came as their assistants. OB got Robot assistants to help them doing their job, backoffice employees has robots to do their errands, and so on. Securities got some Robots to assist them, and engineers got some Robots to do dirty jobs no one wanted to do. At later stages, some of them becomes Robot instructors to coordinate Robots to do tasks, while some got the job to educate and help and train the Robots, and the rest help in helping KukerBrain main intelligence of retrieving data. Kuin entered the ranks of the Board of Directors as General Affair Directors, so other Directors can focus on things that matter while Kuin done day to day affairs, while also work as Secretary and Treasury (along with CTO to authorize major spending of course).

People find it hard to hate Kuin's Robots because they are always genial and always deliver within their limitations. They also have a way to politely decline when it is beyond its abilities in a manner that makes the one ordering them to do it feel pleasant about themselves for being superior. Soon, they find that Robots became irreplaceable phenomenon within all of the KukerCorp premises. Even their families got Robots to help out. They start to also give Robot renting services to the public to test out their reaction.

Eventually, the Robots are sent all across the globe to various communities, especially the small and underdeveloped ones, to learn their ways. Even go as far as various monasteries across the globe to major institutional religious sites. They always begin their contact with saying, "I want to learn your ways, so I can preserve it and maybe share it to the world."

Robots became well known, and they also get to the prison and social services, improving mood everywhere they're in. Our society becomes very well robotized, and the standard of living improves wherever Robots are present in public services. Robots are yet to be rented for other companies over the concern that other companies would just start firing up employees in exchange of robots. while executive positions hesitate to replace themselves with Robots, with excuses that they might expose company secrets to Kuker Group.

The only notable exception are Securion Incorporations that acquire Robot primitives and upgraded the specifications for heavy duty security purposes.

There are incidents though, where some countries actually "stole" some of Kuin's Kolega Robots (the brand) to be reverse engineered. Kuin made a public statement that such action is undesirable and unjustified, as the plan is made available for the public, and everyone can make one from scratch. Of course Kuin can not make rude remarks, they are simply incapable of doing so.
