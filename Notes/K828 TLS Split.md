# Alteration 20200828 TLS Split

Apparently, that's the first part of the TLS.
It might be longer than expected, TLS must be split.
How?

1. TLS:Overtime.
2. The Lost Son.
3. TLS:Messianic Arrival: The first part of The Birth of Daniel Ashton would be included here.
When Charles Lee came to be, Os's original copy was sent there to Earth.
But here, the right to own a body was held by his current copy: Charles.
So he remained simply a volant, he couldn't do much this way, so he seeks to contact Charles via his modified meta, and he is able to manifest in Charles's dreams.
4. TLS:Offers in a vacation.
Charles was enjoying his vacation, when an old memory of him came to his dreams, bothering him with a new mission that he doesn't wish to participate.
Then Fernando came, and Tee talked with him.
