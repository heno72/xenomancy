# Alteration 20200728: Febrian and Elbert

Febrian is born in 2009, both in Integra and in Xenomancy.
So Elbert must be at least two years older, born in 2007.
So, Elbert must be a junior to David.
Febrian, in the other hand, is a senior to Peter.

During the Battle of Kendari, 2022, Elbert is 15 years old, and Febrian is 13 years old.
Elbert is a first year high school student, and Febrian is a second year junior high school student.

In 2027, close to the Integra event, Elbert is 20 years old.
and Febrian is 18 years old.
They're still in college.
