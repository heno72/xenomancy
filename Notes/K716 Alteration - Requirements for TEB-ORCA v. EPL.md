# 20200716 Alteration: Requirements for TEB; ORCA v. EPL

TEB must be able to show:
1. KIA and a gist of its organization
2. One of the mission to find Aditya
3. First encounter of Henokh and Derictor with Reflexiors
4. On their way to track down the Reflexiors and/or Anthony, happen to encounter an attack from a Paramundus.
5. Meet Heinrich the Angel.
6. Have Hendrik along with the entire 5th Division.

The goal is that, to show, that KIA exists, is a sufficiently big organization, and currently they're on the lookout of Aditya.
On their path to find Aditya, they stumbled upon WTF v. EPL war, WTF v. Jagadlangit, and got WTF's attention.
So it also serves as an introduction of both KIA and WTF.

Meanwhile, given that Nurhayati Maulidia is no longer the girlfriend of Bright in Xe (where they are in Integra), what if she's currently assigned, per order by the DoC, as they reviewed possible new movements from DoS, to remain on where EPL might be?
It was chronologically, way back before she meets Tee.
Wait, I need to establish the timeline first.
When is it that they meet Anthony?
By 2018 Anthony is 16 years old.
If the attack with Paramundus Jagadlangit is to occur, it must occur prior to 2016.
Anthony is 14 years old.
It might work.

So set the time to 2016 for the requirement no. 3.
At that time, Nurhayati Maulidia is 20 years old.
She must be a junior officer, recruited by talent hunter of the KIA.
Tee, is about the same age as Anthony, apparently.
Depending on the date, in 2016, Ashton is Migun of Geng ORCA (until May 2016), or just starting out as Alfa, overlapping with Xiangyu's life, and fighting EPL.
When does the incursion of Tengu of Jagatlangit to Jagatpadang?

There's no mention, it is just said that, "as recent as 2016."
Then, any time is fine.
I think, it is most sensible when Migun is still around.
So as early as March 2016.
The attack from the Tengus might be on its early time, so Barong Ket, Barong Bangkal, and Barong Macan are still around, but not yet meeting Manov.
Manov is still studying in Mahatirto.
So it leaves us with Heinrich.
Perfect.
That means Xiangyu would be there as well, because he's also quite adept at dealing with volants.
But Xiangyu would be yet to encounter Alfa.
What changes after meeting Alfa?

How is it related to Henokh's attempt to meet Aditya?
Perhaps, just perhaps, it is related to Gang ORCA.
Gang ORCA gains traction first in term of relationship with EPL, especially as the leader of the Gang ORCA, is an Ambassador, his presence would be detrimental on EPL's movements, as Migun can just cancel their magic.
And Gang ORCA, promoting diversity and order, is antithesis to equality and chaotic nature of EPL.
Gang ORCA also trained some reflexiors, with Migun's style teaching.
Perhaps, the battle at the concert is not about EPL v. WTF at all.
It was a battle between EPL and Gang ORCA.
Perhaps Gang ORCA is affiliated with WTF, or is an organization under the WTF law.
I think the approach of Gang ORCA is to train young areflexi children to realize their Reflexius Cluster at their early age, a more benign EPL-like movement that WTF allowed to exist, as a form of control.
Fighting fire with fire.

## ORCA: Organized Regiment of Cooperative Associates.
It makes Ashton/Calvin's first encounter with Hendrik meaningful.
Because through Hendrik, he realized that humanity can be trained to have etoan-like brain activities, through rigorous use of memetic tools.
With mental toolkits.
Hendrik uses lucid dreaming as his media of mastering his own meta, and eventually, Soul Shearing.
ORCAs uses sensory association akin to synesthesia, to expand their waking perception, and hence can do sensory feats no human can achieve normally, as they tap into RFL, should Migun's style be practiced in depth.
Eventually connecting to their meta, and can exercise more control to the environment.
Some dubbed it Kanuragan style, though Migun hated that terminology.

EPL is in a war with ORCA at that particular area, apparently, at around the same time WTF is at war with Jagadlangit.
So ORCA is like a special task force to deal with EPL, while WTF focusses more on Jagadlangit.
And KIA, happen to try to fit in to the ecosystem of esotericism, of shadow governmental bodies.

Who are those that are fighting?
Dominic Muerte (contemporary to Hendrik, born November 1994), would be the rough guy.
Ezekiel Tanputra (born 2001), would be the boy that fought him.
So Ezekiel Tanputra is somewhat related to Migun.
He's Migun's junior in Migun's school.
A first year student where Migun is a third year student.
Migun's trusted aide.

Juan Muerte (born 1968), Dominic's grandfather, might actually still exist, and he's on EPL.
His son, must be the guy that reports to Anthony, in Angel's job.
What would be the name of his son?
Juan, Dominic, Roland?
Roland Muerte, born 1988, is the guy that reported to Anthony.
Roland, is an areflexi, an outlier in their family.
Could Juan be the one that introduced Keenan Ignatius to Aditya and Anthony?
Perhaps.

**Edit 20200812:**
Roland Muerte, full name is Roland Arwin Muerte.
He was often called Arwin. 

Stitching them together, should Migun's story be shown?
Either concurrent or after TEB.
Remember, Aditya found Anthony at around 2008, so Anthony must be about 6 years old.
In 2009, he's enrolled to the EPL for his study on reflexiology.

## TEB split

I think TEB must be divided into 2: The Silent Concert and The Orca-strated Event.
Also, The Boy From The Wood could be a part of this TEB or it predated TEB.
Actually, it predated TEB, as most of TEB occured in 2016, while Aditya meets the boy at 2008.

TEB:TSC must be able to show:
1. KIA and a gist of its organization
1. One of the mission to find Aditya
1. Tension between ORCA and EPL first erupted
1. First encounter of Henokh and Derictor with Reflexiors
1. Chandra is taking over on Finding Aditya mission.

TEB:TOE must be able to show:
1. Tension between Paramundus Jagatlangit and Paramundus Jagatpadang erupted to the real world.
1. On their way to track down the Reflexiors and/or Anthony, happen to encounter an attack from a Paramundus, and meet Heinrich the Angel.
1. Manov's encounter with Barong Ket, and his departure to finish his study.
1. Have Hendrik along with the entire 5th Division.

### TEB:TSC
TEB:TSC must be able to show:
1. KIA and a gist of its organization
2. One of the mission to find Aditya
3. Tension between ORCA and EPL first erupted
4. First encounter of Henokh and Derictor with Reflexiors
5. Chandra is taking over on Finding Aditya mission.

Let's explore how each points are to be delivered.
1. It can be performed by first showing the beginning of their creation, like it was in the original TEB draft. Then to get a gist of the operation, we can start with Derictor and his conversation with Nurhayati, a new recruit recommended by Haein. Nurhayati explained that she's currently doing a mission where she must infiltrate a new movement that DoS detected recently. It seemed to be related to the 2015 Surabaya Bombing, with a lot of inconsistent accounts. She's meeting Derictor because, apparently, during her investigation, she discovered a name that she knew Derictor and Henokh mentioned: Aditya Wijaya. She wished to discover more about Aditya, but Derictor said that there isn't much lead to find him. She said that apparently, he had a son, Anthony, and that she knew at least one person that knows Anthony. Derictor asked, a son? Nurhayati handed him a brochure, about an upcoming religious-like gathering. She said that if Derictor knew more about it, she wished Derictor would inform her, Derictor didn't say anything. She said she'd have to go, as she is meeting her other contact soon.
2. Derictor came to Henokh with the brochure, and passed the information Nurhayati gathered. Derictor said he'd ask Anderson to join him on the search of this contact. Henokh said no, he and Derictor would go together. Henokh pleaded, and Derictor agreed.
3. This part would highlight the separation between Migun and Dominic post the establishment of ORCA. The separation leads to the conflict in 2015 Surabaya Bombing, where Kiel, Migun's apprentince, fought Dominic. At last, Dominic is being tracked by Kiel, and they fought in the middle of the concert.
4. The other half of their battle is witnessed by Derictor and Henokh in the middle of the concert. Because Dominic is so fierce, and wouldn't submit, Kiel is forced to use lethal means.
5. Chandra was mad because Derictor didn't confirm the mission with him, and that Henokh took an initiative to do a solo mission. Henokh is from DoC, he shouldn't be up at the field, it is Chandra's job. Chandra said he'd take over the mission, but Henokh must not interfere, not anymore.

### TEB:TOE
TEB:TOE must be able to show:
1. Tension between Paramundus Jagatlangit and Paramundus Jagatpadang erupted to the real world.
1. On their way to track down the Reflexiors and/or Anthony, happen to encounter an attack from a Paramundus, and meet Heinrich the Angel.
1. Manov's encounter with Barong Ket, and his departure to finish his study.
1. Have Hendrik along with the entire 5th Division.

Let's see how it all unfolds:
1. Supposedly the same as Ket's backstory. It was Tengus from Paramundus Jagatlangit that decided to expand their influence beyond a single Paramundus. They were involved in a war. Ket is recruited with his entire Band to deter the incoming Tengu forces. It turned out, they reached the heavily guarded Jagatpadang through a Jagatpadang's hub on Bali. So to cut off the support lines of the Tengus, Ket and his Band had to transverse to the physical plane. The fight erupted as Tengu got cornered and had to materialize in the physical plane, specifically atop a highrise building where a concert is being held. The performers, shocked, decided to give their best and continue performing. Heinrich the angel and Xiangyu was present, they're there to enforce the wildlife preservervation: to protect humans from this war. The humans however, consider this as a part of the show, and cheered on it, given that they're protected from the damage.
1. Given that one of the performer was a contact that could help them get to Anthony, the Intelligence Agency task force was at the concert. They're alarmed to discover the clash between the Barongs and the Tengus materializing on the concert. However they saw a person and a polar bear in a uniform by the edge of the room, doing something seemingly supranatural. Surrounding them are a mixture of people in black cossacks, and people wearing tuxedos and black-white masks. All wore black high hats. The IA Task Forces tried to approach them, but the crowd is unforgiving. The polar bear, Michael, Hendrik, and Heinrich appeared to go to the VIP section after the otherworldly battle disappeared. So the task force followed, and overheard the conversation between the Major and Heinrich. Heinrich said "we're sorry for the inconveniences." Manov manifested with Purpose and Pride, the stripped dolphins. Manov healed the Major, as he's suffering body shearing after the attack manifested near his seat, and afterwards, he and the dolphins disappeared into thin air. Heinrich gave a card to the Major, a WTF contact card. Afterwards, he and Xiangyu went out of the premises, but realized that they're followed by an unknown armed task forces. Hendrik casted something and the IA Task Forces suddenly petrified in horror. They literally couldn't move. On his hand was a spare WTF Contact Card, so he provided it to the team leader of the IA Task Force. Then the WTF and ORCA joint task forces left the premise. Only after that, the IA Task Forces regain their ability to move. They just gazed at the WTF Contact Card.
1. ***(I'm not sure if this part should be included)*** Manov and Pride went to the Paramundus Jagatpadang, and discovered that Ket is almost died, and the Tengu forces already killed Barong Macan and Barong Bangkal.
1. The Contact Card was brought to the DoC of the IA, and in the middle of the meeting, the card is activated. Hendrik and Michael entered the room that's being heavily guarded without anyone noticing. He started by saying that they're offering a joint operation between the IA and The Oneiric Task Force. OTF, Hendrik said, offered a chance to deal with esoteric enemies, as currently the Intelligence Agency couldn't deal with esoteric opponents, as demonstrated in the latest attack on the concert. Chandra recognized Michael, and he told Henokh, that Michael is known well among his peers, a member of the Clandestine Esoteric Coalition of Southeast Asia that occasionally would help the military of various Southeast Asia military on dealing with supranatural phenomenon. It is hard to believe, but they are able to do their job just well. Anderson said that they should have esoteric wings in their organization structure, just about time he wanted to mention that. Henokh wasn't sure what to do. Months later, the 5th Division of the DFO has already been established, and a special task force dedicated for extraordinary feats was prepared: The Nine Tailed Fox. Henokh reasoned that if they couldn't control the esoteric members, he'd at least align with them so they wouldn't have much reason to disturb against the IA.
