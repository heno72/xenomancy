# L330 Apex Souls

| Soul | Incarnations | Abilities | Uses | Alignment |
| --- | --- | --- | --- | --- |
| Vishnu | <ol type="1"><li>Elbert Hardiman (m)</li><li>Lee Ah Ping (m)</li><li>Liong In Chuang (m)</li></ol> | Preserver, the ability to force normalcy on his immediate vicinity. | A part of the SVRA System (Sperry-Vishnu Reality Anchor System). His blood and cultured tissues can force normalcy to a limited extent when excited. | GFE; rogue |
| Brahma | <ol type="1"><li>Hendrik Lie (m)</li><li>Michael Carmichael (m)</li></ol> | Creator, has the ability to freely shape reality, as long as he knew the intended outcome in great details. Costs him a lot of headache to manifest complex reality bending phenomenon. | As Michael, was a part of the MTF Nine Tailed Fox. | GFE (former); unaffiliated |
| Shiva | <ol type="1"><li>Dominic Muerte (m)</li><li>Juan Muerte (m)</li></ol> | Destroyer, able to annihilate objects or people to nothingness, provided that he concentrated hard on what to erase. | One of the major striker of the Butterfly Initiative. | Butterfly Initiative |
| Boson | Nurhayati Maulidia (f) | Bosonic Force-Carrier Manipulation. Can control the flow and states of boson particles. | An intern in a public notary office, never knew about her power until she had to face Tiamat. | Unaffiliated |
| Fermion | Zarah Walidah (f) | Fermionic Particle Manipulation. Effectively telekinesis. | One of the major striker of the Butterfly Initiative | Butterfly Initiative |
| Tachyon | Bright Spears (m) | Nonlocality. Has the ability to cause discontinuity or transcontinuity in space-time. Can shift from a location to another location in integra by traversing the *Antarabhava*. | Just a normal prosecutor that sometimes ran away with his secret power. | unaffiliated |
| Sperry | <ol type="1"><li>Febrian Gautama (m)</li><li>Chen Yun Fei (f)</li><li>Zhang Kim Fa (m)</li></ol> | Interface-level brain manipulation. Can alter the state of consciousness of someone's soul that interact with their body. Results in altered state of consciousness to his subjects. | A part of the SVRA System (Sperry-Vishnu Reality Anchor System). His blood and cultured tissues has amnestic and tranquility qualities. | GFE; rogue |
| Erickson | Ezekiel Tanputera (m) | Object-level brain manipulation. Can override the command chain of one's soul to their body. Effectively, the ability to dictate what his victim should be doing, and they wouldn't be able to resist his command, even though they didn't want to do it. | has no use in this life, he hasn't develop it yet. | unaffiliated. |
| Freud | Tere Luxury (f) | Soul-level brain manipulation. Can alter the soul itself. Has the ability to read and write thought processes to her subjects. | She's using her power mainly to help her career as a public figure. | Uh, freelancing? |

# Mobile Task Force Alpha-47 "Samsara"

A task force of the **Department of Normalcy**, **Global Foundation of Esotericism** (GFE).
Specialized in dealing with the Apex Souls.
In 1993, the [MTF](/Notes/MTF%20Alpha-47%20Samsara%20c.1993.md) was led by Sergeant Chandra Watthuprasongkh.
It was a squad composed of three Fireteams.

## 1st Fireteam: The Iron Fists

| Name | Position | Characterization |
| --- | --- | --- |
| Sgt. Chandra Watthuprasongkh<br>(m, 26)<br>P'Chan | Squad/Team Leader | Highly determined to do his jobs, very friendly and accommodating, and very attentive to his team. However he's very emotionally attached to his squad, that losing a squad member is almost always devastating for him. |
| Pvt. Go Jiaota<br>(f, 23)<br>Ms. Go | Automatic Rifleman | A calm, quiet, and stern woman. Very organized, and hate mess. Always precise and very determined to finish whatever she's starting. Wouldn't twitch for every soul killed, that is ironically very trained in medic. |
| Pvt. Wang Shidi<br>(m, 24)<br>Wang | Grenadier | A highly reliable wingman, however very disorganized. Does not hesitate to jump and save his colleagues, even when it may cost his life. |
| Pvt. Djunita<br>(f, 24)<br>Djun | Rifleman | A bright and cheerful lady with plenty of bad jokes. Always try to get close with everyone, and never intend to hurt anyone. |

## 2nd Fireteam: The Wizards

| Name | Position | Characterization |
| --- | --- | --- |
| Pvt. Michael Carmichael<br>(m, 24)<br>Mike | Team Leader | The only Apex Soul in the squad, a Brahma Soul. An agile reality bender, that can perform small-scale minor reality bending relatively quickly. A hard and wide-scale reality bending requires more time of focusing and might cost him headaches. |
| Barong Ket<br>(m, ?)<br>Ket | Automatic Rifleman | An extrauniversal agent from Paramundus Jagadpadang. His human form is a lean but strong south asian male martial artist, and his beast form is a male lion. Specializes in aural attack, projected energy, and shielding techniques. Can teleport, but only toward a spot in his direct line of sight. Personality wise, he's playful, easy-going, and prefer to do things in a way that maximize experiences. |
| Barong Bangkal<br>(m, ?)<br>Bangkal | Grenadier | An extrauniversal agent from Paramundus Jagadpadang. His human form is a heavily built aryan male, and his beast form is a Bengal Tiger. Specializes in strength and brute force. Generally very stoic and always face things head-on. Hard to amuse but couldn't resist food. |
| Barong Macan<br>(m, ?)<br>Macan | Rifleman | An extrauniversal agent from Paramundus Jagadpadang. His human form is a javanese male, his beast form is a Bengal Tiger. Specializes in clairvoyance and navigation. Can teleport in rapid succession and is very precise in doing it. Personality wise, he's neat, always grooming himself, and very proud on how he looks. He's very focused, but often missed out details he didn't focus on. He doesn't care about how you looks, but very care on how you smell. |

## 3rd Fireteam: The Harem
| Name | Position | Characterization |
| --- | --- | --- |
| Pvt. Kang Hae-In<br>(m, 20)<br>Hae-In | Team Leader |  |
| Pvt. Angelica Gears<br>(f, 22)<br>Gears | Automatic Rifleman |  |
| Pvt. Ninawati Berlina<br>(f, 21)<br>Watt | Grenadier |  |
| Pvt. Emma Bright<br>(f, 20)<br>Emma | Rifleman |  |


# Xe-2 Outline

## Planned Part Structures

| Parts | Chapters | Part Name | Description |
| :---: | :------: | --------- | ----------- |
| 1 |6 | The Red Thread of Fate | Reunion of a Vishnu Soul and a Sperry Soul. It opened old wounds that stained their fate together. A new hope arises, as this cycle could be their last time in suffering. |
| 2 | 6 | The List | The list of people to kill was found by Hendrik. An investigation is initiated. Meanwhile Anthony and Ashton discovered a connection between Helena's death and their bargaining position with the Powers. Both problems brought their attention to an alternative Earth. |
| 3 | 6 | Integra | Henokh, Derictor, Manov, Steven, Daniel, and Fernando decided to take the risk and entered Integra. Henokh, Derictor, and Manov would investigate the list they found earlier, while Steven, Daniel, and Fernando would search for El. Little did they know the answers they're looking for is the exact same thing: the very reason of Integra's existence. |

## Planned Chapter Structures

### Part 1: The Red Thread of Fate

| Chapter | Title | Description |
| :-----: | ----- | ----------- |
| 1 | | |

### Part 2: The List

| Chapter | Title | Description |
| :-----: | ----- | ----------- |
| | | |

### Part 3: Integra

| Chapter | Title | Description |
| :-----: | ----- | ----------- |
| | | |
