# Alteration 20200813 About Ket's human name.

Ket Khemachat Thunyakorn.
Nick: เข็ด (noun: skein, verb: be afraid, be scared, fear)
Full Name: เขมะชาติ ธันยกรณ์

When Ket was asked for his human name in Integra.
Reason: Thai names because his favorite human is Michael, and Michael's favorite human is Chandra.
Chandra is Thai, and Ket as Thai name is not weird.
If it is from Michael's culture, what would he be called? Caitlyn? Catherine?

So Ket remained.
Khemmacat because it is another Thai names he recognized.
The same for Thunyakorn.
So he stitched them together, even if Thunyakorn is not a proper Thai surname.

Actually, Chandra did comment on this matter.
He said that perhaps the more proper name for Ket should be สิงโต (Singto), as it is "lion" in Thai.
Ket said that he's comfortable with his current name already.
Afterwards, Chandra called him Singto instead, and Ket didn't bother.

So, perhaps, more importantly, Ket's human form would be based on Ohm Pawat, that played as Highlight in Blacklist.
His main specialty would be his speed, resistance, and Lion's Charisma (some sort of shield).

Barong Macan's human form, would be that of Ohm Thitiwat.
His main specialty would be detection (long-ranged and short-ranged), and mobility.
Chandra called him ตะโก (Tako).

For Barong Bangkal, we'd need someone that's bulky and muscular.
He'd be pure muscles.
Chandra called him ถัง (*Thang*, Tank in Thai).
He's the only one among the three that Chandra named not after the beastform such as Macan and Ket.

## Edit O329-0028

Actually i asked Gemini:

> Hey, can you help me to understand the meaning of the following Thai name? เขมะชาติ ธันยกรณ์

Sure, here is the meaning of the Thai name เขมะชาติ ธันยกรณ์:

- **เขมะชาติ** (Khemachart) can be broken down into two parts:
    - **เขมะ** (Khema) means "safe", "secure", or "protected".
    - **ชาติ** (Chat) means "nation", "race", or "birth".
- **ธันยกรณ์** (Tanyakorn) can be broken down into two parts:
    - **ธันย** (Than) means "grain", "corn", or "wealth".
    - **กรณ์** (Korn) means "heap", "pile", or "collection".

So, the full name เขมะชาติ ธันยกรณ์ could be interpreted to mean something like "safe and secure nation of wealth" or "protected collection of grain".

It is important to note that the meaning of a Thai name can vary depending on the context in which it is used. For example, the name เขมะชาติ could also be interpreted as a reference to the Buddhist concept of "khema", which is a state of mental peace and security.

Ultimately, the best way to determine the meaning of a Thai name is to ask the person who has the name.

> What about the following thai nick name:  เข็ด

The Thai nickname "เข็ด (Khed)" can have a few different meanings depending on the context:

1. **Discouragement or Hesitation:** This is the most common meaning of "เข็ด" (Khed). It signifies a feeling of being discouraged or hesitant to do something again, often due to a negative experience in the past. For example, if someone was scared by a dog as a child, they might be "เข็ด" (Khed) of dogs ever since.
    
2. **Experience or Lesson Learned:** "เข็ด" (Khed) can also imply that someone has learned a valuable lesson from a past experience, even if it was negative. They might be "เข็ด" (Khed) enough to avoid repeating the same mistake.
    
3. **Familiarity or Being Used To Something:** In some cases, "เข็ด" (Khed) can suggest a sense of familiarity or being accustomed to something, particularly something unpleasant. For example, someone living in a noisy neighborhood might say they're "เข็ด" (Khed) with the noise, implying they're used to it, although it might not be ideal.
    

**Important Note:**

It's important to consider the context and tone of voice when interpreting "เข็ด" (Khed). While it can sometimes imply a negative experience, it can also suggest learning or adaptation.

Here are some additional points:

- **Informal Nickname:** "เข็ด" (Khed) is a short and informal nickname, often used among friends or family.
- **Alternative Nicknames:** There might be other Thai nicknames with similar meanings, such as "กลัว" (กลัว - Klao) which directly translates to "afraid".

If you're unsure about the exact meaning in a specific situation, you can always ask the person using the nickname for clarification.

