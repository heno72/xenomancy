---
## Consider not having fullcaps for headings.
title: Inspiration
## We don't need a subtitle for now
subtitle: Reimagined 2003
## Information about authorship(s)
author: Hendrik Lie
date: 22 February 2019
## Fonts and formatting.
fontfamily: times
fontsize: 12pt
linestretch: 1.25
indent: true
papersize: a4
## Numbersections
numbersections: true
secnumdepth: 2
---

Three men in a shared apartment.
Daniel is a son of a rich family.
Fernando is the first one among his extended family to study abroad.
And Steven is a son of a family that lives in a post-scarcity society.
They're best friends since high school and decided to study in a medical school, together.
Together they'd explore friendship, love, hate, and conflicts.

Support characters are one Uber rich boy named Henokh that study economy, a cousin of Daniel, then a sexually passionate but seemingly innocent Derictor that study law, and an athletic and competitive Alex that also study law, Steven's brother.
They lives next door to Steven, Daniel, and Fernando.

Lives in a separate door is Aditya, also study law.
He got occasional visit by his cousin, Helena, a class mate of both Fernando, Daniel, and Steven.

Now how should I portray their life?

The apartment is more like a shared house right at the corner of a cross.
It was a repurposed storehouse, the gate facing north.
At the East side were windows of the apartment rooms.

On the third level was three rooms, each with a double bed.
There was a kitchen behind, two bathrooms, then a dining room next to the stairs.
The kitchen was connected to a terrace where one can dry off their laundry.
There was an old washing machine.
After the stairs were two adjacent rooms, right beside a corridor.
At the end of the corridor is a bigger room with large windows heading to the north, other than another window heading to the east, as other rooms also have.

Fernando and Daniel lives on the biggest room.
Alex lives on the room next to the dining room.
Steven lives in a room between the other two rooms.

Similar lay out below, and living in the front room is Henokh.
Derictor lives in a room next to Henokh, and Aditya in a room next to Derictor.

There was just two rooms in the first floor.
One belongs to Nicholas, also on med school, and another belongs to Nicholas's brother, Victor Axel Armaniputera (on the script was Alexander), that study economy.

----

# CHAPTER 01: Blossoms

It starts with their first day at the University, they discovered that, as first inhabitants of this newly constructed dormitory, all of them are fresh year grad students.
The inhabitants of the third floor knows each other, so after they finished their preparation, they went down to that others too, are going to the university.
Daniel and Henokh greeted, then with Derictor, but when he greeted Aditya, Aditya didn't answer, and right away, clumsily ignored Daniel's greetings and eye contact.
Alex however noticed that Aditya wore attributes for the faculty of law, and decided to greet Aditya once more by mentioning that.
Aditya nodded, and they have some small chats.
Alex said he's going with Aditya.

They decided to just descend down.
On the first floor they meet another one of med student, Nicolas.
They asked him to go to their faculty together.
His brother come out of the bathroom at the time, right when Alex and Aditya were down, and called both that they should be going together, as he's too, a faculty of law student.
They agreed, and all med students went first.
(Victor and Fernando meets each other's eyes briefly)

It is unfortunate that Nicholas is assigned to a different team, but also fortunate that Daniel, Fernando, and Steven are on the same team.
Within the team was a girl known as Helena.
Daniel and Steven have their attention grabbed by her.
Helena, however, greeted Fernando first, that then promptly introduced her to his friends.
During break, the triad meet near the toilet to discuss this matter, and it is settled that Daniel and Steven are in a fair fight to win Helena.
Fernando declared that he's not interested romantically with Helena.

(Some games)

Back at their dormitory, Steven is with Alex, discussing about Helena.
He was dazed by her beauty, said Steven, on which Alex laughed hard, teasing Steven.
Fernando and Daniel were in their bed, laying and gazing at the ceilings, having some serious conversation about Helena: Daniel wish to be sure that Fernando isn't after Helena, and that Fernando didn't back down from her just because he didn't want to compete with his best friends.
Fernando, of course, wasn't interested with her, and he went to the first floor with one aim: to meet Victor.
Fernando waited there for some time, pretending to drink some water, and Victor come out of the bathroom, asking why is he on the first floor.
Fernando asked why wouldn't he? They carefully inspected one another, and then Fernando decided that it was too awkward, and he returned to the third floor.

The next morning, Fernando prepared fried rice for four.
Daniel was rather surprised, Steven and Alex thanked him.
Gazing at them for a few moment, Fernando returned their thanks and packed their food into respective lunch boxes.
First he gave the lunchboxes to Steven, and after some pause, he gave the other two to Alex and Daniel.
He descended down and see the others are preparing.
As usual, Alex come and wait for Aditya, and the triad picked Nicholas out.
Fernando was looking for Nico's brother but he's still bathing.
They left the dormitory to today's gathering point.

It was an introductory to the scoring system, history of the university, etc.
Daniel sit beside Steven, in front of him is Fernando, and in front of Steven is Helena, that sit beside Fernando.
Daniel observed Steven and Helena exchanging notes and laughs.
When they're done, Helena exchanged conversations and laughs with Fernando.
Daniel grow certain amount of anger to the situation, that he couldn't say anything during the lunch break.
After some conversation with Fernando, that basically said that Daniel should be more proactive, Daniel decided to be more active in engaging with Helena.

# CHAPTER II: Action

Few days later, Steven intended to ask her to a culinary tour, he had it prepared with his brother and his brother's new friends.
However when he asked her, she said that she's already been asked to go with Daniel instead for a movie theater.
She thought Steven's coming as well.
She said that Fernando is supposedly joining, and ask Steven if he'd come.
He said he couldn't.

Daniel asked Fernando for a favor: that Fernando will pretend to be sick, so that he can't go to the cinema, so that Daniel can be with Helena, alone, in the cinema.
Fernando said that he also wish to watch the movie, but Daniel promised that he'd accompany him at latter days.
After some thought, Fernando said yes.

Fernando decided to meet Victor instead, and asked him to watch a movie.
Victor asked back, why wouldn't he be with his friends, and he said that his friends already have other appointments.
Victor nodded, and asked, what about his brother? Fernando reluctantly said if Nico doesn't mind, then it is okay.
After further thoughts, Victor said that Nico is resting, and he doesn't have the heart to wake Nico up, as he's tired.
Fernando said that they should find some food first before watching the cinema.

Daniel went and meet Helena at the cinema, pretend to call Fernando and get him confirm that he's sick.
Helena bought that and they watch the cinema together.
However after the cinema, which played a horror movie, Daniel come out pale, to Helena's surprise.
Helena teased Daniel for a bit, felt bad, and tried to make him feel better.
After some conversation, Helena said that it would be fun to watch the movie with Fernando and Steven.
Helena told him how three of them were so solid and close.
That reminded him how he's abandoning his friends, and feel ashamed of it.

