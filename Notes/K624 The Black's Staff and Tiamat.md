# Alteration 20200624 The Black's Staff and Tiamat

Perhaps it wasn't Mot that needs to be amused to summon Tiamat.
Perhaps Mot would just give it to anyone but Yam.
It was the instruction anyway, never give it to Yam.
It doesn't say that Mot is not allowed to give it to anyone.
The problem would be how to reach Mot in the first place.

The Black's Staff would be the answer.
It was constructed by one of the native, incorporating a normal, ornamented staff, with an Orb of Hadad embedded in it.
It was the Orb on the head of Hadad's Seraph, that was taken by Yam's Seraph in the battle that Alt Parahomen witnessed.

Alt Parahomen, as Altair Oldman, handed it to the Securion Incorporated International for safekeeping.
However it was taken away by Aditya and his EPL Corps.
EPL Corps then use the staff to locate and open the gate to Mot's domain.

In Mot's domain, they requested for Tiamat.
Mot didn't object, but Tiamat requested a surprise for her to be summoned.
Something chaotic, something destroying the current order.
Must be large scale, and novel, in term of methodology, that could amuse Tiamat.
Also, she requested a female body to be her vessel.
She must be willing though.

At first it felt like it would contradict Angel's Job, that occured prior to the Artifact Hunt.
Fortunately it did not directly mention Tiamat, it just said, Chaos Beast.
It could be, at first EPL interpreted Tiamat = Chaos Beast.
But Anthony as an angel must receive a direct order from a divine first.

Perhaps, it was that, he was ordered to prepare and do the ritual that night, as Yam wanted to bluff Hadad.
That by doing so, Heinrich would be sent to intervene.
That, by Heinrich intervening, the following moment, Hendrik wouldn't be able to use all of his strength, as his quota is depleted by Heinrich.
Then, EPL could obtain the clues to the whereabouts of Black's Staff.
