# Oneironauts: Apex Souls

The nine apex souls that leads the **Butterfly Initiative**.

# L330 Apex Souls

| Soul | Incarnations | Abilities | Uses | Alignment |
| --- | --- | --- | --- | --- |
| Vishnu | <ol type="1"><li>Elbert Hardiman (m)</li><li>Lee Ah Ping (m)</li><li>Liong In Chuang (m)</li></ol> | Preserver, the ability to force normalcy on his immediate vicinity. | A part of the SVRA System (Sperry-Vishnu Reality Anchor System). His blood and cultured tissues can force normalcy to a limited extent when excited. | GFE; rogue |
| Brahma | <ol type="1"><li>Hendrik Lie (m)</li><li>Michael Carmichael (m)</li></ol> | Creator, has the ability to freely shape reality, as long as he knew the intended outcome in great details. Costs him a lot of headache to manifest complex reality bending phenomenon. | As Michael, was a part of the MTF Nine Tailed Fox. | GFE (former); unaffiliated |
| Shiva | <ol type="1"><li>Dominic Muerte (m)</li><li>Juan Muerte (m)</li></ol> | Destroyer, able to annihilate objects or people to nothingness, provided that he concentrated hard on what to erase. | One of the major striker of the Butterfly Initiative. | Butterfly Initiative |
| Boson | Nurhayati Maulidia (f) | Bosonic Force-Carrier Manipulation. Can control the flow and states of boson particles. | An intern in a public notary office, never knew about her power until she had to face Tiamat. | Unaffiliated |
| Fermion | Zarah Walidah (f) | Fermionic Particle Manipulation. Effectively telekinesis. | One of the major striker of the Butterfly Initiative | Butterfly Initiative |
| Tachyon | Bright Spears (m) | Nonlocality. Has the ability to cause discontinuity or transcontinuity in space-time. Can shift from a location to another location in integra by traversing the *Antarabhava*. | Just a normal prosecutor that sometimes ran away with his secret power. | unaffiliated |
| Sperry | <ol type="1"><li>Febrian Gautama (m)</li><li>Chen Yun Fei (f)</li><li>Zhang Kim Fa (m)</li></ol> | Interface-level brain manipulation. Can alter the state of consciousness of someone's soul that interact with their body. Results in altered state of consciousness to his subjects. | A part of the SVRA System (Sperry-Vishnu Reality Anchor System). His blood and cultured tissues has amnestic and tranquility qualities. | GFE; rogue |
| Erickson | Ezekiel Tanputera (m) | Object-level brain manipulation. Can override the command chain of one's soul to their body. Effectively, the ability to dictate what his victim should be doing, and they wouldn't be able to resist his command, even though they didn't want to do it. | has no use in this life, he hasn't develop it yet. | unaffiliated. |
| Freud | Tere Luxury (f) | Soul-level brain manipulation. Can alter the soul itself. Has the ability to read and write thought processes to her subjects. | She's using her power mainly to help her career as a public figure. | Uh, freelancing? |

## Father Cluster: White.
+ Brahma the creator.
Has the ability to cause alterations to the reality.
+ Vhisnu the preserver.
Has the ability to prevent things from changing.
+ Shiva the destroyer.
Has the ability to annihilate things.

## Son Cluster: Blue.
+ Boson the force carrier.
Has the power of controlling force carriers.
+ Fermion the builder.
Has the power of controlling fermionic particles.
+ Tachyon the anomaly.
Has the ability to breach the c limits.

## Holy Spirit Cluster: Green.
+ Sperry the master of material brain.
Has the ability to alter neurochemicals of one's brain.
+ Freud the master of consciousness.
Has the ability to deduce one's conscious line of thoughts.
+ Erickson the master of unconsciousness.
Has the ability to alter the conscious states of people.

# Recent Incarnations of Apex Souls
* Hendrik Lie (In) is a Brahma soul.
His ability won't manifest until very late of the story.
Uses the same method of lucid dreaming as Hendrik (Xe) to awaken his power.
Unlike Hendrik (Xe) that focuses on his quest to find someone that understand him, Hendrik (In) focusses more on theological questioning.
Like, is God exist, and if so, which is the right one?

* Elbert Hardiman is a Vhisnu soul.
At a base of GaFE.
Apparently his mind is vacated to the Manent Realm in his analog, so that his body in Integra is just a mere shell.
To reobtain the soul, one must kill him in Xe.

* Dominic Muerte is a Shiva soul.
He is one of the operative officer of the Butterfly Initiative.
He is one of the guys that helped Men of Hadad and Women of Tiamat.
He helped to chase down Bright.
His past life was Juan Muerte, his own grandfather.

* Nurhayati Maulidia is a Boson soul.
Is a tall woman that is having an internship in a notary office.
Is a girlfriend to Bright.
She knew Hendrik (In) as they were from the same universities (both Unair and Ubaya).

* Zarah Walidah is a Fermion soul.
She is one of the operative officer of the Butterfly Initiative, that chased down Hendrik (In), along with Steven and co.

* Bright Spears is a Tachyon soul.
A prosecutor, that loves to sing.
He likes to use his ability to run away from his co-worker for the purpose of having some free time with music that he loves.

* Febrian Gautama is a Sperry soul.
At a base of GaFE.
Like Elbert, his soul is vacated to the real world so that his body is empty.
Then thaumaturgist experts could control his body to recondition the neurological states of Elbert's brain.

* Tere Luxury is a Freud soul.
She worked as a trader agent and a famous entrepreneur.
She's very people-person, knew a great deal about the thoughts of others.
Can predict people's behavior, and she exploited it to sell her products.

* Ezekiel Tanputra is an Erickson soul.
He is a veterinarian student that lives in AM8, while having his univ on the other side of the city.
His cousin, Keenan Tanputra, is a friend of Hendrik (In).
Quite reserved, he rarely talks and spend most of his time with his friends.

* Unrelated to all of them, is Joseph, a native integra inhabitant.
He is currently leading an effort of the Butterfly Initiative to gather all Apex Souls, for the purpose of the Ascension.

## Vhisnu and Sperry
So, currently, GaFE held in their possession a Vhisnu soul and a Sperry soul.
Sperry is given the task to keep Vhisnu in sedation and had his potential activated almost constantly.
Vhisnu's ability is projected upon the so-called thaumaturgic plates, which is basically a titanium-iron-nickel alloy.
The alloy is engraved with certain microstructures that allows the omnidirectional projection of modulated lights upon a signal from Vhisnu's brain.
In essence, a reality anchor.
It is worn as a pair of gauntlet, that when positioned in a certain gesture, forces a field akin to the casimir effect, that signals Vhisnu to activate is ability via the alloy.
The result is a beam of light, from visible light down to gigahertz radio bands, that measures the environment.
The measurement is sent to Vhisnu's brain, that then enforces the state to not change at all.
Keeping everyting intact from any attempt to alter it.

To make detection easier upon the death of a Vhisnu or a Sperry soul, they performed a ritual to bind them, that, upon death, their souls are entangled.
Which makes their karmic matrix more likely for both of them to be born within the legacy of either of their families.
That way, tracking is easier, and then every newborn in either of their families are screened and closely monitored.
Usually, after a certain time, both found each other, and both of them could be extracted.
This is a cycle of about some 20 years of maturation and finding each other, and averaged 40 years of usable reality anchoring periods.
Followed by a month or so when they're given a subtle orchestrated chance to escape, chased down, and decided to do the ritual again to bind their soul, hoping for a better luck in the next karmic cycle.

Then the cycle resets again to maturation and finding each other.
It has been about that way since the last 5 centuries, as the technique is perfected by GaFE.
Therefore, it has been several cycles of engineered love between Vhisnu and Sperry, for the benefits of GaFE.

Unethical, yes, but the board of directors of GaFE needs them to maintain normalcy via the use of **Sperry-Vhisnu Reality Anchor System** (**[SVRAS]**, colloqially: the Shark System, as Sura is shark in Javanese).

The risk is high after the death of both Vhisnu and Sperry, as during the intervening periods, the Butterfly Initiative might capture it.
So every cycle is one of the most risky endeavour.
