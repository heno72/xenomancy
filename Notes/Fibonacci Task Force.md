# Fibonacci Task Force, or FTF.
David was inspired by his dad's Hexadecimal Ducenti Club (HD Club), where its members combined IQ score is two hundred in hexadecimal (512 in decimal).

There are two possibilities.
The first one is where David calls the squad as the Fibonacci Task Force.
He realized that their combined IQ is 610, the 15th number in Fibonacci sequence.
Here's the composition:

| Rank | IQ Score | Name |
| ---: | -------: | ---- |
| 1 | 185 | [Daniel Ashton](/Characters/Daniel%20Ashton.md) |
| 2 | 182 | [David Pontirijaris](/Characters/David%20Pontirijaris.md) |
| 3 | 125 | [Michelle Williams](/Characters/Michelle%20Williams.md) |
| 4 | 118 | [Zean Lisander](/Characters/Zean%20Lisander.md) |
| **Sum** | **610** | **15th Fibonacci Number** |

Alternatively, David would call their team the Prime Partners, as their combined IQ is 701, a prime number.
Here's the composition:

| Rank | IQ Score | Name |
| ---: | -------: | ---- |
| 1 | 185 | Daniel Ashton |
| 2 | 182 | David Pontirijaris |
| 3 | 169 | Michelle Williams |
| 4 | 165 | Zean Lisander |
| **Sum** | **701** | **126th Prime Number** |

I think, it is too wishful to assemble the entire team as a bunch of geniuses.
It wasn't normal, and even then we need to make the characters believably humans.
Given that Michelle Williams were reincarnated as a full human, unlike that of Daniel Ashton and David Pontirijaris, I'd favor more toward the Fibonacci Task Force.

## Their Weapons of Choice and probably fighting styles (also see document 20190916):

| Name | Nick | Weapon of choice | Note |
| ---- | ---- | ---------------- | ---- |
| Daniel Ashton | The Origami Knight | Ashton's Fight Suit | His armor comes in form of sheets extruding from his body, and folds in a manner people could only describe as origami folds. |
| David Pontirijaris | The Black Angel | [Midnight](/Characters/Midnight%20Ranensis.md) | Midnight is a tweaked version of Adran. It can reshape at will, but recurrent shapes are David's e-bike and The Twin Geniuses |
| Michelle Williams | The Pole Dancer / Sun Wukong | Ruyi Bang (Sun Wukong's Staff) | Ruyi Bang is a telescoping smartmetal pole, that could change its width and length at will (max length: 15 m, min length: 2 m). |
| Zean Lisander | The Dream Caster / Sandman | Oneirobot | Oneirobot is a swarm of utility sandlets. They could swarm into various shapes and forms as required or as commanded. |

### Daniel Ashton: The Origami Knight.
Weapon of choice: his own fighting suit.
No names needed.
His armor comes in form of sheets extruding from his body, and folds in a manner people could only describe as origami folds.
Apparently, in another note, his calling is the Origamist.
But I prefer The Origami Knight better now.

### David Pontirijaris: The Black Angel.
Weapon of choice is Midnight, a tweaked version of Adran's control system and Steven's armor.
Can reshape at will, mostly take the form of David's e-bike, with enough material to make two human-sized servants, or amorphous liquid-like protector (the shadow form).
Apparently in another note, his calling is the Death Goo.
The Black Angel rings better.
Here's a snippet:
David is dubbed The Death Goo with his classmates, because his use of Steven's Armor is often remote, compared to Steven's style that let him attach the armor to his body.
And David's armor usually took a more fluidic form, changing shapes at will.

### Michelle Williams: The Pole Dancer.
Weapon of choice is Ruyi Bang (Sun Wukong's staff), can elongate and shrink on command.
Made of telescoping BCN fibers.
Apparently in another note, she prefers Sun Wu Kong.
Her weapon of choice is the telescopic smartmetal pole, that could change its width and length at will (maximum length is 15 meters, from 2 meters).

### Zean Lisander: The Dream Caster/Sandman.
Weapon of choice is the Oneirobot, basically an utility sand, capable of forming various useful forms out of the swarm.
Apparently in another note he's called the Dust Master.
I think the Dream Caster/Sandman is better.

Apparently, in the other note, they're dubbed the Four Horsemen of Apocalypse.

## PS.
A little background, for etoan descended people, they have an average IQ beyond that of average humans, so when accounted in a human IQ test, they'd be on the upper edge.
Above 180, it is hard to reliably measure their IQ.
It may sounds extraordinary for a layperson, but an IQ test is actually made so that an average person would score 100.
So out of the data points that comprises human results, the average is 100, and outliers were given scores accordingly.
Etoan would most likely scored at upper ends.

But it was merely because they're having a higher intellecual capacity in human standard.
When the IQ test is calibrated for Etoan participants, among normal unenhanced Etoan, Daniel Ashton, Steven, Alex, David, Peter, Eden, Hayden, would all scores around 100.

See also: [KIA MTF Gamma-42 Four Horsemen](Notes/KIA%20MTF%20Gamma-42%20Four%20Horsemen.md)
