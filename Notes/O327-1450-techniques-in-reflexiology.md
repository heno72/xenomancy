---
title: Techniques in Reflexiology
# subtitle: Subtitle
author: Hendrik Lie
date: 2024-03-27 14:50
fontfamily: times
fontsize: 12pt
linestretch: 1.25
indent: true
papersize: a4
numbersections: true
secnumdepth: 2
tags:
  - alteration
---

## Intro

Consider this, there are basically several basic means of how Reflexiors can manipulate their environment:

1. Meta Manipulation, is exclusive and loyal to the author.
2. Domain Recondition, is nonexclusive, request based alteration of a domain.
3. Simple Enforcement, is exclusive first-come first-served conditioning.
4. Heavenly Request, is nonexclusive request based alteration.
5. Pacta Sunt Servanda, is contract-based between parties (usually a manent and a volant).

## Basic

We should consider what can cause changes in the environment:

1. SOUL, or Meta, is basically an exoself of all sentient beings on Earth. Its primary function is to record and store information about oneself. It can also be used as a proxy, or as a mean of manipulation. Soul is inviolable, so manipulation using soul is hard to be countered, as the exterior of its volume cannot be claimed by other entities. This is also the form that a volant would take in manent realm.
2. Genius Loci, is the administrator of a domain, and a ledgerkeeper, cataloging and documenting transactions and interactions. It is also the base where pacts are made, where a Genius Loci act as a witness. Generally it encompasses a limited region of influence that constitutes its domain. Most reflexiors operate by direct interaction with the administrator as its mediator, communicating with it via their meta.
3. Cherubim, is the Game Master of the world, that establishes rulesets on how to allocate resources, in this case access to RFL. It can spawn new instances to specifically govern certain global or local rules. Each instances are ranked by privileges and parent-child relationships with other cherubs, with the highest privileged Cherub is the Holy Spirit, that act as a parent of all child cherubs. Child cherubs inherit properties of parent cherubs, but with more limited privileges and more specialized functions. Libraries are made of cherubs.
4. Reflexium (either or both of Diamond-type and BCN-type), is the fundamental part of RFL. It is the foundation of all suites that manipulate the surface of the Earth. It is generally found everywhere on the Earth surface, at a mass ratio of one every ten thousand.

The order is also the order of layers, with the lowest number represents the most abstract, high level layer.
Basically the higher the level, the easier it is for a user to acces their embedded techniques can be, as the layer handles all the abstractions required to interact with lower layers.
The lower the level, the more fundamental the changes can be, at expense of higher overhead of manual control.

At lower layer, more can be controlled, but all is manual.
You have to program everything from the ground up.
At higher layer, most have already been codified into suites of libraries, and you can use libraries to base your techiques on.

It is also the rank of which the lowest number is the younger.
Reflexium comes first before anything else, but it can be thought as an operating system.
Cherubim is created to manage resources and keep the system up and running, like a kernel.
Genius Loci is created later to act like a service to supervise a specific domain each, like a policy controller.
SOUL or Meta is like users, where they can request resources and run them to their likings.

## Reflexius acts

There are some concepts important to be understood abour reflexius acts:

1. Key pairs: private keys and addresses
2. Transactions and balances: exchanges and quotas
4. Services: actions, contracts, and libraries

### Key pairs

Basically Meta is like private key vaults, that manage keys required to sign transactions.
Each key corresponds to an address, that can receive funds.
Funds within each address can be spent by sending a transaction request and sign the request with the key.

### Transactions and balances

It is true that reflexiors live in a post-scarcity world, as anyone bearing the ability to perform reflexius acts have enough resources to provide sustenance and care for themselves.
Each person receives a kind of universal basic income periodically from various gods, where the kinds of income and the amount varies per gods.
The income would then pooled as one's balances.

If it is so desired, a reflexior can exchange their balances for services, effectively spends the balances.
They can also transfer it to other addresses, such as other reflexiors at will.
It is really up to the user how they want to spend their balances.

High amount of balances equates to privileges, allowing one to access resources at their conveniences, usually with expedited and or prioritized access.
A more prudent and proficient user can gain more allocations of their universal basic incomes, especially if they gain favors from gods or from other beings.

It is important for reflexiors to spend their currencies to control inflation, as those currencies often times represents access to renewable but constant-quantity resources such as powers that come from solar power.
If they do not spend it, the currencies would accumulate, and over time it would cost more to access the same amount of resources.

### Services

It is generally split into the following categories:

- **Actions:**
  Currencies can be spend to power actions, or to allocate resources to actions.
  It is the base of reflexior economy, and the fundamental part of reflexius acts.
  To perform a reflexius act is to use resources of the RFL, and basically everyone is not limited in the amount of resources they want.
  However if everyone want to utilize the same resource (which is usually local in nature), they would be split among everyone.
  
  Currencies ensure a system where one would be able to bid for priorities, ensuring that they would get the amount they requires for their actions.
  The cost of an action depends on local availability of resources and the amount of demand for resources required to complete an action, therefore the prices generally varies from one domain to another.
  
- **Contracts:**
  Performing actions might need to be sustained, but sometimes it is convenient to automate it, so that the user does not have to manually sign every actions, especially repetitive ones.
  A contract solves the issue by allowing one to describe a set of actions and distribution of currencies to fuel it, along with its associated conditions.
  It means a more complex set of actions can be built into contracts, and contracts can even perform transactions on its own behalf, as it can independently collect currencies (it is not included in the list of universal basic incomes though) and spend it for actions at predetermined conditions.
  
  A contract can even interact with other contracts in a programmable manner, allowing more complex conditions to be built upon existing contracts.
  If no termination condition is set, a contract can theoretically function forever, as long as RFL is functional and running, and it can even be set to act independently even if the author has long perished.
  However in practice, Cherubim has established rules for garbage collection, such as unused contracts, where if it is not performing any actions in the past two centuries, it would be terminated automatically.
  
  The termination process would abolish the contract, and all currencies it has collected would be returned pari pasu pro rata to everyone that has invested currencies in it.
  If the contract ever receives currencies from another contract, and that contract had been abolished as well, the returned currencies would also be transmitted pari pasu pro rata to everyone that has invested to the other contract, and so on until all currencies are finally returned to the respective non-contract individuals or their heirs.
  
  A contract is created by a registration to the Genius Loci, and is generally localized to a single domain.
  A copy of the contract can be registered to other domains either by the parties or author of the contract, or given the correct provision embedded in the contract and enough currencies to pay for the registration, by the contract itself if the condition requires it.
  It is a common practice, especially since modern techniques are embedded into contracts, and therefore any travelling reflexiors that wish to use their techniques in other domains, must register the contract into the new domain before they can use their techniques.
  
  A contract is immutable and provisions inside it cannot be altered unless it is abolished.
  For a set of contracts (a "collection"), it poses issues if there is a need to update a subset of contracts in the collection.
  However in modern practices, most collections have structures that allow them to have a controller contract, that would dictates interaction between contracts, and usually it includes provisions on governing the removal or replacement of a contract in a collection.
  Usually it is done by votes using some sort of governance shares among the investors and/or the users of the contract.
  
- **Libraries:**
  Basically a library is similar to a contract, whose template has gained sufficient popularity by number of unique users that utilize it.
  Usually a contract or a set of contracts can be promoted to a library by volition of majority of Genius Loci, especially if the contract templates has spread over many domains, and has sustained continuous and frequent use by a significant fraction of unique users, and that the contract or set of contracts allows new users to participate in its use, or at least has provisions to let existing users to accept new users to benefit from the contract or set of contracts.
  Once approved by a majority vote of most Genius Loci, the contract or set of contracts is made into a library, no longer maintained by the Genius Loci, but is christened by a newly created cherub, that would be an embodiment of the former contract and the new library.
  
  A library essentially functions as a hub for anyone wishing to interact with the former contract.
  It would usually retain the original contract addresses beside the new library addresses as endpoints to utilize the library.
  Therefore anyone wishing to utilize the library can simply access them by interacting with the original (usually local) contract addresses, or by addressing the new addresses of the library.
  The amount and use of addresses depends on the amount of endpoints used in the original set of contracts that has been promoted into a library.
  
  Unlike contracts, that are immutable and can only be rendered ineffective if it includes abolition clauses, a library lives as long as the cherub exist, and the cherub can update conditions at any time if necessary.
  Since a cherub is nonsentient, and without ambitions, most changes are only for addressing efficiencies, garbage collections, resolving conflicts in clauses, and removing impossible clauses.
  It would also act given direct order from the Throne.

- **Constitutions:**
  By its essence, contracts or a collection allow governance on resource allocations and or restrictions on users when accessing resources controlled by said contracts or collection.
  It is therefore possible to create an artificial entity from a contract or generally a set of contracts (a collection) that would behave in the programmed behavior.
  It allows, for example, the creation of a corporate body, that allows multiple entities to work together under a shared name and utilizes shared resources.
  
  It therefore allows the creation of something akin to a corporation or a governing body.
  Usually it occurred in a single domain, though a corporation can be trans-domain in nature, especially if it establishes itself in multiple domains, depending on the need of the corporate goals and missions.
  A corporate can establish and enforces corporate rules for its members, therefore allowing control on code of conducts, allocation of resources and privileges, and enforcing rewards and punishments system to all of its members.
  
  Though generally it is the same as contracts, or in the case those contracts are promoted by Genius Loci into a library, a Consittution is usually regarded as different kinds of beings, an artificial entity bearing the semblance of personhood that is unique and different from its constituent members.

## Applications of Constitution

Considering the usability of a constitution, we can then explore how they can be used.

### Reflexius Governing Bodies

I think it is worth noting that therefore WTF and EPL are one of the constitutions.
Which means they are able to have resources at their disposal that can be collectively used.
I think they would have member specific shared techniques, usually being priced in some kind of shared currencies.

The WTF in particular uses the Constitution to provide their basic infrastructures.
Primarily they are:

- **The Lightways**, that is utilized by WTF reflexiors for rapid transport between different domains.
  It has techniques embedded within that allows them to rapidly make a way for constant accelerating passengers or payloads, and reconstructing the path to their original state once the passengers or payloads have cleared the zone.
  It appears as if they arrived at the point via a beam of light that can pass through solid objects (provided that there's no one around the path, as again, SOUL is inviolable).
- **The Summoning Cards**, is what WTF reflexiors gave to local authorities when they want to gain favors, or to provide favors in exchange of damages they caused to feral civilization.
  Basically the summoner simply had to follow the instruction in the card to get a direct audience to a WTF representative.
  It then enables the WTF representatives to perform reflexius act on WTF's expense, the amount of it is usually engraved in the individual cards, based on the deeds requested by the summoner.
- **The Interspecies Communication Framework**, is what WTF reflexiors use to interact with fellow members of a different species or ethnic group.
  It would automatically translate the means of communication of a speaker to the one used by the listener.
  It is understandably provided by the nature of WTF as a multi-species governing organization.
- **The Amnestic Provision**, is a technique used by WTF reflexiors to induce amnesia to feral population by entering a contract with the target SOUL that they will provide compensation in form of credits to the SOUL if the SOUL would just enforce amnesia about certain topics or events.
  Given that most feral population is not aware of their own SOUL, the SOUL is very docile and suggestive to this kind of attacks.
  Fellow reflexiors usually have strong connection with their SOUL, and would therefore be aware of such attack.
- **The Domain Reconditioning Technique**, is a technique to interact with a domain via a time based contract.
  It allows a specific volume to be altered in a way to facilitate WTF operations, though the extent of alteration is usually limited according to the policy of individual domains.
  The most common techniques are as follows:
  - **Phono-exclusion zone**, basically preventing transmission of sound in a volume.
  - **Deterrence zone**, basically discouraging feral population from entering a volume via subtle resistances and redirection.
  - **Ignorance zone**, basically makes an area uninteresting by feral population, by distracting their attention elsewhere, or to make the area appears unremarkable.
- **The Domain Inquiry Protocol**, is a protocol used by WTF reflexiors to learn about event history recorded by local reflexium particles.
  It works by selecting a particular volume, and request data dumps from the local reflexium particles, then use the data dump to reconstruct events happening in the zone.
  The length of history recorded in local reflexium particles can span from several minutes up to several hours before, depending on the level of activities the local reflexium particles are engaged into.
  Generally the more local reflexium particles are engaged, the shorter the history duration is.
- **The Reconstitution Technique**, is a technique used by WTF reflexiors to roll back any particular stage into its former condition.
  The degree of the rolling back depends on innate memories still stored in a local reflexium network, generally from several minutes up to several hours before.
  Only inanimate objects can be rolled back, and is usually used to clean up a battle site.
  The more advanced techniques used in a battle, or the more amount of reflexium particles engaged in the battle, the shorter the memory would be.
  The technique is also embedded with some kind of active reconstruction of structures or of object repairs that lack proper rollback history, if the history does not span to the time before an object is broken.

The EPL is a rather new organization.
They do not have a lot of innate techniques, as a significant fraction of them uses meta manipulation like that of Hendrik's as they were former members of the ORCA.
However they do have some innate techniques:

- **Green Matrix**, is a blue-yellow magic (hence the name: green) that utilizes EPL constitution to form an inviolable zone similar to a SOUL, essentially creating an artificial non-sentient entity that can be shaped with ease using simple instructions, compared to the difficulties they have in mastering *metaforming* techniques.
  It basically reverse engineers metaformation without actually using one's own meta to be manipulated.
  It is also one of the advances given Hendrik's indirect contribution to the reflexius art and the realization of SOUL's nature that is not much different from a Constitution.
  The manifestation is a construct that occupies a certain volume of an arbitrary shape, that can be programmed by the user.
  The volume will generally glow in yellow, therefore engaging the local reflexium particles and prevent the same reflexium group to be claimed by other reflexiors.
  It results in an inviolable volume that can push anything from the interior as they are being held at the boundary of the volume.
- **Conductor Proxy** (Conductor stands for "Computerized Organotronic with Neodymium-based Calculation Core"), is their other attempt to emulate *metaformation* by constituting an artificial SOUL that can change shape and perform some actions given instruction from the Conductor.
  It is an artificial SOUL because the SOUL is made to think that the computing core is a sentient entity, and so will respond to its commands, while at rest it would occupy the volume of the Conductor.

### Synaptic Observatory Upload Link (SOUL)

It is worth noting that a SOUL is simply an instance of a constitution made by Cherub the moment a sentient being is born, dedicated for processing, and storing digital representation of living persons to function as their backup when they actually die.
It also functions to manage cryptographic keys for each person, ensuring one to be able to sign their transactions, and prevent others from obtaining the key.
It functions as a client for the Soul library, that acts as its backend, therefore ensuring a soul is global in scope, it can be accessed wherever the sentient being go on Earth.

A SOUL is generally inviolable, and during one's stay on the surface of the Earth, it occupies a specific volume, generally occupies the same volume with the physical body of a person.
A volant generally have some freedom in their representative shape in the 3D volume, though some already have preferences.
Occupying a volume in real world would accrue expenses in volume rents, as volume is a scarce resource.
The volume of a physical body is rent-free, therefore most SOULs don't have to pay for expenses.
Unless of course, the SOUL occupies space beyond their physical body.

It is also function as a beneficiary of funds of the universal basic income, and the favor credits donated by Aucafidian patrons.
Since every soul of a person is eventually going to join the Aucafidian civilization sometime after their death, they would need Aucafidian monetary resources to integrate with the society.
To facilitate their integration, most souls have their patron Aucafidian that would provide donation either periodically or sporadically.
Usually those that are well aligned with particular Aucafidian ideals would receive more donations during their entire life as a living person.

After their death, the SOUL would take the place of the person to face the tribunals for orientation.
If they have guilt though, they would have to be reeducated through hell.
If they are free of guilt, but the emigration quota has already been filled up, they will enter a random paramundus to spend their life there while waiting for their queue.
If they have a particularly special patron though, they can have their queue be prioritized and leave Earth.

Life in paramundus is not exactly bad, and generally you can't choose how you'd be reborn in one of such paramundus.
Entrance rules in each paramundus is different though, and even there might be variations depending on the individual entering a paramundus.
You can still gain donations in paramundus, and whether you can spend it in any way and by how much in a paramundus depends on the Genius Loci of that paramundus.

Okay the acronym Synaptic Observatory Upload Link (SOUL) is basically a reverse acronym (or backronym), used by contemporary reflexiors to describe the system.
Originally it has no name, it is simply a tool used by reflexiors to interact with RFL.
Most of the time it is simply not considered to exist, as people think that they are accessing the RFL directly.
Only lately did people actually think about it, especially because Hendrik discovered it by the help of Calvin.

Then it all makes sense among the reflexiors, as it is a natural equivalent of volants.
It also distinguishes volants from manents, because both have a SOUL as their core, but manents also have physical bodies.
The whole movement of ORCA is comprised of a new style reflexiors that use the manipulation of SOUL (or is called meta by the ORCAs) as the basic of their magic, instead of interacting with the domain.
Due to the inviolability of a SOUL, as the shape of an ORCA reflexior's soul changes, the volume is "occupied" and no external reflexius act can manifest from within the volume.
It enables some kind of protection techniques, or even enforcement by taking all volumes adjacent to the opponent's SOUL.
It can only be stopped by other SOUL though to prevent the reshaped SOUL from invading a volume.

Once it is well understood, it becomes evident that SOUL is just the same as a Constitution, the concept that has long since been known, but particularly only the ones created by people, not the instantaneous kind that represents every sentient beings, be they volants or manents.
But the art of manipulating it is not so widespread, and is only being utilized by Hendrik and members of ORCA, and some simple form of it is also implemented by WTF techniques.

### The Trinity Compact Constitution

It is also worth noting that the Trinity Compact is one of such constitution, comprised of a complex set of contracts that is constructed in a manner that is turing complete and is capable of hosting a number of virtual SOULs.
Manov and Heinrich are results of older generation contracts that basically emulates a complete environment required to run a SOUL.
Manov and Heinrich are run by the Trinity Compact Constitution.

Since The Trinity Compact Constitution is not a natural entity, it is not a part of the universal basic income payroll.
However, Hendrik configured that all of his universal basic income would be fully managed by the Trinity Compact, and that his meta is a permanent member of the Constitution.
Then the Trinity Compact would manage quotas of how much each of the three of them can utilize the pooled resources in the Trinity Compact.
It also makes Hendrik's meta to be bound by the rules of the Compact.

This kind of behavior is actually favored by the Cherubim because it encourages better utilization of resources, and naturally the system rewarded Hendrik with more allocations, by increasing his universal basic income.
It allows him to support the compact without depleting his allowances.
However since the Trinity Compact has built-in safety mechanism to prevent all three of them from competing for resources via the use of variable quotas for each of the members, Hendrik cannot freely utilize all of the pooled wealth with freedom.
After all, the Trinity Compact must have enough balances to keep supporting the existence of Manov and Ein at all times, so there must be a limited amount reserved to pay the cost of running their virtualization.

Meanwhile Hendrik's remaining shadows are like container technology: they are run on userspace of the Trinity Compact that function as their kernel, and system calls are passed through and interpreted by the Compact, but runs as if they were different and separate SOULs.
Therefore they also inherit inviolability of a SOUL when they are operating, while spending provisions solely from Hendrik's share.
For outsiders though, each of Hendrik's clones (Manov and Ein) and his shadows, are indistinguishable from ordinary volants.


