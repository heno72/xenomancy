---
## Consider not having fullcaps for headings.
title: Inspiration
## We don't need a subtitle for now
subtitle: Distinctive Characteristics of EPL, ORCA, and WTF
## Information about authorship(s)
author: Hendrik Lie
date: 26 September 2021
## Fonts and formatting.
fontfamily: times
fontsize: 12pt
linestretch: 1.25
indent: true
papersize: a4
## Numbersections
numbersections: true
secnumdepth: 2
---

Distinctions between EPL, ORCA, and WTF to reinforce the Three Ways War theme.

# WTF

People are masters of their environment.
They represents the civilization, order, and protocols.
By nature they're hierarchial, and heavily regulated by laws and protocols they made themselves.
Their main philosophy is based on that: true freedom is the freedom of making a law, and abide by it voluntarily.
Otherwise they'd be mere slaves of their animalistic nature.

Their magic-like manifestation (they insisted to call it reflexology and reflexius acts) tend to be control over elements, or environment around them.

# EPL

All souls are equals.
They represent the natural order of life, that is, chaos.
By nature they're more flexible and tolerant.
They demands freedom in equality, every beings must be free to do whatever they want to do, without worrying about basic necessities.

Their magic-like manifestation (they simply called it magic) tend to be controlling or summoning of familiars, be it physical familiars or virtual familiars.
They tend to have a very good relationship with spirits.

# ORCA

Power comes from within.
They represent humanity.
Their philosophy is not based on control over the environment, or over spirits, but mastery of self.
They see freedom in the ability to understand their own self.
So they understand why they're doing what they're doing.
Their aim is to understand themselves, so they're not enslaved by their own flaws, rather they embrace and expand themselves, to be a better version of themselves.

Their magic-like manifestation (they simply called it as the art) tend to revolve around metamanopulation.
From metapresence, to metaforming, all revolves from themselves first, and projected to the world.

