# KIA MTF Alpha-19 "The Archangels"
All of the members are from the HD Club.
Armed with etoantech Archangel Exoarmor.
Asked to join the Intelligence Agency per Anderson's request ever since they made an appearance during 2022 Denefasan invasion.

The Archangels specialized in engaging with Denefasan forces, or forces reinforced with Denefasan tech.

Members:
- Steven Pontirijaris.
Practically the leader of the group.
Highly adept in using the armor, and reconfiguring it to his will.
His specialization is rapid response.
- Daniel Lusien.
Focus on defense, and always protect civilians first before launching a defensive attack.
- Fernando Suryantara.
With this suit, he's equipped with perfect kinesthetic sense, added agility, and strength.
Specializes in offensive attacks.
