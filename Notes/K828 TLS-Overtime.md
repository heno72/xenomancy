# 20200828 TLS-Overtime
TLS:Overtime must be able to show:

1. That Cal is tired after many years of missions.
He asked Alex, his grandson, to help him out of this life, and move on to a new life.
A life where he could take a rest.
But he remembered about Anthony, a best friend of him.
A flashback would be shown, on the day they meets, the day they perform together, and the day Anthony and Cal had a talk that Cal felt tired.
Perhaps Anthony knew who Cal is: Migun.
Perhaps Aditya disliked Cal simply because it caused Anthony to be distracted.
Perhaps, the reason behind Aditya's dislike to Cal, simply because Cal stayed longer than he should.
2. Perhaps, Aditya again, wanted to use one's free will to torture or teach others lessons.
So Aditya made himself fight Cal, and let Anthony choose, to save Cal or to save him.
Aditya said he wanted to have a son that could choose his own path, it happened that the path Anthony must be choose would be either Cal's ways or Aditya's ways.
Aditya was almost being defeated by Cal, but Anthony, knowing that Cal is Migun, knew he wouldn't be affected with it.
Anthony fought Cal in the end, to make Aditya trust him again, while by the end, Anthony begged Cal to do his thing, so he had a reason not to kill Cal: he's not supposed to kill an ambassador.
Anthony pleaded, and that's when Cal realized Anthony's intention, and declared the ambassadorship.
3. Back to Cal and Alex.
Cal told Alex that, the most hardest part of his job is that he'd be very attached to the subject of his missions.
First Hendrik, then Derictor, then Xiangyu, and now, Anthony.
He should stay for as long as necessary, and move on when his mission is done.
But who can control their heart? Spending time helping them, he must not stay a bit longer than necessary.
The end result was like that with Anthony.
He decided to stay longer, and then Aditya came, a warning that he shouldn't stay any longer.
He had to part ways with Anthony, otherwise, a lot more things will come and overcomplicate the entire plan.
Cal said that all of his life, he could decide to stay as long as possible with anyone he wished to, and they wished to be with him as well.
But this mission tires him out because of the constant need to move on.
He wanted a break.
4. Alex, understand that, so he prepared etoan paperworks to relinquish his position as an ambassador.
He opened Ostaupixtrilis's file, and discovered, that apparently this body must be disposed first, to complete the contract.
This body contains specific physiology inherent to the role of an ambassador, so Cal couldn't keep it.
Alex asked again, after the disposal, would Cal want to be beamed back to Aucafidus? Cal thought for a moment, saying that being on Earth is his dream, and since he had been doing missions, he wanted to try to the fullest, the life on Earth.
He said that he wanted to be born again as a human, an authentic human.
Alex opened the file again, and said that, one of his best option would be to order a human body to the Divine Council of Earth, as human-specific genomes are owned entirely by the Divine Council, so etoan could not print human bodies, except to create human-like beings.
Cal said he'd take that.
Alex warned that it means, the rules that apply for mankind would also be enforced at him, sparring some exceptions inherent of being the citizen of etoan colonization initiative.
Cal said yes.
