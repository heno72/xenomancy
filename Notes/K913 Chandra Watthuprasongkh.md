# Alteration K913: Chandra Watthuprasongkh

In Integra, Chandra's nickname is Chan.
However, I don't think it is necessarily the same in Xe.
That is also for the benefits of the readers.
It is important to make the distinction so they could have a better grasp that they're different.

In Xenomancy, Chandra's nickname is either Kao (เก้า: nine) or Khao (เขา: mountain, hill).
Originally the parents of him called him Khao, as he's firm as mountains.
However, from his elementary school onwards, his friends called him Kao instead, because it sounds like nine.
It is a misunderstanding, mainly because he's also born in September 9th, 1967, so naturally the friends thought his name meant nine instead of a mountain or a hill.
He grew comfortable with the name afterwards, that the name stick.
