---
## Consider not having fullcaps for headings.
title: Inspiration
## We don't need a subtitle for now
subtitle: Transderivational Entity and Tudva
## Information about authorship(s)
author: Hendrik Lie
date: 06 May 2022
## Fonts and formatting.
fontfamily: times
fontsize: 12pt
linestretch: 1.25
indent: true
papersize: a4
## Numbersections
numbersections: true
secnumdepth: 2
---

I think I now know where Leaf character design can be applied on in Xenomancy.
Anderson must be a transderived Leaf, a Transderivational entity to be precise.

The reason is that an Etoan Superior has a rather flexible brain structure that it's used specifically to host nonhumanoid entities into a humanoid shape.
In the past, AIs and uploaded individuals are downloaded to Etoan Superior, and normal Etoan be upgraded to Etoan Superior.
Their programmable, Turing complete brain allows such modification possible.

Leaf is a xenoentity, a true alien.
Their thoughts can't be directly translated to a form normal humanoid brain can handle.
Their thoughts are distributed, each shard is semi autonomous, and can operate independently.
Each has its own personalities and expertises, and their thought streams behaves more like a communion than an individual.

The only anthropomorphic emotion-like responses they have are caution and curiosity.
The rest are incomprehensible and have no direct analog to human emotions.
They are also very logical, straightforward, calculative, and ruthless.
They are very goal oriented and care only on achieving them.

Anderson supposedly behaves like that too.
But there's no hard rule in his line of thinking.
Now at least I have a clearer sight of his thinking modes, being modelled directly from Leaf.

My original reasoning was because, since Adran is here anyway with Ashton, why don't Leaf and Tudva join them there?  Perhaps, Tudva never left to begin with.
She's taking another role.
If Os could roam about Earth, then Tudva too, can do that.

I was thinking that Tudva might be Henokh's mother, or maybe, just maybe, Henokh's wife.

Sylvia Lusien, is a sister of Daniel! Only about 3 years younger.
Then how could she be Tudva? Unless she's adopted! Yes that very much could be it.
Have i said that Daniel's parents are actually genetically identical with Derictor's parents? Names are in my notebook, exist in Surabaya.
I regret that i didn't copy them down to my computer.
It's a rather important piece of information now.

Or maybe not.

Maybe Daniel's mom had some sort of cancer after Daniel's birth, and despite her dream of having a baby girl, she had her womb removed already.
Or that her womb is very weak, Daniel is the only one she could have.
She'd want to adopt a girl.
And so she discovered Sylvia, formerly Sylvia, just Sylvia.
She had her, and Sylvia becomes Daniel's sister.
So is her mission.
Or maybe, her vacation.

It's only fair to think that Ashton must not only be the only one doing mission.
Maybe Sylvia too, did her mission.
Of what nature, I wonder.
It must be scouting then, because there are plenty of unanswered questions.
Like how did Alt find Ashton eventually? And maybe, she also has her role in identifying the vectors of Godfluenza.

While Ashton did his job to establish the scene of the battle by preparing various Elements according to the plan, Sylvia scouts for "talents" to play on the stage.
Perhaps then, Derictor's influence might not be on Ashton's duty but on her.
Because she's simply closer to it than Ashton.

So in her younger days she scouts for Derictor, Henokh, and Aditya.
That's how she could inform Anderson later.
That's also how she grew closer to Henokh, and eventually get married.

Then, it's a right decision to rework 2003, because then, we can include her into the equation.
And maybe it's something about Henokh and Aditya, long before the actual battle starts.
Maybe it's an earlier iteration of the plan, before it's decided that they must be split.
It may also relate to other aspects of 2003, such as the trip, the love confession of Fernando, the coupling of Fernando and Daniel, the rape of Steven, and the resolution of HD Club.

It got me thinking, Ashton and Adran took a great care of their family, while maybe Anderson and Sylvia work together too on the other side, of the A cluster.
So their goals are aligned to the A cluster, while Ashton's role is to prepare for C cluster.

And Anderson did interfere in 2003.
Anderson was there, so maybe Sylvia too! It was inspiration 20190222 by the way.
Maybe, if Ashton is the cure, she's the antibody.

Or maybe not.

But I think unlike Ashton that is able to neutralize magiclike manifestation, I think she can somewhat "steal" them, or rather, "mirror" the, via the use of ericksonian hypnosis, not unlike that of Icpo's power.

