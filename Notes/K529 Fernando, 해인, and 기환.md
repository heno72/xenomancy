# 20200529: Fernando, 해인, and 기환

![Family Tree Diagram of Fernando and Haein](/Pictures/Fernando_Genealogy.jpg)

What if, Fernando is related to 해인, somehow? That 해인 is, Fernando's father or something.
We haven't explored Fernando's family up to now, even though we have a draft for Daniel's and Derictor's parents.
Perhaps, it is also about time to explore their family trees.

First of all, Aditya is related to David, as Aditya is Helena's cousin.
Could Anthony be related to Fernando as well, through 해인? What if 해인 is Fernando's father, but Fernando used his mom's surname because they're never married in the first place? Impossible, he's born in 1973.
Fernando is about a decade after, he's a very young father? Damnit, that means Fernando's mother must be impregnated by 해인 when 해인 was 11 years old.
Is that even possible?

How could Fernando has no birthday assigned to him in gitlab? Was it that way in OStorybook? Check again.
I'm guessing he's around 1985 as well, close to Steven.

What if, Fernando and 해인 are cousins? So their mothers are siblings, and 강해인's mother married a korean named 강우탁.
Then 강해인 has a fraternal twin that is donated to a cousin of his parents that are fertile.
And he becomes 양승민, an adopted son of 양준기 and 김민지.
Then 양승민 married Martha Williams, that becomes the parents of 양기솬 and 양치민.

Therefore, after examining the familial tree, and having the fact that 강우탁 and 양준기 are cousins, 양승민 is biologically 강해인's brother.
That means, 양승민 and 강해인 are Fernando's cousins.
Then, 양기환 and 양치민 are Fernando's nephews, or a relative to the 5th degree.

In Integra version though, 해인 joined GFE, and is sufficiently wealthy.
Meanwhile Fernando (In) is sufficiently poor.
While in Xe, Fernando is not that wealthy, but not poor either.
Then, in Integra version, Fernando (In) is assigned to live with 강해인, as his parents disowned him when they found out that Fernando is gay.
강해인, being kind-hearted, easy-going, and open-minded, let Fernando live in his residence, that he rarely occupy due to his missions.

This also implies they're sufficiently close to one-another.
Fernando might as well be a member of GFE, or one of its frontends, somewhat.
After all, Fernando is still a medic in Integra as he is in Xe.
Perhaps he's related to the Project Antarabhava, as medical supports.

What about Fernando and 해인 in Xe? They're still quite close, and I think, similarly, Fernando is disowned by his family when they found out that he's with Daniel.
Fernando (In) didn't get to be with Daniel.
해인 is in the military, and he's with Chandra Watthuprasongkh, as in Integra, but instead they're a normal Indonesian military? And then, his squad in Integra, could they be real life friends? Especially, 王实地 (Wang Shidi), that didn't die because of Zarah here.

What if 해인 is also a part of KIA? What if, instead of using random, featureless characters, we use the cast from MTF "Samsara" as the members of KIA? It is no longer BAIK after all.
Michael Carmichael (Xe) is not a Brahma soul, but that doesn't mean he couldn't get the same personality.
So, he'd be a part of the quad: P'Chan, Michael, 해인, and Wang Shidi.
Then we get all the 5 ladies to join KIA: Go, Djun, Watt, Emma, Bright.
I think it means we can balance out the woman count.
Bright could also be a part of KIA.
