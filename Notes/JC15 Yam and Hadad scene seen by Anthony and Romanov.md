---
## Consider not having fullcaps for headings.
title: Inspiration
## We don't need a subtitle for now
subtitle: Yam and Hadad scene seen by Anthony and Romanov
## Information about authorship(s)
author: Hendrik Lie
date: 15 December 2019
## Fonts and formatting.
fontfamily: times
fontsize: 12pt
linestretch: 1.25
indent: true
papersize: a4
## Numbersections
numbersections: true
secnumdepth: 2
---

# Yam and Hadad scene seen by Anthony and Romanov

**20191215**

Anthony sees the slaughtering of Yam by Hadad is like having Hadad chained Yam up with ropes (Chaser), and with a hot knife (Driver) castrating Yam.
Manov sees the slaughtering of Yam by Hadad is like having a pack of wolves connected with light harness to Hadad's arm (Chaser) located and lock Yam away, and with a gauntlet (Driver) draws Yam's soul/passion/desire away.
Both sees the soul/balls swallowed by Mot, but to Anthony, Mot looks like a Cerberus, while to Manov, Mot looks like Anubis.
Anthony reasoned Manov has no balls because he is a virtual being, while he sees the desire/passion/courage of Yam that was taken away naturally as an allegory of a man's balls.
Anthony however expressed his concern, after contemplating the data stream, that Tiamat is Yam's balls.
And that Heinrich, as an angel, intervening him, while both are servants of Divinity, meaning there are two different conflicting orders by the Divinity.
Anthony said that such a contradiction hadn't happened since the last time there was a coup in the Heaven.
