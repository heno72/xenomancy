---
## Consider not having fullcaps for headings.
title: Inspiration
## We don't need a subtitle for now
subtitle: Mystery Theme For Fallen
## Information about authorship(s)
author: Hendrik Lie
date: 12 April 2022
## Fonts and formatting.
fontfamily: times
fontsize: 12pt
linestretch: 1.25
indent: true
papersize: a4
## Numbersections
numbersections: true
secnumdepth: 2
---

Fallen started out as an idea to talk about an assassin from Venus that is assigned to kill a God. However, the idea didn't really bite well.

I later changed it to a businesswoman wishing to find her own knight in a white horse, and she was trapped in a riot. Over the course of the story, it was later discovered that she was a creation of Fourth, a mere imagination manufactured to reality. There's also mention of Ashton.

Didn't quite bite as well. Why don't we just start over, with an investigation? It is an investigation about the origin of Aucafidian Civilization, and its relationship with Toru El. Crafted correctly, we could then paint a picture about Aucafidian Civilization in general.

