---
title: Inspiration
## We don't need a subtitle for now
subtitle: Standard Aucafidian Date
## Information about authorship(s)
author: Hendrik Lie
date: 20 February 2017
## Fonts and formatting.
fontfamily: times
fontsize: 12pt
linestretch: 1.25
indent: true
papersize: a4
## Numbersections
numbersections: true
secnumdepth: 2
---

It turns out I have to balance the offset from solar year length when compared to sidereal day.

So I decided to separate traditional Verinsius calendar and Standard Verinsius Calendar.

I make it easy by dividing a year in exactly 407 days, compared to traditional 407,14 days.
The difference is minor, that a standard day is just 1.0003 times as long as traditional day.
Or about 27-28 seconds longer than traditional fate.

Date not fate.

The catch is just that every roughly 7 years the standard calendar lags a day compared to traditional calendar.

This however is not a problem, because the year count would be roughly similar, and every 350 years due to Supercycle of years in traditional calendar, the two calendar would synchronize.
After all, a standard year and a traditional year is exactly on the same length in time.

Due to the catch however, Aucafidus bound aucafidians would use traditional calendar for dating system while Aucafidian not in Aucafidus would use standard calendar.

This is very convenient, especially because most of Aucafidians are not in Aucafidus.
Why don't I think of that in the first place.
It saves me a lot of computation and mental resources.

For dating however, standard calendar uses standard day as, umm, standard measurement of a day, while every year is treated to be anomalous year, which is 407 days long in traditional system.

While in traditional calendar, in an intricate 7 years of cycle (Quartergeneration cycle), a year must be determined to be even or odd years, that are 408 and 406 days respectively, and every 50 Quartergenerations, or a Supercycle, the first even year would be transformed into anomalous year that is 407 days long.
