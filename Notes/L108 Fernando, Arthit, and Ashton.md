---
## Consider not having fullcaps for headings.
title: Alteration
## We don't need a subtitle for now
subtitle: Fernando, Arthit, and Ashton
## Information about authorship(s)
author: Hendrik Lie
date: 08 January 2021
## Fonts and formatting.
fontfamily: times
fontsize: 12pt
linestretch: 1.25
indent: true
papersize: a4
## Numbersections
numbersections: true
secnumdepth: 2
---

Arthit, or Tee, Charles's friend, would be close with Hae-In, his father's best friend.
Meanwhile, Fernando Suryantara, Hae-In's cousin, would by extension be very familiar with Tee.
So I think Tee would call Fernando as either Mr.
Sun (because, well, Surya) or Mr.
Tawan, as Tawan is Sun in thai, other than Arthit.
It is said that, when the two are together, the atmosphere is always too bright, because of the Double Suns.

I think it means that, post Charles era, Ashton would refer to Fernando as Tawan.
Fernando would call Daniel as Hin.
And there we go, a semi-ff treatment of TayNew in Xenomancy.

I've been thinking about Fernando's job in Xenomancy though.
He seemed to be away a lot, and I don't think having him in the G28 forces as viable.
Having an experience as a military doctor might be useful, but I think it is too far-fetched.
So most probably, most probably, he's a model and an actor, an international one.
Uh, based on Thai? I think that's too wishful lol.

But he could be one of the DoS agents.
He'd be having a lot of contacts from the showbiz industry for southeast region, while doing travel blogs, modeling, and acting jobs.
He's one of the Kuker Multinational Mediatainment Corporation (KMM Corp.) talent, with the scope that spans to many countries in Asia, as far away as South Korea.

