---
title: Eto aucafidi
subtitle: Subtitle
author: Hendrik Lie
date: 2023-12-18 16:44
fontfamily: times
fontsize: 12pt
linestretch: 1.25
indent: true
papersize: a4
numbersections: true
secnumdepth: 2
---

**Species:** Eto aucafidi

**Summary:** A humanoid species very closely related to [Homo sapiens](https://xenomancy.id/wiki/Races/HomoSapiens?action=edit), native to [Planet Aucafidus](https://xenomancy.id/wiki/PlanetAucafidus?action=edit). Also known as Etoan.

**Description:** _Eto aucafidi_ or Etoan is a species with at least two existing subspecies: _Eto aucafidi siyakiformes_, and _Eto aucafidi optimus_.
The siyakiformes subspecies is genetically very close to _Homo sapiens_, to the point that some taxonomists consider them to be a single species.
The most notable differences of _Eto aucafidi_ compared to _Homo sapiens_ are their higher concentration of positively charged [myoglobin](https://en.wikipedia.org/wiki/myoglobin) in their muscle tissues, and a complete lack of myostatin producing genes.
Morphologically, their ears tend to be more pointed compared to Homo sapiens.

_Eto aucafidi optimus_, in the other hand, despite sharing a hundred percent of Eto aucafidi siyakiformes's genes, comes with a distinct set of genetic enhancements.
Their skin utilizes zinc oxides instead of [melanine](https://en.wikipedia.org/wiki/melanine) for the purpose of blocking out ultraviolet radiation from their sun.
The red blood cells are not present in their blood vessel, but instead a specialized, multipurpose protein-based nanobots in the form of polyhemeobtan matrices take the role of oxygen transport and waste removal, along with their immune system.
This results in milk-like consistency and coloration of their blood.

The most radical differences that _Eto aucafidi optimus_ possess compared to siyakiformes subspecies and Homo sapiens are in their respiratory, musculoskeletal, and neurological systems.
Their respiratory system are analogous to the [avian respiratory system](https://en.wikipedia.org/wiki/Respiratory_system#Birds), being comprised of true lungs and air-sacks.
The skeletal composition differs significantly from that of mammals.
They also have specialized iron-meshed neuronal tissues.

## Special modifications of Eto aucafidi optimus

### Respiratory system

Their true lungs are comprised of four regions, based on the orientation in left-right orientation, or ventral-dorsal orientation.
They are neither contracting or expanding in each breathing cycle, and are not populated with alveolar sacks.
Instead, their true lungs are comprised of structures similar to avian para-bronchi, millions of fine pipes surrounded with blood vessels to exchange gasses.

When they inhale, they expand their pleural cavities via the movement of their rib cages, along with the contraction of their diaphragm.
The expansion of their pleural cavities produced negative pressure areas in their posterior and anterior air-sacks.
The inhaled air is routed through ventral true lungs to the posterior air-sacks, where it would also be routed to the anterior air-sacks through their dorsal true lungs.
Valves from their anterior air-sacks to their trachea are closed during inhalation.

When they exhale, they compress their pleural cavitias via the movement of their rib cages, along with the relaxation of their diaphragm.
The compression forced remaining air in the posterior air-sacks to evacuate toward anterior air-sacks through their dorsal true lungs.
Oxygen-poor air in their anterior air-sacks would evacuate to their trachea as the valves on tracheal-anterior air-sacks passages are relaxed.
On the contrary, the valves between ventral true lungs to the posterior air-sacks contracts during exhalation, limiting the evacuation of air from posterior air-sacks only through the dorsal true lungs.

### Musculoskeletal system

About twenty percent of the volume is filled with carbon-nanotube reinforced polymers, along with a network of carbon-based phononic computronium clusters connected to protopolyhemeobtan glands.
The introduction of carbon-nanotube reinforced polymers increased their skeletal elasticity and strength, therefore reducing the likelihood of breakage.
The network of carbon-based phononic computronium clusters are dedicated to program protopolyhemeobtan matrices to produce mature specialized polyhemeobtan matrices, and to coordinate their functions.

Mature specialized polyhemeobtan matrices, or colloquially called as silver agents, would then be released to the blood stream to perform their preprogrammed functions.
The functions including normal circulatory functions: introduction and transportation of nutrients and oxygen, followed with waste products removal.
The detection of foreign bodies, and tissue repair could also be programmed into their functions, under supervision and coordination of the skeletal phononic computronium clusters.

### Iron-rich neural tissues

Their specialized iron meshes sheathed neurons are spread across their skeletal structures and cartilages, with the highest concentration are on their temporal and parietal lobes, ear and nose cartilages, cranial shell, and rib cages.
The main functions of those specialized neurons are for the production and detection of radio-frequency broadcasts.
This specialization enables two or more _Eto aucafidi optimus_ to communicate with each other via multi-channeled high-bandwidth radio-frequency data exchange, along with communicating to their technology.
Other than communication purposes, the same infrastructure could also be utilized for passive and active [radio-frequency detection and ranging](https://en.wikipedia.org/wiki/radar).
Essentially equipping _Eto aucafidi optimus_ with medium to long range ranging and imaging of their immediate area, in the absence of other senses.
