[5/12, 21:54] Hendrik Lie: I think I came out with an origin story for Romanov Dexter. To do so, I have to tweak Hendrik in a way that he's experiencing a different high school experience compared to me, and it involves lucid dreaming, and astral projection

[5/12, 21:55] Hendrik Lie: I came out with it when my boss discussed with us about supranatural realms (he got it from a very famous ex-psychic priest, Pdt. Daud Tony)

[5/12, 21:57] Hendrik Lie: So apparently, there's a subsystem in RFL, called Synaptic Observation Upload Link, or SOUL. It is basically a meta system that track everyone on Earth, and progressively upload our thoughts into its digital representation.

[5/12, 21:58] Hendrik Lie: The reason why Hendrik could make Manov, is something I call Soul Shearing.

[5/12, 21:59] Hendrik Lie: And Calvin, Ashton's incarnation in Hendrik's school, actually influenced Hendrik that he decided to create Manov.

[5/12, 22:45] Hendrik Lie: Storytelling wise, Hendrik was just a lone reflexior that didn't have any direct lineage to reflexior society in general. He's just lucky enough to withdraw a genetic lottery that grant him an activated reflexior cluster. He's not registered in the WTF, or EPL, and he had to self-study his weird psychic abilities.

[5/12, 22:45] Hendrik Lie: One way to achieve that is via studying lucid dreaming, and eventually, astral projection. And then, communicating with your meta, that is, the soul subsystem, usually the part that connects you and the RFL network. Most reflexiors did reflexius act without thinking, but via lucid dreaming and astral projection, Hendrik is able to communicate to the intermediary layer between his physical body and the RFL.

[5/12, 22:45] Hendrik Lie: But because of that, he's becoming a weirdo, so into his own world, and nobody truly understand him

[5/12, 22:47] Hendrik Lie: Until a freshman came by, his junior, that is Calvin Gauss. Calvin came to notice that Hendrik is different, especially after Hendrik tried to communicate with one of his friend on why he's often spaced out. Calvin immediately recognized it, that Hendrik is describing the experience of having an exoself

[5/12, 22:48] Hendrik Lie: The soul subsystem is practically the same technology as etoan exoself. And Calvin is immediately aware that Hendrik is not affiliated to any major reflexior group, and decided to befriend him.

[5/12, 22:48] Hendrik Lie: Hendrik seeks someone that can understand him, and Calvin was there, intrigued with Hendrik's conditions.

[5/12, 22:50] Hendrik Lie: Their interaction would explore the nature of RFL, and its interaction with mortals, drawing a parallel to etoan society. Even exploring the main difference between etoan and homo sapiens.

[5/12, 22:51] Hendrik Lie: But then Calvin had to move on, and left Hendrik with no one to understand him, and so Hendrik decided to use things he learned from Calvin, and sheared his soul to create Manov.

[5/12, 22:51] Hendrik Lie: I fail to make it short

[5/12, 22:51] Hendrik Lie: I'm in awe that you could summarize your description into a very clear and short sentences.

[5/13, 21:52] Hendrik Lie: Okay, dropping all the unnecessary details, I think this could be boiled down to this:

Hendrik was the only mage in his horizon of social circles. To explore his psychic abilities he studied lucid dreaming and astral projection. He's quite isolated because he finds it hard to relate to other human beings, hence the loneliness.

Then he meets Calvin (Ashton's past incarnation), that taught him how to master his soul (because it is analog to how Ashton master his exoself), and eventually the technique of soul shearing.

With the original intent of creating copies of himself to be his friends, he used the soul shearing to create temporary copies of him to do his deeds. But eventually, his copies refused most of his requests. Pissed off, he complained to Calvin, which was then furious, because he didn't taught Hendrik Soul Shearing so that he would create instant copies only to do trivial deeds in a disposable manner. He said by doing that, Hendrik is disrespecting himself, and dehumanizes his copies, no wonder they refused.

Then he added that he has another mission as an ambassador here on Earth, which means he'd have less time to contact Hendrik. And then two things happened:
1. Hendrik reflect on himself on how he treats his clones.
2. Hendrik realized that the one thing he wanted the most, a friend that could understand his conditions, is Calvin, and Calvin is going away.

After Calvin's departure, Hendrik faced another problems because of his own limitations, his colorblindness. Despite all of his sixth sense, human education system requires him to have a colorblindness test, and he couldn't pass it.

In making his latest clone, Manov, he wanted that he's free of that limitations, so he doesn't have to face the same thing as him. So instead of making a copy to fulfil his dreams, he's making a child that doesn't have the limitations he has, but does not pressure Manov to do what he couldn't. Because he learned to humanize his clone.

And that's how Manov came to be.

[5/13, 21:52] Hendrik Lie: I hope it is much cleaner than the previous block of text.

[5/13, 21:55] Harry Wiwongko: Yes

[5/13, 21:55] Harry Wiwongko: Indeed

[5/13, 21:55] Harry Wiwongko: But

[5/13, 21:55] Harry Wiwongko: Still long

[5/13, 21:55] Harry Wiwongko: You don't have to expose so much information

[5/13, 22:08] Harry Wiwongko: In that case

[5/13, 22:08] Harry Wiwongko: Don't tell me how the character feels

[5/13, 22:08] Harry Wiwongko: I think that's irrelevant

[5/13, 22:09] Harry Wiwongko: Ok

[5/13, 22:09] Harry Wiwongko: How should i put this

[5/13, 22:09] Harry Wiwongko: Hendrik is special

[5/13, 22:09] Harry Wiwongko: He befriended calvin and they together learn about their soul

[5/13, 22:10] Harry Wiwongko: But Hendrik had a moral mishap and failed as a human being

[5/13, 22:10] Harry Wiwongko: But after realizing the true meaning of friendship he came out of the ordeal as a better person

[5/13, 22:10] Harry Wiwongko: .
