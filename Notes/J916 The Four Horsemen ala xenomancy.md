---
## Consider not having fullcaps for headings.
title: Inspiration
## We don't need a subtitle for now
subtitle: The Four Horsemen ala xenomancy
## Information about authorship(s)
author: Hendrik Lie
date: 16 September 2019
## Fonts and formatting.
fontfamily: times
fontsize: 12pt
linestretch: 1.25
indent: true
papersize: a4
## Numbersections
numbersections: true
secnumdepth: 2
---

# 20190916

What if, during the second ambush of Aditya to Steven's family, especially his sons, David and Company also gain parts.
In the original situation, Steven found that David was already been captured, while he managed to secure his other sons.
There are Ashton, Zean, and Michelle, but Steven said they helped enough.

What if Ashton, Zean, Michelle, did more than going home as ordered by Steven?

David, Ashton, Zean, Michelle had been well known since the Demo Wave of Denefasan, they didn't hide their identity.
So the moment Aditya's men came, they're the first to help their fellow classmates and schoolmates to leave, while holding up the bad guys.

David is dubbed The Death Goo with his classmates, because his use of Steven's Armor is often remote, compared to Steven's style that let him attach the armor to his body.
And David's armor usually took a more fluidic form, changing shapes at will.

Ashton is the Origamist, because his armor comes in form of sheets extruding from his body, and folds in a manner people could only describe as origami folds.

Zean is called the Dust Master, because his autonomous utility fog pet has a foggy appearance, and can disperse like dusts, and recombine into various structures.

Michelle is the Pole Dancer, but she prefers Sun Wu Kong.
Her weapon of choice is the telescopic smartmetal pole, that could change its width and length at will (maximum length is 15 meters, from 2 meters).

They're considered local superheroes.

So what if in that battle, they were lost because they're surprised, caught off-guard.
David was then captured, and Steven came to meet them.
Steven told them to go home, and left for David.
But they decided that they should be able to help somehow, and they came just in time, when Steven was about to kill Aditya.
They managed to stop Aditya's men, enough for Adran to secure Steven, and they left the scene.

But it means Michelle would've known that Steven's wounded.
David-Anthony arc must be updated to reflect it.
Moreover, Michelle would've known that Aditya is the one wounding Steven.
Ergo, it would be less conceivable for David-Anthony arc to happen.

It is better, then, to focus merely on the battle on the school, and have them returned home afterwards.
Steven would take care of it, and Adran would take his own initiative to save Steven.
They must be knocked off during the fight.

Also, could they be, the four horsemen?

Conquest, War, Famine, Death.
And, according to Wikipedia entry on the Four Horsemen of the Apocalypse, it is summarized as: These four are then summed up as follows "They were given power over a fourth of the earth to kill by the sword (war), famine, and plague and by the wild beasts of the earth".

The Death Goo, The Duster, The Origamist, and The Pole Dancer.
Could it be seen that the Origamist is the Conquest, that the Death Goo is the Death, the Duster as the Famine, and the Pole Dancer is the War?

The Duster had his weapons a swarm of micrometer sized robots, I believe it counts as some sort of plague.
With the ability to reshape and reconfigure themselves into various forms, it could allow various feats, including camouflaging, and offensive, by disassembling.

The Death Goo had a flexible armor, that is better described as a Genius (Wikipedia: In Roman religion, the genius (Latin: [ˈɡɛn.jʊs]; plural geniī) is the individual instance of a general divine nature that is present in every individual person, place, or thing.
Much like a guardian angel, the genius would follow each man from the hour of his birth until the day he died.
For women, it was the Juno spirit that would accompany each of them.)

Then, the Sun Wukong or The Pole Dancer, um, this is hard.
Okay, on another note, there's a classic chinese novel that had a telescoping staff as one of the featured weapons.
Ruyi Bang.
From wikipedia:
Ruyi Jingu Bang (Chinese: 如意金箍棒; pinyin: Rúyì Jīngū Bàng), or simply Ruyi Bang or Jingu Bang, is the poetic name of a magical staff wielded by the immortal monkey Sun Wukong in the 16th-century classic Chinese novel Journey to the West.
Anthony Yu translates the name simply as "The Compliant Golden-Hooped Rod", while W.J.F.
Jenner translates it as the "As-You-Will Gold-Banded Cudgel".

Then, The Origamist, capable of forming any form of weapons from his fabric of clothes, by folding them into various shapes.
Given that the power comes from the Power Beads, it has enormous energy content that, for practical purposes, its energy reserve is practically limitless.

So, there's it, the Four Horsemen ala Xenomancy.
