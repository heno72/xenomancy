---
title: Can Daniel and Derictor be Genetically Full Brothers?
author: Hendrik Lie
date: 2019-06-17
tags:
  - alteration
  - poi/dan
  - poi/rik
---

# Can Daniel and Derictor be Genetically Full Brothers?

So the scenario went like this:
due to poverty and hardships, Derictor's and Daniel's fathers that are identical twins had to be adopted by two different families:
The Lusien Clan and the Wijaya Clan.
So their fathers, Cornelius and Carlond, um, Wijaya...,
becomes Cornelius Wijaya and carlond Lusien.
It was done when they were just a few months old, and the fact of that exchange is never known to them, that it was practically "forgotten".

Fast forward, Bella Liesander and Beth Liesander, another pair of twins, enter the scene:
Bella fell for Cornelius Wijaya, and Beth fell for Carlond Lusien.
Their marriage differs by about five years apart.
Beth and Carlond Lusien marries first.

Cornelius Wijaya is a financial auditor with fascination to knives, and has an extensive collection of knives and blades of various era and designs.

He would always carry three fold knives on his suit, most of the time seamlessly hidden to his choice of attire.
One at his shoe, one on his left sleeve (the inner side), and one that shaped like a card on his wallet.

Whenever he's startled, he'd reach for his sleeve.
But he startled a lot.

His obstacle is of course his safety concerned wife, that doesn't want to have all of those knives on her house.
So he decided to work on cities outside her domicile, only to return to her at the weekend.
He always has two safe houses, one that's clean and sterile for when his wife is visiting, and right beside it, that he made to appears unoccupied, is the house where he hides his extensive collection of knives and blades.

He never enters his secret house from its front gate, but uses a secret passage on the study room of his sterile safe house.
His only son knows about the passageways and sometimes sneaks there.
His relationship with Bella strained somewhere around the time Derictor is going to the university (he's intentionally sent to study at other provinces so that he would not see their fights).
The reason was that Bella accuses him of cheating from her, when she discovers that he had a secret bank account (that he uses to fund his hobby) and suspiciously large sum of money withdrawn periodically rom it (to purchase new, rare collections by cash).
He couldn't admit that he's still into knives and blades, so he admitted that he was having an affair.

Ironically, Bella was saved by Cornelius with his knives, when there was a thief that breaks in their residence.
He was killed as well in the fight.

Continues to [Derictor and His Knives](/Notes/J815-derictor-and-his-knives.md).
