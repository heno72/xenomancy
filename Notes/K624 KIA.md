# Alteration 20200624 KIA
The Intelligence Agency is structured in a manner that is so decentralized, that each department works independently.
Each departments collect informations, process them, and send action requests to appropriate action departments.

Okay, so it had been incorporated to the Gitlab document for KIA.
However, I realized something after I divided them into coordinating departments, information gathering departments, and action departments.
We have both Department of Esotericism, and the 5th Division of the DFO.

What if the 5th Division of the DFO is actually a result of a joint operation between DFO and DoE?
So to streamline action and command structures, DoE let their operative branches to be incorporated to the DFO.
It is not just the MTF Nine Tailed Fox, but the entire division that becomes the 5th Division of the DFO.
So the rest of the DoE branches are non-operative branches.
