# Person of Interest

|   # | Tag         | Name                     | Description                                                                 |
| --: | :---------- | :----------------------- | :-------------------------------------------------------------------------- |
| 721 | #poi/steve  | Steven Pontirijaris      | A half-human etoan doctor aspiring to experience life as a human being      |
| 722 | #poi/dan    | Daniel Lusien            | A doctor with ample amount of curiosity and loves science fiction           |
| 723 | #poi/do     | Fernando Suryantara      | A doctor with interest in learning and experimenting new things             |
| 724 | #poi/lena   | Helena Irawan            | A doctor with hidden agenda and ridden with guilt                           |
|     | #poi/rik    | Derictor Wijaya          | A very prolific knife-thrower hobbyist and a reliable financial accountant. |
| 992 | #poi/ashton | Daniel Ashton            | One fine etoan raised on Earth at the dawn of human civilization            |
|     | #poi/leaf   | Leaf the Solenadactilian |                                                                             |
|     | #poi/adran  | Adok Ranensis            |                                                                             |
|     | #poi/tudva  | Tudva Pontiritarax       |                                                                             |
|     | #poi/hendr  | Hendrik Lie              |                                                                             |
|     | #poi/manov  | Romanov Dexter           |                                                                             |
|     | #poi/ein    | Heinrich Potens          |                                                                             |
