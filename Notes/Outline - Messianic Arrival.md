---
tags:
  - messianic-arrival
  - outline
  - poi/ashton
  - poi/tudva
  - poi/adran
  - poi/leaf
---
For [Messianic Arrival draft](/Chapters/Xe-1%20Messianic%20Arrival.md)

The outline is more or less as follows:

1. [Ostaupixtrilis Pontirijaris](/Characters/Daniel%20Ashton.md) - be introduced, and be told on his upcoming (undesired) adventure to Earth
2. [Tudva Pontiritarax](/Characters/Tudva%20Pontiritarax.md) - be reminisced about their arrangements on Earth, and how they decided to settle up and build a family there before they decided to just leave. All being told while exploring the city for the last time before his departure. The purpose was to root him up on reasons to continue despite his reluctance.
3. [Leaf the Solenadactilian](/Characters/Leaf%20the%20Solenadactilian.md) - be told the reason why Os's instance on Earth drifted from the plan, and how his instance there already set up a back up plan for Os' entry should he failed.
5. [Adran](/Characters/Adok%20Ranensis.md) - preparatory briefing on the state of Earth since their last departure, as he is the only one constantly connected with his instance on Earth. He would be the one that transport him from Aucafidus to Earth, akin to SSH tunneling.

It would have to be split into two parts, I think.
We might even need to reconsider its titles, as the current one is unfitting.

## Good heavens

[Good Heavens](/Chapters/Xe-1%20Good%Heavens.md) would be a rewrites of the first half of [Messianic Arrival](/Chapters/Xe-1%20Messianic%20Arrival.md).
It would approach the telling of an upcoming mission not as directly as just stating the facts.
The attempt is to show how perfect Ostaupixtrilis Pontirijaris' life is on the surface of [Aucafidus](/Notes/planet-aucafidus.md).
Then to show that he was being surrounded by people he cared about, and he seemed to be content.

Until it was disturbed and he had to make a choice.

### Ostaupixtrilis Pontirijaris

It was a normal in the buzzing Hanging City of Sidsido Ahidei.
The sky was clear, and the wind was strong.
Structures hanging from the great cables, and air cars buzzing around like flies from a distance.

People of various races and sizes buzzing around one of the local plaza.
They were approaching from above, and land on one of the landing pads.
It was in one of the hanging islands that was best known to be the Earthtown.
For majority of its locals are either descended from Earth or are from Earth.

They took a breakfast at ["Black, Red, and Litters"](/Notes/L925%20Daniel%20Ashton%20at%202kyo.md), Os' favorite food stall.
Then they played frisbees at the park with BRL's employees that are currently on their shift breaks.
However Leaf had their stature changed, for they had receive a message.

They informed him, that circumstances changes, and he needs to leave for Earth immediately.
He was hesitant, because he had grown complacent with the conditions at his estate.
### Tudva Pontiritarax

He was taking a walk with Tudva, spending the evening together.
She was brought again to the BRL, where Os had her proposed two thousand years ago, after he was returning from being Jesus Christ.
They recounted the reason behind his proposal to her.
On this very restaurant, he learned that if he want someone to enter his life, he had to ask them, or there might be no other chance to ask them again in the future.

Os asked Tudva out, to honor them.

Then she asked why would he brought back that memory again.
There are two reasons, first is that they were an uplifted dog.
Just like that in his tribe on Earth, but was made to be an intelligent being.

Secondly, the reason he decided to stay on Aucafidus after his trip to Earth was because he planned to retire, just like them.
There was simply not much more reasons to continue on, there wasn't any worth doing after he contributed to Kav's plan on Earth.
He has peaked, he said.
But he didn't have the heart to leave Tudva, Leaf, and Adran behind, so he didn't share it with them.

He wanted to decide to retire here at the restaurant, living his last incarnation, as much as his biological body would age naturally.
He thought it was a proper way to honor them, however circumstances said otherwise.

It came to him, that maybe, just maybe, Kav came in his place, when it was about the time he felt he had seen everything there is to happen.
He said that he understood now how they felt, when he decided not to ask him to join him.
There wasn't a chance for them to expand their horizon, to find something new.
They couldn't see anything better to do in life, since no one asked them to participate in greater things.

It was Os' greatest regret.
And now Kav reminded him again of his greatest regret.
Except that unlike Os' younger self, Kav asked Os out.

So there they are again, in the same restaurant, Os said.
Os made a decision to go to Earth again, in honor of them.

## On earth as it is in Heaven

### Leaf

Leaf and Os are discussing the details of their mission.
One of Os' interest is why and how his instance on Earth failed.
Leaf explained how that came to be, and how his instance on Earth was at least as smart as Os in that he prepared a backup plan.

The back up plan that he was about to enact was made by a shadow of him.

See [M716](/Notes/M716%20Messianic%20Arrival%20Skeleton.md)

Leaf would start with the encounter of Calvin and [Hendrik](/Characters/Hendrik%20Lie.md).
They would go in some details on how Calvin honed Hendrik's abilities.
A primer on how SOUL works, a brief on Earth immigration customs, and what it means for Os.

Leaf would then disclose another aspect of *their* counterparts on Earth.
Os was not the only one leaving an instance back on Earth.
In fact all four of them did.
Except for Adran though, the instances of the remaining three of them would definitely have diverged.
However Leaf reasoned that it must be good to know that there are at least familiar people on Earth.

Leaf did emphasize though, that Leaf and Tudva on Earth are on a completely different, independent line of mission, that he must not ask for help from them to resolve his own mission.
However Os was relieved to know that Adran on Earth would still be the same entity as Adran he knew here on Aucafidus.

### Adok Ranensis

A brief on how it is possible for Adran despite being separated more than a hundred lightyears from one another, could still be a single entity.
It was primarily through the use of wormhole networks, that regular synchronization between Adran on Earth and Adran on Aucafidus is possible.
It was through the synchronization channel, that Os would be delivered on Earth's counterpart of Adran.

Consider [Bewitched](/Chapters/Bewitched.md).

Adran and Os are preparing for the trip to Earth.
The preparation involves sorting out Os' memories that he's allowed to bring to Earth.
It was because on Earth he would not have the nearly unlimited space for memory storage, and that he would still spare some rooms for merging with his instance there.

It would also be a primer to introduce deep time as they would reside under Adran's internal virtual space.

Adran insisted on trimming down details, while maintaining the overall flow of narrative.
He however, let Os be the judge of which details to keep on his thoughts for caching and indexing purposes, while the rest would go on Adran's memory frames for storage.
At the same time Adran is indexing memories that should be temporarily suppressed according to the specifications of Kav for the purpose of his mission.

As they scoured through the body of Os' extended memories, Os discovered the memory of their first meeting.
He played it and Adran was watching with him, especially on their conversation at that particular day, and the surprise he had on knowing how Leaf decided to adopt Adran.
He was still puzzled on why Leaf decided to adopt Adran, and reasoned at first it must be due to Adran's extreme usefulness as an ATV.
Later Leaf must have developed some attachments with Adran, akin to parent-child relationship.

Adran reminded him on Solenadactilian absence of familial relationship.
Adran reasoned it must be that he was practically slaved at first, but it would soon develop into a mutual respect between him and Leaf.
Leaf might be without easily recognizable emotions, and tend to be extremely logical, but it cannot be denied that their society benefits from cohabitation and social pairings.
It must be the kind of mutual respects between solenadactilian members, more like colleagues, than familial bonds.

Leaf is heartless, but they are not evil, Os concluded.
Adran added, which is why Leaf is more than willing to be in civil partnership with Adran, Os, and Tudva.
Adran too, see the benefit of that partnership coming from their mutual respects, on which he reached for Os' virtual hands.

It was fun, Adran said, and if he could turn back time, he would keep choosing to be in this civil partnership, as this civil partnership is what made Adran be the one it is now.
Also, Adran added, that through this civil partnership Adran decided to have an offspring.

Os was surprised, for it was definitely not a decision made with him, rather with his instance on Earth.

## Don't leave

Note that September 2019 was the date Cal supposedly had an accident.
At 2021-04-06 was the date for Angel's Job.
It must then be the date range of which we can have the first two chapters here to have taken place.
Note that The Trip was no longer in the same time as the [Angel's job](Chapters/Xe-1%20Angel's%20Job.md).

Note that [The Artifact Hunt](Chapters/Xe-1%20The%20Artifact%20Hunt.md) happened on 2021-04-07, and I think we want to anchor our time around that.

Note that 2021-05-13 was the date Charles was supposedly last seen.
It must be then the upper limit of when Charles merged with Os.

Then at around September 2021, Ashton enrolled at David's school.
It must then, be beyond the merge.

What we actually want is that the merge must have happened around 2021-04-07, but before [The Raid](/Chapters/The%20Raid.md) on 2021-04-18.

Hendrik went home to rest after The Artifact Hunt, so maybe we will put the slot there.
However according to the master plan, the [[Xe-1 The Antichrist, The Beast, and The Christ]], the timeline must be that the merge came first, then the Angel's Job, then The Artifact Hunt.

I suppose I would have to sort them out soon.

**Edit 2024-01-13:**
I suppose the timeline should be:

- 20190906: The Lost Son, the date of Cal's accident
- 20210405: The Arrival, transfer was complete by the night, and Hendrik's dream occurs near midnight.
- 20210406: Angel's Job, Heinrich's adventure to intercept EPL's ritual to summon Tiamat. It is a sign of the beginning of the cycle, where Yam's order conflict Hadad's will. Concurrently, it was the day Os talked with Charles
- 20210407: The Artifact Hunt, Hendrik's adventure to fight EPL skirmishes on obtaining information about the Black's Staff, but his quota was depleted by Heinrich the day before.
- 20210418: [The Raid](/Chapters/The%20Raid.md)
- 20210418: [The Lead](/Chapters/The%20Lead.md)
- 20210506: [Edward Kevlar](/Chapters/Recruitment%20of%20Umbra%20Cahaya.md), also see [Black's Staff Theft](/Notes/J224%20Black's%20Staff%20Theft.md)
- 20210513: Charles was last seen, upper limit of the merge.
- 20210522: [The Mass](/Chapters/Mass%20of%20Umbra%20Cahaya.md)
- 20210817: [Signs](/Chapters/Signs.md)
- 20210817: The Dawn Strike
- 20210817: [Distrubances](/Chapters/The%20Disturbances.md)
- 20210817: Inspections
- 20210818: The day after The Dawn Strike
- 202109XX: Ashton enrolled to David's school

I really have to create an official timeline soon.

### It was like another person

<!-- Internal date: 2021-04-05 -->

Starts with Hendrik returning home from his internship.
Heinrich greeted tersely, saying that the dinner is almost ready.
Hendrik nodded, and proceeds to clean up.

Manov came sneaking from the balcony, even though Heinrich said it is useless to sneak when all three of them are capable of clairvoyance.
Old habit dies hard, Manov muttered.
He proceeded to do the laundry at the bathroom, even though Hendrik complained that he is still taking a shower.

One plate is served at the table, and they sat down.
Manov took the plate, but the plate didn't move.
Heinrich took the plate, but the plate also didn't move.
Now there are three plates virtually identical in appearance.

They proceeded like a normal dinner, with each of them asking how the other are doing.

Office where Hendrik is being rejected due to his colorblindness.

Gauss Electronic Components

Connection with the meeting of Calvin Gauss

No one ever confirmed the presence of Calvin that night, not even in the official reports

Sleep with Manov, Heinrich is at the balcony.

Hendrik is thinking back about Cal

Dream about apartment, and beside him was Cal

Reliving the pain, apologizing

Cal is not Cal

Os

Three Ways War

Team up between Hadad and Kav

If Os is allowed to contact Hendrik, it means it is authorized

Soul Shearing

Charles

**Probably discarded:**
Heinrich as an angel was quite busy today, as he is dealing with a visa violation of a large volant in Merapi.
Hendrik reminded them to spare the shared quota of the three of them, on which Heinrich replied that it is not even enough.
Hendrik said the office was scammed by one of the client, that said it was just a small patch of land, so the boss gave the most generous rate, only to find out that the land was actually beside a mainroad.
Manov said he was from Integra, dealing with some apex soul scouting, as the time is near the next red thread cycle, however he got distracted as a dolphin member of the task force is asking for more.

