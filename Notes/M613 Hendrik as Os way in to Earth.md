---
## Consider not having fullcaps for headings.
title: Inspiration
## We don't need a subtitle for now
subtitle: Hendrik and Os' way in to Earth
## Information about authorship(s)
author: Hendrik Lie
date: 13 June 2022
## Fonts and formatting.
fontfamily: times
fontsize: 12pt
linestretch: 1.25
indent: true
papersize: a4
## Numbersections
numbersections: true
secnumdepth: 2
---

We've established at first that Aucafidian can't get more than one instances of them exist on Earth. That's why Os can't get to visit Earth when Charles is still there. We also know that Charles, as Calvin, met Hendrik and helped Hendrik to develop metagenesis.

Then, the story goes that because Os is not allowed to exist (have a body?) on Earth, he had to convince Charles to merge with him. He would be existing as a virtual entity, communicating with Charles via dreams. But Charles has no soul. Original solution said that when Michael cleanslated his memory, he develops soul. But it's too convenient. What if Charles never develop one?

What if Hendrik did another metagenesis to provide a graft to reconstitute Os as a virtual entity using Hendrik's soul shard? Then it explains why Calvin had to meet Hendrik. It isn't just so that Hendrik would help Steven in the future, but also so that in the event of Cal's failure to leave Tony earlier, Os on Aucafidus can provide support to reset his state and start over? Hendrik is an insurance that Os' role could stay on its track.

Then the birth of Daniel Ashton is simply a merger of Hendrik's soul shard that contains Os, and Charles' body, making Ashton a human body with an Etoan soul. Making him whole. Or maybe the shard is simply a mean to facilitate the merge.

Also it simplifies what they're going to talk about on Aucafidus between Leaf and Os. Leaf is going to explain the encounter of Hendrik and Calvin to Os (as Os left Earth before Calvin era), and why is it important on making an entrance back to Earth. Therefore fixing Hendrik's role to help, and helping us to get a viable content to the story.

Also another suggestion, is to have a glimpse of Hendrik's present day family. He had a wife named Matahari Tanputri. Had two sons, David and Peter Lie. And in his house, vocal conversation is not encouraged, they're encouraged more to sign instead. So his household is voluntarily mute. It is consistent with Hendrik's liking of silence, and Matahari's liking of animals, hence she favors emotional and gestural communication over spoken language. The family is also a polyamorous one as Hendrik is not alone, he has Manov and Heinrich. All of them also loves Mata, and Mata also loves them. The children considers that they have three fathers. They're taking turns to raise their children, mirroring Steven, Fernando, Daniel, and Helena.

Also they're all reflexiors, with Mata is a green mage with many animaloid familiars. Perhaps that's how Manov get to know Ket, perhaps because Macan is her familiar. Perhaps also how Manov got to know Purpose, because Purpose is also a good friend of Mata. Perhaps that's how Xiangyu get to know Heinrich, because she was Xiangyu's student as well. She's also a pyromancer, naturally (well, her name).

Perhaps that's how Hendrik got to understand the basics of pyromancy (the base of Heinrich's education is Hendrik's former basic in pyromancy), and that's how Hendrik understands the need of contracts between a reflexior and their familiars in the fight with Solomon. Perhaps the fact that Hendrik has pseudo familiars (well his own shards and metagenesis instead of actually acquiring other virtual entities through contractual agreements), and Mata finds it amusing and interesting.

Perhaps Mata is simply an embodiment of Jie, but with a better twist, Hendrik is straight, so he doesn't have to think twice about having Mata in his life. He didn't hesitate when he meets Mata again ten years after their high school graduation. They get married and have children after Hendrik becomes a lawyer. It's 2019, Hendrik graduates from his master of law study and becomes a lawyer, then he marries Mata (a more proper name might be needed), and in 2021 he had a twin.
