# 20200602 Alteration: KIA
K Intelligence Agency, often just mentioned as the Intelligence Agency, is a transnational clandestine operation founded via a joint effort of Kukergroup and Securion International. It is a massive operative spanning many countries, though most of the Kukergroup assets are concentrated in southeast asia, while the rest of the world is supplemented by Securion infrastructures.

When I first made BAIK, I composed the following agents.
Active:
- Sukarno, Hendrik Lie (2018)
- Hatta, Herman Toro (2020)
- Klandestin, Rafael Hariadi (2021)
- Jokowi, Nicolas Armaniputera (2017)
- Pancasila, Edwin Kurniawan (2019)
- Soeharto, Henokh Lisander Putera Henobic (2009)
- Dua, Therella Lie (2015)
- Tiga, Nasha Viorella (2009)
- Empat, Andre Tjahaya Purnama (2009)
- Enam, Anderson Pondalissido (2009)
- Delapan, Derictor Wijaya (2009)
- Sembilan, Stefanus Selka (2009)

Defunct:
- Lima, Bain Armaniputera (2009-2021)
- Tujuh, Graham Prasetyo Putera Amdal (2009-2016)

However I later learned that Agents are inappropriate for inner circles. Agents are usually those that officers in duty assign to do special deeds, as a part of the officers' local intelligence network.

Meanwhile, for the new, reformed BAIK, now known only as K Intelligence Agency, or simply the Intelligence Agency, I have to draft a new structure.

KIA Departments.
1. Department of Coordination.
Henokh Lisander is the Chief of Executive Officer.
They are the command center of the entire Intelligence Agency.
2. Department of Operation.
Officers stationed in this department is called Field Operation Officers. The Chief in Command is Col. Chandra "Chan" Watthuprasongkh ("จัน" จันทรา วัตถุประสงค์) (m, 54). He is very close with Michael (calling him Nong Mich), as they went into training at a similar time, and spend years together. Highly determined to do his jobs, very friendly to anyone, and knows every single one of his team very well. However, he's very emotionally attached to his team, that losing a squad member is almost always devastating for him.
    - Lt. Col. 강해인 (m, 48), usually called 해인. Head of the **1st Division, *Frontline Responder.*** A bright, cheerful, and playful person. Also the chief of the G28 Forces. Barely shows his emotions, always mask it with smiles and positive vibes. Very perceptive to other individuals, and highly sensitive to changes in mood, and emotional distress.
    - Lt. Col. 吳脚踏 (Go Jiaota) (f, 51), usually called Ms. Go. **Head of the 2nd Division, *Medical Support.*** A calm, quiet, and stern person. Dislikes unorganized actions, and always executes everything with precision and determination.
    - Lt. Col. 王实地 (Wang Shidi) (m, 52), usually called Wang. Head of the **3rd Division, *Heavy Artilery and Armored Vehicle Combatant.*** However, he's quite messy when it comes to his own belongings. Does not hesitate to jump and save his colleagues, even when it may cost his life.
    - Lt. Col. Djunita (f, 52), usually called Djun. Head of the **4th Divison, *Logistic.*** A bright and cheerful lady, always trying to liven up her squad by throwing jokes (often bad or fail, but, well). Never wish to hurt anyone, and always try to get close with everyone.
    - Michael "Mich" Carmichael (m, 52). Head of the **5th Division, *Esoteric Counter-Response.*** Calculative, assertive, and analitical in battlefield, while in a social setting, he is quite warm and welcoming, although somewhat reserved. Hendrik's Reflexior mentor, and a close friend of Chandra. An agile reflexior, that can perform small-scale minor reality bending relatively quickly. A hard and wide-scale reality bending requires more time of focusing.
3. Department of Treasury.
Officers stationed in this department is called Treasury Officers.
    - Dr. Angelica Gears (f, 50), usually called Dr. Gears. Head of Robotics and Artificially Intelligent Agents. A replacement for Dr. Bain Armaniputera. She is highly perceptive, a perfectionist, and like to tidy her belongings and appearances. Very adaptive and resourceful in battle.
    - Dr. Ninawati Berlina (f, 49), usually called Watt. Head of Manufacturing and Production of Armed Vehicles. A strong woman, with a warm heart.
    - Dr. Emma Bright (f, 48), usually called Emma. Head of Memetics and Crowd Control. A hypersex and playful woman. At the same time being very emotionally matured, high self control (except for sex), and very intuitive.
4. Department of Surveilance.
5. Department of Normalcy.
6. Department of Innovation.
7. Department of Esotericism.

For Code Names, I am reconsidering that. Our previous convention was to refer to them as Agent [Codename]. Provided that we will no longer call them agents, it seems, unnecessary.

The codename is added as it was inspired by the Kingsman. However in this new iteration, KIA is more like a regular, national intelligence agency, it seems unnecessary. Fake names would be provided for specific missions that requires it. Meanwhile non-operative officers might not need it at all.

Officers would have staffs, that work in a compartmentalized way that there is no way they'd know that they're working in the Intelligence Agency. Officers mostly know that they're working for an agency, a clandestine one indeed. Inner circles would be the only ones that know about the Intelligence Agency.

Perhaps the most identifiable would be their Intelligence Agency insignia, which looks more like a vertically conjoined M and W. Most gadgets, Agency assets, and armored vehicles they have are engraved with that symbol, and that's all about it.

Meanwhile, for the original BAIK casts, in this KIA iterations, since they've been seen to be participating in some missions, they might be one of the task force. Henokh Lisander might not be one of the operative, so most likely he's the current Chief Executive Officer of the Intelligence Agency.

However, since most of the MTF Samsara in Integra from 1993 would be seniors by 2020s, perhaps we can use some of the names of Apex souls, that around 2020s would be those in early to mid 20s? Elbert Hardiman and Ferdian Gautama are out of the picture, as they would harbor the souls of their conterparts from Integra. What do we have?

1. Dominic Muerte. His counterpart in Integra is one of the operative officer of the Butterfly Initiative. His counterpart helped to chase down Bright. In Integra, his past life was Juan Muerte, his own grandfather. Interestingly, as they're not a reincarnation of one another in Xe, both Dominic and Juan could exist at the same time, hence:
2. Juan Muerte.
3. Nurhayati Maulidia. Is a tall woman that is having an internship in a notary office. Is a girlfriend to Bright. She knew Hendrik (In) as they were from the same universities (both Unair and Ubaya).
4. Zarah Walidah. She is one of the operative officer of the Butterfly Initiative, that chased down Hendrik (In), along with Steven and co. I'm tempted to have her as an opposition, merely because she is, in Integra. Note, that she's in her mid 20s already in 1993, most likely as senior as the original member of MTF Samsara.
5. Bright Spears is a Tachyon soul. A prosecutor, that loves to sing. He likes to use his ability to run away from his co-worker for the purpose of having some free time with music that he loves.
6. Stella Lie is a Freud soul. She worked in a property agent. She's very people-person, knew a great deal about the thoughts of others. Can predict people's behavior, and she exploited it to sell properties.
7. Ezekiel Tanputra is an Erickson soul. He is a veterinarian student that lives in AM8, while having his univ on the other side of the city. His cousin, Keenan Tanputra, is a friend of Hendrik (In). Quite reserved, he rarely talks and spend most of his time with his friends. Since Keenan is most likely Hendrik (Xe)'s friend as well, then:
8. Keenan Tanputra.

I think I will have Nurhayati Maulidia to the team. Nur is a strong, and agile tall lady. However, she's sweet, sensitive, and caring. She's very good at drawings, and very perceptive. I think it would made her a very good spy or infiltrator. Perhaps she'd be in place of Klandestin, instead of Rafael Hariadi. Rafael had no development whatsoever, so let's consider him replaced.

Bright Spears, I'm quite attached to him already, as he's lighthearted, and a good singer. Perhaps he's a polymath with many skills, almost like Daniel HS, or perhaps closer to Keenan Agape. Perhaps more Keenan Agape, a chemical engineer (his counterpart in Integra is a prosecutor, perhaps it doesn't really fit his actual dreams, that's why he tend to run away from job in Integra). He almost finished his civil engineering courses, when he decided it is enough and pursue chemical engineer later. He graduated at a normal graduation age, as he finished early in high school. I think he would make a good field agent, especially as an early response armed team, in disguise. Or as a guard. Perhaps he originated from the G28 forces. Consider him a replacement for Stefanus Selka. Meanwhile, his namecode, doesn't need to be Pancasila either. I was thinking, Arjuna, because of his name, Bright Spears. Okay, not an Archer, but let's stick with that, than to stay with Pancasila.

So, here we are, with the 9 members of the task force Nine Tailed Fox.

[**MTF Psi-72 "Nine Tailed Fox"**](Notes/KIA%20MTF%20Psi-72%20Nine%20Tailed%20Fox.md)
- [Hendrik Lie](Characters/Hendrik%20Lie.md), Sukarno.
- [Romanov Dexter](Characters/Romanov%20Dexter.md), Airlangga.
- [Heinrich Potens](Characters/Heinrich%20Potens.md), Srivijaya.
- [Nicolas Armaniputera](Characters/Nicolas%20Armaniputera.md), Jokowi.
- [Andre Tjahaya Purnama](Characters/Andre%20Tjahaya%20Purnama.md), Empat.
- [Anderson Pondalissido](Characters/Anderson%20Pondalissido.md), Enam.
- [Derictor Wijaya](Characters/Derictor%20Wijaya.md), Delapan. His strength is in martial arts and skilled with knives.
- [Nurhayati Maulidia](), Klandestin. She's highly adaptive in most social settings, and is very good at obtaining information without the target aware of it. When her cover is breached, she's very capable of handling a fight.
- [Bright Spears](), Arjuna.

The organizational structure is ad-hoc, so there's no leader, just nine of them of equal status. They could be broken down to smaller units according to needs.

## CONCLUSION:
1. Rafael Hariadi, codename Klandestin, is replaced by Nurhayati Maulidia.
2. Stefanus Selka, codename Pancasila, is replaced by Bright Spears. His codename would be Arjuna.
3. Romanov Dexter is included as an official member of the MTF Nine Tailed Fox, codenamed Airlangga.
4. Heinrich Potens is included as an official member of the MTF Nine Tailed Fox, codenamed Srivijaya.

## Update 20200624
Department of Defense could be joined with Department of Operation.
As defense operations could be carried well by the DoO anyway.
Meanwhile, I'd replace it with Department of Coordination.
DoC would be supervising all other departments and act as a hub to connect all of the informations and channel them to appropriate departments.

So The Intelligence Agency works like this.
Each departments have their own specialization, and function specificially.
Every single one of them are independent from one another.
Basically, to their knowledge, they're doing their job and route reports to the Department of Coordination.
Department of Coordination would process the report and route it to relevant departments.

At first, I separated them into:
- Department of Coordination.
- Department of Operation.
- Department of Treasury.
- Department of Surveilance.
- Department of Normalcy.
- Department of Innovation.
- Department of Esotericism.

I think it is wise to rearrange them accordingly, into coordinating departments, action departments, and information collection departments.
I also took the liberty of changing DoO into DFO, because it sounds better like that.
DFO stands for Department of Field Operation.

1. Department of Coordination, and Department of Treasury would be coordinating departments.
These departments function like the brain and the heart of our body.
They ensures that everything is coordinated, and there are funds to do so.

    Department of Coordination oversees everything, make decisions, and route orders as necessary.
    Department of Treasury would ensure that all facilities are taken care of, applying upgrades as necessary, and procure new facilities.
    Actually, Department of Coordination and Department of Treasury are equal, and can operate independently.
    They're the departments that can make their own decisions, and issue orders to other departments.
2. Department of Surveilance, and Department of Normalcy would be information collection departments.
Department of Surveilance functions as a way to look for specific patterns that needs to be acted upon.
Usually what to look for is issued by the Department of Coordination.
Whatever patterns that are significant or are requested by the Department of Coordination, would be reported back to the Department of Coordination.

    Department of Normalcy functions as a way to see, if there is anomaly from baseline.
    It would alert the Department of Coordination if anything went off the baseline normalcy, so the Department of Coordination can act accordingly.
    This is like a check and balance to ensure that the operation stayed covert, and appears ordinary.
    Unlike Department of Surveilance, this department did not take orders, they're just reporting.
    So this is more like a barometer, while Department of Surveilance act more like a probe.
3. Department of Field Operation, Department of Innovation, and Department of Esotericism would be action departments.
The remaining three departments act solely on orders.
However, DoI can issue suggestions to DoC and DoT.

    DFO would initiate actions with objectives and constraints provided by the DoC.
    Occasionally, they'd take orders from DoT, but it would consist mostly on procuring new facilities, or repairs and maintenance of existing ones.
    There are armed tasks, and there are civil tasks.

    However, not all assignments are possible to be done, simply because of the constraints of the common sense.
    This is where Department of Esotericism came to the field.
    They allow unconventional or esoteric methods to get things done.
    However, they're the smallest department so far, consisting of less than a hundred officials.

    DoE is not a part of DFO simply because they have their own chain of commands, that are less structured like that of DFO.
    Also because it is not normally possible to incorporate esoteric command structures and individuals of DoE into any other departments.
    Perhaps the most they can do to incorporate DoE to DFO is by the inclusion of DoE's operative branch under the command of DFO.
    So the 5th Division of the DFO is actually from the DoE.

Given that the divisions had been outlined, we can move on how each departments are organized.
DoC and DoT are tied closely to their frontends.
DoS and DoN are spread over many frontends and operations.
DFO is comprised mostly of select individuals from G28 Forces, Securion Corps, and many other.
DoI is comprised of academicians, and privately funded research facilities from both Kuker Group, Prion Group, and many more smaller institutions.
DoE is perhaps the most esoteric: some people refer to them as powers or djinns over certain jurisdiction or areas.

1. DoC consists of the CEOs of companies from both Kuker Group and Prion Group.
Their concern would be first and foremost, the core goals agreed upon by the founding members.
Welfare of all employees, and make profits to support the first goal.
After the employees are all taken care, they'd focus on spreading the welfare to the countries they're operating.

1. DoT on the other hand, are comprised of CFOs of companies from both Kuker Group and Prion Group.
Included to DoT are Treasurers of the G28 Forces.
They procure and provide facilities and maintenance, and those are their sole functions.
Other matters didn't concern them.

1. DoS are comprised of news institutions, press, journalists, lawyers, notaries, judges, prosecutors, investigators, and many more associates.
Their modus of operandi sometimes involve exchange of favors and informations.
They are comprised of networks of individuals or professionals, often piggibacking at other facilites to obtain information.
All of informations obtained by every associates were sent to and compiled by nodes.
Each nodes would be clustered into task forces, processing all of the relevant informations, exchange results with other nodes, and reported directly to the DoC.

1. DoN are comprised of statisticians, observers, and academicians.
They observed trends, instead of particular events or situations that DoS conducted.
They are more spread out and thinner than DoS.
They are grouped into associated topic nodes, that individual officers of DoN would report to.
Topic nodes would exchange and associate all of the reports to be submitted to the DoC.

1. DFO are select individuals from both G28 Forces and Securion Corps.
They are often called Oneiric Protocolers, because they are signees of the Oneiric Protocol.
From G28 Forces are about eight hundred strong protocolers, and twice as much from Securion Corps.
They were sworn for secrecy, and compensated accordingly.

1. DoI produce innovations and new technologies that DoT would acquire and produce.
They are divided into many smaller institutions, and private research facilities.
Most are disguised as commercial research facilities.
Most of them are institutions or research facilities acquired by DoT, and majority are not even aware that they're a part of DoI.
Chief Researchers are elected and assigned to each institutions and facilities to oversee the researches and answer directly to DoT.
Like DoS, they are comprised of networks of individuals or professionals, and grouped into nodes.

1. DoE are the most esoteric, with members comprised of about a hundred strong individuals.
They are comprised of many races, most notably members of *Homo sapiens reflexi*, *Ursus ignitus*, and *Stenella coeruleoalba reflexi*.
All of them are associated with an extragovernmental organization known as **the Watchtower Foundation** (the WTF).
Unlike other departments, they have no frontends, as most of our modern civilization didn't consider them to exist.
Likewise, they have no need to have frontends, as they are already invisible for modern civilization.
