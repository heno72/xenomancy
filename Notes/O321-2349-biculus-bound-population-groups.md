---
title: Biculus-bound Population Groups
subtitle: Habitat environmental bias of various population groups
author: Hendrik Lie
date: 2024-03-21 23:49
fontfamily: times
fontsize: 12pt
linestretch: 1.25
indent: true
papersize: a4
numbersections: true
secnumdepth: 2
tags:
  - zet
  - alteration
---


| Code | Gravity   | Atmosphere                      | Note                                                                                                                  |
| ---- | --------- | ------------------------------- | --------------------------------------------------------------------------------------------------------------------- |
| SE   | ~1.0 g_au | ~1.98 bar                       | Near Aucafidus environment                                                                                            |
| ME   | ~0.7 g_au | ~0.99-1.98 bar (often 1.32 bar) | A significant fraction of space bound civilization chooses this region. A favorite alternative for Earth civilization |
| LGE  | ~0.3 g_au | ~0.67-1.32 bar                  | Primarily for space habitats                                                                                          |



Apparently I had a note about this matter long time in the past.
The distinction is apparently based on three primary bias of surface acceleration and atmospheric pressure:

1. Standard Environment (SE), is an environment configuration mimicking Aucafidus' natural environment. It is characterized by surface acceleration of near 12.43 m/s^2, and atmospheric pressure of near 1.98 bar. 
2. Medium Environment (ME), is an environment configuration that might be lighter than Aucafidus but is still comfortable for humanoids. It is characterized by surface acceleration of 2/3 of that of Aucafidus, or around 8.3 m/s^2, and atmospheric pressure between 2/3 of aucafidus (around 1.32 bar) up to near 1.98 bar.
3. Low Gravity Environment (LGE), is an environment configuration that is significantly lighter than Aucafidus, and the atmospheric pressure is significantly lower. It is characterized by surface acceleration of 1/3 of that of Aucafidus, or around 4.14 m/s^2, and atmospheric pressure of around 1/3 of that of Aucafidus (0.66 bar) up to 2/3 of that of Aucafidus, or 1.32 bar.

I think it is primarily caused by the number of colonizable worlds in Biculus star system.


| Celestial Body          | Type                  | Gravity (m/s^2) | Note |
| ----------------------- | --------------------- | --------------- | ---- |
| Pixtrilis               | Mercurian             | 3.82            | LGE  |
| Ulix                    | Mercurian             | 3.88            | LGE  |
| Aucafidus               | Eugaian               | 12.43           | SE   |
| Malaucar                | Waterworld            | 8.85            | ME   |
| Vadelatur               | Arean                 | 3.99            | LGE  |
| Romic                   | Ceres-like            | 2.45            | LGE  |
| Subralis II             | Waterworld            | 2.33            | LGE  |
| Subralis III            | Waterworld            | 2.69            | LGE  |
| Subralis IV / Pranale   | Waterworld            | 3.39            | LGE  |
| Subralis VI / Rabala    | Captured rocky planet | 8.18            | ME   |
| Subralis VII / Kavrile  | Waterworld            | 4.18            | LGE  |
| Subralis VIII / Handora | Captured rocky planet | 7.99            | ME   |
| Subralis IX / Uraal     | Waterworld            | 4.76            | LGE  |
| Subralis V / Atrium     | Rocky moon            | 5.77            | LGE  |

It is safe to say that SE population would be the smallest fraction, as such environment is rarer in cis-Subralis and in Biculus system region in general.

I think historically, it is conceivable that the first few colonized worlds would have been ME worlds.
So those ME worlds are probably old colonies, and Aucafidian can thrive there, especially since the gravity is very near Earth, and that most life in Aucafidus, despite has been adapted for high gravity environment, is still derived from Earth.

So when the Subralis captured rocky moons are colonized and are accordingly terraformed, they might want to replicate their world's environments.
That also includes their 1.98 bar atmospheres, since it would not be much a problem either, as the jovian system is quite rich in volatiles.
Malaucar is also rich in volatiles, so colonizers there would also like to trim the atmosphere into something like 1.98 bar.

Then at later date, they will also deal with low gravity environments, as their technology advances and more modifications are possible to Aucafidian colonizers.
Therefore it is conceivable that LGE worlds are colonized primarily by genetically modified Aucafidian.
Because of that, they are also the most diverse and numerous.
They are numerous because they are able to navigate well in low gravity, and hence spaceships are their natural habitat as well.

There is also possibility that a number of terran inhabitants would prefer more in ME habitats.
This variant would probably prefer the 2/3 Aucafidian atmospheric pressure, hence the 1.32 bar variant.
It is simply due to the fact that such mixture would be the closest mixture to Earth environment.

I think it is appropriate to say that LGE adapted entities can survive in an even lower atmospheric pressure.
Therefore their atmospheric pressure range might be in 1/3 to 2/3 of Aucafidus (0.66 bar to 1.32 bar).
Though I think lower pressure variants would mostly inhabit space habitats and spaceships, while higher pressure variants would be on-world inhabitants.
Their diversity would also be reflected in their numbers, since probably Biculus is a busy system with plenty of space habitats.

When Subralisian bands are created, they are created primarily to cater majority populations of all three groups.
Therefore the bands are grouped into:

- SE Band: 12.34 m/s^2; 1.98 bar
- ME Band: 7.24 m/s^2; 1.98 bar
- LGE Band: 3.78 m/s^2; 1.32 bar

Meanwhile, Aucafidian surface and bands are there to cater mostly SE, ME, and terran variety,:

- Surface (SE group, vanilla variant): 12.43 m/s^2; 1.98 bar
- First Band (ME): 8.63 m/s^2; 1.98 bar
- Second Band (Terran variant): 7.35 m/s^2; 1.32 bar
- Third Band (ME, LGE): 5.52 m/s^2; 0.99 bar
