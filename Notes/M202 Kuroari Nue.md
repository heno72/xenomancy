---
## Consider not having fullcaps for headings.
title: Inspiration
## We don't need a subtitle for now
subtitle: Kuroari Nue, an addition to Dawn Strike
## Information about authorship(s)
author: Hendrik Lie
date: 02 February 2022
## Fonts and formatting.
fontfamily: times
fontsize: 12pt
linestretch: 1.25
indent: true
papersize: a4
## Numbersections
numbersections: true
secnumdepth: 2
---

In the Dawn Strike, there's going to be a moment where the team is split, distracted, while Aditya is facing Anderson.
I think in the original old script for the Iron Fist, they were fighting a Kuntilanak.
Now we know it's straight away, um, sacrilegious.
So we'll use it to incorporate our latest alterations.
Nue Kuroari, and Solomon.

Nue Kuroari is introduced as a way of Hendrik, toying around with one of his favorite manga, or basically just want to have an anime sword scene.
The sword is wielded by one of his shadows, and the sword itself is one of his shadows.
So it consumes two shadows.
It is used specifically to combat volantformes in the manent realm.
I think Hendrik is inspired with the Tengu Incursion event, since they use swords.

Solomon Sacrilege, is a mage on the side of the EPL, a friend of Anthony Matthias.
She's in possession of a volantformes Asmodeus Gavin, a dragon.
However in most of the time he's in this world, he's disguised as a falcon.

The scene is going to be portrayed as follows: Hendrik stated how he wish to have an anime fight when he first learn that he's different from feral humans.
So the recent incursion of Tengu Incident made him realize he had almost zero preparation to have one.
That's why he and his shadows managed to master the form, so when the time comes, he can have his own proper anime battle.
The sword is of course, a Tier 3 ability: meta transformation.
It wouldn't be hard since Hendrik could master the Tier 4 ability: metagenesis.
While he's explaining it to an unknown character (I imagined it to be a man, but maybe it is most likely to be Nurhayati, since Enam is missing), his shadows are fighting anime style with the dragon.

Apparently they were having a hard time at first, with the first sword transformation into the swarm form failed, as the dragon breathes fire.
However the second attempt is by using the second transformation, where the ant swarm develops earth towers around the dragon, imbuing them with the Shadow Seals, made of strings of metas, a technique made possible with meta transformation.
The seals prevent the dragons from manifesting outside the cage.
That's when Hendrik's shadows performed the last stage of the anime fight: a kill move.
It's basically just a deportation spell done dramatically.

Solomon was enraged, and manifested a weapon in the shape of a violin shield and a bow sword, made of lights.
Hendrik stated how sacrilegious her move was, using fine instruments like a violin to be her weapon of choice.
They were about to fight, when suddenly the Earth crumbles and mounted out, consuming her with it.
Heinrich came and told Hendrik to stop toying around.
Instead he instructed Hendrik to rejoin, as it is getting intense in the headquarter.
Hendrik conjured two more shadows to shape black flying pegasuses, and ride on one.
While the anime sword wielder shadow of Hendrik jolted there in an instant.

Explosion is heard, and Bain is located.

While they were panicking around the main building, Anthony arrived at the previous battle scene.
He called Aurelia, and she swam under the ground and retrieved Solomon, that was buried alive.
Later in their camp, Solomon learned from Anthony that Asmodeus was deported back through the Antarabhava to his former paramundus.
Solomon begged Anthony to use his powers to retrieve Asmodeus as he was her best friend.
Anthony said he couldn't do that, unless Asmodeus went to hell.
But he knows someone that can.
That's when Manov is summoned, and after learning the situation, said he couldn't do that, other Hendriks would found out and scold him.
So he told them, that he'd just call Ket.

Ket was summoned, but he was naked, and complained that he's in the middle of a business, that is fucking a blond babe.
Manov stated the request to retrieve Asmodeus, and Ket said he'd just return to his business first before searching for Asmodeus.
He disappeared.
However later that day he appeared in his lion form, with Asmodeus, his neck is on Ket's jaws.
He reverted back to human form, holding Asmodeus' neck on his hand.
He said that the blond babe got pissed off that he left middle action, he couldn't get relieved, and got no babe to fuck, while tossing the Asmodeus to the ground.
Then left.

Anthony commented, how Manov is collecting weird friends: a horny dolphin, and an exhibitionistic lion.
Manov said, yeah you included, a necromancer with a daddy issue.
Anthony said fair enough, but that's because Manov just helped him.
