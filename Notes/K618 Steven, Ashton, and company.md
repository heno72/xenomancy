---
tags:
  - poi/steve
  - poi/dan
  - poi/sun
  - poi/lena
  - poi/ashton
  - poi/david
  - poi/zean
  - poi/michelle
---

# Inspiration 20200618

## Intro
The interactions between members of [The Archangels](Notes/KIA%20MTF%20Alpha-19%20The%20Archangels.md) vs [The Four Horsemen](Notes/KIA%20MTF%20Gamma-42%20Four%20Horsemen.md).
It must be carefully crafted, considering that they're of different generations.
Especially to consider that Ashton, being literally the great grandpa of David, is actually very lively, active, and energetic.
The model I'm going to use for The Archangels would be SBFIVE, particularly Tae and Tee.
For The Four Horsemen, I would use OXQ, particularly, I'm pretty certain that Ashton is similar to Pavel.

## The Archangels
Given that the Archangels are all doctors, um, I forgot what to say.
Point is, in the original design, Fernando is actually shorter than Daniel.
But I was thinking that I should reverse it.
Fernando has a darker complexion, and Daniel is having a brighter complexion.
Thick and penetrating gaze of Daniel, and soft, and charismatic gaze of Fernando.
It is also easier for Daniel to develop his mustache, just by not shaving in a couple of days or so.
Fernando is almost always clean-shaved.

### Steven Pontirijaris

As previously explored, the main reason Steven wanted to study as a medical doctor, is because he wanted to do something etoan baseline doctors often can't, actually diagnosing and curing people with the advent of etoan tech.
Also a part of his curiosity on how different a human body is compared to etoan, especially etoan-human hybrid.
He's doing it for the sake of curiosity and experience.

### Daniel Lusien

Daniel, in the other hand, has his *situs inversus totalis* as the main drive of his passion.
At first, he wanted to be a doctor solely because he want to learn more about his own medical condition.
Over time, he enjoyed studying medical doctor, because all of the intricaties, and beauty of a working biomachineries in a human body.
Especially the muscloskeletal system.
His *situs inversus totalis* did not just spark his interest in medic, but also saved his life once.

### Fernando Suryantara
Fernando, decided to enroll to a medic school because he wanted to get to see naked male bodies.
The problem is that, he's dissapointed a lot when he knew the suboptimal conditions of averaged human body that died: of old age, fat, etc.
However he's able to interest himself in studying the beauty of human body and its workings.
It wasn't as hard as he imagined.

### Honorable Mention: Helena Irawan
Out of curiosity, what about Helena?
She has to be developed too, it is not fair if we just develop the men.
Her mother died of alzheimer, is not something inconceivalbe either.
I think her focus of research would be neurodegenerative diseases.
If we think about it, she's quite noble.
But is that all?

Hobbies, I think that is one more way to look at her.
She loves to help people, so she joined congregations, and choirs, and other religious stuffs.
Even though she's always having a conflicting doubt in her heart, for the fact that her husband is practically an atheist.
Not to mention that the living incarnation of Jesus is her grandfather-in-law, and is a classmate of her son.

I think, she's the kind of person that could easily overthink a lot of things.
She overthink a lot that she concluded, by adopting etoan tech to her body, would mean she's denying her God.
She had this framework in her head, that her husband, despite seemingly violate the religious tenets she believes in, is a godsent husband.
But disregarding her husband, she's quite active everywhere, helping everywhere, and everyone respected her, as long as they don't mention her husband.

As a somewhat mainstream christian adopter, she originally believed that being gay is wrong.
However, equally conflicting to that, is the fact that she accepts Daniel and Fernando, as Jesus taught her to love the person, hate the sin.
But talking with Ashton, whenenver they're reaching the zone of theological exploration, she usually backed away, because deep inside, she's afraid.
She's afraid that everything she learned so far, might be wrong.

I symphatesized with her actually, as she spent a lot of her life worrying unimportant details such as that.
She's here to represent most people, that stayed in their religion just because of fear.
Of fear, that if they strayed afar, they'd be doomed in hell, should their faith is actually valid.
In the end, her main desire is to be a good person, remained faithful to the end, and to bring the light of God to the world.

## The Four Horsemen
We should also consider, what they're studying, because well, obviously when I was in high school I don't have much image on what people usually want to choose.
David ended up having a nuclear physics degree.
Not exactly practical.
I mean, why would he needs to study it when Etoan could provide more information about it?

Steven have his own reasons for studying medical doctor.
David should have his own reasons too, especially Ashton.
I suspect they'd be doing the things that would be more about networking, or things that are novelties for etoan.

### Daniel Ashton
Ashton is similar to Pavel in many ways, especially the energetic soul he has, and the fact that both loves large bikes.
They're both multitalented, and very social.
I somewhat want to make Ashton a dirty grandpa type individual, considering that to him, sexual encounter is just as normal as any social interactions.
And there's this scene where he's having a conversation relating to consent in a sexual encounter, and he's reported by a more conservative classmate of him.
It is during the university years of Ashton.
Given that Ashton had been established to be a motorcyclist lover since about some 2013 era, I think it makes sense if he'd choose engineering.

> #### Background: Etoan technological progression circa 0.0000 CL
> Combustion engines are actually rarer for etoan, and humans spent majority of their industrialized existence by tweaking around with combustion engines.
> Etoan, since they have no natural fossil fuels, would most likely pursue electric cars first.
> How do they obtain electricity?
> From burning woods, I suppose.
> But then they're already around 1900s level of technology back in their year zero.
> And they actually have a transapient overseeing them.
>
> I suppose it is not inconceivable that the overseeing transapient can provide free electricity by nuclear fission reactors, the cheapest and simplest one to make.
> I imagined Dis Pontirijaris was more like the strategy Kukerises use, biological computer encased in a body, that can reproduce.
> Once the industrial supply chain is established, they can advance more to our information age in no time.
> Perhaps even more under a hundred years.
> The civilization would've been rapidly returning to high-tech in no time, as the transapient already know where to work on.
> I suppose, the cities led by Dis Pontirijaris would be far more at advantage than those that did not join the network.
>
> Given that in early days, combusion engine-powered car and electric car reached the marketshare of about 50%, but combusion engine wins over the following years as it is easier to just refuel with gasoline.
> It means that, in the lack of natural fossil fuel, liquid fuel such as gasoline analog (biofuel, anyone?) would've been harder to obtain, than the freely available, fission powered free electricity.
> Hence, since ancient times, etoan is more used with EV than we are today.
> A combustion engine in a vehicle, would be something unique, or at least rare for them.


Yes, I think Ashton would've preferred a mechanical engineering major.
Hey, an engineer like Pavel's role, P'Forth!
His research interest would be, wait for it, origami!
Specifically foldable structures, and reconfigurable folds.
After all, he's the Origami Knight.

I digressed.

### David Pontirijaris
Meanwhile, David can draw well, so I suppose he'd study arts.
Arts are something constant, even in high tech civilization such as etoan, and human arts aren't necessarily better or worse than etoan arts.
So studying it still offers new insights for David.
Or perhaps graphic design?
Yeah, graphic design is more likely.

### Zean Lisander
What about Zean?
What is his interest?
I haven't explored that yet.
He's the son of Henokh, but definitely that's not the only identity he has.
Let's see what we know of him so far.
He's easily startled, very attentive, and want to prove that he's good enough.
I think, he'd most probably ending up as a computer science student.

### Michelle Williams
Michelle?
What about her?
With her innate, computational precognitive ability, it would be much easier for her to predict people, though usually more effective if they're having a bad intention.
I suppose it would be interesting for her to study law, especially considering a carreer as a prosecutor.
Usually she's very focussed, and is able to distinguish professional and personal matter, to the level that sometimes it hurt those around her.
