---
## Consider not having fullcaps for headings.
title: Inspiration
## We don't need a subtitle for now
subtitle: Tudva on Earth (depreciated)
## Information about authorship(s)
author: Hendrik Lie
date: 9 January 2020
## Fonts and formatting.
fontfamily: times
fontsize: 12pt
linestretch: 1.25
indent: true
papersize: a4
## Numbersections
numbersections: true
secnumdepth: 2
---

# 20200109
What if [Tudva Pontiritarax](/Characters/Tudva%20Pontiritarax.md) is doing a mission as well? A helper.
Weaved deeper into the society of humankind, she's to be reborn the way [Ashton](/Characters/Daniel%20Ashton.md) did two thousand years ago as Jesus.
So they'd regroup again, as a team.
He's a part of the Divine Team, and the team includes [Adran](/Characters/Adok%20Ranensis), Tudva, and Leaf.

Adran is relatively unchanged, he's Steven's Adran, the helper of Ashton's descendants.
He'd be the only one remembering the entire mission, however the instructions were hardwired and he couldn't spoil it.
He'd be a guide, in fact he stored the guide for both Ashton, of the Christ Cluster, and Anderson, the Antichrist Cluster.

Tudva is to be reborn as Michelle Williams.
Knowing nearly nothing about the whole ordeals.
She's given the power of foresight.
The same courage and dexterity Tudva had is inherited to her.

Leaf is basically an avatar of Kavretojives, but it leaves no reason for him not to represent himself too on Earth.
He's going to be reborn as Xiangyu, the Dragon Dancer.
His activation is when he meets Adran.

Adran is the key, Ashton is the actor, Tudva is the Eye, and Leaf is the lock.
The actor must activate the Eye, and the Eye must spot the Lock, and then the actor use the key to open the lock.
And judgement day occurred when the lock opens.

It means Adran came here on Earth to provide guidance for Anderson.
So Etovexiri is represented by him, which is also an ordinary citizen.
He also prepared Steven's family for the second coming.
And when Ashton came to Earth, Adran knew it was the time for the Christ Cluster to start rolling.

This solution saves a lot of work because it streamlined the casts and there's no need to add more exotic characters.
Etoan colony's government is not centralized, but highly decentralized.
The true isocracy on Earth, because everyone has equal sayings and to create rules, with some appointed as executives.
Adran currently represents one of the twelve Etovexiri, and is in charge of ensuring the mission of A and C clusters be carried well.
It also means that the characterization of Tudva and Michelle could be merged.
And that Xiangyu is basically Leaf, but with emotions.

Like Leaf, Xiangyu would be encouraging and promoting, providing mentorship to Heinrich as it functions as Kavretojives' avatar like the way Leaf functions as Kavretojives' avatar on Aucafidus.
Xiangyu's polar bear body provides him with emotional layer, while at core he was Leaf the calculative heartless beast.
It enabled him to do separation of the triangle of fire, hence enabling deep mastery of pyrokinesis.
His impeccable intelligence allows him to understand vast array of information, seeing patterns, and producing accurate conclusion of things, enabling him to function as the interpreter.
That is why he is the lock, he guards the gate of information, as he is able to interpret things, but to open the information, he must first be unlocked by the key, the missing piece to assemble it all, Adran.
With Adran, they are able to unlock the knowledge required for the mission, that Ashton, the actor, can use and decide upon.

Adran may have the information, but to interpret it, they need Xiangyu.
The duo works this way: Adran is the official representative of Kavretojives here, that is the source of information, and Xiangyu be the one that interpret the information provided by Adran.

Tudva/Michelle, in the other hand, is like the right hand of Ashton, that Ashton can trust, uniting the unlikely union of B and C cluster, be the center of attention of both David and Anthony.
She is also taking an active role, be the one inspiring David and co.
to fight.
She's his second in command.

Ashton, in the other hand, has this job of orchestrating his team, that has arms in both directions.
He has a hand in the A cluster, that is Xiangyu, and he has another hand in the B cluster, that is Michelle.
In the C cluster, he's assisted with Adran, being the closest friends of the core C cluster members.
