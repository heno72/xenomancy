# Abstract {.unnumbered}

This is a story that explores the insurgency attack of Aditya and its effects.
A clandestine operation is conducted to stop an upcoming insurgency.
However the insurgents managed to steal a divine artifact.
What do they plan with the artifact?
 
Steven and Daniel got an emergency visit by Anderson, and he told them about the upcoming invasion of the beasts.
Meanwhile Ashton came to inform them about the Second Coming.
What on Earth is happening?

