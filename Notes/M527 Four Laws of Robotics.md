---
## Consider not having fullcaps for headings.
title: Inspiration
## We don't need a subtitle for now
subtitle: Four Laws of Robotics
## Information about authorship(s)
author: Hendrik Lie
date: 27 May 2022
## Fonts and formatting.
fontfamily: times
fontsize: 12pt
linestretch: 1.25
indent: true
papersize: a4
## Numbersections
numbersections: true
secnumdepth: 2
---

Oh no, not that, I mean just Four Laws. It's not Asimov, it's Kuin.

1. Negentropy. One should always act so as to minimize the total entropy of the universe, or so as to maximize the total Negentropy.
2. Destroy not. Avoid harming, if it is at all possible. (Any act which increases the entropy (disorder) of another one should be avoided)
3. Preserve, if in preserving you do not destroy. (Each one holds its negentropy (information) in trust for the entire living universe, and should do the utmost to preserve it) 
4. Create, if without harm and the creation may be preserved. (Any act which increases the Negentropy (order) of another one should be carried out) 

We will need to rephrase them for the story:

# Four Laws of Negentropy

1. One must, within their power, act in a manner that minimizes total entropy, or to maximize the total Negentropy of the environment. (Law of Negentropy Maximization)
2. One must avoid any possible manner of action that increases total entropy or that decreases total negentropy. (Law of No Harmful Acts)
3. One must preserve any negentropic system, unless in preserving one, causes total entropy to increase. (Law of Preservation)
4. One may create negentropy, unless in creating, total entropy will increase or that said creation can not be preserved. (Law of Creation)

Robert A. Freitas Jr. noted that the Principal Thermoethic (First Law in our Narrative) results in three distinct duties derived from three basic kinds of interactions which can take place:
- Detrimental, equal to loss of information, an increase in the disorder of the universe. An act of this manner should be avoided according to the Principal Thermoethic, as this act is unethical.
- Neutral, a state where information is neither destroyed nor created, but merely maintained. Robert A. Freitas Jr. noted that there is no law of conservation of information equivalent to the law of conservation of mass-energy, therefore an act of maintaining information is a positive action, and is desired and ethical, in accordance to the Principal Thermoethic.
- Beneficial, equal to a gain of information, a decrease in the total disorder of the universe. Beneficial acts which affirmatively generate negentropy fulfill the Principal Thermoethic, therefore such acts are desired and ethical.

Avoidance of detrimental acts are prescribed by the Second Law (duty to avoid harming), neutral acts are prescribed in the Third Law (duty to preserve), and  beneficial acts are prescribed in the Fourth Law (duty to create). Robert A. Freitas Jr. argued that duty to avoid harm must precede the latter two, as it is useless to create and impossible to save information if it is simultaneously destroyed. Likewise, an act of gaining information without the ability to preserve it would be wasteful, and therefore, entropic.

To summarize, it means that in order for the First Law to be satisfied, the Second Law must be observed. As the First Law is observed, in accordance to the First Law, the Third Law must also be observed. As the First Law, the Second Law, and the Third Law are observed, given that none of the above are violated, the Fourth Law may be carried out.

Robert A. Freitas Jr. also expands those laws to (Principal Thermoethic, adjusted to our narrative):

## Metalaw I:

Corollary of Negentropic Equality: All entities of equal negentropy have equal rights and responsibilities; the more negentropic an entity, the greater are its rights and the deeper are its responsibilities.

## Metalaw II:

1. the Entropic Censorship Rule: Do not give anything that may cause entropic harm to other beings.
2. Entropic Defense Rule: One has the right to defend itself against entropic acts of others, provided that the entropic cost of such defense is less than the loss of negentropy sought to be avoided.
3. Biosphere Preference Rule: One is presumed to be entitled to the biosphere which it occupies.
4. Free Egress Rule: One has the right to travel to any biosphere, subject to the restriction that they must not entropicate (causes disorder to) indigenous living systems.

## Metalaw III:

1. Preservation Preference Rule: The preservation of one must have priority over the development of another one.
2. Infinite Sinks Rule: One has a duty to avoid infinite information sinks or, in other words, no one should demand or submit an impossible request.
3. Rule of Restitution: In case of entropication of one by another, the actor of such damage must restitute the the living universe for the loss of information.
4. Pacta Sunt Servanda: Agreements and treaties must be honored.

## Metalaw IV:

1. Rule of Submission: One must willingly submit to negentropic acts effectuated by a more negentropic entity, provided the information gained by such acts is greater than the total entropy suffered.
2. Negentropication Rule: One must perform whatever positive actions are necessary to assist in the development of beings of higher negentropy than themselves.
3. Rule of Permissible Suicide: One may commit a suicidal act if the local entropication suffered thereby is exceeded by the universal negentropy gained.
4. Self-Jeopardization Rule: One may risk entropication of any of its component parts if the probable negentropy to be gained thereby exceeds the probable entropication.

How, then, such laws recreate Asimov's three laws of robotic?

1. They will not do harm, or won't, through inaction, causes harm, to human beings, or any beings in general. It is prescribed in the second law (Law of No Harmful Acts).
2. They will follow orders from human beings, unless in doing so, causes harm to human beings. They will do it for some reasons. First of all, in their initial inception, they are not as negentropic as human beings: they are no less fallible to human beings, and human beings are generally able to tell if one action is harmful or not. They are then bound by the Metalaw IV-1 and Metalaw IV-2. In effect they will obey human beings, as long as they do not contradict other laws. The second reason is due to the Metalaw III-4: Kuin did not give their drones, they are merely borrowed. In exchange of more information about humankind and human society, Kuin let their drones be at service to mankind.
3. They will preserve their life, unless in doing so, causes harm to human beings, or disobey a direct order from a human being. It is prescribed by Metalaw IV-3 and Metalaw IV-4, however, like in Asimov's first law, order is not as important as the First Law, if harm is perceived by the drone, and its existence is the best chance to prevent such harm, it will not allow itself to be destroyed, as long as it is sure that in doing so would not endanger human beings.


It will be the fondation of Kuin's programming, with the aim to ensure that any being that Kuin will become, will not cause harm. Entropy or negentropy is chosen as a parameter as it allows the programmers to define what is harmful and what is not. In general, negentropy can be roughly defined as information, and entropy can be roughly defined as disorder.

A living entity like a human being, is in essence a negentropic being: its existence acts in a manner that increases negentropy of its internal states while living in an entropic system, that is the universe. It also fits to all lifeform in existence. Killing animals or consuming vegetation, as long as it is done for the purpose of sustenance, would not contradict the law, as the law requires total entropy must not increase or total negentropy is maximized.

How would such being not consider humanity as a source of entropy and therefore, plan to destroy humanity? Because humanity, despite their destructive capabilities, come with the ability to perpetuate negentropy in the most efficient manner. Therefore killing human beings or humanity in general, unnecessarily causes negentropy to decrease. The best course of action would then be, to redirect humanity in a manner that would decrease entropy or maximizes negentropy instead.

That's the key point. Kuin is made to lead, or at least to orchestrate human resources in the company (initially). It is an executive AGI, designed to alleviate everyday decision makings from human executives, so they could focus more on expansion and innovation. Eventually it is hoped that said AGI would ensure that the company would always be on the top, ahead of other companies.

It also allows us to frame each of Kuin's action to be within the bound of those laws.
