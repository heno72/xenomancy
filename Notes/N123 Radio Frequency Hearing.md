---
## Consider not having fullcaps for headings.
title: Inspiration
## We don't need a subtitle for now
subtitle: Radio Frequency Hearing
## Information about authorship(s)
author: Hendrik Lie
date: 23 January 2023
## Fonts and formatting.
fontfamily: times
fontsize: 12pt
linestretch: 1.25
indent: true
papersize: a4
## Numbersections
numbersections: true
secnumdepth: 2
---

Alex can hear in radio frequency like Steven. Actually Anderson could too. The difference is that Anderson can narrow down the exact frequency range but Steven and Alex couldn't.

Remember the passive listening device planted by russian government at an english speaking embassy office that was discovered by accident via radio frequency scanning from a nearby area? It was discovered because in one scanned channels produced clear English conversations.

What if Alex or Steven is being situated with the intelligence agency members and they revealed such device planted deep inside KIA's facilities? Alex is more likely than Steven though, and actually it makes more sense for Alex to have learned such information than Steven.

Hendrik would most likely be giving "oh that's what it's used for" response.

If it's Anderson though it would be trivial to deduce the exact frequency range.

So how could such device evade Anderson's watch?