# The Origin of Daniel Ashton
To be precise, the first instance of Ostaupixtrilis Pontirijaris is an etoan baby stranded on Earth, ~15.000 years ago.
It could withdraw parallel to the story of Isaac and Ismael, or Cain and Abel.
Actually, Cain and Abel is a more interesting parallel, as Alt Parahomen precedes Has Homen, but Has Homen was favored by their parents.

It might went this way: Alt grew jealous to Has, and then as a new beautiful lady come to the world, Istar, she preferred Alt.
Both Alt and Has presented their gifts to Istar, but it is apparent that Istar favors Has as well.
Not because Alt's gift is unfavorable, but simply because Has is etoan.

The story goes that Alt hated the entire ordeal, and that the family also afraid of the skypeople taking Has back.
Alt has no one wanting him not be taken away.
Alt felt unwanted.
The only way for him to be favored, is that Has to be eliminated.

Added with jealousy over Istar, Alt killed Has, and made him an offering to Istar.
Which, grieving Istar accepted, so she could salvage whatever remains of Has.
On which, afterwards Istar left Earth with Has' soul.
And Alt was left with just Has's body, gazing at the floating black diamond, moving away to the stars.

Vipara was surprised to learn that Has died, and inquired who killed Has.
Vipara asked Alt, on which Alt said he isn't the keeper of his brother.
Vipara saw dried white liquids on Alt's clothing, and knew it all.

Vipara banished Alt from the village.
Alt said that people would kill him on sight.
Out of mercy, Vipara consulted to Vel.

Vel prayed to Hadad, and the pact was made with Hadad.
Vel fashioned Alt a sign on his forehead with Has's blood.
Hadad sealed the deal, that no people shall kill him.
That forever will Alt carry Has's blood, the gift of the Gods that was taken away by his very own hands.

And away Alt went, with Has's blood in his body.
He becomes the first immortal on Earth.

# RAW
Has Homen, son of Vipara (father) and Dakav (mother).

The story goes that there was a prayer to help Vipara and Dakav to get a son. Because Vipara and Dakav had been together in their old age without children. The shaman Vel had a vision after consuming the Guru Mushroom, that one baby shall rise from the ashes and they would raise him. The baby would be the best son, the brightest, and the strongest.

But here's a catch. The sky people would want him back, as a sacrifice, for giving them what they wanted, a son.

Dakav couldn't wait, and was honestly doubting the shaman's words. So she ordered Vipara to sleep with one of their slaves. And a son was born. Alt Parahomen. The little boy on their old age.

The sky was furious, and there was blight, Vel said, because Vipara and Dakav weren't being patient. And a black shard fell from the sky. It crashes on Earth and reduced to ashes, black ashes.

There are bodies, with pale skin, and pointed ears. They're asking for help. And they have a son.

Dakav and Vipara was there. The baby was the most gorgeous they've ever seen. From the ashes. And they see that the parents are the sky people. And they wanted the baby back.

Vipara set them on fire. Botched them all. And they ran to the village. Telling the entire village what they had seen, sky exploded and there was black ashes, and within are people pale as death, and they killed them. But they took a baby, so perfect they couldn't kill him.

And he was named Has Homen. A man from the ashes. And boy do he excel in everything.

He grew to be very athletic, very smart and sharp, and had supranatural powers (wifi sense). But he always feel displaced. Like out of this world.

He slept with many women in his tribe, and neighboring tribes. But all of them are too dull. Everyone are dull. Nobody understands him. Everyone was too simple. And his intellectual satisfaction has never been fulfilled.

Until one day, a black diamond flew above a field in the middle of the night. He had this desire to come closer, and he checked the site.

There he meets a beautiful woman. Just like him, she's pale, with white hair, and white eyes. Ears pointed. And a song not from this world could be heard. In the world almost devoid of modulated radio frequency signals, he could see patterns, songs, and then visuals, sensations. When she gazed at him, the pulse intensifies, and there's that, a language he never heard before, yet held so dear. A language of no words. Of comprehension.

"Who are you?" Said the woman.

"Who are you?" Said Has.

The woman smiled, she giggled.

"I'm Istar. Ishtar Laupatura."

"I'm Ostaupixtrilis Pontirijaris," replied Has.

Has turned silent.

In the language he never knew he has, he knew his identity.

"Precious moon. How long have you been here?"

"Where have you been?"

"It was my first time here."

"Is there anywhere but here?"

"Shouldn't there be more than here?"

Has paused.

Istar paused.

They paused.

But the song never ends. The song in radio frequency. And she was surprised, when it crossed across their symphony, the imagination that she's from the end of the world. Of a flat world.

"A flat world must have another side. Are you from the other side of Ara?"

Ara was Earth's old name, fifteen years ago, in Has's tribe anyway.

Istar gasped. She couldn't believe that Has genuinely believed that.

He was raised here, on Ara. And he never knew better.

Nevertheless they spent the night together, under the floating black diamond, in the middle of a grassfield, and the full moon was above them. Never did Has move his gaze from Istar, everything she said expanded his horizon.

All there is kilometers away from his tribe, wasn't even a percent from the entire Earth, that is a globe, not a disk. And the firmament is actually an empty space, so mind bogglingly big that everything there is, is literally nothing.

She showed him HD 28185, their home star. A star just like their sun, but was so far away they looked like dots. And she had been in many worlds. Each with their own stars as their sun.

And for the first time in his life, he met someone, that's not dull. Someone that's bright, fast, and vast. So wise and so alive. And he was in love.

Dakav wasn't happy about Istar. Neither do Vipara. She's a skypeople. And she is going to take Has away.

Dakav and Vipara asked Has to avoid her, to not talk with her. 

They killed Istar that night. And they burn the body that very night.
