# Alteration 20200728: Xe-1 Chapter Re-Structure

In accordance to 20200716 Alteration on Requirements for TEB, TEB is split into two.

Part 5: The First Wave, will be renamed into The Battle of Kendari.
Accordingly, all chapter names containing The Battle of Kendari (number) will be removed.

Part 7: The Second Wave, will be renamed into The Battle of Medan.
Accordingly, all chapter names containing April Fool 2027: The Battle of Medan / The 2027 April Fool Insurgency (number) will be removed.

Chapter The Esoteric Battle, due to a possible confusion with The Esoteric Business, will be renamed to The Esoteric Counter-Response.
