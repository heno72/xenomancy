---
title: Inspiration
subtitle: Explosion in a warground and Daniel
author: Hendrik Lie
date: 6 January 2020
fontfamily: times
fontsize: 12pt
linestretch: 1.25
indent: true
papersize: a4
numbersections: true
secnumdepth: 2
tags:
  - poi/dan
---

Explosion in a warground does not sound like the way it was usually portrayed in a movie.
In real life, it is like the air near you ripped.
*Zhoosh.*
That.

Daniel hated the way air ripped around him.
He ran toward the one throwing grenades around.
It was a woman, beautifully gracious.
If Daniel wasn't with Fernando already, he'd give his heart to her.
Fernando was the only one reason why Daniel did not hesitate to slap the grenade she threw with the back of his left wing surface.

The grenade returned to her, rolled toward her feet, and exploded.

"S, self-defense," he muttered.

The sight wasn't pleasant.
Open wounds on her leg stumps, her femurs could be seen, severe penetration wounds around her genitalia and abdnomen area.
Loads of fine projectiles from the explosion penetrated and scarred her skin area from beneath her.

She was breathing rapidly.
She couldn't even feel the pain, she tried to get up.
Endomorphines kicked in, a natural painkiller our brain produced shortly after a severe trauma.
It was our natural defense evolved to help us not feel pain after a severe attack from a wild beast, and is just enough to help us crawl away to safety.
Then we can feel pain in safety.
