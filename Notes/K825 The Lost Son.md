# Alteration 20200825 The Lost Son

Now that Part 1 is more or less ready, I should think for the part 2.
Particularly, TLS.
TLS started by the end of September 2019.
Cal wanted to take a break, why? Perhaps it coincided with the time Anthony realized his true nature, and no longer trust him enough as a friend? Oh, wait, Aditya found out that a lot of people joined the LUC because they're attracted with the duo Cal-Tony.
The influx of people disturbs the movement as most of them are not very loyal to the church itself.
They came with a wrong motivation.

This part is to critique GMS's approach, that it is okay to come with a wrong motivation, the most important thing is that they were drawn in.
Then they could furnish the newcomers according to their needs.
Aditya disliked that, that he confronted Cal.
Why would Anthony side with Aditya? Because Aditya made a valid point that Cal distracted him.
Perhaps Anthony would also made a realization that Cal was M.G., and M.G., was the head of the ORCA.
But Anthony liked M.G.

Skip over that, I was thinking about something else, how did Kav communicate with Yam? How do they discuss the terms of the contact? What are the basic terms?

- Biosphere first before humanity
- Humanity shouldn't be eradicated, they must be relocated
- Kavretojives would take his share, while some must remains in the sol to determine their own fate.
- Denefasan would provide the means to help humanity so they won't perish immediately: an alternative living space as their original living space was taken away.
- Wake up El, so El would resume the Throne instead of Hadad.
- Take down the Divine Council, except for Yam and his select aides.

How do they communicate that?

- Understanding that nature is important and must be preserved, even in the expense of mankind.
Reflexiors don't have such restrictions, because they're mainly using the power of the sun and geothermal anyway.
So feral humans must be controlled.
- To control feral humans, EPL wished to educate as many of them to the ways of EPL first, so they don't have to destroy the Earth to get what they need.
But the existing system that already been in place, is not flexible enough to facilitate the change, so a revolution is necessary.
- Eventually before reaching the tipping point, humanity must be dealt with, so Cal thought that perhaps, a massive displacement of humanity away from Earth would be necessary, while those that remained in EPL's way, would remain down here.
Cal said that, for the argument's sake, if it is possible to displace humanity away from Earth, while ensuring that they don't perish immediately, how would Anthony feel about it? Anthony said it would be ideal, so they don't ruin Earth at least.
A win-win solution.
- Anthony pointed out that with the current throne of the Divine Council, it would be very unlikely, Hadad would care more about the economical output of the whole Earth system, in terms of soul exchanges.
Removing a massive amount of mankind from Earth would not be ideal for Hadad.
- Cal said that Hadad is worried more about El, but Anthony said that El had been missing.
If he existed though, Hadad would probably have to relinquish his power and El would take over.
If it happens, perhaps that is a perfect time to disturb the balance of the Divine Council.
Cal said, enough for an opening to destroy the Divine Council.
Anthony said, if one could destroy the Divine Council, yes.

Then why Cal hesitates to continue on? Perhaps it wasn't something that hard.
At this point he thought that he already had done everything he needs to do, and all of those information was enough for Kav to order someone else to finish it off.
That conversation was not even need to be shown.
What needs to be shown then?

1. That Cal is tired after many years of missions.
He asked Alex, his grandson, to help him out of this life, and move on to a new life.
A life where he could take a rest.
But he remembered about Anthony, a best friend of him.
A flashback would be shown, on the day they meets, the day they perform together, and the day Anthony and Cal had a talk that Cal felt tired.
Perhaps Anthony knew who Cal is: Migun.
Perhaps Aditya disliked Cal simply because it caused Anthony to be distracted.
Perhaps, the reason behind Aditya's dislike to Cal, simply because Cal stayed longer than he should.
2. Perhaps, Aditya again, wanted to use one's free will to torture or teach others lessons.
So Aditya made himself fight Cal, and let Anthony choose, to save Cal or to save him.
Aditya said he wanted to have a son that could choose his own path, it happened that the path Anthony must be choose would be either Cal's ways or Aditya's ways.
Aditya was almost being defeated by Cal, but Anthony, knowing that Cal is Migun, knew he wouldn't be affected with it.
Anthony fought Cal in the end, to make Aditya trust him again, while by the end, Anthony begged Cal to do his thing, so he had a reason not to kill Cal: he's not supposed to kill an ambassador.
Anthony pleaded, and that's when Cal realized Anthony's intention, and declared the ambassadorship.
3. Back to Cal and Alex.
Cal told Alex that, the most hardest part of his job is that he'd be very attached to the subject of his missions.
First Hendrik, then Derictor, then Xiangyu, and now, Anthony.
He should stay for as long as necessary, and move on when his mission is done.
But who can control their heart? Spending time helping them, he must not stay a bit longer than necessary.
The end result was like that with Anthony.
He decided to stay longer, and then Aditya came, a warning that he shouldn't stay any longer.
He had to part ways with Anthony, otherwise, a lot more things will come and overcomplicate the entire plan.
Cal said that all of his life, he could decide to stay as long as possible with anyone he wished to, and they wished to be with him as well.
But this mission tires him out because of the constant need to move on.
He wanted a break.
4. Alex, understand that, so he prepared etoan paperworks to relinquish his position as an ambassador.
He opened Ostaupixtrilis's file, and discovered, that apparently this body must be disposed first, to complete the contract.
This body contains specific physiology inherent to the role of an ambassador, so Cal couldn't keep it.
Alex asked again, after the disposal, would Cal want to be beamed back to Aucafidus? Cal thought for a moment, saying that being on Earth is his dream, and since he had been doing missions, he wanted to try to the fullest, the life on Earth.
He said that he wanted to be born again as a human, an authentic human.
Alex opened the file again, and said that, one of his best option would be to order a human body to the Divine Council of Earth, as human-specific genomes are owned entirely by the Divine Council, so etoan could not print human bodies, except to create human-like beings.
Cal said he'd take that.
Alex warned that it means, the rules that apply for mankind would also be enforced at him, sparring some exceptions inherent of being the citizen of etoan colonization initiative.
Cal said yes.

Apparently, that's the first part of the TLS.
It might be longer than expected, TLS must be split.
How?
1. Overtime.
Those we discussed above.
2. The Lost Son.
3. Messianic Arrival: The first part of The Birth of Daniel Ashton would be included here.
When Charles Lee came to be, Os's original copy was sent there to Earth.
But here, the right to own a body was held by his current copy: Charles.
So he remained simply a volant, he couldn't do much this way, so he seeks to contact Charles via his modified meta, and he is able to manifest in Charles's dreams.
4. Offers in a vacation.
Charles was enjoying his vacation, when an old memory of him came to his dreams, bothering him with a new mission that he doesn't wish to participate.
Then Fernando came, and Tee talked with him.
