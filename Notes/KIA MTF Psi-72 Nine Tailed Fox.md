# KIA MTF Psi-72 "Nine Tailed Fox"
Formerly KIA MTF Psi-72 "Flying Fox."
A joint task force under the Department of Operation of the Intelligence Agency.
Took direct orders from Colonel Chandra Watthuprasongkh.

The MTF Flying Fox specializes in carrying out missions that requires utmost secrecy.
Since the introduction of the 5th Division on the DFO command structure, MTF Flying Fox is renamed to MTF Nine Tailed Fox, that could also deal with unusual situations, especially those involving reflexior opponents.

Codenames are provided for their nicknames in communication and organizing moves.

| Division | Name | Codename | Description |
| :-: | - | - | - |
| 1st | [Anderson Pondalissido](Characters/Anderson%20Pondalissido.md) | Enam | An etoan superior, capable of transitioning his looks (skintone, body, features such as hair, etc) in a matter of days. |
| 1st | Nurhayati Maulidia | Klandestin | She's highly adaptive in most social settings, and is very good at obtaining information without the target being aware of it. When her cover is breached, she's very capable of handling a fight. |
| 2nd | [Nicolas Armaniputera](Characters/Nicolas%20Armaniputera.md) | Jokowi | A co-owner of the Sejahtera Medica clinic. |
| 3rd | [Andre Tjahaya Purnama](Characters/Andre%20Tjahaya%20Purnama.md) | Empat | He is huge, 185cm tall. Playful and strong, but somewhat calm and charismatic. Loves board games, and is a good tactician. |
| 3rd | Bright Spears | Arjuna | Like to sing, play instruments, very curious albeit not very bright (okay pun intended), and love sports. Love to brags on his knowledge, and also very witty. |
| 4th | [Derictor Wijaya](Characters/Derictor%20Wijaya.md) | Delapan | A proficient Knife Thrower and a martial artist, good with handling knives. Very determined on his goals, very hard on making decisions, and usually levelheaded, except when it comes about Nurhayati. |
| 5th | [Hendrik Lie](Characters/Hendrik%20Lie.md) | Sukarno | A lawyer and an engineer with the ability of soul shearing. Can control up to four concurrent copies of his meta, and borrow the powers of Manov or Potens to himself, or his metas. |
| 5th | [Romanov Dexter](Characters/Romanov%20Dexter.md) | Airlangga | The first meta fragment of Hendrik. Master in Medicine of Mammals and Volantforms, also a freelancer. His strength is in sensory ability and fine control with surgical precision. |
| 5th | [Heinrich Potens](Characters/Heinrich%20Potens.md) | Srivijaya | The second meta fragment of Hendrik. An Angel by profession, also a master in Pyrokinesis. His strength is in brute force, good on a broad-ranged attack, very poor in fine control. |

When they have to split into teams, they usually go with 3 teams, 3 members each.
Each of the team have their own "mages" (Hendrik and Manov always insisted that theyre not mages.)

## Squad Command Structure of the Flying Fox (pre 5th Division)
Command Team would organize their attack, including in providing clearance to engage.
Squad Leader provide orders and clearance, together with Assistant Squad Leader.
System Operator would operate and facilitate communication, and technical assistance, including drones, system infiltration, and such.
However, due to the lack of personnel, Kang Hae-In took both the role of Ass. Sq. Leader and System Operator.

Each fire team, except for the Command Team, is comprised of a Grenadier and a Rifleman.
Grenadier usually highly trained and specialized, while Rifleman is usually not specialized, or new recruits.

### Command Team: Ein Sof.
The ones issuing command for the operation.
Nine Tailed Fox is a special task force that answers directly to the Chief of the DFO.

| Position | Name | Codename |
| - | - | - |
| Squad Leader | จันทรา วัตถุประสงค์ (Chandra Watthuprasongkh) | Arthit |
| Ass. Sq. Ldr. and System Operator | 강해인 (Kang Hae-In) | Houston |

### 1st Fire Team: Alter Ego.
Specializes in infiltration and data collection.

| Position | Name | Codename |
| - | - | - |
| Grenadier | Anderson Pondalissido | Enam |
| Rifleman | Nurhayati Maulidia | Klandestin |

### 2nd Fire Team: The Strait.
Specializes in armed infiltration and stealth operation.

| Position | Name | Codename |
| - | - | - |
| Grenadier | Derictor Wijaya | Delapan |
| Rifleman | Nicolas Armaniputera | Jokowi |

### 3rd Fire Team: First Contact.
Specializes in heavily armed combat operations.
They can recruit select G28 Corps and organize them for actions that require large-scale assault.

| Position | Name | Codename |
| - | - | - |
| Grenadier | Andre Tjahaya Purnama | Empat |
| Rifleman | Bright Spears | Arjuna |

## Squad Command Structure of the Nine Tailed Fox (post 5th Division)
Since the inclusion of three members from the 5th Division, the command structure is renewed to be a proper squad team.
Wang Shi Di is recruited to be the system operator, while Kang Hae-In act as the assistant squad leader.
Team leaders are assigned, so the fire teams could be more independent, acting under their discretion to maximize the success of their fire team objectives.

### Command Team.
The ones issuing command for the operation.
Nine Tailed Fox is a special task force that answers directly to the Chief of the DFO.

| Position | Name | Codename |
| - | - | - |
| Squad Leader | จันทรา วัตถุประสงค์ (Chandra Watthuprasongkh) | Arthit |
| Ass. Sq. Leader | 강해인 (Kang Hae-In) | Houston |
| System Operator | 王实地 (Wang Shi Di) | Kothar |

### 1st Fire Team: Trojan Horse.
Specializes in infiltration and data collection.

| Position | Name | Codename |
| - | - | - |
| Team Leader | Anderson Pondalissido | Enam |
| Grenadier | Hendrik Lie | Sukarno |
| Rifleman | Nurhayati Maulidia | Klandestin |

### 2nd Fire Team: Virion.
Specializes in armed infiltration and stealth operation.

| Position | Name | Codename |
| - | - | - |
| Team Leader | Nicolas Armaniputera | Jokowi |
| Grenadier | Romanov Dexter | Airlangga |
| Rifleman | Bright Spears | Arjuna |

### 3rd Fire Team: Force of Nature.
Specializes in heavily armed combat operations.
They can recruit select G28 Corps and organize them for actions that require large-scale assault.

| Position | Name | Codename |
| - | - | - |
| Team Leader | Derictor Wijaya | Delapan |
| Grenadier | Heinrich Potens | Srivijaya |
| Rifleman | Andre Tjahaya Purnama | Empat |
