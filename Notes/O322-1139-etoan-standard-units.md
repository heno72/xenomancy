---
title: Etoan Standard Units
author: Hendrik Lie
date: 2024-03-22 11:39
fontfamily: times
fontsize: 12pt
linestretch: 1.25
tags:
  - aucafidus
  - alteration
  - measurements
---

# Etoan Standard Units

The interstellar unit of measurements is the **Etoan Standard Units** of measurements (**ESU**), also commonly known as **etoan metric**.
It is the most widely used system of measurement in the galaxy, employed in science, technology, industry, and everyday commerce.
The ESU provides a standardized system of units to ensure consistency and eliminate confusion caused by using different measurement systems.

## Base Units

The ESU is based on several fundamental units.
Below are base units used in everyday life.

| Dimension | Unit    | Definition                                                                                                                                                                 |            SI value | SI unit |
| --------- | ------- | -------------------------------------------------------------------------------------------------------------------------------------------------------------------------- | ------------------: | ------- |
| Length    | viter   | Distance traversed by light within 1e-9 sternus                                                                                                                            | `0.371726668801752` | m       |
| Mass      | wit     | Mass of 1e26 Carbon-12 atoms                                                                                                                                               |     `1.99264687992` | kg      |
| Time      | sternus | unit of time defined by taking the fixed numerical value of the unperturbed ground-state hyperfine transition frequency of the caesium 133 atom, to be 11398373422/sternus |  `1.23994669939879` | s       |

<!--
| Dimension                 | Unit | Definition | SI value | SI unit |
| ------------------------- | ---- | ---------- | -------: | ------- |
| Electric current          |      |            |          |         |
| Thermodynamic temperature |      |            |          |         |
| Amount of substance       |      |            |   `1e26` |         |
| Luminous intensity        |      |            |          |         |
-->

## Related Units

Below are everyday units derived from base units:

| Dimension | Unit      | Definition                                                                                 |            SI value | SI unit |
| --------- | --------- | ------------------------------------------------------------------------------------------ | ------------------: | ------- |
| Mass      | ke        | Unit of mass that is equal to 1000 wit                                                     |     `1992.64687992` | kg      |
| Mass      | dodeks    | Unit of mass that is equal to 0.001 wit                                                    |  `0.00199264687992` | kg      |
| Time      | cison     | A period of time that is 2^11 sternus long                                                 |  `2539.41084036873` | s       |
| Time      | sandik    | A period of time that is 2^6 sternus long                                                  |  `79.3565887615229` | s       |
| Time      | tosit     | An Aucafidian solar day (2^16 sternus)                                                     |  `81261.1468917994` | s       |
| Time      | jarwat    | A percent of a tosit                                                                       |  `812.611468917994` | s       |
| Time      | arsin     | A permil of a jarwat                                                                       | `0.812611468917994` | s       |
| Time      | verinsius | A period of time that is 407.14 tosit long (or 400 askitos long; 26682327.04 sternus long) |  `33084663.3455272` | s       |
| Time      | askitos   | A space-day (Askitos, lit: artificial day) is 25 Jadan (*votirjar sin jadan*) long         |   `82711.658363818` | s       |
| Time      | jadan     | A period of time that is 0.0001CL (verinsius) long                                         |  `3308.46633455272` | s       |
| Time      | derajat   | A period of time that is 1/5000 jadan long                                                 | `0.661693266910544` | s       |

## Different Units of Time

Note that Aucafidian units of time are unique in that they have three major time unit conventions:

- Scientific Time Convention, the de-facto convention that predates second civilization, primarily used in academics and computer timestamping.
- Aucafidian Time Convention, based on the progression of solar day in planet Aucafidus, popular in cis-Aucafidus region.
- Convenient Time Units, based on divisions of Aucafidian astronomical year, popular in space-bound Aucafidians beyond cis-Aucafidus region, especially those living in artificial habitats and other star systems.

### Scientific Time Convention

This is a group that uses etoan metric time units.

| Unit      | SI Value           | SI Unit | Note                                                        |
| --------- | ------------------ | ------- | ----------------------------------------------------------- |
| verinsius | `33084663.3455272` | s       | Astronomical year, used for long time periods.              |
| tosit     | `81261.1468917994` | s       | Aucafidian solar day, defined to be 2^16 sternus in length. |
| cison     | `2539.41084036873` | s       | Defined to be 2^11 sternus in length.                       |
| sandik    | `79.3565887615229` | s       | Defined to be 2^6 sternus in length.                        |
| sternus   | `1.23994669939879` | s       | A base unit of time on ESU.                                 |

This system is actually Aucafidus' original universal units of time that predates the current civilization.
It is a time convention that has been present before year 0.0000 CL, that is preserved in varous technical texts and textbooks when Aucafidian civilization is reset as an aftermath of a war that almost wiped out Aucafidian civilization.
The convention uses sternus as the smallest unit of time, and the remaining units are based on the power of twos (with the exception of verinsius as a time unit).

Due to its importance in science, it is the unit of time used in teaching and academic texts, and for timestamps.
Its value and definition was precisely defined that it is used universally across all known worlds.

The basic idea is that a day (*tosit*) is comprised of 2^16 sternus.
Then each day is further broken down into 2^5 (32) *cison*, whose length is 2^11 sternus.
Then each cison is further broken down into 2^5 (32) *sandik*, whose length is 2^6 sternus.
The reason of why 2^5 is used as a divisor is long gone, and in modern time it is thought to be arbitrarily chosen.

### Aucafidian Time Convention

This is the convention that is presently used in cis-Aucafidus region simply because the system closely matches natural progression of day in planet Aucafidus.
It is preferred by non-technical folks primarily because time units are split in base ten basis (with the exception of verinsius as a time unit).

| Unit      | SI Value                   | SI Unit | Note                                                        |
| --------- | -------------------------- | ------- | ----------------------------------------------------------- |
| verinsius | `33084663.3455272`         | s       | Astronomical year, used for long time periods.              |
| tosit     | `81261.1468917994`         | s       | Aucafidian solar day, defined to be 2^16 sternus in length. |
| jarwat    |  `812.611468917994`        | s       | Defined to be a percent of tosit.                           |
| arsin     | `0.812611468917994`        | s       | Defined to be a permil of jarwat.                           |

In this convention, a day (*tosit*) is comprised of a hundred *jarwat*.
Then a *jarwat* is comprised of a thousand *arsin*.
Therefore the passage of time in a day can simply be mentioned as percentages, where 50 *jarwat* is well known to be half a day, and so on.
An *arsin* is used only when smaller chunks of time is required.

### Convenient Time Units

This is the time convention used beyond cis-Aucafidus region.
The system is entirely based on the length of time of Aucafidian astronomical year.
Time is represented in terms of fraction of an astronomical year, and yet an artificial day (literal translation of *askitos*) matches closely to Aucafidian natural day-night cycle.
It also makes documentation and administration easier because a year is exactly 400 askitos long.

| Unit      | SI Value            | SI Unit | Note                                           |
| --------- | ------------------- | ------- | ---------------------------------------------- |
| verinsius | `33084663.3455272`  | s       | Astronomical year, used for long time periods. |
| askitos   | `82711.658363818`   | s       | A space day, defined to be 0.0025CL in length. |
| jadan     | `3308.46633455272`  | s       | A period of time that is 0.0001CL in length.   |
| derajat   | `0.661693266910544` | s       | A period of time that is 1/5000 jadan long.    |

A space-day (*Askitos*, lit: artificial day) is 25 Jadan (*votirjar sin jadan*) long.
A jadan is divided into 5000 derajat.
Derajat is an etoan time unit, of about 2/3 seconds long.  

The usual date-time convention when using Convenient Time Units is Year-ASK-JD, for example 27th September 2020, 10:57 UTC+07, or 67,550.1657CL, is written as: 67550-066-07, while in traditional calendar it is 67550-03-18.48.
Optionally, another set of four digits number would be appended to the back to indicate derajat: `67553-195-05 0191` (human datetime: 2024-03-22 15:04:16 +0700).
