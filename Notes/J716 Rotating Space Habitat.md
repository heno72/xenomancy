---
title: Rotating Space Habitat
author: Hendrik Lie
date: 20190716
---

Tangential stress ( $\sigma_t$ or circumferential stress) is the forces acting to the surface of a tube caused by the internal pressure $P$ and the total tension in the wall $T$.
If we define $D$ to be the diameter of the cylinder, and $t$ be thickness of the wall, and $r$ is the radius of the cylinder.

$$
\sigma_t = \frac{P \cdot D}{2 \cdot t}
$$

Centrifugal force acceleration is:

$$
F = \frac{ ( m  \times  v^2 ) }{ r }
$$

If we know the angular velocity ($\omega$),

$$
v = 2\pi r\omega
$$

Centripetal Acceleration is expressed as:[^c_a]

$$
a_c = \frac{v^2}{r}
$$

$$
a_c = \omega^2 r
$$

$$
a_c = \left(2\pi\: n_{rps}\right)^2 r
$$

$$
a_c = \left(\frac{2\pi\: n_{rpm}}{60}\right)^2 r
$$

$$
a_c = \left(\frac{\pi n_{rpm}}{60} \right)^2 r$$
    
Where:

--------- -- ------------------------
$a_c$     =  centripetal acceleration in $m/s^2$
$v$       =  tangential velocity in $m/s$
$r$       =  circular radius in m
$\omega$  =  angular velocity in $rad/s$
$n_{rps}$ =  revolutions per second
$n_{rpm}$ =  revolutions per minute
--------- -- ------------------------

[^c_a]: The Engineering Toolbox, Centripetal and Centrifugal Force-Acceleration.

    Accessed from: https://www.engineeringtoolbox.com/centripetal-acceleration-d_1285.html

Centripetal force is described as follows:

$$
F_c = ma
$$
$$
F_c = \frac{mv^2}{r}
$$
$$
F_c = m\omega^2 r
$$
$$
F_c = m \left(2 \pi n_{rps}\right)^2 r
$$
$$
F_c = mr \left(\frac{2 \pi n_{rpm}}{60}\right)^2
$$
$$
F_c = mr \left(\frac{\pi n_{rpm}}{30}\right)^2
$$

Now,  civilization Gamma[^gamma]  has a tube wall of $8 km$ high, $4,977 km$ in radius.
Greatest pressure is at $10^8 Pa$.
Assume carbon nanotube structures with maximum tension at $10^{11} Pa$, then:

[^gamma]: Civilization Gamma is just one of Avelican-42 civilizations, a fictional setting I wrote.

$$
\sigma_t = \frac{10^{11} Pa \cdot 4.977 \cdot 10^6}{8 \cdot 10^3}
$$
$$
\sigma_t = \frac{10^{14} \cdot 4.977}{8} Pa
$$
$$
\sigma_t = 6.22 \times 10^{13} Pa
$$

So,

$$
\sigma_t = \frac{p\cdot r}{t}
$$
$$
\sigma_t = \frac{F\cdot r}{A\cdot t}
$$
$$
\sigma_t = \frac{m\cdot a\cdot r}{A\cdot t}
$$

No, it is $\sigma_t$ that we seek, $p$ is material strength.

Actually, $\sigma_t$ must not exceeds $\sigma$ of the material, so $\sigma_t \le \sigma$, while $P$ as the internal pressure, is the pressure of which our material must endure from the inside, expressed in Pascals.

As $\sigma_t = \frac{p \cdot r}{t}$, we want to know thickness of the material, so $t=\frac{P \cdot r}{\sigma_t}$, and we can set $\sigma_t = \sigma$, which, for carbon nanotube, is in the order of $10^{11} Pa$, while for our calculation is $10^8 Pa$, so we get $t=\frac{4.98\times 10^8 \times 10^6}{10^{11}} = 4.98 \times 10^3 m$, ~5 km thick.

Wait, $P = \frac{F}{A}$, and since we were talking about areal density, $P \times A=F$, and $F=m \times a$, it is all about the weight of the landscaping, and the weight of the tube, which is dependent to the thickness multiplied  by support density and multiplied by acceleration.

In a way:

$$
\sigma_t = \frac{ \left( P_{landscaping} + P_{support}\right)  \times  r }{t}
$$

But then $P_{support}$ is:
$$
P_{support} = t \cdot a \cdot \rho_{support}
$$

and $P_{landscaping}$ is:
$$
P_{landscaping} = h \cdot a \cdot  \rho_{landscaping}
$$
Then
$$
\Sigma_P = t\cdot a\cdot \rho_{support} + h\cdot a\cdot \rho_{landscaping}
$$
$$
\Sigma_P = a \left( t\cdot\rho_{support} + h\cdot\rho_{landscaping} \right)$$
Now,
$$
L = h  \times  \rho_{landscaping}
$$
then,
$$
\Sigma_P = a \left( t  \cdot  \rho_{support} + L \right)
$$
Therefore,
$$
\sigma_t = \frac{\left( t  \cdot \rho_{support} + L \right)  \cdot  a  \cdot  r }{ t }
$$
$$
\sigma_t = a  \cdot  r  \cdot  \left( \rho_{support} + \frac{L}{t} \right)
$$

Since $L$ is essentially the areal mass of the landscaping, we can redefine it as
$$
L = \rho_{areal\_landscaping} = \rho_{A\_landscaping}
$$

Then,
$$
\sigma_t = a  \cdot  t \cdot ( \rho_{support} + \frac{\rho_{A\_landscaping}}{t} )$$

We want $t$ to be out, but how?
Insert $a \times r$ in:
$$
\sigma_t = \rho_{support}  \cdot  a  \cdot  r + \frac{\rho_{A\_landscaping}}{t}  \cdot  a  \cdot  r
$$
$$
\frac{\rho_{A\_landscaping}  \cdot  a  \cdot  r}{t} = \sigma_t - \rho_{support} a  \cdot  r
$$
$$
t = \frac{ \rho_{A\_landscaping}  \cdot  a  \cdot  r }{\sigma_t - \rho_{support}  \cdot  a  \cdot  r }
$$

Say, let's test it.
$$
\rho_{A\_landscape} = 1.4\times 10^4 m  \times  750 kgm^{-3} = 1.05\times 10^7 kgm^{-2}
$$

That is the maximum areal density of the habitat surface.
Then consider:
$$
a = 6 ms^{-2}
$$
$$
r = 4.98\times 10^6 m$$
$$\sigma_t = 10^{11} Pa$$
$$\rho_{support} = 1300 kgm^{-3}$$

Then, $t = 5137m$

This is a tube with radius of $4.98\times 10^6 m$, and surface thickness of $5,137 m$.
Its material density is $1,300 kgm^{-3}$, so areal density of $6.68\times 10^6 kgm^{-2}$, which with an outward acceleration of $6 ms^{-2}$, is equal to $4.01\times 10^7 Nm^{-2}$.
On the inner surface is landscaping with maximum areal density of $1.05\times 10^7 kgm^{-2}$, or $6.3\times 10^7 Nm^{-2}$.

The combined pressure is $1.03\times 10^8 Nm^{-2}$, then let's find its tangential stress:
$$
\sigma_t = \frac{1.03\times 10^8 Pa  \times  4.98\times 10^6 m}{5,137 m}
$$
$$
\sigma_t = 10^{11} Pa
$$

So that $5,137 m$ is the minimum thickness.
Let's multiply by 1.2 and we get $6,164 m$, and so the structure self-pressure is $6,164 m  \times  1,300 kgm^{-3}  \times  6 ms^{-2} = 4.81\times 10^7 Nm^{-2}$.
Then combined pressure is $1.11\times 10^8 Pa$, and tangential stress is:

$$
\sigma_t = \frac{1.11\times 10^8 Pa  \times  4.98\times 10^6 m}{6,164 m}
$$
$$
\sigma_t = 9\times 10^{10} Pa
$$

I think it is safe to say that a perturbation of $10^{10}$ Pa is fatal.
Say, $t = 10 km$, then $\rho_{structure} = 10^4 m  \times  1,300 kgm^{-3}  \times  6ms^{-2} = 7.8\times 10^7 Pa$, and $\Sigma_P = 1.4\times 10^8 Pa$, $\sigma_t = 7.02\times 10^{10} Pa$, I think this is far safer, as at least $2.98\times 10^{10} Pa$ perturbation is required to break the structure.

Since we mentioned centripetal acceleration to be $6ms^{-2}$, we must also calculate that:
$$
a_c = \frac{v^2}{r} = \omega^2  \cdot  r
$$

And $\omega$ is defined as $\omega = \frac{v}{r}$ in $s^{-1}$.

As $v = \frac{2\pi r}{T}$, and $\omega = \frac{2\pi}{T}$, therefore:

$$
T = \frac{2\pi r}{v} = \frac{2\pi}{\omega}
$$

Suppose at $a_c = 6ms^{-2}$, $\omega = \left( \frac{a_c}{r} \right)^{0.5} = 1.1\times 10^{-3} s^{-1}$, then $T = \frac{2 \pi}{\omega} = 5,724.25s$ or `1h35m24.25s`

We get $T = 5722.53s$ (`1h35m22.53s`) for $r = 4.977\times 10^6 m$.
