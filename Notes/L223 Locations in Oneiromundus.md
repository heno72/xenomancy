# Locations in Oneiromundus.

## Five states:
1. SMP: The State of Metal Petals, led by Prick Petal Proctor.
2. HH: The Hypnocratic Hypernation, led by Lead Manu Deisono and Firm Felix Redstone.
3. FLS: The Free Land of the Sun, led by Tawan Viraloneiro.
4. PSUL: The Police State of the Umbra Legion, led by Kao Ninekao Waikabkwamrak.
5. FSP: The Free States of People, led by Cad Aleirer Cadviri.

## Positions
Let's determine the positions. We know that PSUL and FSP tend to be in conflict. PSUL is an archipelago, while FSP is mountainous. SMP is either arctic or antarctic. HH is like the mainland china. FLS might be temperate, but no snow. Higher than the philipines, but still connected to the mainland.

The route we're after is: FLS, HH, SMP, FSP, PSUL. The model we're using is Asia.

I think, FSP and PSUL should be quite close to one another, so perhaps it's around Nepal and China? Or perhaps, FSP is on Australia, and PSUL is the Greater Indonesia region?

Meanwhile HH is mainland China, and is very close to FLS that spans around Thailand, Vietnam, and Myanmar.

Then SMP must be very higher up, toward the north of HH.
