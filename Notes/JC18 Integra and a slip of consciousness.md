---
## Consider not having fullcaps for headings.
title: Inspiration
## We don't need a subtitle for now
subtitle: Integra and a slip of consciousness
## Information about authorship(s)
author: Hendrik Lie
date: 18 December 2019
## Fonts and formatting.
fontfamily: times
fontsize: 12pt
linestretch: 1.25
indent: true
papersize: a4
## Numbersections
numbersections: true
secnumdepth: 2
---

# **20191218**

## 1. Realm-Slipping

Sometimes down to the road in between awareness and unawareness, I slipped through involuntary dream scenes.
There, sometimes I see snippets of life, of those I don't really recognize.
Sometimes, there's a story continuing from one episode to another episode of slipping.

Recently, as I fell to the dreamlike state at work, while reading Prof Is' book, I saw a woman by the airport.
She talked something I don't really remember.
Then I turned awake, and she's gone.
However the next time I slipped to the dreamlike state, I saw that woman departing to a flight, waving goodbyes toward me.

Perhaps, the experiences the tangible observers are similar to what I had.
Sometimes they slipped into the life that isn't theirs.
They're slipping into the life of people not found here in our world, but could exist in another.
To them, those were dream-like hallucination, otherworldly and probably never happens there.
To me, those dream-like hallucination, might be a real event int their reality.

By design, it means when people from my world saw people on their mundane hallucination, they saw the world of their counterpart in my world.
And when I saw snippets of mundane life in my dream-like hallucination, I saw through the lens of my counterpart in their world.
That is the basic premise of consciousness leakage.
The thing that Hadad really want to prevent.

So Hadad decided to use a part of Yam that was taken away from him using Aymuri.
Hadad wanted to just seal that part forever, but now rise a need to utilize it.
Hadad needs Tiamat with a wish that she would continually distract El from getting up.
Therefore Hadad could remain in command, and maintain Earth's immune system.

That is when Hadad ordering Anthony to summon Tiamat, Yam's heart.
Yam should agree on that as well, because that means he would be whole again.
But Yam has yet another agenda, so he halted the summoning with Heinrich.

> ***Note L917:*** The above paragraph is no longer valid.
As established, Anthony works directly under the supervision of Yam, he's Yam's angel.
Meanwhile Heinrich is an angel directly under Hadad, hence the uniform.

> Yam prepared to summon Tiamat, hence he ordered Anthony.
But it was just a distraction, so that Hadad would've realized the move, and ordered Heinrich to intervene.
An action of which, caused Heinrich to deplete the quota he, Hendrik, and Manov shared.
Therefore, Hendrik would be facing a next wave of attack the following night, with scarce quota, barely anything.

> The real reason for that first order to summon Tiamat, is to set up the situations suitable to finally, really summon Tiamat later, many years later.
In between them, are skirmishes, to force Hadad to cooperate with Yam to face a common enemy.
A common enemy, that Yam was made aware much earlier than Hadad.

Yam needs to recondition the competing clusters first, so that the Antichrist Cluster and the Christ Cluster were ready.
Otherwise, El wouldn't be awakened, and he couldn't start the next phase of his plans: insulting his father, El.
Hadad must be insulted too, so when El is awakened, the throne returned to him, and El would lose the throne he held for many millenia already.
And then, The Christ Cluster had the weapons ready, and would slaughter the Divines of Earth with the two weapons Kothar made.
And then Earth would have its immune system inactive, and Denefasan entered the scene.

## 2. Planned Integra Crisis Plot

So there are some people in Integra.
The plan is, the storyline progression is about their personal growth.
Each must learn something new, until they're finally awakened, and acquire qualities, that liberate their minds.
Then, only afterwards El will be awakened after they reach certain collective awareness.

However, the Men of Tiamat are still there.
After days of upfront display of strength, many inconsistent reports were around.
Hendrik Integra realized as a consequence of reality bending here.
He said that in Integra, the most important factor is the inhabitant's perceptions.
If something is unobserved, it does not exist.
And if something anomalous observed, the collective consciousness will attempt to rationalize it, and sometimes, realities overlap.

For example in one junction, some observed the dissapearance of objects directly.
However those that just come to the site sees it as some sort of the remains of bomb explosions.
Because of the overlap of what they perceives, a glitch usually manifested, and paranormal situation occured instead.
GaFE confirmed that in the place of attacks, there are some anomalous objects recovered.
For example, the events at Hendrik's apartment, it manifested in a dangerous paranormal entity code-named as Kamaitachi.
For every car and people made dissappear with Men of Tiamat's guns, at the same time of the day in an appropriate condition (nobody is around to see it), about five minutes prior to the time it occured the day before, sometimes the object made appearances.
Suddenly the person walks normally, before dissappearing at the time the gun was shot the day before.
Usually, it gives enough time for the dissapeared persons to contact his peers, or interact with the surroundings.
Or in the case of a car, when nobody is observing their spot five minutes before dissapearance, it will appear, and sometimes intersect with other car around the other corner.

All of those anomaly would gain publicity toward the Men of Tiamat, that decided to go after a more subtle approach.
They are going to put those that had been made aware (usually by interaction with Hendrik Integra) will be approached, and convinced that they needs help.
If they resisted not to, the last resort is to made them dissappear.
If they decided to join, they'd provide self help sessions, designed to make them ignore ugly memories.

1. **Hendrik Integra.**
Master of Notary, currently is a lawyer, also wrote hard science fictions, and do worldbuilding.
Has a wide array of knowledge about Xenomancy Earth, and Integra Earth, though not very detailed.
He has this dissatisfaction with his life, because he wanted to be a doctor, but he couldn't.
Had been through phases of religious, atheistic, agnosticism, practical religious-like state, etc, to the point he doesn't think labelling him is wise.
Had plenty of big questions about life, the universe, and everything.
Recently revisiting one of his old ideas, that the purpose of life is so that we could improve, and be one with God.
The end result of life is that we will be just like God.
Also pondered over the idea that God is a living entity in collective consciousness, a propagating idea.
Over the course of the story, he realized who he really is, who everyone in Integra is.

2. **Rahmah.**
A woman longing to be loved.
A free woman, she had been single for too long.
She is in a relationship, but she said she's not ready to progress to the next level (marriage).
Over the course of the story, she admitted that she knew she wouldn't get any younger.
Then she meets the one she recognized from her past, that she thought wasn't her first love.
She realized that true love is the kind of love that makes us feel home and welcomed, and we choose to embrace it for our own liberation.
True love is not something that we choose to keep because of necessity, but something that makes us feel home.

3. **Joseph.**
Joseph had his heart aimed for a woman.
However that woman never reciprocated back properly.
He had been hanging for too long, and his career is set.
He also nightwalking, preparing his side businesses beside his daily job as a prosecutor.
Had been alone for quite some time, never did he ever reconsider having a woman in his life.
Never since the one he loved long time ago.
And then, they meet again.

7. **Elbert.**
He had been through the business of networking.
He had plenty of networks, mainly under the product of insurance.
His main desire was to acquire as many money as he can.
But Hendrik changed something in his life, a taste of discussion never before he had.
Ever since then, money felt hollow. He had plenty of money, "but then, what?"
That was one of his most frequently asked questions.
Reintroduction of Hendrik to his life expanded his life premise.
It is no longer how to make people work for him.
It is about building a support system, where his recruited people work for their common benefits.
The more positive people in this world, the better the world will be.
It is not about him anymore, when he had plenty of money.
It is about people around him.
If everyone is happy, the world will be a centimeter closer to perfection.
Egoistical Altruism.
This is what Hendrik said to him:
> "Wealth is irrelevant, material wealth holds no meaning," Hendrik said.
> "What is important, is control over people.
> Power is control over people, and people control material wealths.
> Control people, and material wealths will follow."

8. **Ricardo.**

4. **Fernando Integra.**
Fernando is lonely, and never come out of the closet.
His life is heavy, and secluded.
He barely has any life beyond his work and his home.
He has many friends, but his true self is wrapped deep under the masks he wore for socialization.
He made up stories, he pretended to like women from time to time.
However people started to ask him questions, especially because he hasn't married yet.
He told people he needs to ensure his career security and to care of his parents first before he could even think of a life partner.
What he didn't tell people, is that he likes men.
His mind is opened, because for the first time in his life, he meet in person, his other self from a parallel world, that is open about himself.
And that he is with Daniel in that world, the love of his counterpart's life, and an old crush of him.
Then, he meets Daniel in his world, and things went awry when he kissed him, and realized that Daniel had a wife and a family already.
And Daniel here, is the husband of his ex, Helena, a woman that once dated him, that he accepts only out of necessity, for status.

6. **Daniel Integra.**

5. **Sylvester.**

6. **Helena.**
Was so in love with Fernando, dating him, but Fernando distanced himself away.
Growing tired of it, on the fifth month of their relationship, Helena called it off.
Years later she meets Daniel and had been together ever since.
However Daniel didn't provide the kind of emotional stimulation she wished to have, that she once had with Fernando.
Daniel is so different from Fernando in that, compared to Daniel, Fernando's emotional quotient is much higher.
Daniel's intelligence quotient might be higher, but it is not what she want to have.
She learned to tolerate Daniel for years already, and she felt quite content with her life already.
Until she meets Fernando for the second time.
