---
title: Types of Beings and Kinship
author: Hendrik Lie
date: M826
---

# Types of Beings and Kinship

## Beings

1. **Biological:**
   Biological entities are naturally evolving life forms or derived from naturally evolving life forms
   while still retain their biological nature.
   Includes xenobiological entites.
1. **Synthetic:**
   Entities equivalent in complexity with biological entities, but are designed instead of naturally evolving.
   Can be biological in nature, or alternative forms of biology, complex biochemistry, or any complex system.
1. **Infomorph:**
   Entities that entirely abolish the association with their physical forms and live only as information patterns in a computing substrate.
   Can occasionally manifest into a physical form with technological aids or body rentals.
   If the infospace is an emulation or a simulation of real or fictional physical worlds,
   resident infomorphs may be described as other types with an added suffix of "info-".
   For example a simulation of Planet Earth may contain biological simulation of a human person,
   then said person can be described as an info-biological entity.
1. **Artificial:**
   Entities specifically made for or specifically optimized for a certain purpose or a set of purposes.
   Distinct from Synthetics in term of its specialization of form or psychology.
1. **Transderivational:**	
   Entities whose original or previous form belong to a different type, but are currently adapted into another completely different type.
   The most common of transderivational entities are uploads or downloads.

## Nuclear Family

1. **Courtship:**
   A familial relationship of biological entities with the intent of biological breeding.
   In some society, courtship is equivalent to a marriage or partnership, with an added goal of forming and maintaining a nuclear family unit.
1. **Partnership:**
   A civil relationship between two or more individuals with the intent of maintaining a nuclear family unit.
   It comes with a benefit of shared wealth and civil management.
1. **Marriage:**
   A traditional civil partnership commonly defined as a union of a male and a female to form and maintain a nuclear family unit.

## Natural Kinship

1. **Sibling:**
   A relationship between at least two entities who share a common direct ancestor.
   
1. **Descendant:**
   A relationship where the entity is a direct product of at least one ancestor.
   
1. **Ancestor:**
   A relationship where the entity is a direct ancestor of a product entity, be it the only ancestor or just one of contributing ancestors.

## Synthetic Kinship

Before we describe synthetic kinship, it is the best to describe the kinds of relationship possible in kinship creation or acquisition (hereinafter referred as a kinship constitution).
Whatever ways a kinship is constituted, the end result is the creation of at least one more entity distinctive from the origin entity.
For a more general definition of a **constitution**, it is a creation of a new entity, that contains at least a **construct,** with or without existing **states.**

One could see a **construct** as a body belong to one of the types discussed above.
For example a **biological construct** can be seen as a biological body capable of self-regulating active negentropic activities (or in another word, to *stay alive*).
An **infomorph construct** can be seen as a code to describe the way it can interact with its containing infospace.

Meanwhile, a **state** can be seen as the state of mind of an entity.
A **biological construct**, for example, has its **state** of intercranial neural connections to define its identity.
An **infomorph construct**, in the other hand, can consider its working or storage memory as its **state**.

A **constitution** is therefore said to be a **vertical constitution** if the constitution result only in a new **construct** without an already existing **states** exported to the **construct**.
It can also be referred as a **cleanslate construct**, and the entity that create such entity is considered its **parents**.
The **cleanslate construct** would then be under the care of its **parents** until it is able to develop a proper **state** and be self-sufficient.

A **horizontal constitution** is the constitution of a new **construct** with an existing **state** exported.
It is sometimes colloquially said to be a **dirty construct**, though rarely described as such in formal documents.
A **horizontally constituted entity** is then analogous to a sibling to its parent entity, if the parent entity is the one whose construct and state are exported from.
Generally, exports of a person's construct or states to constitute a new entity cannot be done by any other entity but the very entity that embody such construct and states, or with their consent.

The entity that initiate or contribute its construct or state for a constitution is called a **producer**,
while a constituted entity is called a **product**.
In a **vertical constitution**, the **producer** is called a **parent**, while the constituted is called a **child**.
Meanwhile, for a **horizontal constitution**, the **producer** is called a **master**, while the constituted is called a **fork**.
Note that the terminology of a **master** has nothing to do with actually control or authority of the master over their forks.
The terminology of a **master** only refer to the origin of a constitution's construct and states.

If there is more than one **producer** (eg. genetic material donors of an embryo in a biological fertilization process),
then the **constitution** can also be called as a **synthesis**.
If a constitution event is initated by a **producer** with an existing **product**, then the **products** would have its relationship with the new **product** that depends on the nature of its constitution:

a. In the event of a **parent** initiate a new **vertical constitution**, then the new product be a **passive sibling** of existing **children**.
a. In the event of a **parent** initiate a new **horizontal constitution**, then the new product would be the parent's **active sibling**, and therefore be considered an **uncle/aunt** of the **parent**'s existing **children**.
a. In the event of a **master** initiate a new **vertical constitution**, then the new product would be the **nephew/niece** of existing **forks**.
a. In the event of a **master** initiate a new **horizontal constitution**, then the new product would be a **passive sibling** of existing **forks**.

In general, a **parent/children** (vertical) relationship would result in passive siblinghood between the children,
while a **master/forks** (horizontal) relationship would result in active siblinghood between the master and forks,
or passive siblinghood between the forks.

## Inheritance in general

Inheritance might vary in every worlds, but the most common pattern is as follows:

1. **Youngest fork first.**
   In the event of death, without "do not restore" notice signed by the victim,
   a new fork will be created from the latest backup.
   Under the **youngest fork first** rule, the new post-mortem fork will inherit everything.
   
   If "do not restore" notice is present, then the youngest fork will be chosen as the victim's sole inheritor.
   If there are more than one youngest forks at about the same age as they are created in a single action by the victim,
   then all forks under the same action that are equally youngest among all other forks, would receive the inheritance in equal parts.
   
1. **Children are second with equal parts.**
   In the event of death, and there are no forks made from the victim, then the inheritance will be split to equal parts for all children.

1. **Master is third.**
   In the event of death, and there are no forks or children of the victim, then the inheritance will be transferred to the master of the victim.

1. **Parents are fourth with equal parts.**
   In the event of death, and there are no forks, children, or master of the victim, then the inheritance will be transferred to the parent(s) of the victim.
   If there are more than one parents, then all parents will receive equal parts of the inheritance.

1. **Children of Parents are fifth with equal parts.**
   In the event of death, and there are no forks, children, master, or parents of the victim, then the inheritance will be transferred to all children of parents in equal parts.

1. **Youngest forks of Parents are sixth to replace each Parents.**
   In the event of death, and there are no forks, children, master, parents, or children of parents present,
   then for all youngest forks of each and every parents, they will receive the inheritance in parts according to the share of each parents.

Basically, the rest will function recursively until there is an inheritor for an inheritance.
The basic pattern is down first (children), then right (youngest fork), then left (master), then up (parents), and the cycle start again from parents' point of view.
The rationale is that as after every first degree of separation is explored (down, right, left, up) with no inheritor is found, then the inheritance is now a part of the parent's inheritance bundle.

Then the parent's point of view will also be explored as the second degree of separation.
At this point, if there are multiple parents, the bundle will seek out its inheritors on each branch individually, as they are considered to be split in equal parts to each individual parents.
Generally, at the seventh degree of separation and no inheritors are found, then the bundles that fail to find its inheritors will turn itself into public properties, managed directly by the Patron or its subsidiaries.

As multiple extant layer of generations are common in the Aucafidian volume, it is very likely for an inheritance bundle to reach an inheritor.
It is then a common theme, for most person in the majority age, at a random moment in their life, be notified that they have received an inheritance bundle from a relative they have no knowledge of.

